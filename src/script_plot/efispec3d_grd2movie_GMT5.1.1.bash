#!/bin/bash


############################################################################################################################################
#check the number of input parameter
############################################################################################################################################
if [ $# -lt 2 ] 
then
   echo "========================================================================================="
   echo "How to use this script"
   echo ""
   echo "To generate movie of displacement ground motion : $0 dis \"Movie's title\" [DEM.grd]"
   echo "To generate movie of velocity     ground motion : $0 vel \"Movie's title\" [DEM.grd]"
   echo "To generate movie of acceleration ground motion : $0 acc \"Movie's title\" [DEM.grd]"
   echo "========================================================================================="
   exit 1
else
   title=$2
fi

############################################################################################################################################
#get the prefix of the run and the time step
############################################################################################################################################
prefix=`cat prefix`
if [ "$prefix" = "" ]
then
   echo "========================================================================================="
   echo "Illegal prefix. Please check the file named prefix"
   echo "========================================================================================="
   exit 2
else
   echo "Prefix of the simulation = " $prefix
fi

############################################################################################################################################
#check file existency
############################################################################################################################################
if [ ! -f $prefix".cfg" ]
then
   echo $prefix".cfg" not found
   exit 4
fi
if [ ! -f $prefix".lst" ]
then
   echo $prefix".lst" not found
   exit 5
fi


############################################################################################################################################
#set snapshot file name and unit
############################################################################################################################################
if [ $1 = "dis" ]
then
   uva="u"
   prefix_x=${prefix}".snapshot.ux."
   prefix_y=${prefix}".snapshot.uy."
   prefix_z=${prefix}".snapshot.uz."
   dis_vel_acc="displacement"
   my_unit="displacement (m)"
elif [ $1 = "vel" ]
then
   uva="v"
   prefix_x=${prefix}".snapshot.vx."
   prefix_y=${prefix}".snapshot.vy."
   prefix_z=${prefix}".snapshot.vz."
   dis_vel_acc="velocity"
   my_unit="velocity (m/s)"
elif [ $1 = "acc" ]
then
   uva="a"
   prefix_x=${prefix}".snapshot.ax."
   prefix_y=${prefix}".snapshot.ay."
   prefix_z=${prefix}".snapshot.az."
   dis_vel_acc="acceleration"
   my_unit="acceleration (m/s@+2@+)"
fi

############################################################################################################################################
#Set GMT parameters
############################################################################################################################################
GMT_PATH="gmt "
${GMT_PATH}gmtdefaults -Ds > ./gmt.conf
W_page=1920
H_page=1080
${GMT_PATH}gmtset PS_PAGE_ORIENTATION landscape
${GMT_PATH}gmtset PS_MEDIA Custom_"$H_page"x"$W_page"
${GMT_PATH}gmtset FONT_TITLE 35
${GMT_PATH}gmtset FONT_ANNOT_PRIMARY 22
${GMT_PATH}gmtset FONT_ANNOT_SECONDARY 22
${GMT_PATH}gmtset FONT_LABEL 22
${GMT_PATH}gmtset FORMAT_FLOAT_OUT %.12g

############################################################################################################################################
#check if *.snapshot.*.grd files exist
############################################################################################################################################
check_snap=`ls *.snapshot.*.grd`
if [ "${check_snap}" = "" ]
then
   echo "========================================================================================="
   echo "Snapshot files not found"
   echo "========================================================================================="
   exit 3
else
   echo "Snapshot files found"
fi

############################################################################################################################################
#compute DEM gradient if a DEM file is given on command line
############################################################################################################################################
if [ $# -eq 3 ]
then
   is_dem="true"
   grd_dem=$3
   grd_dem_gradient=${grd_dem%.*}"_gradient.grd"

   echo "DEM file found = "${grd_dem}
   echo "Computing DEM gradient with option -A270"

   ${GMT_PATH}grdgradient ${grd_dem} -G${grd_dem_gradient} -A270 -Nt

#->Shift EFISPEC result to original geographic coordinates of the MNT file
#  for GRD_FILE in `ls ${prefix_x}*.grd ${prefix_y}*.grd ${prefix_z}*.grd`
#  do
#     echo "Translating" $GRD_FILE "to DEM coordinates"
#     ${GMT_PATH}grdedit ${GRD_FILE}=bf `${GMT_PATH}grdinfo ${grd_dem} -I-`
#  done
else
   is_dem="false"
fi

############################################################################################################################################
#get time step of the simulation
############################################################################################################################################
dt=`cat ${prefix}.cfg | grep -i "time step" | awk '{print $4}' | sed s/,/\\./g`

if [ "$dt" = "" ]
then
   dt=`cat ${prefix}.lst | grep -i "size     of time step" | awk '{print $7}' | sed s/,/\\./g`
fi

dt_mod=`cat ${prefix}.lst | grep -i "time step size has been modified to" | awk '{print $10}' | sed s/,/\\./g`
if [ "$dt_mod" != "" ]
then
   dt=$dt_mod
fi

echo "Time step = " $dt "s"

############################################################################################################################################
#Get min and max of all *.grd files
############################################################################################################################################
echo "Searching for min/max inside *.grd files"
for GRD_FILE in `ls ${prefix_x}*.grd ${prefix_y}*.grd ${prefix_z}*.grd`
do
   ${GMT_PATH}grdinfo ${GRD_FILE}=bf -C | awk '{ print $6,$7 }' >> xyz_minmax.tmp 
done

echo ""
echo "Searching for domain boundary inside"
for GRD_FILE in `ls ${prefix_x}"000001.grd"`
do
   ${GMT_PATH}grdinfo ${GRD_FILE}=bf -C | awk '{ print $2,$3,$4,$5 }' >> domain_xy_minmax.tmp 
done


#min max of displacement or velocity or acceleration in 3 directions
xyz_min=`${GMT_PATH}gmtinfo xyz_minmax.tmp -C | awk '{ print $1 }'`
xyz_max=`${GMT_PATH}gmtinfo xyz_minmax.tmp -C | awk '{ print $4 }'`

#min max of the domain in x and - coordinates
domain_x_min=`${GMT_PATH}gmtinfo domain_xy_minmax.tmp -C | awk '{ print $1 }'`
domain_x_max=`${GMT_PATH}gmtinfo domain_xy_minmax.tmp -C | awk '{ print $4 }'`
domain_y_min=`${GMT_PATH}gmtinfo domain_xy_minmax.tmp -C | awk '{ print $5 }'`
domain_y_max=`${GMT_PATH}gmtinfo domain_xy_minmax.tmp -C | awk '{ print $8 }'`
rm -f xyz_minmax.tmp domain_xy_minmax.tmp
echo "Size of the domain"
echo " --> xmin = " $domain_x_min
echo " --> xmax = " $domain_x_max
echo " --> ymin = " $domain_y_min
echo " --> ymax = " $domain_y_max

############################################################################################################################################
#compute GMT variables
############################################################################################################################################

abs_xyz_min=`echo ${xyz_min} | awk ' { if($1>=0) {print $1} else {print $1*-1} }'`
abs_xyz_max=`echo ${xyz_max} | awk ' { if($1>=0) {print $1} else {print $1*-1} }'`

xyz_max=`echo "${abs_xyz_min} ${abs_xyz_max}" | awk '{if ($1 > $2) {print +1*$1} else {print +1*$2}}'`
xyz_min=`echo "${abs_xyz_min} ${abs_xyz_max}" | awk '{if ($2 > $1) {print -1*$2} else {print -1*$1}}'`

nz_inc=100
dz_inc=`echo "${xyz_max} ${xyz_min} ${nz_inc}" | awk '{print ($1 - $2)/$3}'`

nz_scale=4
dz_scale=`echo "${xyz_max} ${xyz_min} ${nz_scale}" | awk '{print ($1 - $2)/$3}' | xargs printf "%6.2e" | sed s/,/\\./g`

${GMT_PATH}makecpt -Cjet -T${xyz_min}/${xyz_max}/${dz_inc} -D -Z > color.cpt

res=150

size_graph_x=`echo "${W_page}" | awk '{print $1/3.7}'`
size_graph_y=`echo "${size_graph_x} ${domain_x_min} ${domain_x_max} ${domain_y_min} ${domain_y_max}" | awk '{print $1*($5-$4)/($3-$2)}'`

x_psscale=`echo  "${size_graph_x}" | awk '{print +1*$1/2}' `
y_psscale=`echo  "${size_graph_y}" | awk '{print -1*$1/4}' `
size_scale=`echo "${size_graph_x}" | awk '{print $1*0.9}'`

my_JX_page="-JX${W_page}p/${H_page}p"
my_R_page="-R0/${W_page}/0/${H_page}"
my_JX_graph="-JX${size_graph_x}p/${size_graph_y}p"

x_title=`echo "${W_page}" | awk '{print $1/2.00}'`
y_title=`echo "${H_page}" | awk '{print $1*0.95}'`

x_code=`echo "${W_page}" | awk '{print $1*0.10}'`
y_code=`echo "${H_page}" | awk '{print $1*0.98}'`

x_time=`echo "${W_page}" | awk '{print $1/2.00}'`
y_time=`echo "${H_page}" | awk '{print $1*0.87}'`

x_graph1=`echo "$size_graph_x" | awk '{print $1*0.225}'`
x_graph_shift=`echo "$size_graph_x" | awk '{print $1*1.12}'`

ax_stride=`echo "${domain_x_max} ${domain_x_min}" | awk '{print ($1 - $2)/4}'`
ay_stride=`echo "${domain_y_max} ${domain_y_min}" | awk '{print ($1 - $2)/4}'`

############################################################################################################################################
#Plot time series *.grd files
############################################################################################################################################
frame_movie=0
for grd_file in `ls ${prefix_x}*.grd`
do

   frame=`echo ${grd_file} | awk -F "." '{print $4}'`
   frame_movie=`echo "${frame_movie} + 1" | bc -l | xargs printf "%06d"`
   my_time=`echo "${dt} ${frame}" | awk '{print $1 * ($2-1)}' | xargs printf "%10.5f" | sed s/,/\\./g`
   echo " "
   echo "Time = " ${my_time}

   ps_file=${prefix}".snapshot."${uva}"xyz."${frame}".ps"
   jpg_file=${prefix}".snapshot."${uva}"xyz."${frame_movie}".jpg"

   grdx_file=${prefix}".snapshot."${uva}"x."${frame}".grd"
   grdy_file=${prefix}".snapshot."${uva}"y."${frame}".grd"
   grdz_file=${prefix}".snapshot."${uva}"z."${frame}".grd"

   my_R_graph=`${GMT_PATH}grdinfo ${grdx_file}=bf -I-`

   echo "Plotting" ${grdx_file} ${grdy_file} ${grdz_file} "into" ${ps_file}

   
   echo "$x_title $y_title $title"              | gmt pstext ${my_JX_page} ${my_R_page} -F+f35p,Helvetica-Bold+jCM -N -Xf -Yf    -K > $ps_file

   echo "$x_code $y_code Computed by EFISPEC3D" | gmt pstext ${my_JX_page} ${my_R_page} -F+f28p,Helvetica-Bold+jCM -N -Xf -Yf -O -K >> $ps_file

   echo "$x_time $y_time $my_time s"            | gmt pstext ${my_JX_page} ${my_R_page} -F+f28p,Helvetica-Bold+jCM -N -Xf -Yf -O -K >> $ps_file

   if [ "$is_dem" = "true" ]
   then
      ${GMT_PATH}grdimage ${grdx_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."EW-${dis_vel_acc}":WeSn -E${res} -I${grd_dem_gradient} -X${x_graph1}p -Yc  -O -K >> $ps_file
   else
      ${GMT_PATH}grdimage ${grdx_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."EW-${dis_vel_acc}":WeSn -E${res}                       -X${x_graph1}p -Yc  -O -K >> $ps_file
   fi
   ${GMT_PATH}gmtset FORMAT_FLOAT_OUT %6.2e
   ${GMT_PATH}psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}:"${my_unit}":/:" ": -S                                                                                                         -O -K >> $ps_file
   ${GMT_PATH}gmtset FORMAT_FLOAT_OUT %.12g
 
   if [ "$is_dem" = "true" ]
   then
      ${GMT_PATH}grdimage ${grdy_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."NS-${dis_vel_acc}":weSn -E${res} -I${grd_dem_gradient} -X${x_graph_shift}p -O -K >> $ps_file
   else
      ${GMT_PATH}grdimage ${grdy_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."NS-${dis_vel_acc}":weSn -E${res}                       -X${x_graph_shift}p -O -K >> $ps_file
   fi
   ${GMT_PATH}gmtset FORMAT_FLOAT_OUT %6.2e
   ${GMT_PATH}psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}:"${my_unit}":/:" ": -S                                                                                                         -O -K >> $ps_file
   ${GMT_PATH}gmtset FORMAT_FLOAT_OUT %.12g
                                                                                                                                                        
   if [ "$is_dem" = "true" ]
   then
      ${GMT_PATH}grdimage ${grdz_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."UD-${dis_vel_acc}":wESn -E${res} -I${grd_dem_gradient} -X${x_graph_shift}p -O -K >> $ps_file
   else
      ${GMT_PATH}grdimage ${grdz_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."UD-${dis_vel_acc}":wESn -E${res}                       -X${x_graph_shift}p -O -K >> $ps_file
   fi
   ${GMT_PATH}gmtset FORMAT_FLOAT_OUT %6.2e
   ${GMT_PATH}psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}:"${my_unit}":/:" ": -S                                                                                                         -O    >> $ps_file
   ${GMT_PATH}gmtset FORMAT_FLOAT_OUT %.12g

   echo "Converting ps file to jpg"  
   convert $ps_file -rotate 90 -flatten -quality 100 -density ${res} $jpg_file
   rm $ps_file

done

rm color.cpt

############################################################################################################################################
#Create movie from jpg files
############################################################################################################################################
rm -f ${prefix}".movie."${uva}"xyz.avi"
avconv -r 10 -f image2 -i "${prefix}.snapshot.${uva}xyz."%06d".jpg" -b 10000k -minrate 10000k -maxrate 10000k -bufsize 5000k ${prefix}".movie."${uva}"xyz.avi"
