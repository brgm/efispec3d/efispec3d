#!/bin/bash


############################################################################################################################################
#check the number of input parameter
############################################################################################################################################
if [ $# -lt 3 ] 
then
   echo "========================================================================================="
   echo "Wrong number of input parameter"
   echo "How to use : $0 grd_filename dis/vel/acc \"My title\" [DEM.grd]"
   echo "========================================================================================="
   exit 1
else
   grd_file_ini=$1
   dis_vel_acc=$2
   title=$3
fi

grdformat="=bf"

logscale="false"

filter="false"


############################################################################################################################################
#Set GMT parameters
############################################################################################################################################
gmt gmtdefaults -Ds > ./gmt.conf
W_page=1500
H_page=1080
gmt gmtset PS_PAGE_ORIENTATION landscape
gmt gmtset PS_MEDIA Custom_"$H_page"x"$W_page"
gmt gmtset FONT_TITLE 35
gmt gmtset FONT_ANNOT_PRIMARY 25
gmt gmtset FONT_ANNOT_SECONDARY 25
gmt gmtset FONT_LABEL 25
gmt gmtset FORMAT_FLOAT_MAP %.12g
res=100

############################################################################################################################################
#Filter the data if needed
############################################################################################################################################
if [ "$filter" == "true" ]
then
   echo "Filtering results"
   tmp_file="$(mktemp)"
   gmt grdfilter ${grd_file_ini}${grdformat} -D0 -Fg400 -G${tmp_file}${grdformat} -V
   grd_file=${tmp_file}${grdformat}
else
   grd_file=${grd_file_ini}${grdformat}
fi

############################################################################################################################################
#compute DEM gradient if a DEM file is given on command line
############################################################################################################################################
if [ $# -eq 4 ]
then
   is_dem="true"
   grd_dem=$4
   grd_dem_gradient=${grd_dem%.*}"_gradient.grd"

   echo "DEM file found = "${grd_dem}
   echo "Computing DEM gradient with option -A270"

   ${GMT_PATH}grdgradient ${grd_dem} -G${grd_dem_gradient} -A270 -Nt -V

#->resample EFISPEC result to MNT resolution
#  tmp_file="$(mktemp)"
#  gmt grdsample ${grd_file} -G${tmp_file}${grdformat} -I10/10 -V
#  grd_file=${tmp_file}${grdformat}

#->Shift EFISPEC result to original geographic coordinates of the MNT file
   ${GMT_PATH}grdedit ${grd_file} `${GMT_PATH}grdinfo ${grd_dem} -I-` -V
else
   is_dem="false"
fi


############################################################################################################################################
#get domain size
############################################################################################################################################
gmt grdinfo ${grd_file} -C | awk '{ print $2,$3,$4,$5 }' >> domain_xy_minmax.tmp 

#min max of the domain in x and - coordinates
domain_x_min=`gmt gmtinfo domain_xy_minmax.tmp -C | awk '{ print $1 }'`
domain_x_max=`gmt gmtinfo domain_xy_minmax.tmp -C | awk '{ print $4 }'`
domain_y_min=`gmt gmtinfo domain_xy_minmax.tmp -C | awk '{ print $5 }'`
domain_y_max=`gmt gmtinfo domain_xy_minmax.tmp -C | awk '{ print $8 }'`
rm -f domain_xy_minmax.tmp
echo "Size of the domain"
echo " --> xmin = " $domain_x_min
echo " --> xmax = " $domain_x_max
echo " --> ymin = " $domain_y_min
echo " --> ymax = " $domain_y_max

############################################################################################################################################
#compute GMT variables
############################################################################################################################################

size_graph_x=`echo "${W_page}" | awk '{print $1/1.5}'`
size_graph_y=`echo " $size_graph_x * ($domain_y_max - $domain_y_min) / ($domain_x_max - $domain_x_min)" |bc -l`

x_psscale=`echo  "${size_graph_x}" | awk '{print +1*$1/2}' `
y_psscale=`echo  "${size_graph_y}" | awk '{print -1*$1/10}' `
size_scale=`echo "${size_graph_x}" | awk '{print $1*1.0}'`

my_JX_page="-JX${W_page}p/${H_page}p"
my_R_page="-Rp0/${W_page}/0/${H_page}"

my_JX_graph="-JX${size_graph_x}p/${size_graph_y}p"

x_title=`echo "${W_page}" | awk '{print $1/2.00}'`
y_title=`echo "${H_page}" | awk '{print $1*0.90}'`

x_code=`echo "${W_page}" | awk '{print $1/2.00}'`
y_code=`echo "${H_page}" | awk '{print $1*0.98}'`

x_time=`echo "${W_page}" | awk '{print $1/2.00}'`
y_time=`echo "${H_page}" | awk '{print $1*0.87}'`

ax_stride=`echo "${domain_x_max} ${domain_x_min}" | awk '{print ($1 - $2)/3}'`
ay_stride=`echo "${domain_y_max} ${domain_y_min}" | awk '{print ($1 - $2)/3}'`
ax_stride=10000
ay_stride=10000

############################################################################################################################################
#Plot PGA *.grd files
############################################################################################################################################
if [ "$dis_vel_acc" == "dis" ]
then
   my_unit="Horizontal PGD = (Ux@+2@+ + Uy@+2@+)@+1/2@+ [m]"
elif [ "$dis_vel_acc" == "vel" ]
then
   my_unit="Horizontal PGV = (Vx@+2@+ + Vy@+2@+)@+1/2@+ [m/s]"
elif [ "$dis_vel_acc" == "acc" ]
then
   my_unit="Horizontal PGA = (Ax@+2@+ + Ay@+2@+)@+1/2@+ [m/s@+2@+]"
fi

gmt grdinfo ${grd_file} -C | awk '{ print $6,$7 }' >> xyz_minmax.tmp

#min max of displacement or velocity or acceleration in 3 directions
if [ $logscale == "false" ]
then
   max_div=1
   xyz_min=0.0
   xyz_max=`gmt gmtinfo xyz_minmax.tmp -C | awk '{ print $3 }'`
   xyz_max=`echo "${xyz_max} ${max_div}" | awk '{print $1/$2}'`
#  xyz_max=0.06
   echo "max_val = " ${xyz_max}
   echo "min_val = " ${xyz_min}
else
   xyz_max=`gmt gmtinfo xyz_minmax.tmp -C | awk '{ print $3 }'`
   echo $xyz_max
   xyz_max=`echo "${xyz_max}" | awk '{print log($1)/log(10)}'`
   xyz_max=`echo "${xyz_max}" | awk '{if ($1 < 0.0 ) {print int($1-1)} else {print int($1+1)}}'`
   nscale=5
   xyz_min=`echo "${xyz_max} ${nscale}" | awk '{print $1 - $2}'`
fi
rm -f xyz_minmax.tmp

############################################################################
nz_inc=500
dz_inc=`echo "${xyz_max}  ${xyz_min} ${nz_inc}" | awk '{print ($1 - $2)/$3}'`

if [ $logscale == "false" ]
then
   nz_scale=6
else
   nz_scale=10
fi
dz_scale=`echo "${xyz_max}  ${xyz_min} ${nz_scale}" | awk '{print ($1 - $2)/$3}' | xargs printf "%6.2e" | sed s/,/\\./g`

if [ $logscale == "false" ]
then
   gmt makecpt -Ccubhelix -T${xyz_min}/${xyz_max}/${dz_inc} -D > color.cpt
#  python ~/Codes/EFISPEC3D/script_plot/efispec3d_cpt.py ${nz_inc} ${xyz_min} ${xyz_max}
#  python ./shakemap_cpt.py ${nz_inc} ${xyz_min} ${xyz_max}
else
   gmt makecpt -Cjet -T${xyz_min}/${xyz_max}/${dz_inc} -Qi -D -Z > color.cpt
fi

ps_file=${grd_file_ini%.*}".ps"
jpg_file=${grd_file_ini%.*}".jpg"

my_R_graph=`gmt grdinfo ${grd_file} -I-`

echo "Plotting" ${ps_file}

#title
echo "$x_title $y_title $title" | gmt pstext ${my_JX_page} ${my_R_page} -F+f35p,Helvetica-Bold+jCM -N -Xa -Ya    -K > $ps_file

#code name
echo "$x_code $y_code Computed by EFISPEC3D / Plotted with GMT" | gmt pstext ${my_JX_page} ${my_R_page} -F+f28p,Helvetica-Bold+jCM -N -Xa -Ya -O -K >> $ps_file

if [ "$is_dem" == "true" ]
then
   gmt grdimage ${grd_file} ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"X-UTM-34 (m)":/${ay_stride}:"Y-UTM-34 (m)":WESn -E${res} -I${grd_dem_gradient} -Xc -Yc  -O -K >> $ps_file
else
   gmt grdimage ${grd_file} ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"X-UTM-34 (m)":/${ay_stride}:"Y-UTM-34 (m)":WESn -E${res}                       -Xc -Yc  -O -K >> $ps_file
fi

#echo "713654.581  4492081.361     9.000      71.000     82.000   -121.000     3.300  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  6
#echo "706020.537  4506087.836     7.000      73.000     53.000   -118.000     3.700  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  7
#echo "708973.434  4507859.215     9.000     258.000     47.000    -96.000     3.700  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  8
#echo "698516.609  4515304.582     3.000     253.000     46.000    -98.000     3.100  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  9
#echo "693143.111  4505297.285     4.000     240.000     51.000    -89.000     2.700  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  10
#echo "661338.254  4519654.481     4.000     103.000     58.000    -94.000     3.500  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  11
#echo "699263.491  4508258.698     4.000     301.000     52.000    -77.000     3.000  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  12
#echo "705659.344  4518177.272     8.000      64.000     74.000   -116.000     3.400  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  13
#echo "683268.829  4510267.994    11.000     276.000     59.000    -95.000     3.500  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  14
#echo "696895.728  4503807.167     3.000      80.000     48.000    -83.000     2.800  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  15
#echo "704723.930  4498496.951     9.000     306.000     58.000    -52.000     2.900  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  16
#echo "701521.796  4507297.568    10.000      63.000     60.000   -174.000     3.400  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  17
#echo "718302.020  4493138.216     8.000     235.000     52.000   -157.000     4.600  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  18
#echo "694926.924  4499844.745     5.000      14.000     84.000      0.000     2.800  0.0 0.0" | gmt psmeca ${my_JX_graph} ${my_R_graph} -Sa2 -O -K >> $ps_file     #  19



gmt gmtset FORMAT_FLOAT_MAP %6.2e

if [ $logscale == "false" ]
then
   gmt psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}:"${my_unit}":/:" ":       -S -O    >> $ps_file
else
   gmt psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}pf3:"${my_unit}":/:" ": -Q -S -O    >> $ps_file
fi

gmt gmtset FORMAT_FLOAT_MAP %.12g


echo "Converting ps file to jpg"  
convert $ps_file -rotate 90 -flatten -quality 100 -density 300 $jpg_file
