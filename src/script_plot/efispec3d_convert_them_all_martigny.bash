#!/bin/bash

echo starting date : `date`
here=$PWD
workdir=${here}/WORK.$$
mkdir $workdir

simudir="${here}"
sacdir="${here}/SAC"

mkdir -p $sacdir

cd $workdir

#../MARTIGNY/RUN_EQ1_POUR_COMPARAISON_MANU/martigny.fsr.000001.gpl
#../MARTIGNY/RUN_EQ1_POUR_COMPARAISON_MANU/martigny.vor.000001.gpl
for fileb in `ls ${simudir}/*.gpl`
#for fileb in `ls ${simudir}/martigny.fsr.000001.gpl`
do
file=`basename $fileb`
/bin/cp -f $fileb $file
recid=`echo $file |awk -F"." '{print $3}'`
rectype=`echo $file |awk -F"." '{print $2}'`

echo $rectype $recid

case $rectype in
fsr) col=3;;
vor) col=4
esac
echo recid = $recid
recname=`sed -n ${recid}p ${simudir}/martigny.${rectype} | awk '{print $var}' var="$col"`
echo recname = $recname ...

efibin2sac.exe $file $recname

mv ${recname}.*.sac ${sacdir}

/bin/rm -f $file

done # fileb
cd $here

/bin/rm -rf $workdir
echo ending date : `date`
