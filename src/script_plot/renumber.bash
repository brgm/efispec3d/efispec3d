#!/bin/sh

i=0
for file in `ls *.jpg`
do
   i=`echo $i | awk '{printf("%06d",$i+1)}'`
   mv -v $file "snap"$i".jpg"
done

#avconv -r 10 -f image2 -i snap%06d.jpg -b 10000k -minrate 10000k -maxrate 10000k -bufsize 5000k e2vp2_S1.mp4
