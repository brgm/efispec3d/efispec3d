#!/bin/bash

grdformat="=bf"

############################################################################################################################################
#check the number of input parameter
############################################################################################################################################
if [ $# -lt 4 ] 
then

   echo "========================================================================================="
   echo "How to use this script"
   echo ""
   echo "To generate movie of displacement ground motion : $0 dt dis component \"Movie's title\" [DEM.grd]"
   echo "To generate movie of velocity     ground motion : $0 dt vel component \"Movie's title\" [DEM.grd]"
   echo "To generate movie of acceleration ground motion : $0 dt acc component \"Movie's title\" [DEM.grd]"
   echo "========================================================================================="
   exit 1

else

   dt=$1
   echo "Time step (s) = " ${dt}

   comp="$3"
   echo "Component = " ${comp}

   title=$4
   echo "Title = " ${title}

fi

############################################################################################################################################
#get the prefix of the run and the time step
############################################################################################################################################
prefix=`cat prefix`
if [ "$prefix" = "" ]
then
   echo "========================================================================================="
   echo "Illegal prefix. Please check the file named prefix"
   echo "========================================================================================="
   exit 2
else
   echo "Prefix of the simulation = " $prefix
fi


############################################################################################################################################
#set snapshot file name and unit
############################################################################################################################################

if [ $2 = "dis" ]
then
   uva="u"
   prefix_comp=${prefix}".snapshot."${uva}${comp}"."
   dis_vel_acc="displacement"
   my_unit="displacement (m)"
elif [ $2 = "vel" ]
then
   uva="v"
   prefix_comp=${prefix}".snapshot."${uva}${comp}"."
   dis_vel_acc="velocity"
   my_unit="velocity (m/s)"
elif [ $2 = "acc" ]
then
   uva="a"
   prefix_comp=${prefix}".snapshot."${uva}${comp}"."
   dis_vel_acc="acceleration"
   my_unit="acceleration (m/s@+2@+)"
fi

############################################################################################################################################
#Set GMT parameters
############################################################################################################################################
GMT_PATH="gmt "
${GMT_PATH}gmtdefaults -Ds > ./gmt.conf
W_page=1920
H_page=1080
${GMT_PATH}gmtset PS_PAGE_ORIENTATION landscape
${GMT_PATH}gmtset PS_MEDIA Custom_"$H_page"x"$W_page"
${GMT_PATH}gmtset FONT_TITLE 35
${GMT_PATH}gmtset FONT_ANNOT_PRIMARY 22
${GMT_PATH}gmtset FONT_ANNOT_SECONDARY 22
${GMT_PATH}gmtset FONT_LABEL 22
${GMT_PATH}gmtset FORMAT_FLOAT_OUT %.12g

############################################################################################################################################
#check if *.snapshot.*.grd files exist
############################################################################################################################################
check_snap=`ls ${prefix_comp}*.grd`
if [ "${check_snap}" = "" ]
then
   echo "========================================================================================="
   echo "Snapshot files not found"
   echo "========================================================================================="
   exit 3
else
   echo "Snapshot files found"
fi

############################################################################################################################################
#compute DEM gradient if a DEM file is given on command line
############################################################################################################################################
if [ $# -eq 5 ]
then

   is_dem="true"
   grd_dem=$5
   grd_dem_gradient=${grd_dem%.*}"_gradient_10x10.grd"

   echo "DEM file found = "${grd_dem}
   echo "Computing DEM gradient with option -A270"

#  ${GMT_PATH}grdgradient ${grd_dem} -G${grd_dem_gradient} -A270 -Nt

#->resample MNT result to EFI resolution
  tmp_file="$(mktemp)"
  gmt grdsample ${grd_dem_gradient} -G${tmp_file} `gmt grdinfo ${prefix_comp}"000001.grd${grdformat}" -I` -V
  grd_dem_gradient=${tmp_file}

#->Shift MNT gradient to grd_file coordinate and cut
   tmp_file="$(mktemp)"
   gmt grdedit ${grd_dem_gradient} -T -V
   gmt grdedit ${grd_dem_gradient} -R0/64990/0/42990 -V
   gmt grdcut  ${grd_dem_gradient} -G${tmp_file} `gmt grdinfo ${prefix_comp}"000001.grd${grdformat}" -I-` -V
   grd_dem_gradient=${tmp_file}
   gmt grdinfo ${grd_dem_gradient}

else

   is_dem="false"

fi

############################################################################################################################################
#Get min and max of all *.grd files
############################################################################################################################################
#echo "Searching for min/max inside *.grd files"
#for GRD_FILE in `ls ${prefix_comp}*.grd ${prefix_comp}*.grd ${prefix_z}*.grd`
#do
#   ${GMT_PATH}grdinfo ${GRD_FILE}=bf -C | awk '{ print $6,$7 }' >> xyz_minmax.tmp 
#done

echo ""
echo "Searching for domain boundary inside"
for GRD_FILE in `ls ${prefix_comp}"000001.grd"`
do
   ${GMT_PATH}grdinfo ${GRD_FILE}${grdformat} -C | awk '{ print $2,$3,$4,$5 }' >> domain_xy_minmax.tmp 
done


#min max of displacement or velocity or acceleration in 3 directions
#xyz_min=`${GMT_PATH}gmtinfo xyz_minmax.tmp -C | awk '{ print $1 }'`
#xyz_max=`${GMT_PATH}gmtinfo xyz_minmax.tmp -C | awk '{ print $4 }'`
xyz_min=-4.50
xyz_max=+4.50

#min max of the domain in x and - coordinates
domain_x_min=`${GMT_PATH}gmtinfo domain_xy_minmax.tmp -C | awk '{ print $1 }'`
domain_x_max=`${GMT_PATH}gmtinfo domain_xy_minmax.tmp -C | awk '{ print $4 }'`
domain_y_min=`${GMT_PATH}gmtinfo domain_xy_minmax.tmp -C | awk '{ print $5 }'`
domain_y_max=`${GMT_PATH}gmtinfo domain_xy_minmax.tmp -C | awk '{ print $8 }'`
rm -f xyz_minmax.tmp domain_xy_minmax.tmp
echo "Size of the domain"
echo " --> xmin = " $domain_x_min
echo " --> xmax = " $domain_x_max
echo " --> ymin = " $domain_y_min
echo " --> ymax = " $domain_y_max

############################################################################################################################################
#compute GMT variables
############################################################################################################################################

abs_xyz_min=`echo ${xyz_min} | awk ' { if($1>=0) {print $1} else {print $1*-1} }'`
abs_xyz_max=`echo ${xyz_max} | awk ' { if($1>=0) {print $1} else {print $1*-1} }'`

xyz_max=`echo "${abs_xyz_min} ${abs_xyz_max}" | awk '{if ($1 > $2) {print +1*$1} else {print +1*$2}}'`
xyz_min=`echo "${abs_xyz_min} ${abs_xyz_max}" | awk '{if ($2 > $1) {print -1*$2} else {print -1*$1}}'`

nz_inc=100
dz_inc=`echo "${xyz_max} ${xyz_min} ${nz_inc}" | awk '{print ($1 - $2)/$3}'`

nz_scale=4
dz_scale=`echo "${xyz_max} ${xyz_min} ${nz_scale}" | awk '{print ($1 - $2)/$3}' | xargs printf "%6.2e" | sed s/,/\\./g`

${GMT_PATH}makecpt -Cblack,magenta,white,purple,black,blue,white,cyan,black,127/127/127,white,green,black,yellow,white,orange,black,red,white -T${xyz_min}/${xyz_max}/${dz_inc} -D -Z > color.cpt

res=150

size_graph_x=`echo "${W_page}" | awk '{print $1/1.5}'`
size_graph_y=`echo "${size_graph_x} ${domain_x_min} ${domain_x_max} ${domain_y_min} ${domain_y_max}" | awk '{print $1*($5-$4)/($3-$2)}'`

x_psscale=`echo  "${size_graph_x}" | awk '{print +1*$1/2}' `
y_psscale=`echo  "${size_graph_y}" | awk '{print -1*$1/4}' `
size_scale=`echo "${size_graph_x}" | awk '{print $1*1.0}'`

my_JX_page="-JX${W_page}p/${H_page}p"
my_R_page="-R0/${W_page}/0/${H_page}"
my_JX_graph="-JX${size_graph_x}p/${size_graph_y}p"

ax_stride=`echo "${domain_x_max} ${domain_x_min}" | awk '{print ($1 - $2)/4}'`
ay_stride=`echo "${domain_y_max} ${domain_y_min}" | awk '{print ($1 - $2)/4}'`


############################################################################################################################################
#Plot time series *.grd files
############################################################################################################################################

frame_movie=0

#for grd_file in `ls ${prefix_comp}*.grd`
for grd_file in `ls ${prefix_comp}000050.grd`
do

   frame=`echo ${grd_file} | awk -F "." '{print $4}'`

   frame_movie=`echo "${frame_movie} + 1" | bc -l | xargs printf "%06d"`

   my_time=`echo "${dt} ${frame}" | awk '{print $1 * ($2-1)}' | xargs printf "%10.5f" | sed s/,/\\./g`

   echo " "
   echo "Time = " ${my_time}

    ps_file=${prefix}".snapshot."${uva}${comp}"."${frame}".ps"
   jpg_file=${prefix}".snapshot."${uva}${comp}"."${frame_movie}".jpg"

   grd_file=${prefix}".snapshot."${uva}${comp}"."${frame}".grd"

#
#->normalization
   tmp_file="$(mktemp)"
   gmt grdmath ${grd_file}${grdformat} 12.2 DIV = ${tmp_file}${grdformat} -V
   grd_file=${tmp_file}${grdformat}

   my_R_graph=`${GMT_PATH}grdinfo ${grd_file}${grdformat} -I-`

   echo "Plotting" ${grd_file} "into" ${ps_file}

   echo "$title"                   | gmt pstext ${my_JX_page} ${my_R_page} -F+f35p,Helvetica-Bold+jCT+cTC -DJ0p/40p  -N -Xf -Yf    -K > $ps_file

   echo "EFISPEC3D Simulation"     | gmt pstext ${my_JX_page} ${my_R_page} -F+f28p,Helvetica-Bold+jLT+cTL -DJ10p/10p -N -Xf -Yf -O -K >> $ps_file

   echo "Time = $my_time s"        | gmt pstext ${my_JX_page} ${my_R_page} -F+f28p,Helvetica-Bold+jRT+cTR -DJ10p/10p -N -Xf -Yf -O -K >> $ps_file

    if [ "$is_dem" = "true" ]
    then
       ${GMT_PATH}grdimage ${grd_file}${grdformat} ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -BWESn -Bx${ax_stride} -By${ay_stride} -E${res} -I${grd_dem_gradient} -Xc -Yc -O -K >> $ps_file
    else
       ${GMT_PATH}grdimage ${grd_file}${grdformat} ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -BWESn -Bx${ax_stride} -By${ay_stride} -E${res}                       -Xc -Yc -O -K >> $ps_file
    fi
 
   ${GMT_PATH}psxy Bordure_bassin_xy_shifted_small_box.txt ${my_JX_graph} ${my_R_graph} -W2.0,, -Xc -Yc  -O -K >> $ps_file

   ${GMT_PATH}gmtset FORMAT_FLOAT_OUT %6.2e
   ${GMT_PATH}psscale -Ccolor.cpt -DjCB+w${size_scale}p/20p+o0/-2c+h -Bx0.5+l"Normalized amplitude" ${my_JX_graph} ${my_R_graph} -S  -O -K >> $ps_file
   ${GMT_PATH}gmtset FORMAT_FLOAT_OUT %.12g
                                                                                                                                                        
   echo "Converting ps file to jpg"  
   convert $ps_file -rotate 90 -flatten -quality 100 -density ${res} $jpg_file
   rm $ps_file

   rm ${tmp_file}

done

#remove tmp files
rm color.cpt
rm -f /tmp/tmp.*

############################################################################################################################################
#Create movie from jpg files
############################################################################################################################################
#rm -f ${prefix}".movie."${uva}"xyz.avi"
#avconv -r 10 -f image2 -i "${prefix}.snapshot.${uva}xyz."%06d".jpg" -b 10000k -minrate 10000k -maxrate 10000k -bufsize 5000k ${prefix}".movie."${uva}"xyz.avi"
