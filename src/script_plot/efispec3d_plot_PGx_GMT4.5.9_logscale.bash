#!/bin/bash

############################################################################################################################################
#Set GMT parameters
############################################################################################################################################
gmtdefaults -Ds > ~/.gmtdefault4
W_page=1920
H_page=1080
gmtset PAGE_ORIENTATION landscape
gmtset PAPER_MEDIA Custom_"$H_page"x"$W_page" #paper format adpated for generating mp4 movie
gmtset HEADER_FONT_SIZE 35
gmtset ANNOT_FONT_SIZE_PRIMARY 22
gmtset LABEL_FONT_SIZE 22
gmtset D_FORMAT %.12g

############################################################################################################################################
#check the number of input parameter
############################################################################################################################################
if [ $# -ne 1 ] 
then
   echo "========================================================================================="
   echo "Wrong number of input parameter"
   echo "How to use : $0 \"My title\""
   echo "========================================================================================="
   exit 1
fi

############################################################################################################################################
#get the prefix of the run and the time step
############################################################################################################################################
prefix=`cat prefix.in`
if [ "$prefix" = "" ]
then
   echo "========================================================================================="
   echo "Illegal prefix"
   echo "Check file prefix.in"
   echo "========================================================================================="
   exit 2
fi

############################################################################################################################################
#check if *.snapshot.*.grd files exist
############################################################################################################################################
check_snap=`ls *.snapshot.PG*.grd`
if [ "${check_snap}" = "" ]
then
   echo "========================================================================================="
   echo "Snapshots of peak ground motion not found"
   echo "========================================================================================="
   exit 3
fi

title=$1

############################################################################################################################################
#get domain size
############################################################################################################################################
for GRD_FILE in `ls *snapshot.PG*.grd`
do
   grdinfo ${GRD_FILE}=bf -C | awk '{ print $2,$3,$4,$5 }' >> domain_xy_minmax.tmp 
done
#min max of the domain in x and - coordinates
domain_x_min=`minmax domain_xy_minmax.tmp -C | awk '{ print $1 }'`
domain_x_max=`minmax domain_xy_minmax.tmp -C | awk '{ print $4 }'`
domain_y_min=`minmax domain_xy_minmax.tmp -C | awk '{ print $5 }'`
domain_y_max=`minmax domain_xy_minmax.tmp -C | awk '{ print $8 }'`
rm -f domain_xy_minmax.tmp
echo "Size of the domain"
echo " --> xmin = " $domain_x_min
echo " --> xmax = " $domain_x_max
echo " --> ymin = " $domain_y_min
echo " --> ymax = " $domain_y_max

############################################################################################################################################
#compute GMT variables
############################################################################################################################################
res=150

size_graph_x=`echo "${W_page}" | awk '{print $1/3.7}'`
size_graph_y=$size_graph_x

x_psscale=`echo  "${size_graph_x}" | awk '{print +1*$1/2}' `
y_psscale=`echo  "${size_graph_y}" | awk '{print -1*$1/4}' `
size_scale=`echo "${size_graph_x}" | awk '{print $1*0.9}'`

my_JX_page="-JX${W_page}p/${H_page}p"
my_R_page="-R0/${W_page}/0/${H_page}"

my_JX_graph="-JX${size_graph_x}p/${size_graph_x}p"

x_title=`echo "${W_page}" | awk '{print $1/2.00}'`
y_title=`echo "${H_page}" | awk '{print $1*0.95}'`

x_code=`echo "${W_page}" | awk '{print $1*0.08}'`
y_code=`echo "${H_page}" | awk '{print $1*0.99}'`
code_name="EFISPEC3D"

x_time=`echo "${W_page}" | awk '{print $1/2.00}'`
y_time=`echo "${H_page}" | awk '{print $1*0.87}'`

x_graph1=`echo "$size_graph_x" | awk '{print $1*0.225}'`
x_graph_shift=`echo "$size_graph_x" | awk '{print $1*1.12}'`

ax_stride=`echo "${domain_x_max} ${domain_x_min}" | awk '{print ($1 - $2)/4}'`
ay_stride=`echo "${domain_y_max} ${domain_y_min}" | awk '{print ($1 - $2)/4}'`

############################################################################################################################################
#Plot PGD *.grd files
############################################################################################################################################
pgm="PGD"
dis_vel_acc="displacement"
my_unit="displacement (m)"

grdx_file=${prefix}".snapshot."${pgm}"x.grd"
grdy_file=${prefix}".snapshot."${pgm}"y.grd"
grdz_file=${prefix}".snapshot."${pgm}"z.grd"

if [ ! -f ${grdx_file} ]
then
   echo "File ${grdx_file} not found. Peak ground motion not plotted."
   exit 4
fi
if [ ! -f ${grdy_file} ]
then
   echo "File ${grdy_file} not found. Peak ground motion not plotted."
   exit 4
fi
if [ ! -f ${grdz_file} ]
then
   echo "File ${grdz_file} not found. Peak ground motion not plotted."
   exit 4
fi

for GRD_FILE in "${grdx_file} ${grdy_file} ${grdz_file}"
do
   grdinfo ${GRD_FILE}=bf -C | awk '{ print $6,$7 }' >> xyz_minmax.tmp
done
#min max of displacement or velocity or acceleration in 3 directions
xyz_max=`minmax xyz_minmax.tmp -C | awk '{ print $4 }'`
xyz_max=`echo "${xyz_max}" | awk '{print log($1)/log(10)}'`
xyz_max=`echo "${xyz_max}" | awk '{if ($1 < 0.0 ) {print int($1-1)} else {print int($1+1)}}'`
nscale=1
xyz_min=`echo "${xyz_max} ${nscale}" | awk '{print $1 - $2}'`
rm -f xyz_minmax.tmp

############################################################################
nz_inc=100
dz_inc=`echo "${xyz_max}  ${xyz_min} ${nz_inc}" | awk '{print ($1 - $2)/$3}'`

nz_scale=10
dz_scale=`echo "${xyz_max}  ${xyz_min} ${nz_scale}" | awk '{print ($1 - $2)/$3}' | xargs printf "%6.2e" | sed s/,/\\./g`

makecpt -Cpolar -T${xyz_min}/${xyz_max}/${dz_inc} -Qi -D -Z > color.cpt

my_time="${pgm}"

ps_file=${prefix}".snapshot."${pgm}"xyz.logscale.ps"
jpg_file=${prefix}".snapshot."${pgm}"xyz.logscale.jpg"

my_R_graph=`grdinfo ${grdx_file}=bf -I-`

echo "Plotting" ${ps_file}

#title
pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya    -K <<EOF  > $ps_file
$x_title $y_title 50 0 1 CM $title
EOF
#code name
pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya -O -K <<EOF >> $ps_file
$x_code $y_code 25 0 1 CM Computed by $code_name
EOF
#time
pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya -O -K <<EOF >> $ps_file
$x_time $y_time 25 0 1 CM ${my_time}
EOF

grdimage ${grdx_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."EW-${dis_vel_acc}":WeSn -E${res} -X${x_graph1}p -Yc  -O -K >> $ps_file
gmtset D_FORMAT %6.2e
psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}pf3:"${my_unit}":/:" ": -Q -S                                                                              -O -K >> $ps_file
gmtset D_FORMAT %.12g

grdimage ${grdy_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."NS-${dis_vel_acc}":weSn -E${res} -X${x_graph_shift}p -O -K >> $ps_file
gmtset D_FORMAT %6.2e
psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}pf3:"${my_unit}":/:" ": -Q -S                                                                              -O -K >> $ps_file
gmtset D_FORMAT %.12g
                                                                                                                                                     
grdimage ${grdz_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."UD-${dis_vel_acc}":wESn -E${res} -X${x_graph_shift}p -O -K >> $ps_file
gmtset D_FORMAT %6.2e
psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}pf3:"${my_unit}":/:" ": -Q -S                                                                              -O    >> $ps_file
gmtset D_FORMAT %.12g

echo "Converting ps file to jpg"  
convert $ps_file -rotate 90 -flatten -quality 100 -density ${res} $jpg_file

############################################################################################################################################
#Plot PGV *.grd files
############################################################################################################################################
pgm="PGV"
dis_vel_acc="velocity"
my_unit="velocity (m/s)"

grdx_file=${prefix}".snapshot."${pgm}"x.grd"
grdy_file=${prefix}".snapshot."${pgm}"y.grd"
grdz_file=${prefix}".snapshot."${pgm}"z.grd"

if [ ! -f ${grdx_file} ]
then
   echo "File ${grdx_file} not found. Peak ground motion not plotted."
   exit 5
fi
if [ ! -f ${grdy_file} ]
then
   echo "File ${grdy_file} not found. Peak ground motion not plotted."
   exit 5
fi
if [ ! -f ${grdz_file} ]
then
   echo "File ${grdz_file} not found. Peak ground motion not plotted."
   exit 5
fi

for GRD_FILE in "${grdx_file} ${grdy_file} ${grdz_file}"
do
   grdinfo ${GRD_FILE}=bf -C | awk '{ print $6,$7 }' >> xyz_minmax.tmp
done
#min max of displacement or velocity or acceleration in 3 directions
xyz_max=`minmax xyz_minmax.tmp -C | awk '{ print $4 }'`
xyz_max=`echo "${xyz_max}" | awk '{print log($1)/log(10)}'`
xyz_max=`echo "${xyz_max}" | awk '{if ($1 < 0.0 ) {print int($1-1)} else {print int($1+1)}}'`
xyz_min=`echo "${xyz_max} ${nscale}" | awk '{print $1 - $2}'`
rm -f xyz_minmax.tmp

############################################################################
nz_inc=100
dz_inc=`echo "${xyz_max}  ${xyz_min} ${nz_inc}" | awk '{print ($1 - $2)/$3}'`

dz_scale=`echo "${xyz_max}  ${xyz_min} ${nz_scale}" | awk '{print ($1 - $2)/$3}' | xargs printf "%6.2e" | sed s/,/\\./g`

makecpt -Cpolar -T${xyz_min}/${xyz_max}/${dz_inc} -Qi -D -Z > color.cpt

my_time="${pgm}"

ps_file=${prefix}".snapshot."${pgm}"xyz.logscale.ps"
jpg_file=${prefix}".snapshot."${pgm}"xyz.logscale.jpg"

my_R_graph=`grdinfo ${grdx_file}=bf -I-`

echo "Plotting" ${ps_file}

#title
pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya    -K <<EOF  > $ps_file
$x_title $y_title 50 0 1 CM $title
EOF
#code name
pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya -O -K <<EOF >> $ps_file
$x_code $y_code 25 0 1 CM Computed by $code_name
EOF
#time
pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya -O -K <<EOF >> $ps_file
$x_time $y_time 25 0 1 CM ${my_time}
EOF

grdimage ${grdx_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."EW-${dis_vel_acc}":WeSn -E${res} -X${x_graph1}p -Yc  -O -K >> $ps_file
gmtset D_FORMAT %6.2e
psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}pf3:"${my_unit}":/:" ": -Q -S                                                                              -O -K >> $ps_file
gmtset D_FORMAT %.12g

grdimage ${grdy_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."NS-${dis_vel_acc}":weSn -E${res} -X${x_graph_shift}p -O -K >> $ps_file
gmtset D_FORMAT %6.2e
psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}pf3:"${my_unit}":/:" ": -Q -S                                                                              -O -K >> $ps_file
gmtset D_FORMAT %.12g
                                                                                                                                                     
grdimage ${grdz_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."UD-${dis_vel_acc}":wESn -E${res} -X${x_graph_shift}p -O -K >> $ps_file
gmtset D_FORMAT %6.2e
psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}pf3:"${my_unit}":/:" ": -Q -S                                                                              -O    >> $ps_file
gmtset D_FORMAT %.12g

echo "Converting ps file to jpg"  
convert $ps_file -rotate 90 -flatten -quality 100 -density ${res} $jpg_file


############################################################################################################################################
#Plot PGA *.grd files
############################################################################################################################################
pgm="PGA"
dis_vel_acc="acceleration"
my_unit="acceleration (m/s@+2@%%)"

grdx_file=${prefix}".snapshot."${pgm}"x.grd"
grdy_file=${prefix}".snapshot."${pgm}"y.grd"
grdz_file=${prefix}".snapshot."${pgm}"z.grd"

if [ ! -f ${grdx_file} ]
then
   echo "File ${grdx_file} not found. Peak ground motion not plotted."
   exit 6
fi
if [ ! -f ${grdy_file} ]
then
   echo "File ${grdy_file} not found. Peak ground motion not plotted."
   exit 6
fi
if [ ! -f ${grdz_file} ]
then
   echo "File ${grdz_file} not found. Peak ground motion not plotted."
   exit 6
fi

for GRD_FILE in `ls ${grdx_file} ${grdy_file} ${grdz_file}`
do
   grdinfo ${GRD_FILE}=bf -C | awk '{ print $6,$7 }' >> xyz_minmax.tmp
done
#min max of displacement or velocity or acceleration in 3 directions
xyz_max=`minmax xyz_minmax.tmp -C | awk '{ print $4 }'`
xyz_max=`echo "${xyz_max}" | awk '{print log($1)/log(10)}'`
xyz_max=`echo "${xyz_max}" | awk '{if ($1 < 0.0 ) {print int($1-1)} else {print int($1+1)}}'`
xyz_min=`echo "${xyz_max} ${nscale}" | awk '{print $1 - $2}'`
rm -f xyz_minmax.tmp

############################################################################
nz_inc=100
dz_inc=`echo "${xyz_max}  ${xyz_min} ${nz_inc}" | awk '{print ($1 - $2)/$3}'`

dz_scale=`echo "${xyz_max}  ${xyz_min} ${nz_scale}" | awk '{print ($1 - $2)/$3}' | xargs printf "%6.2e" | sed s/,/\\./g`

makecpt -Cpolar -T${xyz_min}/${xyz_max}/${dz_inc} -Qi -D -Z > color.cpt

my_time="${pgm}"

ps_file=${prefix}".snapshot."${pgm}"xyz.logscale.ps"
jpg_file=${prefix}".snapshot."${pgm}"xyz.logscale.jpg"

my_R_graph=`grdinfo ${grdx_file}=bf -I-`

echo "Plotting" ${ps_file}

#title
pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya    -K <<EOF  > $ps_file
$x_title $y_title 50 0 1 CM $title
EOF
#code name
pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya -O -K <<EOF >> $ps_file
$x_code $y_code 25 0 1 CM Computed by $code_name
EOF
#time
pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya -O -K <<EOF >> $ps_file
$x_time $y_time 25 0 1 CM ${my_time}
EOF

grdimage ${grdx_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."EW-${dis_vel_acc}":WeSn -E${res} -X${x_graph1}p -Yc  -O -K >> $ps_file
gmtset D_FORMAT %6.2e
psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}pf3:"${my_unit}":/:" ": -Q -S                                                                              -O -K >> $ps_file
gmtset D_FORMAT %.12g

grdimage ${grdy_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."NS-${dis_vel_acc}":weSn -E${res} -X${x_graph_shift}p -O -K >> $ps_file
gmtset D_FORMAT %6.2e
psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}pf3:"${my_unit}":/:" ": -Q -S                                                                              -O -K >> $ps_file
gmtset D_FORMAT %.12g
                                                                                                                                                     
grdimage ${grdz_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."UD-${dis_vel_acc}":wESn -E${res} -X${x_graph_shift}p -O -K >> $ps_file
gmtset D_FORMAT %6.2e
psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}pf3:"${my_unit}":/:" ": -Q -S                                                                              -O    >> $ps_file
gmtset D_FORMAT %.12g

echo "Converting ps file to jpg"  
convert $ps_file -rotate 90 -flatten -quality 100 -density ${res} $jpg_file
