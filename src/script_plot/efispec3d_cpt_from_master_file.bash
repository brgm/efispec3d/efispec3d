#!/bin/bash

#master cpt in python
python efispec3d_cpt_white_delimitation.py 20 0 1 

#linear color scale
gmt makecpt -Ccolor.cpt -T0/10 -Z > test_lin.cpt

#log color scale
gmt makecpt -Ccolor.cpt -T1e-10/1e10/1  -Qo -Z > test_log1.cpt
gmt makecpt -Ccolor.cpt -T1e-10/1e10/2  -Qo -Z > test_log2.cpt
gmt makecpt -Ccolor.cpt -T1e-10/1e10/3  -Qo -Z > test_log3.cpt
gmt makecpt -Ccolor.cpt -T1e-25/1e-5/1  -Qo -Z > test_log4.cpt

#plot example
psscale -D13/2/25/1h  -Ctest_lin.cpt     -Xc        -K > scale.ps

psscale -D13/5/25/1h  -Ctest_log1.cpt -Q -Xa -Ya -O -K >> scale.ps

psscale -D13/8/25/1h  -Ctest_log2.cpt -Q -Xa -Ya -O -K >> scale.ps

psscale -D13/11/25/1h -Ctest_log3.cpt -Q -Xa -Ya -O -K >> scale.ps

psscale -D13/14/25/1h -Ctest_log4.cpt -Q -Xa -Ya -O    >> scale.ps

okular scale.ps
