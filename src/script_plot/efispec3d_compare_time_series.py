#!/usr/bin/python

import sys

import matplotlib.pyplot as plt

import numpy as np

import scipy.signal as signal

#######################################################################################################################################################
def read_efi_binary_time_series(myfile):

   """read binary file of EFISPEC3D's time series"""

   dt = np.dtype([('time','f4'),('ux','f4'),('uy','f4'),('uz','f4'),('vx','f4'),('vy','f4'),('vz','f4'),('ax','f4'),('ay','f4'),('az','f4')])

   with open(myfile,"rb") as f:
      x = np.fromfile(f,dtype=dt)

   return x
#######################################################################################################################################################

def my_plot(t1,ux1,uy1,uz1,vx1,vy1,vz1,ax1,ay1,az1,t2,ux2,uy2,uz2,vx2,vy2,vz2,ax2,ay2,az2,dva):

   """Plot displacement, velocity or acceleration"""

   #Make plot
   fig = plt.figure()
   
   if dva == "dis":
      #subplot ux
      fig.add_subplot(3,1,1)
      plt.plot(t1,ux1,'r')
      plt.plot(t2,ux2,'b')
      plt.ylabel("Ux (m)")
      #subplot uy
      fig.add_subplot(3,1,2)
      plt.plot(t1,uy1,'r')
      plt.plot(t2,uy2,'b')
      plt.ylabel("Uy (m)")
      #subplot uy
      fig.add_subplot(3,1,3)
      plt.plot(t1,uy1,'r')
      plt.plot(t2,uy2,'b')
      plt.xlabel("Time (s)")
      plt.ylabel("Uz (m)")
      plt.show()
   
   if dva == "vel":
      #subplot vx
      fig.add_subplot(3,1,1)
      plt.plot(t1,vx1,'r')
      plt.plot(t2,vx2,'g')
      plt.ylabel("Vx (m/s)")
      #subplot vy
      fig.add_subplot(3,1,2)
      plt.plot(t1,vy1,'r')
      plt.plot(t2,vy2,'g')
      plt.ylabel("Vy (m/s)")
      #subplot vz
      fig.add_subplot(3,1,3)
      plt.plot(t1,vz1,'r')
      plt.plot(t2,vz2,'g')
      plt.xlabel("Time (s)")
      plt.ylabel("Vz (m/s)")
      plt.show()
      
   if dva == "acc":
      #subplot ax
      fig.add_subplot(3,1,1)
      plt.plot(t1,ax1,'r')
      plt.plot(t2,ax2,'y')
      plt.ylabel("Ax (m/s/s)")
      #subplot ay
      fig.add_subplot(3,1,2)
      plt.plot(t1,ay1,'r')
      plt.plot(t2,ay2,'y')
      plt.ylabel("Ay (m/s/s)")
      #subplot az
      fig.add_subplot(3,1,3)
      plt.plot(t1,az1,'r')
      plt.plot(t2,az2,'y')
      plt.xlabel("Time (s)")
      plt.ylabel("Az (m/s/s)")
      plt.show()

   return

#######################################################################################################################################################

total_arg  = len(sys.argv)

if total_arg != 4:
   print "script usage: "+str(sys.argv[0])+" EFISPEC3D_file1.gpl EFISPEC3D_file2.gpl dis/vel/acc"
   quit()

filename1 = str(sys.argv[1])
filename2 = str(sys.argv[2])
dva       = str(sys.argv[3])

time_series1 = read_efi_binary_time_series(filename1)
time1 = time_series1['time'] #not a copy, just a 'pointer'
ux1   = time_series1['ux']
uy1   = time_series1['uy']
uz1   = time_series1['uz']
vx1   = time_series1['vx']
vy1   = time_series1['vy']
vz1   = time_series1['vz']
ax1   = time_series1['ax']
ay1   = time_series1['ay']
az1   = time_series1['az']

time_series2 = read_efi_binary_time_series(filename2)
time2 = time_series2['time'] #not a copy, just a 'pointer'
ux2   = time_series2['ux']
uy2   = time_series2['uy']
uz2   = time_series2['uz']
vx2   = time_series2['vx']
vy2   = time_series2['vy']
vz2   = time_series2['vz']
ax2   = time_series2['ax']
ay2   = time_series2['ay']
az2   = time_series2['az']

#Time series info
dt1  = time1[1] - time1[0]
fs1  = np.floor(1.0/dt1)

#Filter design
order = 4
fcut  = 5.0
Nyq   = fs1*0.5
B,A   = signal.butter(N=order,Wn=fcut/Nyq,btype='lowpass',output='ba')

#Filter data
ux1f = signal.filtfilt(B,A,ux1)
uy1f = signal.filtfilt(B,A,uy1)
uz1f = signal.filtfilt(B,A,uz1)
vx1f = signal.filtfilt(B,A,vx1)
vy1f = signal.filtfilt(B,A,vy1)
vz1f = signal.filtfilt(B,A,vz1)
ax1f = signal.filtfilt(B,A,ax1)
ay1f = signal.filtfilt(B,A,ay1)
az1f = signal.filtfilt(B,A,az1)

#Time series info
dt2  = time2[1] - time2[0]
fs2  = np.floor(1.0/dt2)

#Filter design
Nyq   = fs2*0.5
B,A   = signal.butter(N=order,Wn=fcut/Nyq,btype='lowpass',output='ba')

#Filter data
ux2f = signal.filtfilt(B,A,ux2)
uy2f = signal.filtfilt(B,A,uy2)
uz2f = signal.filtfilt(B,A,uz2)
vx2f = signal.filtfilt(B,A,vx2)
vy2f = signal.filtfilt(B,A,vy2)
vz2f = signal.filtfilt(B,A,vz2)
ax2f = signal.filtfilt(B,A,ax2)
ay2f = signal.filtfilt(B,A,ay2)
az2f = signal.filtfilt(B,A,az2)



my_plot(time1,ux1,uy1,uz1,vx1,vy1,vz1,ax1,ay1,az1,time2,ux2,uy2,uz2,vx2,vy2,vz2,ax2,ay2,az2,dva)
#my_plot(time1,ux1f,uy1f,uz1f,vx1f,vy1f,vz1f,ax1f,ay1f,az1f,time2,ux2f,uy2f,uz2f,vx2f,vy2f,vz2f,ax2f,ay2f,az2f,dva)
