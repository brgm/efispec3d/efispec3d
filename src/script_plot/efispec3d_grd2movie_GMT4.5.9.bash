#!/bin/bash

GMT_PATH=~/Codes/GMT/GMT4.5.9/bin/

############################################################################################################################################
#Set GMT parameters
############################################################################################################################################
${GMT_PATH}gmtdefaults -Ds > ~/.gmtdefault4
W_page=1920
H_page=1080
${GMT_PATH}gmtset PAGE_ORIENTATION landscape
${GMT_PATH}gmtset PAPER_MEDIA Custom_"$H_page"x"$W_page" #paper format adpated for generating mp4 movie
${GMT_PATH}gmtset HEADER_FONT_SIZE 35
${GMT_PATH}gmtset ANNOT_FONT_SIZE_PRIMARY 22
${GMT_PATH}gmtset LABEL_FONT_SIZE 22
${GMT_PATH}gmtset D_FORMAT %.12g

############################################################################################################################################
#check the number of input parameter
############################################################################################################################################
if [ $# -ne 2 ] 
then
   echo "========================================================================================="
   echo "Wrong number of input parameter"
   echo "To generate movie of displacement ground motion : $0 dis \"Movie's title\""
   echo "To generate movie of velocity     ground motion : $0 vel \"Movie's title\""
   echo "To generate movie of acceleration ground motion : $0 acc \"Movie's title\""
   echo "========================================================================================="
   exit 1
fi

############################################################################################################################################
#get the prefix of the run and the time step
############################################################################################################################################
prefix=`cat prefix`
if [ "$prefix" = "" ]
then
   echo "========================================================================================="
   echo "Illegal prefix"
   echo "Check file prefix"
   echo "========================================================================================="
   exit 2
fi

############################################################################################################################################
#check if *.snapshot.*.grd files exist
############################################################################################################################################
check_snap=`ls *.snapshot.*.grd`
if [ "${check_snap}" = "" ]
then
   echo "========================================================================================="
   echo "Snapshot files not found"
   echo "========================================================================================="
   exit 3
fi

dt=`cat ${prefix}.cfg | grep -i "time step" | awk '{print $4}' | sed s/,/\\./g`

if [ "$dt" = "" ]
then
   dt=`cat ${prefix}.lst | grep -i "size     of time step" | awk '{print $7}' | sed s/,/\\./g`
fi

echo "Time step = " $dt "s"
title=$2

############################################################################################################################################
#topo gradient is needed
############################################################################################################################################
grd_topo_path=/home/demartin/Projets/MICROZONAGE3D_MARTIGNY/DATA_V1/
grd_topo_file=My_3D_Z2_25m_GMTbf.grd
grd_topo_gradient=My_3D_Z2_25m_gradient.grd

E_option="-A270"
${GMT_PATH}grdgradient ${grd_topo_path}${grd_topo_file}=bf -G${grd_topo_gradient} ${E_option} -Nt

############################################################################################################################################
#set snapshot file name
############################################################################################################################################
if [ $1 = "dis" ]
then
   uva="u"
   prefix_x=${prefix}".snapshot.ux."
   prefix_y=${prefix}".snapshot.uy."
   prefix_z=${prefix}".snapshot.uz."
   dis_vel_acc="displacement"
   my_unit="displacement (m)"
elif [ $1 = "vel" ]
then
   uva="v"
   prefix_x=${prefix}".snapshot.vx."
   prefix_y=${prefix}".snapshot.vy."
   prefix_z=${prefix}".snapshot.vz."
   dis_vel_acc="velocity"
   my_unit="velocity (m/s)"
elif [ $1 = "acc" ]
then
   uva="a"
   prefix_x=${prefix}".snapshot.ax."
   prefix_y=${prefix}".snapshot.ay."
   prefix_z=${prefix}".snapshot.az."
   dis_vel_acc="acceleration"
   my_unit="acceleration (m/s@+2@+)"
fi

############################################################################################################################################
#Get min and max of all *.grd files
############################################################################################################################################
echo "Searching for min/max inside *.grd files"
for GRD_FILE in `ls ${prefix_x}*.grd ${prefix_y}*.grd ${prefix_z}*.grd`
do

###Shift EFISPEC result to original geographic coordinates of the MNT file
   ${GMT_PATH}grdedit ${GRD_FILE}=bf `${GMT_PATH}grdinfo ${grd_topo_path}${grd_topo_file}=bf -I-`

   ${GMT_PATH}grdinfo ${GRD_FILE}=bf -C | awk '{ print $6,$7 }' >> xyz_minmax.tmp 
   ${GMT_PATH}grdinfo ${GRD_FILE}=bf -C | awk '{ print $2,$3,$4,$5 }' >> domain_xy_minmax.tmp 

done

#min max of displacement or velocity or acceleration in 3 directions
xyz_min=`${GMT_PATH}minmax xyz_minmax.tmp -C | awk '{ print $1 }'`
xyz_max=`${GMT_PATH}minmax xyz_minmax.tmp -C | awk '{ print $4 }'`

#min max of the domain in x and - coordinates
domain_x_min=`${GMT_PATH}minmax domain_xy_minmax.tmp -C | awk '{ print $1 }'`
domain_x_max=`${GMT_PATH}minmax domain_xy_minmax.tmp -C | awk '{ print $4 }'`
domain_y_min=`${GMT_PATH}minmax domain_xy_minmax.tmp -C | awk '{ print $5 }'`
domain_y_max=`${GMT_PATH}minmax domain_xy_minmax.tmp -C | awk '{ print $8 }'`
rm -f xyz_minmax.tmp domain_xy_minmax.tmp
echo "Size of the domain"
echo " --> xmin = " $domain_x_min
echo " --> xmax = " $domain_x_max
echo " --> ymin = " $domain_y_min
echo " --> ymax = " $domain_y_max

############################################################################################################################################
#compute GMT variables
############################################################################################################################################

abs_xyz_min=`echo ${xyz_min} | awk ' { if($1>=0) {print $1} else {print $1*-1} }'`
abs_xyz_max=`echo ${xyz_max} | awk ' { if($1>=0) {print $1} else {print $1*-1} }'`

xyz_max=`echo "${abs_xyz_min} ${abs_xyz_max}" | awk '{if ($1 > $2) {print +1*$1} else {print +1*$2}}'`
xyz_min=`echo "${abs_xyz_min} ${abs_xyz_max}" | awk '{if ($2 > $1) {print -1*$2} else {print -1*$1}}'`

nz_inc=100
dz_inc=`echo "${xyz_max} ${xyz_min} ${nz_inc}" | awk '{print ($1 - $2)/$3}'`

nz_scale=4
dz_scale=`echo "${xyz_max} ${xyz_min} ${nz_scale}" | awk '{print ($1 - $2)/$3}' | xargs printf "%6.2e" | sed s/,/\\./g`

${GMT_PATH}makecpt -Cjet -T${xyz_min}/${xyz_max}/${dz_inc} -D -Z > color.cpt

res=150

size_graph_x=`echo "${W_page}" | awk '{print $1/3.7}'`
size_graph_y=`echo "${size_graph_x} ${domain_x_min} ${domain_x_max} ${domain_y_min} ${domain_y_max}" | awk '{print $1*($5-$4)/($3-$2)}'`

x_psscale=`echo  "${size_graph_x}" | awk '{print +1*$1/2}' `
y_psscale=`echo  "${size_graph_y}" | awk '{print -1*$1/4}' `
size_scale=`echo "${size_graph_x}" | awk '{print $1*0.9}'`

my_JX_page="-JX${W_page}p/${H_page}p"
my_R_page="-R0/${W_page}/0/${H_page}"
my_JX_graph="-JX${size_graph_x}p/${size_graph_y}p"

x_title=`echo "${W_page}" | awk '{print $1/2.00}'`
y_title=`echo "${H_page}" | awk '{print $1*0.95}'`

x_code=`echo "${W_page}" | awk '{print $1*0.08}'`
y_code=`echo "${H_page}" | awk '{print $1*0.99}'`
code_name="EFISPEC3D"

x_time=`echo "${W_page}" | awk '{print $1/2.00}'`
y_time=`echo "${H_page}" | awk '{print $1*0.87}'`

x_graph1=`echo "$size_graph_x" | awk '{print $1*0.225}'`
x_graph_shift=`echo "$size_graph_x" | awk '{print $1*1.12}'`

ax_stride=`echo "${domain_x_max} ${domain_x_min}" | awk '{print ($1 - $2)/4}'`
ay_stride=`echo "${domain_y_max} ${domain_y_min}" | awk '{print ($1 - $2)/4}'`

############################################################################################################################################
#Plot time series *.grd files
############################################################################################################################################
frame_movie=0
for grd_file in `ls ${prefix_x}*.grd`
do

   frame=`echo ${grd_file} | awk -F "." '{print $4}'`
   frame_movie=`echo "${frame_movie} + 1" | bc -l | xargs printf "%06d"`
   my_time=`echo "${dt} ${frame}" | awk '{print $1 * ($2-1)}' | xargs printf "%10.5f" | sed s/,/\\./g`
   echo " "
   echo "Time = " ${my_time}

   ps_file=${prefix}".snapshot."${uva}"xyz."${frame}".ps"
   jpg_file=${prefix}".snapshot."${uva}"xyz."${frame_movie}".jpg"

   grdx_file=${prefix}".snapshot."${uva}"x."${frame}".grd"
   grdy_file=${prefix}".snapshot."${uva}"y."${frame}".grd"
   grdz_file=${prefix}".snapshot."${uva}"z."${frame}".grd"

   my_R_graph=`${GMT_PATH}grdinfo ${grdx_file}=bf -I-`

   echo "Plotting" ${grdx_file} ${grdy_file} ${grdz_file} "into" ${ps_file}

#title
${GMT_PATH}pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya    -K <<EOF  > $ps_file
$x_title $y_title 50 0 1 CM $title
EOF
#code name
${GMT_PATH}pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya -O -K <<EOF >> $ps_file
$x_code $y_code 25 0 1 CM Computed by $code_name
EOF
#time
${GMT_PATH}pstext ${my_JX_page} ${my_R_page} -N -W255/255/255 -Xa -Ya -O -K <<EOF >> $ps_file
$x_time $y_time 25 0 1 CM Time = ${my_time} s
EOF

   ${GMT_PATH}grdimage ${grdx_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."EW-${dis_vel_acc}":WeSn -E${res} -X${x_graph1}p -Yc -I${grd_topo_gradient}  -O -K >> $ps_file
   ${GMT_PATH}gmtset D_FORMAT %6.2e
   ${GMT_PATH}psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}:"${my_unit}":/:" ": -S                                                                                -O -K >> $ps_file
   ${GMT_PATH}gmtset D_FORMAT %.12g
 
   ${GMT_PATH}grdimage ${grdy_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."NS-${dis_vel_acc}":weSn -E${res} -X${x_graph_shift}p -I${grd_topo_gradient} -O -K >> $ps_file
   ${GMT_PATH}gmtset D_FORMAT %6.2e
   ${GMT_PATH}psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}:"${my_unit}":/:" ": -S                                                                                -O -K >> $ps_file
   ${GMT_PATH}gmtset D_FORMAT %.12g
                                                                                                                                                        
   ${GMT_PATH}grdimage ${grdz_file}=bf ${my_JX_graph} ${my_R_graph} -Ccolor.cpt -B${ax_stride}:"EW-distance (m)":/${ay_stride}:"NS-distance (m)"::."UD-${dis_vel_acc}":wESn -E${res} -X${x_graph_shift}p -I${grd_topo_gradient} -O -K >> $ps_file
   ${GMT_PATH}gmtset D_FORMAT %6.2e
   ${GMT_PATH}psscale -Ccolor.cpt -D${x_psscale}p/${y_psscale}p/${size_scale}p/20ph -Ba${dz_scale}:"${my_unit}":/:" ": -S                                                                                -O    >> $ps_file
   ${GMT_PATH}gmtset D_FORMAT %.12g

   echo "Converting ps file to jpg"  
   convert $ps_file -rotate 90 -flatten -quality 100 -density ${res} $jpg_file
   rm $ps_file

done

############################################################################################################################################
#Create movie from jpg files
############################################################################################################################################
rm -f ${prefix}".movie."${uva}"xyz.avi"
avconv -f image2 -r 10 -i "${prefix}.snapshot.${uva}xyz."%06d".jpg" -b 9600000 ${prefix}".movie."${uva}"xyz.avi"
