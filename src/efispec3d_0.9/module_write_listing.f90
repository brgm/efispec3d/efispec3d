!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to write information in the listing file *.lst.

!>@brief
!!This module contains subroutines to write information in the listing file *.lst.
module mod_write_listing

   use mod_precision

   implicit none

   private

   public  :: write_header 
   public  :: write_temporal_domain_info
   private :: write_receiver_saving_info
   private :: write_snapshot_saving_info
   public  :: write_filter_info
   public  :: write_cfl_condition_ok
   public  :: write_cfl_condition_ko
   public  :: write_lst_fault_info

   contains

!
!
!>@brief subroutine that writes the header of the listing file (*.lst).
!***********************************************************************************************************************************************************************************
   subroutine write_header()
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables

      implicit none


      integer(kind=IXP)                                         :: ios
      integer(kind=IXP)                                         :: icpu
      integer(kind=IXP), dimension(8_IXP)                       :: values
      integer(kind=IXP), dimension(ig_ncpu)                     :: cpu_rank_world
      integer(kind=IXP), dimension(ig_ncpu)                     :: cpu_rank_simu 
                                                                
      character(len=5)                                          :: zone
      character(len=255)                                        :: info
      character(len=MPI_MAX_PROCESSOR_NAME), dimension(ig_ncpu) :: cpu_name

!
!---->cpu0 gathers all the name of the other cpu
      cpu_name(:) = " "

      call mpi_gather(cg_cpu_name,MPI_MAX_PROCESSOR_NAME,mpi_character,cpu_name,MPI_MAX_PROCESSOR_NAME,mpi_character,ZERO_IXP,ig_mpi_comm_simu,ios)

      if (ios /= ZERO_IXP) then

         write(info,'(a)') "error while gathering cpus' name"
         call error_stop(info)

      endif

!
!---->cpu0 gathers the "MPI_COMM_WORLD rank" of all cpus in mpi_comm_simu communicator
      call mpi_gather(ig_myrank_world,ONE_IXP,MPI_INTEGER,cpu_rank_world,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!---->cpu0 gathers the "mpi_comm_simu rank" of all cpus in mpi_comm_simu communicator
      call mpi_gather(ig_myrank,ONE_IXP,MPI_INTEGER,cpu_rank_simu,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!---->cpu0 writes the header in the listing file
      if (ig_myrank == ZERO_IXP) then

         open(unit=IG_LST_UNIT,file=trim(cg_prefix)//".lst") !IG_LST_UNIT = 10

         write(IG_LST_UNIT,'(a)') "**************************************************************************************************"
         write(IG_LST_UNIT,'(a)') "***                                                                                            ***"
         write(IG_LST_UNIT,'(a)') "***                                          EFISPEC3D                                         ***"
         write(IG_LST_UNIT,'(a)') "***                               (Elements FInis SPECtraux 3D)                                ***"
         write(IG_LST_UNIT,'(a)') "***                                                                                            ***"
         write(IG_LST_UNIT,'(a)') "***                                         version 0.9                                        ***"
         write(IG_LST_UNIT,'(a)') "***                                                                                            ***"
         write(IG_LST_UNIT,'(a)') "***                                     http://efispec.free.fr                                 ***"
         write(IG_LST_UNIT,'(a)') "***                                                                                            ***"
         write(IG_LST_UNIT,'(a)') "***                       Developpers                                                          ***"
         write(IG_LST_UNIT,'(a)') "***                                    --> Florent DE MARTIN                                   ***"
         write(IG_LST_UNIT,'(a)') "***                                    --> David MICHEA                                        ***"
         write(IG_LST_UNIT,'(a)') "***                                    --> Philippe THIERRY                                    ***"
         write(IG_LST_UNIT,'(a)') "***                                                                                            ***"
         write(IG_LST_UNIT,'(a)') "***                    Copyright 2009-2019 BRGM (French Geological Survey)                     ***"
         write(IG_LST_UNIT,'(a)') "***                                                                                            ***"
         write(IG_LST_UNIT,'(a)') "**************************************************************************************************"

!
!------->date and time of the simulation
         call date_and_time(VALUES=values,ZONE=zone)
         write(IG_LST_UNIT,'("",/,a)') "date and time of the simulation"
         write(IG_LST_UNIT,'(" --> date : ",I4.4,"-",I2.2,"-",I2.2                         )') values(1),values(2),values(3)
         write(IG_LST_UNIT,'(" --> time : ",I2.2,"H",I2.2,"M",I2.2,"S ",a," (HHMM) wrt UTC")') values(5),values(6),values(7),zone

!
!------->simulation mode: single or multiple
         if (ig_nsimu == ONE_IXP) then
            write(IG_LST_UNIT,'("",/,a)') "single simulation mode"
         else
            write(IG_LST_UNIT,'("",/,a)') "multiple simulation mode"
            write(IG_LST_UNIT,'(a,i0)') " --> total number of simulation           = ",ig_nsimu
            write(IG_LST_UNIT,'(a,i0)') " --> current simulation number            = ",ig_simu
            write(IG_LST_UNIT,'(a,i0)') " --> uq      simulation number            = ",ig_uq_isimu
            write(IG_LST_UNIT,'(a,i0)') " --> total number of uncertain parameters = ",ig_uq_ndim
         endif


!
!------->cpu name used for computation
         write(IG_LST_UNIT,'("",/,a)') "name of the cpus used for computation - world_rank - simu_rank"
         do icpu = ONE_IXP,ig_ncpu
            write(IG_LST_UNIT,'(2a,2(a,i6))')   " --> ",trim(adjustl(cpu_name(icpu)))," - ",cpu_rank_world(icpu)," - ",cpu_rank_simu(icpu)
         enddo

!
!------->epsilon of the machine
         write(IG_LST_UNIT,'("",/,a)')   "epsilon machine"
         write(IG_LST_UNIT,'(a,es15.7)') " -->",EPSILON_MACHINE_RXP

!
!------->smallest positive (non zero) number in the model of the type real
         write(IG_LST_UNIT,'("",/,a)')   "smallest positive (non zero) number of the type real"
         write(IG_LST_UNIT,'(a,es15.7)') " -->",TINY_REAL_RXP

!
!------->size in byte of MPI_REAL type
         write(unit=IG_LST_UNIT,fmt='("",/,a)') "size of MPI_REAL (in bytes)"
         write(unit=IG_LST_UNIT,fmt='(a,I2)')   " --> ",ig_mpi_nboctet_real

!
!------->size in byte of mpi_integer type
         write(unit=IG_LST_UNIT,fmt='("",/,a)') "size of MPI_INTEGER (in bytes)"
         write(unit=IG_LST_UNIT,fmt='(a,I2)')   " --> ",ig_mpi_nboctet_int

!
!------->communication type
         if (LG_ASYNC_MPI_COMM) then
            write(IG_LST_UNIT,'("",/,a)') "communication between cpus"
            write(IG_LST_UNIT,'(a)')      " --> non-blocking"
         else
            write(IG_LST_UNIT,'("",/,a)') "communication between cpus"
            write(IG_LST_UNIT,'(a)')      " --> blocking"
         endif

!
!------->spectral element's polynomial order
         write(IG_LST_UNIT,'("",/,a,I0  )') "spectral elements use Lagrange polynomial of order ",IG_LAGRANGE_ORDER
         write(IG_LST_UNIT,'(     a,I0,a)') " --> ",IG_NGLL**IG_NDOF," GLL nodes per hexa"

!
!---------->simulation rheology
         if (.not.LG_VISCO) then
            write(IG_LST_UNIT,'(" ",/,a)') "rheology of simulation"
            write(IG_LST_UNIT,'(a)')       " --> elastic"
         else
            write(IG_LST_UNIT,'(" ",/,a)') "rheology of simulation"
            write(IG_LST_UNIT,'( a,I0,a)') " --> viscoelastic using ",IG_NRELAX," memory variables"
         endif

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_header
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine that writes temporal information about the simulation in the listing file (*.lst).
!***********************************************************************************************************************************************************************************
   subroutine write_temporal_domain_info()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      cg_prefix&
                                     ,ig_myrank&
                                     ,TINY_REAL_RXP&
                                     ,IG_LST_UNIT&
                                     ,rg_dt&
                                     ,rg_simu_total_time&
                                     ,ig_ndt

      implicit none

      if (ig_myrank == ZERO_IXP) then

         if (rg_dt > TINY_REAL_RXP) then

            write(IG_LST_UNIT,'("",/,a)')  "time domain information found in the file "//trim(cg_prefix)//".cfg"
            write(IG_LST_UNIT,'(a,f10.6)') " --> duration of simulation = ",rg_simu_total_time
            write(IG_LST_UNIT,'(a,i10)')   " --> number   of time step  = ",ig_ndt
            write(IG_LST_UNIT,'(a,es15.6)')" --> size     of time step  =" ,rg_dt
            write(IG_LST_UNIT,'(a)')       " --> the stability of the simulation will be checked with the Courant-Friedrichs-Lewy condition (see below)."

            call write_receiver_saving_info()
 
            call write_snapshot_saving_info() 

         else

            write(IG_LST_UNIT,'("",/,a)') "size of time step not found in the file "//trim(cg_prefix)//".cfg or inferior to TINY_REAL_RXP"
            write(IG_LST_UNIT,'(a)')      " --> the optimal size will be set by EFISPEC3D with respect to the Courant-Friedrichs-Lewy condition"

         endif

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_temporal_domain_info
!***********************************************************************************************************************************************************************************

!
!
!>@brief subroutine that writes receivers information in the listing file (*.lst).
!***********************************************************************************************************************************************************************************
   subroutine write_receiver_saving_info()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_LST_UNIT&
                                     ,ig_myrank&
                                     ,ig_ndt&     
                                     ,ig_receiver_saving_incr

      implicit none

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(a)') " "

         if (ig_receiver_saving_incr < ig_ndt) then

            write(IG_LST_UNIT,'(a,i0,a)' ) "receivers' time history saved every ",ig_receiver_saving_incr," time steps"

         else

            write(IG_LST_UNIT,'(a)' )      "receivers' time history are not saved"

         endif

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_receiver_saving_info
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine that writes snapshots information in the listing file (*.lst).
!***********************************************************************************************************************************************************************************
   subroutine write_snapshot_saving_info()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_LST_UNIT&
                                     ,ig_myrank&
                                     ,ig_ndt&
                                     ,ig_snapshot_saving_incr&
                                     ,lg_snapshot_displacement&
                                     ,lg_snapshot_velocity&
                                     ,lg_snapshot_acceleration&
                                     ,ig_snapshot_volume_saving_incr&
                                     ,lg_snapshot_volume_displacement&
                                     ,lg_snapshot_volume_velocity&
                                     ,lg_snapshot_volume_acceleration

      implicit none

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(a)')                            " "

         if (ig_snapshot_saving_incr < ig_ndt) then
                              write(IG_LST_UNIT,'(a,i0,a)' ) "surface snapshot saved every        ",ig_snapshot_saving_incr," time steps"
         else
                              write(IG_LST_UNIT,'(a)' )      "surface snapshot are not saved"
         endif
         if (lg_snapshot_displacement) then
                              write(IG_LST_UNIT,'(a)')       " --> displacement snapshot are     saved"
         else
                              write(IG_LST_UNIT,'(a)')       " --> displacement snapshot are not saved"
         endif
         if (lg_snapshot_velocity) then
                              write(IG_LST_UNIT,'(a)')       " --> velocity     snapshot are     saved"
         else
                              write(IG_LST_UNIT,'(a)')       " --> velocity     snapshot are not saved"
         endif
         if (lg_snapshot_acceleration) then
                              write(IG_LST_UNIT,'(a)')       " --> acceleration snapshot are     saved"
         else
                              write(IG_LST_UNIT,'(a)')       " --> acceleration snapshot are not saved"
         endif

         write(IG_LST_UNIT,'(a)')                            " "

         if (ig_snapshot_volume_saving_incr < ig_ndt) then
                              write(IG_LST_UNIT,'(a,i0,a)' ) "volume  snapshot saved every        ",ig_snapshot_volume_saving_incr," time steps"
         else
                              write(IG_LST_UNIT,'(a)' )      "volume  snapshot are not saved"
         endif
         if (lg_snapshot_volume_displacement) then
                              write(IG_LST_UNIT,'(a)')       " --> displacement snapshot are     saved"
         else
                              write(IG_LST_UNIT,'(a)')       " --> displacement snapshot are not saved"
         endif
         if (lg_snapshot_volume_velocity) then
                              write(IG_LST_UNIT,'(a)')       " --> velocity     snapshot are     saved"
         else
                              write(IG_LST_UNIT,'(a)')       " --> velocity     snapshot are not saved"
         endif
         if (lg_snapshot_volume_acceleration) then
                              write(IG_LST_UNIT,'(a)')       " --> acceleration snapshot are     saved"
         else
                              write(IG_LST_UNIT,'(a)')       " --> acceleration snapshot are not saved"
         endif

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_snapshot_saving_info
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine that writes filter information in the listing file (*.lst).
!***********************************************************************************************************************************************************************************
   subroutine write_filter_info(iir_or_fir,ideal_cutoff,transition_band,attenuation,order,dec_factor,dec_dt,nsave)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_LST_UNIT&
                                     ,ig_myrank

      implicit none

      character(len=3), intent(in) :: iir_or_fir
      real(kind=RXP)           , intent(in) :: ideal_cutoff
      real(kind=RXP)           , intent(in) :: transition_band
      real(kind=RXP)           , intent(in) :: attenuation
      integer(kind=IXP)        , intent(in) :: order
      integer(kind=IXP)        , intent(in) :: dec_factor
      real(kind=RXP)           , intent(in) :: dec_dt
      integer(kind=IXP)        , intent(in) :: nsave

      if (ig_myrank == ZERO_IXP) then

         if (iir_or_fir == "fir") then

            write(IG_LST_UNIT,'(/,a)'      ) "low-pass FIR filter information"

         else

            write(IG_LST_UNIT,'(/,a)'      ) "low-pass IIR filter information"

         endif

         write(IG_LST_UNIT,'(a,F15.7,a)') " --> ideal cutoff frequency    = ",ideal_cutoff," Hz"
         write(IG_LST_UNIT,'(a,F15.7,a)') " --> transition band           = ",transition_band," Hz"
         write(IG_LST_UNIT,'(a,F15.7,a)') " --> attenuation               = ",attenuation," dB"
         write(IG_LST_UNIT,'(a,F15.7,a)') " --> passband cutoff frequency = ",ideal_cutoff - transition_band/TWO_RXP," Hz"
         write(IG_LST_UNIT,'(a,F15.7,a)') " --> stopband cutoff frequency = ",ideal_cutoff + transition_band/TWO_RXP," Hz"
         write(IG_LST_UNIT,'(a,I7     )') " --> filter order              = ",order
         write(IG_LST_UNIT,'(a,I7     )') " --> decimation factor         = ",dec_factor
         write(IG_LST_UNIT,'(a,es18.6 )') " --> time step after decimation= ",dec_dt
         write(IG_LST_UNIT,'(a,I7     )') " --> number time step after dec= ",nsave

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_filter_info
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine that writes valid CFL condition in the listing file (*.lst).
!***********************************************************************************************************************************************************************************
   subroutine write_cfl_condition_ok(dt)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_LST_UNIT&
                                     ,ig_myrank&
                                     ,rg_simu_total_time&
                                     ,ig_ndt

      implicit none

      real(kind=RXP), intent(in) :: dt

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'("",/,a         )') "Courant–Friedrichs–Lewy (CFL) condition has been checked"
         write(IG_LST_UNIT,'(     a,es14.6,a)') " --> simulation's time step size equal to",dt," honors the CFL condition."
         write(IG_LST_UNIT,'(a,f10.6)')         " --> duration of simulation = ",rg_simu_total_time
         write(IG_LST_UNIT,'(a,i10)')           " --> number   of time step  = ",ig_ndt
         write(IG_LST_UNIT,'(a,es15.6)')        " --> size     of time step  =" ,dt

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_cfl_condition_ok
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine that writes invalid CFL condition in the listing file (*.lst).
!***********************************************************************************************************************************************************************************
   subroutine write_cfl_condition_ko(dt)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_LST_UNIT&
                                     ,ig_myrank&
                                     ,rg_simu_total_time&
                                     ,ig_ndt

      implicit none

      real(kind=RXP), intent(in) :: dt

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'("",/,a         )') "Courant–Friedrichs–Lewy (CFL) condition has been checked"
         write(IG_LST_UNIT,'(     a,es15.7,a)') " --> simulation's time step size has been modified to",dt
         write(IG_LST_UNIT,'(a,f11.7)')         " --> duration of simulation = ",rg_simu_total_time
         write(IG_LST_UNIT,'(a,i11)')           " --> number   of time step  = ",ig_ndt
         write(IG_LST_UNIT,'(a,es15.6)')        " --> size     of time step  =" ,dt
   
         call write_receiver_saving_info()

         call write_snapshot_saving_info() 

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_cfl_condition_ko
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine that writes information about the faults in the listing file (*.lst).
!***********************************************************************************************************************************************************************************
   subroutine write_lst_fault_info(tl_fault)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_LST_UNIT&
                                     ,type_fault&
                                     ,info_all_cpu&
                                     ,ig_myrank

      implicit none

      type(type_fault), dimension(:), intent(in) :: tl_fault

      integer(kind=IXP)                          :: ndcs
      integer(kind=IXP)                          :: nfault
      integer(kind=IXP)                          :: ifault

      character(len=  6)                         :: cfault
      character(len=255)                         :: info

      nfault = size(tl_fault)

      if (ig_myrank == ZERO_IXP) then

         do ifault = ONE_IXP,nfault

            write(IG_LST_UNIT,'(/,a,i0   )') "fault ",ifault
            write(IG_LST_UNIT,'(a, f5.1  )') " --> moment magnitude                 = ",tl_fault(ifault)%mw
            write(IG_LST_UNIT,'(a, f6.2  )') " --> velocity rupture ratio           = ",tl_fault(ifault)%vrr
            write(IG_LST_UNIT,'(a, f5.1,a)') " --> strike                           = ",tl_fault(ifault)%str," deg."
            write(IG_LST_UNIT,'(a, f5.1,a)') " --> dip                              = ",tl_fault(ifault)%dip," deg."
            write(IG_LST_UNIT,'(a, f5.1,a)') " --> rake                             = ",tl_fault(ifault)%rak," deg."
            write(IG_LST_UNIT,'(a,e15.7,a)') " --> strike length                    = ",tl_fault(ifault)%ls," m"
            write(IG_LST_UNIT,'(a,e15.7,a)') " --> dip    length                    = ",tl_fault(ifault)%ld," m"
            write(IG_LST_UNIT,'(a,e15.7,a)') " --> center x-coordinate              = ",tl_fault(ifault)%xc," m"
            write(IG_LST_UNIT,'(a,e15.7,a)') " --> center y-coordinate              = ",tl_fault(ifault)%yc," m"
            write(IG_LST_UNIT,'(a,e15.7,a)') " --> center z-coordinate              = ",tl_fault(ifault)%zc," m"
            write(IG_LST_UNIT,'(a,e15.7,a)') " --> hypocenter s-coordinate          = ",tl_fault(ifault)%sh," m"
            write(IG_LST_UNIT,'(a,e15.7,a)') " --> hypocenter d-coordinate          = ",tl_fault(ifault)%dh," m"
            write(IG_LST_UNIT,'(a,2x,a   )') " --> autocorrelation function         = ",tl_fault(ifault)%acf
            write(IG_LST_UNIT,'(a,e15.7,a)') " --> correlation length along strike  = ",tl_fault(ifault)%lcs," m"
            write(IG_LST_UNIT,'(a,e15.7,a)') " --> correlation length along dip     = ",tl_fault(ifault)%lcd," m"
                                                                                  
            if (tl_fault(ifault)%acf == "vk") then                                
                                                                                  
               write(IG_LST_UNIT,'(a,f6.2)') " --> kappa (Hurst number)             = ",tl_fault(ifault)%k
                                                                                  
            endif                                                                 
                                                                                  
            write(IG_LST_UNIT,'(a, i3    )') " --> source time function             = ",tl_fault(ifault)%icur
            write(IG_LST_UNIT,'(a, f6.2,a)') " --> rise time                        = ",tl_fault(ifault)%rt," s"
            write(IG_LST_UNIT,'(a, f6.2,a)') " --> time shift                       = ",tl_fault(ifault)%ts," s"
            write(IG_LST_UNIT,'(a, i0    )') " --> number of sub-fault along strike = ",tl_fault(ifault)%ns
            write(IG_LST_UNIT,'(a, i0    )') " --> number of sub-fault along dip    = ",tl_fault(ifault)%nd
            write(IG_LST_UNIT,'(a, i0    )') " --> total number of sub-faults       = ",tl_fault(ifault)%ns*tl_fault(ifault)%nd
            write(IG_LST_UNIT,'(a, f6.2,a)') " --> discrete step size  along strike = ",tl_fault(ifault)%ds," m"
            write(IG_LST_UNIT,'(a, f6.2,a)') " --> discrete step size  along dip    = ",tl_fault(ifault)%dd," m"
            write(IG_LST_UNIT,'(a        )') "sanity check after fault discretization"
            write(IG_LST_UNIT,'(a, f5.1  )') " --> moment magnitude                 = ",(log10(tl_fault(ifault)%m0) - 9.1_R64)/1.5_R64
            write(IG_LST_UNIT,'(a,e15.7,a)') " --> seismic moment                   = ",tl_fault(ifault)%m0," N.m"

         enddo

      endif

      do ifault = ONE_IXP,nfault

         ndcs = tl_fault(ifault)%ndcsource

         write(cfault,'(i0)') ifault

         write(info,'(a)') "double couple point sources for fault "//trim(adjustl(cfault))

         call info_all_cpu(ndcs,info)

      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_lst_fault_info
!***********************************************************************************************************************************************************************************


end module mod_write_listing
