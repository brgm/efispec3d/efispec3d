!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to read mesh and create GLL nodes global numbering.

!>@brief
!!This module contains subroutines to read mesh files and creates GLL nodes global numbering in cpu myrank.
module mod_init_mesh

   use mod_precision
   
   implicit none

   private

!>size of buffer array to read file *.dat
   integer(kind=IXP), parameter :: BUFFER_READ_SIZE = 8192                                                     
!>buffer array to read file *.dat
   integer(kind=IXP), dimension(BUFFER_READ_SIZE) :: buffer 

   public  :: init_mesh
   private :: init_gll_number
   private :: propagate_gll_nodes_quad
   private :: propagate_gll_nodes_face
   private :: propagate_gll_nodes_edge
   private :: propagate_gll_nodes_corner
   public  :: init_element
   private :: read_bin_int
   
   contains

!
!
!>@brief
!>This subroutine reads mesh files *.dat for cpu myrank and creates GLL numbering of hexahedron and quadrangle elements for cpu myrank
!***********************************************************************************************************************************************************************************
    subroutine init_mesh()
!***********************************************************************************************************************************************************************************
 
      use mpi
 
      use mod_global_variables, only : &
                                       ig_nhexa&
                                      ,ig_nhexa_outer&
                                      ,ig_nhexa_inner&
                                      ,ig_nquad_parax&
                                      ,ig_nquad_fsurf&
                                      ,ig_mesh_nnode&
                                      ,ig_hexa_nnode&
                                      ,ig_quad_nnode&
                                      ,ig_hexa_gnode_glonum&
                                      ,ig_quadp_gnode_glonum&
                                      ,ig_quadf_gnode_glonum&
                                      ,rg_gnode_x&
                                      ,rg_gnode_y&
                                      ,rg_gnode_z&
                                      ,rg_mesh_xmax&
                                      ,rg_mesh_ymax&
                                      ,rg_mesh_zmax&
                                      ,rg_mesh_xmin&
                                      ,rg_mesh_ymin&
                                      ,rg_mesh_zmin&
                                      ,rg_gll_displacement&
                                      ,rg_gll_velocity&
                                      ,rg_gll_acceleration&
                                      ,rg_gll_acctmp&
                                      ,cg_prefix&
                                      ,cg_myrank&
                                      ,cg_ncpu&
                                      ,ig_myrank&
                                      ,ig_ncpu_neighbor&
                                      ,tg_cpu_neighbor&
                                      ,ig_hexa_gll_glonum&
                                      ,ig_quadp_gll_glonum&
                                      ,ig_quadf_gll_glonum&
                                      ,ig_hexa_material_number&
                                      ,ig_quadp_neighbor_hexa&
                                      ,ig_quadp_neighbor_hexaface&
                                      ,IG_NGLL&
                                      ,ig_ngll_total&
                                      ,get_newunit&
                                      ,error_stop&
                                      ,ig_cpu_neighbor_info&
                                      ,ig_nneighbor_all_kind&
                                      ,lg_boundary_absorption&
                                      ,IG_LST_UNIT&
                                      ,IG_NDOF&
                                      ,info_all_cpu&
                                      ,LG_OUTPUT_DEBUG_FILE

      use mod_init_memory

      implicit none

      integer(kind=IXP) ,parameter :: NFACE=6
      integer(kind=IXP) ,parameter :: NEDGE=12
      integer(kind=IXP) ,parameter :: NNODE=8

      real(kind=RXP)               :: rl_max_coord_x
      real(kind=RXP)               :: rl_min_coord_x
      real(kind=RXP)               :: rl_max_coord_y
      real(kind=RXP)               :: rl_min_coord_y
      real(kind=RXP)               :: rl_max_coord_z
      real(kind=RXP)               :: rl_min_coord_z

      integer(kind=IXP)            :: ios
      integer(kind=IXP)            :: size_real_t
      integer(kind=IXP)            :: icpu
      integer(kind=IXP)            :: ihexa
      integer(kind=IXP)            :: iface
      integer(kind=IXP)            :: iedge
      integer(kind=IXP)            :: inode
      integer(kind=IXP)            :: ielt_type
      integer(kind=IXP)            :: ielt_number
      integer(kind=IXP)            :: ielt_face
      integer(kind=IXP)            :: ielt_edge
      integer(kind=IXP)            :: ielt_corner
      integer(kind=IXP)            :: ielt_coty
      integer(kind=IXP)            :: ielt_cpu
      integer(kind=IXP)            :: myunit_debug
      integer(kind=IXP)            :: myunit_dat
      integer(kind=IXP)            :: ifsurf
      integer(kind=IXP)            :: iparax
      character(len=255_IXP) :: fname
      character(len=255_IXP) :: info 
      
      integer(kind=IXP)            :: irec
      integer(kind=IXP)            :: igll
      integer(kind=IXP)            :: jgll
      integer(kind=IXP)            :: kgll
      integer(kind=IXP)            :: ineigh
      integer(kind=IXP)            :: nbneigh


      !
      !
      !********************************************************************************************************************************
      !open file that contains mesh information (outer and inner hexa, adjacency, etc.)
      !********************************************************************************************************************************
      fname = trim(cg_prefix)//"."//trim(cg_ncpu)//".elements.cpu."//trim(cg_myrank)//".dat"

      open(unit       = get_newunit(myunit_dat)&
          ,file       = trim(fname)&
          ,status     = 'old'&
          ,action     = 'read'&
          ,access     = 'sequential'&
          ,form       = 'unformatted'&
          ,recordtype = 'stream'&
          ,iostat     = ios)

      if (ios /= ZERO_IXP) then

         write(info,'(a)') "Error in subroutine init_mesh while opening hexa file"
         call error_stop(info)

      endif


      !
      !
      !********************************************************************************************************************************
      !read geometric nodes coordinates (right handed coordinate system: x-->est, y-->north, z-->upward)
      !********************************************************************************************************************************
      read(unit=myunit_dat) size_real_t,ig_mesh_nnode

      write(info,'(a)') "geometric nodes"
      call info_all_cpu(ig_mesh_nnode,info)

      ios = init_array_real(rg_gnode_x,ig_mesh_nnode,"rg_gnode_x")

      ios = init_array_real(rg_gnode_y,ig_mesh_nnode,"rg_gnode_y")

      ios = init_array_real(rg_gnode_z,ig_mesh_nnode,"rg_gnode_z")
      
      read(unit=myunit_dat) (rg_gnode_x(inode),rg_gnode_y(inode),rg_gnode_z(inode),inode=1,ig_mesh_nnode)


      !
      !
      !********************************************************************************************************************************
      !compute min/max (x,y,z) coordinates of the entire domain (i.e., for all cpu)
      !********************************************************************************************************************************
      rl_max_coord_x = maxval(rg_gnode_x(:))
      rl_max_coord_y = maxval(rg_gnode_y(:))
      rl_max_coord_z = maxval(rg_gnode_z(:))
      rl_min_coord_x = minval(rg_gnode_x(:))
      rl_min_coord_y = minval(rg_gnode_y(:))
      rl_min_coord_z = minval(rg_gnode_z(:))

      call mpi_allreduce(rl_max_coord_x,rg_mesh_xmax,1,MPI_REAL,MPI_MAX,MPI_COMM_WORLD,ios)
      call mpi_allreduce(rl_max_coord_y,rg_mesh_ymax,1,MPI_REAL,MPI_MAX,MPI_COMM_WORLD,ios)
      call mpi_allreduce(rl_max_coord_z,rg_mesh_zmax,1,MPI_REAL,MPI_MAX,MPI_COMM_WORLD,ios)
      call mpi_allreduce(rl_min_coord_x,rg_mesh_xmin,1,MPI_REAL,MPI_MIN,MPI_COMM_WORLD,ios)
      call mpi_allreduce(rl_min_coord_y,rg_mesh_ymin,1,MPI_REAL,MPI_MIN,MPI_COMM_WORLD,ios)
      call mpi_allreduce(rl_min_coord_z,rg_mesh_zmin,1,MPI_REAL,MPI_MIN,MPI_COMM_WORLD,ios)

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(" "   ,/,a      )') "Boundaries of the entire domain"
         write(IG_LST_UNIT,'(" -->",  a,E14.7)') " xmin = ",rg_mesh_xmin
         write(IG_LST_UNIT,'(" -->",  a,E14.7)') " xmax = ",rg_mesh_xmax
         write(IG_LST_UNIT,'(" -->",  a,E14.7)') " ymin = ",rg_mesh_ymin
         write(IG_LST_UNIT,'(" -->",  a,E14.7)') " ymax = ",rg_mesh_ymax
         write(IG_LST_UNIT,'(" -->",  a,E14.7)') " zmin = ",rg_mesh_zmin
         write(IG_LST_UNIT,'(" -->",  a,E14.7)') " zmax = ",rg_mesh_zmax

      endif


      !
      !
      !********************************************************************************************************************************
      !read information about cpu connected to cpu myrank
      !********************************************************************************************************************************
      irec  = ONE_IXP
      call read_bin_int(myunit_dat,irec,ig_ncpu_neighbor)
      write(info,'(a)') "connected cpu"
      call info_all_cpu(ig_ncpu_neighbor,info)
 
      allocate(tg_cpu_neighbor(ig_ncpu_neighbor),stat=ios)
      if (ios /= ZERO_IXP) then
         write(info,'(a)') "Error in subroutine init_mesh while allocating tg_cpu_neighbor"
         call error_stop(info)
      endif

      do icpu = ONE_IXP,ig_ncpu_neighbor
         irec = irec + ONE_IXP
         call read_bin_int(myunit_dat,irec,tg_cpu_neighbor(icpu)%icpu)
      enddo


      !
      !
      !********************************************************************************************************************************
      !read info about hexa and quad (number, number of geometric nodes per hexa, etc.)
      !********************************************************************************************************************************
      irec = irec + ONE_IXP
      call read_bin_int(myunit_dat,irec,ig_nhexa)
      write(info,'(a)') " hexahedra"
      call info_all_cpu(ig_nhexa,info)
 
      irec = irec + ONE_IXP
      call read_bin_int(myunit_dat,irec,ig_nhexa_outer)
      write(info,'(a)') " outer hexahedra"
      call info_all_cpu(ig_nhexa_outer,info)

      ig_nhexa_inner = ig_nhexa - ig_nhexa_outer
      write(info,'(a)') " inner hexahedra"
      call info_all_cpu(ig_nhexa_inner,info)
 
      irec = irec + ONE_IXP
      call read_bin_int(myunit_dat,irec,ig_nquad_parax)
      write(info,'(a)') "paraxial quadrangles"
      call info_all_cpu(ig_nquad_parax,info)
 
      irec = irec + ONE_IXP
      call read_bin_int(myunit_dat,irec,ig_nquad_fsurf)
      write(info,'(a)') "free surface quadrangles"
      call info_all_cpu(ig_nquad_fsurf,info)
 
      irec = irec + ONE_IXP
      call read_bin_int(myunit_dat,irec,ig_hexa_nnode)
 
      if (ig_hexa_nnode == 8_IXP) then

         ig_quad_nnode = 4_IXP

      elseif (ig_hexa_nnode == 27_IXP) then

         ig_quad_nnode = 9_IXP

      else

         write(info,'(a)') "Error in subroutine init_mesh: illegal number of local nodes for hexa"
         call error_stop(info)

      endif


      !
      !
      !********************************************************************************************************************************
      !initialize convention numbering of finite element
      !********************************************************************************************************************************
      call init_element(ig_hexa_nnode,ig_quad_nnode)


      !
      !
      !********************************************************************************************************************************
      !initialize some arrays
      !********************************************************************************************************************************
      ios = init_array_int(ig_cpu_neighbor_info,26_IXP*ig_nhexa_outer,3_IXP,"ig_cpu_neighbor_info") !DAVID: deallocation in subroutine create_mpi_buffer()

      ios = init_array_int(ig_hexa_gnode_glonum,ig_nhexa,ig_hexa_nnode,"ig_hexa_gnode_glonum")

      ios = init_array_int(ig_hexa_gll_glonum,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"ig_hexa_gll_glonum")

      if (ig_nquad_parax > 0_IXP .and. lg_boundary_absorption) then

         ios = init_array_int(ig_quadp_gnode_glonum,ig_nquad_parax,ig_quad_nnode,"ig_quadp_gnode_glonum")

         ios = init_array_int(ig_quadp_gll_glonum,ig_nquad_parax,IG_NGLL,IG_NGLL,"ig_quadp_gll_glonum")

         ios = init_array_int(ig_quadp_neighbor_hexa,ig_nquad_parax,"ig_quadp_neighbor_hexa")

         ios = init_array_int(ig_quadp_neighbor_hexaface,ig_nquad_parax,"ig_quadp_neighbor_hexaface")

      endif
 
      if (ig_nquad_fsurf > 0_IXP) then

         ios = init_array_int(ig_quadf_gnode_glonum,ig_nquad_fsurf,ig_quad_nnode,"ig_quadf_gnode_glonum")
         ios = init_array_int(ig_quadf_gll_glonum,ig_nquad_fsurf,IG_NGLL,IG_NGLL,"ig_quadf_gll_glonum")

      endif

      ios = init_array_int(ig_hexa_material_number,ig_nhexa,"ig_hexa_material_number")


      !
      !
      !********************************************************************************************************************************
      !initialize mesh and create GLL nodes numbering
      !********************************************************************************************************************************
      if (ig_myrank == 0_IXP) write(IG_LST_UNIT,'(" ",/,a)') "Generating global gll nodes numbering..."

      iparax = 0_IXP
      ifsurf = 0_IXP
      do ihexa = 1_IXP,ig_nhexa
         !
         !read material number of hexa 'ihexa'
         irec = irec + 1_IXP
         call read_bin_int(myunit_dat,irec,ig_hexa_material_number(ihexa))
         !
         !read geometric nodes of hexa 'ihexa'
         do inode = 1_IXP,ig_hexa_nnode
            irec = irec + 1_IXP
            call read_bin_int(myunit_dat,irec,ig_hexa_gnode_glonum(inode,ihexa))
         enddo
         !
         !generate gll node for current hexa
         call init_gll_number(ihexa,ig_ngll_total)
         !
         !loop on 6 faces to propagate gll node of hexa 'ihexa' to its neighbours
         do iface = 1_IXP,NFACE

            irec = irec + 1_IXP
            call read_bin_int(myunit_dat,irec,ielt_type) !either hexa or quad_parax or quad_fsurf

            select case (ielt_type)
               case(1_IXP) !another hexa is connected

                  irec = irec + 1_IXP
                  call read_bin_int(myunit_dat,irec,ielt_number)
                  if (ielt_number == 0_IXP) then
                     write(info,'(a)') "Error in subroutine init_mesh: ielt_number == 0 for face"
                     call error_stop(info)
                  endif

                  irec = irec + 1_IXP
                  call read_bin_int(myunit_dat,irec,ielt_face)
                  if (ielt_face == 0_IXP) then
                     write(info,'(a)') "Error in subroutine init_mesh: ielt_face == 0 for face"
                     call error_stop(info)
                  endif

                  irec = irec + 1_IXP
                  call read_bin_int(myunit_dat,irec,ielt_coty)

                  irec = irec + 1_IXP
                  call read_bin_int(myunit_dat,irec,ielt_cpu)

                  if (ielt_cpu == ig_myrank) then

                     if (ielt_number > ihexa) then
                        call propagate_gll_nodes_face(ihexa,iface,ielt_number,ielt_face,ielt_coty)
                     endif

                  else

                     ig_nneighbor_all_kind = ig_nneighbor_all_kind + 1_IXP

                     if (ig_nneighbor_all_kind > 26_IXP*ig_nhexa_outer) then
                        write(info,'(a)') "Error in subroutine init_mesh: ig_nneighbor_all_kind too large for face"
                        call error_stop(info)
                     endif

                     ig_cpu_neighbor_info(1_IXP, ig_nneighbor_all_kind) = ihexa
                     ig_cpu_neighbor_info(2_IXP, ig_nneighbor_all_kind) = iface
                     ig_cpu_neighbor_info(3_IXP, ig_nneighbor_all_kind) = ielt_cpu

                  endif

               case(2_IXP) !quad_parax is connected
                  if (ig_nquad_parax > 0_IXP .and. lg_boundary_absorption) then

                     iparax                        = iparax + 1_IXP
                     ig_quadp_neighbor_hexa(iparax)      = ihexa
                     ig_quadp_neighbor_hexaface(iparax) = iface

                     if (ig_quad_nnode == 4_IXP) then
                        select case (iface)
                           case(1_IXP)
                              ig_quadp_gnode_glonum(1_IXP,iparax) = ig_hexa_gnode_glonum(1_IXP,ihexa)
                              ig_quadp_gnode_glonum(2_IXP,iparax) = ig_hexa_gnode_glonum(2_IXP,ihexa)
                              ig_quadp_gnode_glonum(3_IXP,iparax) = ig_hexa_gnode_glonum(3_IXP,ihexa)
                              ig_quadp_gnode_glonum(4_IXP,iparax) = ig_hexa_gnode_glonum(4_IXP,ihexa)
                           case(2_IXP)
                              ig_quadp_gnode_glonum(1_IXP,iparax) = ig_hexa_gnode_glonum(1_IXP,ihexa)
                              ig_quadp_gnode_glonum(2_IXP,iparax) = ig_hexa_gnode_glonum(5_IXP,ihexa)
                              ig_quadp_gnode_glonum(3_IXP,iparax) = ig_hexa_gnode_glonum(6_IXP,ihexa)
                              ig_quadp_gnode_glonum(4_IXP,iparax) = ig_hexa_gnode_glonum(2_IXP,ihexa)
                           case(3_IXP)
                              ig_quadp_gnode_glonum(1_IXP,iparax) = ig_hexa_gnode_glonum(2_IXP,ihexa)
                              ig_quadp_gnode_glonum(2_IXP,iparax) = ig_hexa_gnode_glonum(6_IXP,ihexa)
                              ig_quadp_gnode_glonum(3_IXP,iparax) = ig_hexa_gnode_glonum(7_IXP,ihexa)
                              ig_quadp_gnode_glonum(4_IXP,iparax) = ig_hexa_gnode_glonum(3_IXP,ihexa)
                           case(4_IXP)
                              ig_quadp_gnode_glonum(1_IXP,iparax) = ig_hexa_gnode_glonum(4_IXP,ihexa)
                              ig_quadp_gnode_glonum(2_IXP,iparax) = ig_hexa_gnode_glonum(3_IXP,ihexa)
                              ig_quadp_gnode_glonum(3_IXP,iparax) = ig_hexa_gnode_glonum(7_IXP,ihexa)
                              ig_quadp_gnode_glonum(4_IXP,iparax) = ig_hexa_gnode_glonum(8_IXP,ihexa)
                           case(5_IXP)
                              ig_quadp_gnode_glonum(1_IXP,iparax) = ig_hexa_gnode_glonum(1_IXP,ihexa)
                              ig_quadp_gnode_glonum(2_IXP,iparax) = ig_hexa_gnode_glonum(4_IXP,ihexa)
                              ig_quadp_gnode_glonum(3_IXP,iparax) = ig_hexa_gnode_glonum(8_IXP,ihexa)
                              ig_quadp_gnode_glonum(4_IXP,iparax) = ig_hexa_gnode_glonum(5_IXP,ihexa)
                           case(6_IXP)
                              ig_quadp_gnode_glonum(1_IXP,iparax) = ig_hexa_gnode_glonum(5_IXP,ihexa)
                              ig_quadp_gnode_glonum(2_IXP,iparax) = ig_hexa_gnode_glonum(8_IXP,ihexa)
                              ig_quadp_gnode_glonum(3_IXP,iparax) = ig_hexa_gnode_glonum(7_IXP,ihexa)
                              ig_quadp_gnode_glonum(4_IXP,iparax) = ig_hexa_gnode_glonum(6_IXP,ihexa)
                        end select
                     else
                        ! TODO : idem for 9 nodes quads
                        write(info,'(a)') "Error in init_mesh(): not yet ready for 27 nodes elements"
                        call error_stop(info)
                     endif
                     call propagate_gll_nodes_quad(ihexa,iface,iparax,ig_quadp_gll_glonum,ig_nquad_parax)
                  endif

               case(3) !quad_fsurf is connected

                  ifsurf = ifsurf + 1_IXP

                  if (ig_quad_nnode == 4_IXP) then
                     select case (iface)
                        case(1_IXP)
                           ig_quadf_gnode_glonum(1_IXP,ifsurf) = ig_hexa_gnode_glonum(1_IXP,ihexa) !see sub. init_element
                           ig_quadf_gnode_glonum(2_IXP,ifsurf) = ig_hexa_gnode_glonum(2_IXP,ihexa)
                           ig_quadf_gnode_glonum(3_IXP,ifsurf) = ig_hexa_gnode_glonum(3_IXP,ihexa)
                           ig_quadf_gnode_glonum(4_IXP,ifsurf) = ig_hexa_gnode_glonum(4_IXP,ihexa)
                        case(2_IXP)                                                            
                           ig_quadf_gnode_glonum(1_IXP,ifsurf) = ig_hexa_gnode_glonum(1_IXP,ihexa)
                           ig_quadf_gnode_glonum(2_IXP,ifsurf) = ig_hexa_gnode_glonum(5_IXP,ihexa)
                           ig_quadf_gnode_glonum(3_IXP,ifsurf) = ig_hexa_gnode_glonum(6_IXP,ihexa)
                           ig_quadf_gnode_glonum(4_IXP,ifsurf) = ig_hexa_gnode_glonum(2_IXP,ihexa)
                        case(3_IXP)                                                            
                           ig_quadf_gnode_glonum(1_IXP,ifsurf) = ig_hexa_gnode_glonum(2_IXP,ihexa)
                           ig_quadf_gnode_glonum(2_IXP,ifsurf) = ig_hexa_gnode_glonum(6_IXP,ihexa)
                           ig_quadf_gnode_glonum(3_IXP,ifsurf) = ig_hexa_gnode_glonum(7_IXP,ihexa)
                           ig_quadf_gnode_glonum(4_IXP,ifsurf) = ig_hexa_gnode_glonum(3_IXP,ihexa)
                        case(4_IXP)                                                            
                           ig_quadf_gnode_glonum(1_IXP,ifsurf) = ig_hexa_gnode_glonum(4_IXP,ihexa)
                           ig_quadf_gnode_glonum(2_IXP,ifsurf) = ig_hexa_gnode_glonum(3_IXP,ihexa)
                           ig_quadf_gnode_glonum(3_IXP,ifsurf) = ig_hexa_gnode_glonum(7_IXP,ihexa)
                           ig_quadf_gnode_glonum(4_IXP,ifsurf) = ig_hexa_gnode_glonum(8_IXP,ihexa)
                        case(5_IXP)                                                            
                           ig_quadf_gnode_glonum(1_IXP,ifsurf) = ig_hexa_gnode_glonum(1_IXP,ihexa)
                           ig_quadf_gnode_glonum(2_IXP,ifsurf) = ig_hexa_gnode_glonum(4_IXP,ihexa)
                           ig_quadf_gnode_glonum(3_IXP,ifsurf) = ig_hexa_gnode_glonum(8_IXP,ihexa)
                           ig_quadf_gnode_glonum(4_IXP,ifsurf) = ig_hexa_gnode_glonum(5_IXP,ihexa)
                        case(6_IXP)                                                            
                           ig_quadf_gnode_glonum(1_IXP,ifsurf) = ig_hexa_gnode_glonum(5_IXP,ihexa)
                           ig_quadf_gnode_glonum(2_IXP,ifsurf) = ig_hexa_gnode_glonum(8_IXP,ihexa)
                           ig_quadf_gnode_glonum(3_IXP,ifsurf) = ig_hexa_gnode_glonum(7_IXP,ihexa)
                           ig_quadf_gnode_glonum(4_IXP,ifsurf) = ig_hexa_gnode_glonum(6_IXP,ihexa)
                     end select
                  else
                     ! TODO : idem for 9 nodes quads
                     write(info,'(a)') "Error in init_mesh(): not yet ready for 27 nodes elements"
                     call error_stop(info)
                  endif
                  call propagate_gll_nodes_quad(ihexa,iface,ifsurf,ig_quadf_gll_glonum,ig_nquad_fsurf)

               case default !no element is connected

            end select
         enddo
 
         do iedge = 1_IXP,NEDGE

            irec = irec + 1_IXP
            call read_bin_int(myunit_dat,irec,nbneigh) !nb neighbors of iedge
            
            do ineigh = 1_IXP, nbneigh

               irec = irec + 1_IXP
               call read_bin_int(myunit_dat,irec,ielt_type) !hexa or none

               select case (ielt_type)
                  case(1_IXP) !another hexa is connected

                     irec = irec + 1_IXP
                     call read_bin_int(myunit_dat,irec,ielt_number)

                     irec = irec + 1_IXP
                     call read_bin_int(myunit_dat,irec,ielt_edge)

                     irec = irec + 1_IXP
                     call read_bin_int(myunit_dat,irec,ielt_coty)

                     irec = irec + 1_IXP
                     call read_bin_int(myunit_dat,irec,ielt_cpu)

                     if (ielt_cpu == ig_myrank) then

                        if (ielt_number > ihexa) then
                           call propagate_gll_nodes_edge(ihexa,iedge,ielt_number,ielt_edge,ielt_coty)
                        endif

                     else

                        ig_nneighbor_all_kind = ig_nneighbor_all_kind + 1_IXP

                        if (ig_nneighbor_all_kind > 26_IXP * ig_nhexa_outer) then
                           write(info,'(a)') "Error in subroutine init_mesh: ig_nneighbor_all_kind too large for edge"
                           call error_stop(info)
                        endif

                        ig_cpu_neighbor_info(1_IXP, ig_nneighbor_all_kind) = ihexa
                        ig_cpu_neighbor_info(2_IXP, ig_nneighbor_all_kind) = NFACE + iedge
                        ig_cpu_neighbor_info(3_IXP, ig_nneighbor_all_kind) = ielt_cpu

                     endif

                  case default !no element is connected

               end select

            enddo
         enddo

         do inode = 1_IXP,NNODE

            irec = irec + 1_IXP
            call read_bin_int(myunit_dat,irec,nbneigh) ! nb neighbors of iedge
            
            do ineigh = 1_IXP, nbneigh

               irec = irec + 1_IXP
               call read_bin_int(myunit_dat,irec,ielt_type) !hexa or none

               select case (ielt_type)

                  case(1_IXP) !another hexa is connected

                     irec = irec + 1_IXP
                     call read_bin_int(myunit_dat,irec,ielt_number)

                     irec = irec + 1_IXP
                     call read_bin_int(myunit_dat,irec,ielt_corner)

                     irec = irec + 1_IXP
                     call read_bin_int(myunit_dat,irec,ielt_cpu)

                     if (ielt_cpu == ig_myrank) then

                        if (ielt_number > ihexa) then
                           call propagate_gll_nodes_corner(ihexa,inode,ielt_number,ielt_corner)
                        endif

                     else

                        ig_nneighbor_all_kind = ig_nneighbor_all_kind + 1_IXP

                        if (ig_nneighbor_all_kind > 26_IXP*ig_nhexa_outer) then
                           write(info,'(a)') "Error in subroutine init_mesh: ig_nneighbor_all_kind too large for node"
                           call error_stop(info)
                        endif

                        ig_cpu_neighbor_info(1_IXP, ig_nneighbor_all_kind) = ihexa
                        ig_cpu_neighbor_info(2_IXP, ig_nneighbor_all_kind) = NFACE + NEDGE + inode
                        ig_cpu_neighbor_info(3_IXP, ig_nneighbor_all_kind) = ielt_cpu

                     endif

                  case default !no element is connected

               end select

            enddo
         enddo
      enddo
      close(myunit_dat)
      if (ig_myrank == 0_IXP) write(IG_LST_UNIT,'(a)') "Done"


      !
      !
      !***********************************************************
      !initialize displacement, velocity and acceleration arrays
      !***********************************************************
      ios = init_array_real(rg_gll_displacement,ig_ngll_total,IG_NDOF,"rg_gll_displacement")

      ios = init_array_real(rg_gll_velocity    ,ig_ngll_total,IG_NDOF,"rg_gll_velocity")

      ios = init_array_real(rg_gll_acceleration,ig_ngll_total,IG_NDOF,"rg_gll_acceleration")

      ios = init_array_real(rg_gll_acctmp      ,ig_ngll_total,IG_NDOF,"rg_gll_acctmp")

      !
      !
      !******************************
      !write gll info in *.lst file
      !******************************
      write(info,'(a)') "gll nodes"
      call info_all_cpu(ig_ngll_total,info)


      !
      !
      !*************************************************
      !debug mode : write array ig_hexa_gll_glonum
      !*************************************************
      if (LG_OUTPUT_DEBUG_FILE) then
         open(unit=get_newunit(myunit_debug),file="debug."//trim(cg_prefix)//".global.gll."//trim(cg_myrank))
         do ihexa = 1_IXP,ig_nhexa
            write(unit=myunit_debug,fmt='(a,i10)') "hexa ",ihexa
            do igll = 1_IXP,IG_NGLL
               write(unit=myunit_debug,fmt='(a,i10)') "igll ",igll
               do jgll = 1_IXP,IG_NGLL
                  write(unit=myunit_debug,fmt='(10I10)') (ig_hexa_gll_glonum(kgll,jgll,igll,ihexa),kgll=1,IG_NGLL)
               enddo
            enddo
         enddo
         close(myunit_debug)
      endif

      !
      !
      !*************************************************
      !debug mode : write free surface geom nodes
      !*************************************************
      if (LG_OUTPUT_DEBUG_FILE) then
         open(unit=get_newunit(myunit_debug),file="debug."//trim(cg_prefix)//".freesurface.geom."//trim(cg_myrank))
         do ifsurf = 1_IXP,ig_nquad_fsurf
            do inode = 1_IXP,ig_quad_nnode
               write(unit=myunit_debug,fmt='(2(E14.7,1X))') rg_gnode_x(ig_quadf_gnode_glonum(inode,ifsurf))&
                                                           ,rg_gnode_y(ig_quadf_gnode_glonum(inode,ifsurf))
            enddo
         enddo
         close(myunit_debug)
      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_mesh
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine increments GLL numbering of hexahedron elements in cpu myrank (see variable mod_global_variables::ig_ngll_total) 
!!and creates the local to global GLL indirection array mod_global_variables::ig_hexa_gll_glonum for hexahedron elements.
!>@param ihexa      : hexahedron element number in cpu myrank
!>@param ngll_total : local name of global variable mod_global_variables::ig_ngll_total
!***********************************************************************************************************************************************************************************
   subroutine init_gll_number(ihexa,ngll_total) bind(c,name='init_gll_number')
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only : &
                                       ig_hexa_gll_glonum&
                                      ,IG_NGLL
       
      implicit none

      integer(kind=IXP), intent(in   ) :: ihexa
      integer(kind=IXP), intent(inout) :: ngll_total
                             
      integer(kind=IXP)               :: k
      integer(kind=IXP)               :: l
      integer(kind=IXP)               :: m

      do k = ONE_IXP,IG_NGLL        !along zeta
         do l = ONE_IXP,IG_NGLL     !along eta
            do m = ONE_IXP,IG_NGLL  !along xi

               if (ig_hexa_gll_glonum(m,l,k,ihexa) == ZERO_IXP) then !GLL has not been numbered yet

                  ngll_total                      = ngll_total + ONE_IXP
                  ig_hexa_gll_glonum(m,l,k,ihexa) = ngll_total

               endif

            enddo
         enddo
      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_gll_number
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine creates the local to global GLL indirection array for quadrangle elements by propagating existing GLL numbering of hexahedron elements.
!>@param ihexa              : hexahedron element number to which iquad is connected
!>@param iface              : face number of element ihexa to which iquad is connected
!>@param iquad              : quadrangle element connected to hexahedron element ihexa
!>@param global_gll_of_quad : local to global GLL indirection array for quadrangle elements
!>@param number_of_quad     : number of quadrangle elements in array global_gll_of_quad
!***********************************************************************************************************************************************************************************
    subroutine propagate_gll_nodes_quad(ihexa,iface,iquad,global_gll_of_quad,number_of_quad) bind(c,name='propagate_gll_nodes_quad')
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only : IG_NGLL&
                                      ,error_stop&
                                      ,ig_hexa_gll_glonum

      implicit none

      integer(kind=IXP), intent(in)                                               :: ihexa
      integer(kind=IXP), intent(in)                                               :: iface
      integer(kind=IXP), intent(in)                                               :: iquad
      integer(kind=IXP), intent(in)                                               :: number_of_quad
      integer(kind=IXP), intent(inout), dimension(IG_NGLL,IG_NGLL,number_of_quad) :: global_gll_of_quad
      
      integer(kind=IXP)                                                           :: k,l,m
 
      character(len=255)                                                          :: info
      
      select case(iface)
         case(ONE_IXP)
            do l = ONE_IXP,IG_NGLL
               do m = ONE_IXP,IG_NGLL
                  global_gll_of_quad(m,l,iquad) = ig_hexa_gll_glonum(m,l,ONE_IXP,ihexa)
               enddo
            enddo
            return

         case(TWO_IXP)
            do k = ONE_IXP,IG_NGLL
               do m = ONE_IXP,IG_NGLL
                  global_gll_of_quad(k,m,iquad) = ig_hexa_gll_glonum(m,ONE_IXP,k,ihexa)
               enddo
            enddo
            return

         case(THREE_IXP)
            do k = ONE_IXP,IG_NGLL
               do l = ONE_IXP,IG_NGLL
                  global_gll_of_quad(k,l,iquad) = ig_hexa_gll_glonum(IG_NGLL,l,k,ihexa)
               enddo
            enddo
            return

         case(FOUR_IXP)
            do k = ONE_IXP,IG_NGLL
               do m = ONE_IXP,IG_NGLL
                  global_gll_of_quad(m,k,iquad) = ig_hexa_gll_glonum(m,IG_NGLL,k,ihexa)
               enddo
            enddo
            return

         case(FIVE_IXP)
            do k = ONE_IXP,IG_NGLL
               do l = ONE_IXP,IG_NGLL
                  global_gll_of_quad(l,k,iquad) = ig_hexa_gll_glonum(ONE_IXP,l,k,ihexa)
               enddo
            enddo
            return

         case(SIX_IXP)
            do l = ONE_IXP,IG_NGLL
               do m = ONE_IXP,IG_NGLL
                  global_gll_of_quad(l,m,iquad) = ig_hexa_gll_glonum(m,l,IG_NGLL,ihexa)
               enddo
            enddo
            return

         case default
            write(info,'(a)') "error in subroutine propagate_gll_nodes_quad. Invalid face number"
            call error_stop(info)

      end select
      
!***********************************************************************************************************************************************************************************
   end subroutine propagate_gll_nodes_quad
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine propagates existing GLL numbering of hexahedron elements to the face of its neighbors hexahedron elements.
!>@param ihexa_old : hexahedron element already numbered to which ihexa_new is connected
!>@param iface_old : face number of element ihexa_old to which ihexa_new is connected
!>@param ihexa_new : hexahedron element to be numbered connected to ihexa_old
!>@param iface_new : face number of element ihexa_new connected to iface_old
!>@param icoty_new : connexion type to redirect correctly GLL numbering between connected faces of hexahedron elements
!***********************************************************************************************************************************************************************************
    subroutine propagate_gll_nodes_face(ihexa_old,iface_old,ihexa_new,iface_new,icoty_new) bind(c,name='propagate_gll_nodes_face')
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only : IG_NGLL&
                                      ,ig_hexa_gll_glonum

      implicit none

      integer(kind=IXP), intent(in)    :: ihexa_new
      integer(kind=IXP), intent(in)    :: ihexa_old
      integer(kind=IXP), intent(in)    :: iface_new
      integer(kind=IXP), intent(in)    :: iface_old
      integer(kind=IXP), intent(in)    :: icoty_new
     
      integer(kind=IXP)                :: ext_vec
      integer(kind=IXP)                :: ext_sgn
      integer(kind=IXP)                :: int_vec
      integer(kind=IXP)                :: int_sgn
      integer(kind=IXP)                :: fixed_val
      integer(kind=IXP)                :: i_ext
      integer(kind=IXP)                :: i_int
      integer(kind=IXP)                :: i_ext_rev
      integer(kind=IXP)                :: i_int_rev
      integer(kind=IXP)                :: i_ext_target
      integer(kind=IXP)                :: i_int_target
      integer(kind=IXP)                :: igll_source
      integer(kind=IXP)                :: mid_gll

     
!     icoty_new : 
!     0 . 0 .SX.VX.VX.SI.VI.VI
!     7   6  5  4  3  2  1  0   th bit 
!     128 64 32 16 8  4  2  1  
      
      ext_vec = ishft(iand(24_IXP, icoty_new), -3_IXP)
      ext_sgn = ishft(iand(32_IXP, icoty_new), -5_IXP)
      
      int_vec = iand(THREE_IXP, icoty_new)
      int_sgn = ishft(iand(4_IXP, icoty_new), -2_IXP)
 
      mid_gll   = ceiling(IG_NGLL/TWO_RXP)

      fixed_val = ZERO_IXP

      select case(iface_new)
         case(ONE_IXP)
            if (ig_hexa_gll_glonum(mid_gll,mid_gll,ONE_IXP,ihexa_new) /= ZERO_IXP) return
            fixed_val = ONE_IXP

         case(TWO_IXP)
            if (ig_hexa_gll_glonum(mid_gll,ONE_IXP,mid_gll,ihexa_new) /= ZERO_IXP) return
            fixed_val = ONE_IXP

         case(THREE_IXP)
            if (ig_hexa_gll_glonum(IG_NGLL,mid_gll,mid_gll,ihexa_new) /= ZERO_IXP) return
            fixed_val = IG_NGLL

         case(FOUR_IXP)
            if (ig_hexa_gll_glonum(mid_gll,IG_NGLL,mid_gll,ihexa_new) /= ZERO_IXP) return
            fixed_val = IG_NGLL

         case(FIVE_IXP)
            if (ig_hexa_gll_glonum(ONE_IXP,mid_gll,mid_gll,ihexa_new) /= ZERO_IXP) return
            fixed_val = ONE_IXP

         case(SIX_IXP)
            if (ig_hexa_gll_glonum(mid_gll,mid_gll,IG_NGLL,ihexa_new) /= ZERO_IXP) return
            fixed_val = IG_NGLL

      end select
   
      do i_ext = ONE_IXP,IG_NGLL

         i_ext_rev = (IG_NGLL+ONE_IXP)-i_ext

         do i_int = ONE_IXP,IG_NGLL

            i_int_rev = (IG_NGLL+ONE_IXP)-i_int

            select case(iface_old)
               case(ONE_IXP)
                  igll_source = ig_hexa_gll_glonum(i_ext, i_int, ONE_IXP, ihexa_old)
               case(TWO_IXP)
                  igll_source = ig_hexa_gll_glonum(i_int, ONE_IXP, i_ext, ihexa_old)
               case(THREE_IXP)
                  igll_source = ig_hexa_gll_glonum(IG_NGLL, i_int_rev, i_ext_rev, ihexa_old)
               case(FOUR_IXP)
                  igll_source = ig_hexa_gll_glonum(i_ext_rev, IG_NGLL, i_int_rev, ihexa_old)
               case(FIVE_IXP)
                  igll_source = ig_hexa_gll_glonum(ONE_IXP, i_ext, i_int, ihexa_old)
               case(SIX_IXP)
                  igll_source = ig_hexa_gll_glonum(i_int_rev, i_ext_rev, IG_NGLL, ihexa_old)
            end select
            
            if (ext_sgn == ONE_IXP) then
               i_ext_target = i_ext
            else
               i_ext_target = i_ext_rev
            endif
            if (int_sgn == ONE_IXP) then
               i_int_target = i_int
            else
               i_int_target = i_int_rev
            endif
            
            select case(ext_vec)
               case(ONE_IXP)
                  select case(int_vec)
                     case(TWO_IXP)
                        if (ig_hexa_gll_glonum(fixed_val, i_int_target, i_ext_target, ihexa_new) /= igll_source&
                         .and. ig_hexa_gll_glonum(fixed_val, i_int_target, i_ext_target, ihexa_new) /= ZERO_IXP) write(*,*) 'propagate_gll_nodes_face() ', ihexa_old,iface_old,ihexa_new,iface_new
                        ig_hexa_gll_glonum(fixed_val, i_int_target, i_ext_target, ihexa_new) = igll_source
                     case(THREE_IXP)
                        if (ig_hexa_gll_glonum(i_int_target, fixed_val, i_ext_target, ihexa_new) /= igll_source&
                          .and. ig_hexa_gll_glonum(i_int_target, fixed_val, i_ext_target, ihexa_new) /= ZERO_IXP) write(*,*) 'propagate_gll_nodes_face() ', ihexa_old,iface_old,ihexa_new,iface_new
                        ig_hexa_gll_glonum(i_int_target, fixed_val, i_ext_target, ihexa_new) = igll_source
                  end select
               case(TWO_IXP)
                  select case(int_vec)
                     case(ONE_IXP)
                        if (ig_hexa_gll_glonum(fixed_val, i_ext_target, i_int_target, ihexa_new) /= igll_source&
                          .and. ig_hexa_gll_glonum(fixed_val, i_ext_target, i_int_target, ihexa_new) /= ZERO_IXP) write(*,*) 'propagate_gll_nodes_face() ', ihexa_old,iface_old,ihexa_new,iface_new
                        ig_hexa_gll_glonum(fixed_val, i_ext_target, i_int_target, ihexa_new) = igll_source
                     case(THREE_IXP)
                        if (ig_hexa_gll_glonum(i_int_target, i_ext_target, fixed_val, ihexa_new) /= igll_source&
                          .and. ig_hexa_gll_glonum(i_int_target, i_ext_target, fixed_val, ihexa_new) /= ZERO_IXP) write(*,*) 'propagate_gll_nodes_face() ', ihexa_old,iface_old,ihexa_new,iface_new
                        ig_hexa_gll_glonum(i_int_target, i_ext_target, fixed_val, ihexa_new) = igll_source
                  end select
               case(THREE_IXP)
                  select case(int_vec)
                     case(ONE_IXP)
                        if (ig_hexa_gll_glonum(i_ext_target, fixed_val, i_int_target, ihexa_new) /= igll_source&
                          .and. ig_hexa_gll_glonum(i_ext_target, fixed_val, i_int_target, ihexa_new) /= ZERO_IXP) write(*,*) 'propagate_gll_nodes_face() ', ihexa_old,iface_old,ihexa_new,iface_new
                        ig_hexa_gll_glonum(i_ext_target, fixed_val, i_int_target, ihexa_new) = igll_source
                     case(TWO_IXP)
                        if (ig_hexa_gll_glonum(i_ext_target, i_int_target, fixed_val, ihexa_new) /= igll_source&
                          .and. ig_hexa_gll_glonum(i_ext_target, i_int_target, fixed_val, ihexa_new) /= ZERO_IXP) write(*,*) 'propagate_gll_nodes_face() ', ihexa_old,iface_old,ihexa_new,iface_new
                        ig_hexa_gll_glonum(i_ext_target, i_int_target, fixed_val, ihexa_new) = igll_source
                  end select
            end select
         enddo
      enddo
      
      return
!***********************************************************************************************************************************************************************************
   end subroutine propagate_gll_nodes_face
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine propagates existing GLL numbering of hexahedron elements to the edge of its neighbors hexahedron elements.
!>@param ihexa_old : hexahedron element already numbered to which ihexa_new is connected
!>@param iedge_old : edge number of element ihexa_old to which ihexa_new is connected
!>@param ihexa_new : hexahedron element to be numbered connected to ihexa_old
!>@param iedge_new : edge number of element ihexa_new connected to iedge_old
!>@param icoty_new : connexion type to redirect correctly GLL numbering between connected edges of hexahedron elements
!***********************************************************************************************************************************************************************************
   subroutine propagate_gll_nodes_edge(ihexa_old,iedge_old,ihexa_new,iedge_new,icoty_new) bind(c,name='propagate_gll_nodes_edge')
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only : IG_NGLL&
                                      ,ig_hexa_gll_glonum&
                                      ,LG_OUTPUT_DEBUG_FILE

      implicit none

      integer(kind=IXP), intent(in)    :: ihexa_new
      integer(kind=IXP), intent(in)    :: ihexa_old
      integer(kind=IXP), intent(in)    :: iedge_new
      integer(kind=IXP), intent(in)    :: iedge_old
      integer(kind=IXP), intent(in)    :: icoty_new
      
      integer(kind=IXP), pointer       :: pedge_old(:)
      integer(kind=IXP), pointer       :: pedge_new(:)
      integer(kind=IXP)                :: i, j

      select case(iedge_old)
         case(1_IXP)
            pedge_old => ig_hexa_gll_glonum(ONE_IXP:IG_NGLL,ONE_IXP,ONE_IXP,ihexa_old)
         case(2_IXP)
            pedge_old => ig_hexa_gll_glonum(IG_NGLL,ONE_IXP:IG_NGLL,ONE_IXP,ihexa_old)
         case(3_IXP)
            pedge_old => ig_hexa_gll_glonum(ONE_IXP:IG_NGLL,IG_NGLL,ONE_IXP,ihexa_old)
         case(4_IXP)
            pedge_old => ig_hexa_gll_glonum(ONE_IXP,ONE_IXP:IG_NGLL,ONE_IXP,ihexa_old)
         case(5_IXP)
            pedge_old => ig_hexa_gll_glonum(ONE_IXP:IG_NGLL,ONE_IXP,IG_NGLL,ihexa_old)
         case(6_IXP)
            pedge_old => ig_hexa_gll_glonum(IG_NGLL,ONE_IXP:IG_NGLL,IG_NGLL,ihexa_old)
         case(7_IXP)
            pedge_old => ig_hexa_gll_glonum(ONE_IXP:IG_NGLL,IG_NGLL,IG_NGLL,ihexa_old)
         case(8_IXP)
            pedge_old => ig_hexa_gll_glonum(ONE_IXP,ONE_IXP:IG_NGLL,IG_NGLL,ihexa_old)
         case(9_IXP)
            pedge_old => ig_hexa_gll_glonum(ONE_IXP,ONE_IXP,ONE_IXP:IG_NGLL,ihexa_old)
         case(10_IXP)
            pedge_old => ig_hexa_gll_glonum(IG_NGLL,ONE_IXP,ONE_IXP:IG_NGLL,ihexa_old)
         case(11_IXP)
            pedge_old => ig_hexa_gll_glonum(IG_NGLL,IG_NGLL,ONE_IXP:IG_NGLL,ihexa_old)
         case(12_IXP)
            pedge_old => ig_hexa_gll_glonum(ONE_IXP,IG_NGLL,ONE_IXP:IG_NGLL,ihexa_old)
      end select

      select case(iedge_new)
         case(1_IXP)
            pedge_new => ig_hexa_gll_glonum(ONE_IXP:IG_NGLL,ONE_IXP,ONE_IXP,ihexa_new)
         case(2_IXP)
            pedge_new => ig_hexa_gll_glonum(IG_NGLL,ONE_IXP:IG_NGLL,ONE_IXP,ihexa_new)
         case(3_IXP)
            pedge_new => ig_hexa_gll_glonum(ONE_IXP:IG_NGLL,IG_NGLL,ONE_IXP,ihexa_new)
         case(4_IXP)
            pedge_new => ig_hexa_gll_glonum(ONE_IXP,ONE_IXP:IG_NGLL,ONE_IXP,ihexa_new)
         case(5_IXP)
            pedge_new => ig_hexa_gll_glonum(ONE_IXP:IG_NGLL,ONE_IXP,IG_NGLL,ihexa_new)
         case(6_IXP)
            pedge_new => ig_hexa_gll_glonum(IG_NGLL,ONE_IXP:IG_NGLL,IG_NGLL,ihexa_new)
         case(7_IXP)
            pedge_new => ig_hexa_gll_glonum(ONE_IXP:IG_NGLL,IG_NGLL,IG_NGLL,ihexa_new)
         case(8_IXP)
            pedge_new => ig_hexa_gll_glonum(ONE_IXP,ONE_IXP:IG_NGLL,IG_NGLL,ihexa_new)
         case(9_IXP)
            pedge_new => ig_hexa_gll_glonum(ONE_IXP,ONE_IXP,ONE_IXP:IG_NGLL,ihexa_new)
         case(10_IXP)
            pedge_new => ig_hexa_gll_glonum(IG_NGLL,ONE_IXP,ONE_IXP:IG_NGLL,ihexa_new)
         case(11_IXP)
            pedge_new => ig_hexa_gll_glonum(IG_NGLL,IG_NGLL,ONE_IXP:IG_NGLL,ihexa_new)
         case(12_IXP)
            pedge_new => ig_hexa_gll_glonum(ONE_IXP,IG_NGLL,ONE_IXP:IG_NGLL,ihexa_new)
      end select
      
      do i = ONE_IXP,IG_NGLL
         if (icoty_new == ONE_IXP) then
            j = i
         else 
            j = IG_NGLL+ONE_IXP-i
         endif
         if ( (pedge_new(j) /= pedge_old(i) .and. pedge_new(j) /= ZERO_IXP) .or. pedge_old(i) == ZERO_IXP ) write(*,*) 'propagate_gll_nodes_edge',ihexa_old,iedge_old,ihexa_new,iedge_new
         pedge_new(j) = pedge_old(i)
      enddo
      
      return
!***********************************************************************************************************************************************************************************
   end subroutine propagate_gll_nodes_edge
!***********************************************************************************************************************************************************************************

!
! 
!>@brief This subroutine propagates existing GLL numbering of hexahedron elements to corner (i.e., vertex) of its neighbors hexahedron elements.
!>@param ihexa_old   : hexahedron element already numbered to which ihexa_new is connected
!>@param icorner_old : corner number of element ihexa_old to which ihexa_new is connected
!>@param ihexa_new   : hexahedron element to be numbered connected to ihexa_old
!>@param icorner_new : corner number of element ihexa_new connected to icorner_old
!***********************************************************************************************************************************************************************************
   subroutine propagate_gll_nodes_corner(ihexa_old,icorner_old,ihexa_new,icorner_new) bind(c,name='propagate_gll_nodes_corner')
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only : IG_NGLL&
                                      ,ig_hexa_gll_glonum&
                                      ,LG_OUTPUT_DEBUG_FILE

      implicit none

      integer(kind=IXP), intent(in)    :: ihexa_new
      integer(kind=IXP), intent(in)    :: ihexa_old
      integer(kind=IXP), intent(in)    :: icorner_new
      integer(kind=IXP), intent(in)    :: icorner_old
      
      integer(kind=IXP), pointer       :: pcorner_old
      integer(kind=IXP), pointer       :: pcorner_new

      select case(icorner_old)
         case(1_IXP)
            pcorner_old => ig_hexa_gll_glonum(ONE_IXP,ONE_IXP,ONE_IXP,ihexa_old)
         case(2_IXP)
            pcorner_old => ig_hexa_gll_glonum(IG_NGLL,ONE_IXP,ONE_IXP,ihexa_old)
         case(3_IXP)
            pcorner_old => ig_hexa_gll_glonum(IG_NGLL,IG_NGLL,ONE_IXP,ihexa_old)
         case(4_IXP)
            pcorner_old => ig_hexa_gll_glonum(ONE_IXP,IG_NGLL,ONE_IXP,ihexa_old)
         case(5_IXP)
            pcorner_old => ig_hexa_gll_glonum(ONE_IXP,ONE_IXP,IG_NGLL,ihexa_old)
         case(6_IXP)
            pcorner_old => ig_hexa_gll_glonum(IG_NGLL,ONE_IXP,IG_NGLL,ihexa_old)
         case(7_IXP)
            pcorner_old => ig_hexa_gll_glonum(IG_NGLL,IG_NGLL,IG_NGLL,ihexa_old)
         case(8_IXP)
            pcorner_old => ig_hexa_gll_glonum(ONE_IXP,IG_NGLL,IG_NGLL,ihexa_old)
      end select

      select case(icorner_new)
         case(1_IXP)
            pcorner_new => ig_hexa_gll_glonum(ONE_IXP,ONE_IXP,ONE_IXP,ihexa_new)
         case(2_IXP)
            pcorner_new => ig_hexa_gll_glonum(IG_NGLL,ONE_IXP,ONE_IXP,ihexa_new)
         case(3_IXP)
            pcorner_new => ig_hexa_gll_glonum(IG_NGLL,IG_NGLL,ONE_IXP,ihexa_new)
         case(4_IXP)
            pcorner_new => ig_hexa_gll_glonum(ONE_IXP,IG_NGLL,ONE_IXP,ihexa_new)
         case(5_IXP)
            pcorner_new => ig_hexa_gll_glonum(ONE_IXP,ONE_IXP,IG_NGLL,ihexa_new)
         case(6_IXP)
            pcorner_new => ig_hexa_gll_glonum(IG_NGLL,ONE_IXP,IG_NGLL,ihexa_new)
         case(7_IXP)
            pcorner_new => ig_hexa_gll_glonum(IG_NGLL,IG_NGLL,IG_NGLL,ihexa_new)
         case(8_IXP)
            pcorner_new => ig_hexa_gll_glonum(ONE_IXP,IG_NGLL,IG_NGLL,ihexa_new)
      end select
      if ( (pcorner_new /= pcorner_old .and. pcorner_new /= ZERO_IXP) .or. pcorner_old == ZERO_IXP ) write(*,*) 'propagate_gll_nodes_corner',ihexa_old,icorner_old,ihexa_new,icorner_new
      pcorner_new = pcorner_old
      
      return
!***********************************************************************************************************************************************************************************
   end subroutine propagate_gll_nodes_corner
!***********************************************************************************************************************************************************************************

!
!
!>@brief subroutine to set up convention of hexahedron and quadrangle elements
!>@param ilnnhe : local name of global variable mod_global_variables::ig_hexa_nnode
!>@param ilnnqu : local name of global variable mod_global_variables::ig_quad_nnode
!>@return mod_global_variables::rg_gnode_abscissa
!>@return mod_global_variables::rg_gnode_abscissa_dist
!>@return mod_global_variables::ig_line_nnode
!>@return mod_global_variables::ig_hexa_gnode_xiloc
!>@return mod_global_variables::ig_hexa_gnode_etloc
!>@return mod_global_variables::ig_hexa_gnode_zeloc
!>@return mod_global_variables::ig_quad_gnode_xiloc
!>@return mod_global_variables::ig_quad_gnode_etloc
!***********************************************************************************************************************************************************************************
    subroutine init_element(ilnnhe,ilnnqu)
!***********************************************************************************************************************************************************************************
 
      use mpi

      use mod_global_variables, only : ig_hexa_gnode_xiloc&
                                      ,ig_hexa_gnode_etloc&
                                      ,ig_hexa_gnode_zeloc&
                                      ,ig_line_nnode&
                                      ,ig_hexa_node2gll&
                                      ,ig_hexa_face2mid_gll&
                                      ,ig_hexa_face_node&
                                      ,rg_gnode_abscissa&
                                      ,ig_quad_gnode_xiloc&
                                      ,ig_quad_gnode_etloc&
                                      ,rg_gnode_abscissa_dist&
                                      ,IG_NGLL&
                                      ,error_stop

      use mod_init_memory
      
      implicit none
      
      integer(kind=IXP), intent(in) :: ilnnhe
      integer(kind=IXP), intent(in) :: ilnnqu

      integer(kind=IXP)            :: ios
      integer(kind=IXP)            :: i
      integer(kind=IXP)            :: j

      character(len=255)  :: info
      !
      !
      !**********************************************************************************
      !fill local position of geometric node of hexa8 or hexa27 on xi, eta and zeta axis
      !**********************************************************************************
      ios = init_array_int(ig_hexa_gnode_xiloc,ilnnhe,"ig_hexa_gnode_xiloc")

      ios = init_array_int(ig_hexa_gnode_etloc,ilnnhe,"ig_hexa_gnode_etloc")

      ios = init_array_int(ig_hexa_gnode_zeloc,ilnnhe,"ig_hexa_gnode_zeloc")

      if (ilnnhe == 8_IXP) then

         ig_line_nnode = TWO_IXP
!
!------->fill rg_gnode_abscissa: local coordinate of geometric nodes
         ios = init_array_real(rg_gnode_abscissa,ig_line_nnode,"rg_gnode_abscissa")

         rg_gnode_abscissa(1_IXP) = +ONE_RXP
         rg_gnode_abscissa(2_IXP) = -ONE_RXP
!
!------->local position of geometric nodes
         ig_hexa_gnode_xiloc(1_IXP) = ONE_IXP
         ig_hexa_gnode_etloc(1_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc(1_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc(2_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(2_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc(2_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc(3_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(3_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(3_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc(4_IXP) = ONE_IXP
         ig_hexa_gnode_etloc(4_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(4_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc(5_IXP) = ONE_IXP
         ig_hexa_gnode_etloc(5_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc(5_IXP) = TWO_IXP
         ig_hexa_gnode_xiloc(6_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(6_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc(6_IXP) = TWO_IXP
         ig_hexa_gnode_xiloc(7_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(7_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(7_IXP) = TWO_IXP
         ig_hexa_gnode_xiloc(8_IXP) = ONE_IXP
         ig_hexa_gnode_etloc(8_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(8_IXP) = TWO_IXP

      elseif (ilnnhe == 27_IXP) then

         ig_line_nnode = 3_IXP
!
!------->fill rg_gnode_abscissa: local coordinate of geometric nodes
         ios = init_array_real(rg_gnode_abscissa,ig_line_nnode,"rg_gnode_abscissa")

         rg_gnode_abscissa(1_IXP) = +ONE_RXP
         rg_gnode_abscissa(2_IXP) = ZERO_RXP
         rg_gnode_abscissa(3_IXP) = -ONE_RXP
!
!------->local position of geometric nodes
         ig_hexa_gnode_xiloc( 1_IXP) = ONE_IXP
         ig_hexa_gnode_etloc( 1_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc( 1_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc( 2_IXP) = THREE_IXP
         ig_hexa_gnode_etloc( 2_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc( 2_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc( 3_IXP) = THREE_IXP
         ig_hexa_gnode_etloc( 3_IXP) = THREE_IXP
         ig_hexa_gnode_zeloc( 3_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc( 4_IXP) = ONE_IXP
         ig_hexa_gnode_etloc( 4_IXP) = THREE_IXP
         ig_hexa_gnode_zeloc( 4_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc( 5_IXP) = ONE_IXP
         ig_hexa_gnode_etloc( 5_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc( 5_IXP) = THREE_IXP
         ig_hexa_gnode_xiloc( 6_IXP) = THREE_IXP
         ig_hexa_gnode_etloc( 6_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc( 6_IXP) = THREE_IXP
         ig_hexa_gnode_xiloc( 7_IXP) = THREE_IXP
         ig_hexa_gnode_etloc( 7_IXP) = THREE_IXP
         ig_hexa_gnode_zeloc( 7_IXP) = THREE_IXP
         ig_hexa_gnode_xiloc( 8_IXP) = ONE_IXP
         ig_hexa_gnode_etloc( 8_IXP) = THREE_IXP
         ig_hexa_gnode_zeloc( 8_IXP) = THREE_IXP
         ig_hexa_gnode_xiloc( 9_IXP) = TWO_IXP
         ig_hexa_gnode_etloc( 9_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc( 9_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc(10_IXP) = THREE_IXP
         ig_hexa_gnode_etloc(10_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(10_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc(11_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(11_IXP) = THREE_IXP
         ig_hexa_gnode_zeloc(11_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc(12_IXP) = ONE_IXP
         ig_hexa_gnode_etloc(12_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(12_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc(13_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(13_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc(13_IXP) = THREE_IXP
         ig_hexa_gnode_xiloc(14_IXP) = THREE_IXP
         ig_hexa_gnode_etloc(14_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(14_IXP) = THREE_IXP
         ig_hexa_gnode_xiloc(15_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(15_IXP) = THREE_IXP
         ig_hexa_gnode_zeloc(15_IXP) = THREE_IXP
         ig_hexa_gnode_xiloc(16_IXP) = ONE_IXP
         ig_hexa_gnode_etloc(16_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(16_IXP) = THREE_IXP
         ig_hexa_gnode_xiloc(17_IXP) = ONE_IXP
         ig_hexa_gnode_etloc(17_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc(17_IXP) = TWO_IXP
         ig_hexa_gnode_xiloc(18_IXP) = THREE_IXP
         ig_hexa_gnode_etloc(18_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc(18_IXP) = TWO_IXP
         ig_hexa_gnode_xiloc(19_IXP) = THREE_IXP
         ig_hexa_gnode_etloc(19_IXP) = THREE_IXP
         ig_hexa_gnode_zeloc(19_IXP) = TWO_IXP
         ig_hexa_gnode_xiloc(20_IXP) = ONE_IXP
         ig_hexa_gnode_etloc(20_IXP) = THREE_IXP
         ig_hexa_gnode_zeloc(20_IXP) = TWO_IXP
         ig_hexa_gnode_xiloc(21_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(21_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(21_IXP) = ONE_IXP
         ig_hexa_gnode_xiloc(22_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(22_IXP) = ONE_IXP
         ig_hexa_gnode_zeloc(22_IXP) = TWO_IXP
         ig_hexa_gnode_xiloc(23_IXP) = THREE_IXP
         ig_hexa_gnode_etloc(23_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(23_IXP) = TWO_IXP
         ig_hexa_gnode_xiloc(24_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(24_IXP) = THREE_IXP
         ig_hexa_gnode_zeloc(24_IXP) = TWO_IXP
         ig_hexa_gnode_xiloc(25_IXP) = ONE_IXP
         ig_hexa_gnode_etloc(25_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(25_IXP) = TWO_IXP
         ig_hexa_gnode_xiloc(26_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(26_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(26_IXP) = THREE_IXP
         ig_hexa_gnode_xiloc(27_IXP) = TWO_IXP
         ig_hexa_gnode_etloc(27_IXP) = TWO_IXP
         ig_hexa_gnode_zeloc(27_IXP) = TWO_IXP
      else
         write(info,'(a)') "error in init_element: invalid number of geometrical nodes for hexa"
         call error_stop(info)
      endif


      !
      !
      !******************************************************************
      !init rg_gnode_abscissa_dist
      !******************************************************************
      ios = init_array_real(rg_gnode_abscissa_dist,ig_line_nnode,ig_line_nnode,"rg_gnode_abscissa_dist")

      do i = ONE_IXP,ig_line_nnode
         do j = ONE_IXP,ig_line_nnode
            rg_gnode_abscissa_dist(j,i) = ZERO_RXP
            if (i /= j) rg_gnode_abscissa_dist(j,i) = ONE_RXP/(rg_gnode_abscissa(i) - rg_gnode_abscissa(j))
         enddo
      enddo

      !
      !
      !******************************************************************
      !init local position of geometric node of quad4 or quad9 on xi, eta
      !******************************************************************
      ios = init_array_int(ig_quad_gnode_xiloc,ilnnqu,"ig_quad_gnode_xiloc")

      ios = init_array_int(ig_quad_gnode_etloc,ilnnqu,"ig_quad_gnode_etloc")

      if (ilnnqu == 4_IXP) then

         ig_quad_gnode_xiloc(1_IXP) = ONE_IXP
         ig_quad_gnode_etloc(1_IXP) = ONE_IXP
         ig_quad_gnode_xiloc(2_IXP) = TWO_IXP
         ig_quad_gnode_etloc(2_IXP) = ONE_IXP
         ig_quad_gnode_xiloc(3_IXP) = TWO_IXP
         ig_quad_gnode_etloc(3_IXP) = TWO_IXP
         ig_quad_gnode_xiloc(4_IXP) = ONE_IXP
         ig_quad_gnode_etloc(4_IXP) = TWO_IXP

      elseif (ilnnqu == 9_IXP) then

         ig_quad_gnode_xiloc(1_IXP) = ONE_IXP
         ig_quad_gnode_etloc(1_IXP) = ONE_IXP
         ig_quad_gnode_xiloc(2_IXP) = THREE_IXP
         ig_quad_gnode_etloc(2_IXP) = ONE_IXP
         ig_quad_gnode_xiloc(3_IXP) = THREE_IXP
         ig_quad_gnode_etloc(3_IXP) = THREE_IXP
         ig_quad_gnode_xiloc(4_IXP) = ONE_IXP
         ig_quad_gnode_etloc(4_IXP) = THREE_IXP
         ig_quad_gnode_xiloc(5_IXP) = TWO_IXP
         ig_quad_gnode_etloc(5_IXP) = ONE_IXP
         ig_quad_gnode_xiloc(6_IXP) = THREE_IXP
         ig_quad_gnode_etloc(6_IXP) = TWO_IXP
         ig_quad_gnode_xiloc(7_IXP) = TWO_IXP
         ig_quad_gnode_etloc(7_IXP) = THREE_IXP
         ig_quad_gnode_xiloc(8_IXP) = ONE_IXP
         ig_quad_gnode_etloc(8_IXP) = TWO_IXP
         ig_quad_gnode_xiloc(9_IXP) = TWO_IXP
         ig_quad_gnode_etloc(9_IXP) = TWO_IXP

      else

         write(info,'(a)') "error in init_element: invalid number of geometrical nodes for quad"
         call error_stop(info)

      endif
      !
      !
      !***********************************************************************
      !indirection from geom nodes to GLL nodes (see docs/Convention.html)
      !***********************************************************************
      !                 xi       eta      zeta
      !                 m   -     l   -    k
      !
      !geom node
      !    1            1         1        1
      !    2          IG_NGLL     1        1
      !    3          IG_NGLL  IG_NGLL     1
      !    4            1      IG_NGLL     1
      !    5            1         1     IG_NGLL
      !    6          IG_NGLL     1     IG_NGLL
      !    7          IG_NGLL  IG_NGLL  IG_NGLL
      !    8            1      IG_NGLL  IG_NGLL
      !

      ig_hexa_node2gll(ONE_IXP,ONE_IXP) = ONE_IXP
      ig_hexa_node2gll(TWO_IXP,ONE_IXP) = ONE_IXP
      ig_hexa_node2gll(THREE_IXP,ONE_IXP) = ONE_IXP

      ig_hexa_node2gll(ONE_IXP,TWO_IXP) = ONE_IXP
      ig_hexa_node2gll(TWO_IXP,TWO_IXP) = ONE_IXP
      ig_hexa_node2gll(THREE_IXP,TWO_IXP) = IG_NGLL

      ig_hexa_node2gll(ONE_IXP,THREE_IXP) = ONE_IXP
      ig_hexa_node2gll(TWO_IXP,THREE_IXP) = IG_NGLL
      ig_hexa_node2gll(THREE_IXP,THREE_IXP) = IG_NGLL

      ig_hexa_node2gll(ONE_IXP,FOUR_IXP) = ONE_IXP
      ig_hexa_node2gll(TWO_IXP,FOUR_IXP) = IG_NGLL
      ig_hexa_node2gll(THREE_IXP,FOUR_IXP) = ONE_IXP

      ig_hexa_node2gll(ONE_IXP,FIVE_IXP) = IG_NGLL
      ig_hexa_node2gll(TWO_IXP,FIVE_IXP) = ONE_IXP
      ig_hexa_node2gll(THREE_IXP,FIVE_IXP) = ONE_IXP

      ig_hexa_node2gll(ONE_IXP,SIX_IXP) = IG_NGLL
      ig_hexa_node2gll(TWO_IXP,SIX_IXP) = ONE_IXP
      ig_hexa_node2gll(THREE_IXP,SIX_IXP) = IG_NGLL

      ig_hexa_node2gll(ONE_IXP,SEVEN_IXP) = IG_NGLL
      ig_hexa_node2gll(TWO_IXP,SEVEN_IXP) = IG_NGLL
      ig_hexa_node2gll(THREE_IXP,SEVEN_IXP) = IG_NGLL

      ig_hexa_node2gll(ONE_IXP,HEIGHT_IXP) = IG_NGLL
      ig_hexa_node2gll(TWO_IXP,HEIGHT_IXP) = IG_NGLL
      ig_hexa_node2gll(THREE_IXP,HEIGHT_IXP) = ONE_IXP

      !
      !
      !**************************************************************************
      !indirection from face's node indices to hexa's node indices
      !geometric nodes order is such that the normal to the face point outward
      !first node corresponds to the indice (m,l) or (m,k) or (l,k) ... = (ONE_IXP,ONE_IXP) 
      !**************************************************************************

      !facenode indice--|         |--face indice
      !                 v         v
      ig_hexa_face_node(ONE_IXP  ,ONE_IXP) = ONE_IXP !<-- hexa node indice
      ig_hexa_face_node(TWO_IXP  ,ONE_IXP) = TWO_IXP
      ig_hexa_face_node(THREE_IXP,ONE_IXP) = THREE_IXP
      ig_hexa_face_node(FOUR_IXP ,ONE_IXP) = FOUR_IXP

      ig_hexa_face_node(ONE_IXP  ,TWO_IXP) = ONE_IXP
      ig_hexa_face_node(TWO_IXP  ,TWO_IXP) = FIVE_IXP
      ig_hexa_face_node(THREE_IXP,TWO_IXP) = SIX_IXP
      ig_hexa_face_node(FOUR_IXP ,TWO_IXP) = TWO_IXP

      ig_hexa_face_node(ONE_IXP  ,THREE_IXP) = TWO_IXP
      ig_hexa_face_node(TWO_IXP  ,THREE_IXP) = SIX_IXP
      ig_hexa_face_node(THREE_IXP,THREE_IXP) = SEVEN_IXP
      ig_hexa_face_node(FOUR_IXP ,THREE_IXP) = THREE_IXP

      ig_hexa_face_node(ONE_IXP  ,FOUR_IXP) = FOUR_IXP
      ig_hexa_face_node(TWO_IXP  ,FOUR_IXP) = THREE_IXP
      ig_hexa_face_node(THREE_IXP,FOUR_IXP) = SEVEN_IXP
      ig_hexa_face_node(FOUR_IXP ,FOUR_IXP) = HEIGHT_IXP

      ig_hexa_face_node(ONE_IXP  ,FIVE_IXP) = ONE_IXP
      ig_hexa_face_node(TWO_IXP  ,FIVE_IXP) = FOUR_IXP
      ig_hexa_face_node(THREE_IXP,FIVE_IXP) = HEIGHT_IXP
      ig_hexa_face_node(FOUR_IXP ,FIVE_IXP) = FIVE_IXP

      ig_hexa_face_node(ONE_IXP  ,SIX_IXP) = FIVE_IXP
      ig_hexa_face_node(TWO_IXP  ,SIX_IXP) = HEIGHT_IXP
      ig_hexa_face_node(THREE_IXP,SIX_IXP) = SEVEN_IXP
      ig_hexa_face_node(FOUR_IXP ,SIX_IXP) = SIX_IXP

      !
      !
      !******************************************************************************
      !indirection from face number to local indices k(index=1=,l(index=2),m(index=3)
      !of the gll node in the middle of the face 
      !(WARNING: for odd GLL node number only)
      !******************************************************************************

      !for k
      ig_hexa_face2mid_gll(ONE_IXP,ONE_IXP) = ONE_IXP
      ig_hexa_face2mid_gll(ONE_IXP,TWO_IXP) = ceiling(IG_NGLL/TWO_RXP)
      ig_hexa_face2mid_gll(ONE_IXP,THREE_IXP) = ceiling(IG_NGLL/TWO_RXP) 
      ig_hexa_face2mid_gll(ONE_IXP,FOUR_IXP) = ceiling(IG_NGLL/TWO_RXP) 
      ig_hexa_face2mid_gll(ONE_IXP,FIVE_IXP) = ceiling(IG_NGLL/TWO_RXP) 
      ig_hexa_face2mid_gll(ONE_IXP,SIX_IXP) = IG_NGLL
  
      !for l
      ig_hexa_face2mid_gll(TWO_IXP,ONE_IXP) = ceiling(IG_NGLL/TWO_RXP)
      ig_hexa_face2mid_gll(TWO_IXP,TWO_IXP) = ONE_IXP
      ig_hexa_face2mid_gll(TWO_IXP,THREE_IXP) = ceiling(IG_NGLL/TWO_RXP)
      ig_hexa_face2mid_gll(TWO_IXP,FOUR_IXP) = IG_NGLL
      ig_hexa_face2mid_gll(TWO_IXP,FIVE_IXP) = ceiling(IG_NGLL/TWO_RXP)
      ig_hexa_face2mid_gll(TWO_IXP,SIX_IXP) = ceiling(IG_NGLL/TWO_RXP)
  
      !for m
      ig_hexa_face2mid_gll(THREE_IXP,ONE_IXP) = ceiling(IG_NGLL/TWO_RXP)
      ig_hexa_face2mid_gll(THREE_IXP,TWO_IXP) = ceiling(IG_NGLL/TWO_RXP)
      ig_hexa_face2mid_gll(THREE_IXP,THREE_IXP) = IG_NGLL
      ig_hexa_face2mid_gll(THREE_IXP,FOUR_IXP) = ceiling(IG_NGLL/TWO_RXP)
      ig_hexa_face2mid_gll(THREE_IXP,FIVE_IXP) = ONE_IXP
      ig_hexa_face2mid_gll(THREE_IXP,SIX_IXP) = ceiling(IG_NGLL/TWO_RXP)
  
      return
!***********************************************************************************************************************************************************************************
   end subroutine init_element
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine reads integer in binary files *.dat using a buffer of size BUFFER_READ_SIZE.
!>@param u  : FORTRAN unit file to be read
!>@param ir : record number to read in buffer array
!>@param i  : output integer
!***********************************************************************************************************************************************************************************
   subroutine read_bin_int(u,ir,i)
!***********************************************************************************************************************************************************************************

      use mod_precision

      implicit none

      integer(kind=IXP), intent(in   )  :: u
      integer(kind=IXP), intent(inout)  :: ir
      integer(kind=IXP), intent(  out)  :: i

      integer(kind=IXP)                 :: ios

      if (ir == ONE_IXP) then

         read(u,iostat=ios) buffer
         i = buffer(ir)

      elseif (ir < BUFFER_READ_SIZE) then

         i = buffer(ir)

      else

         i = buffer(ir)
         ir = ZERO_IXP

      endif

      return 

!***********************************************************************************************************************************************************************************
   end subroutine read_bin_int
!***********************************************************************************************************************************************************************************:w

end module mod_init_mesh
