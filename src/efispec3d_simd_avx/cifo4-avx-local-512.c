//!>!===================================================================================================================================!<!
//!>!                                                        EFISPEC3D                                                                  !<!
//!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
//!>!                                                                                                                                   !<!
//!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
//!>!                                                                                                                                   !<!
//!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                                                 http://efispec.free.fr                                                            !<!
//!>!                                                                                                                                   !<!
//!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
//!>!                                                                                                                                   !<!
//!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
//!>!                                                                David    MICHEA                                                    !<!
//!>!                                                                Philippe THIERRY                                                   !<!
//!>!                                                                Sylvain  JUBERTIE                                                  !<!
//!>!                                                                Emmanuel CHALJUB                                                   !<!
//!>!                                                                Francois LAVOUE                                                    !<!
//!>!                                                                Tom      BUDON                                                     !<!
//!>!                                                                Emmanuel MELIN                                                     !<!
//!>!                                                                                                                                   !<!
//!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
//!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
//!>!                                                                                                                                   !<!
//!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
//!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
//!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
//!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
//!>!                           "http://www.cecill.info".                                                                               !<!
//!>!                                                                                                                                   !<!
//!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
//!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
//!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
//!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
//!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
//!>!                                                                                                                                   !<!
//!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
//!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
//!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
//!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
//!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
//!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
//!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
//!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
//!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
//!>!                           securite.                                                                                               !<!
//!>!                                                                                                                                   !<!
//!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
//!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
//!>!                                                                                                                                   !<!
//!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
//!>!                           motion using a finite spectral-element method.                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
//!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
//!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
//!>!                           version.                                                                                                !<!
//!>!                                                                                                                                   !<!
//!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
//!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
//!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
//!>!                                                                                                                                   !<!
//!>!                           You should have received a copy of the GNU General Public License along with                            !<!
//!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  3 ---> Thirdparty libraries                                                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
//!>!                                                                                                                                   !<!
//!>!                             --> METIS 5.1.0                                                                                       !<! 
//!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
//!>!                                                                                                                                   !<!
//!>!                             --> Lib_VTK_IO                                                                                        !<!
//!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
//!>!                                                                                                                                   !<!
//!>!                             --> INTERP_LINEAR                                                                                     !<!
//!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
//!>!                                                                                                                                   !<!
//!>!                             --> FLASProc                                                                                          !<!
//!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
//!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
//!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                             --> EXODUS II                                                                                         !<!
//!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
//!>!                                                                                                                                   !<!
//!>!                             --> NETCDF                                                                                            !<!
//!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                             --> HDF5                                                                                              !<!
//!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
//!>!                                                                                                                                   !<!
//!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
//!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
//!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
//!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
//!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
//!>!                           Computers & Structures, 245, 106459.                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
//!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
//!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
//!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
//!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
//!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
//!>!                                                                                                                                   !<!
//!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
//!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
//!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
//!>!                           Journal International, 201(1), 90-111.                                                                  !<!
//!>!                                                                                                                                   !<!
//!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
//!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
//!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
//!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
//!>!                                                                                                                                   !<!
//!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
//!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
//!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
//!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
//!>!                                                                                                                                   !<!
//!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
//!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
//!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
//!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
//!>!                           170(1), 43-64.                                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
//!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
//!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                  5 ---> Enjoy !                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!===================================================================================================================================!<!

/** \file cifo4-avx-local.c
 *   \brief This file contains a C subroutine to compute the internal forces using SIMD AVX paradigm.
 */


#include <immintrin.h>

#include <stdio.h>
#include <stdint.h>


#define IDX2( m, l ) ( 5 * l + m )
#define IDX3( m, l, k ) ( 25 * k + 5 * l + m )
#define IDX4( m, l, k, iel ) ( 125 * (iel) + 25 * (k) + 5 * (l) + (m) )


void compute_internal_forces_order4(
             int32_t * p_elt_start,
				     int32_t * p_elt_end,

				     float ** p_rg_gll_displacement,
				     float ** p_rg_gll_lagrange_deriv,

				     float ** p_rg_hexa_gll_dxidx,
				     float ** p_rg_hexa_gll_dxidy,
				     float ** p_rg_hexa_gll_dxidz,
				     float ** p_rg_hexa_gll_detdx,
				     float ** p_rg_hexa_gll_detdy,
				     float ** p_rg_hexa_gll_detdz,
				     float ** p_rg_hexa_gll_dzedx,
				     float ** p_rg_hexa_gll_dzedy,
				     float ** p_rg_hexa_gll_dzedz,

				     float ** p_rg_hexa_gll_jacobian_det,

				     float ** p_rg_gll_weight,

				     float ** p_rg_hexa_gll_rhovp2,
				     float ** p_rg_hexa_gll_rhovs2,

				     float ** p_rg_gll_acceleration,

				     int ** p_ig_hexa_gll_glonum
				     )
{
  int32_t elt_start = (*p_elt_start) - 1;
  int32_t elt_end = (*p_elt_end) - 1;

  float * rg_gll_displacement = *p_rg_gll_displacement;
  float * rg_gll_lagrange_deriv = *p_rg_gll_lagrange_deriv;

  float * rg_hexa_gll_dxidx = *p_rg_hexa_gll_dxidx;
  float * rg_hexa_gll_dxidy = *p_rg_hexa_gll_dxidy;
  float * rg_hexa_gll_dxidz = *p_rg_hexa_gll_dxidz;
  float * rg_hexa_gll_detdx = *p_rg_hexa_gll_detdx;
  float * rg_hexa_gll_detdy = *p_rg_hexa_gll_detdy;
  float * rg_hexa_gll_detdz = *p_rg_hexa_gll_detdz;
  float * rg_hexa_gll_dzedx = *p_rg_hexa_gll_dzedx;
  float * rg_hexa_gll_dzedy = *p_rg_hexa_gll_dzedy;
  float * rg_hexa_gll_dzedz = *p_rg_hexa_gll_dzedz;

  float * rg_hexa_gll_jacobian_det = *p_rg_hexa_gll_jacobian_det;

  float * rg_gll_weight = *p_rg_gll_weight;

  float * rg_hexa_gll_rhovp2 = *p_rg_hexa_gll_rhovp2;
  float * rg_hexa_gll_rhovs2 = *p_rg_hexa_gll_rhovs2;

  float * rg_gll_acceleration = *p_rg_gll_acceleration;

  int * ig_hexa_gll_glonum = *p_ig_hexa_gll_glonum;

  __m512 rl_displacement_gll[5*5*5*3];

  __m512 local[ 5 * 5 * 5 * 9 ];

  __m512 * intpx1 = &local[    0 ];
  __m512 * intpy1 = &local[  125 ];
  __m512 * intpz1 = &local[  250 ];

  __m512 * intpx2 = &local[  375 ];
  __m512 * intpy2 = &local[  500 ];
  __m512 * intpz2 = &local[  625 ];

  __m512 * intpx3 = &local[  750 ];
  __m512 * intpy3 = &local[  875 ];
  __m512 * intpz3 = &local[ 1000 ];

  int32_t elt_16_start = (elt_start % 16 == 0) ? elt_start : (elt_start/16+1)*16;
  int32_t elt_16_end = (elt_end+1)/16 * 16 /*-1*/;


  // if( elt_start % 8 )
  // {
  //
  //   for( size_t k = 0 ; k < 5 ; ++k )
  //   {
  //     for( size_t l = 0 ; l < 5 ; ++l )
  //     {
  //       for( size_t m = 0 ; m < 5 ; ++m )
  //       {
	//   for( int32_t i = 0 ; i < elt_8_start - elt_start ; ++i )
	//   {
	//     int32_t igll = 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, (elt_start + i) ) ] - 1 );
  //
	//     rl_displacement_gll[ 0 + 3 * IDX3( m, l, k ) ][i] = rg_gll_displacement[ 0 + igll ];
	//     rl_displacement_gll[ 1 + 3 * IDX3( m, l, k ) ][i] = rg_gll_displacement[ 1 + igll ];
	//     rl_displacement_gll[ 2 + 3 * IDX3( m, l, k ) ][i] = rg_gll_displacement[ 2 + igll ];
	//   }
  //       }
  //     }
  //   }
  //
  //
  //   for( int32_t k = 0 ; k < 5 ; ++k )
  //   {
  //     for( int32_t l = 0 ; l < 5 ; ++l )
  //     {
  //       for( int32_t m = 0 ; m < 5 ; ++m )
  //       {
  //         __m512 coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 0, m ) ] );
  //
  //         unsigned int index = 0 + 3 * IDX3( 0, l, k );
  //
  //         __m512 duxdxi = rl_displacement_gll[ 0 + index ] * coeff;
  //         __m512 duydxi = rl_displacement_gll[ 1 + index ] * coeff;
  //         __m512 duzdxi = rl_displacement_gll[ 2 + index ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 1, m ) ] );
  //
  //         duxdxi += rl_displacement_gll[ 3 + index ] * coeff;
  //         duydxi += rl_displacement_gll[ 4 + index ] * coeff;
  //         duzdxi += rl_displacement_gll[ 5 + index ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 2, m ) ] );
  //
  //         duxdxi += rl_displacement_gll[ 6 + index ] * coeff;
  //         duydxi += rl_displacement_gll[ 7 + index ] * coeff;
  //         duzdxi += rl_displacement_gll[ 8 + index ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 3, m ) ] );
  //
  //         duxdxi += rl_displacement_gll[  9 + index ] * coeff;
  //         duydxi += rl_displacement_gll[ 10 + index ] * coeff;
  //         duzdxi += rl_displacement_gll[ 11 + index ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 4, m ) ] );
  //
  //         duxdxi += rl_displacement_gll[ 12 + index ] * coeff;
  //         duydxi += rl_displacement_gll[ 13 + index ] * coeff;
  //         duzdxi += rl_displacement_gll[ 14 + index ] * coeff;
  //
  //         //
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 0, l ) ] );
  //
  //         __m512 duxdet = rl_displacement_gll[ 0 + 3 * IDX3( m, 0, k ) ] * coeff;
  //         __m512 duydet = rl_displacement_gll[ 1 + 3 * IDX3( m, 0, k ) ] * coeff;
  //         __m512 duzdet = rl_displacement_gll[ 2 + 3 * IDX3( m, 0, k ) ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 1, l ) ] );
  //
  //         duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 1, k ) ] * coeff;
  //         duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 1, k ) ] * coeff;
  //         duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 1, k ) ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 2, l ) ] );
  //
  //         duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 2, k ) ] * coeff;
  //         duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 2, k ) ] * coeff;
  //         duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 2, k ) ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 3, l ) ] );
  //
  //         duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 3, k ) ] * coeff;
  //         duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 3, k ) ] * coeff;
  //         duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 3, k ) ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 4, l ) ] );
  //
  //         duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 4, k ) ] * coeff;
  //         duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 4, k ) ] * coeff;
  //         duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 4, k ) ] * coeff;
  //
  //         //
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 0, k ) ] );
  //
  //         __m512 duxdze = rl_displacement_gll[ 0 + 3 * IDX3( m, l, 0 ) ] * coeff;
  //         __m512 duydze = rl_displacement_gll[ 1 + 3 * IDX3( m, l, 0 ) ] * coeff;
  //         __m512 duzdze = rl_displacement_gll[ 2 + 3 * IDX3( m, l, 0 ) ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 1, k ) ] );
  //
  //         duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 1 ) ] * coeff;
  //         duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 1 ) ] * coeff;
  //         duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 1 ) ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 2, k ) ] );
  //
  //         duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 2 ) ] * coeff;
  //         duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 2 ) ] * coeff;
  //         duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 2 ) ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 3, k ) ] );
  //
  //         duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 3 ) ] * coeff;
  //         duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 3 ) ] * coeff;
  //         duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 3 ) ] * coeff;
  //
  //         coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 4, k ) ] );
  //
  //         duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 4 ) ] * coeff;
  //         duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 4 ) ] * coeff;
  //         duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 4 ) ] * coeff;
  //
  //         //
	//   __m512 dxidx;
	//   __m512 detdx;
	//   __m512 dzedx;
  //
	//   for( int32_t i = 0 ; i < elt_8_start - elt_start ; ++i )
	//   {
	//     dxidx[ i ] = rg_hexa_gll_dxidx[ IDX4( m, l, k, elt_start + i ) ];
	//     detdx[ i ] = rg_hexa_gll_detdx[ IDX4( m, l, k, elt_start + i ) ];
	//     dzedx[ i ] = rg_hexa_gll_dzedx[ IDX4( m, l, k, elt_start + i ) ];
	//   }
  //
  //         __m512 duxdx = duxdxi * dxidx + duxdet * detdx + duxdze * dzedx;
  //         __m512 duydx = duydxi * dxidx + duydet * detdx + duydze * dzedx;
  //         __m512 duzdx = duzdxi * dxidx + duzdet * detdx + duzdze * dzedx;
  //
	//   __m512 dxidy;
	//   __m512 detdy;
	//   __m512 dzedy;
  //
	//   for( int32_t i = 0 ; i < elt_8_start - elt_start ; ++i )
	//   {
	//     dxidy[ i ] = rg_hexa_gll_dxidy[ IDX4( m, l, k, elt_start + i ) ];
	//     detdy[ i ] = rg_hexa_gll_detdy[ IDX4( m, l, k, elt_start + i ) ];
	//     dzedy[ i ] = rg_hexa_gll_dzedy[ IDX4( m, l, k, elt_start + i ) ];
	//   }
  //
  //         __m512 duxdy = duxdxi * dxidy + duxdet * detdy + duxdze * dzedy;
  //         __m512 duydy = duydxi * dxidy + duydet * detdy + duydze * dzedy;
  //         __m512 duzdy = duzdxi * dxidy + duzdet * detdy + duzdze * dzedy;
  //
	//   __m512 dxidz;
	//   __m512 detdz;
	//   __m512 dzedz;
  //
	//   for( int32_t i = 0 ; i < elt_8_start - elt_start ; ++i )
	//   {
	//     dxidz[ i ] = rg_hexa_gll_dxidz[ IDX4( m, l, k, elt_start + i ) ];
	//     detdz[ i ] = rg_hexa_gll_detdz[ IDX4( m, l, k, elt_start + i ) ];
	//     dzedz[ i ] = rg_hexa_gll_dzedz[ IDX4( m, l, k, elt_start + i ) ];
	//   }
  //
  //         __m512 duxdz = duxdxi * dxidz + duxdet * detdz + duxdze * dzedz;
  //         __m512 duydz = duydxi * dxidz + duydet * detdz + duydze * dzedz;
  //         __m512 duzdz = duzdxi * dxidz + duzdet * detdz + duzdze * dzedz;
  //
	//   __m512 rhovp2, rhovs2;
  //
	//   for( int32_t i = 0 ; i < elt_8_start - elt_start ; ++i )
	//   {
	//     rhovp2[ i ] = rg_hexa_gll_rhovp2[ IDX4( m, l, k, elt_start + i ) ];
	//     rhovs2[ i ] = rg_hexa_gll_rhovs2[ IDX4( m, l, k, elt_start + i ) ];
	//   }
  //
	//   __m512 two = _mm512_set1_ps(2.0);
  //
  //         __m512 trace_tau = ( rhovp2 - two * rhovs2 )*(duxdx+duydy+duzdz);
  //         __m512 tauxx     = trace_tau + two*rhovs2*duxdx;
  //         __m512 tauyy     = trace_tau + two*rhovs2*duydy;
  //         __m512 tauzz     = trace_tau + two*rhovs2*duzdz;
  //         __m512 tauxy     =                 rhovs2*(duxdy+duydx);
  //         __m512 tauxz     =                 rhovs2*(duxdz+duzdx);
  //         __m512 tauyz     =                 rhovs2*(duydz+duzdy);
  //
  //         __m512 tmp;
  //
	//   for( int32_t i = 0 ; i < elt_8_start - elt_start ; ++i )
	//   {
	//     tmp[ i ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, elt_start + i ) ];
	//   }
  //
  //         intpx1[ IDX3( m, l, k ) ] = tmp * (tauxx*dxidx+tauxy*dxidy+tauxz*dxidz);
  //         intpx2[ IDX3( m, l, k ) ] = tmp * (tauxx*detdx+tauxy*detdy+tauxz*detdz);
  //         intpx3[ IDX3( m, l, k ) ] = tmp * (tauxx*dzedx+tauxy*dzedy+tauxz*dzedz);
  //
  //         intpy1[ IDX3( m, l, k ) ] = tmp * (tauxy*dxidx+tauyy*dxidy+tauyz*dxidz);
  //         intpy2[ IDX3( m, l, k ) ] = tmp * (tauxy*detdx+tauyy*detdy+tauyz*detdz);
  //         intpy3[ IDX3( m, l, k ) ] = tmp * (tauxy*dzedx+tauyy*dzedy+tauyz*dzedz);
  //
  //         intpz1[ IDX3( m, l, k ) ] = tmp * (tauxz*dxidx+tauyz*dxidy+tauzz*dxidz);
  //         intpz2[ IDX3( m, l, k ) ] = tmp * (tauxz*detdx+tauyz*detdy+tauzz*detdz);
  //         intpz3[ IDX3( m, l, k ) ] = tmp * (tauxz*dzedx+tauyz*dzedy+tauzz*dzedz);
  //       }
  //     }
  //   }
  //
  //   for( int32_t k = 0 ; k < 5 ; ++k )
  //   {
  //     for( int32_t l = 0 ; l < 5 ; ++l )
  //     {
  //       for( int32_t m = 0 ; m < 5 ; ++m )
  //       {
  //         __m512 fac1 = _mm512_set1_ps( rg_gll_weight[ l ] ) * _mm512_set1_ps( rg_gll_weight[ k ] );
  //         __m512 fac2 = _mm512_set1_ps( rg_gll_weight[ m ] ) * _mm512_set1_ps( rg_gll_weight[ k ] );
  //         __m512 fac3 = _mm512_set1_ps( rg_gll_weight[ m ] ) * _mm512_set1_ps( rg_gll_weight[ l ] );
  //
  //         __m512 tmpx1 = intpx1[ IDX3( 0, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
  //                    + intpx1[ IDX3( 1, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
  //                    + intpx1[ IDX3( 2, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
  //                    + intpx1[ IDX3( 3, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
  //                    + intpx1[ IDX3( 4, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );
  //
  //         __m512 tmpy1 = intpy1[ IDX3( 0, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
  //                    + intpy1[ IDX3( 1, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
  //                    + intpy1[ IDX3( 2, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
  //                    + intpy1[ IDX3( 3, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
  //                    + intpy1[ IDX3( 4, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );
  //
  //         __m512 tmpz1 = intpz1[ IDX3( 0, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
  //                    + intpz1[ IDX3( 1, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
  //                    + intpz1[ IDX3( 2, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
  //                    + intpz1[ IDX3( 3, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
  //                    + intpz1[ IDX3( 4, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );
  //
  //         __m512 tmpx2 = intpx2[ IDX3( m, 0, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
  //                    + intpx2[ IDX3( m, 1, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
  //                    + intpx2[ IDX3( m, 2, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
  //                    + intpx2[ IDX3( m, 3, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
  //                    + intpx2[ IDX3( m, 4, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );
  //
  //         __m512 tmpy2 = intpy2[ IDX3( m, 0, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
  //                    + intpy2[ IDX3( m, 1, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
  //                    + intpy2[ IDX3( m, 2, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
  //                    + intpy2[ IDX3( m, 3, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
  //                    + intpy2[ IDX3( m, 4, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );
  //
  //         __m512 tmpz2 = intpz2[ IDX3( m, 0, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
  //                    + intpz2[ IDX3( m, 1, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
  //                    + intpz2[ IDX3( m, 2, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
  //                    + intpz2[ IDX3( m, 3, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
  //                    + intpz2[ IDX3( m, 4, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );
  //
  //         __m512 tmpx3 = intpx3[ IDX3( m, l, 0 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
  //                    + intpx3[ IDX3( m, l, 1 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
  //                    + intpx3[ IDX3( m, l, 2 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
  //                    + intpx3[ IDX3( m, l, 3 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
  //                    + intpx3[ IDX3( m, l, 4 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );
  //
  //         __m512 tmpy3 = intpy3[ IDX3( m, l, 0 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
  //                    + intpy3[ IDX3( m, l, 1 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
  //                    + intpy3[ IDX3( m, l, 2 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
  //                    + intpy3[ IDX3( m, l, 3 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
  //                    + intpy3[ IDX3( m, l, 4 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );
  //
  //         __m512 tmpz3 = intpz3[ IDX3( m, l, 0 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
  //                    + intpz3[ IDX3( m, l, 1 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
  //                    + intpz3[ IDX3( m, l, 2 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
  //                    + intpz3[ IDX3( m, l, 3 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
  //                    + intpz3[ IDX3( m, l, 4 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );
  //
  //         __m512 rx = fac1 * tmpx1 + fac2 * tmpx2 + fac3 * tmpx3;
  //         __m512 ry = fac1 * tmpy1 + fac2 * tmpy2 + fac3 * tmpy3;
  //         __m512 rz = fac1 * tmpz1 + fac2 * tmpz2 + fac3 * tmpz3;
  //
  //         float tx[ 8 ] __attribute__((aligned(32)));
  //         float ty[ 8 ] __attribute__((aligned(32)));
  //         float tz[ 8 ] __attribute__((aligned(32)));
  //         _mm512_store_ps( tx, rx );
  //         _mm512_store_ps( ty, ry );
  //         _mm512_store_ps( tz, rz );
  //
  //         for( int32_t i = 0 ; i < elt_8_start - elt_start ; ++i )
  //         {
  //           int idx = ig_hexa_gll_glonum[ IDX4( m, l, k, elt_start + i ) ] - 1;
  //
  //           rg_gll_acceleration[ 0 + 3 * idx ] -= tx[ i ];
  //           rg_gll_acceleration[ 1 + 3 * idx ] -= ty[ i ];
  //           rg_gll_acceleration[ 2 + 3 * idx ] -= tz[ i ];
  //         }
  //
  //       }
  //     }
  //   }
  //
  // }
  //
  // /**
  //  * Full SIMD.
  //  */

  for( int32_t iel = elt_16_start ; iel < elt_16_end ; iel+=16 )
  {

    for( int32_t k = 0 ; k < 5 ; ++k )
    {
      for( int32_t l = 0 ; l < 5 ; ++l )
      {
        for( int32_t m = 0 ; m < 5 ; ++m )
        {
          rl_displacement_gll[ 0 + 3 * IDX3( m, l, k ) ] = _mm512_setr_ps( rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  0 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  1 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  2 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  3 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  4 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  5 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  6 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  7 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  8 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  9 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 10 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 11 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 12 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 13 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 14 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 0 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 15 ) ) ] - 1 ) ] );

          rl_displacement_gll[ 1 + 3 * IDX3( m, l, k ) ] = _mm512_setr_ps( rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  0 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  1 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  2 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  3 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  4 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  5 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  6 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  7 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  8 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  9 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 10 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 11 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 12 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 13 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 14 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 1 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 15 ) ) ] - 1 ) ] );

          rl_displacement_gll[ 2 + 3 * IDX3( m, l, k ) ] = _mm512_setr_ps( rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  0 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  1 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  2 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  3 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  4 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  5 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  6 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  7 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  8 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel +  9 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 10 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 11 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 12 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 13 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 14 ) ) ] - 1 ) ]
                                                                         , rg_gll_displacement[ 2 + 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, ( iel + 15 ) ) ] - 1 ) ] );
        }
      }
    }

    for( int32_t k = 0 ; k < 5 ; ++k )
    {
      for( int32_t l = 0 ; l < 5 ; ++l )
      {
        for( int32_t m = 0 ; m < 5 ; ++m )
        {
          __m512 coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 0, m ) ] );

          unsigned int index = 0 + 3 * IDX3( 0, l, k );

          __m512 duxdxi = rl_displacement_gll[ 0 + index ] * coeff;
          __m512 duydxi = rl_displacement_gll[ 1 + index ] * coeff;
          __m512 duzdxi = rl_displacement_gll[ 2 + index ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 1, m ) ] );

          duxdxi += rl_displacement_gll[ 3 + index ] * coeff;
          duydxi += rl_displacement_gll[ 4 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 5 + index ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 2, m ) ] );

          duxdxi += rl_displacement_gll[ 6 + index ] * coeff;
          duydxi += rl_displacement_gll[ 7 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 8 + index ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 3, m ) ] );

          duxdxi += rl_displacement_gll[  9 + index ] * coeff;
          duydxi += rl_displacement_gll[ 10 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 11 + index ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 4, m ) ] );

          duxdxi += rl_displacement_gll[ 12 + index ] * coeff;
          duydxi += rl_displacement_gll[ 13 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 14 + index ] * coeff;

          //

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 0, l ) ] );

          __m512 duxdet = rl_displacement_gll[ 0 + 3 * IDX3( m, 0, k ) ] * coeff;
          __m512 duydet = rl_displacement_gll[ 1 + 3 * IDX3( m, 0, k ) ] * coeff;
          __m512 duzdet = rl_displacement_gll[ 2 + 3 * IDX3( m, 0, k ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 1, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 1, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 1, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 1, k ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 2, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 2, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 2, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 2, k ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 3, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 3, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 3, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 3, k ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 4, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 4, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 4, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 4, k ) ] * coeff;

          //

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 0, k ) ] );

          __m512 duxdze = rl_displacement_gll[ 0 + 3 * IDX3( m, l, 0 ) ] * coeff;
          __m512 duydze = rl_displacement_gll[ 1 + 3 * IDX3( m, l, 0 ) ] * coeff;
          __m512 duzdze = rl_displacement_gll[ 2 + 3 * IDX3( m, l, 0 ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 1, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 1 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 1 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 1 ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 2, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 2 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 2 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 2 ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 3, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 3 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 3 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 3 ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 4, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 4 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 4 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 4 ) ] * coeff;

          //
          __m512 dxidx = _mm512_setr_ps( rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel +  0 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel +  1 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel +  2 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel +  3 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel +  4 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel +  5 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel +  6 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel +  7 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel +  8 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel +  9 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel + 10 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel + 11 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel + 12 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel + 13 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel + 14 ) ) ]
                                     , rg_hexa_gll_dxidx[ IDX4( m, l, k, ( iel + 15 ) ) ] );
          __m512 detdx = _mm512_setr_ps( rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 0 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 1 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 2 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 3 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 4 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 5 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 6 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 7 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel +  8 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel +  9 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 10 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 11 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 12 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 13 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 14 ) ) ]
                                     , rg_hexa_gll_detdx[ IDX4( m, l, k, ( iel + 15 ) ) ] );
          __m512 dzedx = _mm512_setr_ps( rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 0 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 1 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 2 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 3 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 4 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 5 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 6 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 7 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel +  8 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel +  9 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 10 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 11 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 12 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 13 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 14 ) ) ]
                                     , rg_hexa_gll_dzedx[ IDX4( m, l, k, ( iel + 15 ) ) ] );

          __m512 duxdx = duxdxi * dxidx + duxdet * detdx + duxdze * dzedx;
          __m512 duydx = duydxi * dxidx + duydet * detdx + duydze * dzedx;
          __m512 duzdx = duzdxi * dxidx + duzdet * detdx + duzdze * dzedx;

          __m512 dxidy = _mm512_setr_ps( rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel +  0 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel +  1 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel +  2 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel +  3 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel +  4 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel +  5 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel +  6 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel +  7 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel +  8 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel +  9 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel + 10 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel + 11 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel + 12 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel + 13 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel + 14 ) ) ]
                                     , rg_hexa_gll_dxidy[ IDX4( m, l, k, ( iel + 15 ) ) ] );
          __m512 detdy = _mm512_setr_ps( rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel +  0 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel +  1 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel +  2 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel +  3 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel +  4 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel +  5 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel +  6 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel +  7 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel +  8 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel +  9 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel + 10 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel + 11 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel + 12 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel + 13 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel + 14 ) ) ]
                                     , rg_hexa_gll_detdy[ IDX4( m, l, k, ( iel + 15 ) ) ] );
          __m512 dzedy = _mm512_setr_ps( rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel +  0 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel +  1 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel +  2 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel +  3 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel +  4 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel +  5 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel +  6 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel +  7 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel +  8 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel +  9 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel + 10 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel + 11 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel + 12 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel + 13 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel + 14 ) ) ]
                                     , rg_hexa_gll_dzedy[ IDX4( m, l, k, ( iel + 15 ) ) ] );

          __m512 duxdy = duxdxi * dxidy + duxdet * detdy + duxdze * dzedy;
          __m512 duydy = duydxi * dxidy + duydet * detdy + duydze * dzedy;
          __m512 duzdy = duzdxi * dxidy + duzdet * detdy + duzdze * dzedy;

          __m512 dxidz = _mm512_setr_ps( rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel +  0 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel +  1 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel +  2 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel +  3 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel +  4 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel +  5 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel +  6 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel +  7 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel +  8 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel +  9 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel + 10 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel + 11 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel + 12 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel + 13 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel + 14 ) ) ]
                                     , rg_hexa_gll_dxidz[ IDX4( m, l, k, ( iel + 15 ) ) ] );
          __m512 detdz = _mm512_setr_ps( rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel +  0 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel +  1 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel +  2 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel +  3 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel +  4 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel +  5 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel +  6 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel +  7 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel +  8 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel +  9 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel + 10 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel + 11 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel + 12 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel + 13 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel + 14 ) ) ]
                                     , rg_hexa_gll_detdz[ IDX4( m, l, k, ( iel + 15 ) ) ] );
          __m512 dzedz = _mm512_setr_ps( rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel +  0 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel +  1 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel +  2 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel +  3 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel +  4 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel +  5 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel +  6 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel +  7 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel +  8 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel +  9 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel + 10 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel + 11 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel + 12 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel + 13 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel + 14 ) ) ]
                                     , rg_hexa_gll_dzedz[ IDX4( m, l, k, ( iel + 15 ) ) ] );

          __m512 duxdz = duxdxi * dxidz + duxdet * detdz + duxdze * dzedz;
          __m512 duydz = duydxi * dxidz + duydet * detdz + duydze * dzedz;
          __m512 duzdz = duzdxi * dxidz + duzdet * detdz + duzdze * dzedz;

          __m512 rhovp2 = _mm512_setr_ps( rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel +  0 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel +  1 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel +  2 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel +  3 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel +  4 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel +  5 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel +  6 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel +  7 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel +  8 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel +  9 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel + 10 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel + 11 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel + 12 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel + 13 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel + 14 ) ]
                                      , rg_hexa_gll_rhovp2[ IDX4( m, l, k, iel + 15 ) ] );

          __m512 rhovs2 = _mm512_setr_ps( rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel +  0 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel +  1 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel +  2 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel +  3 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel +  4 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel +  5 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel +  6 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel +  7 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel +  8 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel +  9 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel + 10 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel + 11 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel + 12 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel + 13 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel + 14 ) ]
                                      , rg_hexa_gll_rhovs2[ IDX4( m, l, k, iel + 15 ) ] );

	  __m512 two = _mm512_set1_ps(2.0);

          __m512 trace_tau = ( rhovp2 - two * rhovs2 )*(duxdx+duydy+duzdz);
          __m512 tauxx     = trace_tau + two*rhovs2*duxdx;
          __m512 tauyy     = trace_tau + two*rhovs2*duydy;
          __m512 tauzz     = trace_tau + two*rhovs2*duzdz;
          __m512 tauxy     =                 rhovs2*(duxdy+duydx);
          __m512 tauxz     =                 rhovs2*(duxdz+duzdx);
          __m512 tauyz     =                 rhovs2*(duydz+duzdy);

          __m512 tmp = _mm512_setr_ps( rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel +  0 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel +  1 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel +  2 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel +  3 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel +  4 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel +  5 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel +  6 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel +  7 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel +  8 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel +  9 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel + 10 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel + 11 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel + 12 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel + 13 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel + 14 ) ]
                                   , rg_hexa_gll_jacobian_det[ IDX4( m, l, k, iel + 15 ) ] );

          intpx1[ IDX3( m, l, k ) ] = tmp * (tauxx*dxidx+tauxy*dxidy+tauxz*dxidz);
          intpx2[ IDX3( m, l, k ) ] = tmp * (tauxx*detdx+tauxy*detdy+tauxz*detdz);
          intpx3[ IDX3( m, l, k ) ] = tmp * (tauxx*dzedx+tauxy*dzedy+tauxz*dzedz);

          intpy1[ IDX3( m, l, k ) ] = tmp * (tauxy*dxidx+tauyy*dxidy+tauyz*dxidz);
          intpy2[ IDX3( m, l, k ) ] = tmp * (tauxy*detdx+tauyy*detdy+tauyz*detdz);
          intpy3[ IDX3( m, l, k ) ] = tmp * (tauxy*dzedx+tauyy*dzedy+tauyz*dzedz);

          intpz1[ IDX3( m, l, k ) ] = tmp * (tauxz*dxidx+tauyz*dxidy+tauzz*dxidz);
          intpz2[ IDX3( m, l, k ) ] = tmp * (tauxz*detdx+tauyz*detdy+tauzz*detdz);
          intpz3[ IDX3( m, l, k ) ] = tmp * (tauxz*dzedx+tauyz*dzedy+tauzz*dzedz);
        }
      }
    }

    for( int32_t k = 0 ; k < 5 ; ++k )
    {
      for( int32_t l = 0 ; l < 5 ; ++l )
      {
        for( int32_t m = 0 ; m < 5 ; ++m )
        {
          __m512 fac1 = _mm512_set1_ps( rg_gll_weight[ l ] ) * _mm512_set1_ps( rg_gll_weight[ k ] );
          __m512 fac2 = _mm512_set1_ps( rg_gll_weight[ m ] ) * _mm512_set1_ps( rg_gll_weight[ k ] );
          __m512 fac3 = _mm512_set1_ps( rg_gll_weight[ m ] ) * _mm512_set1_ps( rg_gll_weight[ l ] );

          __m512 tmpx1 = intpx1[ IDX3( 0, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpx1[ IDX3( 1, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpx1[ IDX3( 2, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpx1[ IDX3( 3, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpx1[ IDX3( 4, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpy1 = intpy1[ IDX3( 0, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpy1[ IDX3( 1, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpy1[ IDX3( 2, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpy1[ IDX3( 3, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpy1[ IDX3( 4, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpz1 = intpz1[ IDX3( 0, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpz1[ IDX3( 1, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpz1[ IDX3( 2, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpz1[ IDX3( 3, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpz1[ IDX3( 4, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpx2 = intpx2[ IDX3( m, 0, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpx2[ IDX3( m, 1, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpx2[ IDX3( m, 2, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpx2[ IDX3( m, 3, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpx2[ IDX3( m, 4, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpy2 = intpy2[ IDX3( m, 0, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpy2[ IDX3( m, 1, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpy2[ IDX3( m, 2, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpy2[ IDX3( m, 3, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpy2[ IDX3( m, 4, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpz2 = intpz2[ IDX3( m, 0, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpz2[ IDX3( m, 1, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpz2[ IDX3( m, 2, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpz2[ IDX3( m, 3, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpz2[ IDX3( m, 4, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpx3 = intpx3[ IDX3( m, l, 0 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpx3[ IDX3( m, l, 1 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpx3[ IDX3( m, l, 2 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpx3[ IDX3( m, l, 3 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpx3[ IDX3( m, l, 4 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpy3 = intpy3[ IDX3( m, l, 0 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpy3[ IDX3( m, l, 1 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpy3[ IDX3( m, l, 2 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpy3[ IDX3( m, l, 3 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpy3[ IDX3( m, l, 4 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpz3 = intpz3[ IDX3( m, l, 0 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpz3[ IDX3( m, l, 1 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpz3[ IDX3( m, l, 2 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpz3[ IDX3( m, l, 3 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpz3[ IDX3( m, l, 4 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 rx = fac1 * tmpx1 + fac2 * tmpx2 + fac3 * tmpx3;
          __m512 ry = fac1 * tmpy1 + fac2 * tmpy2 + fac3 * tmpy3;
          __m512 rz = fac1 * tmpz1 + fac2 * tmpz2 + fac3 * tmpz3;

          float tx[ 8 ] __attribute__((aligned(32)));
          float ty[ 8 ] __attribute__((aligned(32)));
          float tz[ 8 ] __attribute__((aligned(32)));
          _mm512_store_ps( tx, rx );
          _mm512_store_ps( ty, ry );
          _mm512_store_ps( tz, rz );

          for( int32_t i = 0 ; i < 16 ; ++i )
          {
            int idx = ig_hexa_gll_glonum[ IDX4( m, l, k, iel + i ) ] - 1;

            rg_gll_acceleration[ 0 + 3 * idx ] -= tx[ i ];
            rg_gll_acceleration[ 1 + 3 * idx ] -= ty[ i ];
            rg_gll_acceleration[ 2 + 3 * idx ] -= tz[ i ];
          }
        }
      }
    }
  }

  // Remaining elements

  if( elt_16_end < elt_end )
  {

    for( size_t k = 0 ; k < 5 ; ++k )
    {
      for( size_t l = 0 ; l < 5 ; ++l )
      {
        for( size_t m = 0 ; m < 5 ; ++m )
        {
	  for( int32_t i = 0 ; i <= elt_end - elt_16_end ; ++i )
	  {
	    int32_t igll = 3 * ( ig_hexa_gll_glonum[ IDX4( m, l, k, elt_16_end + i ) ] - 1 );

	    rl_displacement_gll[ 0 + 3 * IDX3( m, l, k ) ][i] = rg_gll_displacement[ 0 + igll ];
	    rl_displacement_gll[ 1 + 3 * IDX3( m, l, k ) ][i] = rg_gll_displacement[ 1 + igll ];
	    rl_displacement_gll[ 2 + 3 * IDX3( m, l, k ) ][i] = rg_gll_displacement[ 2 + igll ];
	  }
        }
      }
    }

    for( int32_t k = 0 ; k < 5 ; ++k )
    {
      for( int32_t l = 0 ; l < 5 ; ++l )
      {
        for( int32_t m = 0 ; m < 5 ; ++m )
        {
          __m512 coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 0, m ) ] );

          unsigned int index = 0 + 3 * IDX3( 0, l, k );

          __m512 duxdxi = rl_displacement_gll[ 0 + index ] * coeff;
          __m512 duydxi = rl_displacement_gll[ 1 + index ] * coeff;
          __m512 duzdxi = rl_displacement_gll[ 2 + index ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 1, m ) ] );

          duxdxi += rl_displacement_gll[ 3 + index ] * coeff;
          duydxi += rl_displacement_gll[ 4 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 5 + index ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 2, m ) ] );

          duxdxi += rl_displacement_gll[ 6 + index ] * coeff;
          duydxi += rl_displacement_gll[ 7 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 8 + index ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 3, m ) ] );

          duxdxi += rl_displacement_gll[  9 + index ] * coeff;
          duydxi += rl_displacement_gll[ 10 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 11 + index ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 4, m ) ] );

          duxdxi += rl_displacement_gll[ 12 + index ] * coeff;
          duydxi += rl_displacement_gll[ 13 + index ] * coeff;
          duzdxi += rl_displacement_gll[ 14 + index ] * coeff;

          //

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 0, l ) ] );

          __m512 duxdet = rl_displacement_gll[ 0 + 3 * IDX3( m, 0, k ) ] * coeff;
          __m512 duydet = rl_displacement_gll[ 1 + 3 * IDX3( m, 0, k ) ] * coeff;
          __m512 duzdet = rl_displacement_gll[ 2 + 3 * IDX3( m, 0, k ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 1, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 1, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 1, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 1, k ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 2, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 2, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 2, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 2, k ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 3, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 3, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 3, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 3, k ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 4, l ) ] );

          duxdet += rl_displacement_gll[ 0 + 3 * IDX3( m, 4, k ) ] * coeff;
          duydet += rl_displacement_gll[ 1 + 3 * IDX3( m, 4, k ) ] * coeff;
          duzdet += rl_displacement_gll[ 2 + 3 * IDX3( m, 4, k ) ] * coeff;

          //

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 0, k ) ] );

          __m512 duxdze = rl_displacement_gll[ 0 + 3 * IDX3( m, l, 0 ) ] * coeff;
          __m512 duydze = rl_displacement_gll[ 1 + 3 * IDX3( m, l, 0 ) ] * coeff;
          __m512 duzdze = rl_displacement_gll[ 2 + 3 * IDX3( m, l, 0 ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 1, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 1 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 1 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 1 ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 2, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 2 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 2 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 2 ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 3, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 3 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 3 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 3 ) ] * coeff;

          coeff = _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( 4, k ) ] );

          duxdze += rl_displacement_gll[ 0 + 3 * IDX3( m, l, 4 ) ] * coeff;
          duydze += rl_displacement_gll[ 1 + 3 * IDX3( m, l, 4 ) ] * coeff;
          duzdze += rl_displacement_gll[ 2 + 3 * IDX3( m, l, 4 ) ] * coeff;

          //
	  __m512 dxidx;
	  __m512 detdx;
	  __m512 dzedx;

	  for( int32_t i = 0 ; i <= elt_end - elt_16_end ; ++i )
	  {
	    dxidx[ i ] = rg_hexa_gll_dxidx[ IDX4( m, l, k, elt_16_end + i ) ];
	    detdx[ i ] = rg_hexa_gll_detdx[ IDX4( m, l, k, elt_16_end + i ) ];
	    dzedx[ i ] = rg_hexa_gll_dzedx[ IDX4( m, l, k, elt_16_end + i ) ];
	  }

          __m512 duxdx = duxdxi * dxidx + duxdet * detdx + duxdze * dzedx;
          __m512 duydx = duydxi * dxidx + duydet * detdx + duydze * dzedx;
          __m512 duzdx = duzdxi * dxidx + duzdet * detdx + duzdze * dzedx;

	  __m512 dxidy;
	  __m512 detdy;
	  __m512 dzedy;

	  for( int32_t i = 0 ; i <= elt_end - elt_16_end ; ++i )
	  {
	    dxidy[ i ] = rg_hexa_gll_dxidy[ IDX4( m, l, k, elt_16_end + i ) ];
	    detdy[ i ] = rg_hexa_gll_detdy[ IDX4( m, l, k, elt_16_end + i ) ];
	    dzedy[ i ] = rg_hexa_gll_dzedy[ IDX4( m, l, k, elt_16_end + i ) ];
	  }

          __m512 duxdy = duxdxi * dxidy + duxdet * detdy + duxdze * dzedy;
          __m512 duydy = duydxi * dxidy + duydet * detdy + duydze * dzedy;
          __m512 duzdy = duzdxi * dxidy + duzdet * detdy + duzdze * dzedy;

	  __m512 dxidz;
	  __m512 detdz;
	  __m512 dzedz;

	  for( int32_t i = 0 ; i <= elt_end - elt_16_end ; ++i )
	  {
	    dxidz[ i ] = rg_hexa_gll_dxidz[ IDX4( m, l, k, elt_16_end + i ) ];
	    detdz[ i ] = rg_hexa_gll_detdz[ IDX4( m, l, k, elt_16_end + i ) ];
	    dzedz[ i ] = rg_hexa_gll_dzedz[ IDX4( m, l, k, elt_16_end + i ) ];
	  }

          __m512 duxdz = duxdxi * dxidz + duxdet * detdz + duxdze * dzedz;
          __m512 duydz = duydxi * dxidz + duydet * detdz + duydze * dzedz;
          __m512 duzdz = duzdxi * dxidz + duzdet * detdz + duzdze * dzedz;

	  __m512 rhovp2, rhovs2;

	  for( int32_t i = 0 ; i <= elt_end - elt_16_end ; ++i )
	  {
	    rhovp2[ i ] = rg_hexa_gll_rhovp2[ IDX4( m, l, k, elt_16_end + i ) ];
	    rhovs2[ i ] = rg_hexa_gll_rhovs2[ IDX4( m, l, k, elt_16_end + i ) ];
	  }

	  __m512 two = _mm512_set1_ps(2.0);

          __m512 trace_tau = ( rhovp2 - two * rhovs2 )*(duxdx+duydy+duzdz);
          __m512 tauxx     = trace_tau + two*rhovs2*duxdx;
          __m512 tauyy     = trace_tau + two*rhovs2*duydy;
          __m512 tauzz     = trace_tau + two*rhovs2*duzdz;
          __m512 tauxy     =                 rhovs2*(duxdy+duydx);
          __m512 tauxz     =                 rhovs2*(duxdz+duzdx);
          __m512 tauyz     =                 rhovs2*(duydz+duzdy);

          __m512 tmp;

	  for( int32_t i = 0 ; i <= elt_end - elt_16_end ; ++i )
	  {
	    tmp[ i ] = rg_hexa_gll_jacobian_det[ IDX4( m, l, k, elt_16_end + i ) ];
	  }

          intpx1[ IDX3( m, l, k ) ] = tmp * (tauxx*dxidx+tauxy*dxidy+tauxz*dxidz);
          intpx2[ IDX3( m, l, k ) ] = tmp * (tauxx*detdx+tauxy*detdy+tauxz*detdz);
          intpx3[ IDX3( m, l, k ) ] = tmp * (tauxx*dzedx+tauxy*dzedy+tauxz*dzedz);

          intpy1[ IDX3( m, l, k ) ] = tmp * (tauxy*dxidx+tauyy*dxidy+tauyz*dxidz);
          intpy2[ IDX3( m, l, k ) ] = tmp * (tauxy*detdx+tauyy*detdy+tauyz*detdz);
          intpy3[ IDX3( m, l, k ) ] = tmp * (tauxy*dzedx+tauyy*dzedy+tauyz*dzedz);

          intpz1[ IDX3( m, l, k ) ] = tmp * (tauxz*dxidx+tauyz*dxidy+tauzz*dxidz);
          intpz2[ IDX3( m, l, k ) ] = tmp * (tauxz*detdx+tauyz*detdy+tauzz*detdz);
          intpz3[ IDX3( m, l, k ) ] = tmp * (tauxz*dzedx+tauyz*dzedy+tauzz*dzedz);
        }
      }
    }

    for( int32_t k = 0 ; k < 5 ; ++k )
    {
      for( int32_t l = 0 ; l < 5 ; ++l )
      {
        for( int32_t m = 0 ; m < 5 ; ++m )
        {
          __m512 fac1 = _mm512_set1_ps( rg_gll_weight[ l ] ) * _mm512_set1_ps( rg_gll_weight[ k ] );
          __m512 fac2 = _mm512_set1_ps( rg_gll_weight[ m ] ) * _mm512_set1_ps( rg_gll_weight[ k ] );
          __m512 fac3 = _mm512_set1_ps( rg_gll_weight[ m ] ) * _mm512_set1_ps( rg_gll_weight[ l ] );

          __m512 tmpx1 = intpx1[ IDX3( 0, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpx1[ IDX3( 1, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpx1[ IDX3( 2, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpx1[ IDX3( 3, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpx1[ IDX3( 4, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpy1 = intpy1[ IDX3( 0, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpy1[ IDX3( 1, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpy1[ IDX3( 2, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpy1[ IDX3( 3, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpy1[ IDX3( 4, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpz1 = intpz1[ IDX3( 0, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpz1[ IDX3( 1, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpz1[ IDX3( 2, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpz1[ IDX3( 3, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpz1[ IDX3( 4, l, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( m, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpx2 = intpx2[ IDX3( m, 0, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpx2[ IDX3( m, 1, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpx2[ IDX3( m, 2, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpx2[ IDX3( m, 3, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpx2[ IDX3( m, 4, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpy2 = intpy2[ IDX3( m, 0, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpy2[ IDX3( m, 1, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpy2[ IDX3( m, 2, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpy2[ IDX3( m, 3, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpy2[ IDX3( m, 4, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpz2 = intpz2[ IDX3( m, 0, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpz2[ IDX3( m, 1, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpz2[ IDX3( m, 2, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpz2[ IDX3( m, 3, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpz2[ IDX3( m, 4, k ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( l, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpx3 = intpx3[ IDX3( m, l, 0 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpx3[ IDX3( m, l, 1 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpx3[ IDX3( m, l, 2 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpx3[ IDX3( m, l, 3 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpx3[ IDX3( m, l, 4 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpy3 = intpy3[ IDX3( m, l, 0 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpy3[ IDX3( m, l, 1 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpy3[ IDX3( m, l, 2 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpy3[ IDX3( m, l, 3 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpy3[ IDX3( m, l, 4 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 tmpz3 = intpz3[ IDX3( m, l, 0 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 0 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 0 ] )
                     + intpz3[ IDX3( m, l, 1 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 1 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 1 ] )
                     + intpz3[ IDX3( m, l, 2 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 2 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 2 ] )
                     + intpz3[ IDX3( m, l, 3 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 3 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 3 ] )
                     + intpz3[ IDX3( m, l, 4 ) ] * _mm512_set1_ps( rg_gll_lagrange_deriv[ IDX2( k, 4 ) ] ) * _mm512_set1_ps( rg_gll_weight[ 4 ] );

          __m512 rx = fac1 * tmpx1 + fac2 * tmpx2 + fac3 * tmpx3;
          __m512 ry = fac1 * tmpy1 + fac2 * tmpy2 + fac3 * tmpy3;
          __m512 rz = fac1 * tmpz1 + fac2 * tmpz2 + fac3 * tmpz3;

          float tx[ 8 ] __attribute__((aligned(32)));
          float ty[ 8 ] __attribute__((aligned(32)));
          float tz[ 8 ] __attribute__((aligned(32)));
          _mm512_store_ps( tx, rx );
          _mm512_store_ps( ty, ry );
          _mm512_store_ps( tz, rz );

          for( int32_t i = 0 ; i <= elt_end - elt_16_end ; ++i )
          {
            int idx = ig_hexa_gll_glonum[ IDX4( m, l, k, elt_16_end + i ) ] - 1;

            rg_gll_acceleration[ 0 + 3 * idx ] -= tx[ i ];
            rg_gll_acceleration[ 1 + 3 * idx ] -= ty[ i ];
            rg_gll_acceleration[ 2 + 3 * idx ] -= tz[ i ];
          }
        }
      }
    }

  }

}
