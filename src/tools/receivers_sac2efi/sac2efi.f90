PROGRAM sac2efi

      use mod_precision

      implicit none

      integer(kind=IXP), parameter                   :: TRACE_NMAX = 1000000_IXP

      real   (kind=RXP), dimension(TRACE_NMAX)       :: trace_ns_full
      real   (kind=RXP), dimension(TRACE_NMAX)       :: trace_ew_full
      real   (kind=RXP), dimension(TRACE_NMAX)       :: trace_ud_full

      real   (kind=RXP), dimension(:),   allocatable :: time
      real   (kind=RXP), dimension(:),   allocatable :: trace_ns
      real   (kind=RXP), dimension(:),   allocatable :: trace_ew
      real   (kind=RXP), dimension(:),   allocatable :: trace_ud

      real   (kind=RXP)                              :: trace_ns_beg
      real   (kind=RXP)                              :: trace_ew_beg
      real   (kind=RXP)                              :: trace_ud_beg
      real   (kind=RXP)                              :: trace_ns_dt
      real   (kind=RXP)                              :: trace_ew_dt
      real   (kind=RXP)                              :: trace_ud_dt
      real   (kind=RXP)                              :: dt

      integer(kind=IXP)                              :: trace_ns_ndata
      integer(kind=IXP)                              :: trace_ew_ndata
      integer(kind=IXP)                              :: trace_ud_ndata
      integer(kind=IXP)                              :: ndata

      integer(kind=IXP)                              :: i
      integer(kind=IXP)                              :: ios
      integer(kind=IXP)                              :: narg
      integer(kind=IXP)                              :: funit

      character(len=255)                             :: pname
      character(len= 90)                             :: f_trace_ns
      character(len= 90)                             :: f_trace_ew
      character(len= 90)                             :: f_trace_ud


!
!
!*****************************************************************************************************************************
!---->Get command line arguments
!*****************************************************************************************************************************
     
      narg = command_argument_count()
     
      if ( narg /= 3_IXP ) then
     
         call get_command_argument(0,pname)
         
         write(*,'(a)') "Usage : "//trim(adjustl(pname))//" trace.ns.sac trace.ew.sac trace.ud.sac"

         stop "wrong number of argument"
     
      endif
     
      call get_command_argument(1_IXP,f_trace_ns)
      call get_command_argument(2_IXP,f_trace_ew)
      call get_command_argument(3_IXP,f_trace_ud)

      print *,trim(f_trace_ns),trim(f_trace_ew),trim(f_trace_ud)

!
!
!*****************************************************************************************************************************
!---->READ TRACE 
!*****************************************************************************************************************************

      trace_ns_full(:) = ZERO_RXP
      trace_ew_full(:) = ZERO_RXP
      trace_ud_full(:) = ZERO_RXP
   
      call rsac1(f_trace_ns, trace_ns_full, trace_ns_ndata, trace_ns_beg, trace_ns_dt, TRACE_NMAX, ios)
      call rsac1(f_trace_ew, trace_ew_full, trace_ew_ndata, trace_ew_beg, trace_ew_dt, TRACE_NMAX, ios)
      call rsac1(f_trace_ud, trace_ud_full, trace_ud_ndata, trace_ud_beg, trace_ud_dt, TRACE_NMAX, ios)

      if ( trace_ns_ndata == trace_ew_ndata .and. trace_ns_ndata == trace_ud_ndata) then

         ndata = trace_ns_ndata

      else

         write(*,'(a)') "error: different ndata for traces"
         stop

      endif

      dt = trace_ns_dt

      print *,"time step = ",dt

      allocate(time(ndata))
      allocate(trace_ns(ndata))
      allocate(trace_ew(ndata))
      allocate(trace_ud(ndata))

      trace_ns(1_IXP:ndata) = trace_ns_full(1_IXP:ndata)
      trace_ew(1_IXP:ndata) = trace_ew_full(1_IXP:ndata)
      trace_ud(1_IXP:ndata) = trace_ud_full(1_IXP:ndata)

      time(:) = [( dt * (i-1_IXP), i=1_IXP,ndata )]

!
!
!*****************************************************************************************************************************
!---->WRITE TRACE IN EFISPEC3D FORMAT
!*****************************************************************************************************************************

      open(newunit=funit,file=trim(f_trace_ns)//".gpl",status='replace',action='write',access='stream',form='unformatted')

      write(funit) (time(i),trace_ew(i),trace_ns(i),trace_ud(i),trace_ew(i),trace_ns(i),trace_ud(i),trace_ew(i),trace_ns(i),trace_ud(i),i=1_IXP,ndata)
      
      close(funit)

      stop

END PROGRAM
