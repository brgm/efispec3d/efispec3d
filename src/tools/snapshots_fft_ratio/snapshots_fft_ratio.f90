!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

program snapshots_fft_ratio

   use mpi

   use mod_precision
   
   use mod_global_variables, only :&
                                    IG_NGLL&
                                   ,LG_LUSTRE_FILE_SYS&
                                   ,CIL&
                                   ,cg_prefix&
                                   ,cg_lst_ext&
                                   ,cg_iir_filter_output_motion&
                                   ,get_prefix&
                                   ,rg_dt&
                                   ,ig_ndt&
                                   ,ig_ncpu&
                                   ,ig_myrank&
                                   ,ig_mpi_comm_simu&
                                   ,ig_mpi_nboctet_real&
                                   ,ig_nquad_fsurf&
                                   ,ig_hexa_nnode&
                                   ,ig_quad_nnode&
                                   ,rg_simu_total_time

   use mod_efi_mpi          , only :&
                                    init_mpi

   use mod_init_efi         , only :&
                                    init_input_variables&
                                   ,init_gll_nodes

   use mod_init_mesh        , only :&
                                    init_element

   use mod_init_memory

   use mod_io_array

   use mod_signal_processing, only :&
                                    ft&
                                   ,nextpow2&
                                   ,spewin

   use mod_write_listing    , only :&
                                    write_temporal_domain_info

   implicit none

   complex(kind=RXP), dimension(:)        , allocatable :: ux_fft
   complex(kind=RXP), dimension(:)        , allocatable :: uy_fft
   complex(kind=RXP), dimension(:)        , allocatable :: uz_fft
   
   real   (kind=RXP), dimension(:,:,:,:,:), allocatable :: all_quad_gll_xyz_filter
   real   (kind=RXP), dimension(:,:,:)    , allocatable :: all_quad_gll_xfft_max
   real   (kind=RXP), dimension(:,:,:)    , allocatable :: all_quad_gll_yfft_max
   real   (kind=RXP), dimension(:,:,:)    , allocatable :: all_quad_gll_zfft_max
   real   (kind=RXP), dimension(:)        , allocatable :: time
   real   (kind=RXP), dimension(:)        , allocatable :: ux
   real   (kind=RXP), dimension(:)        , allocatable :: uy
   real   (kind=RXP), dimension(:)        , allocatable :: uz
   real   (kind=RXP), dimension(:)        , allocatable :: ux_fft_abs
   real   (kind=RXP), dimension(:)        , allocatable :: uy_fft_abs
   real   (kind=RXP), dimension(:)        , allocatable :: uz_fft_abs
                                                        
   real   (kind=RXP)                                    :: rdum
   real   (kind=RXP)                                    :: df
   real   (kind=RXP)                                    :: win_b
   real   (kind=RXP)                                    :: f
                                                          
   integer(kind=IXP)                                    :: nfft
   integer(kind=IXP)                                    :: nfold
   integer(kind=IXP)                                    :: ndt
   integer(kind=IXP)                                    :: idt
   integer(kind=IXP)                                    :: iquad
   integer(kind=IXP)                                    :: kgll
   integer(kind=IXP)                                    :: lgll
   integer(kind=IXP)                                    :: imax
   integer(kind=IXP)                                    :: nquad_fsurf_all_cpu
   integer(kind=IXP)                                    :: nquad_floor
   integer(kind=IXP)                                    :: iquad_offset
   integer(kind=IXP)                                    :: ios
   integer(kind=IXP)                                    :: myunit
                                                        
   character(len=CIL)                                   :: fname
   character(len=CIL)                                   :: fname_fft

                                               
!
!
!**********************************************************************************************************************
   call init_mpi()
!**********************************************************************************************************************


!
!
!**********************************************************************************************************************
   cg_prefix = get_prefix()

   cg_lst_ext = ".post.snapshots.fft.ratio.lst"

   call init_input_variables(cg_prefix)
!**********************************************************************************************************************


!
!
!*****************************************************************************************************************************
!->init time domain
!*****************************************************************************************************************************

!
!->first pass to count

   ndt = ZERO_IXP

   if (ig_myrank == ZERO_IXP) then

      fname = trim(cg_prefix)//".snapshot.time.dec"

      open(newunit=myunit,file=trim(fname),status='old',action='read',access='stream',form='unformatted',iostat=ios)

      do

         read(myunit,iostat=ios) rdum

         if (ios /= ZERO_IXP) exit

         ndt = ndt + ONE_IXP

      enddo

      close(myunit)

   endif

   ig_ndt = ndt

   call mpi_bcast(ig_ndt,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)

   ios = init_array_real(time,ig_ndt,"snapshots_fft_ratio:time")


!
!->second pass to load in memory

   if (ig_myrank == ZERO_IXP) then

      fname = trim(cg_prefix)//".snapshot.time.dec"

      open(newunit=myunit,file=trim(fname),status='old',action='read',access='stream',form='unformatted',iostat=ios)

      do idt = ONE_IXP,ig_ndt

         read(myunit,iostat=ios) time(idt)

      enddo

      close(myunit)

   endif

   call mpi_bcast(time,ig_ndt,MPI_REAL,ZERO_IXP,ig_mpi_comm_simu,ios)

   rg_dt = abs(time(TWO_IXP) - time(ONE_IXP))

   rg_simu_total_time = time(ig_ndt)

   call write_temporal_domain_info()

!
!
!**********************************************************************************************************************
!->read file containing receivers time series 
!**********************************************************************************************************************

   selectcase(trim(cg_iir_filter_output_motion))

      case("dis")
         fname = trim(cg_prefix)//".snapshot.quadf.gll.uxyz.dec"

      case("vel")
         fname = trim(cg_prefix)//".snapshot.quadf.gll.vxyz.dec"

      case("acc")
         fname = trim(cg_prefix)//".snapshot.quadf.gll.axyz.dec"

   endselect

   call efi_mpi_file_read_at_all(ig_mpi_comm_simu,fname,ig_myrank,ig_ncpu,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS,all_quad_gll_xyz_filter,nquad_fsurf_all_cpu,ig_nquad_fsurf,nquad_floor,iquad_offset)

   ios = init_array_real(all_quad_gll_xfft_max,ig_nquad_fsurf,IG_NGLL,IG_NGLL,"snapshots_fft_ratio:all_quad_gll_xfft_max")

   ios = init_array_real(all_quad_gll_yfft_max,ig_nquad_fsurf,IG_NGLL,IG_NGLL,"snapshots_fft_ratio:all_quad_gll_yfft_max")

   ios = init_array_real(all_quad_gll_zfft_max,ig_nquad_fsurf,IG_NGLL,IG_NGLL,"snapshots_fft_ratio:all_quad_gll_zfft_max")

!**********************************************************************************************************************


!
!
!**********************************************************************************************************************
!->compute FFT for each times series 
!**********************************************************************************************************************

!
!->init FFT into

   nfft = nextpow2(ig_ndt)

   nfold = nfft/2_IXP + 1_IXP

   df = ONE_RXP/(real(nfft,kind=RXP)*rg_dt)

   print *,ig_myrank,rg_dt,df
   
!
!->init arrays containing time series and their FFT

   ios = init_array_real(ux,ig_ndt,"snapshots_fft_ratio:ux")

   ios = init_array_real(uy,ig_ndt,"snapshots_fft_ratio:uy")

   ios = init_array_real(uz,ig_ndt,"snapshots_fft_ratio:uz")

   ios = init_array_complex(ux_fft,nfft,"snapshots_fft_ratio:ux_fft")

   ios = init_array_complex(uy_fft,nfft,"snapshots_fft_ratio:uy_fft")

   ios = init_array_complex(uz_fft,nfft,"snapshots_fft_ratio:uz_fft")

   ios = init_array_real(ux_fft_abs,nfold,"snapshots_fft_ratio:ux_fft_abs")

   ios = init_array_real(uy_fft_abs,nfold,"snapshots_fft_ratio:uy_fft_abs")

   ios = init_array_real(uz_fft_abs,nfold,"snapshots_fft_ratio:uz_fft_abs")

   win_b = 0.5_RXP

   f = 0.0_RXP

!
!->loop on every GLL containing time series and compute FFT

   do iquad = ONE_IXP,ig_nquad_fsurf

      do kgll = ONE_IXP,IG_NGLL

         do lgll = ONE_IXP,IG_NGLL

            ux(:) = all_quad_gll_xyz_filter(:,1_IXP,lgll,kgll,iquad)
            uy(:) = all_quad_gll_xyz_filter(:,2_IXP,lgll,kgll,iquad)
            uz(:) = all_quad_gll_xyz_filter(:,3_IXP,lgll,kgll,iquad)

            if (iquad == 1 .and. kgll == 1 .and. lgll == 1 .and. ig_myrank == 0) write(500,'(E15.7)') uy(:)

            ux_fft(1_IXP:ig_ndt) = cmplx(ux(1_IXP:ig_ndt),ZERO_RXP)
            uy_fft(1_IXP:ig_ndt) = cmplx(uy(1_IXP:ig_ndt),ZERO_RXP) 
            uz_fft(1_IXP:ig_ndt) = cmplx(uz(1_IXP:ig_ndt),ZERO_RXP)

            call ft(ux_fft,-1_IXP)
            call ft(uy_fft,-1_IXP)
            call ft(uz_fft,-1_IXP)

            ux_fft(:) = ux_fft(:)*rg_dt
            uy_fft(:) = uy_fft(:)*rg_dt
            uz_fft(:) = uz_fft(:)*rg_dt


!
!---------->smooth FFT module

            ux_fft_abs(1_IXP:nfold) = abs(ux_fft(1_IXP:nfold))
            uy_fft_abs(1_IXP:nfold) = abs(uy_fft(1_IXP:nfold))
            uz_fft_abs(1_IXP:nfold) = abs(uz_fft(1_IXP:nfold))

            call spewin(ux_fft_abs,nfold,df,win_b)
            call spewin(uy_fft_abs,nfold,df,win_b)
            call spewin(uz_fft_abs,nfold,df,win_b)

            if (iquad == 1 .and. kgll == 1 .and. lgll == 1 .and. ig_myrank == 0) write(600,'(E15.7)') uy_fft_abs(:)
!
!---------->find first max of module of FFT

          !  imax = 10_IXP

          !  do while ( (uy_fft_abs(imax) <= uy_fft_abs(imax+1_IXP)) .and. ( imax < nfold ) )

          !     imax = imax + 1_IXP

          !  enddo

           !f = real(imax-1_IXP,kind=RXP)*df
           !imax = 
            f = uy_fft_abs(int(1.0_RXP/df,kind=IXP))
           !f = 1.0_RXP
           !f = f + 1.0_RXP
   
            all_quad_gll_yfft_max(lgll,kgll,iquad) = f


         enddo

      enddo

   enddo


!
!
!**************************************************************************************************************************
!---->write decimated displacement time histories to disk
!**************************************************************************************************************************

   fname_fft = trim(fname)//".yfft.fundamental.freq"

   call efi_mpi_file_write_at_all(ig_mpi_comm_simu,all_quad_gll_yfft_max,fname_fft,ig_myrank,iquad_offset,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS)

   call mpi_finalize(ios)

   stop

end program snapshots_fft_ratio
