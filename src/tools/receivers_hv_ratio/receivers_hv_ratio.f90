!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
program receivers_hv_ratio

   use efispec3d_tools

   implicit none

   real   , parameter                 :: EPSILON_MACHINE = epsilon(EPSILON_MACHINE)
   real   , parameter                 :: PI = acos(-1.0)

   integer                            :: ndt
   integer                            :: ndt_s
   integer                            :: nt
   integer                            :: nt_s
   integer                            :: m
   integer                            :: m_s
   integer                            :: i
   integer                            :: nfold
   integer                            :: nfold_s
   integer                            :: narg
   integer                            :: win_type

   real                               :: o
   real                               :: t
   real                               :: dt
   real                               :: dt_s
   real                               :: df
   real                               :: df_s
   real                               :: td
   real                               :: td_s
   real                               :: freq
   real                               :: taper_percentage
   real                               :: win_b

   real   , allocatable, dimension(:) :: time
   real   , allocatable, dimension(:) :: time_s
   real   , allocatable, dimension(:) :: stf 
   real   , allocatable, dimension(:) :: hv

   real   , allocatable, dimension(:) :: ux_h
   real   , allocatable, dimension(:) :: uy_h
   real   , allocatable, dimension(:) :: uz_h
   real   , allocatable, dimension(:) :: vx_h
   real   , allocatable, dimension(:) :: vy_h
   real   , allocatable, dimension(:) :: vz_h
   real   , allocatable, dimension(:) :: ax_h
   real   , allocatable, dimension(:) :: ay_h
   real   , allocatable, dimension(:) :: az_h

   real   , allocatable, dimension(:) :: ux_v
   real   , allocatable, dimension(:) :: uy_v
   real   , allocatable, dimension(:) :: uz_v
   real   , allocatable, dimension(:) :: vx_v
   real   , allocatable, dimension(:) :: vy_v
   real   , allocatable, dimension(:) :: vz_v
   real   , allocatable, dimension(:) :: ax_v
   real   , allocatable, dimension(:) :: ay_v
   real   , allocatable, dimension(:) :: az_v

   real   , allocatable, dimension(:) :: i_h
   real   , allocatable, dimension(:) :: i_v

   complex, allocatable, dimension(:) :: c_h
   complex, allocatable, dimension(:) :: c_v
   complex, allocatable, dimension(:) :: c_s

   character(len= 1)                  :: sfdir
   character(len= 1)                  :: dva
   character(len=90)                  :: prog_name
   character(len=90)                  :: file_s
   character(len=90)                  :: file_h
   character(len=90)                  :: file_v
   character(len=90)                  :: file_hv

   logical                            :: is_taper=.true.

   narg = command_argument_count()

   if (narg /= 5) then

      call get_command_argument(0,prog_name)

      write(*,'(//,a)') "Usage : "//trim(adjustl(prog_name))//" stf Gh Gv dir dva"
      write(*,'( /,a)') "stf = source time function"
      write(*,'(   a)') "Gh  = horizontal Green function"
      write(*,'(   a)') "Gv  = vertical   Green function"
      write(*,'(   a)') "dir = direction. Choose between 'x' or 'y'"
      write(*,'(a,//)') "dva = displacement (d), velocity (v) or acceleration (a). Choose between 'd', 'v' or 'a'"

      stop

   endif

!
!->get command line argument

   call get_command_argument(1,file_s)

   call get_command_argument(2,file_h)

   call get_command_argument(3,file_v)

   call get_command_argument(4,sfdir)

   call get_command_argument(5,dva)


   call read_receiver_binary(file_h,ndt,dt,time,ux_h,uy_h,uz_h,vx_h,vy_h,vz_h,ax_h,ay_h,az_h)

   call read_receiver_binary(file_v,ndt,dt,time,ux_v,uy_v,uz_v,vx_v,vy_v,vz_v,ax_v,ay_v,az_v)

!  
!->read source time function or compute it 

!  call read_stf(file_s,ndt_s,dt_s,time_s,stf)

   ndt_s = ndt
   dt_s  = dt
   allocate(stf(ndt))

   do i = 1,ndt

      t      = real(i-1)*dt 
      stf(i) = gabor(t,0.3,0.15,1.0)

   enddo

!
!->check for equality of dt and dt_s
   if ( abs(dt-dt_s) > EPSILON_MACHINE) then
      stop "error: time step of source time function different of time step of receiver"
   endif

!
!->apply taper at the end only (see subroutine taper in module efispec3d_tools)

   if (is_taper) then

      taper_percentage = 10.0

      write(*,*) "Waveform is tapered (at the end): ",taper_percentage," percent"
      
      call taper(ux_h,ndt,taper_percentage)
      call taper(uy_h,ndt,taper_percentage)
      call taper(uz_h,ndt,taper_percentage)
      call taper(vx_h,ndt,taper_percentage)
      call taper(vy_h,ndt,taper_percentage)
      call taper(vz_h,ndt,taper_percentage)
      call taper(ax_h,ndt,taper_percentage)
      call taper(ay_h,ndt,taper_percentage)
      call taper(az_h,ndt,taper_percentage)
      
      call taper(ux_v,ndt,taper_percentage)
      call taper(uy_v,ndt,taper_percentage)
      call taper(uz_v,ndt,taper_percentage)
      call taper(vx_v,ndt,taper_percentage)
      call taper(vy_v,ndt,taper_percentage)
      call taper(vz_v,ndt,taper_percentage)
      call taper(ax_v,ndt,taper_percentage)
      call taper(ay_v,ndt,taper_percentage)
      call taper(az_v,ndt,taper_percentage)

   else

      write(*,*) "Waveform is not tapered"

   endif

!
!->compute FFT parameters
   call cnfast(ndt,nt,m,nfold,dt,df,td)
   call cnfast(ndt_s,nt_s,m_s,nfold_s,dt_s,df_s,td_s)

!
!->check for equality of nt and nt_s
   if (nt /= nt_s) then
      stop "error: nt of source time function different of nt of receiver"
   endif

   print *,"nt = ",nt

!
!->memory allocation for complex number
   allocate(c_h(nt),c_v(nt),c_s(nt))

   c_h(:) = cmplx(0.0,0.0)
   c_v(:) = cmplx(0.0,0.0)
   c_s(:) = cmplx(0.0,0.0)


!
!->prepare FFT arrays

   c_s(1:ndt_s) = +stf(1:ndt_s)

   if (sfdir == 'x') then

      if (dva == 'd') then

         c_h(1:ndt) = ux_h(1:ndt)
         c_v(1:ndt) = uz_v(1:ndt)

      elseif (dva == 'v') then

         c_h(1:ndt) = vx_h(1:ndt)
         c_v(1:ndt) = vz_v(1:ndt)

      elseif (dva == 'a') then

         c_h(1:ndt) = ax_h(1:ndt)
         c_v(1:ndt) = az_v(1:ndt)

      else

         stop "Error: unknown variable. Choose either 'd' (for displacement) or 'v' (for velocity) or 'a' (for acceleration)"

      endif

   elseif (sfdir == 'y') then

      if (dva == 'd') then

         c_h(1:ndt) = uy_h(1:ndt)
         c_v(1:ndt) = uz_v(1:ndt)

      elseif (dva == 'v') then

         c_h(1:ndt) = vy_h(1:ndt)
         c_v(1:ndt) = vz_v(1:ndt)

      elseif (dva == 'a') then

         c_h(1:ndt) = ay_h(1:ndt)
         c_v(1:ndt) = az_v(1:ndt)

      else

         stop "Error: unknown variable. Choose either 'd' (for displacement) or 'v' (for velocity) or 'a' (for acceleration)"

      endif

   else

      stop "Error: unknown direction. Choose either 'x' or 'y'"

   endif


!
!->compute fast Fourier transform
   call fft(c_h,nt,-1)
   call fft(c_v,nt,-1)
   call fft(c_s,nt,-1)

   c_h(:) = c_h(:)*dt
   c_v(:) = c_v(:)*dt
   c_s(:) = c_s(:)*dt

!
!->deconvolve GF wrt stf
   c_h(:) = c_h(:)/c_s(:)
   c_v(:) = c_v(:)/c_s(:)


!
!->compute imaginary part of Green's function and HV ratio
   allocate(hv(nfold),i_h(nfold),i_v(nfold))

   i_h(1:nfold) = abs(aimag(c_h(1:nfold)))
   i_v(1:nfold) = abs(aimag(c_v(1:nfold)))

!
!->smooting imaginary part before computing HV ratio
   win_type = 1

   if (win_type == 1) then

      win_b = 0.5

      write(*,*) " "
      write(*,*) "Parzen spectral window, b = ",win_b
      write(*,*) " "

      call spewin(i_h,nfold,df,win_b)
      call spewin(i_v,nfold,df,win_b)

   else

      win_b = 500.0

      write(*,*) " "
      write(*,*) "Konno-Omachi spectral window, bexp = ",win_b
      write(*,*) " "

      call konnoo(i_h,nfold,df,win_b)
      call konnoo(i_v,nfold,df,win_b)

   endif

   do i = 1,nfold

      hv(i) = sqrt(i_h(i)/i_v(i))
        
   enddo


!
!->write HV ratio
   file_hv = trim(file_h)//".hv."//trim(sfdir)//trim(dva)

   open(unit=11,file=trim(file_hv))

   do i = 1,nfold

      freq = real(i-1)*df

      if (freq <= 50.0) then

         write(unit=11,fmt='(4(E17.7,1X))') freq,hv(i),i_h(i),i_v(i)

      endif

   enddo

   close(11)

!!
!!->back to time domain after deconvolution
!   call fft_to_time_domain(c_h,nt,ux_h,dt)
!   call fft_to_time_domain(c_v,nt,uz_h,dt)
!
!
!!
!!->write deconvolve wave
!   file_hv = trim(file_h)//".deconv."//trim(sfdir)//trim(dva)
!
!   open(unit=11,file=trim(file_hv))
!
!   do i = 1,ndt
!
!      write(unit=11,fmt='(3(E17.7,1X))') time(i),ux_h(i),uz_h(i)
!
!   enddo
!
!   close(11)

   stop

end program
