!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
program receivers_filter
   
   use mod_precision
   
   use mod_receivers_filter
   
   use mod_signal_processing, only : nextpow2

   use mod_filter           , only :&
                                     butterworth_lowpass&
                                    ,butterworth_highpass&
                                    ,butterworth_bandpass&
                                    ,tandem&
                                    ,decimate

   implicit none
   
   integer(kind=IXP), parameter                              :: BUFFER_SIZE = 10_IXP
   
   real   (kind=RXP)                , dimension(BUFFER_SIZE) :: buffer
   real   (kind=RXP)   , allocatable, dimension(:)           :: buffer_rec
   real   (kind=RXP)   , allocatable, dimension(:)           :: time
   real   (kind=RXP)   , allocatable, dimension(:)           :: ux
   real   (kind=RXP)   , allocatable, dimension(:)           :: uy
   real   (kind=RXP)   , allocatable, dimension(:)           :: uz
   real   (kind=RXP)   , allocatable, dimension(:)           :: vx
   real   (kind=RXP)   , allocatable, dimension(:)           :: vy
   real   (kind=RXP)   , allocatable, dimension(:)           :: vz
   real   (kind=RXP)   , allocatable, dimension(:)           :: ax
   real   (kind=RXP)   , allocatable, dimension(:)           :: ay
   real   (kind=RXP)   , allocatable, dimension(:)           :: az

   real   (kind=R64)   , allocatable, dimension(:)           :: butter_coeff_lp
   real   (kind=R64)   , allocatable, dimension(:)           :: butter_coeff_hp
   real   (kind=R64)   , allocatable, dimension(:)           :: butter_coeff_bp

   real   (kind=R64)                                         :: butter_gain_lp
   real   (kind=R64)                                         :: butter_gain_hp
   real   (kind=R64)                                         :: butter_gain_bp
   integer(kind=IXP)                                         :: butter_order_lp
   integer(kind=IXP)                                         :: butter_order_hp
   integer(kind=IXP)                                         :: butter_order_bp
   integer(kind=IXP)                                         :: butter_func_order_lp
   integer(kind=IXP)                                         :: butter_func_order_hp
   integer(kind=IXP)                                         :: butter_func_order_bp
   real   (kind=RXP)                                         :: ideal_cutoff_lp
   real   (kind=RXP)                                         :: ideal_cutoff_hp
   real   (kind=RXP)                                         :: transition_band_lp
   real   (kind=RXP)                                         :: transition_band_hp
   real   (kind=RXP)                                         :: freq_pass_lp
   real   (kind=RXP)                                         :: freq_pass_hp
   real   (kind=RXP)                                         :: freq_stop_lp
   real   (kind=RXP)                                         :: freq_stop_hp
   real   (kind=RXP)                                         :: freq_low_bp
   real   (kind=RXP)                                         :: freq_high_bp
   real   (kind=RXP)                                         :: freq_stop_bp
   real   (kind=RXP)                                         :: ap_lp
   real   (kind=RXP)                                         :: ap_hp
   real   (kind=RXP)                                         :: ap_bp
   real   (kind=RXP)                                         :: as_lp
   real   (kind=RXP)                                         :: as_hp
   real   (kind=RXP)                                         :: as_bp
                                                           
   real   (kind=RXP)                                         :: ts = 0.0_RXP
   real   (kind=RXP)                                         :: dt
   real   (kind=RXP)                                         :: dt1
   real   (kind=RXP)                                         :: dt2
   real   (kind=RXP)                                         :: dt_dec
                                                         
   integer(kind=IXP)                                         :: unit_rec
   integer(kind=IXP)                                         :: i
   integer(kind=IXP)                                         :: ios
   integer(kind=IXP)                                         :: idt
   integer(kind=IXP)                                         :: ndt
   integer(kind=IXP)                                         :: nfft_lp
   integer(kind=IXP)                                         :: nfft_hp
   integer(kind=IXP)                                         :: nfft_bp
   integer(kind=IXP)                                         :: fdec
   integer(kind=IXP)                                         :: ndt_dec
   integer(kind=IXP)                                         :: ndt_pad
   integer(kind=IXP)                                         :: narg
                                                             
   character(len= 15)                                        :: ext
   character(len=100)                                        :: pname
   character(len=100)                                        :: fname
   character(len=255)                                        :: command_line
                                                             
   logical(kind=IXP)                                         :: is_filter_hp  = .false.
   logical(kind=IXP)                                         :: is_filter_lp  = .false.
   logical(kind=IXP)                                         :: is_filter_bp  = .false.
   logical(kind=IXP)                                         :: is_decimate   = .false.
   logical(kind=IXP)                                         :: is_time_shift = .false.


!
!
!**************************************************************************************************************************
!->Get command line argument
!**************************************************************************************************************************

   narg = command_argument_count()

   if ( narg <= 1_IXP ) then

      call get_command_argument(0,pname)

      write(*,'(a)') "Usage : "//trim(adjustl(pname))//" filename [-lp cutoff band] [-hp cutoff band] [-bp lowfreq highfreq stopfreq] [-dc decimation_facor] [-ts time_shift]"

      stop

   endif

   call get_command(command_line)

   call get_filter(command_line,' -lp',is_filter_lp,ideal_cutoff_lp,transition_band_lp)

   call get_filter(command_line,' -hp',is_filter_hp,ideal_cutoff_hp,transition_band_hp)

   call get_filter(command_line,' -bp',is_filter_bp,freq_low_bp,freq_high_bp,freq_stop_bp)

   call get_decimation(command_line,' -dc',is_decimate,fdec)

   call get_time_shift(command_line,' -ts',is_time_shift,ts)

   if ( (.not.is_filter_lp) .and. (.not.is_filter_hp) .and. (.not.is_filter_bp) ) stop "error: no filter applied. check command line keyword"

   call get_command_argument(1,fname)

   ext = ".filter"

   if(is_time_shift) ext = trim(ext)//".shift"

   fname = trim(adjustl(fname))

   open(newunit=unit_rec,file=trim(fname),status='old',form='unformatted',action='read',access='stream',iostat=ios)

   if (ios /= ZERO_IXP) then
      write(*,*) "error while opening ",trim(fname)
      stop
   endif

   ndt = ZERO_IXP

   do

      read(unit=unit_rec,iostat=ios) (buffer(i),i=ONE_IXP,BUFFER_SIZE)

      if (ios /= ZERO_IXP) exit
  
      ndt = ndt + ONE_IXP
  
      if (ndt == ONE_IXP) dt1 = buffer(ONE_IXP)
      if (ndt == TWO_IXP) dt2 = buffer(ONE_IXP)
  
   enddo

   close(unit_rec)
  
   dt = dt2-dt1

   write(*,*) "Number of time step found = ",ndt
   write(*,*) "Size   of time step found = ", dt

!
!->pad zero at the beginning if needed (especially when signal starts close to zero)
!  ndt_pad = ndt*TWO_IXP - ONE_IXP
   ndt_pad = ndt
   print *,ndt_pad

!
!
!***********************************************************************************************************************
!->design IIR filter 
!***********************************************************************************************************************

   if (is_filter_lp) then

!
!---->lowpass butterworth filter properties

      freq_pass_lp       =  ideal_cutoff_lp - transition_band_lp/TWO_RXP
      freq_stop_lp       =  ideal_cutoff_lp + transition_band_lp/TWO_RXP
      ap_lp              =  0.01_RXP
      as_lp              = 100.0_RXP
      nfft_lp            = nextpow2(n=ndt_pad,nmin=1024_IXP)

!
!---->design low-pass butterworth filter

      call butterworth_lowpass(butter_coeff_lp,butter_order_lp,butter_gain_lp,butter_func_order_lp,freq_pass_lp,freq_stop_lp,dt,ap_lp,as_lp)

      write(*,*) "Low-pass filter info"
      write(*,*) "Number of time step FFT   = ",nfft_lp
      write(*,*) "filter order              = ",butter_order_lp
      write(*,*) "filter Low  Freq Cutoff   = ",freq_pass_lp
      write(*,*) "filter High Freq Cutoff   = ",freq_stop_lp
      write(*,*) "filter passes             = ",TWO_IXP

      call compute_impulse_response(dt,nfft_lp,butter_coeff_lp,butter_order_lp,butter_gain_lp,"iir.butter.lp")

   endif

   if (is_filter_hp) then

!
!---->highpass butterworth filter properties
     
      freq_pass_hp       =  ideal_cutoff_hp - transition_band_hp/TWO_RXP
      freq_stop_hp       =  ideal_cutoff_hp + transition_band_hp/TWO_RXP
      ap_hp              =  0.01_RXP
      as_hp              = 100.0_RXP
      nfft_hp            = nextpow2(n=ndt_pad,nmin=1024_IXP)
     
!   
!---->design highpass butterworth filter
     
      call butterworth_highpass(butter_coeff_hp,butter_order_hp,butter_gain_hp,butter_func_order_hp,freq_pass_hp,freq_stop_hp,dt,ap_hp,as_hp)
     
      write(*,*) "High-pass filter info"
      write(*,*) "Number of time step FFT   = ",nfft_hp
      write(*,*) "filter order              = ",butter_order_hp
      write(*,*) "filter Low  Freq Cutoff   = ",freq_pass_hp
      write(*,*) "filter High Freq Cutoff   = ",freq_stop_hp
      write(*,*) "filter passes             = ",TWO_IXP

      call compute_impulse_response(dt,nfft_hp,butter_coeff_hp,butter_order_hp,butter_gain_hp,"iir.butter.hp")

   endif

   if (is_filter_bp) then

      ap_bp        =   0.1_RXP
      as_bp        =  10.0_RXP
      nfft_bp      = nextpow2(n=ndt_pad,nmin=1024_IXP)

      call butterworth_bandpass(butter_coeff_bp,butter_order_bp,butter_gain_bp,butter_func_order_bp,freq_low_bp,freq_high_bp,freq_stop_bp,dt,ap_bp,as_bp)

      write(*,*) "Band-pass filter info"
      write(*,*) "Number of time step FFT   = ",nfft_bp
      write(*,*) "filter order              = ",butter_order_bp
      write(*,*) "filter Low  Freq Cutoff   = ",freq_low_bp
      write(*,*) "filter High Freq Cutoff   = ",freq_high_bp
      write(*,*) "filter stop Freq          = ",freq_stop_bp
      write(*,*) "filter passes             = ",TWO_IXP
 
      call compute_impulse_response(dt,nfft_bp,butter_coeff_bp,butter_order_bp,butter_gain_bp,"iir.butter.bp")

   endif


!
!***********************************************************************************************************************
!->read FSR receiver one by one and filter it
!***********************************************************************************************************************

!
!->memory allocation

   allocate(buffer_rec(BUFFER_SIZE*ndt))

   open(newunit=unit_rec,file=trim(fname),status='old',form='unformatted',access='stream',action='read',iostat=ios)
 
   if (ios /= ZERO_IXP) stop "error while opening file"
 
   read(unit=unit_rec,iostat=ios) (buffer_rec(i),i=ONE_IXP,BUFFER_SIZE*ndt)

   if (ios /= ZERO_IXP) stop "error while reading file"

   write(*,*) "Reading, filtering and decimating receiver "//trim(fname)

   close(unit_rec)

   allocate(time(ndt_pad))
   allocate(ux  (ndt_pad))
   allocate(uy  (ndt_pad))
   allocate(uz  (ndt_pad))
   allocate(vx  (ndt_pad))
   allocate(vy  (ndt_pad))
   allocate(vz  (ndt_pad))
   allocate(ax  (ndt_pad))
   allocate(ay  (ndt_pad))
   allocate(az  (ndt_pad))

   time(:) = ZERO_RXP
   ux  (:) = ZERO_RXP
   uy  (:) = ZERO_RXP
   uz  (:) = ZERO_RXP
   vx  (:) = ZERO_RXP
   vy  (:) = ZERO_RXP
   vz  (:) = ZERO_RXP
   ax  (:) = ZERO_RXP
   ay  (:) = ZERO_RXP
   az  (:) = ZERO_RXP

   do idt = ONE_IXP,ndt_pad

      time(idt) = real(idt-ONE_IXP,kind=RXP)*dt

   enddo

   do idt = ONE_IXP,ndt

      ux  (ndt_pad-ndt+idt) = buffer_rec((idt-ONE_IXP)*BUFFER_SIZE+ 2_IXP)
      uy  (ndt_pad-ndt+idt) = buffer_rec((idt-ONE_IXP)*BUFFER_SIZE+ 3_IXP)
      uz  (ndt_pad-ndt+idt) = buffer_rec((idt-ONE_IXP)*BUFFER_SIZE+ 4_IXP)
      vx  (ndt_pad-ndt+idt) = buffer_rec((idt-ONE_IXP)*BUFFER_SIZE+ 5_IXP)
      vy  (ndt_pad-ndt+idt) = buffer_rec((idt-ONE_IXP)*BUFFER_SIZE+ 6_IXP)
      vz  (ndt_pad-ndt+idt) = buffer_rec((idt-ONE_IXP)*BUFFER_SIZE+ 7_IXP)
      ax  (ndt_pad-ndt+idt) = buffer_rec((idt-ONE_IXP)*BUFFER_SIZE+ 8_IXP)
      ay  (ndt_pad-ndt+idt) = buffer_rec((idt-ONE_IXP)*BUFFER_SIZE+ 9_IXP)
      az  (ndt_pad-ndt+idt) = buffer_rec((idt-ONE_IXP)*BUFFER_SIZE+10_IXP)

   enddo

!
!->shift in time

   if (abs(ts) > TINY_REAL_RXP) then

      call time_shift(ux,dt,ts)
      call time_shift(uy,dt,ts)
      call time_shift(uz,dt,ts)
      call time_shift(vx,dt,ts)
      call time_shift(vy,dt,ts)
      call time_shift(vz,dt,ts)
      call time_shift(ax,dt,ts)
      call time_shift(ay,dt,ts)
      call time_shift(az,dt,ts)

   endif

!
!->filtering

   if (is_filter_lp) then

      call tandem(ux,ndt_pad,butter_coeff_lp,butter_order_lp,butter_gain_lp)
      call tandem(uy,ndt_pad,butter_coeff_lp,butter_order_lp,butter_gain_lp)
      call tandem(uz,ndt_pad,butter_coeff_lp,butter_order_lp,butter_gain_lp)
      call tandem(vx,ndt_pad,butter_coeff_lp,butter_order_lp,butter_gain_lp)
      call tandem(vy,ndt_pad,butter_coeff_lp,butter_order_lp,butter_gain_lp)
      call tandem(vz,ndt_pad,butter_coeff_lp,butter_order_lp,butter_gain_lp)
      call tandem(ax,ndt_pad,butter_coeff_lp,butter_order_lp,butter_gain_lp)
      call tandem(ay,ndt_pad,butter_coeff_lp,butter_order_lp,butter_gain_lp)
      call tandem(az,ndt_pad,butter_coeff_lp,butter_order_lp,butter_gain_lp)

   endif

   if (is_filter_hp) then

      call tandem(ux,ndt_pad,butter_coeff_hp,butter_order_hp,butter_gain_hp)
      call tandem(uy,ndt_pad,butter_coeff_hp,butter_order_hp,butter_gain_hp)
      call tandem(uz,ndt_pad,butter_coeff_hp,butter_order_hp,butter_gain_hp)
      call tandem(vx,ndt_pad,butter_coeff_hp,butter_order_hp,butter_gain_hp)
      call tandem(vy,ndt_pad,butter_coeff_hp,butter_order_hp,butter_gain_hp)
      call tandem(vz,ndt_pad,butter_coeff_hp,butter_order_hp,butter_gain_hp)
      call tandem(ax,ndt_pad,butter_coeff_hp,butter_order_hp,butter_gain_hp)
      call tandem(ay,ndt_pad,butter_coeff_hp,butter_order_hp,butter_gain_hp)
      call tandem(az,ndt_pad,butter_coeff_hp,butter_order_hp,butter_gain_hp)

   endif

   if (is_filter_bp) then

      call tandem(ux,ndt_pad,butter_coeff_bp,butter_order_bp,butter_gain_bp)
      call tandem(uy,ndt_pad,butter_coeff_bp,butter_order_bp,butter_gain_bp)
      call tandem(uz,ndt_pad,butter_coeff_bp,butter_order_bp,butter_gain_bp)
      call tandem(vx,ndt_pad,butter_coeff_bp,butter_order_bp,butter_gain_bp)
      call tandem(vy,ndt_pad,butter_coeff_bp,butter_order_bp,butter_gain_bp)
      call tandem(vz,ndt_pad,butter_coeff_bp,butter_order_bp,butter_gain_bp)
      call tandem(ax,ndt_pad,butter_coeff_bp,butter_order_bp,butter_gain_bp)
      call tandem(ay,ndt_pad,butter_coeff_bp,butter_order_bp,butter_gain_bp)
      call tandem(az,ndt_pad,butter_coeff_bp,butter_order_bp,butter_gain_bp)

   endif

!
!->decimate time series

   ndt_dec = ndt_pad

   if ( (is_filter_lp .or. is_filter_bp) .and. is_decimate ) then

       dt_dec =  dt
      
       call decimate(time,ndt_dec,fdec)
       call decimate(ux  ,ndt_dec,fdec)
       call decimate(uy  ,ndt_dec,fdec)
       call decimate(uz  ,ndt_dec,fdec)
       call decimate(vx  ,ndt_dec,fdec)
       call decimate(vy  ,ndt_dec,fdec)
       call decimate(vz  ,ndt_dec,fdec)
       call decimate(ax  ,ndt_dec,fdec)
       call decimate(ay  ,ndt_dec,fdec)
       call decimate(az  ,ndt_dec,fdec)
       ndt_dec = int((ndt_dec-ONE_IXP)/fdec,kind=IXP) + ONE_IXP
        dt_dec = dt_dec*real(fdec,kind=RXP)
      
   endif

!
!-->write time series
 
   open(newunit=unit_rec,file=trim(fname)//trim(ext),form='unformatted',access='stream',action='write')

   do idt = ONE_IXP,ndt_dec

      write(unit=unit_rec) time(idt),ux(idt),uy(idt),uz(idt),vx(idt),vy(idt),vz(idt),ax(idt),ay(idt),az(idt)

   enddo

   close(unit_rec)

   deallocate(ux,uy,uz,vx,vy,vz,ax,ay,az,time)
 
   stop

end program
