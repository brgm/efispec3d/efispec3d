!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
module mod_receivers_filter
   
   use mod_precision

   use mod_init_memory
   
   implicit none

   private

   public  :: time_shift
   public  :: compute_impulse_response
   public  :: get_filter
   public  :: get_decimation
   public  :: get_time_shift
   public  :: load_file

   contains


!
!
!>@brief subroutine to shift time series in time
!>@param  x : input time series
!>@param  dt: time step
!>@param  ts: time shift in second (if positive -> delay in time. if negative -> advance in time)
!>@return x : output time series
!***********************************************************************************************************************************************************************************
   subroutine time_shift(x,dt,ts)
!***********************************************************************************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in)                               :: dt
      real(kind=RXP), intent(in)                               :: ts
      real(kind=RXP), intent(inout)             , dimension(:) :: x

      real(kind=RXP)               , allocatable ,dimension(:) :: y

      integer(kind=IXP)                                        :: nts
      integer(kind=IXP)                                        :: i
      integer(kind=IXP)                                        :: k
      integer(kind=IXP)                                        :: nd
      integer(kind=IXP)                                        :: ios

      nd = size(x)

      ios = init_array_real(y,nd,"mod_receivers_filter:time_shift:y")

      nts = int(ts/dt)

      do i = ONE_IXP,nd
   
         k = i + nts
   
         if (k >= ONE_IXP .and. k <= nd) then
   
            y(k) = x(i)
   
         endif
   
      enddo

      x(:) = y(:)
   
      return

!***********************************************************************************************************************************************************************************
   end subroutine time_shift
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute the impulse response of a filter and write it to disk
!>@param  dt    : time step
!>@param  nfft  : number of time steps
!>@param  coeff : filter coeff 
!>@param  order : filter order
!>@param  gain  : filter gain
!***********************************************************************************************************************************************************************************
   subroutine compute_impulse_response(dt,nfft,coeff,order,gain,fname)
!***********************************************************************************************************************************************************************************

      use mod_filter           , only : tandem

      use mod_signal_processing, only : fft => ft

      implicit none

      real   (kind=RXP)              , intent(in)  :: dt
      integer(kind=IXP)              , intent(in)  :: nfft
      real   (kind=R64), dimension(:), intent(in)  :: coeff
      integer(kind=IXP)              , intent(in)  :: order
      real   (kind=R64)              , intent(in)  :: gain
      character(len=*)               , intent(in)  :: fname

      real   (kind=RXP), dimension(:), allocatable :: dirac
      complex(kind=RXP), dimension(:), allocatable :: dirac_fft
      integer(kind=IXP)                            :: idt
      integer(kind=IXP)                            :: ios
      integer(kind=IXP)                            :: myunit

      ios = init_array_real(dirac,nfft,"mod_receivers_filter:compute_impulse_response:dirac")

      ios = init_array_complex(dirac_fft,nfft,"mod_receivers_filter:compute_impulse_response:dirac_fft")

      dirac(nfft/TWO_IXP) = ONE_RXP
      
      call tandem(dirac,nfft,coeff,order,gain)
      
      dirac_fft(:) = dirac(:)
      
      call fft(dirac_fft,-ONE_IXP)
      
      open(newunit=myunit,file=trim(adjustl(fname))//".coef",action='write')
      do idt = ONE_IXP,size(coeff)
         write(unit=myunit,fmt='(2(E15.7,1X))') real(idt-ONE_IXP,kind=RXP)*dt,coeff(idt)
      enddo
      close(myunit)
      
      open(newunit=myunit,file=trim(adjustl(fname))//".time",action='write')
      do idt = ONE_IXP,nfft
         write(unit=myunit,fmt='(2(E15.7,1X))') real(idt-ONE_IXP,kind=RXP)*dt,dirac(idt)
      enddo
      close(myunit)
      
      open(newunit=myunit,file=trim(adjustl(fname))//".freq",action='write')
      do idt = ONE_IXP,nfft/TWO_IXP+ONE_IXP
         write(unit=myunit,fmt='(2(E15.7,1X))') real(idt-ONE_IXP,kind=RXP)*(ONE_RXP/(real(nfft,kind=RXP)*dt)),abs(dirac_fft(idt))
      enddo
      close(myunit)

      deallocate(dirac)
      deallocate(dirac_fft)
   
      return

!***********************************************************************************************************************************************************************************
   end subroutine compute_impulse_response
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to search filter parameters inside command line
!>@param   l   : line
!>@param   k   : key to search in line
!>@return  lo  : logical value, true if key is found
!>@return  idc : ideal cutoff frequency (low or high pass filter) or low frequency cut-off (bandpass filter)
!>@return  trb : transition band (low or high pass filter) or high frequency cut-off (bandpass filter)
!>@return  fs  : roll-off frequency (bandpass filter)
!***********************************************************************************************************************************************************************************
   subroutine get_filter(l,k,lo,idc,trb,fs)
!***********************************************************************************************************************************************************************************

      implicit none

      character(len=*) , intent(in )           :: l
      character(len=*) , intent(in )           :: k
      logical(kind=IXP), intent(out)           :: lo
      real   (kind=RXP), intent(out)           :: idc
      real   (kind=RXP), intent(out)           :: trb
      real   (kind=RXP), intent(out), optional :: fs

      character(len=:), allocatable :: v
      integer(kind=IXP)             :: p
      integer(kind=IXP)             :: ll
   
      p = index(l,k)

      if (p >= 2_IXP) then

         lo = .true.
       
         ll = len(trim(l))
       
         v = trim(l)
       
         v = l(p+len(k):ll)

         if (.not.present(fs)) then
       
            read(v,*) idc,trb

         else

            read(v,*) idc,trb,fs

         endif

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine get_filter
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to search decimation parameter inside command line
!>@param   l   : line
!>@param   k   : key to search in line
!>@return  lo  : logical value, true if key is found
!>@return  fd  : decimation factor
!***********************************************************************************************************************************************************************************
   subroutine get_decimation(l,k,lo,fd)
!***********************************************************************************************************************************************************************************

      implicit none

      character(len=*) , intent(in ) :: l
      character(len=*) , intent(in ) :: k
      logical(kind=IXP), intent(out) :: lo
      integer(kind=IXP), intent(out) :: fd

      character(len=:), allocatable :: v
      integer(kind=IXP)             :: p
      integer(kind=IXP)             :: ll
   
      p = index(l,k)

      if (p >= 2_IXP) then

         lo = .true.
       
         ll = len(trim(l))
       
         v = trim(l)
       
         v = l(p+len(k):ll)
       
         read(v,*) fd

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine get_decimation
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to search time shift parameter inside command line
!>@param   l   : line
!>@param   k   : key to search in line
!>@return  lo  : logical value, true if key is found
!>@return  x   : time shiff
!***********************************************************************************************************************************************************************************
   subroutine get_time_shift(l,k,lo,x)
!***********************************************************************************************************************************************************************************

      implicit none

      character(len=*) , intent(in ) :: l
      character(len=*) , intent(in ) :: k
      logical(kind=IXP), intent(out) :: lo
      real   (kind=RXP), intent(out) :: x

      character(len=:), allocatable :: v
      integer(kind=IXP)             :: p
      integer(kind=IXP)             :: ll
   
      p = index(l,k)

      if (p >= 2_IXP) then

         lo = .true.
       
         ll = len(trim(l))
       
         v = trim(l)
       
         v = l(p+len(k):ll)
       
         read(v,*) x

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine get_time_shift
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to read two columns file
!>@param   f  : filename
!>@param   n  : number of header lines to skip
!>@return  x  : first  column
!>@return   y  : second column
!***********************************************************************************************************************************************************************************
   subroutine load_file(f,nskip,x,y)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only : count_file_line

      implicit none

      character(len=*)                            , intent(in ) :: f
      integer(kind=IXP)                           , intent(in ) :: nskip
      real   (kind=RXP), dimension(:), allocatable, intent(out) :: x
      real   (kind=RXP), dimension(:), allocatable, intent(out) :: y

      integer(kind=IXP)                                         :: idt
      integer(kind=IXP)                                         :: ndt
      integer(kind=IXP)                                         :: iskip
      integer(kind=IXP)                                         :: u
      integer(kind=IXP)                                         :: ios

      character(len=1)                                          :: cskip

      ndt = count_file_line(f) - nskip

      ios = init_array_real(x,ndt,"mod_receivers_filter:load_file:x")

      ios = init_array_real(y,ndt,"mod_receivers_filter:load_file:y")

      open(newunit=u,file=trim(f),form='formatted',status='old',action='read',iostat=ios)

      if (ios /= ZERO_IXP) then

         write(*,*) "error while opening ",trim(f)
         stop

      endif

      do iskip = 1_IXP,nskip

         read(unit=u,fmt=*) cskip

      enddo

      do idt = 1_IXP,ndt

         read(unit=u,fmt=*) x(idt),y(idt)

      enddo

      close(u)

      return

!***********************************************************************************************************************************************************************************
   end subroutine load_file
!***********************************************************************************************************************************************************************************

end module mod_receivers_filter
