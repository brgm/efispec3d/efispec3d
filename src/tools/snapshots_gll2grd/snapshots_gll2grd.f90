!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
program snapshot_gll2grd

   use mpi

   use mod_precision

   use mod_global_variables, only :&
                                   ig_hexa_nnode&
                                  ,ig_quad_nnode&
                                  ,ig_nquad_fsurf_all_cpu&
                                  ,ig_nquad_fsurf&
                                  ,ig_quadf_gll_glonum&
                                  ,ig_quadf_gnode_glonum&
                                  ,ig_ngll_total&
                                  ,rg_gnode_x&
                                  ,rg_gnode_y&
                                  ,rg_gnode_z&
                                  ,rg_mesh_xmin&
                                  ,rg_mesh_xmax&
                                  ,rg_mesh_ymin&
                                  ,rg_mesh_ymax&
                                  ,rg_mesh_zmin&
                                  ,rg_mesh_zmax&
                                  ,ig_myrank&
                                  ,ig_ncpu&
                                  ,cg_prefix&
                                  ,cg_lst_ext&
                                  ,get_prefix&
                                  ,get_newunit&
                                  ,ig_mpi_nboctet_real&
                                  ,ig_mpi_comm_simu&
                                  ,IG_LST_UNIT&
                                  ,IG_NDOF&
                                  ,IG_NGLL&
                                  ,LG_LUSTRE_FILE_SYS
   
   use mod_efi_mpi

   use mod_init_efi

   use mod_init_memory

   use mod_init_mesh       , only : init_element
   
   use mod_receiver        , only :&
                                   init_quad_receiver&
                                  ,write_receiver_output

   use mod_snapshot_surface, only :&
                                   init_snapshot_surface&
                                  ,write_snapshot_surface&
                                  ,write_peak_ground_motion_gll

   use mod_io_array

   use mod_write_listing   , only : write_temporal_domain_info

   use mod_snapshots_gll2grd

   implicit none

   real   (kind=R64)                                    :: start_time
   real   (kind=R64)                                    :: end_time
   real   (kind=R64)                                    :: dltim2

   real   (kind=RXP), allocatable, dimension(:,:,:)     :: quad_gnode_xyz
   real   (kind=RXP), allocatable, dimension(:,:,:)     :: all_quad_gll_value
   real   (kind=RXP), allocatable, dimension(:)         :: gll_max

   real   (kind=RXP)                                    :: gnode_x
   real   (kind=RXP)                                    :: gnode_y
   real   (kind=RXP)                                    :: gnode_z
   real   (kind=RXP)                                    :: gnode_xmin
   real   (kind=RXP)                                    :: gnode_xmax
   real   (kind=RXP)                                    :: gnode_ymin
   real   (kind=RXP)                                    :: gnode_ymax
   real   (kind=RXP)                                    :: gnode_zmin
   real   (kind=RXP)                                    :: gnode_zmax
                                                       
   integer(kind=IXP)                                    :: nquad_fsurf_all_cpu
   integer(kind=IXP)                                    :: nquad_floor
   integer(kind=IXP)                                    :: iquad_offset
   integer(kind=IXP)                                    :: iquad
   integer(kind=IXP)                                    :: inode
   integer(kind=IXP)                                    :: iloc
   integer(kind=IXP)                                    :: k
   integer(kind=IXP)                                    :: l
                                                       
   integer(kind=IXP)                                    :: ios
                                                       
   character(len=255)                                   :: cpost
   character(len=255)                                   :: fname
   character(len=255)                                   :: fname_gno


!
!
!**********************************************************************************************************************
   call init_mpi()

   start_time = mpi_wtime()
!**********************************************************************************************************************

   call get_command_argument(1_IXP,cpost) 
   call get_command_argument(2_IXP,fname) 

!
!
!**********************************************************************************************************************
   cg_prefix = get_prefix()

   cg_lst_ext = ".post.snapshots.gll2grd.lst"

   call init_input_variables(cg_prefix)
!**********************************************************************************************************************

   
!
!
!**********************************************************************************************************************
   ig_hexa_nnode = 8_IXP

   ig_quad_nnode = 4_IXP

   call init_element(ig_hexa_nnode,ig_quad_nnode)
!**********************************************************************************************************************

!
!
!**********************************************************************************************************************
   call init_gll_nodes()
!**********************************************************************************************************************


!
!
!**********************************************************************************************************************
   fname_gno = trim(cg_prefix)//".snapshot.quadf.gno.xyz"

   call efi_mpi_file_read_at_all(ig_mpi_comm_simu,fname_gno,ig_myrank,ig_ncpu,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS,quad_gnode_xyz,ig_nquad_fsurf_all_cpu,ig_nquad_fsurf,nquad_floor,iquad_offset)

   print '(5(I10))',ig_myrank,ig_nquad_fsurf_all_cpu,ig_nquad_fsurf,nquad_floor,iquad_offset

!**********************************************************************************************************************


!
!
!****************************************************************************************************************************************************
!->determine the xmin/xmax/ymin/ymax boundaries of the box surrounding the arbitrary shape formed by the free surface quadrangle in cpu 'myrank'
!  In the first pass (below), if the receiver is not inside this box, then there is no need to check if it is inside a quadrangle
!****************************************************************************************************************************************************

   gnode_xmin = +huge(gnode_xmin)
   gnode_xmax = -huge(gnode_xmax)
   gnode_ymin = +huge(gnode_ymin)
   gnode_ymax = -huge(gnode_ymax)
   gnode_zmin = +huge(gnode_zmin)
   gnode_zmax = -huge(gnode_zmax)

   do iquad = ONE_IXP,ig_nquad_fsurf

      do inode = ONE_IXP,ig_quad_nnode

         gnode_x   = quad_gnode_xyz(ONE_IXP,inode,iquad)
         gnode_y   = quad_gnode_xyz(TWO_IXP,inode,iquad)
         gnode_z   = quad_gnode_xyz(THREE_IXP,inode,iquad)

         gnode_xmin = min(gnode_xmin,gnode_x)
         gnode_xmax = max(gnode_xmax,gnode_x)
         gnode_ymin = min(gnode_ymin,gnode_y)
         gnode_ymax = max(gnode_ymax,gnode_y)
         gnode_zmin = min(gnode_zmin,gnode_z)
         gnode_zmax = max(gnode_zmax,gnode_z)

      enddo

   enddo

!
!->set min/max x,y,z coordinates of the free surface quadrangle elements

   call mpi_allreduce(gnode_xmin,rg_mesh_xmin,ONE_IXP,MPI_REAL,MPI_MIN,ig_mpi_comm_simu,ios)
   call mpi_allreduce(gnode_ymin,rg_mesh_ymin,ONE_IXP,MPI_REAL,MPI_MIN,ig_mpi_comm_simu,ios)
   call mpi_allreduce(gnode_zmin,rg_mesh_zmin,ONE_IXP,MPI_REAL,MPI_MIN,ig_mpi_comm_simu,ios)

   call mpi_allreduce(gnode_xmax,rg_mesh_xmax,ONE_IXP,MPI_REAL,MPI_MAX,ig_mpi_comm_simu,ios)
   call mpi_allreduce(gnode_ymax,rg_mesh_ymax,ONE_IXP,MPI_REAL,MPI_MAX,ig_mpi_comm_simu,ios)
   call mpi_allreduce(gnode_zmax,rg_mesh_zmax,ONE_IXP,MPI_REAL,MPI_MAX,ig_mpi_comm_simu,ios)

!
!
!*****************************************************************************************************************************
!->initialize all global variables needed for the post-processing
!*****************************************************************************************************************************

!
!->ig_quadf_gnode_glonum is not the global numbering anymore but just the numbering refering to rg_gnode_x and rg_gnode_y

   ios = init_array_int(ig_quadf_gnode_glonum,ig_nquad_fsurf,ig_quad_nnode,"ig_quadf_gnode_glonum")

   ios = init_array_real(rg_gnode_x,ig_nquad_fsurf*ig_quad_nnode,"rg_gnode_x")

   ios = init_array_real(rg_gnode_y,ig_nquad_fsurf*ig_quad_nnode,"rg_gnode_y")

   ios = init_array_real(rg_gnode_z,ig_nquad_fsurf*ig_quad_nnode,"rg_gnode_z")

   iloc = ZERO_IXP

!
!->fill ig_quadf_gnode_glonum using a global numbering independent of quad in contact

   do iquad = ONE_IXP,ig_nquad_fsurf

      do inode = ONE_IXP,ig_quad_nnode

         iloc = iloc + ONE_IXP

         ig_quadf_gnode_glonum(inode,iquad) = iloc

      enddo

   enddo

!
!->fill rg_gnode_x (must be align with ig_quadf_gnode_glonum)

   iloc = ZERO_IXP

   do iquad = ONE_IXP,ig_nquad_fsurf

      do inode = ONE_IXP,ig_quad_nnode

         iloc = iloc + ONE_IXP

         rg_gnode_x(iloc) = quad_gnode_xyz(ONE_IXP,inode,iquad)

      enddo

   enddo

!
!->fill rg_gnode_y

   iloc = ZERO_IXP

   do iquad = ONE_IXP,ig_nquad_fsurf

      do inode = ONE_IXP,ig_quad_nnode

         iloc = iloc + ONE_IXP

         rg_gnode_y(iloc) = quad_gnode_xyz(TWO_IXP,inode,iquad)

      enddo

   enddo

!
!->fill rg_gnode_z

   iloc = ZERO_IXP

   do iquad = ONE_IXP,ig_nquad_fsurf

      do inode = ONE_IXP,ig_quad_nnode

         iloc = iloc + ONE_IXP

         rg_gnode_z(iloc) = quad_gnode_xyz(THREE_IXP,inode,iquad)

      enddo

   enddo

   deallocate(quad_gnode_xyz)

!
!->fill ig_quadf_gll_glonum using a global numbering independent of quad in contact
 
   ios = init_array_int(ig_quadf_gll_glonum,ig_nquad_fsurf,IG_NGLL,IG_NGLL,"ig_quadf_gll_glonum")

   iloc = ZERO_IXP

   do iquad = ONE_IXP,ig_nquad_fsurf

      do k = ONE_IXP,IG_NGLL

         do l = ONE_IXP,IG_NGLL

            iloc = iloc + ONE_IXP

            ig_quadf_gll_glonum(l,k,iquad) = iloc

         enddo

      enddo

   enddo

   ig_ngll_total = IG_NGLL*IG_NGLL*ig_nquad_fsurf

!
!
!*****************************************************************************************************************************
!->init snapshots
!*****************************************************************************************************************************

   call init_snapshot_surface()

!
!
!*****************************************************************************************************************************
!->read file containing GLL value to be interpolated
!*****************************************************************************************************************************

   fname = trim(fname)

   call efi_mpi_file_read_at_all(ig_mpi_comm_simu,fname,ig_myrank,ig_ncpu,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS,all_quad_gll_value,nquad_fsurf_all_cpu,ig_nquad_fsurf,nquad_floor,iquad_offset)

   print '(5(I10))',ig_myrank,nquad_fsurf_all_cpu,ig_nquad_fsurf,nquad_floor,iquad_offset

   ios = init_array_real(gll_max,ig_ngll_total,"gll_max")

   call fill_global_from_local(all_quad_gll_value,gll_max)

   fname = trim(fname)//".grd"

   call write_peak_ground_motion_gll(gll_max,fname)

!
!
!*****************************************************************************************************************************
!->compute total time spent for post-processing free surface
!*****************************************************************************************************************************
   end_time = mpi_wtime() - start_time

   call mpi_reduce(end_time,dltim2,ONE_IXP,mpi_double_precision,mpi_max,ZERO_IXP,ig_mpi_comm_simu,ios)

   if (ig_myrank == ZERO_IXP) then

      write(unit=IG_LST_UNIT,fmt='("",/a,e15.7,a)') "elapsed time for post-processing = ",dltim2," s"

   endif

   call mpi_finalize(ios)

end program snapshot_gll2grd
