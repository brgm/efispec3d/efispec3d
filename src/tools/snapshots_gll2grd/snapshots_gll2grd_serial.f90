!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
program snapshots_gll2grd_serial

   use mpi

   use mod_global_variables, only :&
                                   get_newunit&
                                  ,get_prefix&
                                  ,error_stop&
                                  ,cg_prefix&
                                  ,cg_lst_ext&
                                  ,ig_myrank&
                                  ,ig_ncpu&
                                  ,ig_mpi_nboctet_real&
                                  ,ig_mpi_comm_simu&
                                  ,LG_LUSTRE_FILE_SYS

   use mod_efi_mpi

   use mod_init_efi

   use mod_precision

   use mod_io_array

   use mod_snapshot_surface

   implicit none

   integer(kind=IXP), parameter                     :: HEADER_SIZE = 892_IXP

!  real   (kind=RXP), dimension(:,:,:,:), allocatable :: quad_gll_value
   real   (kind=RXP), dimension(:,:,:)  , allocatable :: quad_gll_value
!  integer(kind=IXP), dimension(:,:,:)  , allocatable :: quad_gll_value_int
   real   (kind=RXP), dimension(:,:)    , allocatable :: grid_meta
   real   (kind=RXP), dimension(:)      , allocatable :: grid_value

   real   (kind=RXP)                                :: x_min
   real   (kind=RXP)                                :: x_max
   real   (kind=RXP)                                :: y_min
   real   (kind=RXP)                                :: y_max
   real   (kind=RXP)                                :: v_min
   real   (kind=RXP)                                :: v_max
   real   (kind=RXP)                                :: dx

   integer(kind=IXP), dimension(MPI_STATUS_SIZE)    :: statut
   integer(kind=IXP)                                :: narg
   integer(kind=IXP)                                :: unit_prefix
   integer(kind=IXP)                                :: unit_fgrid
   integer(kind=IXP)                                :: unit_fgrd
   integer(kind=IXP)                                :: ios
   integer(kind=IXP)                                :: nelt_all_cpu
   integer(kind=IXP)                                :: nelt_rank
   integer(kind=IXP)                                :: nelt_floor
   integer(kind=IXP)                                :: rank_offset
   integer(kind=IXP)                                :: ncol
   integer(kind=IXP)                                :: nrow
   integer(kind=I64)                                :: pos
                                                    
   character(len= 12)                               :: cdx
   character(len= 12)                               :: ccol
   character(len= 12)                               :: crow
   character(len=255)                               :: info
   character(len=255)                               :: fquad
   character(len=255)                               :: fgrid
   character(len=255)                               :: fgrd
                                                    
!
!->init mpi EFI style BUT CODE IS SEQUENTIAL SO FAR (grd file is written only by proc0 without gather). Need to code the gather part before writting the grd file.

   call init_mpi()

!
!->init GLL in reference interval

   call init_gll_nodes() 
!
!->read prefix

   cg_prefix = get_prefix()

   cg_lst_ext = ".post.snapshots.gll2grd.serial.lst"

!
!->get argument

   narg = command_argument_count()

   if (narg == FIVE_IXP) then

      call getarg(ONE_IXP,fquad)

      call getarg(TWO_IXP,fgrid)

      call getarg(THREE_IXP,ccol)

      call getarg(FOUR_IXP,crow)

      call getarg(FIVE_IXP,cdx)

      read(ccol,*) ncol
 
      read(crow,*) nrow 
 
      read(cdx ,*) dx

   else

      write(info,'(a)') "wrong number of argument on command line. ./exe fquad fgrid ncol nrow dx"
      call error_stop(info)

   endif

   !for real
   call efi_mpi_file_read_at_all(ig_mpi_comm_simu,fquad,ig_myrank,ig_ncpu,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS,quad_gll_value,nelt_all_cpu,nelt_rank,nelt_floor,rank_offset) 

   !for integer
   !call efi_mpi_file_read_at_all(ig_mpi_comm_simu,fquad,ig_myrank,ig_ncpu,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS,quad_gll_value_int,nelt_all_cpu,nelt_rank,nelt_floor,rank_offset)

   print *,nelt_all_cpu
   print *,minval(quad_gll_value)
   print *,maxval(quad_gll_value)

   allocate(grid_meta(6_IXP,ncol*nrow))

   open(unit=get_newunit(unit_fgrid),file=trim(adjustl(fgrid)),form='unformatted',access='stream',action='read')

   read(unit=unit_fgrid) grid_meta(:,:)

   close(unit_fgrid)

   call convert_quad_to_grd(quad_gll_value,grid_meta,grid_value)

!  x_min = minval(grid_meta(1,:))
!  x_max = maxval(grid_meta(1,:))
!  y_min = minval(grid_meta(2,:))
!  y_max = maxval(grid_meta(2,:))
   x_min = ZERO_RXP 
   x_max = real(ncol-ONE_IXP,kind=RXP)*dx
   y_min = ZERO_RXP
   y_max = real(nrow-ONE_IXP,kind=RXP)*dx
   v_min = minval(grid_value)
   v_max = maxval(grid_value)

   print *,x_min,x_max,y_min,y_max,v_min,v_max,ncol,nrow,dx

   if (ig_myrank == ZERO_IXP) then

      fgrd = trim(fquad)//".grd"
     
      call mpi_file_open(ig_mpi_comm_simu,fgrd,MPI_MODE_WRONLY + MPI_MODE_CREATE,MPI_INFO_NULL,unit_fgrd,ios)
     
      call write_header_gmt_nat_fmt(unit_fgrd,ncol,nrow,x_min,x_max,y_min,y_max,v_min,v_max,dx,dx)

      pos  = int(HEADER_SIZE,kind=I64) !warning: should be written in terms of mpi_type_size

      call mpi_file_write_at(unit_fgrd,pos,grid_value,ncol*nrow,MPI_REAL,statut,ios)

      call mpi_file_close(unit_fgrd,ios)

   endif

   call mpi_finalize(ios)

   stop

end program snapshots_gll2grd_serial
