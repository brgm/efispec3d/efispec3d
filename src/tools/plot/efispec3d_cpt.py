#!/usr/bin/python

import os
import sys

###############################################################################################
def getColorCorrespondingTovalue(myval,mymin,mymax):

#  color = [[0.5,0.5,0.5],[0.0,1.0,0.0],[1.0,1.0,1.0],[1.0,1.0,0.0],[0.9,0.9,0.9],[1.0,0.502,0.0],[0.6,0.6,0.6],[1.0,0.0,0.0],[0.0,0.0,0.0]]
#  color = [[0.0,1.0,1.0],[0.0,1.0,0.0],[1.0,1.0,1.0],[1.0,1.0,0.0],[0.9,0.9,0.9],[1.0,0.502,0.0],[0.6,0.6,0.6],[1.0,0.0,0.0],[0.0,0.0,0.0]]

   color = [[100.0,100.0,100.0]\
#          ,[191.0,204.0,255.0]\
           ,[  0.0,  0.0,255.0]\
           ,[  0.0,255.0,  0.0]\
           ,[255.0,255.0,  0.0]\
#          ,[100.0,100.0,100.0]\
           ,[255.0,127.0,  0.0]\
#          ,[ 50.0, 50.0, 50.0]\
           ,[255.0,  0.0,  0.0]\
           ,[  0.0,  0.0,  0.0]]

#  shakemap color
#   color = [[255.0,255.0,255.0]\
#           ,[191.0,204.0,255.0]\
#           ,[160.0,230.0,255.0]\
#           ,[128.0,255.0,255.0]\
#           ,[122.0,255.0,147.0]\
#           ,[255.0,255.0,  0.0]\
#           ,[255.0,200.0,  0.0]\
#           ,[255.0,145.0,  0.0]\
#           ,[255.0,  0.0,  0.0]\
#           ,[200.0,  0.0,  0.0]\
#           ,[128.0,  0.0,  0.0]]

   numColorNodes = len(color)

   myrange = mymax - mymin

   for i in range(0,numColorNodes-1):
      currFloor = mymin + ( float(i    ) / float(numColorNodes - 1)) * myrange
      currCeil  = mymin + ( float(i + 1) / float(numColorNodes - 1)) * myrange

      if myval >= currFloor and myval <= currCeil:
         currFraction = (myval - currFloor) / (currCeil - currFloor)
         r = color[i][0] * (1.0 - currFraction) + color[i + 1][0] * currFraction
         g = color[i][1] * (1.0 - currFraction) + color[i + 1][1] * currFraction
         b = color[i][2] * (1.0 - currFraction) + color[i + 1][2] * currFraction

   return r,g,b

###############################################################################################

total_arg = len(sys.argv)

numColors = int(sys.argv[1])
valmin    = float(sys.argv[2])
valmax    = float(sys.argv[3])
dval      = (valmax-valmin)/numColors

with open('color.cpt','w') as f:

   for i in range(0,numColors):

      val1  = valmin + float(i  )*dval
      val2  = valmin + float(i+1)*dval

      r,g,b = getColorCorrespondingTovalue(val1,valmin,valmax)
      f.write('%f\t%f/%f/%f\t%f\t%f/%f/%f\n' % (val1,r,g,b,val2,r,g,b))

   r,g,b = getColorCorrespondingTovalue(valmin,valmin,valmax)
   f.write('%s\t%f/%f/%f\n' % ('B',r,g,b))

   r,g,b = getColorCorrespondingTovalue(valmax,valmin,valmax)
   f.write('%s\t%f/%f/%f\n' % ('F',r,g,b))

   f.write('%s\t%f\n' % ('N',127.5))
