#!/usr/bin/python

import os
import sys
import numpy as np

###############################################################################################
def getColorCorrespondingTovalue(myval):

   color = [[255.0,255.0,255.0]\
           ,[191.0,204.0,255.0]\
           ,[160.0,230.0,255.0]\
           ,[128.0,255.0,255.0]\
           ,[122.0,255.0,147.0]\
           ,[255.0,255.0,  0.0]\
           ,[255.0,200.0,  0.0]\
           ,[255.0,145.0,  0.0]\
           ,[255.0,  0.0,  0.0]\
           ,[200.0,  0.0,  0.0]\
           ,[128.0,  0.0,  0.0]]

   PGVRange = [0.0,0.1,0.6,1.1,3.4,8.1,16.0,31.0,60.0,116.0,120.0]

   mymin = PGVRange[ 0] 
   mymax = PGVRange[-1] 

   myrange = mymax - mymin

   myval = myval*100.0

   if myval < mymin:
      myval = mymin

   i = 1
   while (PGVRange[i] < myval) and (i < len(PGVRange)):
      i = i + 1 

   currFloor    = PGVRange[i-1]
   currCeil     = PGVRange[i  ]
   currFraction = (myval - currFloor) / (currCeil - currFloor)

   r = color[i-1][0] * (1.0 - currFraction) + color[i][0] * currFraction
   g = color[i-1][1] * (1.0 - currFraction) + color[i][1] * currFraction
   b = color[i-1][2] * (1.0 - currFraction) + color[i][2] * currFraction

   return r,g,b

###############################################################################################

def PGA_to_Intensity_V_VIII(pga):
   "Convert PGA value to intensity"

   I = 3.66*np.log(pga) - 1.66
   return I

###############################################################################################

def PGA_to_Intensity(pga):
   "Convert PGA value to intensity"

   I = 2.20*np.log(pga) + 1.00
   return I
###############################################################################################

def PGV_To_Intensity_V_IX(pgv):
   "Convert PGV value to intensity"

   I = 3.47*np.log(pgv) + 2.35
   return I
###############################################################################################

def PGV_To_Intensity(pgv):
   "Convert PGV value to intensity"

   epsilon = 1.0e-6

   pgv = pgv*100.0

   if pgv <= epsilon:
      pgv  = epsilon

   I = 2.10*np.log(pgv) + 3.40
   return I
###############################################################################################


###############################################################################################

total_arg = len(sys.argv)
numColors = int(sys.argv[1])
valmin    = float(sys.argv[2])
valmax    = float(sys.argv[3])
dval      = (valmax-valmin)/numColors

with open('color.cpt','w') as f:

   for i in range(0,numColors):

      val1  = valmin + float(i  )*dval
      val2  = valmin + float(i+1)*dval

      r,g,b = getColorCorrespondingTovalue(val1)
      f.write('%f\t%f/%f/%f\t%f\t%f/%f/%f\n' % (val1,r,g,b,val2,r,g,b))

   r,g,b = getColorCorrespondingTovalue(valmin)
   f.write('%s\t%f/%f/%f\n' % ('B',r,g,b))

   r,g,b = getColorCorrespondingTovalue(valmax)
   f.write('%s\t%f/%f/%f\n' % ('F',r,g,b))

   f.write('%s\t%f\n' % ('N',127.5))
