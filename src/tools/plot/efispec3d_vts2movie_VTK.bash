#!/bin/bash

EFIDIR=/home/${USER}/Codes/EFISPEC3D/bin/
MYPYTHON=/home/demartin/Codes/VTK-6.1.0/VTK-6.1.0-Linux-64bit/bin/vtkpython

#export display to avoid rendering in DISPLAY=:0
#Xvfb :1 -screen 0 1280x1024x24 -auth localhost
#export DISPLAY=:1

for file in `ls *.vts`
do
   echo $file
#  xvfb-run --server-args="-screen 0 1280x1024x24" $MYPYTHON ${EFIDIR}efispec3d_VTS2JPG.py $file
   $MYPYTHON ${EFIDIR}efispec3d_VTS2JPG.py $file
done
