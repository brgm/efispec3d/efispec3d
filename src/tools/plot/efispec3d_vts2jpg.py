#!/usr/bin/python

import vtk
import os
import sys

###############################################################################################
def GetFrameNumber(fname):
   """Get the frame number from *.vts file"""
   frame = fname.split(".")[-2]
   return frame
###############################################################################################

def getColorCorrespondingTovalue(myval,mymin,mymax):

   numColorNodes = 9
#  color = [[0.0, 0.0, 1.0],[1.0, 1.0, 1.0],[1.0, 0.0, 0.0]]
   color = [[0.5,0.5,0.5],[0.0,1.0,0.0],[1.0,1.0,1.0],[1.0,1.0,0.0],[0.9,0.9,0.9],[1.0,0.502,0.0],[0.6,0.6,0.6],[1.0,0.0,0.0],[0.0,0.0,0.0]]

   myrange = mymax - mymin
   
   for i in range(0,numColorNodes-1):
      currFloor = mymin + ( float(i    ) / float(numColorNodes - 1)) * myrange
      currCeil  = mymin + ( float(i + 1) / float(numColorNodes - 1)) * myrange

      if myval >= currFloor and myval <= currCeil:
         currFraction = (myval - currFloor) / (currCeil - currFloor)
         r = color[i][0] * (1.0 - currFraction) + color[i + 1][0] * currFraction
         g = color[i][1] * (1.0 - currFraction) + color[i + 1][1] * currFraction
         b = color[i][2] * (1.0 - currFraction) + color[i + 1][2] * currFraction

   return r,g,b

###############################################################################################


total_arg  = len(sys.argv)
filename   = str(sys.argv[1])
simulation = "S5"
dt         = 1.0e-4

xe=1414.6
ye=10226.0
mw="5.5"


reader = vtk.vtkXMLStructuredGridReader()
reader.SetFileName(filename)
reader.Update() # by calling Update() we read the file

# get array name
var1  = reader.GetOutput().GetPointData().GetArrayName(0)
var2  = reader.GetOutput().GetPointData().GetArrayName(1)

myfunction = 'sqrt(' + var1 + '^2 + ' + var2 + '^2)'

calc = vtk.vtkArrayCalculator()
calc.SetInputConnection(reader.GetOutputPort())
calc.SetAttributeModeToUsePointData()
calc.AddScalarArrayName(var1)
calc.AddScalarArrayName(var2)
calc.SetResultArrayName('Result')
calc.SetFunction(myfunction)
calc.Update()


# get origin, extend and spacing
extend  = calc.GetOutput().GetExtent()
bounds  = calc.GetOutput().GetBounds()
xl = (bounds[1]-bounds[0])
yl = (bounds[3]-bounds[2])
xc = xl/2.0
yc = yl/2.0

## get array name and min, max
nm  = calc.GetOutput().GetPointData().GetArrayName(3)
minval,maxval = calc.GetOutput().GetPointData().GetScalars(nm).GetRange()
minval=1.0e-1
maxval=1.0e+1

# lookup table
numColors = 256
lut = vtk.vtkLogLookupTable()
lut.SetRange(minval,maxval)
lut.SetNumberOfTableValues(numColors)

for i in range(0,numColors):
   val = minval + (float(i) / float(numColors-1)) * (maxval-minval)
   r,g,b = getColorCorrespondingTovalue(val,minval,maxval)
   lut.SetTableValue(i,r,g,b)

lut.Build()

# set epicenter as a sphere
epicenter = vtk.vtkPoints()
epicenter.InsertPoint(0,xe,ye,400.0)
epicenterData = vtk.vtkPolyData()
epicenterData.SetPoints(epicenter)
gs = vtk.vtkSphereSource()
gs.SetRadius(1.0)
gs.SetPhiResolution(20)
gs.SetThetaResolution(20)
glyph = vtk.vtkGlyph3D()
glyph.SetInputData(epicenterData)
glyph.SetSourceConnection(gs.GetOutputPort())
glyph.SetScaleFactor(300.0)
glyphMapper = vtk.vtkPolyDataMapper()
glyphMapper.SetInputConnection(glyph.GetOutputPort())
glyphActor = vtk.vtkActor()
glyphActor.SetMapper(glyphMapper)
glyphActor.GetProperty().SetColor(0.0,0.0,0.0)
epicenterText = vtk.vtkTextActor()
#epicenterText.SetInput("Epicenter")
epicenterText.GetTextProperty().SetColor(0.0,0.0,0.0)
epicenterText.GetTextProperty().SetJustificationToLeft()
epicenterText.GetTextProperty().SetFontSize(15)
#epicenterText.GetTextProperty().ShadowOn()
epicenterTextCoord = epicenterText.GetPositionCoordinate()
epicenterTextCoord.SetCoordinateSystemToWorld()
epicenterTextCoord.SetValue(xe,ye,100.0)


# set distance scale
distanceScale = vtk.vtkPoints()
distanceScale.InsertPoint(0,3000.0,1000.0,100.0)
distanceScaleData = vtk.vtkPolyData()
distanceScaleData.SetPoints(distanceScale)
distanceScaleGlyph = vtk.vtkCubeSource()
distanceScaleGlyph.SetXLength(5000.0)
distanceScaleGlyph.SetYLength( 200.0)
distanceScaleGlyph.SetZLength( 100.0)
glyphCube = vtk.vtkGlyph3D()
glyphCube.SetInputData(distanceScaleData)
glyphCube.SetSourceConnection(distanceScaleGlyph.GetOutputPort())
glyphCubeMapper = vtk.vtkPolyDataMapper()
glyphCubeMapper.SetInputConnection(glyphCube.GetOutputPort())
glyphCubeActor = vtk.vtkActor()
glyphCubeActor.SetMapper(glyphCubeMapper)
glyphCubeActor.GetProperty().SetColor(0.0,0.0,0.0)
distanceScaleText = vtk.vtkTextActor()
distanceScaleText.SetInput("5 km")
distanceScaleText.GetTextProperty().SetColor(0.0,0.0,0.0)
distanceScaleText.GetTextProperty().SetJustificationToCentered()
distanceScaleText.GetTextProperty().SetFontSize(25)
distanceScaleTextCoord = distanceScaleText.GetPositionCoordinate()
distanceScaleTextCoord.SetCoordinateSystemToWorld()
distanceScaleTextCoord.SetValue(3000.0,1300.0,100.0)

# computed by EFI
efiText = vtk.vtkTextActor()
efiText.SetInput("Euroseistest Verification and Validation Project. Simulation performed by EFISPEC3D (http://efispec.free.fr) on Froggy high-performance architecture")
efiText.GetTextProperty().SetColor(1.0,1.0,1.0)
efiText.GetTextProperty().SetJustificationToCentered()
efiText.GetTextProperty().SetFontSize(20)
efiTextCoord = efiText.GetPositionCoordinate()
efiTextCoord.SetCoordinateSystemToNormalizedViewport()
efiTextCoord.SetValue(0.5,0.98)

# VTK
vtkText = vtk.vtkTextActor()
vtkText.SetInput("Figure generated by VTK")
vtkText.GetTextProperty().SetColor(0.0,0.0,0.0)
vtkText.GetTextProperty().SetJustificationToRight()
vtkText.GetTextProperty().SetFontSize(15)
vtkTextCoord = vtkText.GetPositionCoordinate()
vtkTextCoord.SetCoordinateSystemToNormalizedViewport()
vtkTextCoord.SetValue(0.99,0.01)

# Time info
frame_number = GetFrameNumber(filename)
time         = float(int(frame_number)-1)*dt
timeInfo     = "Time = {:4.2f} s".format(time)
timeText     = vtk.vtkTextActor()
timeText.SetInput(timeInfo)
timeText.GetTextProperty().SetColor(0.0,0.0,0.0)
timeText.GetTextProperty().SetJustificationToRight()
timeText.GetTextProperty().SetFontSize(25)
timeTextCoord = timeText.GetPositionCoordinate()
timeTextCoord.SetCoordinateSystemToNormalizedViewport()
timeTextCoord.SetValue(0.97,0.02)

# mapper
mapper = vtk.vtkDataSetMapper()
mapper.SetLookupTable(lut)
mapper.SetScalarRange(minval,maxval)
mapper.SetInputData(calc.GetOutput())
mapper.SetScalarModeToUsePointFieldData()
mapper.ScalarVisibilityOn()
mapper.SelectColorArray(3)

# actor
myActor = vtk.vtkActor()
myActor.SetMapper(mapper)

# scalarbar background property
scalarBarBackgroundProp = vtk.vtkProperty2D()
scalarBarBackgroundProp.SetColor(0.9,0.9,0.9)
#scalarBarBackgroundProp.SetColor(1.0,1.0,1.0)

# text property
textProp = vtk.vtkTextProperty()
textProp.SetFontSize(10)
textProp.SetColor(0,0,0)

labelProp = vtk.vtkTextProperty()
labelProp.SetFontSize(10)
labelProp.SetColor(0,0,0)

# a colorbar to display the colormap
scalarBar = vtk.vtkScalarBarActor()
scalarBar.SetLookupTable(mapper.GetLookupTable())
scalarBar.SetTitle("Horizontal velocity (m/s) generated by an earthquake of magnitude Mw="+mw)
scalarBar.SetOrientationToHorizontal()
scalarBar.SetWidth(0.9)
scalarBar.SetHeight(0.08)
scalarBar.GetLabelTextProperty().SetColor(0,0,0)
scalarBar.GetTitleTextProperty().SetColor(0,0,0)
scalarBar.GetTitleTextProperty().ShadowOff()
scalarBar.GetLabelTextProperty().ShadowOff()
scalarBar.SetNumberOfLabels(5)
#scalarBar.SetLabelFormat("%-#4.1e")
scalarBar.SetLabelFormat("%-#4.1e")
scalarBar.SetMaximumNumberOfColors(256)
scalarBar.DrawBackgroundOn()
scalarBar.DrawTickLabelsOn()
scalarBar.SetBackgroundProperty(scalarBarBackgroundProp)
scalarBar.SetTitleTextProperty(textProp)
scalarBar.SetLabelTextProperty(labelProp)

# position scalar bar in viewport
coord = scalarBar.GetPositionCoordinate()
coord.SetCoordinateSystemToNormalizedViewport()
coord.SetValue(0.05,0.90)

#camera
camera = vtk.vtkCamera()
camera.SetFocalPoint(xc,yc,0.0)
camera.SetPosition(xc,yc,30000.0)

# renderer and render window 
ren = vtk.vtkRenderer()
ren.SetBackground(1,1,1)
ren.SetActiveCamera(camera)

renWin = vtk.vtkRenderWindow()
renWin.SetOffScreenRendering(1)
xsize = int(xl/10.0)
ysize = int(yl/10.0)
renWin.SetSize(xsize,ysize)
renWin.AddRenderer(ren)

# render window interactor. Interactor must be removed if renWin.SetOffScreenRendering(1)
#iren = vtk.vtkRenderWindowInteractor()
#iren.SetRenderWindow(renWin)

# add the actors to the renderer
ren.AddViewProp(myActor)
ren.AddViewProp(scalarBar)
ren.AddViewProp(glyphActor)
ren.AddViewProp(glyphCubeActor)
ren.AddViewProp(distanceScaleText)
ren.AddViewProp(epicenterText)
ren.AddViewProp(timeText)
ren.AddViewProp(efiText)
#ren.AddViewProp(vtkText)
ren.ResetCameraClippingRange()

# render and initialize and start the interactor
#iren.Initialize()
#renWin.Render()
#iren.Start()

# write file
w2i = vtk.vtkWindowToImageFilter()
w2i.SetInput(renWin)
w2i.Update()
writer = vtk.vtkPNGWriter()
writer.SetFileName(filename+'.png')
writer.SetInputConnection(w2i.GetOutputPort())
#renWin.Render()
writer.Write()

exit()
