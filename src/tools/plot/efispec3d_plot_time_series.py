#!/usr/bin/python

import sys

import matplotlib.pyplot as plt

import numpy as np

import scipy.signal as signal

#######################################################################################################################################################
def read_efi_binary_time_series(myfile):

   """read binary file of EFISPEC3D's time series"""

   dt = np.dtype([('time','f4'),('ux','f4'),('uy','f4'),('uz','f4'),('vx','f4'),('vy','f4'),('vz','f4'),('ax','f4'),('ay','f4'),('az','f4')])

   with open(myfile,"rb") as f:
      x = np.fromfile(f,dtype=dt)

   return x
#######################################################################################################################################################

total_arg  = len(sys.argv)

if total_arg != 3:
   print "script usage: "+str(sys.argv[0])+" EFISPEC3D_file.gpl dis/vel/acc"
   quit()

filename = str(sys.argv[1])
dva      = str(sys.argv[2])

time_series = read_efi_binary_time_series(filename)

time = time_series['time'] #not a copy, just a 'pointer'
ux   = time_series['ux']
uy   = time_series['uy']
uz   = time_series['uz']
vx   = time_series['vx']
vy   = time_series['vy']
vz   = time_series['vz']
ax   = time_series['ax']
ay   = time_series['ay']
az   = time_series['az']

#Time series info
dt  = time[1] - time[0]
fs  = np.floor(1.0/dt)

#Filter design
Nyq   = fs*0.5
order = 4
fcut  = 5.0
B,A   = signal.butter(N=order,Wn=fcut/Nyq,btype='lowpass',output='ba')

#Filter data
uxf = signal.filtfilt(B,A,ux)
uyf = signal.filtfilt(B,A,uy)
uzf = signal.filtfilt(B,A,uz)
vxf = signal.filtfilt(B,A,vx)
vyf = signal.filtfilt(B,A,vy)
vzf = signal.filtfilt(B,A,vz)
axf = signal.filtfilt(B,A,ax)
ayf = signal.filtfilt(B,A,ay)
azf = signal.filtfilt(B,A,az)

#Make plot
fig = plt.figure()

if dva == "dis":
   #subplot ux
   fig.add_subplot(3,1,1)
   plt.plot(time,ux ,'r')
   plt.plot(time,uxf,'b')
   plt.ylabel("Ux (m)")
   #subplot uy
   fig.add_subplot(3,1,2)
   plt.plot(time,uy ,'r')
   plt.plot(time,uyf,'b')
   plt.ylabel("Uy (m)")
   #subplot uy
   fig.add_subplot(3,1,3)
   plt.plot(time,uy ,'r')
   plt.plot(time,uyf,'b')
   plt.ylabel("Uz (m)")
   plt.show()

if dva == "vel":
   #subplot vx
   fig.add_subplot(3,1,1)
   plt.plot(time,vx ,'r')
   plt.plot(time,vxf,'g')
   plt.ylabel("Vx (m/s)")
   #subplot vy
   fig.add_subplot(3,1,2)
   plt.plot(time,vy ,'r')
   plt.plot(time,vyf,'g')
   plt.ylabel("Vy (m/s)")
   #subplot vz
   fig.add_subplot(3,1,3)
   plt.plot(time,vz ,'r')
   plt.plot(time,vzf,'g')
   plt.ylabel("Vz (m/s)")
   plt.show()
   
if dva == "acc":
   #subplot ax
   fig.add_subplot(3,1,1)
   plt.plot(time,ax ,'r')
   plt.plot(time,axf,'y')
   plt.ylabel("Ax (m/s/s)")
   #subplot ay
   fig.add_subplot(3,1,2)
   plt.plot(time,ay ,'r')
   plt.plot(time,ayf,'y')
   plt.ylabel("Ay (m/s/s)")
   #subplot az
   fig.add_subplot(3,1,3)
   plt.plot(time,az ,'r')
   plt.plot(time,azf,'y')
   plt.ylabel("Az (m/s/s)")
   plt.show()
