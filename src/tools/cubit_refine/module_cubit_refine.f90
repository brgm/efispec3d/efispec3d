!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
module mod_cubit_refine

   use mod_precision

   implicit none

   private

   public  :: read_topography
   public  :: compute_z_dem
   private :: bilinear_interp
   private :: pos
   public  :: set_element_convention
   public  :: read_curve
   public  :: get_hexa_centroid
   private :: lagrap_geom
   public  :: in_poly_curve
   private :: get_local_node_from_global

   contains

!
!
!***********************************************************************************************************************************************************************************
   subroutine read_topography(fname,x,y,z,dx,dy,x0,y0,nx,ny)
!***********************************************************************************************************************************************************************************
   
      implicit none

      real(kind=RXP), parameter                :: EPSILON_MACHINE = epsilon(EPSILON_MACHINE)
   
      character(len=90), intent( in) :: fname

      real(kind=RXP), allocatable, intent(out) :: x(:)
      real(kind=RXP), allocatable, intent(out) :: y(:)
      real(kind=RXP), allocatable, intent(out) :: z(:,:)
      real(kind=RXP)             , intent(out) :: dx
      real(kind=RXP)             , intent(out) :: dy
      real(kind=RXP)             , intent(out) :: x0
      real(kind=RXP)             , intent(out) :: y0

      integer(kind=IXP)          , intent(out) :: nx
      integer(kind=IXP)          , intent(out) :: ny
   
      real(kind=RXP)                           :: nodata
      integer(kind=IXP)                        :: ix
      integer(kind=IXP)                        :: iy
      character(len=12)              :: ctmp
   
   
      write(*,'(a)') "Reading topography from file"//trim(fname)
   
      open(unit=10,file=trim(adjustl(fname)))
   
      read(unit=10,fmt=*) ctmp,nx
      read(unit=10,fmt=*) ctmp,ny
      read(unit=10,fmt=*) ctmp,x0
      read(unit=10,fmt=*) ctmp,y0
      read(unit=10,fmt=*) ctmp,dx
      read(unit=10,fmt=*) ctmp,nodata
   
      dy = dx

      write(*,'(a,I0    )') "ncols     = ",nx
      write(*,'(a,I0    )') "nrows     = ",ny
      write(*,'(a,F22.10)') "xllcorner = ",x0
      write(*,'(a,F22.10)') "yllcorner = ",y0
      write(*,'(a,F22.10)') "cellsize  = ",dx
   

      write(*,'(a)') ""
      write(*,'(a)') "******************************************************************************************************"
      write(*,'(a)') "WARNING: xllcorner and yllcorner flushed to zero to avoid precision problem with single precision real(kind=RXP)"
      write(*,'(a)') "******************************************************************************************************"
      x0 = 0.0
      y0 = 0.0
      write(*,'(a,F22.10)') "xllcorner = ",x0
      write(*,'(a,F22.10)') "yllcorner = ",y0
 
      allocate(x(nx),y(ny),z(nx,ny))
   
      do ix = 1,nx
         x(ix) = x0 + real(ix-1,kind=RXP)*dx
      enddo
   
      do iy = 1,ny
         y(iy) = y0 + real(ny-1,kind=RXP)*dy - real(iy-1,kind=RXP)*dy
      enddo
   
      do iy = 1,ny
         read(unit=10,fmt=*) (z(ix,iy),ix=1,nx)
      enddo

      close(10)

!
!---->modify nodata value to HUGE real(kind=RXP) (more adapted than -9999 generally used)
      write(*,'(a)') ""
      write(*,'(a)') "******************************************************************************************************"
      write(*,'(a)') "WARNING: nodata value modified to HUGE(nodata)"
      write(*,'(a)') "******************************************************************************************************"

      do iy = 1,ny
         do ix = 1,nx

           if ( abs(z(ix,iy)) >= abs(nodata)-EPSILON_MACHINE ) z(ix,iy) = HUGE(nodata)

         enddo
      enddo
   
      return
   
!***********************************************************************************************************************************************************************************
   end subroutine read_topography
!***********************************************************************************************************************************************************************************


!
!
!***********************************************************************************************************************************************************************************
   subroutine compute_z_dem(x_node,y_node,z_dem_interp,x_dem,y_dem,z_dem,nx_dem,ny_dem)
!***********************************************************************************************************************************************************************************
   
      implicit none
   
      real(kind=RXP)             , intent(in   ) :: x_node
      real(kind=RXP)             , intent(in   ) :: y_node
      real(kind=RXP)             , intent(  out) :: z_dem_interp
      integer(kind=IXP)          , intent(in   ) :: nx_dem
      integer(kind=IXP)          , intent(in   ) :: ny_dem
      real(kind=RXP)             , intent(in   ) :: x_dem(nx_dem)
      real(kind=RXP)             , intent(in   ) :: y_dem(ny_dem)
      real(kind=RXP)             , intent(in   ) :: z_dem(nx_dem,ny_dem)
   
      integer(kind=IXP)                          :: iloc
      integer(kind=IXP)                          :: jloc
   
      call pos(x_dem,nx_dem,x_node,iloc)
      if ( (iloc == 0) .or. (iloc >= nx_dem) ) then
         write(*,*) "error: iloc = ",iloc
         stop
      endif
   
      call pos(y_dem,ny_dem,y_node,jloc)
      if ( (jloc == 0) .or. (jloc >= ny_dem) ) then
         write(*,*) "error: jloc = ",jloc
         stop
      endif
   
      call bilinear_interp(x_node,y_node,z_dem_interp,x_dem,y_dem,z_dem,nx_dem,ny_dem,iloc,iloc+1,jloc,jloc+1)
   
      return
   
!***********************************************************************************************************************************************************************************
   end subroutine compute_z_dem
!***********************************************************************************************************************************************************************************


!
!
!***********************************************************************************************************************************************************************************
   subroutine bilinear_interp(xn,yn,z,x,y,array,nx,ny,jx,jxp1,jy,jyp1)
!***********************************************************************************************************************************************************************************
   
      implicit none
   
      real(kind=RXP)   , intent( in)      :: xn
      real(kind=RXP)   , intent( in)      :: yn
      real(kind=RXP)   , intent(out)      :: z
   
      real(kind=RXP)   , dimension(nx)    :: x
      real(kind=RXP)   , dimension(ny)    :: y
      real(kind=RXP)   , dimension(nx,ny) :: array
   
      integer(kind=IXP), intent( in)      :: nx
      integer(kind=IXP), intent( in)      :: ny
   
      integer(kind=IXP), intent( in)      :: jx
      integer(kind=IXP), intent( in)      :: jxp1
      integer(kind=IXP), intent( in)      :: jy
      integer(kind=IXP), intent( in)      :: jyp1
                                
      real(kind=RXP)                      :: s1
      real(kind=RXP)                      :: s2
      real(kind=RXP)                      :: s3
      real(kind=RXP)                      :: s4
      real(kind=RXP)                      :: zt
      real(kind=RXP)                      :: zta
      real(kind=RXP)                      :: ztb
      real(kind=RXP)                      :: zu
      real(kind=RXP)                      :: zua
      real(kind=RXP)                      :: zub
   !
   !--------------------------------------------------------------------
   !
      s1 = array(jx  ,jy  )
      s2 = array(jxp1,jy  )
      s3 = array(jxp1,jyp1)
      s4 = array(jx  ,jyp1)
   !
   !  find slopes used in interpolation
   !  i) x.
      zta = xn - x(jx)
      ztb = x(jxp1) - x(jx)
      zt  = zta/ztb
   !
   !  ii) y.
      zua = yn - y(jy)
      zub = y(jyp1) - y(jy)
      zu  = zua/zub
   !
   !  use bilinear interpolation
      z = (1.0-zt)*(1.0-zu)*s1 + zt*(1.0-zu)*s2 + zt*zu*s3 + (1.0-zt)*zu*s4
   
      return
   
!***********************************************************************************************************************************************************************************
   end subroutine bilinear_interp
!***********************************************************************************************************************************************************************************

!
!   
!***********************************************************************************************************************************************************************************
   subroutine pos(xx,n,x,j)
!***********************************************************************************************************************************************************************************
   
      implicit none
   
      integer(kind=IXP), intent( in) :: n
      real(kind=RXP)   , intent( in) :: x
      real(kind=RXP)   , intent( in) :: xx(n)
      integer(kind=IXP), intent(out) :: j
   
      integer(kind=IXP) :: jl
      integer(kind=IXP) :: jm
      integer(kind=IXP) :: ju
   !
   !  initialize upper & lower limits.
      jl = 0
      ju = n + 1
   
      do while ( .true. )
   
         if ( .not..true. ) then
            return
   
         elseif ( ju-jl>1 ) then
   
            jm = (ju+jl)/2
            if ( xx(n)>xx(1) .eqv. x>xx(jm) ) then
               jl = jm
            else
               ju = jm
            endif
   !
   !        repeat untill the test condition is satisfied.
            cycle
         endif
   !
   !     set the output.
   !FDM
         if (jl >= n) jl = n - 1
         if (jl <= 0) jl = 1
   !FDM
         j = jl
         exit
   
      enddo
   
!***********************************************************************************************************************************************************************************
   end subroutine pos
!***********************************************************************************************************************************************************************************


!***********************************************************************************************************************************************************************************
subroutine set_element_convention(xiloc,etloc,zeloc,coordloc,absci)
!***********************************************************************************************************************************************************************************

   implicit none
      
   integer(kind=IXP), intent(out) :: xiloc(:)
   integer(kind=IXP), intent(out) :: etloc(:)
   integer(kind=IXP), intent(out) :: zeloc(:)
   real(kind=RXP)   , intent(out) :: coordloc(:)
   real(kind=RXP)   , intent(out) :: absci(:,:)

   integer(kind=IXP)              :: i
   integer(kind=IXP)              :: j

!
!->local coordinate of geometric nodes
   coordloc(1) = +1.0
   coordloc(2) = -1.0
!
!->local position of geometric nodes (see cubit convention)
   xiloc(1) = 1
   etloc(1) = 1
   zeloc(1) = 1

   xiloc(2) = 1
   etloc(2) = 2
   zeloc(2) = 1

   xiloc(3) = 2
   etloc(3) = 2
   zeloc(3) = 1

   xiloc(4) = 2
   etloc(4) = 1
   zeloc(4) = 1

   xiloc(5) = 1
   etloc(5) = 1
   zeloc(5) = 2

   xiloc(6) = 1
   etloc(6) = 2
   zeloc(6) = 2

   xiloc(7) = 2
   etloc(7) = 2
   zeloc(7) = 2

   xiloc(8) = 2
   etloc(8) = 1
   zeloc(8) = 2

!
!->compute inverse of distance between geometric nodes

   do i = 1,size(coordloc)
      do j = 1,size(coordloc)

         absci(j,i) = 0.0

         if (i /= j) absci(j,i) = 1.0/(coordloc(i) - coordloc(j))

      enddo
   enddo

   return

!***********************************************************************************************************************************************************************************
end subroutine set_element_convention
!***********************************************************************************************************************************************************************************


!***********************************************************************************************************************************************************************************
subroutine read_curve(f,n,x,y)
!***********************************************************************************************************************************************************************************

   implicit none

   character(len= 90), intent( in) :: f
   integer(kind=IXP)           , intent(out) :: n
   real(kind=RXP), allocatable , intent(out) :: x(:)
   real(kind=RXP), allocatable , intent(out) :: y(:)
   real(kind=RXP)                            :: rtmp

   integer(kind=IXP)                         :: i
   integer(kind=IXP)                         :: ios


!
!->first pass to count the number of data
   n = 0
   open(unit=1,file=trim(adjustl(f)))
   do
      read(unit=1,fmt=*,iostat=ios) rtmp
      if (ios /= 0) exit
      n = n + 1
   enddo
   rewind(1)

!
!->memory allocation
   n = n + 1
   allocate(x(n),y(n))

!
!->second pass to read x,y coordinates
   do i = 1,n-1
      read(unit=1,fmt=*) x(i),y(i)
   enddo 
   close(1)

!
!->close the curve
   x(n) = x(1)
   y(n) = y(1)

   return

!***********************************************************************************************************************************************************************************
end subroutine read_curve
!***********************************************************************************************************************************************************************************

!***********************************************************************************************************************************************************************************
subroutine  get_hexa_centroid(xiloc,etloc,zeloc,absci,coloc,xn,yn,zn,x,y,z)
!***********************************************************************************************************************************************************************************

   implicit none

   integer(kind=IXP), intent( in) :: xiloc(:)
   integer(kind=IXP), intent( in) :: etloc(:)
   integer(kind=IXP), intent( in) :: zeloc(:)
   real(kind=RXP)   , intent( in) :: absci(:,:)
   real(kind=RXP)   , intent( in) :: coloc(:)
   real(kind=RXP)   , intent( in) :: xn(:)
   real(kind=RXP)   , intent( in) :: yn(:)
   real(kind=RXP)   , intent( in) :: zn(:)
            
   real(kind=RXP)   , intent(out) :: x
   real(kind=RXP)   , intent(out) :: y
   real(kind=RXP)   , intent(out) :: z

   real(kind=RXP), parameter      :: ZERO = 0.0
   real(kind=RXP)                 :: local_lagrap1
   real(kind=RXP)                 :: local_lagrap2
   real(kind=RXP)                 :: local_lagrap3

   integer(kind=IXP)              :: i
   integer(kind=IXP)              :: n_node_geom
   integer(kind=IXP)              :: n_node_geom_loc

   n_node_geom     = size(xiloc)
   n_node_geom_loc = size(coloc)

   x = 0.0
   y = 0.0
   z = 0.0

   do i = 1,n_node_geom

      local_lagrap1 = lagrap_geom(xiloc(i),ZERO,n_node_geom_loc,coloc,absci)
      local_lagrap2 = lagrap_geom(etloc(i),ZERO,n_node_geom_loc,coloc,absci)
      local_lagrap3 = lagrap_geom(zeloc(i),ZERO,n_node_geom_loc,coloc,absci)

      x = x + local_lagrap1*local_lagrap2*local_lagrap3*xn(i)
      y = y + local_lagrap1*local_lagrap2*local_lagrap3*yn(i)
      z = z + local_lagrap1*local_lagrap2*local_lagrap3*zn(i)

   enddo

   return

!***********************************************************************************************************************************************************************************
end subroutine get_hexa_centroid
!***********************************************************************************************************************************************************************************

!
!
!***********************************************************************************************************************************************************************************
real(kind=RXP) function lagrap_geom(i,x,n,c,a)
!***********************************************************************************************************************************************************************************
!---->compute lagrange polynomial

   implicit none

   integer(kind=IXP), intent(in) :: i !local position (i.e. node) in the line [-1:1] where lagrange polynomial = 1 (0 at others nodes)
   real(kind=RXP)   , intent(in) :: x !abscissa where the polynomial is calculated
   integer(kind=IXP), intent(in) :: n !number of nodes in the line [-1:1]
   real(kind=RXP)   , intent(in) :: c(n)
   real(kind=RXP)   , intent(in) :: a(n,n)

   integer(kind=IXP)             :: j

   lagrap_geom = 1.0

   do j = 1,n
      if (j.ne.i) lagrap_geom = lagrap_geom*( (x-c(j))*a(j,i) )
   enddo

!***********************************************************************************************************************************************************************************
end function lagrap_geom
!***********************************************************************************************************************************************************************************


!
!
!***********************************************************************************************************************************************************************************
SUBROUTINE in_poly_curve(X0, Y0, X, Y, N, L, M)
!***********************************************************************************************************************************************************************************
   !-----------------------------------------------------------------------
   ! GIVEN A POLYGONAL LINE CONNECTING THE VERTICES (X(I),Y(I)) (I = 1,...,N) TAKEN IN THIS ORDER.  
   ! IT IS ASSUMED THAT THE POLYGONAL PATH IS A LOOP, WHERE (X(N),Y(N)) = (X(1),Y(1)) OR THERE IS 
   ! AN ARC FROM (X(N),Y(N)) TO (X(1),Y(1)).  
   !
   ! N.B. THE POLYGON MAY CROSS ITSELF ANY NUMBER OF TIMES.
   !
   ! (X0,Y0) IS AN ARBITRARY POINT AND L AND M ARE VARIABLES.
   ! ON OUTPUT, L AND M ARE ASSIGNED THE FOLLOWING VALUES:
   !    L = -1   IF (X0,Y0) IS OUTSIDE THE POLYGONAL PATH
   !    L =  0   IF (X0,Y0) LIES ON THE POLYGONAL PATH
   !    L =  1   IF (X0,Y0) IS INSIDE THE POLYGONAL PATH
   !    M =  0   IF (X0,Y0) IS ON OR OUTSIDE THE PATH.  IF (X0,Y0) IS INSIDE THE
   !             PATH THEN M IS THE WINDING NUMBER OF THE PATH AROUND THE POINT (X0,Y0).
   !
   ! FORTRAN 66 VERSION BY A.H. MORRIS
   ! CONVERTED TO ELF90 COMPATIBILITY BY Alan MILLER, 15 FEBRUARY 1997
   !
   IMPLICIT NONE
   !
   REAL            , INTENT(IN)  :: X0, Y0, X(N), Y(N)
   INTEGER         , INTENT(IN)  :: N
   INTEGER         , INTENT(OUT) :: L, M
   
   !LOCAL VARIABLES
   INTEGER                       :: I, N0
   REAL                          :: ANGLE
   REAL                          :: EPS
   REAL                          :: PI
   REAL                          :: PI2
   REAL                          :: SUM
   REAL                          :: THETA
   REAL                          :: THETA1
   REAL                          :: THETAI
   REAL                          :: TOL
   REAL                          :: U
   REAL                          :: V
   
   !     ****** EPS IS A MACHINE DEPENDENT CONSTANT. EPS IS THE
   !            SMALLEST NUMBER SUCH THAT 1.0 + EPS > 1.0
   !
   EPS = EPSILON(1.0)
   !
   !-----------------------------------------------------------------------
   N0 = N
   IF (X(1) == X(N) .AND. Y(1) == Y(N)) N0 = N - 1
   PI  = ATAN2(0.0, -1.0)
   PI2 = 2.0*PI
   TOL = 4.0*EPS*PI
   L   = -1
   M   =  0
   !
   U = X(1) - X0
   V = Y(1) - Y0
   IF (U == 0.0 .AND. V == 0.0) GO TO 20
   IF (N0 < 2) RETURN
   THETA1 = ATAN2(V, U)
   !
   SUM   = 0.0
   THETA = THETA1
   DO I = 2, N0
     U = X(I) - X0
     V = Y(I) - Y0
     IF (U == 0.0 .AND. V == 0.0) GO TO 20
     THETAI = ATAN2(V, U)
   !  
     ANGLE = ABS(THETAI - THETA)
     IF (ABS(ANGLE - PI) < TOL) GO TO 20
     IF (ANGLE > PI    ) ANGLE = ANGLE - PI2
     IF (THETA > THETAI) ANGLE = -ANGLE
     SUM   = SUM + ANGLE
     THETA = THETAI
   END DO
   !
   ANGLE = ABS(THETA1 - THETA)
   IF (ABS(ANGLE - PI) < TOL) GO TO 20
   IF (ANGLE > PI    ) ANGLE = ANGLE - PI2
   IF (THETA > THETA1) ANGLE = -ANGLE
   SUM = SUM + ANGLE
   !
   !SUM = 2*PI*M WHERE M IS THE WINDING NUMBER
   M = ABS(SUM)/PI2 + 0.2
   IF (M == 0) RETURN
   L = 1
   IF (SUM < 0.0) M = -M
   RETURN
   !
   !(X0, Y0) IS ON THE BOUNDARY OF THE PATH
   20 L = 0

   RETURN
!***********************************************************************************************************************************************************************************
END SUBROUTINE in_poly_curve
!***********************************************************************************************************************************************************************************

!***********************************************************************************************************************************************************************************
integer(kind=IXP) function get_local_node_from_global(target_num,a,n)
!***********************************************************************************************************************************************************************************

   implicit none

   integer(kind=IXP),               intent(in) :: n
   integer(kind=IXP), dimension(n), intent(in) :: a
   integer(kind=IXP),               intent(in) :: target_num

   integer(kind=IXP)                           :: i

   do i = 1,n

      if (a(i) == target_num) then
         get_local_node_from_global = i
         exit
      endif

   enddo

!***********************************************************************************************************************************************************************************
end function get_local_node_from_global
!***********************************************************************************************************************************************************************************

end module mod_cubit_refine
