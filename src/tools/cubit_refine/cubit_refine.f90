!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                                  !
!Author : Florent DE MARTIN, July 2013                                             !
!                                                                                  !
!Contact: f.demartin at brgm.fr                                                    !
!                                                                                  !
!This program is free software: you can redistribute it and/or modify it under     !
!the terms of the GNU General Public License as published by the Free Software     !
!Foundation, either version 3 of the License, or (at your option) any later        !
!version.                                                                          !
!                                                                                  !
!This program is distributed in the hope that it will be useful, but WITHOUT ANY   !
!WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A   !
!PARTICULAR PURPOSE. See the GNU General Public License for more details.          !
!                                                                                  !
!You should have received a copy of the GNU General Public License along with      !
!this program. If not, see http://www.gnu.org/licenses/.                           !
!                                                                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program cubit_curve_refining

   use mod_precision

   use mod_init_memory

   use mod_cubit_refine

   use mod_topography
   
   implicit none

   include 'exodusII.inc'

   integer(kind=IXP), parameter                 :: NHEX_GROUP        = 1000
   integer(kind=IXP), parameter                 :: N_GEOM_NODE_HEXA  = 8
   integer(kind=IXP), parameter                 :: N_GEOM_NODE_LOC   = 2
   integer(kind=IXP), parameter                 :: N_HEXA_INSIDE_MAX = 1000000
                                                
   real(kind=RXP), dimension(:), allocatable    :: x_geom_node !>X coordinates of geometrical nodes
   real(kind=RXP), dimension(:), allocatable    :: y_geom_node !>Y coordinates of geometrical nodes
   real(kind=RXP), dimension(:), allocatable    :: z_geom_node !>Z coordinates of geometrical nodes
                                                
   real(kind=RXP)                               :: x0_dem
   real(kind=RXP)                               :: y0_dem
   real(kind=RXP)                               :: dx_dem
   real(kind=RXP)                               :: dy_dem
   real(kind=RXP)                               :: z_dem_interp
   real(kind=RXP)                               :: ex_version
                                                
   real(kind=RXP), dimension(:)  , allocatable  :: x_dem
   real(kind=RXP), dimension(:)  , allocatable  :: y_dem
   real(kind=RXP), dimension(:,:), allocatable  :: z_dem
                                                
   integer(kind=IXP)                            :: nx_dem
   integer(kind=IXP)                            :: ny_dem
                                                
   real(kind=RXP)                               :: x_node_hexa(N_GEOM_NODE_HEXA)
   real(kind=RXP)                               :: y_node_hexa(N_GEOM_NODE_HEXA)
   real(kind=RXP)                               :: z_node_hexa(N_GEOM_NODE_HEXA)
                                                
   real(kind=RXP)                               :: depth
   real(kind=RXP)                               :: zmin
                                                
   real(kind=RXP)                               :: x_centroid
   real(kind=RXP)                               :: y_centroid
   real(kind=RXP)                               :: z_centroid
                                                
   real(kind=RXP), allocatable                  :: xcurve(:),ycurve(:)
                                                
   integer(kind=IXP), allocatable               :: connectivity(:)
                                                
   integer(kind=IXP)                            :: xi_loc_gnode_hexa(N_GEOM_NODE_HEXA)
   integer(kind=IXP)                            :: et_loc_gnode_hexa(N_GEOM_NODE_HEXA)
   integer(kind=IXP)                            :: ze_loc_gnode_hexa(N_GEOM_NODE_HEXA)
                                                
   integer(kind=IXP)                            :: hexa_inside(N_HEXA_INSIDE_MAX)
                                                
   real(kind=RXP)                               :: co_loc_gnode_hexa(N_GEOM_NODE_LOC)
   real(kind=RXP)                               :: abscissa(N_GEOM_NODE_LOC,N_GEOM_NODE_LOC)
                                                
   integer(kind=IXP)                            :: ndim
   integer(kind=IXP)                            :: ndata_curve
   integer(kind=IXP)                            :: n_hexa_inside
   integer(kind=IXP)                            :: number_of_nodes
   integer(kind=IXP)                            :: number_of_elements
   integer(kind=IXP)                            :: number_of_blocks
   integer(kind=IXP)                            :: number_of_node_sets
   integer(kind=IXP)                            :: number_of_side_sets
   integer(kind=IXP)                            :: nblock
   integer(kind=IXP)                            :: iblock
   integer(kind=IXP)                            :: igroup
   integer(kind=IXP)                            :: mynode
   integer(kind=IXP)                            :: inode
   integer(kind=IXP)                            :: ihexa
   integer(kind=IXP)                            :: narg
   integer(kind=IXP)                            :: number_of_hexa
   integer(kind=IXP)                            :: numelb
   integer(kind=IXP)                            :: numlnk
   integer(kind=IXP)                            :: numatt
   integer(kind=IXP), dimension(:), allocatable :: block_ids

   integer(kind=IXP)                            :: ex_cpu_word_size
   integer(kind=IXP)                            :: ex_io_word_size
   integer(kind=IXP)                            :: ex_file_id
                                                
   integer(kind=IXP)                            :: is_in
   integer(kind=IXP)                            :: is_out
   integer(kind=IXP)                            :: ios
                                                
   character(len=MXLNLN)                        :: ex_title
   character(len=MXSTLN)                        :: typ
   character(len=255)                           :: file_dem
   character(len=255)                           :: prog_name
   character(len=255)                           :: file_mesh
   character(len=255)                           :: file_cur
   character(len= 90)                           :: czmin
   character(len= 90)                           :: myformat
   character(len= 15)                           :: cn
   character(len=  3)                           :: dem_file_ext
                                                
   logical                                      :: is_dem

   write(*,*) ""
   write(*,*) "*********************************************"
   write(*,*) "           CUBIT CURVE REFINING              "
   write(*,*) "*********************************************"

   narg = command_argument_count()

   if (narg /= 4) then

      call get_command_argument(0,prog_name)
      write(*,*) "Usage : "//trim(adjustl(prog_name))//" file.ex2 close_curve file.dem depth"

      stop

   endif

   call getarg(1,file_mesh)
   call getarg(2,file_cur)
   call getarg(3,file_dem)
   call getarg(4,czmin)

   read(czmin,'(F10.0)') depth

   write(*,*) ""
   write(*,*) "ex2   file = ",trim(file_mesh)
   write(*,*) "curve file = ",trim(file_cur)
   write(*,*) "dem   file = ",trim(file_dem)
   write(*,*) "depth      = ",depth


   write(*,*) ""
   write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
   write(*,*) "Warning : 'compress ids all' must be done in CUBIT before saving *.ex2 file"
   write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
   write(*,*) ""

!
!->initialize variable 

   call set_element_convention(xi_loc_gnode_hexa,et_loc_gnode_hexa,ze_loc_gnode_hexa,co_loc_gnode_hexa,abscissa)
   
   number_of_nodes         = 0
   nblock                  = 0
   number_of_hexa          = 0

!
!->read file that contains close curve

   call read_curve(file_cur,ndata_curve,xcurve,ycurve)

   if (file_dem /= "zplane") then

      is_dem = .true.

      dem_file_ext = file_dem(len_trim(file_dem)-2:len_trim(file_dem))

      if (dem_file_ext == "asc") then

         call read_topography(file_dem,x_dem,y_dem,z_dem,dx_dem,dy_dem,x0_dem,y0_dem,nx_dem,ny_dem)

      else

         call read_topography_grd(file_dem,x_dem,y_dem,z_dem,dx_dem,dy_dem,x0_dem,y0_dem,nx_dem,ny_dem)

      endif

   else

      is_dem = .false.

   endif

!
!->read exodusII info

   ex_file_id = exopen(trim(adjustl(file_mesh)),EXREAD,ex_cpu_word_size,ex_io_word_size,ex_version,ios)

   print *,"exodusII fil ID = ",ex_file_id

   if (ios /= 0) stop "error in cubit_refine: function exopen"

   call exgini(ex_file_id,ex_title,ndim,number_of_nodes,number_of_elements,number_of_blocks,number_of_node_sets,number_of_side_sets,ios)

   if (ios /= 0) stop "error in cubit_refine: subroutine exgini"

   write(*,*) ""
   write(*,*) "information about exodusII mesh"
   write(*,*) "number of geometric nodes = ",number_of_nodes
   write(*,*) "number of elements        = ",number_of_elements
   write(*,*) "number of blocks          = ",number_of_blocks
   write(*,*) "number of node sets       = ",number_of_node_sets
   write(*,*) "number of side sets       = ",number_of_side_sets

!
!->Arrays allocation

   ios = init_array_real(x_geom_node,number_of_nodes,"cubit_refine:x_geom_node")

   ios = init_array_real(y_geom_node,number_of_nodes,"cubit_refine:y_geom_node")

   ios = init_array_real(z_geom_node,number_of_nodes,"cubit_refine:z_geom_node")

   ios = init_array_int(block_ids,number_of_blocks,"cubit_refine:block_ids")


!
!->get coordinates from exodusII mesh file

   call exgcor(ex_file_id,x_geom_node,y_geom_node,z_geom_node,ios)
  
   if (ios /= 0) stop "error in cubit_refine: subroutine exgcor"

!
!->get block ids

   call exgebi(ex_file_id,block_ids,ios)

!
!->loop over blocks to find hexa located inside closed curve

   do iblock = 1_IXP,number_of_blocks

      call exgelb(ex_file_id,block_ids(iblock),typ,numelb,numlnk,numatt,ios)

      print *,block_ids(iblock),typ, numelb, numlnk

!---->test only hexahedron (i.e. not quadrangle)

      if (numlnk == N_GEOM_NODE_HEXA) then

         if (allocated(connectivity)) deallocate(connectivity)
       
         ios = init_array_int(connectivity,numelb*numlnk,"cubit_refine:connectivity")
       
         call exgelc(ex_file_id,block_ids(iblock),connectivity,ios)
       
         do ihexa = 1,numelb
         
            do inode = 1,numlnk
         
               mynode             = connectivity((ihexa-1_IXP)*N_GEOM_NODE_HEXA+inode)
         
               x_node_hexa(inode) = x_geom_node(mynode)
               y_node_hexa(inode) = y_geom_node(mynode)
               z_node_hexa(inode) = z_geom_node(mynode)
         
            enddo
         
            call get_hexa_centroid(                  &
                                   xi_loc_gnode_hexa &
                                  ,et_loc_gnode_hexa &
                                  ,ze_loc_gnode_hexa &
                                  ,abscissa          &
                                  ,co_loc_gnode_hexa &
                                  ,x_node_hexa       &
                                  ,y_node_hexa       &
                                  ,z_node_hexa       &
                                  ,x_centroid        &
                                  ,y_centroid        &
                                  ,z_centroid)
         
            if (is_dem) then
         
               call compute_z_dem(x_centroid,y_centroid,z_dem_interp,x_dem,y_dem,z_dem,nx_dem,ny_dem)
         
               zmin = z_dem_interp + depth
         
            else
         
               zmin = depth
         
            endif
         
            if (z_centroid >= zmin) then
           
               call in_poly_curve(x_centroid,y_centroid,xcurve,ycurve,ndata_curve,is_in,is_out)
           
               if(is_in >= 0) then
           
                  n_hexa_inside = n_hexa_inside + 1
           
                  if (n_hexa_inside > N_HEXA_INSIDE_MAX) then
       
                     stop "Code stopped: Too much hexa inside curve"
       
                  else
       
                     hexa_inside(n_hexa_inside) = ihexa
       
                  endif
           
               endif
           
            endif
         
         enddo

      endif

   enddo

   write(*,*) "Number of hexa found inside curve = ",n_hexa_inside

!
!->write hexa inside closed curve for cubit

   write(cn,'(i15)') NHEX_GROUP
   myformat = "(a,"//trim(adjustl(cn))//"(i0,1X))"

   open (unit=2,file="curve_refining.jou")
   do igroup = 1,int(n_hexa_inside/NHEX_GROUP)
      write(unit=2,fmt =trim(adjustl(myformat))) "group ""basin"" add hex ",(hexa_inside(ihexa),ihexa=(igroup-1)*NHEX_GROUP+1,igroup*NHEX_GROUP)
   enddo
   write(unit=2,fmt =trim(adjustl(myformat))) "group ""basin"" add hex ",(hexa_inside(ihexa),ihexa=int(n_hexa_inside/NHEX_GROUP)*NHEX_GROUP+1,n_hexa_inside)
   
   write(unit=2,fmt ='(a)') "refine hex in group basin numsplit 1 depth 0"
   close(2)

   stop

end program cubit_curve_refining
