!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
module Make_Layers_surface_module

contains

subroutine make_basin_layers(xa, ya, zf, z1, z2)

   implicit none

   real(kind=8) :: l, h1, h2, Rx, Ry, Rx2, Ry2, dh(2), n
   real(kind=8) :: Rmx, Rmy, xa, ya
   real(kind=8) :: z1, z2, yo, zf
   real(kind=8) :: za
   real(kind=8) :: hi, hi1, hi2, hr, m, zn, zdiff
   integer      :: id
       
   id = 1
! case 1: Nagoya (2 layers)
   if (id.eq.1) then

      l = 8.0
      h1 = 1.6
      h2 = 2.0
      Rx= 15.0
      Ry= 16.0
      Rx2= 15.0
      Ry2=16.0
      n = 3.0
      dh(1)=0.5
      dh(2)=0.5

! case 2: Grenoble (2 layers)
   else if (id.eq.2) then

      l = 4.0
      h1 = 0.85
      h2 = 0.85
      Rx= 3.0
      Ry= 3.0
      Rx2= 3.0
      Ry2= 3.0
      n = 2.0
      dh(1)=0.5
      dh(2)=0.5

! case 3: LA (3 layers)
   else if (id.eq.3) then
      l = 8.0
      h1 = 7.0
      h2 = 9.0
      Rx=22.5
      Ry=11.0
      Rx2=22.5
      Ry2=11.0
      n = 4.0
      dh(1)=0.2
      dh(2)=0.3

   else

      write(*,*) "no corresponding structure", id
      stop

   endif
!
   hr = h1
   if (h2.gt.hr) hr = h2
   hi = dh(1)*hr

   Rmx = Rx
   if (Rx2.gt.Rx) Rmx = Rx2
   Rmy = Ry
   if (Ry2.gt.Ry) Rmy = Ry2

!   p = 0.9
!   pis = acos(1.-2*p)

   yo = 0.0
!
! basin shape
!
            if (ya.gt.(yo + l/2.0)) then
              zf = func1(xa, ya-(yo+l/2.0), Rx, Ry, h1)
            else if (ya.lt.(yo - l/2.0)) then
              zf = func2(xa, ya-(yo-l/2.0), Rx2, Ry2, h2, n)
            else
              z1 = func1(xa, yo, Rx, Ry, h1)
              z2 = func2(xa, yo, Rx2, Ry2, h2, n)
              zf = z1*(ya-(yo-l/2.0))/l + z2*(yo+l/2.0-ya)/l
            endif
! layers
! top layer
            if (zf.lt.(-hi)) then
              z1 = -hi
            else
              z1 = zf
            endif
! second layer
            if (dh(1)+dh(2).ge.1) then
              z2 = zf
            else
              hi1 = -(dh(1)+dh(2))*h1
              hi2 = -(dh(1)+dh(2))*h2
              m = (hi1-hi2)/l
              zn = m*(ya-l/2) + hi1
              zdiff = zf - zn
              if (zdiff.lt.1e-16) then
                za = 0.0
              else
                za = zdiff
              endif
              z2 = za + zn
            endif

   return

end
!
real(kind=8) function func1(xa, ya, Rx, Ry, h1)

   implicit none

   real(kind=8) xa, ya, Rx, Ry
   real(kind=8) p, pis, h1, xi, yi

   p = 0.9
   pis = acos(1.-2*p)
!  h1 = 7.0

   xi=xa/Rx
   if (abs(xi).gt.1.0) xi = 1.0

   yi=ya/Ry
   if (abs(yi).gt.1.0) yi = 1.0

   func1 = -(cos(pis*xi)+2.*p-1.)*(cos(pis*yi)+2.*p-1.0)*h1/(4.*p*p)

   return
end

real(kind=8) function func2(xa, ya, Rx2, Ry2, h2, n)

   implicit none

   real(kind=8) xa, ya, xi, yi, Rx2, Ry2, h2, n

!   n = 4.0
!   h2 = 9.0

   xi=abs(xa/Rx2)
   yi=abs(ya/Ry2)

   if ((1.-xi**n-yi**n).gt.0.0) then

      func2 = -h2*sqrt(1.-xi**n-yi**n)
   else

     func2 = 0.0

   endif

   return

end

subroutine write_esri_ascii(f,nc,nr,xll,yll,ds,z)

   implicit none

   character(len=*)                , intent(in) :: f
   integer                         , intent(in) :: nc
   integer                         , intent(in) :: nr
   real(kind=8)                    , intent(in) :: xll
   real(kind=8)                    , intent(in) :: yll
   real(kind=8)                    , intent(in) :: ds
   real(kind=8)    , dimension(:,:), intent(in) :: z

   integer                                      :: u
   integer                                      :: ir
   integer                                      :: ic

   open(newunit=u,file=trim(f),form="formatted",action="write")

   write(u,'(a,1x,I6)'   ) "ncols",nc
   write(u,'(a,1x,I6)'   ) "nrows",nr
   write(u,'(a,1x,E15.7)') "xllcenter",xll
   write(u,'(a,1x,E15.7)') "yllcenter",yll
   write(u,'(a,1x,E15.7)') "cellsize",ds
   write(u,'(a,1x,I6)'   ) "nodata_value",-9999

   do ir = nr,1,-1

      write(u,'(2000(F10.3,1x))') (z(ic,ir),ic=1,nc)

   enddo

   close(u)

   return

end subroutine

end module
