!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
program receivers_fft_ratio

   use mod_precision

   use efispec3d_tools

   implicit none

   real                               :: EPSILON_MACHINE = EPSILON(EPSILON_MACHINE)

   integer                            :: ndt
   integer                            :: ndt1
   integer                            :: ndt2
   integer                            :: nt
   integer                            :: m
   integer                            :: i
   integer                            :: nfold
   integer                            :: narg
   integer                            :: fdec

   real                               :: dt
   real                               :: dt1
   real                               :: dt2
   real                               :: df
   real                               :: td
   real                               :: freq
   real                               :: ts
   real                               :: te
   real                               :: cs

   real   , allocatable, dimension(:) :: time1
   real   , allocatable, dimension(:) :: time2
   real   , allocatable, dimension(:) :: ux1 
   real   , allocatable, dimension(:) :: uy1
   real   , allocatable, dimension(:) :: uz1
   real   , allocatable, dimension(:) :: vx1
   real   , allocatable, dimension(:) :: vy1
   real   , allocatable, dimension(:) :: vz1
   real   , allocatable, dimension(:) :: ax1
   real   , allocatable, dimension(:) :: ay1
   real   , allocatable, dimension(:) :: az1
   real   , allocatable, dimension(:) :: ux2 
   real   , allocatable, dimension(:) :: uy2
   real   , allocatable, dimension(:) :: uz2
   real   , allocatable, dimension(:) :: vx2
   real   , allocatable, dimension(:) :: vy2
   real   , allocatable, dimension(:) :: vz2
   real   , allocatable, dimension(:) :: ax2
   real   , allocatable, dimension(:) :: ay2
   real   , allocatable, dimension(:) :: az2

   complex, allocatable, dimension(:) :: cux1 
   complex, allocatable, dimension(:) :: cuy1
   complex, allocatable, dimension(:) :: cuz1
   complex, allocatable, dimension(:) :: cvx1
   complex, allocatable, dimension(:) :: cvy1
   complex, allocatable, dimension(:) :: cvz1
   complex, allocatable, dimension(:) :: cax1
   complex, allocatable, dimension(:) :: cay1
   complex, allocatable, dimension(:) :: caz1
   complex, allocatable, dimension(:) :: cux2 
   complex, allocatable, dimension(:) :: cuy2
   complex, allocatable, dimension(:) :: cuz2
   complex, allocatable, dimension(:) :: cvx2
   complex, allocatable, dimension(:) :: cvy2
   complex, allocatable, dimension(:) :: cvz2
   complex, allocatable, dimension(:) :: cax2
   complex, allocatable, dimension(:) :: cay2
   complex, allocatable, dimension(:) :: caz2

   character(len=255)                 :: prog_name
   character(len=255)                 :: file_rec1
   character(len=255)                 :: file_rec2
   character(len=255)                 :: file_fft

   logical                            :: ls
   logical                            :: le


   narg = command_argument_count()

   if ( narg /= 2 ) then

      call get_command_argument(0,prog_name)

      write(*,'(//,a,//)') "Usage : "//trim(adjustl(prog_name))//" receiver1_file receiver2_file"

      stop

   endif

   call get_command_argument(1,file_rec1)

   call get_command_argument(2,file_rec2)

!
!->read receiver file and source file
   call read_receiver_binary(file_rec1,ndt1,dt1,time1,ux1,uy1,uz1,vx1,vy1,vz1,ax1,ay1,az1)

   call read_receiver_binary(file_rec2,ndt2,dt2,time2,ux2,uy2,uz2,vx2,vy2,vz2,ax2,ay2,az2)

   if (ndt1/= ndt2) stop "error: different ndt"

   if (abs(dt1-dt2) > EPSILON_MACHINE) stop "error: different dt"

   ndt = ndt1
   dt  = dt1

!!
!!->decimate time series
!   fdec = 10
!
!   call decimate(time1,ndt,fdec)
!   call decimate(ux1  ,ndt,fdec)
!   call decimate(uy1  ,ndt,fdec)
!   call decimate(uz1  ,ndt,fdec)
!   call decimate(ux2  ,ndt,fdec)
!   call decimate(uy2  ,ndt,fdec)
!   call decimate(uz2  ,ndt,fdec)
!
!   ndt = int((ndt-ONE_IXP)/fdec,kind=IXP) + ONE_IXP
!    dt = dt*real(fdec,kind=RXP)
!
!   fdec = 15
!
!   call decimate(time1,ndt,fdec)
!   call decimate(ux1  ,ndt,fdec)
!   call decimate(uy1  ,ndt,fdec)
!   call decimate(uz1  ,ndt,fdec)
!   call decimate(ux2  ,ndt,fdec)
!   call decimate(uy2  ,ndt,fdec)
!   call decimate(uz2  ,ndt,fdec)
!
!   ndt = int((ndt-ONE_IXP)/fdec,kind=IXP) + ONE_IXP
!    dt = dt*real(fdec,kind=RXP)


!
!->apply taper window between start time (ts) and end time (te)
   cs =  5.0
   ts =  0.0
   te = real(ndt-1)*dt
   ls = .true.
   le = .true.
 
   if (ls .or. le) then

      write(*,*) "Applying cosine shape window"
      write(*,*) "Cosine length = ",cs
      write(*,*) "Start time = ",ts,ls
      write(*,*) "End   time = ",te,le
      
      call taper_time(ux1,ndt,dt,cs,ts,te,ls,le)
      call taper_time(uy1,ndt,dt,cs,ts,te,ls,le)
      call taper_time(uz1,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(vx1,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(vy1,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(vz1,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(ax1,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(ay1,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(az1,ndt,dt,cs,ts,te,ls,le)

      call taper_time(ux2,ndt,dt,cs,ts,te,ls,le)
      call taper_time(uy2,ndt,dt,cs,ts,te,ls,le)
      call taper_time(uz2,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(vx2,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(vy2,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(vz2,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(ax2,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(ay2,ndt,dt,cs,ts,te,ls,le)
!     call taper_time(az2,ndt,dt,cs,ts,te,ls,le)

   else

      write(*,*) "No cosine shape"

     !do i = 1,ndt
     !   write(unit=100,fmt='(4(E17.7,1X))') time1(i),ux(i),uy(i),uz(i)
     !enddo

   endif

!
!->compute FFT variables
   call cnfast(ndt,nt,m,nfold,dt,df,td)

   allocate(cux1(nt),cuy1(nt),cuz1(nt))
!  allocate(cvx1(nt),cvy1(nt),cvz1(nt))
!  allocate(cax1(nt),cay1(nt),caz1(nt))
   allocate(cux2(nt),cuy2(nt),cuz2(nt))
!  allocate(cvx2(nt),cvy2(nt),cvz2(nt))
!  allocate(cax2(nt),cay2(nt),caz2(nt))

   cux1(:) = cmplx(0.0,0.0)
   cuy1(:) = cmplx(0.0,0.0)
   cuz1(:) = cmplx(0.0,0.0)
!  cvx1(:) = cmplx(0.0,0.0)
!  cvy1(:) = cmplx(0.0,0.0)
!  cvz1(:) = cmplx(0.0,0.0)
!  cax1(:) = cmplx(0.0,0.0)
!  cay1(:) = cmplx(0.0,0.0)
!  caz1(:) = cmplx(0.0,0.0)

   cux2(:) = cmplx(0.0,0.0)
   cuy2(:) = cmplx(0.0,0.0)
   cuz2(:) = cmplx(0.0,0.0)
!  cvx2(:) = cmplx(0.0,0.0)
!  cvy2(:) = cmplx(0.0,0.0)
!  cvz2(:) = cmplx(0.0,0.0)
!  cax2(:) = cmplx(0.0,0.0)
!  cay2(:) = cmplx(0.0,0.0)
!  caz2(:) = cmplx(0.0,0.0)

   cux1(1:ndt) = ux1(1:ndt)
   cuy1(1:ndt) = uy1(1:ndt)
   cuz1(1:ndt) = uz1(1:ndt)
!  cvx1(1:ndt) = vx1(1:ndt)
!  cvy1(1:ndt) = vy1(1:ndt)
!  cvz1(1:ndt) = vz1(1:ndt)
!  cax1(1:ndt) = ax1(1:ndt)
!  cay1(1:ndt) = ay1(1:ndt)
!  caz1(1:ndt) = az1(1:ndt)

   cux2(1:ndt) = ux2(1:ndt)
   cuy2(1:ndt) = uy2(1:ndt)
   cuz2(1:ndt) = uz2(1:ndt)
!  cvx2(1:ndt) = vx2(1:ndt)
!  cvy2(1:ndt) = vy2(1:ndt)
!  cvz2(1:ndt) = vz2(1:ndt)
!  cax2(1:ndt) = ax2(1:ndt)
!  cay2(1:ndt) = ay2(1:ndt)
!  caz2(1:ndt) = az2(1:ndt)

   call fft(cux1,nt,-1)
   call fft(cuy1,nt,-1)
   call fft(cuz1,nt,-1)
!  call fft(cvx1,nt,-1)
!  call fft(cvy1,nt,-1)
!  call fft(cvz1,nt,-1)
!  call fft(cax1,nt,-1)
!  call fft(cay1,nt,-1)
!  call fft(caz1,nt,-1)

   call fft(cux2,nt,-1)
   call fft(cuy2,nt,-1)
   call fft(cuz2,nt,-1)
!  call fft(cvx2,nt,-1)
!  call fft(cvy2,nt,-1)
!  call fft(cvz2,nt,-1)
!  call fft(cax2,nt,-1)
!  call fft(cay2,nt,-1)
!  call fft(caz2,nt,-1)

   cux1(:) = cux1(:)*dt
   cuy1(:) = cuy1(:)*dt
   cuz1(:) = cuz1(:)*dt
!  cvx1(:) = cvx1(:)*dt
!  cvy1(:) = cvy1(:)*dt
!  cvz1(:) = cvz1(:)*dt
!  cax1(:) = cax1(:)*dt
!  cay1(:) = cay1(:)*dt
!  caz1(:) = caz1(:)*dt

   cux2(:) = cux2(:)*dt
   cuy2(:) = cuy2(:)*dt
   cuz2(:) = cuz2(:)*dt
!  cvx2(:) = cvx2(:)*dt
!  cvy2(:) = cvy2(:)*dt
!  cvz2(:) = cvz2(:)*dt
!  cax2(:) = cax2(:)*dt
!  cay2(:) = cay2(:)*dt
!  caz2(:) = caz2(:)*dt


   file_fft = trim(file_rec1)//"."//trim(file_rec2)//".fft.ratio"
   
   open(unit=10,file=trim(file_fft),form='unformatted',access='stream')
   
   do i = 1,nfold
   
      freq = real(i-1)*df
   
      write(unit=10) freq,abs(cux1(i)/cux2(i)),abs(cuy1(i)/cuy2(i)),abs(cuz1(i)/cuz2(i))
   
   enddo
   
   close(10)
      
   stop

end program
