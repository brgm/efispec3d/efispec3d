!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
module efispec3d_tools

   implicit none

   private

   public  :: read_receiver_binary
   public  :: read_stf
   public  :: fft
   public  :: cnfast
   public  :: taper_time
   public  :: spewin
   public  :: decimate

   contains

!
!
!*******************************************************************************
!  read efispec binary file
!*******************************************************************************
   subroutine read_receiver_binary(f,ndt,dt,time,ux,uy,uz,vx,vy,vz,ax,ay,az)

      implicit none

      character(len=90), intent( in)                            :: f
      integer          , intent(out)                            :: ndt
      real             , intent(out)                            :: dt
      real             , intent(out), allocatable, dimension(:) :: time
      real             , intent(out), allocatable, dimension(:) :: ux
      real             , intent(out), allocatable, dimension(:) :: uy
      real             , intent(out), allocatable, dimension(:) :: uz
      real             , intent(out), allocatable, dimension(:) :: vx
      real             , intent(out), allocatable, dimension(:) :: vy
      real             , intent(out), allocatable, dimension(:) :: vz
      real             , intent(out), allocatable, dimension(:) :: ax
      real             , intent(out), allocatable, dimension(:) :: ay
      real             , intent(out), allocatable, dimension(:) :: az

      real                                                      :: tmp

      integer                                                   :: idt
      integer                                                   :: ios

      ndt = 0

      open(unit=1,file=trim(f),form='unformatted',access='stream')

      do

         read(unit=1,iostat=ios) tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp

         if (ios /= 0) exit

         ndt = ndt + 1

      enddo

      write(*,'(A,I0)') "number of time step found in file "//trim(f)//" = ",ndt

      close(1)

      allocate(time(ndt))
      allocate(ux(ndt),uy(ndt),uz(ndt))
      allocate(vx(ndt),vy(ndt),vz(ndt))
      allocate(ax(ndt),ay(ndt),az(ndt))

      open(unit=1,file=trim(f),form='unformatted',access='stream')
 
      read(unit=1) (time(idt),ux(idt),uy(idt),uz(idt),vx(idt),vy(idt),vz(idt),ax(idt),ay(idt),az(idt),idt=1,ndt)

      close(1)

      dt = time(2) - time(1)

      write(*,'(A,E17.7)') "time step = ",dt

      return

!*******************************************************************************
   end subroutine read_receiver_binary
!*******************************************************************************

!
!
!*******************************************************************************
!  read efispec source time function
!*******************************************************************************
   subroutine read_stf(f,ndt,dt,time,stf)
   
      implicit none

      character(len=90), intent( in)                            :: f
      integer          , intent(out)                            :: ndt
      real             , intent(out)                            :: dt
      real             , intent(out), allocatable, dimension(:) :: time
      real             , intent(out), allocatable, dimension(:) :: stf

      real                                                      :: tmp
      integer                                                   :: ios
      integer                                                   :: idt

      ndt = 0

      open(unit=1,file=trim(f))

      do

         read(unit=1,fmt=*,iostat=ios) tmp,tmp

         if (ios /= 0) exit

         ndt = ndt + 1

      enddo

      rewind(1)

      write(*,'(/,A,I0)') "number of time step found in file "//trim(f)//" = ",ndt

      allocate(time(ndt))
      allocate(stf(ndt))

      do idt = 1,ndt

         read(unit=1,fmt=*) time(idt),stf(idt)
         
      enddo

      close(1)

      dt = time(2) - time(1)

      write(*,'(/,A,E17.7)') "time step = ",dt

      return

!*******************************************************************************
   end subroutine read_stf
!*******************************************************************************


!
!
!*******************************************************************************
!  fast fourier transform from ohsaki's book
!*******************************************************************************
   subroutine fft(x,n,ind)
!-----------------------------------------------------------------------
!      input
!-----------------------------------------------------------------------
!      x    :data before fast fourier transform
!      n    :size of x
!      ind  :index for fourier transform or fourier inverse transform
!            -1 --> fourier transform
!             1 --> fourier inverse transeform
!-----------------------------------------------------------------------
!      output
!-----------------------------------------------------------------------
!      x   :data after fast fourier transform
!-----------------------------------------------------------------------
 
   implicit none
 
   integer, intent(in)                  :: ind
   integer, intent(in)                  :: n
   complex, intent(inout), dimension(n) :: x
   complex                              :: temp,theta
 
   real                                 :: pi
 
   integer                              :: i,j,k,m,kmax,istep
 
   pi = acos(-1.0)
 
   j  = 1

   do 40 i = 1,n
      if(i.ge.j) go to 10
      temp = x(j)
      x(j) = x(i)
      x(i) = temp
10    m    = n/2
20    if(j.le.m) go to 30
      j    = j-m
      m    = m/2
      if(m.ge.2) go to 20
30    j    = j+m
40 continue
!
   kmax = 1
50 if(kmax.ge.n) return
   istep = kmax*2
   do 70 k = 1,kmax
      theta = cmplx(0.0,pi*(ind*(k-1))/(kmax))
      do 60 i = k,n,istep
         j    = i+kmax
         temp = x(j)*cexp(theta)
         x(j) = x(i)-temp
         x(i) = x(i)+temp
60    continue
70 continue
   kmax = istep
   go to 50
!
!*******************************************************************************
   end subroutine fft
!*******************************************************************************

!
!
!*******************************************************************************
!  compute multiple of power of 2 for fast fourier transform
!*******************************************************************************
   subroutine cnfast(n,nt,m,nfold,dt,df,td)
!-----------------------------------------------------------------------
!      input
!-----------------------------------------------------------------------
!      n     : number of data of waveform
!      dt    : time sampling of waveform
!-----------------------------------------------------------------------
!      output
!-----------------------------------------------------------------------
!      nt    : closest power of 2 of n. however nt = 4096 if nt < 4096
!              to ensure a good frequency resolution
!      m     : exponent of power of 2, nt = 2**m
!      nfold : nfold = nt/2 + 1 -> data up to nyguist frequency
!      df    : frequency sampling of fft
!      td    : total duration of signal up to nt data
!-----------------------------------------------------------------------

   implicit none

   real            , intent(inout) :: df,td
   real            , intent(in)    :: dt

   integer         , intent(inout) :: nt,m,nfold
   integer         , intent(in)    :: n

   nt = 2
   m  = 1
   do while (nt.lt.n)
     nt = nt*2
     m  = m + 1
   enddo
!
!->length for fft should be at least...
   if ( nt <= 131072 ) then 
      nt = 131072
      m  = 17
   endif

   nfold = nt/2 + 1
   td    = nt*dt
   df    = 1.0/td

   return

!*******************************************************************************
   end subroutine cnfast
!*******************************************************************************

!
!*******************************************************************************
   subroutine taper_time(x,n,dt,c,ts,te,ls,le)
!-----------------------------------------------------------------------
!      input
!-----------------------------------------------------------------------
!      x : wave to be cosine shaped
!      n : size of x
!      dt: time step of x
!      c : percentage of cosine shape
!      ts: start time (s) of the cosine window
!      te: end   time (s) of the cosine window
!      ls: if .true. apply cosine at the start
!      le: if .true. apply cosine at the end
!-----------------------------------------------------------------------
!      output
!-----------------------------------------------------------------------
!     x  : tapered wave
!-----------------------------------------------------------------------
!
   implicit none

   integer         , intent(in)                  :: n
   real            , intent(inout), dimension(n) :: x
   real            , intent(in)                  :: c
   real            , intent(in)                  :: dt
   real            , intent(in)                  :: ts
   real            , intent(in)                  :: te
   logical         , intent(in)                  :: ls
   logical         , intent(in)                  :: le
   real                                          :: pi
   real                                          :: v

   integer                                       :: i
   integer                                       :: is
   integer                                       :: ie
   integer                                       :: nc

   pi = acos(-1.0)

   if (ts >= te) then

      write(*,*) "error in subroutine taper_time : ts >= te",ts,te
      stop

   endif

   if (ts < 0.0) then

      write(*,*) "error in subroutine taper_time : ts < 0.0",ts
      stop

   endif

   if (te > real(n-1)*dt) then

      write(*,*) "error in subroutine taper_time : te > total time",ts,real(n-1)*dt
      stop

   endif


!
!->initialization of start time, end time, etc.
   is = int(ts/dt)
   ie = int(te/dt)
   nc = int( real(ie-is) * (c/100.0) )


!
!->processing around ts if ls is true
   if (ls) then
!
!---->flush to zero before ts
      do i = 1,is-1
         x(i) = 0.0 
      enddo

!
!---->apply hanning window at the beginning/end of the array x
      do i = 0,nc
     
         v = 0.5 - 0.5 * cos( (pi /(real(nc))) * real(i) )
     
         x( i+is) = x( i+is)*v
     
      enddo

   endif

   if (le) then

!
!---->flush to zero after te
      do i = ie+1,n
         x(i) = 0.0 
      enddo
    
!
!---->apply hanning window at the beginning/end of the array x
      do i = 0,nc
    
         v = 0.5 - 0.5 * cos( (pi /(real(nc))) * real(i) )
    
         x(ie-i ) = x(ie-i )*v
    
      enddo

   endif

   return
!*******************************************************************************
   end subroutine taper_time
!*******************************************************************************

!
!
!*******************************************************************************
!smoothed spectra by parzen's spectral window
!*******************************************************************************
   subroutine spewin(x,n,df,b)

   implicit none

   integer         , intent(in)                  :: n
   real            , intent(inout), dimension(n) :: x
   real            , intent(inout)               :: b
   real            , intent(in)                  :: df

   real            , allocatable  , dimension(:) :: w,x1,x2 
   real                                          :: bmin,bmax1,bmax2,bmax,t,udf,dif,s

   integer                                       :: k,l,lmax,ll,ln,lt,le
   integer                                       :: nd

   nd = 2*(n-1)

   allocate(x1(nd+1),x2(nd+1))

   bmin  =   560.0/151.0*df
   bmax1 =   140.0*real(n-1)/151.0*df
   bmax2 = 14000.0/150.0*df
   bmax  = min(bmax1,bmax2)

   if( (b.lt.bmin) .or. (b.gt.bmax) ) then
      if(nd >= 65536) then
         b = bmax*1.0
      elseif(nd.eq.32768) then
         b = bmax*0.5
      elseif(nd.eq.16384) then
         b = bmax*0.25
      elseif(nd.eq.8192) then
         b = bmax*0.125
      else
         b = bmax*0.125
      endif
   endif

   write(*,*) "spewin : b = ",b

   t   = 1.0/df
   udf = 1.854305/b*df

   if (udf.gt.0.5) then
      write(0,*)                 "***error in spewin. bandwidth is too narrow"
      write(0,'(e15.7,a,e15.7)') bmin," < band width (hz) < ",bmax
   endif

   lmax = int(2.0/udf)+1
   if (lmax.gt.101) then
      write(0,*)                 "***error in spewin. bandwidth is too wide"
      write(0,'(e15.7,a,e15.7)') bmin," < band width (hz) < ",bmax
   endif
!
!  spectral window
   allocate(w(lmax))
   w(1) = 0.75*udf
   do l = 2,lmax
      dif  = 1.570796*real(l-1)*udf
      w(l) = w(1)*(sin(dif)/dif)**4
   enddo
!
!  smoothing of fourier spectrum
   ll = lmax*2-1
   ln =  ll-1    + n
   lt = (ll-1)*2 + n
   le = lt-lmax+1

   do k = 1,lt
      x1(k) = 0.0
   enddo
   do k = 1,n
      x1(ll-1+k) = x(k)
   enddo

   do k = lmax,le
      s = w(1)*x1(k)
      do l = 2,lmax
         s = s + w(l)*(x1(k-l+1) + x1(k+l-1))
      enddo
      x2(k) = s
   enddo

   do l = 2,lmax
      x2(ll+l-1) = x2(ll+l-1) + x2(ll-l+1)
      x2(ln-l+1) = x2(ln-l+1) + x2(ln+l-1)
   enddo

   do k = 1,n
      x(k) = x2(ll-1+k)
   enddo

   deallocate(x1,x2,w)

   return
!*******************************************************************************
   end subroutine spewin
!*******************************************************************************

!
!
!>@brief subroutine to decimate time series
!>@param  x : input time series
!>@param  n : length of x
!>@param  f : factor of decimation
!>@return x : output time series
!***********************************************************************************************************************************************************************************
   subroutine decimate(x,n,f)
!***********************************************************************************************************************************************************************************

      use mod_precision

      implicit none

      integer(kind=IXP), intent(in)                               :: n
      integer(kind=IXP), intent(in)                               :: f
      real   (kind=RXP), intent(inout), allocatable, dimension(:) :: x

      real   (kind=RXP)               , allocatable ,dimension(:) :: y
      integer(kind=IXP)                                           :: nd
      integer(kind=IXP)                                           :: i
      integer(kind=IXP)                                           :: id

      nd  = int((n-ONE_IXP)/f,kind=IXP) + ONE_IXP

      allocate(y(nd))

      id  = ZERO_IXP

      do i = ONE_IXP,n,f

         id    = id + ONE_IXP
         y(id) = x(i) 

      enddo

      deallocate(x)

      allocate(x(nd))

      x(:) = y(:)

      return

!***********************************************************************************************************************************************************************************
   end subroutine decimate
!***********************************************************************************************************************************************************************************


end module efispec3d_tools
