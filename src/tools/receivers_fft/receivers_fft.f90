!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
program receivers_fft

   use, intrinsic :: iso_fortran_env, only : stdout => output_unit

   use mod_precision

   use mod_signal_processing

   use mod_receivers_fft

   implicit none

   integer(kind=IXP)                            :: ndt
   integer(kind=IXP)                            :: nt
   integer(kind=IXP)                            :: nt_wish = 0_IXP
   integer(kind=IXP)                            :: i
   integer(kind=IXP)                            :: nfold
   integer(kind=IXP)                            :: narg
   integer(kind=IXP)                            :: fdec

   real   (kind=RXP)                            :: dt
   real   (kind=RXP)                            :: dt_s
   real   (kind=RXP)                            :: df
   real   (kind=RXP)                            :: td
   real   (kind=RXP)                            :: freq
   real   (kind=RXP)                            :: ts
   real   (kind=RXP)                            :: te
   real   (kind=RXP)                            :: cs
   real   (kind=RXP)                            :: xn!real number of point used for fast fourier transform
   real   (kind=RXP)                            :: bw!bandwidth

   real   (kind=RXP), allocatable, dimension(:) :: time
   real   (kind=RXP), allocatable, dimension(:) :: ux
   real   (kind=RXP), allocatable, dimension(:) :: uy
   real   (kind=RXP), allocatable, dimension(:) :: uz
   real   (kind=RXP), allocatable, dimension(:) :: vx
   real   (kind=RXP), allocatable, dimension(:) :: vy
   real   (kind=RXP), allocatable, dimension(:) :: vz
   real   (kind=RXP), allocatable, dimension(:) :: ax
   real   (kind=RXP), allocatable, dimension(:) :: ay
   real   (kind=RXP), allocatable, dimension(:) :: az
   real   (kind=RXP), allocatable, dimension(:) :: ux_phase
   real   (kind=RXP), allocatable, dimension(:) :: uy_phase
   real   (kind=RXP), allocatable, dimension(:) :: uz_phase
   real   (kind=RXP), allocatable, dimension(:) :: vx_phase
   real   (kind=RXP), allocatable, dimension(:) :: vy_phase
   real   (kind=RXP), allocatable, dimension(:) :: vz_phase
   real   (kind=RXP), allocatable, dimension(:) :: ax_phase
   real   (kind=RXP), allocatable, dimension(:) :: ay_phase
   real   (kind=RXP), allocatable, dimension(:) :: az_phase

   real   (kind=RXP), allocatable, dimension(:) :: cux_abs
   real   (kind=RXP), allocatable, dimension(:) :: cuy_abs
   real   (kind=RXP), allocatable, dimension(:) :: cuz_abs
   real   (kind=RXP), allocatable, dimension(:) :: cvx_abs
   real   (kind=RXP), allocatable, dimension(:) :: cvy_abs
   real   (kind=RXP), allocatable, dimension(:) :: cvz_abs
   real   (kind=RXP), allocatable, dimension(:) :: cax_abs
   real   (kind=RXP), allocatable, dimension(:) :: cay_abs
   real   (kind=RXP), allocatable, dimension(:) :: caz_abs

   complex(kind=RXP), allocatable, dimension(:) :: cux
   complex(kind=RXP), allocatable, dimension(:) :: cuy
   complex(kind=RXP), allocatable, dimension(:) :: cuz
   complex(kind=RXP), allocatable, dimension(:) :: cvx
   complex(kind=RXP), allocatable, dimension(:) :: cvy
   complex(kind=RXP), allocatable, dimension(:) :: cvz
   complex(kind=RXP), allocatable, dimension(:) :: cax
   complex(kind=RXP), allocatable, dimension(:) :: cay
   complex(kind=RXP), allocatable, dimension(:) :: caz

   character(len=255)                           :: command_line
   character(len=255)                           :: prog_name
   character(len=255)                           :: file_rec
   character(len=255)                           :: file_fft
   character(len=255)                           :: cnt

   logical(kind=IXP)                            :: is_unwrap_phase = .false.
   logical(kind=IXP)                            :: is_n            = .false.
   logical(kind=IXP)                            :: is_s            = .false.

   narg = command_argument_count()

   if ( narg < 1_IXP ) then

      call get_command_argument(0_IXP,prog_name)

      write(stdout,'(//,a,//)') "Usage : "//trim(adjustl(prog_name))//" receiver_file [-n nfft] [-s bandwidth(Hz)]"

      stop

   endif

   call get_command(command_line)

   call get_command_argument(1_IXP,file_rec)

   call get_option(command_line,'-n',is_n,xn)

   if (is_n) nt_wish = int(xn,kind=IXP) 

   call get_option(command_line,'-s',is_s,bw)

!
!->read receiver file

   call read_receiver_binary(file_rec,ndt,dt,time,ux,uy,uz,vx,vy,vz,ax,ay,az)

!
!->compute FFT variables

   nt = nextpow2(ndt,nt_wish)

   df = 1.0_RXP/(real(nt,kind=RXP)*dt)

   nfold = nt/2_IXP + 1_IXP

   write(stdout,*) "ndt           = ",ndt
   write(stdout,*) "dt            = ",dt
   write(stdout,*) "nfft          = ",nt
   write(stdout,*) "df            = ",df
   write(stdout,*) "smoothing FFT = ",is_s
   if (is_s) write(stdout,*) "smoothing bandwidth = ",bw

   allocate(cux(nt),cuy(nt),cuz(nt))
   allocate(cvx(nt),cvy(nt),cvz(nt))
   allocate(cax(nt),cay(nt),caz(nt))

   allocate(cux_abs(nfold))
   allocate(cuy_abs(nfold))
   allocate(cuz_abs(nfold))

   allocate(cvx_abs(nfold))
   allocate(cvy_abs(nfold))
   allocate(cvz_abs(nfold))

   allocate(cax_abs(nfold))
   allocate(cay_abs(nfold))
   allocate(caz_abs(nfold))

   cux(:) = cmplx(0.0_RXP,0.0_RXP)
   cuy(:) = cmplx(0.0_RXP,0.0_RXP)
   cuz(:) = cmplx(0.0_RXP,0.0_RXP)
   cvx(:) = cmplx(0.0_RXP,0.0_RXP)
   cvy(:) = cmplx(0.0_RXP,0.0_RXP)
   cvz(:) = cmplx(0.0_RXP,0.0_RXP)
   cax(:) = cmplx(0.0_RXP,0.0_RXP)
   cay(:) = cmplx(0.0_RXP,0.0_RXP)
   caz(:) = cmplx(0.0_RXP,0.0_RXP)

   cux(1_IXP:ndt) = ux(1_IXP:ndt)
   cuy(1_IXP:ndt) = uy(1_IXP:ndt)
   cuz(1_IXP:ndt) = uz(1_IXP:ndt)
   cvx(1_IXP:ndt) = vx(1_IXP:ndt)
   cvy(1_IXP:ndt) = vy(1_IXP:ndt)
   cvz(1_IXP:ndt) = vz(1_IXP:ndt)
   cax(1_IXP:ndt) = ax(1_IXP:ndt)
   cay(1_IXP:ndt) = ay(1_IXP:ndt)
   caz(1_IXP:ndt) = az(1_IXP:ndt)

   call ft(cux,-1_IXP)
   call ft(cuy,-1_IXP)
   call ft(cuz,-1_IXP)
   call ft(cvx,-1_IXP)
   call ft(cvy,-1_IXP)
   call ft(cvz,-1_IXP)
   call ft(cax,-1_IXP)
   call ft(cay,-1_IXP)
   call ft(caz,-1_IXP)

   cux(:) = cux(:)*dt
   cuy(:) = cuy(:)*dt
   cuz(:) = cuz(:)*dt
   cvx(:) = cvx(:)*dt
   cvy(:) = cvy(:)*dt
   cvz(:) = cvz(:)*dt
   cax(:) = cax(:)*dt
   cay(:) = cay(:)*dt
   caz(:) = caz(:)*dt

   if (is_unwrap_phase) then

      allocate(ux_phase(nfold))
      allocate(uy_phase(nfold))
      allocate(uz_phase(nfold))

      ux_phase(1_IXP:nfold) = atan2(aimag(cux(1_IXP:nfold)),real(cux(1_IXP:nfold)))
      uy_phase(1_IXP:nfold) = atan2(aimag(cuy(1_IXP:nfold)),real(cuy(1_IXP:nfold)))
      uz_phase(1_IXP:nfold) = atan2(aimag(cuz(1_IXP:nfold)),real(cuz(1_IXP:nfold)))

      call unwrap_1d(uy_phase)

   endif

   cux_abs(1_IXP:nfold) = abs(cux(1_IXP:nfold))
   cuy_abs(1_IXP:nfold) = abs(cuy(1_IXP:nfold))
   cuz_abs(1_IXP:nfold) = abs(cuz(1_IXP:nfold))
                      
   cvx_abs(1_IXP:nfold) = abs(cvx(1_IXP:nfold))
   cvy_abs(1_IXP:nfold) = abs(cvy(1_IXP:nfold))
   cvz_abs(1_IXP:nfold) = abs(cvz(1_IXP:nfold))
                      
   cax_abs(1_IXP:nfold) = abs(cax(1_IXP:nfold))
   cay_abs(1_IXP:nfold) = abs(cay(1_IXP:nfold))
   caz_abs(1_IXP:nfold) = abs(caz(1_IXP:nfold))

   if (is_s) then

      call spewin(cux_abs,nfold,df,bw)
      call spewin(cuy_abs,nfold,df,bw)
      call spewin(cuz_abs,nfold,df,bw)

      call spewin(cvx_abs,nfold,df,bw)
      call spewin(cvy_abs,nfold,df,bw)
      call spewin(cvz_abs,nfold,df,bw)

      call spewin(cax_abs,nfold,df,bw)
      call spewin(cay_abs,nfold,df,bw)
      call spewin(caz_abs,nfold,df,bw)

   endif

   file_fft = trim(file_rec)//".fft"
   
   open(unit=10_IXP,file=trim(file_fft),form='unformatted',access='stream',action='write')
   
   do i = 1_IXP,nfold
   
      freq = real(i-1_IXP,kind=RXP)*df
   
      write(unit=10_IXP) freq,cux_abs(i),cuy_abs(i),cuz_abs(i),cvx_abs(i),cvy_abs(i),cvz_abs(i),cax_abs(i),cay_abs(i),caz_abs(i)
   
   enddo
   
   close(10_IXP)
      
   stop

end program
