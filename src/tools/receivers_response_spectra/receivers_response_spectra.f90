! Program to compute the response spectra. see subroutine RSPS

program receivers_response_spectra

   use mod_precision

   use mod_receivers_fft, only : read_receiver_binary

   implicit none

   integer          , parameter                   :: Nrsp    = 1024_IXP
                                                  
   integer                                        :: lu
   integer                                        :: i
   integer                                        :: narg
   integer                                        :: ndt
                                                  
   real   (kind = IXP)                            :: dt
   real   (kind = IXP)                            :: damp

   real   (kind = IXP)                            :: sa(Nrsp)
   real   (kind = IXP)                            :: sv(Nrsp)
   real   (kind = IXP)                            :: sd(Nrsp)
   real   (kind = IXP)                            :: peri(Nrsp)

   real   (kind=RXP), allocatable, dimension(:)         :: time
   real   (kind=RXP), allocatable, dimension(:)         :: ux
   real   (kind=RXP), allocatable, dimension(:)         :: uy
   real   (kind=RXP), allocatable, dimension(:)         :: uz
   real   (kind=RXP), allocatable, dimension(:)         :: vx
   real   (kind=RXP), allocatable, dimension(:)         :: vy
   real   (kind=RXP), allocatable, dimension(:)         :: vz
   real   (kind=RXP), allocatable, dimension(:), target :: ax
   real   (kind=RXP), allocatable, dimension(:), target :: ay
   real   (kind=RXP), allocatable, dimension(:), target :: az

   real   (kind=RXP)             , dimension(:), pointer :: acc
                                                
   character(len =   1)                           :: c1
   character(len =   1)                           :: component
   character(len = 255)                           :: pname
   character(len = 255)                           :: fname

!
!->check command line validity

   narg = command_argument_count()

   if ( narg /= 2_IXP ) then

      call get_command_argument(0_IXP,pname)

      write(*,'(/,a,//,a,/)') "Usage: "//trim(adjustl(pname))//" efispec3d_receiver_filename component","Ex: "//trim(adjustl(pname))//" test.000001.gpl x"

      stop

   endif

!
!->read acceleration in efispec3d receiver file

   call get_command_argument(1,fname)
   call get_command_argument(2,component)

   call read_receiver_binary(fname,ndt,dt,time,ux,uy,uz,vx,vy,vz,ax,ay,az)

   if (component == "x") then

      acc => ax

   elseif(component == "y") then

      acc => ay

   elseif(component == "z") then

      acc => az

   else

      stop "error: invalid component"

   endif

!
!->Create frequency axis

   call logspace(log10(0.02_RXP),log10(10.0_RXP),Nrsp,peri)

!  
!  Damping factor for response spectra

   damp = 5.0_RXP/100.0_RXP

!
!  compute RSPS

!  acc(:) = acc(:)*100.0_RXP ! x100 --> cm/s/s

   sa(:) = 0.0_RXP
   sv(:) = 0.0_RXP
   sa(:) = 0.0_RXP

   call RSPS(acc,ndt,dt,damp,peri,Nrsp,sa,sv,sd)

   open(newunit=lu,file=trim(fname)//".rsp.a"//trim(component),action="write")

!  sa(:) = sa(:)/100.0_RXP
!  sv(:) = sv(:)/100.0_RXP
!  sd(:) = sd(:)/100.0_RXP

   do i = 1_IXP,Nrsp

      write(lu,'(4(E15.7,1X))') peri(i),sa(i),sv(i),sd(i)

   enddo

   close(lu)

   stop

!*********************************************************************************************************************
end program receivers_response_spectra
!*********************************************************************************************************************


!
!
!*********************************************************************************************************************
subroutine logspace(x0,xu,n,x)
!*********************************************************************************************************************

! Computes a logarithmic equally spaced grid

   use mod_precision

   implicit none

   integer(kind=IXP), intent(in ) :: n
   real   (kind=RXP), intent(in ) :: x0
   real   (kind=RXP), intent(in ) :: xu
   real   (kind=RXP), intent(out) :: x(n)

   real   (kind=RXP)              :: dx

   integer(kind=IXP)              :: i

   dx = (xu-x0) / real(n-1_IXP,kind=RXP)

   do i=1_IXP,n

      x(i) = 10.0_RXP**(x0 + dx*real(i-1_IXP,kind=RXP))

   enddo

   return

!*********************************************************************************************************************
end subroutine logspace
!*********************************************************************************************************************

!!*********************************************************************************************************************
!subroutine freq2per(x,n)
!!*********************************************************************************************************************
!
!! Computes period from frequency
!
!   implicit none
!
!   integer            n, i
!   real    (kind = 8) x(n)
!
!   do i=1,n
!      if (x(i) .ne. 0.0d0) then
!         x(i) = 1.0d0/x(i)
!      else
!         stop 'null frequency'
!      end if
!   end do
!
!   return
!end subroutine freq2per
!!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

!*********************************************************************************************************************
!   Subroutine RSPS was published by Li Dahua and Di Qingyan in
!   "Earthquake Research in China",Vol.7,N.4,1993(Allerton Press, NY)
!
      SUBROUTINE RSPS(A,N,DT,D,T,M,SA,SV,SD)
!
!   RSPS:  Compute maximum response.
!   On entry --
!     N = number of values given in the time series.
!     M = number of values in the output response spectrum
!     A()= acceleration time series, m/sec/sec.
!     T()= array of oscillator periods
!     D   = damping fraction.
!     DT  = sampling interval, seconds.
!
!   On return --
!     SD()= maximum relative displacement response spectrum, m.
!     SV  =   "        "     velocity       "        "     , m/sec.
!     SA  =   "     absolute acceleration   "        "     , m/sec/sec.
!
!*********************************************************************************************************************

      use mod_precision

      implicit none

      integer(kind=IXP), intent(in ) :: N
      integer(kind=IXP), intent(in ) :: M

      real   (kind=RXP), intent(in ) :: T(M)
      real   (kind=RXP), intent(in ) :: A(N)
      real   (kind=RXP), intent(in ) :: D
      real   (kind=RXP), intent(in ) :: DT
             
      real   (kind=RXP), intent(out) :: SD(M)
      real   (kind=RXP), intent(out) :: SV(M)
      real   (kind=RXP), intent(out) :: SA(M)
             
      real   (kind=RXP)              :: SQD
      real   (kind=RXP)              :: AMAX
      real   (kind=RXP)              :: VMAX
      real   (kind=RXP)              :: DMAX
      real   (kind=RXP)              :: DP
      real   (kind=RXP)              :: DLT
      real   (kind=RXP)              :: W
      real   (kind=RXP)              :: DW
      real   (kind=RXP)              :: W2
      real   (kind=RXP)              :: W2D
      real   (kind=RXP)              :: WSQD
      real   (kind=RXP)              :: Z
      real   (kind=RXP)              :: XT
      real   (kind=RXP)              :: SXT
      real   (kind=RXP)              :: DSXT
      real   (kind=RXP)              :: CXT
      real   (kind=RXP)              :: A11
      real   (kind=RXP)              :: A12
      real   (kind=RXP)              :: A22
      real   (kind=RXP)              :: A21
      real   (kind=RXP)              :: GA1
      real   (kind=RXP)              :: V0
      real   (kind=RXP)              :: A0
      real   (kind=RXP)              :: V1
      real   (kind=RXP)              :: A1
      real   (kind=RXP)              :: DX
      real   (kind=RXP)              :: DXWD
      real   (kind=RXP)              :: VDXWD
      real   (kind=RXP)              :: XA1
      real   (kind=RXP)              :: SA1
      real   (kind=RXP)              :: SV1
      real   (kind=RXP)              :: SD1

      integer(kind=IXP)              :: I
      integer(kind=IXP)              :: J
      integer(kind=IXP)              :: K
      integer(kind=IXP)              :: L

      SQD=SQRT(1.0_RXP-D*D)
      DO 2 J=1,M
      AMAX=0.0_RXP
      VMAX=0.0_RXP
      DMAX=0.0_RXP
      DP=T(J)/10.0_RXP
      L=1_IXP
      IF(DP.LT.DT) L=INT(DT/DP+1.0_RXP-0.00001_RXP)
      DLT=DT/DBLE(L)
      W=6.283185308_RXP/T(J)
      DW=2.0_RXP*D*W
      W2=W*W
      W2D=W2*DLT
      WSQD=W*SQD
      Z=EXP(-D*W*DLT)
      XT=WSQD*DLT
      SXT=SIN(XT)
      DSXT=D*SXT/SQD
      CXT=COS(XT)
      A11=Z*(DSXT+CXT)
      A12=Z*SXT/WSQD
      A21=-A12*W2
      A22=Z*(-DSXT+CXT)
      GA1=A(1)
      V0=0.0_RXP
      A0=-GA1
      DO 1 I=1_IXP,N-1_IXP
      DX=(A(I+1)-A(I))/REAL(L,kind=RXP)
      DXWD=DX/W2D
      DO 11 K=1_IXP,L
      GA1=GA1+DX
      VDXWD=V0+DXWD
      V1=A11*VDXWD+A12*A0-DXWD
      A1=A21*VDXWD+A22*A0
      XA1=GA1+A1
      SA1=ABS(XA1)
      SV1=ABS(V1)
      SD1=ABS(XA1+DW*V1)/W2
      IF(SA1.GT.AMAX) AMAX=SA1
      IF(SV1.GT.VMAX) VMAX=SV1
      IF(SD1.GT.DMAX) DMAX=SD1
      V0=V1
11     A0=A1
1     continue
      SA(J)=AMAX
      SV(J)=VMAX
2     SD(J)=DMAX

      RETURN

!*********************************************************************************************************************
      END SUBROUTINE RSPS
!*********************************************************************************************************************
