!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
module GeoPolygonProc_Module

   use mod_precision
 
   use GeoPoint_Module
   use GeoVector_Module
   use GeoPlane_Module
   use GeoPolygon_Module
   use GeoFace_Module
   use Utility_Module
   
   implicit none
   
   real(kind=RXP), parameter :: MaxUnitMeasureError = 0.001
   
   ! data member
    type GeoPolygonProc

      type(GeoPolygon) :: polygon

      real(kind=RXP) :: x0
      real(kind=RXP) :: x1
      real(kind=RXP) :: y0
      real(kind=RXP) :: y1
      real(kind=RXP) :: z0
      real(kind=RXP) :: z1
      real(kind=RXP) :: maxError

      type(GeoFace) , dimension(:), allocatable :: Faces

      type(GeoPlane), dimension(:), allocatable :: FacePlanes

      integer(kind=IXP) :: NumberOfFaces

      contains

         procedure :: InitGeoPolygonProc
         procedure :: SetBoundary
         procedure :: SetMaxError
         procedure :: SetFacePlanes
         procedure :: PointInside3DPolygon
         procedure :: UpdateMaxError
         procedure :: Delete

    endtype GeoPolygonProc 
   
contains

   subroutine InitGeoPolygonProc(this, polygon)

      implicit none

      class(GeoPolygonProc) :: this
      
      type(GeoPolygon), intent(in) :: polygon      
    
      this%polygon = polygon
      
      call SetBoundary(this)
      call SetMaxError(this)
      call SetFacePlanes(this)
      
   end subroutine InitGeoPolygonProc
   
   subroutine SetBoundary(this)

      implicit none

      class(GeoPolygonProc) :: this

      integer(kind=IXP) :: i, n      
   
      n = this%polygon%n;

      this%x0 = this%polygon%pts(1)%x
      this%y0 = this%polygon%pts(1)%y
      this%z0 = this%polygon%pts(1)%z
      this%x1 = this%polygon%pts(1)%x
      this%y1 = this%polygon%pts(1)%y
      this%z1 = this%polygon%pts(1)%z     
      
      do i = 1, n

         if (this%polygon%pts(i)%x < this%x0) then
            this%x0 = this%polygon%pts(i)%x
         end if
         if (this%polygon%pts(i)%y < this%y0) then
            this%y0 = this%polygon%pts(i)%y
         end if
         if (this%polygon%pts(i)%z < this%z0) then
            this%z0 = this%polygon%pts(i)%z
         end if
         if (this%polygon%pts(i)%x > this%x1) then       
            this%x1 = this%polygon%pts(i)%x
         end if
         if (this%polygon%pts(i)%y > this%y1) then
            this%y1 = this%polygon%pts(i)%y
         end if
         if (this%polygon%pts(i)%z > this%z1) then
            this%z1 = this%polygon%pts(i)%z
         end if         

      enddo
      
   end subroutine SetBoundary
   
   subroutine SetMaxError(this)

      implicit none

      class(GeoPolygonProc) :: this
      
      this%maxError = (abs(this%x0) + abs(this%x1) + &
                       abs(this%y0) + abs(this%y1) + &
                       abs(this%z0) + abs(this%z1)) / 6.0 * MaxUnitMeasureError; 
             
   end subroutine SetMaxError
   
   subroutine SetFacePlanes(this)

      implicit none

      class(GeoPolygonProc) :: this
      
      logical :: isNewFace

      integer(kind=IXP) :: i, j, k, m, n, p, l, onLeftCount, onRightCount, numberOfFaces, maxFaceIndexCount

      real(kind=RXP) :: dis

      type(GeoPoint) :: p0, p1, p2, pt
      type(GeoPlane) :: trianglePlane
      type(GeoPoint), dimension(:), allocatable :: pts
      type(GeoPlane), dimension(:), allocatable :: fpOutward

      integer(kind=IXP), dimension(:), allocatable :: &
         pointInSamePlaneIndex, verticeIndexInOneFace, faceVerticeIndexCount, idx
            
      ! vertices indexes 2d array for all faces, 
      ! variable face index is major dimension, fixed total number of vertices as minor dimension
      ! vertices index is the original index value in the input polygon    
      integer(kind=IXP), dimension(:,:), allocatable :: faceVerticeIndex    
      
      ! face planes for all faces defined with outward normal vector
      allocate(fpOutward(this%polygon%n))
            
      ! indexes of other points that are in same plane 
      ! with the 3 face triangle plane point
      allocate(pointInSamePlaneIndex(this%polygon%n - 3))
      
      ! vertice indexes in one face
      maxFaceIndexCount = this%polygon%n -1
      allocate(verticeIndexInOneFace(maxFaceIndexCount))
      
      numberOfFaces = 0

      do i = 1, this%polygon%n
         
         ! triangle point #1
         p0 = this%polygon%pts(i)

         do j = i + 1, this%polygon%n
         
            ! triangle point #2
            p1 = this%polygon%pts(j)

            do k = j + 1, this%polygon%n
                        
               ! triangle point #3
               p2 = this%polygon%pts(k)

               call trianglePlane%initGeoPlane(p0, p1, p2)           
            
               onLeftCount = 0
               onRightCount = 0
               
               m = 0
               do l = 1, this%polygon%n
                     
                  ! any point except the 3 triangle points
                  if (l .ne. i .and. l .ne. j .and. l .ne. k) then
                  
                     pt = this%polygon%pts(l)

                     dis = trianglePlane * pt
                                       
                     ! if point is in the triangle plane
                     ! add it to pointInSamePlaneIndex
                     if (abs(dis) .lt. this%maxError ) then 
                        m = m + 1                     
                        pointInSamePlaneIndex(m) = l                       
                     else                    
                        if (dis .lt. 0) then
                           onLeftCount = onLeftCount + 1                                           
                        else                       
                           onRightCount = onRightCount + 1
                        end if
                     end if
                     
                  end if
                  
               end do
                        
               n = 0

               do p = 1, maxFaceIndexCount
                  verticeIndexInOneFace(p) = -1
               end do
               
               ! This is a face for a CONVEX 3d polygon. 
               ! For a CONCAVE 3d polygon, this maybe not a face.             
               if ((onLeftCount .eq. 0) .or. (onRightCount .eq. 0)) then
                                 
                  ! add 3 triangle plane point index
                  n = n + 1
                  verticeIndexInOneFace(n) = i
                  n = n + 1
                  verticeIndexInOneFace(n) = j
                  n = n + 1
                  verticeIndexInOneFace(n) = k
                                                            
                  ! if there are other vertices in this triangle plane
                  ! add them to the face plane
                  if (m .gt. 0) then                  
                     do p = 1, m          
                        n = n + 1
                        verticeIndexInOneFace(n) = pointInSamePlaneIndex(p)
                     end do
                  end if
            
                  ! if verticeIndexInOneFace is a new face, 
                  ! add it in the faceVerticeIndex list, 
                  ! add the trianglePlane in the face plane list fpOutward
                  !print *, n, size(verticeIndexInOneFace), size(faceVerticeIndex)
                  isNewFace = .not. list2dContains(faceVerticeIndex, verticeIndexInOneFace, maxFaceIndexCount)
                  
                  if ( isNewFace ) then
                                    
                     numberOfFaces = numberOfFaces + 1   
                                                            
                     call push2d(faceVerticeIndex, verticeIndexInOneFace)
                                                                                 
                     if (onRightCount .eq. 0) then       
                        fpOutward(numberOfFaces) = trianglePlane                                      
                     else if (onLeftCount .eq. 0) then                     
                        fpOutward(numberOfFaces) = -trianglePlane
                     end if
                     
                     call push(faceVerticeIndexCount, n)
                                                               
                  end if
               
               else
                              
                  ! possible reasons:
                  ! 1. the plane is not a face of a convex 3d polygon, 
                  !    it is a plane crossing the convex 3d polygon.
                  ! 2. the plane is a face of a concave 3d polygon
               end if

            end do ! k loop
         end do ! j loop      
      end do ! i loop                        

      ! Number of Faces
      this%NumberOfFaces = numberOfFaces        
                     
      allocate(this%FacePlanes(this%NumberOfFaces))
      allocate(this%Faces(this%NumberOfFaces))
      
      ! loop faces
      do i = 1, this%NumberOfFaces 
          
         ! set FacePlanes  
         this%FacePlanes(i) = GeoPlane(fpOutward(i)%a, fpOutward(i)%b, &
                                 fpOutward(i)%c, fpOutward(i)%d)
                                       
         ! actual vertices count in the face
         n = faceVerticeIndexCount(i)
         
         allocate(pts(n))                 
         allocate(idx(n))
                        
         ! loop face vertices
         do j = 1, n 
                                       
            k = faceVerticeIndex(i, j)                                  
            pt = GeoPoint(this%polygon%pts(k)%x, this%polygon%pts(k)%y, this%polygon%pts(k)%z)
                        
            pts(j) = pt                         
            idx(j) = k
            
         end do

         !set Faces        
         this%Faces(i) = GeoFace(pts, idx)         
         
         deallocate(pts)
         deallocate(idx)
         
      end do   
      
      deallocate(pointInSamePlaneIndex)
      deallocate(verticeIndexInOneFace)
      deallocate(faceVerticeIndex)
      deallocate(faceVerticeIndexCount)
      deallocate(fpOutward)
            
   end subroutine SetFacePlanes
   
   ! main function to be called. check if a point is inside 3d polygon
   logical function PointInside3DPolygon(this, x, y, z) result(ret)

      implicit none

      class(GeoPolygonProc) :: this
      
      real(kind=RXP), intent(in) :: x, y, z            
      integer(kind=IXP)          :: i
      real(kind=RXP)             :: dis

      ! If the point is in the opposite half space with normal vector for all 6 faces, 
      ! then it is inside of the 3D polygon
      ret = .true.
      
      do i = 1, this%NumberOfFaces

         dis = this%FacePlanes(i) * GeoPoint(x, y, z)

         ! If the point is in the same half space with normal vector for any face of the 3D polygon, 
         ! then it is outside of the 3D polygon
         if (dis .gt. 0.0) then
            ret = .false.
            exit
         endif

      enddo
               
   end function PointInside3DPolygon
   
   ! update maxError attribute value of GeoPolygonProc object
   ! maxError is used in SetFacePlanes to threshold a maximum distance to 
   ! check the points nearby the triangle plane if being considered to be inside the triangle plane
   ! maxError default value is calculated from polygon boundary in SetMaxError
   subroutine UpdateMaxError(this, maxError)

      implicit none

      class(GeoPolygonProc) :: this

      real(kind=RXP), intent(in) :: maxError

      this%maxError = maxError

   end subroutine UpdateMaxError

!
!   
!***********************************************************************************************************************************************************************************
   subroutine Delete(self)
!***********************************************************************************************************************************************************************************

      implicit none

      class(GeoPolygonProc), intent(inout) :: self

      if (allocated(self%Faces)     ) deallocate(self%Faces)
      if (allocated(self%FacePlanes)) deallocate(self%FacePlanes)

   end subroutine Delete

end module GeoPolygonProc_Module
