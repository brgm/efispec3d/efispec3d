!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                                  !
!Author : Florent DE MARTIN, July 2013                                             !
!                                                                                  !
!Contact: f.demartin at brgm.fr                                                    !
!                                                                                  !
!This program is free software: you can redistribute it and/or modify it under     !
!the terms of the GNU General Public License as published by the Free Software     !
!Foundation, either version 3 of the License, or (at your option) any later        !
!version.                                                                          !
!                                                                                  !
!This program is distributed in the hope that it will be useful, but WITHOUT ANY   !
!WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A   !
!PARTICULAR PURPOSE. See the GNU General Public License for more details.          !
!                                                                                  !
!You should have received a copy of the GNU General Public License along with      !
!this program. If not, see http://www.gnu.org/licenses/.                           !
!                                                                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                                                                   

program cubit_find_hex_in_polygon

   use mod_precision

   use mod_init_memory, only :&
                              init_array_int&
                             ,init_array_real

   use mod_cubit_find_hex_3D
   use GeoPoint_Module
   use GeoVector_Module
   use GeoPlane_Module
   use GeoPolygon_Module
   use GeoFace_Module
   use Utility_Module
   use GeoPolygonProc_Module

   implicit none

   include 'exodusII.inc'

   type(GeoPolygon)                                          :: polygon
                                                             
   type(GeoPoint)                                            :: testPoint
   type(GeoPoint), allocatable, dimension(:)                 :: verticesArray
                                                             
   type(GeoPolygonProc), allocatable, dimension(:)           :: procObj

   real   (kind=RXP), parameter                              :: EPSILON_MACHINE = epsilon(EPSILON_MACHINE)

   integer(kind=IXP), parameter                              :: N_GEOM_NODE_HEXA  = 8_IXP
                                                   
   real(kind=RXP), allocatable, dimension(:)                 :: x_geom_node !>X coordinates of geometrical nodes
   real(kind=RXP), allocatable, dimension(:)                 :: y_geom_node !>Y coordinates of geometrical nodes
   real(kind=RXP), allocatable, dimension(:)                 :: z_geom_node !>Z coordinates of geometrical nodes
                                                   
   real(kind=RXP)                                            :: ex_version
   real(kind=RXP)                                            :: rdum
   real(kind=RXP)                                            :: x
   real(kind=RXP)                                            :: y
   real(kind=RXP)                                            :: z
                                                   
   integer(kind=IXP), parameter                              :: nHexaGroup     = 1000
   integer(kind=IXP), parameter                              :: nHexaInsideMax = 5000000
                                                   
   integer(kind=IXP)             , dimension(nHexaInsideMax) :: hexaInside
   integer(kind=IXP), allocatable, dimension(:)              :: block_ids
   integer(kind=IXP), allocatable, dimension(:)              :: block_nelem
   integer(kind=IXP), allocatable, dimension(:)              :: block_element_start
   integer(kind=IXP), allocatable, dimension(:,:)            :: hex_conn
   integer(kind=IXP), allocatable, dimension(:)              :: connectivity
   integer(kind=IXP)                                         :: ihexa
   integer(kind=IXP)                                         :: inode
   integer(kind=IXP)                                         :: iface
   integer(kind=IXP)                                         :: igroup
   integer(kind=IXP)                                         :: imat
   integer(kind=IXP)                                         :: currentNode
   integer(kind=IXP)                                         :: nHexaInside
   integer(kind=IXP)                                         :: nNodeInside
   integer(kind=IXP)                                         :: iPolygon
   integer(kind=IXP)                                         :: nPolygon
   integer(kind=IXP)                                         :: ndim
   integer(kind=IXP)                                         :: npoint
   integer(kind=IXP)                                         :: number_of_nodes
   integer(kind=IXP)                                         :: number_of_elements
   integer(kind=IXP)                                         :: number_of_blocks
   integer(kind=IXP)                                         :: number_of_blocks_with_hex
   integer(kind=IXP)                                         :: number_of_node_sets
   integer(kind=IXP)                                         :: number_of_side_sets
   integer(kind=IXP)                                         :: num_elt_block
   integer(kind=IXP)                                         :: num_node_elt_block
   integer(kind=IXP)                                         :: num_attr_elt_block
   integer(kind=IXP)                                         :: iblock
   integer(kind=IXP)                                         :: jblock
   integer(kind=IXP)                                         :: iblock_ids
   integer(kind=IXP)                                         :: is
   integer(kind=IXP)                                         :: numelb
   integer(kind=IXP)                                         :: numlnk
   integer(kind=IXP)                                         :: numatt
   integer(kind=IXP)                                         :: ios
   integer(kind=IXP)                                         :: narg
   integer(kind=IXP)                                         :: ex_cpu_word_size
   integer(kind=IXP)                                         :: ex_io_word_size
   integer(kind=IXP)                                         :: ex_file_id
   integer(kind=IXP)                                         :: cubit_file_id
                                                   
   logical                                                   :: isNodeInside
   logical, allocatable, dimension(:)                        :: isHexaInsidePolygon
                                                             
   character(len=MXLNLN)                                     :: ex_title
   character(len=MXSTLN), allocatable, dimension(:)          :: ex_block_name
   character(len=MXSTLN)                                     :: typ
   character(len=255)                                        :: prog_name
   character(len= 90)                                        :: prefix
   character(len= 90)                                        :: file_mesh
   character(len= 90), allocatable, dimension(:)             :: file_poly
   character(len= 90)                                        :: myformat
   character(len= 15)                                        :: cn
   character(len=  3)                                        :: ext
   character(len=  2)                                        :: cPolygon

   write(*,*) ""
   write(*,*) "*********************************************"
   write(*,*) "             CUBIT FIND HEX IN POLYGON       "
   write(*,*) "*********************************************"

   narg = command_argument_count()

   if (narg <= 1_IXP) then

      call get_command_argument(0,prog_name)

      write(*,'(A)') "Usage : "//trim(adjustl(prog_name))//" mesh_file polygon_file_1 .. polygon_file_n"

      stop

   endif

   call get_command_argument(1_IXP,file_mesh)

   file_mesh = trim(adjustl(file_mesh))

   nPolygon = narg - 1_IXP

   allocate(file_poly(nPolygon))

   do iPolygon = 1_IXP,nPolygon
   
      call get_command_argument(iPolygon+1_IXP,file_poly(iPolygon))
      
      file_poly(iPolygon) = trim(adjustl(file_poly(iPolygon)))

      write (*,"(a)") trim(file_poly(iPolygon))
   
   enddo
 
   prefix = file_mesh(1_IXP:len_trim(file_mesh)-4_IXP)
   ext    = file_mesh(len_trim(file_mesh)-2_IXP:len_trim(file_mesh))

   if (ext /= "ex2") stop "error: unknown mesh file extension. Only exodusII mesh file are accepted."

!
!->create polygon to check if a hex element is inside or outside

   allocate(procObj(nPolygon))

   do iPolygon = 1_IXP,nPolygon

      npoint = 0_IXP

      open(unit=10_IXP,file=trim(file_poly(iPolygon)),action='read') 
 
      do
 
         read(unit=10_IXP,fmt=*,iostat=ios) rdum
 
         if (ios /= 0_IXP) exit
 
         npoint = npoint + 1_IXP
 
      enddo
 
      rewind(10)
 
      allocate(verticesArray(npoint))
 
      do inode = 1_IXP,npoint
 
         read(10,*) x,y,z
 
         verticesArray(inode) = GeoPoint(x,y,z)
 
      enddo
 
      close(10)
 
      polygon = GeoPolygon(verticesArray)
 
      write(*,*) ""
      write(*,*) "polygon info"
 
      do inode = 1_IXP, polygon%n
 
         write(*,*) polygon%idx(inode), ": (", polygon%pts(inode)%x, ", ", polygon%pts(inode)%y, ", ", polygon%pts(inode)%z, ")"
 
      enddo
 
      call procObj(iPolygon)%InitGeoPolygonProc(polygon)
 
      write(*,*) ""
      write(*,*) "GeoPolygonProc ",iPolygon," info"
      write(*,*) "number of faces generated = ",procObj(iPolygon)%NumberOfFaces

      do iface = 1_IXP,procObj(iPolygon)%NumberOfFaces

         write(*,'(100(I3))') procObj(iPolygon)%Faces(iface)%idx(:)

      enddo

      deallocate(verticesArray)

      call polygon%destructor()

   enddo

!
!->read exodusII info

   ex_file_id = exopen(trim(adjustl(file_mesh)),EXWRIT,ex_cpu_word_size,ex_io_word_size,ex_version,ios)

   print *,"exodusII fil ID = ",ex_file_id

   if (ios /= 0_IXP) stop "error: function exopen"

   call exgini(ex_file_id,ex_title,ndim,number_of_nodes,number_of_elements,number_of_blocks,number_of_node_sets,number_of_side_sets,ios)

   if (ios /= 0_IXP) stop "error: subroutine exgini"

   write(*,*) ""
   write(*,*) "information about exodusII mesh"
   write(*,*) "number of geometric nodes = ",number_of_nodes
   write(*,*) "number of elements        = ",number_of_elements
   write(*,*) "number of blocks          = ",number_of_blocks
   write(*,*) "number of node sets       = ",number_of_node_sets
   write(*,*) "number of side sets       = ",number_of_side_sets

!    
!->Arrays allocation
  
   ios = init_array_real(x_geom_node,number_of_nodes,"cubit_find_hex_in_polygon:x_geom_node")

   ios = init_array_real(y_geom_node,number_of_nodes,"cubit_find_hex_in_polygon:y_geom_node")

   ios = init_array_real(z_geom_node,number_of_nodes,"cubit_find_hex_in_polygon:z_geom_node")

   ios = init_array_int(block_ids,number_of_blocks,"cubit_find_hex_in_polygon:block_ids")

   ios = init_array_int(block_nelem,number_of_blocks,"cubit_find_hex_in_polygon:block_nelem")

   ios = init_array_int(block_element_start,number_of_blocks,"cubit_find_hex_in_polygon:block_element_start")

   allocate(ex_block_name(number_of_blocks))

!
!->get coordinates from exodusII mesh file

   call exgcor(ex_file_id,x_geom_node,y_geom_node,z_geom_node,ios)
  
   if (ios /= 0_IXP) stop "error: subroutine exgcor"

!
!->get block names

   call exgnams(ex_file_id,EXEBLK,number_of_blocks,ex_block_name,ios)

!
!->get block ids, number of elements per block, and number of blocks with HEX

   number_of_blocks_with_hex = 0_IXP

   call exgebi(ex_file_id,block_ids,ios)

   do iblock = 1_IXP,number_of_blocks

      call exgelb(ex_file_id,block_ids(iblock),typ,numelb,numlnk,numatt,ios)

      block_nelem(iblock) = numelb

      if (numlnk == N_GEOM_NODE_HEXA) number_of_blocks_with_hex = number_of_blocks_with_hex + 1_IXP

   enddo


!
!->set start index of the first element of each block to get local (block) to global element numbering. start index is set by
!  suppossing that block are ordered from 1 to number_of_blocks.

   do iblock = 1_IXP,number_of_blocks

      do jblock = 1_IXP,iblock-1_IXP

         block_element_start(iblock) = block_element_start(iblock) + block_nelem(jblock)

      enddo

   enddo

!
!->init CUBIT journal file and variables
 
   open(newunit=cubit_file_id,file="cubit_hexaInside"//".jou")

   write(unit=cubit_file_id,fmt='(a)') "import mesh '"//trim(adjustl(file_mesh))//"' no_geom"

   write(cn,'(i15)') nHexaGroup
  
   myformat = "(a,"//trim(adjustl(cn))//"(i0,1X))"

!
!->store cubit's block 'fsu' and 'prx' into cubit's group 'fsu' and 'prx', respectively, to switch them at the end of block declaration

   do iblock = 1_IXP,number_of_blocks

      selectcase(trim(ex_block_name(iblock))) 

      case('prx')

         write(unit=cubit_file_id,fmt='(a,I0)') "group 'prx' add face in block ",block_ids(iblock)
         write(unit=cubit_file_id,fmt='(a,I0)') "delete block ",block_ids(iblock)

      case('fsu')

         write(unit=cubit_file_id,fmt='(a,I0)') "group 'fsu' add face in block ",block_ids(iblock)
         write(unit=cubit_file_id,fmt='(a,I0)') "delete block ",block_ids(iblock)

      endselect

   enddo

!
!->loop over polygons

   do iPolygon = 1_IXP,nPolygon

      nHexaInside = 0_IXP
!
!---->loop over blocks to find hexa located inside closed curve

      do iblock = 1_IXP,number_of_blocks
     
         call exgelb(ex_file_id,block_ids(iblock),typ,numelb,numlnk,numatt,ios)


! 
!------->test only hexahedron (i.e. not quadrangle)
     
         if (numlnk == N_GEOM_NODE_HEXA) then
     
            if (allocated(connectivity)) deallocate(connectivity)
     
            ios = init_array_int(connectivity,numelb*numlnk,"cubit_find_hex_in_polygon:connectivity")
     
            call exgelc(ex_file_id,block_ids(iblock),connectivity,ios)

            do ihexa = 1_IXP,numelb
            
               nNodeInside = 0_IXP

               do inode = 1_IXP,numlnk
            
                  currentNode = connectivity((ihexa-1_IXP)*N_GEOM_NODE_HEXA+inode)
            
                  x = x_geom_node(currentNode)
                  y = y_geom_node(currentNode)
                  z = z_geom_node(currentNode)
            
                  testPoint = GeoPoint(x,y,z)
            
                  isNodeInside = procObj(iPolygon)%PointInside3DPolygon(testPoint%x,testPoint%y,testPoint%z)
            
                  if (isNodeInside) then
            
                     nNodeInside = nNodeInside + 1_IXP
            
                  endif
            
               enddo
            
               if (nNodeInside >= 1_IXP) then
            
                  nHexaInside = nHexaInside + 1_IXP
            
                  if (nHexaInside > nHexaInsideMax) stop "error: nHexaInside > nHexaInsideMax"
            
                  hexaInside(nHexaInside) = block_element_start(iblock) + ihexa
            
               endif
            
            enddo

         endif !is HEX8 ?
     
      enddo !iblock
     
      write(*,*) ""
      write(*,*) "number of hexahedron elements inside polygon ",iPolygon," = ",nHexaInside

!
!---->write hexa inside polygon 'iPolygon' for cubit 

      do igroup = 1_IXP,int(nHexaInside/nHexaGroup)
      
         write(unit=cubit_file_id,fmt =trim(adjustl(myformat))) "group '"//trim(file_poly(iPolygon))//"' add hex ",(hexaInside(ihexa),ihexa=(igroup-1_IXP)*nHexaGroup+1_IXP,igroup*nHexaGroup)
      
      enddo
      
      write(unit=cubit_file_id,fmt =trim(adjustl(myformat))) "group '"//trim(file_poly(iPolygon))//"' add hex ",(hexaInside(ihexa),ihexa=int(nHexaInside/nHexaGroup)*nHexaGroup+1_IXP,nHexaInside)

!
!---->remove hexa inside polygon 'iPolygon' from existing cubit blocks

      do iblock = 1_IXP,number_of_blocks

         call exgelb(ex_file_id,block_ids(iblock),typ,numelb,numlnk,numatt,ios)

         if (numlnk == N_GEOM_NODE_HEXA) then

            write(unit=cubit_file_id,fmt ='(a,i0,a)') "block ",iblock," group "//trim(file_poly(iPolygon))//" remove"

         endif

      enddo

!
!---->put hexa inside polygon 'iPolygon' in new cubit block

      iblock = number_of_blocks          + iPolygon
      imat   = number_of_blocks_with_hex + iPolygon
     
      write(cPolygon,'(I2.2)') imat
      
      write(unit=cubit_file_id,fmt ='(a,i0,a)') "block ",imat," hex in group "//trim(file_poly(iPolygon))
      write(unit=cubit_file_id,fmt ='(a,i0,a)') "block ",imat," name 'l"//trim(cPolygon)//"'"

   enddo !iPolygon

!
!->set cubit block name because they are lost when importing mesh (exodusII bug ?)

   jblock = number_of_blocks_with_hex + nPolygon

   do iblock = 1_IXP,number_of_blocks

      call exgelb(ex_file_id,block_ids(iblock),typ,numelb,numlnk,numatt,ios)

      if (numlnk == N_GEOM_NODE_HEXA) then

         write(unit=cubit_file_id,fmt ='(a,i0,a)') "block ",iblock," name '"//trim(ex_block_name(iblock))//"'"

      else

         jblock = jblock + 1_IXP

         write(unit=cubit_file_id,fmt ='(a,i0,a)') "block ",jblock," group "//trim(ex_block_name(iblock))
         write(unit=cubit_file_id,fmt ='(a,i0,a)') "block ",jblock," name '"//trim(ex_block_name(iblock))//"'"

      endif

   enddo

!
!-->compress ids, export mesh and list model

   write(unit=cubit_file_id,fmt ='(a)') "compress ids all"

   write(unit=cubit_file_id,fmt ='(a)') "export mesh '"//trim(adjustl(file_mesh))//"' block all dimension 3 overwrite"

   write(unit=cubit_file_id,fmt ='(a)') "list model"
!
!->close files

   call exclos(ex_file_id,ios)

   if (ios /= 0_IXP) stop 'error while closing exodusII mesh file'

   close(cubit_file_id)

!
!->destruct polygon objects

   do iPolygon = 1_IXP,nPolygon

      call procObj(iPolygon)%Delete

   enddo

   stop

end program cubit_find_hex_in_polygon
