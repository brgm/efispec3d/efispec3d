!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!<b>This file contains the main spectral finite element program: EFISPEC3D</b>.
!!
!>@brief
!!This program is composed by :
!!- an initialization phase;
!!- a time loop;
!!- a finalization phase.

!>@return void
program snapshots_filter

   use mpi

   use mod_precision
   
   use mod_global_variables, only :&
                                    IG_LST_UNIT&
                                   ,LG_SNAPSHOT_VTK&
                                   ,LG_SNAPSHOT_SURF_GLL&
                                   ,LG_SNAPSHOT_SURF_GLL_FILTER&
                                   ,LG_FIR_FILTER&
                                   ,cg_prefix&
                                   ,get_prefix&
                                   ,ig_ncpu&
                                   ,ig_myrank&
                                   ,ig_mpi_comm_simu&
                                   ,error_stop&
                                   ,lg_boundary_absorption

   use mod_efi_mpi

   use mod_init_efi

   use mod_init_mesh
   
   use mod_init_medium

   use mod_init_time_step

   use mod_filter

   use mod_init_memory

   use mod_post_snapshot_filter

   implicit none
   
   real   (kind=R64), allocatable, dimension(:) :: buffer_double
   real   (kind=R64), allocatable, dimension(:) :: buffer_double_all_cpu

   real   (kind=R64)                            :: start_time
   real   (kind=R64)                            :: end_time
                                                  
   real   (kind=R64)                            :: dltim1
   real   (kind=R64)                            :: dltim2
                                                  
   real   (kind=R64)                            :: time_init_mesh          = ZERO_R64
   real   (kind=R64)                            :: time_init_gll           = ZERO_R64           
   real   (kind=R64)                            :: time_init_mpi_buffers   = ZERO_R64
   real   (kind=R64)                            :: time_init_medium        = ZERO_R64
   real   (kind=R64)                            :: time_init_time_step     = ZERO_R64
   real   (kind=R64)                            :: time_init_fir_filter    = ZERO_R64
   real   (kind=R64)                            :: time_init_source        = ZERO_R64
   real   (kind=R64)                            :: time_init_receiver      = ZERO_R64
   real   (kind=R64)                            :: time_init_jacobian      = ZERO_R64
   real   (kind=R64)                            :: time_init_mass_matrix   = ZERO_R64
   real   (kind=R64)                            :: time_compute_dcsource   = ZERO_R64
   real   (kind=R64)                            :: time_init_snapshot      = ZERO_R64
   real   (kind=R64)                            :: time_memory_consumption = ZERO_R64
                                               
   integer(kind=IXP), parameter                 :: NTIME_INIT = 12_IXP
   integer(kind=IXP)                            :: ios
   integer(kind=IXP)                            :: icpu
   integer(kind=IXP)                            :: itime
                                               
!
!
!*************************************************************************************************************************
!*************************************************************************************************************************
!phase 1: initialization
!*************************************************************************************************************************
!*************************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   call init_mpi()
   start_time = mpi_wtime()
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   cg_prefix = get_prefix()

   call init_input_variables(cg_prefix)
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_mesh = mpi_wtime()

   call init_mesh()

   time_init_mesh = mpi_wtime() - time_init_mesh
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_gll = mpi_wtime()

   call init_gll_nodes()
   call init_gll_nodes_coordinates()

   time_init_gll = mpi_wtime() - time_init_gll
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
!  time_init_mpi_buffers = mpi_wtime()
!
!  call init_mpi_buffers()
!
!  time_init_mpi_buffers = mpi_wtime() - time_init_mpi_buffers
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_medium = mpi_wtime()

   call init_hexa_medium()

   if (lg_boundary_absorption) call init_quadp_medium()

   time_init_medium = mpi_wtime() - time_init_medium
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_time_step = mpi_wtime()

   call init_time_step()

   time_init_time_step = mpi_wtime() - time_init_time_step
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_fir_filter = mpi_wtime()

   if (LG_FIR_FILTER) call init_fir_filter()

   time_init_fir_filter = mpi_wtime() - time_init_fir_filter
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_snapshot = mpi_wtime()
   
   if (LG_SNAPSHOT_SURF_GLL) call post_init_snapshot_surface_gll()
   
   time_init_snapshot = mpi_wtime() - time_init_snapshot
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_memory_consumption = mpi_wtime()

   call memory_consumption()

   time_memory_consumption = mpi_wtime() - time_memory_consumption
   !**********************************************************************************************************************


   end_time = (mpi_wtime() - start_time)

   call mpi_reduce(end_time,dltim1,ONE_IXP,mpi_double_precision,mpi_max,ZERO_IXP,ig_mpi_comm_simu,ios)


   !
   !
   !**********************************************************************************************************************
   !summarize elapsed time in subroutines for initialization
   !**********************************************************************************************************************
   allocate(buffer_double(NTIME_INIT))

   allocate(buffer_double_all_cpu(NTIME_INIT*ig_ncpu))

   buffer_double( 1_IXP) = time_init_mesh
   buffer_double( 2_IXP) = time_init_gll
   buffer_double( 3_IXP) = time_init_mpi_buffers
   buffer_double( 4_IXP) = time_init_medium
   buffer_double( 5_IXP) = time_init_source
   buffer_double( 6_IXP) = time_init_receiver
   buffer_double( 7_IXP) = time_init_jacobian
   buffer_double( 8_IXP) = time_init_mass_matrix
   buffer_double( 9_IXP) = time_compute_dcsource
   buffer_double(10_IXP) = time_init_snapshot
   buffer_double(11_IXP) = time_memory_consumption
   buffer_double(12_IXP) = time_init_time_step

   call mpi_gather(buffer_double,NTIME_INIT,mpi_double_precision,buffer_double_all_cpu,NTIME_INIT,mpi_double_precision,ZERO_IXP,ig_mpi_comm_simu,ios)

   if (ig_myrank == ZERO_IXP) then

      write(unit=IG_LST_UNIT,fmt='(" ",/,a)') "elapsed time for initialization"
      write(unit=IG_LST_UNIT,fmt='(      a)') "             init_mesh    init_gll_nodes  init_mpi_buff  init_medium    init_source    init_receiver    init_jaco     init_mass      init_dcs      init_snapshot   init_memory   init_time_step"

      do icpu = ONE_IXP,ig_ncpu
         write(unit=IG_LST_UNIT,fmt='(a,i6,1x,12(e14.7,1x))') "cpu ",icpu-ONE_IXP,(buffer_double_all_cpu((icpu-ONE_IXP)*NTIME_INIT+itime),itime=ONE_IXP,NTIME_INIT)
      enddo

   endif

   deallocate(buffer_double)
   deallocate(buffer_double_all_cpu)

!
!
!*********************************************************************************************************************
!*********************************************************************************************************************
!phase 3: output elapsed time info and finalize computation
!*********************************************************************************************************************
!*********************************************************************************************************************

   !
   !
   !***************************************************************************************************************
   !output results in file *.lst
   !***************************************************************************************************************
   if (ig_myrank == ZERO_IXP) then

      write(IG_LST_UNIT,'(" ",/,a,e15.7,a)') "elapsed time for initialization        = ",dltim1," s"
   
   endif

!
!
!*********************************************************************************************************************
!*********************************************************************************************************************
!phase 4: post-processing
!*********************************************************************************************************************
!*********************************************************************************************************************
   if ( (LG_SNAPSHOT_SURF_GLL) .and. (LG_SNAPSHOT_SURF_GLL_FILTER) ) then

      call mpi_barrier(ig_mpi_comm_simu,ios)
 
      start_time = mpi_wtime()

      call filter_snapshot_surface_gll()

      end_time = (mpi_wtime() - start_time)

      call mpi_reduce(end_time,dltim2,ONE_IXP,mpi_double_precision,mpi_max,ZERO_IXP,ig_mpi_comm_simu,ios)

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'("",/a,e15.7,a)') "elapsed time for post-processing = ",dltim2," s"

      endif

   endif
   
   call mpi_finalize(ios)
   stop

end program snapshots_filter
