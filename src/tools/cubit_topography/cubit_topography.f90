!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                                  !
!Author : Florent DE MARTIN, July 2013                                             !
!                                                                                  !
!Contact: f.demartin at brgm.fr                                                    !
!                                                                                  !
!This program is free software: you can redistribute it and/or modify it under     !
!the terms of the GNU General Public License as published by the Free Software     !
!Foundation, either version 3 of the License, or (at your option) any later        !
!version.                                                                          !
!                                                                                  !
!This program is distributed in the hope that it will be useful, but WITHOUT ANY   !
!WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A   !
!PARTICULAR PURPOSE. See the GNU General Public License for more details.          !
!                                                                                  !
!You should have received a copy of the GNU General Public License along with      !
!this program. If not, see http://www.gnu.org/licenses/.                           !
!                                                                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                                                                   

program cubit_topography

   use mod_precision

   use mod_topography

   implicit none

   include 'exodusII.inc'

   real(kind=RXP), parameter                      :: EPSILON_MACHINE = epsilon(EPSILON_MACHINE)

   real(kind=RXP), allocatable, dimension(:)      :: x_geom_node !>X coordinates of geometrical nodes
   real(kind=RXP), allocatable, dimension(:)      :: y_geom_node !>Y coordinates of geometrical nodes
   real(kind=RXP), allocatable, dimension(:)      :: z_geom_node !>Z coordinates of geometrical nodes
                                        
   real(kind=RXP), allocatable, dimension(:)      :: dem1_x
   real(kind=RXP), allocatable, dimension(:)      :: dem1_y
   real(kind=RXP), allocatable, dimension(:,:)    :: dem1_z
                                        
   real(kind=RXP), allocatable, dimension(:)      :: dem2_x
   real(kind=RXP), allocatable, dimension(:)      :: dem2_y
   real(kind=RXP), allocatable, dimension(:,:)    :: dem2_z
                                        
   real(kind=RXP)                                 :: x_node
   real(kind=RXP)                                 :: y_node
   real(kind=RXP)                                 :: z_node
                                        
   real(kind=RXP)                                 :: xmin
   real(kind=RXP)                                 :: xmax
   real(kind=RXP)                                 :: ymin
   real(kind=RXP)                                 :: ymax
   real(kind=RXP)                                 :: zmin
   real(kind=RXP)                                 :: zmax
                                        
   real(kind=RXP)                                 :: dem1_x0
   real(kind=RXP)                                 :: dem1_y0
   real(kind=RXP)                                 :: dem1_dx
   real(kind=RXP)                                 :: dem1_dy
   real(kind=RXP)                                 :: dem2_x0
   real(kind=RXP)                                 :: dem2_y0
   real(kind=RXP)                                 :: dem2_dx
   real(kind=RXP)                                 :: dem2_dy

   real(kind=RXP)                                 :: z_dum
   real(kind=RXP)                                 :: z_apply_topo
   real(kind=RXP)                                 :: z_min_dem1
   real(kind=RXP)                                 :: z_dem1
   real(kind=RXP)                                 :: ex_version

   real(kind=RXP)                                 :: x_min_box 
   real(kind=RXP)                                 :: x_max_box
   real(kind=RXP)                                 :: y_min_box 
   real(kind=RXP)                                 :: y_max_box

   integer(kind=IXP), allocatable, dimension(:)   :: nodeset
   integer(kind=IXP)                              :: ndim
   integer(kind=IXP)                              :: dem1_nx
   integer(kind=IXP)                              :: dem1_ny
   integer(kind=IXP)                              :: dem2_nx
   integer(kind=IXP)                              :: dem2_ny
   integer(kind=IXP)                              :: number_of_nodes
   integer(kind=IXP)                              :: number_of_elements
   integer(kind=IXP)                              :: number_of_blocks
   integer(kind=IXP)                              :: number_of_node_sets
   integer(kind=IXP)                              :: number_of_side_sets
   integer(kind=IXP)                              :: num_node_nodeset
   integer(kind=IXP)                              :: num_df_nodeset
   integer(kind=IXP)                              :: inode
   integer(kind=IXP)                              :: jnode
   integer(kind=IXP)                              :: ios
   integer(kind=IXP)                              :: narg
   integer(kind=IXP)                              :: ex_cpu_word_size
   integer(kind=IXP)                              :: ex_io_word_size
   integer(kind=IXP)                              :: ex_file_id

   logical(kind=IXP)                              :: is_dem_info
   logical(kind=IXP)                              :: is_box
                                                  
   character(len=MXLNLN)                          :: ex_title
   character(len= 90)                             :: prefix
   character(len=255)                             :: file_mesh
   character(len=255)                             :: file_dem1
   character(len=255)                             :: file_dem2
   character(len=255)                             :: prog_name
   character(len= 20)                             :: cz_hexa
   character(len=  3)                             :: ext
   character(len=  3)                             :: dem_file1_ext
   character(len=  3)                             :: dem_file2_ext

   write(*,*) ""
   write(*,*) "*********************************************"
   write(*,*) "             CUBIT TOPOGRAPHY                "
   write(*,*) "*********************************************"

   narg = command_argument_count()

   if (narg /= 4) then

      call get_command_argument(0,prog_name)

      write(*,'(A)') "Usage : "//trim(adjustl(prog_name))//" mesh_file dem_file1 z_apply_topo dem_file2"

      stop

   endif
 
   call get_command_argument(1,file_mesh)
   call get_command_argument(2,file_dem1)
   call get_command_argument(3,cz_hexa)
   call get_command_argument(4,file_dem2)

   file_mesh = trim(adjustl(file_mesh))
   file_dem1 = trim(adjustl(file_dem1))
   file_dem2 = trim(adjustl(file_dem2))

   read(cz_hexa,*) z_apply_topo

   write(*,*) ""
   write(*,*) "topography will be mapped above z = ",z_apply_topo

   prefix = file_mesh(1:len_trim(file_mesh)-4)
   ext    = file_mesh(len_trim(file_mesh)-2:len_trim(file_mesh))

   if (ext /= "ex2") stop "error in cubit_topography: unknown mesh file extension. Only exodusII mesh file are accepted."

!
!->get extension of the dem files (.asc or .grd)

   if (file_dem1 /= "zplane") then

      dem_file1_ext = file_dem1(len_trim(file_dem1)-2:len_trim(file_dem1))

   endif

   dem_file2_ext = file_dem2(len_trim(file_dem2)-2:len_trim(file_dem2))

   if ( (file_dem1 /= "zplane") .and. (dem_file1_ext /= dem_file2_ext) ) then

      write(*,*) "error in cubit_topography: dem files must have the same extension, either .asc or .grd"
      stop

   endif

!
!->check the existency of file .dem.info

   inquire(file=trim(prefix)//".dem.info",exist=is_dem_info)

   if (is_dem_info) then

      open(unit=10,file=trim(prefix)//".dem.info")
      read(10,*) z_min_dem1
      close(10)

      write(*,*) "z_min_dem1 = ",z_min_dem1

   endif

!
!->check the existency of file box.txt

   inquire(file="box.txt",exist=is_box)

   if (is_box) then

      open(unit=10,file="box.txt")
      read(10,*) x_min_box,x_max_box
      read(10,*) y_min_box,y_max_box
      close(10)

      write(*,*) "x_min_box/x_max_box = ",x_min_box,x_max_box
      write(*,*) "y_min_box/y_max_box = ",y_min_box,y_max_box

   endif 


!
!->read file that contains topography

   if (file_dem1 /= "zplane") then

      if (dem_file1_ext == "asc") then

         call read_topography_asc(file_dem1,dem1_x,dem1_y,dem1_z,dem1_dx,dem1_dy,dem1_x0,dem1_y0,dem1_nx,dem1_ny)

      else

         call read_topography_grd(file_dem1,dem1_x,dem1_y,dem1_z,dem1_dx,dem1_dy,dem1_x0,dem1_y0,dem1_nx,dem1_ny)

      endif

   else

      write(*,*) ""
      write(*,*) "dem1 is a zplane"

   endif

   if (dem_file2_ext == "asc") then

      call read_topography_asc(file_dem2,dem2_x,dem2_y,dem2_z,dem2_dx,dem2_dy,dem2_x0,dem2_y0,dem2_nx,dem2_ny)

   else

      call read_topography_grd(file_dem2,dem2_x,dem2_y,dem2_z,dem2_dx,dem2_dy,dem2_x0,dem2_y0,dem2_nx,dem2_ny)

   endif

!
!->read exodusII info

   ex_file_id = exopen(trim(adjustl(file_mesh)),EXWRIT,ex_cpu_word_size,ex_io_word_size,ex_version,ios)

   print *,"exodusII fil ID = ",ex_file_id

   if (ios /= 0) stop "error in cubit_topography: function exopen"

   call exgini(ex_file_id,ex_title,ndim,number_of_nodes,number_of_elements,number_of_blocks,number_of_node_sets,number_of_side_sets,ios)

   if (ios /= 0) stop "error in cubit_topography: subroutine exgini"

   write(*,*) ""
   write(*,*) "information about exodusII mesh"
   write(*,*) "number of geometric nodes = ",number_of_nodes
   write(*,*) "number of elements        = ",number_of_elements
   write(*,*) "number of blocks          = ",number_of_blocks
   write(*,*) "number of node sets       = ",number_of_node_sets
   write(*,*) "number of side sets       = ",number_of_side_sets

!    
!->Arrays allocation of coordinates
  
   allocate(x_geom_node(number_of_nodes))
   allocate(y_geom_node(number_of_nodes))
   allocate(z_geom_node(number_of_nodes))

!
!->get coordinates from exodusII mesh file

   call exgcor(ex_file_id,x_geom_node,y_geom_node,z_geom_node,ios)
  
   if (ios /= 0) stop "error in cubit_topography: subroutine exgcor"

!
!->if a nodeset exist, only apply topography on the nodeset, and stop the code

   if (number_of_node_sets == 1) then 

      write(*,*) ""
      write(*,'(a)') "nodeset found. surface topography adjusted to dem2"

      call exgnp(ex_file_id,1,num_node_nodeset,num_df_nodeset,ios)
 
      write(*,'(a,I0)') "number of nodes in node set 1 = ",num_node_nodeset
 
      allocate(nodeset(num_node_nodeset))
 
      call exgns(ex_file_id,1,nodeset,ios)

!
!---->set z-coordinate of nodeset to the elevation of dem2

      do inode = 1,num_node_nodeset

         jnode = nodeset(inode)

         x_node = x_geom_node(jnode)
         y_node = y_geom_node(jnode)

         z_node = get_z(x_node,y_node,dem2_x,dem2_y,dem2_z,dem2_nx,dem2_ny)

         z_geom_node(jnode) = z_node

      enddo

!
!--->update exodusII mesh file

      call expcor(ex_file_id,x_geom_node,y_geom_node,z_geom_node,ios)
 
      if (ios /= 0) stop "error in cubit_topography: subroutine expcor"
 
      call exupda(ex_file_id,ios)
 
      if (ios /= 0) stop "error in cubit_topography: subroutine exupda"
 
      call exclos(ex_file_id,ios)
 
      if (ios /= 0) stop "error in cubit_topography: subroutine exclos"

!
!---->find min/max

      xmin      = minval(x_geom_node(:))
      xmax      = maxval(x_geom_node(:))
      ymin      = minval(y_geom_node(:))
      ymax      = maxval(y_geom_node(:))
      zmin      = minval(z_geom_node(:))
      zmax      = maxval(z_geom_node(:))

      write(*,*) ""
      write(*,'(a,E22.10)') "xmin = ",xmin
      write(*,'(a,E22.10)') "xmax = ",xmax
      write(*,'(a,E22.10)') "ymin = ",ymin
      write(*,'(a,E22.10)') "ymax = ",ymax
      write(*,'(a,E22.10)') "zmin = ",zmin
      write(*,'(a,E22.10)') "zmax = ",zmax

!
!---->write cubit journal file

     !call write_cubit_journal_make_boundary(xmin,xmax,ymin,ymax,zmin,zmax)

      stop "all done!"

   endif

! 
!->compute new z that include topo

   if (file_dem1 /= "zplane") then

      do inode = 1,number_of_nodes
      
         x_node = x_geom_node(inode)
         y_node = y_geom_node(inode)
         z_node = z_geom_node(inode)

!
!------->scale new 'z_apply_topo' from flat free surface at z = 0.0 to z of dem1 along the length z_min_dem1 (i.e., old z_apply_topo)

         z_dum = z_apply_topo
        
         call compute_z(x_node,y_node,z_dum,dem1_x,dem1_y,dem1_z,dem1_nx,dem1_ny,0.0,z_min_dem1)

         if (is_box) then

            if ( x_node > x_min_box .and. x_node < x_max_box .and. y_node > y_min_box .and. y_node < y_max_box ) then

               if ( (z_node >= (z_dum-EPSILON_MACHINE)) ) then
             
                  z_dem1 = get_z(x_node,y_node,dem1_x,dem1_y,dem1_z,dem1_nx,dem1_ny)
           
                  call compute_z(x_node,y_node,z_node,dem2_x,dem2_y,dem2_z,dem2_nx,dem2_ny,z_dem1,z_dum)

                  z_geom_node(inode) = z_node
             
               endif

            endif

         else

            if ( (z_node >= (z_dum-EPSILON_MACHINE)) ) then
       
               z_dem1 = get_z(x_node,y_node,dem1_x,dem1_y,dem1_z,dem1_nx,dem1_ny)
          
               call compute_z(x_node,y_node,z_node,dem2_x,dem2_y,dem2_z,dem2_nx,dem2_ny,z_dem1,z_dum)
          
               z_geom_node(inode) = z_node
       
            endif

         endif
      
      enddo

   else

      z_dem1 = maxval(z_geom_node(:))

      do inode = 1,number_of_nodes
      
         x_node = x_geom_node(inode)
         y_node = y_geom_node(inode)
         z_node = z_geom_node(inode)
      
         if ( (z_node >= (z_apply_topo-EPSILON_MACHINE)) ) then
      
            call compute_z(x_node,y_node,z_node,dem2_x,dem2_y,dem2_z,dem2_nx,dem2_ny,z_dem1,z_apply_topo)
      
            z_geom_node(inode) = z_node

         endif
      
      enddo

   endif
  
   call expcor(ex_file_id,x_geom_node,y_geom_node,z_geom_node,ios)

   if (ios /= 0) stop "error in cubit_topography: subroutine expcor"

   call exupda(ex_file_id,ios)

   if (ios /= 0) stop "error in cubit_topography: subroutine exupda"

   call exclos(ex_file_id,ios)

   if (ios /= 0) stop "error in cubit_topography: subroutine exclos"

!
!->write info for next iteration

   open(unit=10,file=trim(prefix)//".dem.info")
   write(10,*) z_apply_topo
   close(10)

!   
!->find min/max

   xmin      = minval(x_geom_node(:))
   xmax      = maxval(x_geom_node(:))
   ymin      = minval(y_geom_node(:))
   ymax      = maxval(y_geom_node(:))
   zmin      = minval(z_geom_node(:))
   zmax      = maxval(z_geom_node(:))
  
   write(*,*) ""
   write(*,'(a,E22.10)') "xmin = ",xmin
   write(*,'(a,E22.10)') "xmax = ",xmax
   write(*,'(a,E22.10)') "ymin = ",ymin
   write(*,'(a,E22.10)') "ymax = ",ymax
   write(*,'(a,E22.10)') "zmin = ",zmin
   write(*,'(a,E22.10)') "zmax = ",zmax

!
!->write journal file to import ex2 file

   open(unit=4,file=trim(adjustl(prefix))//"_topo.jou")
   write(unit=4,fmt='(3a)') "import mesh '",trim(adjustl(file_mesh)),"' no_geom"
!
!->make blocks
   write(unit=4,fmt='( a)') "delete nodeset all"
   write(unit=4,fmt='( a)') "delete block all"
   write(unit=4,fmt='( a)') "playback 'curve_refining.jou'"
   write(unit=4,fmt='( a)') "compress ids all"
   write(unit=4,fmt='( a)') "block 1 hex all"
   write(unit=4,fmt='( a)') "block 1 name 'l01'"
   write(unit=4,fmt='( a)') "#skin+delete here to avoid bad face in block 2"
   write(unit=4,fmt='( a)') "skin hex all make block 3"
   write(unit=4,fmt='( a)') "delete block 3"
   write(unit=4,fmt='(5(a,E15.7))') "block 2 face with x_coord >=",adjust_max(xmax)," or x_coord <=",adjust_min(xmin)," or y_coord >=",adjust_max(ymax)," or y_coord <=",adjust_min(ymin)," or z_coord <=",adjust_min(zmin)
   write(unit=4,fmt='( a)') "block 2 name 'prx'"
   write(unit=4,fmt='( a)') "skin hex all make block 3"
   write(unit=4,fmt='( a)') "block 3 name 'fsu'"
!
!->refine
!  write(unit=4,fmt='( a)') "refine face in block 3 numsplit 1 depth 2"

!
!->make blocks with refined free surface
!  write(unit=4,fmt='( a)') "delete nodeset all"
!  write(unit=4,fmt='( a)') "delete block all"
!  write(unit=4,fmt='( a)') "block 1 hex all"
!  write(unit=4,fmt='( a)') "block 1 name 'l01'"
!  write(unit=4,fmt='( a)') "#skin+delete here to avoid bad face in block 2"
!  write(unit=4,fmt='( a)') "skin hex all make block 3"
!  write(unit=4,fmt='( a)') "delete block 3"
!  write(unit=4,fmt='(5(a,E15.7))') "block 2 face with x_coord >=",adjust_max(xmax)," or x_coord <=",adjust_min(xmin)," or y_coord >=",adjust_max(ymax)," or y_coord <=",adjust_min(ymin)," or z_coord <=",adjust_min(zmin)
!  write(unit=4,fmt='( a)') "block 2 name 'prx'"
!  write(unit=4,fmt='( a)') "skin hex all make block 3"
!  write(unit=4,fmt='( a)') "block 3 name 'fsu'"
!
!->save
   write(unit=4,fmt='( a)') "quality hex all scaled jacobian global"
   write(unit=4,fmt='(3a)') "export mesh '",trim(adjustl(file_mesh)),"' block all dimension 3 overwrite"
   write(unit=4,fmt='( a)') "save as '"//trim(adjustl(prefix))//".cub' overwrite"
   write(unit=4,fmt='( a)') "list model"

   close(4)

!
!->create journal file cubit_make_block

!  open(unit=4,file="cubit_make_block.jou")
!  write(unit=4,fmt='(a)') "delete nodeset all"
!  write(unit=4,fmt='(a)') "delete block all"
!  write(unit=4,fmt='(a)') "block 1 hex all"
!  write(unit=4,fmt='(a)') "block 1 name 'l01'"
!  write(unit=4,fmt='(a)') "#skin+delete here to avoid bad face in block 2"
!  write(unit=4,fmt='(a)') "skin hex all make block 3"
!  write(unit=4,fmt='(a)') "delete block 3"
!  write(unit=4,fmt='(5(a,E15.7))') "block 2 face with x_coord >=",adjust_max(xmax)," or x_coord <=",adjust_min(xmin)," or y_coord >=",adjust_max(ymax)," or y_coord <=",adjust_min(ymin)," or z_coord <=",adjust_min(zmin)
!  write(unit=4,fmt='(a)') "block 2 name 'prx'"
!  write(unit=4,fmt='(a)') "skin hex all make block 3"
!  write(unit=4,fmt='(a)') "block 3 name 'fsu'"
!  close(4)

!->optional
  ! write(unit=4,fmt='( a)') "Graphics WindowSize 1920 1080"
  ! write(unit=4,fmt='( a)') "view reset"
  ! write(unit=4,fmt='( a)') "From 15000.000000 16000.000000 69778.140625"
  ! write(unit=4,fmt='( a)') "At 15000.000000 16000.000000 -6085.802734"
  ! write(unit=4,fmt='( a)') "Up 0.000000   1.000000   0.000000"
  ! write(unit=4,fmt='( a)') "Graphics Perspective OFF"
  ! write(unit=4,fmt='( a)') "hardcopy 'mesh_view1.jpg' jpg"
  ! write(unit=4,fmt='( a)') "Graphics Perspective ON"
  ! write(unit=4,fmt='( a)') "Graphics Perspective Angle 30"
  ! write(unit=4,fmt='( a)') "From 12478.457031 3140.137695 3006.051514"
  ! write(unit=4,fmt='( a)') "At 13638.267578 19645.050781 -471.601746"
  ! write(unit=4,fmt='( a)') "Up -0.029962   0.208100   0.977648"
  ! write(unit=4,fmt='( a)') "hardcopy 'mesh_view2.jpg' jpg"
  ! write(unit=4,fmt='( a)') "From -8104.238281 53588.078125 12034.791016"
  ! write(unit=4,fmt='( a)') "At 12456.608398 23629.798828 -2764.823730"
  ! write(unit=4,fmt='( a)') "Up 0.276230  -0.266576   0.923382"
  ! write(unit=4,fmt='( a)') "hardcopy 'mesh_view3.jpg' jpg"

   stop

end program cubit_topography
