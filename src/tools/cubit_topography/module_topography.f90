!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
module mod_topography

   use mod_precision

   implicit none

   private

   public  :: read_topography_asc
   public  :: read_topography_grd
   public  :: get_z
   public  :: compute_z
   private :: bilinear_interp
   private :: pos
   public  :: adjust_min 
   public  :: adjust_max 

   contains

!
!
!***********************************************************************************************************************************************************************************
   subroutine read_topography_asc(fname,x,y,z,dx,dy,x0,y0,nx,ny)
!***********************************************************************************************************************************************************************************
   
      implicit none
   
      character(len=255), intent(in) :: fname
      real(kind=RXP), allocatable, intent(out) :: x(:)
      real(kind=RXP), allocatable, intent(out) :: y(:)
      real(kind=RXP), allocatable, intent(out) :: z(:,:)
      real(kind=RXP)             , intent(out) :: dx
      real(kind=RXP)             , intent(out) :: dy
      real(kind=RXP)             , intent(out) :: x0
      real(kind=RXP)             , intent(out) :: y0
      integer(kind=IXP)          , intent(out) :: nx
      integer(kind=IXP)          , intent(out) :: ny
   
      real(kind=RXP)                           :: nodata
      integer(kind=IXP)                        :: ix
      integer(kind=IXP)                        :: iy
      character(len=12)              :: ctmp
   
      write(*,*) 
      write(*,'(a)') "reading topography from file "//trim(fname)
   
      open(unit=10,file=trim(adjustl(fname)))
   
      read(unit=10,fmt=*) ctmp,nx
      read(unit=10,fmt=*) ctmp,ny
      read(unit=10,fmt=*) ctmp,x0
      read(unit=10,fmt=*) ctmp,y0
      read(unit=10,fmt=*) ctmp,dx
      read(unit=10,fmt=*) ctmp,nodata
   
      dy = dx

      write(*,'(a,I0    )') "ncols     = ",nx
      write(*,'(a,I0    )') "nrows     = ",ny
      write(*,'(a,F22.10)') "xllcorner = ",x0
      write(*,'(a,F22.10)') "yllcorner = ",y0
      write(*,'(a,F22.10)') "cellsize  = ",dx
   

      write(*,'(a)') "******************************************************************************************************"
      write(*,'(a)') "WARNING: xllcorner and yllcorner flushed to zero to avoid precision problem with single precision real(kind=RXP)"
      write(*,'(a)') "******************************************************************************************************"
      x0 = 0.0
      y0 = 0.0
      write(*,'(a,F22.10)') "xllcorner = ",x0
      write(*,'(a,F22.10)') "yllcorner = ",y0

      allocate(x(nx),y(ny),z(nx,ny))
   
      do ix = 1,nx
         x(ix) = x0 + real(ix-1,kind=RXP)*dx
      enddo
   
      do iy = 1,ny
         y(iy) = y0 + real(ny-1,kind=RXP)*dy - real(iy-1,kind=RXP)*dy
      enddo
   
      do iy = 1,ny
         read(unit=10,fmt=*) (z(ix,iy),ix=1,nx)
      enddo
   
      close(10)

      write(*,*) "zmax = ",maxval(z(:,:))
      write(*,*) "zmin = ",minval(z(:,:))
   
      return
   
!***********************************************************************************************************************************************************************************
   end subroutine read_topography_asc
!***********************************************************************************************************************************************************************************


!
!
!***********************************************************************************************************************************************************************************
   subroutine read_topography_grd(fname,x,y,z,dx,dy,x0,y0,nx,ny)
!***********************************************************************************************************************************************************************************

      use mod_netcdf
   
      implicit none
   
      character(len=255)                         , intent( in) :: fname
      real(kind=RXP), allocatable, dimension(:)  , intent(out) :: x
      real(kind=RXP), allocatable, dimension(:)  , intent(out) :: y
      real(kind=RXP), allocatable, dimension(:,:), intent(out) :: z
      real(kind=RXP)                             , intent(out) :: dx
      real(kind=RXP)                             , intent(out) :: dy
      real(kind=RXP)                             , intent(out) :: x0
      real(kind=RXP)                             , intent(out) :: y0
      integer(kind=IXP)                          , intent(out) :: nx
      integer(kind=IXP)                          , intent(out) :: ny

      real(kind=R64)        , allocatable, dimension(:)        :: xd
      real(kind=R64)        , allocatable, dimension(:)        :: yd
      real(kind=R64)                                           :: x0d
      real(kind=R64)                                           :: y0d

      write(*,*) 
      write(*,'(a)') "reading topography from file "//trim(fname)

      call nf90_get_grid_dimension(fname,nx,ny)

      print *,"nx,ny = ",nx,ny

      allocate(x(nx),y(ny),z(nx,ny))
      allocate(xd(nx),yd(ny))

      call nf90_get_grid(fname,xd,yd,z,nx,ny)

      x0d = xd(1)
      y0d = yd(1)

      print *,"x0,y0 = ",x0d,y0d
   
      xd(:) = xd(:) - x0d
      yd(:) = yd(:) - y0d

      x(:) = real(xd(:),kind=IXP)
      y(:) = real(yd(:),kind=IXP)

      dx = x(2)-x(1)
      dy = y(2)-y(1)

      print *,"dx,dy = ",dx,dy

      x0 = 0.0
      y0 = 0.0

      print *,"warning: x0,y0 set to zero to avoid single precision problem"

      return
   
!***********************************************************************************************************************************************************************************
   end subroutine read_topography_grd
!***********************************************************************************************************************************************************************************



!
!
!***********************************************************************************************************************************************************************************
   real(kind=RXP) function get_z(x_node,y_node,x_topo,y_topo,z_topo,nx_topo,ny_topo) result(z)
!***********************************************************************************************************************************************************************************
   
      implicit none
   
      real(kind=RXP)   , intent(in) :: x_node
      real(kind=RXP)   , intent(in) :: y_node
      real(kind=RXP)   , intent(in) :: x_topo(nx_topo)
      real(kind=RXP)   , intent(in) :: y_topo(ny_topo)
      real(kind=RXP)   , intent(in) :: z_topo(nx_topo,ny_topo)
      integer(kind=IXP), intent(in) :: nx_topo
      integer(kind=IXP), intent(in) :: ny_topo

      integer(kind=IXP)             :: iloc
      integer(kind=IXP)             :: jloc

      call pos(x_topo,nx_topo,x_node,iloc)
 
      if ( (iloc == 0) .or. (iloc >= nx_topo) ) then
         write(*,*) "error: iloc = ",iloc
         stop
      endif
   
      call pos(y_topo,ny_topo,y_node,jloc)

      if ( (jloc == 0) .or. (jloc >= ny_topo) ) then
         write(*,*) "error: jloc = ",jloc
         stop
      endif

      call bilinear_interp(x_node,y_node,z,x_topo,y_topo,z_topo,nx_topo,ny_topo,iloc,iloc+1,jloc,jloc+1)
   
      return
   
!***********************************************************************************************************************************************************************************
   end function get_z
!***********************************************************************************************************************************************************************************

!
!
!***********************************************************************************************************************************************************************************
   subroutine compute_z(x_node,y_node,z_node,x_topo,y_topo,z_topo,nx_topo,ny_topo,z_max,z_min)
!***********************************************************************************************************************************************************************************
   
      implicit none
   
      real(kind=RXP)             , intent(in   ) :: x_node
      real(kind=RXP)             , intent(in   ) :: y_node
      real(kind=RXP)             , intent(inout) :: z_node
      integer(kind=IXP)          , intent(in   ) :: nx_topo
      integer(kind=IXP)          , intent(in   ) :: ny_topo
      real(kind=RXP)             , intent(in   ) :: x_topo(nx_topo)
      real(kind=RXP)             , intent(in   ) :: y_topo(ny_topo)
      real(kind=RXP)             , intent(in   ) :: z_topo(nx_topo,ny_topo)
      real(kind=RXP)             , intent(in   ) :: z_min
      real(kind=RXP)             , intent(in   ) :: z_max
   
      integer(kind=IXP)                          :: iloc
      integer(kind=IXP)                          :: jloc
      real(kind=RXP)                             :: z_topo_max
      real(kind=RXP)                             :: fac
   
      call pos(x_topo,nx_topo,x_node,iloc)
 
      if ( (iloc == 0) .or. (iloc >= nx_topo) ) then
         write(*,*) "error: iloc = ",iloc
         stop
      endif
   
      call pos(y_topo,ny_topo,y_node,jloc)

      if ( (jloc == 0) .or. (jloc >= ny_topo) ) then
         write(*,*) "error: jloc = ",jloc
         stop
      endif
   
      call bilinear_interp(x_node,y_node,z_topo_max,x_topo,y_topo,z_topo,nx_topo,ny_topo,iloc,iloc+1,jloc,jloc+1)

      fac = (z_topo_max - z_min)/(z_max - z_min) !slope of linear interpolation
   
      z_node = z_min + fac*(z_node-z_min) !min + delta_z * slope
   
      return
   
!***********************************************************************************************************************************************************************************
   end subroutine compute_z
!***********************************************************************************************************************************************************************************


!
!
!***********************************************************************************************************************************************************************************
   subroutine bilinear_interp(xn,yn,z,x,y,array,nx,ny,jx,jxp1,jy,jyp1)
!***********************************************************************************************************************************************************************************
   
      implicit none
   
      real(kind=RXP)   , intent( in)      :: xn
      real(kind=RXP)   , intent( in)      :: yn
      real(kind=RXP)   , intent(out)      :: z
   
      real(kind=RXP)   , dimension(nx)    :: x
      real(kind=RXP)   , dimension(ny)    :: y
      real(kind=RXP)   , dimension(nx,ny) :: array
   
      integer(kind=IXP), intent( in)      :: nx
      integer(kind=IXP), intent( in)      :: ny
   
      integer(kind=IXP), intent( in)      :: jx
      integer(kind=IXP), intent( in)      :: jxp1
      integer(kind=IXP), intent( in)      :: jy
      integer(kind=IXP), intent( in)      :: jyp1
                                
      real(kind=RXP)                      :: s1
      real(kind=RXP)                      :: s2
      real(kind=RXP)                      :: s3
      real(kind=RXP)                      :: s4
      real(kind=RXP)                      :: zt
      real(kind=RXP)                      :: zta
      real(kind=RXP)                      :: ztb
      real(kind=RXP)                      :: zu
      real(kind=RXP)                      :: zua
      real(kind=RXP)                      :: zub
   !
   !--------------------------------------------------------------------
   !
      s1 = array(jx  ,jy  )
      s2 = array(jxp1,jy  )
      s3 = array(jxp1,jyp1)
      s4 = array(jx  ,jyp1)
   !
   !  find slopes used in interpolation
   !  i) x.
      zta = xn - x(jx)
      ztb = x(jxp1) - x(jx)
      zt  = zta/ztb
   !
   !  ii) y.
      zua = yn - y(jy)
      zub = y(jyp1) - y(jy)
      zu  = zua/zub
   !
   !  use bilinear interpolation
      z = (1.0-zt)*(1.0-zu)*s1 + zt*(1.0-zu)*s2 + zt*zu*s3 + (1.0-zt)*zu*s4
   
      return
   
!***********************************************************************************************************************************************************************************
   end subroutine bilinear_interp
!***********************************************************************************************************************************************************************************

!
!   
!***********************************************************************************************************************************************************************************
   subroutine pos(xx,n,x,j)
!***********************************************************************************************************************************************************************************
   
      implicit none
   
      integer(kind=IXP), intent( in) :: n
      real(kind=RXP)   , intent( in) :: x
      real(kind=RXP)   , intent( in) :: xx(n)
      integer(kind=IXP), intent(out) :: j
   
      integer(kind=IXP) :: jl
      integer(kind=IXP) :: jm
      integer(kind=IXP) :: ju
   !
   !  initialize upper & lower limits.
      jl = 0
      ju = n + 1
   
      do while ( .true. )
   
         if ( .not..true. ) then
            return
   
         elseif ( ju-jl>1 ) then
   
            jm = (ju+jl)/2
            if ( xx(n)>xx(1) .eqv. x>xx(jm) ) then
               jl = jm
            else
               ju = jm
            endif
   !
   !        repeat untill the test condition is satisfied.
            cycle
         endif
   !
   !     set the output.
   !FDM
         if (jl >= n) jl = n - 1
         if (jl <= 0) jl = 1
   !FDM
         j = jl
         exit
   
      enddo
   
!***********************************************************************************************************************************************************************************
   end subroutine pos
!***********************************************************************************************************************************************************************************

!
!
!***********************************************************************************************************************************
real(kind=RXP) function adjust_min(x) result(f)
!***********************************************************************************************************************************

   implicit none

   real(kind=RXP), intent(in) :: x

   real(kind=RXP), parameter  :: EPSILON_MACHINE = epsilon(EPSILON_MACHINE)

   f = x + abs(x)*0.0001 + EPSILON_MACHINE

   return

!***********************************************************************************************************************************
end function adjust_min
!***********************************************************************************************************************************

!
!
!***********************************************************************************************************************************
real(kind=RXP) function adjust_max(x) result(f)
!***********************************************************************************************************************************

   implicit none

   real(kind=RXP), intent(in) :: x

   real(kind=RXP), parameter  :: EPSILON_MACHINE = epsilon(EPSILON_MACHINE)

   f = x - abs(x)*0.0001 - EPSILON_MACHINE

   return

!***********************************************************************************************************************************
end function adjust_max
!***********************************************************************************************************************************

end module mod_topography
