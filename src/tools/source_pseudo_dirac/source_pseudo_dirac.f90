!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
module mod_read_command_line

   use mod_precision

   implicit none

   private

   public get_option

   interface get_option

      module procedure get_option_1_val
      module procedure get_option_2_val

   end interface

   contains

!
!
!>@brief subroutine to search filter parameters inside command line
!>@param   l   : line
!>@param   k   : key to search in line
!>@return  lo  : logical value, true if key is found
!>@return  idc : ideal cutoff frequency
!>@return  trb : transition band
!***********************************************************************************************************************************************************************************
   subroutine get_option_2_val(l,k,lo,idc,trb)
!***********************************************************************************************************************************************************************************

      implicit none

      character(len=*) , intent(in ) :: l
      character(len=*) , intent(in ) :: k
      logical(kind=IXP), intent(out) :: lo
      real   (kind=RXP), intent(out) :: idc
      real   (kind=RXP), intent(out) :: trb

      character(len=:), allocatable :: v
      integer(kind=IXP)             :: p
      integer(kind=IXP)             :: ll

      p = index(l,k)

      if (p >= 2_IXP) then

         lo = .true.

         ll = len(trim(l))

         v = trim(l)

         v = l(p+len(k):ll)

         read(v,*) idc,trb

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine 
!***********************************************************************************************************************************************************************************

!
!
!>@brief subroutine to search time shift parameter inside command line
!>@param   l   : line
!>@param   k   : key to search in line
!>@return  lo  : logical value, true if key is found
!>@return  x   : time shiff
!***********************************************************************************************************************************************************************************
   subroutine get_option_1_val(l,k,lo,x)
!***********************************************************************************************************************************************************************************

      implicit none

      character(len=*) , intent(in ) :: l
      character(len=*) , intent(in ) :: k
      logical(kind=IXP), intent(out) :: lo
      real   (kind=RXP), intent(out) :: x

      character(len=:), allocatable :: v
      integer(kind=IXP)             :: p
      integer(kind=IXP)             :: ll

      p = index(l,k)

      if (p >= 2_IXP) then

         lo = .true.

         ll = len(trim(l))

         v = trim(l)

         v = l(p+len(k):ll)

         read(v,*) x

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine
!***********************************************************************************************************************************************************************************

end module

!***********************************************************************************************************************************************************************************
program source_pseudo_dirac
!***********************************************************************************************************************************************************************************

   use mod_precision

   use mod_init_memory

   use mod_filter

   use mod_signal_processing

   use mod_read_command_line

   implicit none


   real   (kind=RXP), dimension(:), allocatable :: x
   real   (kind=RXP), dimension(:), allocatable :: y
   real   (kind=RXP), dimension(:), allocatable :: y_dt
   real   (kind=RXP), dimension(:), allocatable :: y_ddt
                                                 
   complex(kind=RXP), dimension(:), allocatable :: x_fft
   complex(kind=RXP), dimension(:), allocatable :: y_fft
   complex(kind=RXP), dimension(:), allocatable :: y_dt_fft
   complex(kind=RXP), dimension(:), allocatable :: y_ddt_fft

   real   (kind=R64), dimension(:), allocatable   :: butter_coeff_lp

   real   (kind=R64)                              :: butter_gain_lp
   integer(kind=IXP)                              :: butter_order_lp
   integer(kind=IXP)                              :: butter_func_order
   real   (kind=RXP)                              :: ideal_cutoff
   real   (kind=RXP)                              :: transition_band
   real   (kind=RXP)                              :: freq_pass
   real   (kind=RXP)                              :: freq_stop
   real   (kind=RXP)                              :: ap
   real   (kind=RXP)                              :: as
   real   (kind=RXP)                              :: t0

   real   (kind=RXP)                              :: dt
   real   (kind=RXP)                              :: td
   real   (kind=RXP)                              :: df
                                                  
   integer(kind=IXP)                              :: i
   integer(kind=IXP)                              :: nt
   integer(kind=IXP)                              :: it0
   integer(kind=IXP)                              :: nfft
   integer(kind=IXP)                              :: nfold
   integer(kind=IXP)                              :: ios
   integer(kind=IXP)                              :: u
   integer(kind=IXP)                              :: narg
                                                  
   character(len=50)                              :: suffix
   character(len=255)                             :: pname
   character(len=255)                             :: command_line

   logical(kind=IXP)                              :: is_time
   logical(kind=IXP)                              :: is_filter_lp

!
!
!**************************************************************************************************************************
!->Get command line argument
!**************************************************************************************************************************

   call get_command_argument(0,pname)

   narg = command_argument_count()

   if ( narg <= 1_IXP ) then

      write(*,'(a)') "Usage : "//trim(adjustl(pname))//" [lp cutoff band] [t total_duration time_step]"

      stop

   endif

   call get_command(command_line)

   command_line = command_line(len_trim(pname)+1_IXP:len_trim(command_line)) !remove program name from command line

   call get_option(command_line,'t',is_time,td,dt)

   call get_option(command_line,'lp',is_filter_lp,ideal_cutoff,transition_band)

   if (.not.is_time     ) stop 'error: time not defined'
   if (.not.is_filter_lp) stop 'error: filter not defined'

!
!->define time info

   nt  = int(td/dt) + ONE_IXP
   t0  = dt
   it0 = int(t0/dt) + ONE_IXP

!
!->memory allocation

   ios = init_array_real(x    ,nt,"source_pseudo_dirac:x")
   ios = init_array_real(y    ,nt,"source_pseudo_dirac:y")
   ios = init_array_real(y_dt ,nt,"source_pseudo_dirac:y_dt")
   ios = init_array_real(y_ddt,nt,"source_pseudo_dirac:y_ddt")

!
!->compute dirac

   x(it0)   = +1.0_RXP/dt

!
!->FFT dirac

   nfft  = nextpow2(nt,2_IXP**17_IXP)

   nfold = nfft/TWO_IXP + ONE_IXP

   df    = 1.0_RXP/(real(nfft,kind=RXP)*dt)

   print *,nt,t0,it0,nfft,df

   ios = init_array_complex(x_fft,nfft,"source_pseudo_dirac:x_fft")

   x_fft(1_IXP:nt) = x(1_IXP:nt)

   call ft(x_fft,-1_IXP)

!
!->write dirac in time domain

   open(10,file='dirac.time.dat')

   do i = ONE_IXP,nt

      write(10,'(2(E15.7,1X))') real(i-ONE_IXP,kind=RXP)*dt,x(i)

   enddo

   close(10)

!
!->write dirac in freq domain

   open(10,file='dirac.freq.dat')

   do i = ONE_IXP,nfold

      write(10,'(2(E15.7,1X))') real(i-ONE_IXP,kind=RXP)*df,abs(x_fft(i))

   enddo

   close(10)

!!
!!->sanity check
!
!   call ft(x_fft,+1_IXP)
!
!    x_fft(:) = x_fft(:)/real(nfft,kind=RXP)
!
!   open(10,file='dirac.time2.dat')
!
!   do i = ONE_IXP,nt
!
!      write(10,'(2(E15.7,1X))') real(i-ONE_IXP,kind=RXP)*dt,real(x_fft(i))
!
!   enddo
!
!   close(10)

!
!->highpass butterworth filter properties

   freq_pass       =  ideal_cutoff - transition_band/TWO_RXP
   freq_stop       =  ideal_cutoff + transition_band/TWO_RXP
   ap              =  0.01_RXP
   as              = 100.0_RXP

!
!->design highpass butterworth filter

   call butterworth_lowpass(butter_coeff_lp,butter_order_lp,butter_gain_lp,butter_func_order,freq_pass,freq_stop,dt,ap,as)

   open(newunit=u,file="dirac.info",action="write")

   write(u,*) "total duration            = ",td
   write(u,*) "time step                 = ",dt
   write(u,*) "filter order              = ",butter_order_lp
   write(u,*) "filter Low  Freq Cutoff   = ",freq_pass
   write(u,*) "filter High Freq Cutoff   = ",freq_stop
   write(u,*) "filter transition band    = ",transition_band

   close(u)

!
!->apply filter

   call tandem_one_pass(x,nt,butter_coeff_lp,butter_order_lp,butter_gain_lp)

   y(:) = x(:)

!
!->compute time derivative in frequency domain

   y_dt(:) = y(:)

   call ft_diff(y_dt,dt)

   y_ddt(:) = y_dt(:)

   call ft_diff(y_ddt,dt)


!
!->write filtered dirac

   suffix=".butlow.1pass.dat"

   open(10,file='dirac.filter.time'//trim(suffix))

   do i = ONE_IXP,nt

      write(10,'(4(E15.7,1X))') real(i-ONE_IXP,kind=RXP)*dt,y(i),y_dt(i),y_ddt(i)

   enddo

   close(10)

!
!->compute nfft
   
   print *,nfft,df

   ios = init_array_complex(y_fft    ,nfft,"source_pseudo_dirac:y_fft")

   ios = init_array_complex(y_dt_fft ,nfft,"source_pseudo_dirac:y_dt_fft")

   ios = init_array_complex(y_ddt_fft,nfft,"source_pseudo_dirac:y_ddt_fft")


!
!->compute FFT

   y_fft    (ONE_IXP:nt) = y    (ONE_IXP:nt)
   y_dt_fft (ONE_IXP:nt) = y_dt (ONE_IXP:nt)
   y_ddt_fft(ONE_IXP:nt) = y_ddt(ONE_IXP:nt)

   call ft(y_fft    ,-ONE_IXP) 
   call ft(y_dt_fft ,-ONE_IXP) 
   call ft(y_ddt_fft,-ONE_IXP) 

   y_fft    (:) = y_fft    (:)*dt
   y_dt_fft (:) = y_dt_fft (:)*dt
   y_ddt_fft(:) = y_ddt_fft(:)*dt


!
!->write FFT amplitude

   open(10,file='dirac.filter.freq'//trim(suffix))

   do i = ONE_IXP,nfold

      write(10,'(4(E15.7,1X))') real(i-ONE_IXP,kind=RXP)*df,abs(y_fft(i)),abs(y_dt_fft(i)),abs(y_ddt_fft(i))

   enddo

   close(10)

   stop

end program
