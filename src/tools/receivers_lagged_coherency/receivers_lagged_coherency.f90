!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
program receivers_lagged_coherency

      use mod_precision
     
      use mod_init_memory
     
      use mod_signal_processing
     
      implicit none
     
      integer(kind=IXP), parameter                   :: BUFFER_SIZE = 10_IXP
      
      real   (kind=RXP), dimension(BUFFER_SIZE)      :: buffer
      real   (kind=R32), dimension(:)  , allocatable :: buffer_irec

      real   (kind=RXP), dimension(:)  , allocatable :: psd_irec_x
      real   (kind=RXP), dimension(:)  , allocatable :: psd_irec_y
      real   (kind=RXP), dimension(:)  , allocatable :: psd_irec_z

      real   (kind=RXP), dimension(:)  , allocatable :: psd_irec_x_avg
      real   (kind=RXP), dimension(:)  , allocatable :: psd_irec_y_avg
      real   (kind=RXP), dimension(:)  , allocatable :: psd_irec_z_avg

      real   (kind=RXP), dimension(:,:), allocatable :: psd_irec_x_avg_all_simu
      real   (kind=RXP), dimension(:,:), allocatable :: psd_irec_y_avg_all_simu
      real   (kind=RXP), dimension(:,:), allocatable :: psd_irec_z_avg_all_simu

      real   (kind=RXP), dimension(:)  , allocatable :: psd_jrec_x
      real   (kind=RXP), dimension(:)  , allocatable :: psd_jrec_y
      real   (kind=RXP), dimension(:)  , allocatable :: psd_jrec_z

      real   (kind=RXP), dimension(:)  , allocatable :: psd_jrec_x_avg
      real   (kind=RXP), dimension(:)  , allocatable :: psd_jrec_y_avg
      real   (kind=RXP), dimension(:)  , allocatable :: psd_jrec_z_avg

      real   (kind=RXP), dimension(:,:), allocatable :: psd_jrec_x_avg_all_simu
      real   (kind=RXP), dimension(:,:), allocatable :: psd_jrec_y_avg_all_simu
      real   (kind=RXP), dimension(:,:), allocatable :: psd_jrec_z_avg_all_simu

      complex(kind=RXP), dimension(:)  , allocatable :: csd_ijrec_x
      complex(kind=RXP), dimension(:)  , allocatable :: csd_ijrec_y
      complex(kind=RXP), dimension(:)  , allocatable :: csd_ijrec_z

      complex(kind=RXP), dimension(:)  , allocatable :: csd_ijrec_x_avg
      complex(kind=RXP), dimension(:)  , allocatable :: csd_ijrec_y_avg
      complex(kind=RXP), dimension(:)  , allocatable :: csd_ijrec_z_avg

      complex(kind=RXP), dimension(:,:), allocatable :: csd_ijrec_x_avg_all_simu
      complex(kind=RXP), dimension(:,:), allocatable :: csd_ijrec_y_avg_all_simu
      complex(kind=RXP), dimension(:,:), allocatable :: csd_ijrec_z_avg_all_simu

      complex(kind=RXP), dimension(:)  , allocatable :: gam_ijrec_x
      complex(kind=RXP), dimension(:)  , allocatable :: gam_ijrec_y
      complex(kind=RXP), dimension(:)  , allocatable :: gam_ijrec_z

      complex(kind=RXP), dimension(:,:), allocatable :: ft_rec_x
      complex(kind=RXP), dimension(:,:), allocatable :: ft_rec_y
      complex(kind=RXP), dimension(:,:), allocatable :: ft_rec_z

      real   (kind=RXP), dimension(:)  , allocatable :: xrec
      real   (kind=RXP), dimension(:)  , allocatable :: yrec
      real   (kind=RXP)                              :: df 
      real   (kind=RXP)                              :: dt  
      real   (kind=RXP)                              :: dt1 
      real   (kind=RXP)                              :: dt2 
      real   (kind=RXP)                              :: xd 
                                                     
      integer(kind=IXP)                              :: ndt
      integer(kind=IXP)                              :: d
      integer(kind=IXP)                              :: i
      integer(kind=IXP)                              :: idt
      integer(kind=IXP)                              :: irec
      integer(kind=IXP)                              :: irec_start
      integer(kind=IXP)                              :: irec_end
      integer(kind=IXP)                              :: nrec_d
      integer(kind=IXP)                              :: jrec
      integer(kind=IXP)                              :: nrec
      integer(kind=IXP)                              :: nrec_all
      integer(kind=IXP)                              :: sloop
      integer(kind=IXP), dimension(:), allocatable   :: sloop_all_simu
      integer(kind=IXP)                              :: nfft
      integer(kind=IXP)                              :: nfold
      integer(kind=IXP)                              :: myunit
      integer(kind=IXP)                              :: myunit_dir
      integer(kind=IXP)                              :: narg
      integer(kind=IXP)                              :: idir
      integer(kind=IXP)                              :: ndir
      integer(kind=IXP)                              :: ios_dir
      integer(kind=IXP)                              :: ios
                                                     
      character(len=  1)                             :: c1
      character(len=  6)                             :: cd
      character(len=  6)                             :: cirec
      character(len=  6)                             :: cjrec
      character(len=  6)                             :: c_rec_start
      character(len=  6)                             :: c_rec_end
      character(len=  6)                             :: c_rec_d
      character(len= 10)                             :: cdir
      character(len=100)                             :: prefix
      character(len=100)                             :: suffix

      logical(kind=IXP)                              :: is_multiple_dir
     
      suffix=".gpl.saitofilter"
     
!   
!********************************************************************************************************************************
!---->get argument from command line
!********************************************************************************************************************************
      narg = command_argument_count()

      if (narg /= 3_IXP) then

         write(*,*) "Usage: first_receiver last_receiver max_number_of_intervals_between_receiver"
         stop

      else

         call get_command_argument(1_IXP,c_rec_start)
         call get_command_argument(2_IXP,c_rec_end)
         call get_command_argument(3_IXP,c_rec_d)

         read(c_rec_start,*) irec_start
         read(c_rec_end  ,*) irec_end
         read(c_rec_d    ,*) nrec_d

         nrec = irec_end - irec_start + 1_IXP

      endif

!   
!********************************************************************************************************************************
!---->get prefix
!********************************************************************************************************************************
      open(unit=10,file='prefix',action='read',status='old')
     
      read(10,*) prefix
     
      close(10)
     
!   
!********************************************************************************************************************************
!---->count number of receivers
!********************************************************************************************************************************
      open(unit=10,file=trim(prefix)//".fsr",action='read',status='old')
     
      nrec_all = 0_IXP
     
      do
 
         read(unit=10,fmt=*,iostat=ios) c1
         if (ios /= 0_IXP) exit
         nrec_all = nrec_all + 1_IXP

      enddo
     
      rewind(10)
     
      write(*,'(A,I6)') 'Total number of receiver found = ',nrec_all
     
!   
!********************************************************************************************************************************
!---->read x,y coordinates of receivers
!********************************************************************************************************************************
     
      ios = init_array_real(xrec,nrec_all,"receivers_lagged_coherency:xrec")
      ios = init_array_real(yrec,nrec_all,"receivers_lagged_coherency:yrec")
     
      do irec = 1_IXP,nrec_all
     
         read(unit=10,fmt=*) xrec(irec),yrec(irec)
     
      enddo
     
      close(10)

!
!---->compute and write distances between receivers to file
      open(newunit=myunit,file=trim(prefix)//".coherency.d.asc",action="write")

      irec = irec_start

      do d = 1_IXP,nrec_d

         jrec = irec + d

         xd = sqrt( (xrec(irec)-xrec(jrec))**2_IXP + (yrec(irec)-yrec(jrec))**2_IXP)

         write(myunit,'(I0)') int(xd,kind=IXP)

      enddo

!   
!********************************************************************************************************************************
!---->search directory file if any
!********************************************************************************************************************************
      open(newunit=myunit_dir,file='coherency.dir',action="read",iostat=ios_dir)

      if (ios_dir /= 0) then

         ndir = 1_IXP

         cdir = "./"

         is_multiple_dir = .false.

      else

         ndir = 0_IXP

         is_multiple_dir = .true.

         do 

            read(unit=myunit_dir,fmt='(a)',iostat=ios) c1

            if (ios /= 0_IXP) exit

            ndir = ndir + 1_IXP
         
         enddo

         rewind(myunit_dir)

      endif

      write(*,*) "Number of directories = ",ndir

      ios = init_array_int(sloop_all_simu,nrec_d,"receivers_lagged_coherency:sloop_all_simu")

      do idir = 1_IXP,ndir

         if (is_multiple_dir) then

            read(myunit_dir,'(A)') cdir
   
         endif
    
         write(*,*) "working in directory "//trim(cdir)

!           
!         
!***********************************************************************************************************************************
!------->Get size and number of time step from receiver1
!***********************************************************************************************************************************
         open(unit=10,file=trim(cdir)//trim(prefix)//".fsr.000001"//trim(suffix),status='old',form='unformatted',action='read',access='stream',iostat=ios)
        
         if (ios /= ZERO_IXP) then

            write(*,*) "error while opening ",trim(cdir)//trim(prefix)//".fsr.000001"//trim(suffix)
            stop

         endif
        
         ndt = ZERO_IXP
        
         do
        
            read(unit=10,iostat=ios) (buffer(i),i=ONE_IXP,BUFFER_SIZE)
        
            if (ios /= ZERO_IXP) exit
         
            ndt = ndt + ONE_IXP
         
            if (ndt == ONE_IXP) dt1 = buffer(ONE_IXP)
            if (ndt == TWO_IXP) dt2 = buffer(ONE_IXP)
         
         enddo
        
         close(10)
         
         dt = dt2-dt1
        
         write(*,*) 'ndt = ',ndt
         write(*,*) ' dt = ', dt
     
!           
!         
!***********************************************************************************************************************************
!------->Prepare FFT and power spectrum array
!***********************************************************************************************************************************
     
         nfft = nextpow2(ndt,8192_IXP)
     
         print *,"nfft = ",nfft
     
         nfold = nfft/2_IXP + 1_IXP
     
         df = 1.0_RXP/(dt*real(nfft-1_IXP,kind=RXP))
     
         ios = init_array_complex(ft_rec_x,nrec,nfft,"receivers_lagged_coherency:ft_rec_x")
         ios = init_array_complex(ft_rec_y,nrec,nfft,"receivers_lagged_coherency:ft_rec_y")
         ios = init_array_complex(ft_rec_z,nrec,nfft,"receivers_lagged_coherency:ft_rec_z")
     
         ios = init_array_real(psd_irec_x,nfold,"receivers_lagged_coherency:ps_irec_x")
         ios = init_array_real(psd_irec_y,nfold,"receivers_lagged_coherency:ps_irec_y")
         ios = init_array_real(psd_irec_z,nfold,"receivers_lagged_coherency:ps_irec_z")
     
         ios = init_array_real(psd_jrec_x,nfold,"receivers_lagged_coherency:ps_jrec_x")
         ios = init_array_real(psd_jrec_y,nfold,"receivers_lagged_coherency:ps_jrec_y")
         ios = init_array_real(psd_jrec_z,nfold,"receivers_lagged_coherency:ps_jrec_z")
     
         ios = init_array_complex(csd_ijrec_x,nfold,"receivers_lagged_coherency:csd_ijrec_x")
         ios = init_array_complex(csd_ijrec_y,nfold,"receivers_lagged_coherency:csd_ijrec_y")
         ios = init_array_complex(csd_ijrec_z,nfold,"receivers_lagged_coherency:csd_ijrec_z")
     
         ios = init_array_real(psd_irec_x_avg,nfold,"receivers_lagged_coherency:psd_irec_x_avg")
         ios = init_array_real(psd_irec_y_avg,nfold,"receivers_lagged_coherency:psd_irec_y_avg")
         ios = init_array_real(psd_irec_z_avg,nfold,"receivers_lagged_coherency:psd_irec_z_avg")
     
         ios = init_array_real(psd_jrec_x_avg,nfold,"receivers_lagged_coherency:psd_jrec_x_avg")
         ios = init_array_real(psd_jrec_y_avg,nfold,"receivers_lagged_coherency:psd_jrec_y_avg")
         ios = init_array_real(psd_jrec_z_avg,nfold,"receivers_lagged_coherency:psd_jrec_z_avg")
     
         ios = init_array_complex(csd_ijrec_x_avg,nfold,"receivers_lagged_coherency:csd_ijrec_x_avg")
         ios = init_array_complex(csd_ijrec_y_avg,nfold,"receivers_lagged_coherency:csd_ijrec_y_avg")
         ios = init_array_complex(csd_ijrec_z_avg,nfold,"receivers_lagged_coherency:csd_ijrec_z_avg")

         ios = init_array_complex(gam_ijrec_x,nfold,"receivers_lagged_coherency:gam_ijrec_x")
         ios = init_array_complex(gam_ijrec_y,nfold,"receivers_lagged_coherency:gam_ijrec_y")
         ios = init_array_complex(gam_ijrec_z,nfold,"receivers_lagged_coherency:gam_ijrec_z")
    
         !allocate once and for all
         if (.not.allocated(psd_irec_x_avg_all_simu)) ios = init_array_real(psd_irec_x_avg_all_simu,nrec_d,nfold,"receivers_lagged_coherency:psd_irec_x_avg_all_simu")
         if (.not.allocated(psd_irec_y_avg_all_simu)) ios = init_array_real(psd_irec_y_avg_all_simu,nrec_d,nfold,"receivers_lagged_coherency:psd_irec_y_avg_all_simu")
         if (.not.allocated(psd_irec_z_avg_all_simu)) ios = init_array_real(psd_irec_z_avg_all_simu,nrec_d,nfold,"receivers_lagged_coherency:psd_irec_z_avg_all_simu")
     
         if (.not.allocated(psd_jrec_x_avg_all_simu)) ios = init_array_real(psd_jrec_x_avg_all_simu,nrec_d,nfold,"receivers_lagged_coherency:psd_jrec_x_avg_all_simu")
         if (.not.allocated(psd_jrec_y_avg_all_simu)) ios = init_array_real(psd_jrec_y_avg_all_simu,nrec_d,nfold,"receivers_lagged_coherency:psd_jrec_y_avg_all_simu")
         if (.not.allocated(psd_jrec_z_avg_all_simu)) ios = init_array_real(psd_jrec_z_avg_all_simu,nrec_d,nfold,"receivers_lagged_coherency:psd_jrec_z_avg_all_simu")
     
         if (.not.allocated(csd_ijrec_x_avg_all_simu)) ios = init_array_complex(csd_ijrec_x_avg_all_simu,nrec_d,nfold,"receivers_lagged_coherency:csd_ijrec_x_avg_all_simu")
         if (.not.allocated(csd_ijrec_y_avg_all_simu)) ios = init_array_complex(csd_ijrec_y_avg_all_simu,nrec_d,nfold,"receivers_lagged_coherency:csd_ijrec_y_avg_all_simu")
         if (.not.allocated(csd_ijrec_z_avg_all_simu)) ios = init_array_complex(csd_ijrec_z_avg_all_simu,nrec_d,nfold,"receivers_lagged_coherency:csd_ijrec_z_avg_all_simu")
 
!   
!      
!***********************************************************************************************************************************
!------->compute and store power spectrum of each receiver
!***********************************************************************************************************************************
     
         ios = init_array_real(buffer_irec,BUFFER_SIZE*ndt,"receivers_lagged_coherency:buffer_irec")
     
         do irec = 1_IXP,nrec
     
           !write(*,*) "Reading and processing receiver ",irec
     
            write(cirec,'(I6.6)') irec + irec_start - 1_IXP
     
            open(unit=11,file=trim(cdir)//trim(prefix)//".fsr."//trim(cirec)//trim(suffix),form='unformatted',access='stream',action='read',status='old',iostat=ios)
     
            read(unit=11,iostat=ios) (buffer_irec(i),i=ONE_IXP,BUFFER_SIZE*ndt)
     
            close(11)
     
            do idt = 1_IXP,ndt
     
               ft_rec_x(idt,irec) = real(buffer_irec((idt-ONE_IXP)*BUFFER_SIZE+5_IXP),kind=RXP)
     
            enddo
     
            do idt = 1_IXP,ndt
     
               ft_rec_y(idt,irec) = real(buffer_irec((idt-ONE_IXP)*BUFFER_SIZE+6_IXP),kind=RXP)
     
            enddo
     
            do idt = 1_IXP,ndt
     
               ft_rec_z(idt,irec) = real(buffer_irec((idt-ONE_IXP)*BUFFER_SIZE+7_IXP),kind=RXP)
     
            enddo
     
            call ft(ft_rec_x(:,irec),-1_IXP)
            call ft(ft_rec_y(:,irec),-1_IXP)
            call ft(ft_rec_z(:,irec),-1_IXP)
     
            ft_rec_x(:,irec) = ft_rec_x(:,irec)*dt
            ft_rec_y(:,irec) = ft_rec_y(:,irec)*dt
            ft_rec_z(:,irec) = ft_rec_z(:,irec)*dt
     
         enddo
     
!   
!      
!***********************************************************************************************************************************
!------->compute lagged coherency for each possible pair of receivers
!***********************************************************************************************************************************
     
         do d = 1_IXP,nrec_d
     
            psd_irec_x_avg(:) = ZERO_RXP
            psd_irec_y_avg(:) = ZERO_RXP
            psd_irec_z_avg(:) = ZERO_RXP
        
            psd_jrec_x_avg(:) = ZERO_RXP
            psd_jrec_y_avg(:) = ZERO_RXP
            psd_jrec_z_avg(:) = ZERO_RXP
       
            csd_ijrec_x_avg(:) = cmplx(ZERO_RXP,ZERO_RXP)
            csd_ijrec_y_avg(:) = cmplx(ZERO_RXP,ZERO_RXP)
            csd_ijrec_z_avg(:) = cmplx(ZERO_RXP,ZERO_RXP)
    
            sloop = 0_IXP
     
            irec  = irec_start
     
            jrec  = irec + d
     
            write(cd,'(I6.6)') d
        
            do while (jrec <= irec_end)
     
               sloop             = sloop             + 1_IXP
               sloop_all_simu(d) = sloop_all_simu(d) + 1_IXP
        
               write(cjrec,'(I6.6)') jrec
     
               write(cirec,'(I6.6)') irec
     
               print '(5I6)',irec,jrec,sloop,irec-irec_start+1_IXP,jrec-irec_start+1_IXP
     
!   
!------------->compute PSD and CSD
     
               call psd(ft_rec_x(:,irec-irec_start+1_IXP),dt,ndt,psd_irec_x)
               call psd(ft_rec_y(:,irec-irec_start+1_IXP),dt,ndt,psd_irec_y)
               call psd(ft_rec_z(:,irec-irec_start+1_IXP),dt,ndt,psd_irec_z)
        
               call psd(ft_rec_x(:,jrec-irec_start+1_IXP),dt,ndt,psd_jrec_x)
               call psd(ft_rec_y(:,jrec-irec_start+1_IXP),dt,ndt,psd_jrec_y)
               call psd(ft_rec_z(:,jrec-irec_start+1_IXP),dt,ndt,psd_jrec_z)
     
               call csd(ft_rec_x(:,irec-irec_start+1_IXP),ft_rec_x(:,jrec-irec_start+1_IXP),dt,ndt,csd_ijrec_x)
               call csd(ft_rec_y(:,irec-irec_start+1_IXP),ft_rec_y(:,jrec-irec_start+1_IXP),dt,ndt,csd_ijrec_y)
               call csd(ft_rec_z(:,irec-irec_start+1_IXP),ft_rec_z(:,jrec-irec_start+1_IXP),dt,ndt,csd_ijrec_z)
     
!   
!------------->sum PSD and CSD one SIMU
               psd_irec_x_avg(:) = psd_irec_x_avg(:) + psd_irec_x(:)
               psd_irec_y_avg(:) = psd_irec_y_avg(:) + psd_irec_y(:)
               psd_irec_z_avg(:) = psd_irec_z_avg(:) + psd_irec_z(:)
        
               psd_jrec_x_avg(:) = psd_jrec_x_avg(:) + psd_jrec_x(:)
               psd_jrec_y_avg(:) = psd_jrec_y_avg(:) + psd_jrec_y(:)
               psd_jrec_z_avg(:) = psd_jrec_z_avg(:) + psd_jrec_z(:)
        
               csd_ijrec_x_avg(:) = csd_ijrec_x_avg(:) + csd_ijrec_x(:)
               csd_ijrec_y_avg(:) = csd_ijrec_y_avg(:) + csd_ijrec_y(:)
               csd_ijrec_z_avg(:) = csd_ijrec_z_avg(:) + csd_ijrec_z(:)
        
               irec = irec + 1_IXP 
     
               jrec = irec + d
               
            enddo

!   
!---------->sum PSD and CSD all SIMU
            psd_irec_x_avg_all_simu(:,d)  = psd_irec_x_avg_all_simu(:,d)  + psd_irec_x_avg(:) 
            psd_irec_y_avg_all_simu(:,d)  = psd_irec_y_avg_all_simu(:,d)  + psd_irec_y_avg(:) 
            psd_irec_z_avg_all_simu(:,d)  = psd_irec_z_avg_all_simu(:,d)  + psd_irec_z_avg(:) 
                                                                      
            psd_jrec_x_avg_all_simu(:,d)  = psd_jrec_x_avg_all_simu(:,d)  + psd_jrec_x_avg(:) 
            psd_jrec_y_avg_all_simu(:,d)  = psd_jrec_y_avg_all_simu(:,d)  + psd_jrec_y_avg(:) 
            psd_jrec_z_avg_all_simu(:,d)  = psd_jrec_z_avg_all_simu(:,d)  + psd_jrec_z_avg(:) 
                                                                      
            csd_ijrec_x_avg_all_simu(:,d) = csd_ijrec_x_avg_all_simu(:,d) + csd_ijrec_x_avg(:) 
            csd_ijrec_y_avg_all_simu(:,d) = csd_ijrec_y_avg_all_simu(:,d) + csd_ijrec_y_avg(:) 
            csd_ijrec_z_avg_all_simu(:,d) = csd_ijrec_z_avg_all_simu(:,d) + csd_ijrec_z_avg(:) 

!
!---------->average one SIMU
            psd_irec_x_avg(:) = psd_irec_x_avg(:)/real(sloop,kind=RXP)
            psd_irec_y_avg(:) = psd_irec_y_avg(:)/real(sloop,kind=RXP)
            psd_irec_z_avg(:) = psd_irec_z_avg(:)/real(sloop,kind=RXP)
     
            psd_jrec_x_avg(:) = psd_jrec_x_avg(:)/real(sloop,kind=RXP)
            psd_jrec_y_avg(:) = psd_jrec_y_avg(:)/real(sloop,kind=RXP)
            psd_jrec_z_avg(:) = psd_jrec_z_avg(:)/real(sloop,kind=RXP)
     
            csd_ijrec_x_avg(:) = csd_ijrec_x_avg(:)/real(sloop,kind=RXP)
            csd_ijrec_y_avg(:) = csd_ijrec_y_avg(:)/real(sloop,kind=RXP)
            csd_ijrec_z_avg(:) = csd_ijrec_z_avg(:)/real(sloop,kind=RXP)
     
            gam_ijrec_x(:) = abs(csd_ijrec_x_avg(:))/sqrt(psd_irec_x_avg(:) * psd_jrec_x_avg(:))
            gam_ijrec_y(:) = abs(csd_ijrec_y_avg(:))/sqrt(psd_irec_y_avg(:) * psd_jrec_y_avg(:))
            gam_ijrec_z(:) = abs(csd_ijrec_z_avg(:))/sqrt(psd_irec_z_avg(:) * psd_jrec_z_avg(:))
     
            open(newunit=myunit,file=trim(cdir)//trim(prefix)//'.coherency.d'//trim(cd)//".asc")
     
            do i = 1_IXP,nfold
     
               write(myunit,'(4(E15.7,1X))') df*real(i-1_IXP,kind=RXP),abs(gam_ijrec_x(i)),abs(gam_ijrec_y(i)),abs(gam_ijrec_z(i))
     
            enddo
     
            close(myunit)
     
         enddo !on d

         deallocate(ft_rec_x)
         deallocate(ft_rec_y)
         deallocate(ft_rec_z)
         deallocate(psd_irec_x)
         deallocate(psd_irec_y)
         deallocate(psd_irec_z)
         deallocate(psd_jrec_x)
         deallocate(psd_jrec_y)
         deallocate(psd_jrec_z)
         deallocate(csd_ijrec_x)
         deallocate(csd_ijrec_y)
         deallocate(csd_ijrec_z)
         deallocate(psd_irec_x_avg)
         deallocate(psd_irec_y_avg)
         deallocate(psd_irec_z_avg)
         deallocate(psd_jrec_x_avg)
         deallocate(psd_jrec_y_avg)
         deallocate(psd_jrec_z_avg)
         deallocate(csd_ijrec_x_avg)
         deallocate(csd_ijrec_y_avg)
         deallocate(csd_ijrec_z_avg)
         deallocate(gam_ijrec_x)
         deallocate(gam_ijrec_y)
         deallocate(gam_ijrec_z)
 
      enddo !on directories

!
!---->average all SIMU
      if (is_multiple_dir) then

         ios = init_array_complex(gam_ijrec_x,nfold,"receivers_lagged_coherency:gam_ijrec_x")
         ios = init_array_complex(gam_ijrec_y,nfold,"receivers_lagged_coherency:gam_ijrec_y")
         ios = init_array_complex(gam_ijrec_z,nfold,"receivers_lagged_coherency:gam_ijrec_z")

         do d = 1_IXP,nrec_d

            write(*,*) "d = ",d," - total number of summation = ",sloop_all_simu(d)
        
            write(cd,'(I6.6)') d

            psd_irec_x_avg_all_simu(:,d) = psd_irec_x_avg_all_simu(:,d)/real(sloop_all_simu(d),kind=RXP)
            psd_irec_y_avg_all_simu(:,d) = psd_irec_y_avg_all_simu(:,d)/real(sloop_all_simu(d),kind=RXP)
            psd_irec_z_avg_all_simu(:,d) = psd_irec_z_avg_all_simu(:,d)/real(sloop_all_simu(d),kind=RXP)
           
            psd_jrec_x_avg_all_simu(:,d) = psd_jrec_x_avg_all_simu(:,d)/real(sloop_all_simu(d),kind=RXP)
            psd_jrec_y_avg_all_simu(:,d) = psd_jrec_y_avg_all_simu(:,d)/real(sloop_all_simu(d),kind=RXP)
            psd_jrec_z_avg_all_simu(:,d) = psd_jrec_z_avg_all_simu(:,d)/real(sloop_all_simu(d),kind=RXP)
           
            csd_ijrec_x_avg_all_simu(:,d) = csd_ijrec_x_avg_all_simu(:,d)/real(sloop_all_simu(d),kind=RXP)
            csd_ijrec_y_avg_all_simu(:,d) = csd_ijrec_y_avg_all_simu(:,d)/real(sloop_all_simu(d),kind=RXP)
            csd_ijrec_z_avg_all_simu(:,d) = csd_ijrec_z_avg_all_simu(:,d)/real(sloop_all_simu(d),kind=RXP)
           
            gam_ijrec_x(:) = abs(csd_ijrec_x_avg_all_simu(:,d))/sqrt(psd_irec_x_avg_all_simu(:,d) * psd_jrec_x_avg_all_simu(:,d))
            gam_ijrec_y(:) = abs(csd_ijrec_y_avg_all_simu(:,d))/sqrt(psd_irec_y_avg_all_simu(:,d) * psd_jrec_y_avg_all_simu(:,d))
            gam_ijrec_z(:) = abs(csd_ijrec_z_avg_all_simu(:,d))/sqrt(psd_irec_z_avg_all_simu(:,d) * psd_jrec_z_avg_all_simu(:,d))
           
            open(newunit=myunit,file=trim(prefix)//'.coherency.all.simu.d'//trim(cd)//".asc")
           
            do i = 1_IXP,nfold
           
               write(myunit,'(4(E15.7,1X))') df*real(i-1_IXP,kind=RXP),abs(gam_ijrec_x(i)),abs(gam_ijrec_y(i)),abs(gam_ijrec_z(i))
           
            enddo
           
            close(myunit)

         enddo

      endif
 
      stop

end program
