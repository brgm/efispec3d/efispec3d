!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
module mod_snapshot_processing
   
   use mod_precision

   use mod_global_variables

   use mod_init_memory

   use mod_io_array

   implicit none

   private

   public  :: snapshot_read
   public  :: snapshot_quad_get_indirection
   public  :: snapshot_quad_reorder
   public  :: snapshot_init_glonum_unconnected
   public  :: snapshot_init_gno_coord
   public  :: snapshot_reshape
   public  :: snapshot_gll2grd
   public  :: write_gnuplot_matrix

   interface snapshot_read

      module procedure snapshot_read_rank3
      module procedure snapshot_read_rank4

   end interface snapshot_read

   interface snapshot_quad_reorder

      module procedure snapshot_quad_reorder_rank3
      module procedure snapshot_quad_reorder_rank4

   end interface snapshot_quad_reorder

   interface snapshot_reshape

      module procedure snapshot_reshape_rank2
      module procedure snapshot_reshape_rank3

   end interface snapshot_reshape

   interface snapshot_init_glonum_unconnected

      module procedure snapshot_init_glonum_unconnected_rank2
      module procedure snapshot_init_glonum_unconnected_rank3

   end interface snapshot_init_glonum_unconnected

   contains

!
!
!>@brief subroutine to load into memory the efispec file 'f' written by module_io_array::efi_mpi_file_write_at_all_rank3
!>@param  f  : filename of snapshot file to be read
!>@return s  : array containing the array stored in the file 'f'
!***********************************************************************************************************************************************************************************
   subroutine snapshot_read_rank3(f,a)
!***********************************************************************************************************************************************************************************

      implicit none

      character(len=*)                 , intent(in ) :: f
      real(kind=RXP) , dimension(:,:,:), intent(out) :: a

      real(kind=RXP) , dimension(:,:,:), allocatable :: atmp

      integer(kind=IXP)                              :: nquad_fsurf_all_cpu
      integer(kind=IXP)                              :: nquad_floor
      integer(kind=IXP)                              :: iquad_offset

      call efi_mpi_file_read_at_all(ig_mpi_comm_simu,trim(f),ig_myrank,ig_ncpu,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS,atmp,nquad_fsurf_all_cpu,ig_nquad_fsurf,nquad_floor,iquad_offset)

      a = atmp

      deallocate(atmp)

      return

!***********************************************************************************************************************************************************************************
   end subroutine snapshot_read_rank3
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to load into memory the efispec file 'f' written by module_io_array::efi_mpi_file_write_at_all_rank4
!>@param  f  : filename of snapshot file to be read
!>@return s  : array containing the array stored in the file 'f'
!***********************************************************************************************************************************************************************************
   subroutine snapshot_read_rank4(f,a)
!***********************************************************************************************************************************************************************************

      implicit none

      character(len=*)                   , intent(in ) :: f
      real(kind=RXP) , dimension(:,:,:,:), intent(out) :: a

      real(kind=RXP) , dimension(:,:,:,:), allocatable :: atmp

      integer(kind=IXP)                                :: nquad_fsurf_all_cpu
      integer(kind=IXP)                                :: nquad_floor
      integer(kind=IXP)                                :: iquad_offset

      call efi_mpi_file_read_at_all(ig_mpi_comm_simu,trim(f),ig_myrank,ig_ncpu,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS,atmp,nquad_fsurf_all_cpu,ig_nquad_fsurf,nquad_floor,iquad_offset)

      a = atmp

      deallocate(atmp)

      return

!***********************************************************************************************************************************************************************************
   end subroutine snapshot_read_rank4
!***********************************************************************************************************************************************************************************

!
!
!>@brief subroutine to get indirection array from master to slave
!>@param  m : master array
!>@return s : slave array to be reorder according to master
!>@return i : indirection array that can be used for other array (e.g., quad_gll_uxyz_max)
!***********************************************************************************************************************************************************************************
   subroutine snapshot_quad_get_indirection(m,s,i)
!***********************************************************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:,:,:)             , intent(in ) :: m
      real   (kind=RXP), dimension(:,:,:)             , intent(in ) :: s
      integer(kind=IXP), dimension(:)    , allocatable, intent(out) :: i

      integer(kind=IXP)                                             :: ios
      integer(kind=IXP)                                             :: nnode
      integer(kind=IXP)                                             :: nquad
      integer(kind=IXP)                                             :: inode
      integer(kind=IXP)                                             :: iquad
      integer(kind=IXP)                                             :: jquad
      integer(kind=IXP)                                             :: nnode_match

      logical(kind=IXP), dimension(:), allocatable                  :: is_found

      character(len=CIL)                                            :: info

      nnode = size(m,2_IXP)
      nquad = size(m,3_IXP)

      if (allocated(i)) deallocate(i)

      ios = init_array_int(i,nquad,"mod_snapshot_processing::snapshot_quad_get_indirection::i")

      ios = init_array_logical(is_found,nquad,"mod_snapshot_processing::snapshot_quad_get_indirection::is_found")

lm:   do iquad = 1_IXP,nquad

ls:      do jquad = 1_IXP,nquad

            if (is_found(jquad)) cycle ls

            nnode_match = ZERO_IXP

            do inode = 1_IXP,nnode

               if ( abs(m(1_IXP,inode,iquad) - s(1_IXP,inode,jquad)) < 10.0_RXP*EPSILON_MACHINE_RXP .and.&
                    abs(m(2_IXP,inode,iquad) - s(2_IXP,inode,jquad)) < 10.0_RXP*EPSILON_MACHINE_RXP .and.&
                    abs(m(3_IXP,inode,iquad) - s(3_IXP,inode,jquad)) < 10.0_RXP*EPSILON_MACHINE_RXP       ) then

                  nnode_match = nnode_match + ONE_IXP

               endif

            enddo

            if (nnode_match == nnode) then

               is_found(jquad) = .true.

               i(jquad) = iquad

               cycle lm

            endif

         enddo ls

      enddo lm

      if (.not.any(is_found)) then

         write(info,'(A)') "error in subroutine mod_snapshot_processing:snapshot_quad_get_indirection. all quad not reordered"

         call error_stop(info)

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine snapshot_quad_get_indirection
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to reorder rank3 array according to an indirection array
!>@param  a : array to be reorder
!>@param  i : indirection array
!>@return a : array reorded
!***********************************************************************************************************************************************************************************
   subroutine snapshot_quad_reorder_rank3(a,i)
!***********************************************************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:,:,:), intent(inout) :: a
      integer(kind=IXP), dimension(:)    , intent(in   ) :: i

      real   (kind=RXP), dimension(:,:,:), allocatable   :: tmp

      integer(kind=IXP)                                  :: ios
      integer(kind=IXP)                                  :: n1
      integer(kind=IXP)                                  :: n2
      integer(kind=IXP)                                  :: n3
      integer(kind=IXP)                                  :: i1
      integer(kind=IXP)                                  :: i2
      integer(kind=IXP)                                  :: i3


      n1    = size(a,1_IXP)
      n2    = size(a,2_IXP)
      n3    = size(a,3_IXP)

      ios = init_array_real(tmp,n3,n2,n1,"mod_snapshot_processing:snapshot_quad_reorder:tmp")

!
!---->fill in 'tmp' array

      do i3 = 1_IXP,n3

         do i2 = 1_IXP,n2

            do i1 = 1_IXP,n1
   
               tmp(i1,i1,i3) = a(i1,i2,i3)

            enddo

         enddo

      enddo

!
!---->reorder array 'a' according to indirection 'i'

      do i3 = 1_IXP,n3

         do i2 = 1_IXP,n2

            do i1 = 1_IXP,n1
   
               a(i1,i2,i(i3)) = tmp(i1,i2,i3)

            enddo

         enddo

      enddo

      deallocate(tmp)

      return

!***********************************************************************************************************************************************************************************
   end subroutine snapshot_quad_reorder_rank3
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to reorder rank4 array according to an indirection array
!>@param  a : array to be reorder
!>@param  i : indirection array
!>@return a : array reorded
!***********************************************************************************************************************************************************************************
   subroutine snapshot_quad_reorder_rank4(a,i)
!***********************************************************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:,:,:,:), intent(inout) :: a
      integer(kind=IXP), dimension(:)      , intent(in   ) :: i

      real   (kind=RXP), dimension(:,:,:,:), allocatable   :: tmp

      integer(kind=IXP)                                    :: ios
      integer(kind=IXP)                                    :: n1
      integer(kind=IXP)                                    :: n2
      integer(kind=IXP)                                    :: n3
      integer(kind=IXP)                                    :: n4
      integer(kind=IXP)                                    :: i1
      integer(kind=IXP)                                    :: i2
      integer(kind=IXP)                                    :: i3
      integer(kind=IXP)                                    :: i4


      n1 = size(a,1_IXP)
      n2 = size(a,2_IXP)
      n3 = size(a,3_IXP)
      n4 = size(a,4_IXP)

      ios = init_array_real(tmp,n4,n3,n2,n1,"mod_snapshot_processing:snapshot_quad_reorder:tmp")

!
!---->fill in 'tmp' array

      do i4 = 1_IXP,n4

         do i3 = 1_IXP,n3

            do i2 = 1_IXP,n2

               do i1 = 1_IXP,n1
   
                  tmp(i1,i2,i3,i4) = a(i1,i2,i3,i4)

               enddo

            enddo

         enddo

      enddo

!
!---->reorder array 'a' according to indirection 'i'

      do i4 = 1_IXP,n4

         do i3 = 1_IXP,n3

            do i2 = 1_IXP,n2

               do i1 = 1_IXP,n1
   
                  a(i1,i2,i3,i(i4)) = tmp(i1,i2,i3,i4)

               enddo

            enddo

         enddo

      enddo

      deallocate(tmp)

      return

!***********************************************************************************************************************************************************************************
   end subroutine snapshot_quad_reorder_rank4
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to fill rank 2 array 'a' using a global numbering independent of any connectivity (i.e., numbering is different even if a geometric contact exist)
!>@param  n1 : size of first  dimension of array 'a'
!>@param  n2 : size of second dimension of array 'a'
!>@param  a  : array to be filled
!>@return a  : array filled
!***********************************************************************************************************************************************************************************
   subroutine snapshot_init_glonum_unconnected_rank2(n1,n2,a)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP)                             , intent( in) :: n1
      integer(kind=IXP)                             , intent( in) :: n2
      integer(kind=IXP), dimension(:,:), allocatable, intent(out) :: a

      integer(kind=IXP)                                           :: ios
      integer(kind=IXP)                                           :: iloc
      integer(kind=IXP)                                           :: i1
      integer(kind=IXP)                                           :: i2

      ios = init_array_int(a,n2,n1,"mod_snapshot_processing::snapshot_init_glonum_unconnected_rank2::a")
     
      iloc = ZERO_IXP
     
      do i2 = ONE_IXP,n2
     
         do i1 = ONE_IXP,n1
     
            iloc = iloc + ONE_IXP
     
            a(i1,i2) = iloc
     
         enddo
     
      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine snapshot_init_glonum_unconnected_rank2
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to fill rank 3 array 'a' using a global numbering independent of any connectivity (i.e., numbering is different even if a geometric contact exist)
!>@param  n1 : size of first  dimension of array 'a'
!>@param  n2 : size of second dimension of array 'a'
!>@param  n3 : size of third  dimension of array 'a'
!>@param  a  : array to be filled
!>@return a  : array filled
!***********************************************************************************************************************************************************************************
   subroutine snapshot_init_glonum_unconnected_rank3(n1,n2,n3,a)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP)                               , intent( in) :: n1
      integer(kind=IXP)                               , intent( in) :: n2
      integer(kind=IXP)                               , intent( in) :: n3
      integer(kind=IXP), dimension(:,:,:), allocatable, intent(out) :: a

      integer(kind=IXP)                                           :: ios
      integer(kind=IXP)                                           :: iloc
      integer(kind=IXP)                                           :: i1
      integer(kind=IXP)                                           :: i2
      integer(kind=IXP)                                           :: i3

      ios = init_array_int(a,n3,n2,n1,"mod_snapshot_processing::snapshot_init_glonum_unconnected_rank3::a")
     
      iloc = ZERO_IXP
     
      do i3 = ONE_IXP,n3

         do i2 = ONE_IXP,n2
        
            do i1 = ONE_IXP,n1
        
               iloc = iloc + ONE_IXP
        
               a(i1,i2,i3) = iloc
        
            enddo
        
         enddo

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine snapshot_init_glonum_unconnected_rank3
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to fill @f$x,y,z@f$-coordinate array using a global numbering
!>@param  q : array to be filled
!>@return q : array filled
!***********************************************************************************************************************************************************************************
   subroutine snapshot_init_gno_coord(gxyz,icoord,g)
!***********************************************************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:,:,:)             , intent( in) :: gxyz
      integer(kind=IXP)                               , intent( in) :: icoord
      real   (kind=RXP), dimension(:)    , allocatable, intent(out) :: g

      integer(kind=IXP)                                             :: ios
      integer(kind=IXP)                                             :: iloc
      integer(kind=IXP)                                             :: i2
      integer(kind=IXP)                                             :: i3
      integer(kind=IXP)                                             :: n2
      integer(kind=IXP)                                             :: n3

      n2 = size(gxyz,2_IXP)
      n3 = size(gxyz,3_IXP)

      ios = init_array_real(g,n2*n3,"mod_snapshot_processing::snapshot_init_gno_coord::rg_gnode_?")

      iloc = ZERO_IXP
     
      do i3 = ONE_IXP,n3
     
         do i2 = ONE_IXP,n2
     
            iloc = iloc + ONE_IXP
     
            g(iloc) = gxyz(icoord,i2,i3)
     
         enddo
     
      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine snapshot_init_gno_coord
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to transfer only one max value of the array 'i' of rank 2 to the array 'o' of rank 1
!>@param  i : input array containing all the max
!>@return o : output array containing only one max
!***********************************************************************************************************************************************************************************
   subroutine snapshot_reshape_rank2(i,o)
!***********************************************************************************************************************************************************************************

      implicit none
 
      real   (kind=RXP), intent( in), dimension(:,:) :: i
      real   (kind=RXP), intent(out), dimension(:)   :: o
 
      integer(kind=IXP)                              :: n1
      integer(kind=IXP)                              :: n2
      integer(kind=IXP)                              :: i1
      integer(kind=IXP)                              :: i2
      integer(kind=IXP)                              :: il
 
      n1 = size(i,1_IXP)
      n2 = size(i,2_IXP)
 
      il = ZERO_IXP
 
      do i2 = ONE_IXP,n2
 
         do i1 = ONE_IXP,n1
 
            il = il + ONE_IXP
 
            o(il) = i(i1,i2)
 
         enddo
 
      enddo
 
      return

!***********************************************************************************************************************************************************************************
   end subroutine snapshot_reshape_rank2
!***********************************************************************************************************************************************************************************



!
!
!>@brief subroutine to transfer only one max value of the array 'i' of rank 3 to the array 'o' of rank 1
!>@param  i : input array containing all the max
!>@return o : output array containing only one max
!***********************************************************************************************************************************************************************************
   subroutine snapshot_reshape_rank3(i,o)
!***********************************************************************************************************************************************************************************

      implicit none
 
      real   (kind=RXP), intent( in), dimension(:,:,:) :: i
      real   (kind=RXP), intent(out), dimension(:)     :: o
 
      integer(kind=IXP)                                :: n1
      integer(kind=IXP)                                :: n2
      integer(kind=IXP)                                :: n3
      integer(kind=IXP)                                :: i1
      integer(kind=IXP)                                :: i2
      integer(kind=IXP)                                :: i3
      integer(kind=IXP)                                :: il
 
      n1 = size(i,1_IXP)
      n2 = size(i,2_IXP)
      n3 = size(i,3_IXP)
 
      il = ZERO_IXP
 
      do i3 = ONE_IXP,n3
 
         do i2 = ONE_IXP,n2
 
            do i1 = ONE_IXP,n1
 
               il = il + ONE_IXP
 
               o(il) = i(i1,i2,i3)
 
            enddo
 
         enddo
 
      enddo
 
      return

!***********************************************************************************************************************************************************************************
   end subroutine snapshot_reshape_rank3
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to convert unstructured gll points to structured grid
!>@param  quad_gll : array of quadrangle gll points (written by module_io_array)
!>@return grid     : grid interpolated from gll points
!***********************************************************************************************************************************************************************************
   subroutine snapshot_gll2grd(quad_gll,grid)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      tg_receiver_snapshot_quad&
                                     ,IG_NGLL&
                                     ,ig_ngll_total&
                                     ,nx => ig_receiver_snapshot_nx&
                                     ,ig_nreceiver_snapshot

      use mod_gll_value, only : get_quad_gll_value

      use mod_lagrange , only : quad_lagrange_interp

      implicit none

      real   (kind=RXP), dimension(ig_ngll_total), intent( in) :: quad_gll
      real   (kind=RXP), dimension(:,:)          , intent(out) :: grid

      real   (kind=RXP), dimension(IG_NGLL,IG_NGLL)            :: quad_gll_local
      real   (kind=RXP)                                        :: grid_val      
      integer(kind=IXP)                                        :: irec
      integer(kind=IXP)                                        :: ix
      integer(kind=IXP)                                        :: iy
      integer(kind=IXP)                                        :: irec_global

      do irec = ONE_IXP,ig_nreceiver_snapshot

!
!------->get GLL nodes displacement, velocity and acceleration xyz-values from quadrangle element which contains the snapshot receiver

         call get_quad_gll_value(quad_gll,tg_receiver_snapshot_quad(irec)%gll,quad_gll_local)

!
!------->interpolate displacement at the snapshot receiver

         call quad_lagrange_interp(quad_gll_local,tg_receiver_snapshot_quad(irec)%lag,grid_val)

!
!------->fill array grid

         irec_global = tg_receiver_snapshot_quad(irec)%rglo

         iy = int( (irec_global-1_IXP)/nx ) + 1_IXP !local numbering from global

         ix = mod( (irec_global-1_IXP),nx ) + 1_IXP !local numbering from global

         grid(ix,iy) = grid_val

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine snapshot_gll2grd
!***********************************************************************************************************************************************************************************


!
!
!********************************************************************************************************************************************
   subroutine write_gnuplot_matrix(f,x,y,m)
!********************************************************************************************************************************************

      implicit none

      character(len=*)              , intent(in) :: f
      real(kind=RXP), dimension(:)  , intent(in) :: x
      real(kind=RXP), dimension(:)  , intent(in) :: y
      real(kind=RXP), dimension(:,:), intent(in) :: m

      integer(kind=IXP)                          :: i
      integer(kind=IXP)                          :: j
      integer(kind=IXP)                          :: nx
      integer(kind=IXP)                          :: ny
      integer(kind=IXP)                          :: myunit

      nx = size(x)
      ny = size(y)

     !write(*,*) "min/max matrix "//trim(f),minval(m),maxval(m)

      open (newunit=myunit,file=trim(f),status='replace',form='unformatted',access='stream',action='write')

      write(myunit) real(nx,kind=RXP),x(:),(y(j),(m(i,j),i=1,nx),j=1,ny)

      close(myunit)

      return

!********************************************************************************************************************************************
   end subroutine
!********************************************************************************************************************************************


end module mod_snapshot_processing
