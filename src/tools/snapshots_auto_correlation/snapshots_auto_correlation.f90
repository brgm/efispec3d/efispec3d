!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
program snapshot_processing

   use mpi

   use mod_precision

   use mod_global_variables

   use mod_efi_mpi

   use mod_init_efi

   use mod_init_mesh, only : init_element

   use mod_init_memory

   use mod_io_array

   use mod_snapshot_surface

   use mod_signal_processing

   use mod_snapshot_processing

   implicit none

   complex(kind=RXP), dimension(:,:)      , allocatable :: grid_fft
   complex(kind=RXP), dimension(:,:)      , allocatable :: grid_psd

   real   (kind=RXP), dimension(:,:,:,:,:), allocatable :: simu_quadf_gll_uxyz_max
   real   (kind=RXP), dimension(:,:,:,:)  , allocatable :: simu_quadf_gno_xyz
   real   (kind=RXP), dimension(:,:,:,:)  , allocatable :: simu_grid_max
   real   (kind=RXP), dimension(:,:,:)    , allocatable :: master_quadf_gno_xyz
   real   (kind=RXP), dimension(:,:,:)    , allocatable :: grid_sum_local
   real   (kind=RXP), dimension(:,:,:)    , allocatable :: grid_mean_global
   real   (kind=RXP), dimension(:,:,:)    , allocatable :: grid_variance
   real   (kind=RXP), dimension(:,:,:)    , allocatable :: grid_variance_global
   real   (kind=RXP), dimension(:,:)      , allocatable :: grid_acf
   real   (kind=RXP), dimension(:,:)      , allocatable :: grid_tmp
   real   (kind=RXP), dimension(:)        , allocatable :: gll_max
   real   (kind=RXP), dimension(:)        , allocatable :: x
   real   (kind=RXP), dimension(:)        , allocatable :: y
   real   (kind=RXP), dimension(:)        , allocatable :: kx
   real   (kind=RXP), dimension(:)        , allocatable :: ky

   real   (kind=RXP)                                    :: dx
   real   (kind=RXP)                                    :: dy
   real   (kind=RXP)                                    :: mean
   real   (kind=RXP)                                    :: var
   real   (kind=RXP)                                    :: fx
   real   (kind=RXP)                                    :: fy

   integer(kind=IXP), dimension(:)        , allocatable :: ind
   integer(kind=IXP), dimension(:)        , allocatable :: shape_tmp

   integer(kind=IXP)                                    :: nmax
   integer(kind=IXP)                                    :: ix
   integer(kind=IXP)                                    :: iy
   integer(kind=IXP)                                    :: nx
   integer(kind=IXP)                                    :: ny

   integer(kind=IXP)                                    :: isimu
   integer(kind=IXP)                                    :: jsimu
   integer(kind=IXP)                                    :: isimu_start
   integer(kind=IXP)                                    :: isimu_end
   integer(kind=IXP)                                    :: nsimu_rank
   integer(kind=IXP)                                    :: nsimu
   integer(kind=IXP)                                    :: usimu
   integer(kind=IXP)                                    :: iquad
   integer(kind=IXP)                                    :: inode
   integer(kind=IXP)                                    :: idof
   integer(kind=IXP)                                    :: imax
   integer(kind=IXP)                                    :: ios

   character(len=CIL), dimension(:), allocatable        :: simu_dir
   character(len=CIL)                                   :: fmax
   character(len=CIL)                                   :: ext
   character(len=CIL)                                   :: fgno
   character(len=CIL)                                   :: fsimu
   character(len=CIL)                                   :: fname
   character(len=CIL)                                   :: prefix

!
!
!***********************************************************************************************************************************************************************************
!->init mpi using efispec subroutine
!***********************************************************************************************************************************************************************************

   call init_mpi()

!
!
!***********************************************************************************************************************************************************************************
!->get the number of simulations
!***********************************************************************************************************************************************************************************

   fsimu = "simu.dir"

   nsimu = get_number_of_simulations(fsimu)

   if (ig_myrank_world == ZERO_IXP) print *,"nsimu = ",nsimu

!
!
!***********************************************************************************************************************************************************************************
!->all cpus store the directory names of the simulations
!***********************************************************************************************************************************************************************************

   simu_dir = get_folder_of_simulations(fsimu,nsimu)

!
!
!***********************************************************************************************************************************************************************************
!->get the prefix and set the listing file extension
!***********************************************************************************************************************************************************************************

   prefix = get_prefix()

   cg_lst_ext = ".post.lst"

!
!
!***********************************************************************************************************************************************************************************
!->create the partition over the cpus. the communicator *_world is used for the partition but the communicator *_simu for post-processing the grid, because to compute serial FFTs, it is simpler that a cpu knows the entire grid
!***********************************************************************************************************************************************************************************

   call partition1d(ig_myrank_world,ig_ncpu_world,nsimu,isimu_start,isimu_end)

   nsimu_rank = isimu_end - isimu_start + 1_IXP

!
!
!***********************************************************************************************************************************************************************************
!->update full prefix of the simulations (including directory path)
!***********************************************************************************************************************************************************************************

   cg_prefix = trim(simu_dir(isimu_start))//"/"//trim(prefix)


!
!
!***********************************************************************************************************************************************************************************
!->call subroutine init_input_variables to init the dx,dy of the grd file and call to init_element to inint the elementary array of the elements
!***********************************************************************************************************************************************************************************

   call init_input_variables(cg_prefix)

   ig_hexa_nnode = 8_IXP

   ig_quad_nnode = 4_IXP

   call init_element(ig_hexa_nnode,ig_quad_nnode)

   call init_gll_nodes() !init gll node locally (coordinates, weights,etc.)

!
!
!***********************************************************************************************************************************************************************************
!->get the shape of the array stored in the file *.snapshot.quadf.gno.xyz
!***********************************************************************************************************************************************************************************

   fgno = trim(cg_prefix)//".snapshot.quadf.gno.xyz"

   call get_file_shape(fgno,ig_mpi_comm_simu,LG_LUSTRE_FILE_SYS,rank(simu_quadf_gno_xyz)-1_IXP,shape_tmp)

   ig_nquad_fsurf = shape_tmp(3_IXP)

   ig_ngll_total = IG_NGLL*IG_NGLL*ig_nquad_fsurf !counting duplicates because global numbering is unconnected

!
!
!***********************************************************************************************************************************************************************************
!->allocate array simu_quadf_gno_xyz and min/max arrays
!***********************************************************************************************************************************************************************************

   ios = init_array_real(simu_quadf_gno_xyz,nsimu_rank,shape_tmp(3_IXP),shape_tmp(2_IXP),shape_tmp(1_IXP),"snapshot_processing:simu_quadf_gno_xyz")

   deallocate(shape_tmp)

!
!
!***********************************************************************************************************************************************************************************
!->get the shape of the array stored in the file *.snapshot.quadf.gll.uxyz.max
!***********************************************************************************************************************************************************************************

   selectcase(trim(cg_iir_filter_output_motion))

      case("dis")
         ext = ".snapshot.quadf.gll.uxyz.max"

      case("vel")
         ext = ".snapshot.quadf.gll.vxyz.max"

      case("acc")
         ext = ".snapshot.quadf.gll.axyz.max"

   endselect

   fmax = trim(cg_prefix)//trim(ext)

   call get_file_shape(fmax,ig_mpi_comm_simu,LG_LUSTRE_FILE_SYS,rank(simu_quadf_gll_uxyz_max)-1_IXP,shape_tmp)

!
!
!***********************************************************************************************************************************************************************************
!->allocate simu_quadf_gll_uxyz_max
!***********************************************************************************************************************************************************************************

   ios = init_array_real(simu_quadf_gll_uxyz_max,nsimu_rank,shape_tmp(4_IXP),shape_tmp(3_IXP),shape_tmp(2_IXP),shape_tmp(1_IXP),"snapshot_processing:simu_quadf_gll_uxyz_max")

   nmax = shape_tmp(1_IXP)

   deallocate(shape_tmp)

!
!
!***********************************************************************************************************************************************************************************
!->loop over simulations to load the max into arrays simu_quadf_gno_xyz and simu_quadf_gll_uxyz_max
!***********************************************************************************************************************************************************************************

   open(newunit=usimu,file=trim(fsimu))

   jsimu = ZERO_IXP

   do isimu = isimu_start,isimu_end

      jsimu = jsimu + ONE_IXP

      fgno = trim(simu_dir(isimu))//"/"//trim(prefix)//".snapshot.quadf.gno.xyz"

      call snapshot_read(fgno,simu_quadf_gno_xyz(:,:,:,jsimu))

   enddo

   rewind(usimu)

   jsimu = ZERO_IXP

   do isimu = isimu_start,isimu_end

      jsimu = jsimu + ONE_IXP

      fmax = trim(simu_dir(isimu))//"/"//trim(prefix)//trim(ext)

      call snapshot_read(fmax,simu_quadf_gll_uxyz_max(:,:,:,:,jsimu))

   enddo

   close(usimu)

!
!
!***********************************************************************************************************************************************************************************
!->reorder quad so that simulations that have been launched on different number of cpu have all the same quad ordering
!***********************************************************************************************************************************************************************************

!
!->ig_myrank_world sends the master ordering to all cpus

   ios = init_array_real(master_quadf_gno_xyz,ig_nquad_fsurf,ig_quad_nnode,IG_NDOF,"snapshot_processing:master_quadf_gno_xyz")

   if (ig_myrank_world == ZERO_IXP) then

      do iquad = 1_IXP,ig_nquad_fsurf

         do inode = 1_IXP,ig_quad_nnode

            do idof = 1_IXP,IG_NDOF

               master_quadf_gno_xyz(idof,inode,iquad) = simu_quadf_gno_xyz(idof,inode,iquad,1_IXP)

            enddo

         enddo

      enddo

   endif

   call mpi_bcast(master_quadf_gno_xyz(1_IXP,1_IXP,1_IXP),ig_nquad_fsurf*ig_quad_nnode*IG_NDOF,MPI_REAL,ZERO_IXP,MPI_COMM_WORLD,ios)

!
!->loop over all the simu to reorder

   if (ig_myrank_world == ZERO_IXP) then

      do isimu = 2_IXP,nsimu_rank

         call snapshot_quad_get_indirection(master_quadf_gno_xyz(:,:,:),simu_quadf_gno_xyz(:,:,:,isimu),ind)

         call snapshot_quad_reorder(simu_quadf_gll_uxyz_max(:,:,:,:,isimu),ind)

      enddo

   else

      do isimu = 1_IXP,nsimu_rank

         call snapshot_quad_get_indirection(master_quadf_gno_xyz(:,:,:),simu_quadf_gno_xyz(:,:,:,isimu),ind)

         call snapshot_quad_reorder(simu_quadf_gll_uxyz_max(:,:,:,:,isimu),ind)

      enddo

   endif

!
!->since arrayq 'simu_quadf_gno_xyz' have been reorder, only 'master_quadf_gno_xyz' is needed by all cpus

   deallocate(simu_quadf_gno_xyz)

!
!
!***********************************************************************************************************************************************************************************
!->set min/max of the free surface mesh save in file *.snapshot.quadf.gno.xyz
!***********************************************************************************************************************************************************************************

   rg_mesh_xmin = minval(master_quadf_gno_xyz(1_IXP,:,:))
   rg_mesh_xmax = maxval(master_quadf_gno_xyz(1_IXP,:,:))
   rg_mesh_ymin = minval(master_quadf_gno_xyz(2_IXP,:,:))
   rg_mesh_ymax = maxval(master_quadf_gno_xyz(2_IXP,:,:))
   rg_mesh_zmin = minval(master_quadf_gno_xyz(3_IXP,:,:))
   rg_mesh_zmax = maxval(master_quadf_gno_xyz(3_IXP,:,:))

!
!->modify the bounding box of the snapshot area

   rg_mesh_xmin =   8000.0_RXP!  7000.0_RXP
   rg_mesh_xmax =  11000.0_RXP! 12000.0_RXP
   rg_mesh_ymin =  24000.0_RXP! 23000.0_RXP
   rg_mesh_ymax =  27000.0_RXP! 28000.0_RXP

  !rg_mesh_xmin =   8700.0_RXP
  !rg_mesh_xmax =   9700.0_RXP
  !rg_mesh_ymin =  24500.0_RXP
  !rg_mesh_ymax =  25500.0_RXP

!
!
!***********************************************************************************************************************************************************************************
!->fill coordinate arrays rg_gnode_x, rg_gnode_y and rg_gnode_z. These arrays use global numbering without connectivity.
!***********************************************************************************************************************************************************************************

   call snapshot_init_gno_coord(master_quadf_gno_xyz(:,:,:),1_IXP,rg_gnode_x)

   call snapshot_init_gno_coord(master_quadf_gno_xyz(:,:,:),2_IXP,rg_gnode_y)

   call snapshot_init_gno_coord(master_quadf_gno_xyz(:,:,:),3_IXP,rg_gnode_z)

   deallocate(master_quadf_gno_xyz)

!
!
!***********************************************************************************************************************************************************************************
!->call to fake arrays ig_quadf_gnode_glonum and ig_quadf_gll_glonum ('fake' because there is no connectivity between quad. It is not needed for post-processing)
!***********************************************************************************************************************************************************************************

   call snapshot_init_glonum_unconnected(ig_quad_nnode,ig_nquad_fsurf,ig_quadf_gnode_glonum)

   call snapshot_init_glonum_unconnected(IG_NGLL,IG_NGLL,ig_nquad_fsurf,ig_quadf_gll_glonum)

!
!
!***********************************************************************************************************************************************************************************
!->init snapshot
!***********************************************************************************************************************************************************************************

!
!->set a common dx/dy for all cpu from ig_myrank_world = 0 (e.g., read in SIMU0001/ *.cfg)

   call mpi_bcast(rg_receiver_snapshot_dxdy,1_IXP,MPI_REAL,ZERO_IXP,MPI_COMM_WORLD,ios)

!
!->ensure that the number of grid points are power of two to simplify FFT computation

   nx = int((rg_mesh_xmax-rg_mesh_xmin)/rg_receiver_snapshot_dxdy,kind=IXP)+ONE_IXP
   ny = int((rg_mesh_ymax-rg_mesh_ymin)/rg_receiver_snapshot_dxdy,kind=IXP)+ONE_IXP

   ig_receiver_snapshot_nx = nextpow2(nx)
   ig_receiver_snapshot_ny = nextpow2(ny)

!
!->init snapshot structured grid

   call init_snapshot_surface() !TODO FLO: check if it could not be done in parallel using all cpus. In the current case, all cpus do the same init!
                                !in this case, ig_quadf_gnode_glonum must be partitioned between all cpus, so another call to efi_mpi_read is needed using MPI_COMM_WORLD

!
!
!***********************************************************************************************************************************************************************************
!->convert gll unstructured points to structured grid (one grid per simulation and per maximum value)
!***********************************************************************************************************************************************************************************

!
!->allocate gll array that will contain only one max value among the 'nmax' values

   ios = init_array_real(gll_max,ig_ngll_total,"snapshot_processing:gll_max")

!
!->allocate grid array that will contain all max for all simu

   ios = init_array_real(simu_grid_max,nsimu_rank,ig_receiver_snapshot_ny,ig_receiver_snapshot_nx,nmax,"snapshot_processing:simu_grid_max")

!
!->compute and write peak ground motion

   if (ig_myrank_world == ZERO_IXP) print *,"convert GLL to GRID"

   jsimu = ZERO_IXP

   do isimu = isimu_start,isimu_end

      jsimu = jsimu + ONE_IXP

      do imax = 1_IXP,nmax

         call snapshot_reshape(simu_quadf_gll_uxyz_max(imax,:,:,:,jsimu),gll_max(:))

         call snapshot_gll2grd(gll_max,simu_grid_max(imax,:,:,jsimu))

         fname = trim(simu_dir(isimu))//"/"//trim(prefix)//".snapshot.PGA"//trim(int2str(imax))//".grd"

         call write_peak_ground_motion_gll(gll_max,fname)

      enddo

   enddo

   deallocate(gll_max)


!
!
!***********************************************************************************************************************************************************************************
!->set extend of domain (spatial or spectral)
!***********************************************************************************************************************************************************************************

   dx = rg_receiver_snapshot_dx
   dy = rg_receiver_snapshot_dy

   nx = ig_receiver_snapshot_nx
   ny = ig_receiver_snapshot_ny

   ios = init_array_real(x,nx,"snapshot_processing:x")

   ios = init_array_real(y,ny,"snapshot_processing:y")

   ios = init_array_real(kx,nx,"snapshot_processing:kx")

   ios = init_array_real(ky,ny,"snapshot_processing:ky")

   x(1_IXP:nx) = (/ (rg_mesh_xmin + real(ix-1_IXP,kind=RXP)*dx,ix=1_IXP,nx) /)
   y(1_IXP:ny) = (/ (rg_mesh_ymin + real(iy-1_IXP,kind=RXP)*dy,iy=1_IXP,ny) /)

   fx = 1.0_RXP/(real(nx,kind=RXP)*dx)
   fy = 1.0_RXP/(real(ny,kind=RXP)*dy)

   do ix = 1_IXP,nx

      kx(ix) = real(ix-1_IXP,kind=RXP)*fx*PI_RXP

   enddo

   do iy = 1_IXP,ny

      ky(iy) = real(iy-1_IXP,kind=RXP)*fy*PI_RXP

   enddo


!
!
!***********************************************************************************************************************************************************************************
!->compute mean and variance from simulations listed in array 'simu_dir'
!***********************************************************************************************************************************************************************************

   if (ig_myrank_world == ZERO_IXP) print *,"Computing MEAN and VARIANCE"

   ios = init_array_real(grid_sum_local,ny,nx,nmax,"snapshot_processing:grid_sum_local")

   ios = init_array_real(grid_mean_global,ny,nx,nmax,"snapshot_processing:grid_mean_global")

   ios = init_array_real(grid_variance,ny,nx,nmax,"snapshot_processing:grid_variance")

   ios = init_array_real(grid_variance_global,ny,nx,nmax,"snapshot_processing:grid_variance_global")

!
!->each cpu sums its simulations and send them to master cpu0_world

   do isimu = ONE_IXP,nsimu_rank

      do iy = ONE_IXP,ny

         do ix = ONE_IXP,nx

            do imax = ONE_IXP,nmax

               grid_sum_local(imax,ix,iy) = grid_sum_local(imax,ix,iy) + simu_grid_max(imax,ix,iy,isimu)

            enddo

         enddo

      enddo

   enddo

!
!->all cpus compute the mean (over all simulations) -> mpi_allreduce

   call mpi_allreduce(grid_sum_local,grid_mean_global,nmax*ny*nx,MPI_REAL,MPI_SUM,MPI_COMM_WORLD,ios)

   grid_mean_global(:,:,:) = grid_mean_global(:,:,:)/real(nsimu,kind=RXP)

!
!->pre-compute member of variance locally

   do isimu = ONE_IXP,nsimu_rank

      do iy = ONE_IXP,ny

         do ix = ONE_IXP,nx

            do imax = ONE_IXP,nmax

               grid_variance(imax,ix,iy) = grid_variance(imax,ix,iy) + ( simu_grid_max(imax,ix,iy,isimu) - grid_mean_global(imax,ix,iy) )**2

            enddo

         enddo

      enddo

   enddo

!
!->only cpu0 world computes the variance -> mpi_reduce

   call mpi_reduce(grid_variance,grid_variance_global,nmax*ny*nx,MPI_REAL,MPI_SUM,ZERO_IXP,MPI_COMM_WORLD,ios)

   if (ig_myrank_world == ZERO_IXP) then

      do iy = ONE_IXP,ny

         do ix = ONE_IXP,nx

            do imax = ONE_IXP,nmax

               grid_variance_global(imax,ix,iy) = grid_variance_global(imax,ix,iy)/real(nsimu,kind=RXP)

            enddo

         enddo

      enddo

!
!---->write mean

      do imax = ONE_IXP,nmax

         fname = trim(prefix)//".snapshot.PGM"//trim(int2str(imax))//".mean.gpl"

         call write_gnuplot_matrix(fname,x,y,real(grid_mean_global(imax,:,:),kind=RXP))

      enddo

!
!---->write variance

      do imax = ONE_IXP,nmax

         fname = trim(prefix)//".snapshot.PGM"//trim(int2str(imax))//".variance.gpl"

         call write_gnuplot_matrix(fname,x,y,real(grid_variance_global(imax,:,:),kind=RXP))

      enddo

   endif

   call mpi_finalize(ios)

   stop 'after mean'

!   jsimu = ZERO_IXP
!
!   do isimu = isimu_start,isimu_end
!
!      jsimu = jsimu + ONE_IXP
!
!      do imax = 1_IXP,nmax
!
!         grid_sum_local
!
!      enddo
!
!   enddo

!
!
!***********************************************************************************************************************************************************************************
!->compute 2D FFT of structured grids
!***********************************************************************************************************************************************************************************

   if (ig_myrank_world == ZERO_IXP) print *,"Computing FFT + ACF"

   ios = init_array_real(gll_max,nx*ny,"snapshot_processing:gll_max")

   ios = init_array_complex(grid_fft,ny,nx,"snapshot_processing:grid_fft")

   ios = init_array_complex(grid_psd,ny,nx,"snapshot_processing:grid_psd")

   ios = init_array_real(grid_acf,ny,nx,"snapshot_processing:grid_asc")

   ios = init_array_real(grid_tmp,ny,nx,"snapshot_processing:grid_tmp")

!
!->compute FFT and ACF

   jsimu = ZERO_IXP

   do isimu = isimu_start,isimu_end

      jsimu = jsimu + ONE_IXP

      do imax = 1_IXP,nmax

         mean = sum(simu_grid_max(imax,:,:,jsimu))/real(size(simu_grid_max,2_IXP)*size(simu_grid_max,3_IXP),kind=RXP) !mean of the grid jsimu,imax

         var = variance(simu_grid_max(imax,:,:,jsimu)) !variance of the grid jsimu,imax

!
!------->PGM

         grid_fft(1_IXP:nx,1_IXP:ny) = cmplx(simu_grid_max(imax,1_IXP:nx,1_IXP:ny,jsimu)-mean,ZERO_RXP)

         fname = trim(simu_dir(isimu))//"/"//trim(prefix)//".snapshot.PGM"//trim(int2str(imax))//".gpl"

         call write_gnuplot_matrix(fname,x,y,real(grid_fft(1_IXP:nx,1_IXP:ny),kind=RXP))

!
!------->FFT

         call ft(grid_fft,-1_IXP)

        ! fname = trim(simu_dir(isimu))//"/"//trim(prefix)//".snapshot.FFT"//trim(int2str(imax))//".gpl"

        ! call write_gnuplot_matrix(fname,kx,ky,abs(grid_fft(:,:)))

!
!------->PSD

         call psd(grid_fft,grid_psd)

         grid_psd(:,:) = grid_psd(:,:)/real(size(grid_psd),kind=RXP) !normalize by number of grid points

        ! fname = trim(simu_dir(isimu))//"/"//trim(prefix)//".snapshot.PSD"//trim(int2str(imax))//".gpl"

        ! call write_gnuplot_matrix(fname,kx,ky,abs(grid_psd(:,:)))

!
!------->PSD FFT inverse -> ACF

         call ft(grid_psd,+1_IXP)

         grid_acf(1_IXP:nx,1_IXP:ny) = real(grid_psd(1_IXP:nx,1_IXP:ny))

        ! grid_tmp(1_IXP:nx,1_IXP:ny) = aimag(grid_psd(1_IXP:nx,1_IXP:ny))

         grid_acf(:,:) = grid_acf(:,:)/var !normalize by variance

         call ft_shift(grid_acf)

         fname = trim(simu_dir(isimu))//"/"//trim(prefix)//".snapshot.ACF"//trim(int2str(imax))//".gpl"

         call write_gnuplot_matrix(fname,x,y,grid_acf(:,:))

        ! fname = trim(simu_dir(isimu))//"/"//trim(prefix)//".snapshot.TMP"//trim(int2str(imax))//".gpl"

        ! call write_gnuplot_matrix(fname,x,y,grid_tmp(:,:))

        ! fname = trim(simu_dir(isimu))//"/"//trim(prefix)//".snapshot.ACF"//trim(int2str(imax))//".grd"

        ! call write_peak_ground_motion_gll(gll_max(:),fname)

!
!------>sanity check of the FFT

        ! call ft(grid_fft,+1_IXP)

        ! grid_fft(:,:) = grid_fft(:,:)/real(size(grid_fft),kind=RXP)

        ! fname = trim(simu_dir(isimu))//"/"//trim(prefix)//".snapshot.FFTINV"//trim(int2str(imax))//".gpl"

        ! call write_gnuplot_matrix(fname,kx(1_IXP:nx),ky(1_IXP:ny),real(grid_fft(1_IXP:nx,1_IXP:ny),kind=RXP))

      enddo

   enddo

   deallocate(gll_max)

   call mpi_finalize(ios)

   stop

end program snapshot_processing
