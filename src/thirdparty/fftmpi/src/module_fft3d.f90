! /* ----------------------------------------------------------------------
!    fftMPI - library for computing 3d/2d FFTs in parallel
!    http://fftmpi.sandia.gov, Sandia National Laboratories
!    Steve Plimpton, sjplimp@sandia.gov
!    Modified to fit EFISPEC3D purpose
!
!    Copyright 2018 National Technology & Engineering Solutions of
!    Sandia, LLC (NTESS). Under the terms of Contract DE-NA0003525 with
!    NTESS, the U.S. Government retains certain rights in this software.
!    This software is distributed under the modified Berkeley Software
!    Distribution (BSD) License.
!
!    See the README file in the top-level fftMPI directory.
! ------------------------------------------------------------------------- */

module mod_fft3d

   use iso_c_binding

   use mpi

   use mod_precision

   implicit none

   integer(kind=IXP) :: world
   integer(kind=IXP) :: me,nprocs
   
   integer(kind=IXP) :: nx,ny,nz
   integer(kind=IXP) :: inpx,inpy,inpz,outpx,outpy,outpz
   integer(kind=IXP) :: nloop
   integer(kind=IXP) :: mode,iflag,cflag,eflag,pflag,tflag,rflag,oflag,vflag
   integer(kind=IXP) :: seed,seedinit
   
   integer(kind=IXP) :: precision
   integer(kind=IXP) :: inxlo,inxhi,inylo,inyhi,inzlo,inzhi  ! initial partition of grid
   integer(kind=IXP) :: outxlo,outxhi,outylo,outyhi,outzlo,outzhi   ! final partition of grid
   integer(kind=IXP) :: nfft_in              ! # of grid pts i own in initial partition
   integer(kind=IXP) :: nfft_out             ! # of grid pts i own in final partition
   integer(kind=IXP) :: fftsize              ! fft buffer size returned by fft setup
   
   integer(kind=IXP) :: tuneflag,tuneper,tuneextra
   real   (kind=R64) ::  tunemax
   
   type(c_ptr)       :: fft
   real   (kind=R64) ::  timefft,timeinit,timesetup,timetune
   real   (kind=R64) :: epsmax
   
   integer(kind=IXP) :: zero = 0
   integer(kind=IXP) :: step = 1
   integer(kind=IXP) :: index = 2
   integer(kind=IXP) :: randominit = 3
   
   integer(kind=IXP) :: point = 0
   integer(kind=IXP) :: all2all = 1
   integer(kind=IXP) :: combo = 2
   
   integer(kind=IXP) :: pencil = 0
   integer(kind=IXP) :: brick = 1
   
   integer(kind=IXP) :: array = 0
   integer(kind=IXP) :: pointer = 1
   integer(kind=IXP) :: memcpy = 2
   
   integer(kind=IXP) :: in = 0
   integer(kind=IXP) :: out = 1
   
   integer(kind=IXP) :: ia = 16807
   integer(kind=IXP) :: im = 2147483647
   real(kind=R64)    :: am
   integer(kind=IXP) :: iq = 127773
   integer(kind=IXP) :: ir = 2836
   
#ifdef FFT_SINGLE
real(kind=R32), allocatable, target :: work(:)
#else
real(kind=R64), allocatable, target :: work(:)
#endif
   
   character(len=256) :: syntax

   public  :: proc_setup
   public  :: proc3d
   public  :: proc2d
   public  :: grid_setup
   public  :: plan
   public  :: allocate_mine
   public  :: initialize
   public  :: output
   public  :: validate
   public  :: deallocate_mine
   public  :: error_all
   public  :: random

   contains

!
!
! ---------------------------------------------------------------------
! partition processors across grid dimensions
! flag = in for input partitions, or out for output partitions
! if user set px,py,pz -> just return
! for in:
!   assign nprocs as bricks to 3d grid to minimize surface area per proc
!   derived from spparks domain::procs2domain_3d()
! for out:
!   assign nprocs as rectangles to xy grid to minimize surface area per proc
!   derived from spparks domain::procs2domain_2d()
!***********************************************************************************************************************************************************************************
   subroutine proc_setup(flag)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent(in) :: flag
      
      if (flag == 0) then
        if (inpx /= 0 .or. inpy /= 0 .or. inpz /= 0) return
        call proc3d(inpx,inpy,inpz)
      endif
      
      if (flag == 1) then
        if (outpx /= 0 .or. outpy /= 0 .or. outpz /= 0) return
        if (mode == 0 .or. mode == 2) call proc3d(outpx,outpy,outpz)
        if (mode == 1 .or. mode == 3) call proc2d(outpx,outpy,outpz)
      endif
   
!***********************************************************************************************************************************************************************************
   end subroutine proc_setup
!***********************************************************************************************************************************************************************************


!
!
!***********************************************************************************************************************************************************************************
   subroutine proc3d(px,py,pz)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP) :: px,py,pz
      integer(kind=IXP) :: ipx,ipy,ipz,nremain

      real(kind=R64) :: boxx,boxy,boxz,surf
      real(kind=R64) :: xprd,yprd,zprd,bestsurf
      
      xprd = nx
      yprd = ny
      zprd = nz
        
      bestsurf = 2.0 * (xprd*yprd + yprd*zprd + zprd*xprd)
        
      ! loop thru all possible factorizations of nprocs
      ! surf = surface area of a proc sub-domain
        
      ipx = 1
      do while (ipx <= nprocs)
        if (mod(nprocs,ipx) == 0) then
          nremain = nprocs/ipx
          ipy = 1
          do while (ipy <= nremain)
            if (mod(nremain,ipy) == 0) then
              ipz = nremain/ipy
              boxx = xprd/ipx
              boxy = yprd/ipy
              boxz = zprd/ipz
              surf = boxx*boxy + boxy*boxz + boxz*boxx
              if (surf < bestsurf) then
                bestsurf = surf
                px = ipx
                py = ipy
                pz = ipz
              endif
            endif
            ipy = ipy + 1
          enddo
        endif
        ipx = ipx + 1
      enddo
        
      if (px*py*pz /= nprocs) &
              call error_all("computed proc grid does not match nprocs")
      
!***********************************************************************************************************************************************************************************
   end subroutine proc3d
!***********************************************************************************************************************************************************************************

!***********************************************************************************************************************************************************************************
   subroutine proc2d(px,py,pz)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP) :: px,py,pz
      integer(kind=IXP) :: ipx,ipy
      real(kind=R64)    :: boxx,boxy,surf,xprd,yprd,bestsurf
      
      xprd = nx
      yprd = ny
        
      bestsurf = 2.0 * (xprd+yprd)
        
      ! loop thru all possible factorizations of nprocs
      ! surf = surface area of a proc sub-domain
        
      ipx = 1
      do while (ipx <= nprocs)
        if (mod(nprocs,ipx) == 0) then
          ipy = nprocs/ipx
          boxx = xprd/ipx
          boxy = yprd/ipy
          surf = boxx + boxy
          if (surf < bestsurf) then
            bestsurf = surf
            px = ipx
            py = ipy
          endif
        endif
        ipx = ipx + 1
      enddo
        
      pz = 1
      if (px*py*pz /= nprocs) &
              call error_all("computed proc grid does not match nprocs")
      
!***********************************************************************************************************************************************************************************
   end subroutine proc2d
!***********************************************************************************************************************************************************************************

! ---------------------------------------------------------------------
! partition fft grid
! once for input grid, once for output grid
! use px,py,pz for in/out
!***********************************************************************************************************************************************************************************
   subroutine grid_setup()
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP) :: ipx,ipy,ipz
      
      ! ipx,ipy,ipz = my position in input 3d grid of procs
      
      ipx = mod(me,inpx)
      ipy = mod(me/inpx,inpy)
      ipz = me / (inpx*inpy)
      
      ! nlo,nhi = lower/upper limits of the 3d brick i own
      
      inxlo = 1.0 * ipx * nx / inpx + 1
      inxhi = 1.0 * (ipx+1) * nx / inpx
      
      inylo = 1.0 * ipy * ny / inpy + 1
      inyhi = 1.0 * (ipy+1) * ny / inpy
      
      inzlo = 1.0 * ipz * nz / inpz + 1
      inzhi = 1.0 * (ipz+1) * nz / inpz
      
      nfft_in = (inxhi-inxlo+1) * (inyhi-inylo+1) * (inzhi-inzlo+1)
      
      ! ipx,ipy,ipz = my position in output 3d grid of procs
      
      ipx = mod(me,outpx)
      ipy = mod(me/outpx,outpy)
      ipz = me / (outpx*outpy)
      
      ! nlo,nhi = lower/upper limits of the 3d brick i own
      
      outxlo = 1.0 * ipx * nx / outpx + 1
      outxhi = 1.0 * (ipx+1) * nx / outpx
      
      outylo = 1.0 * ipy * ny / outpy + 1
      outyhi = 1.0 * (ipy+1) * ny / outpy
      
      outzlo = 1.0 * ipz * nz / outpz + 1
      outzhi = 1.0 * (ipz+1) * nz / outpz
      
      nfft_out = (outxhi-outxlo+1) * (outyhi-outylo+1) * (outzhi-outzlo+1)
   
!***********************************************************************************************************************************************************************************
   end subroutine grid_setup
!***********************************************************************************************************************************************************************************

! ---------------------------------------------------------------------
! create FFT plan
!***********************************************************************************************************************************************************************************
   subroutine plan()
!***********************************************************************************************************************************************************************************

      use fft3d_wrap

      implicit none

      integer(kind=IXP) :: permute,sendsize,recvsize,flag,ierr

      real(kind=R64) :: time1,time2
      
      call fft3d_create(world,precision,fft)
      call fft3d_set(fft,"remaponly",rflag)
      
      call fft3d_set(fft,"collective",cflag)
      call fft3d_set(fft,"exchange",eflag)
      call fft3d_set(fft,"pack",pflag)
      
      if (mode == 0 .or. mode == 2) then
        permute = 0
      else 
        permute = 2
      endif
      
      call MPI_Barrier(world,ierr)
      time1 = MPI_Wtime()
      
      ! will use fftsize to allocate work buffer
      ! ignore sendsize, recvsize b/c let fft allocate remap buffers internally
      ! set timesetup and timetune
      ! reset nloop if tuning and user nloop = 0
      
      if (tuneflag == 0) then
        call fft3d_setup(fft,nx,ny,nz, &
                inxlo,inxhi,inylo,inyhi,inzlo,inzhi, &
                outxlo,outxhi,outylo,outyhi,outzlo,outzhi, &
                permute,fftsize,sendsize,recvsize)
      else
        flag = 0
        if (mode >= 2) flag = 1
        call fft3d_tune(fft,nx,ny,nz, &
                inxlo,inxhi,inylo,inyhi,inzlo,inzhi, &
                outxlo,outxhi,outylo,outyhi,outzlo,outzhi, &
                permute,fftsize,sendsize,recvsize, &
                flag,tuneper,tunemax,tuneextra)
        IF (nloop == 0) nloop = fft3d_get_int(fft,"npertrial"//c_null_char)
      endif
      
      call MPI_Barrier(world,ierr)
      time2 = MPI_Wtime()
      
      if (tuneflag == 0) then
        timesetup = time2 - time1
        timetune = 0.0
      else
        timesetup = fft3d_get_double(fft,"setuptime"//c_null_char)
        timetune = time2 - time1
      endif
      
!***********************************************************************************************************************************************************************************
   end subroutine plan
!***********************************************************************************************************************************************************************************

! ---------------------------------------------------------------------
! allocate memory for fft grid
!***********************************************************************************************************************************************************************************
   subroutine allocate_mine()
!***********************************************************************************************************************************************************************************

      implicit none
      
      allocate(work(2*fftsize))
   
!***********************************************************************************************************************************************************************************
   end subroutine allocate_mine
!***********************************************************************************************************************************************************************************

! ---------------------------------------------------------------------
! must be called by all procs in world
!***********************************************************************************************************************************************************************************
   subroutine initialize()
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP) :: m
      integer(kind=IXP) :: ilocal,jlocal,klocal,iglobal,jglobal,kglobal
      integer(kind=IXP) :: nxlocal,nylocal
      
      if (iflag == zero) then
        do m = 1,2*nfft_in
          work(m) = 0.0
        enddo
      
      else if (iflag == step) then
        nxlocal = inxhi - inxlo + 1
        nylocal = inyhi - inylo + 1
      
        do m = 0,nfft_in-1
          ilocal = mod(m,nxlocal)
          jlocal = mod((m/nxlocal),nylocal)
          klocal = m / (nxlocal*nylocal)
          iglobal = inxlo + ilocal
          jglobal = inylo + jlocal
          kglobal = inzlo + klocal
          if (iglobal < nx/2 .and. jglobal < ny/2 .and. kglobal < nz/2) then
            work(2*m) = 1.0
          else 
            work(2*m) = 0.0
          endif
          work(2*m+1) = 0.0
        enddo
      
      else if (iflag == index) then
          nxlocal = inxhi - inxlo + 1;
          nylocal = inyhi - inylo + 1;
      
          do m = 0,nfft_in-1
            ilocal = mod(m,nxlocal)
            jlocal = mod((m/nxlocal),nylocal)
            klocal = m / (nxlocal*nylocal)
            iglobal = inxlo + ilocal
            jglobal = inylo + jlocal
            kglobal = inzlo + klocal
            work(2*m) = kglobal + jglobal + iglobal + 1
            work(2*m+1) = 0.0
          enddo
      
      else if (iflag == randominit) then
        do m = 1,2*nfft_in
          work(m) = random()
        enddo
      endif
      
!***********************************************************************************************************************************************************************************
   end subroutine initialize
!***********************************************************************************************************************************************************************************

! ---------------------------------------------------------------------
! output fft grid values
! flag = 0 for initial partition
! flag = 1 for final partition
!***********************************************************************************************************************************************************************************
   subroutine output(flag, str)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP) :: flag
      character (len=*) :: str
      integer(kind=IXP) :: iproc,m,tmp,ierr
      integer(kind=IXP) :: ilocal,jlocal,klocal,iglobal,jglobal,kglobal
      integer(kind=IXP) :: nxlocal,nylocal
      
      if (me == 0) print *,str
      
      do iproc = 0,nprocs-1
        if (me /= iproc) continue
        IF (me >= 1) CALL MPI_Recv(tmp,0,MPI_INT,me-1,0,world,MPI_STATUS_IGNORE,ierr)
      
        if (flag == 0) then
          nxlocal = inxhi - inxlo + 1
          nylocal = inyhi - inylo + 1
          
          do m = 0,nfft_in-1
            ilocal = mod(m,nxlocal)
            jlocal = mod((m/nxlocal),nylocal)
            klocal = m / (nxlocal*nylocal)
            iglobal = inxlo + ilocal
            jglobal = inylo + jlocal
            kglobal = inzlo + klocal
            print *,"Value (",iglobal,jglobal,kglobal,") on proc",me, &
                    "= (",work(2*m),work(2*m+1),")"
          enddo
        else
          nxlocal = outxhi - outxlo + 1
          nylocal = outyhi - outylo + 1
      
          do m = 0,nfft_in-1
            ilocal = mod(m,nxlocal)
            jlocal = mod((m/nxlocal),nylocal)
            klocal = m / (nxlocal*nylocal)
            iglobal = outxlo + ilocal
            jglobal = outylo + jlocal
            kglobal = outzlo + klocal
            print *,"Value (",iglobal,jglobal,kglobal,") on proc",me, &
                    "= (",work(2*m),work(2*m+1),")"
          enddo
        endif
      
        if (me < nprocs-1) call MPI_Send(tmp,0,MPI_INT,me+1,0,world,ierr)
      enddo
      
!***********************************************************************************************************************************************************************************
   end subroutine output
!***********************************************************************************************************************************************************************************

! ---------------------------------------------------------------------
! validation check for correct result
!***********************************************************************************************************************************************************************************
   subroutine validate()
!***********************************************************************************************************************************************************************************

      implicit none
      integer(kind=IXP) :: ilocal,jlocal,klocal,iglobal,jglobal,kglobal
      integer(kind=IXP) :: nxlocal,nylocal
      integer(kind=IXP) :: m,ierr
      real(kind=R64)    :: delta,epsilon,value,newvalue
      
      epsilon = 0.0
      
      if (iflag == zero) then
        do m = 0,2*nfft_in-1
          delta = ABS(work(m+1))
          if (delta > epsilon) epsilon = delta
        enddo
      
      else if (iflag == step) then
        nxlocal = inxhi - inxlo + 1
        nylocal = inyhi - inylo + 1
      
        do m = 0,nfft_in-1
          ilocal = mod(m,nxlocal)
          jlocal = mod((m/nxlocal),nylocal)
          klocal = m / (nxlocal*nylocal)
          iglobal = inxlo + ilocal
          jglobal = inylo + jlocal
          kglobal = inzlo + klocal
          if (iglobal < nx/2 .and. jglobal < ny/2 .and. kglobal < nz/2) then
            value = 1.0
          else
            value = 0.0
          endif
          delta = ABS(work(2*m+1)-VALUE)
          if (delta > epsilon) epsilon = delta
          delta = abs(work(2*m+2))
          if (delta > epsilon) epsilon = delta
        enddo
      
      else if (iflag == index) then
        nxlocal = inxhi - inxlo + 1
        nylocal = inyhi - inylo + 1
      
        do m = 0,nfft_in-1
          ilocal = mod(m,nxlocal)
          jlocal = mod((m/nxlocal),nylocal)
          klocal = m / (nxlocal*nylocal)
          iglobal = inxlo + ilocal
          jglobal = inylo + jlocal
          kglobal = inzlo + klocal
          value = kglobal+ jglobal + iglobal + 1
          delta = ABS(work(2*m+1)-VALUE)
          if (delta > epsilon) epsilon = delta
          delta = abs(work(2*m+2))
          if (delta > epsilon) epsilon = delta
        enddo
      
      else if (iflag == randominit) then
        seed = seedinit
        do m = 0,2*nfft_in-1
          newvalue = random()
          delta = ABS(work(m+1)-newvalue)
          if (delta > epsilon) epsilon = delta
        enddo
      endif
      
      call MPI_Allreduce(epsilon,epsmax,1,MPI_DOUBLE,MPI_MAX,world,ierr)
   
!***********************************************************************************************************************************************************************************
   end subroutine validate
!***********************************************************************************************************************************************************************************

! ---------------------------------------------------------------------
! deallocate memory for fft grid
!***********************************************************************************************************************************************************************************
   subroutine deallocate_mine()
!***********************************************************************************************************************************************************************************

      implicit none
      
      deallocate(work)
   
!***********************************************************************************************************************************************************************************
   end subroutine deallocate_mine
!***********************************************************************************************************************************************************************************

! ---------------------------------------------------------------------
! must be called by all procs in world
! shuts down MPI and exits
!***********************************************************************************************************************************************************************************
   subroutine error_all(str)
!***********************************************************************************************************************************************************************************

      implicit none

      character (len=*), intent(in) :: str

      integer(kind=IXP) :: ierr
      
      call MPI_Barrier(world,ierr)
      if (me == 0) print *,"ERROR: ",str
      call MPI_Finalize(ierr)
      call exit()
   
!***********************************************************************************************************************************************************************************
   end subroutine error_all
!***********************************************************************************************************************************************************************************

! ----------------------------------------------------------------------
! simple park rng
! pass in non-zero seed
!***********************************************************************************************************************************************************************************
   function random()
!***********************************************************************************************************************************************************************************

      implicit none
      integer(kind=IXP) :: k
      real(kind=R64)    :: ans,random
      
      k = seed/iq
      seed = ia*(seed-k*iq) - ir*k
      if (seed < 0) seed = seed + im
      ans = am*seed
      random = ans
      return
   
!***********************************************************************************************************************************************************************************
   end function random
!***********************************************************************************************************************************************************************************

end module mod_fft3d
