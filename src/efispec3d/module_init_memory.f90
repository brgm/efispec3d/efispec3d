!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to allocate arrays and to compute an approximation of the total RAM used by EFISPEC3D.

!>@brief
!!This module contains subroutines to allocate arrays and to compute an approximation of the total RAM used by EFISPEC3D.
module mod_init_memory

   use mod_precision

   use mod_global_variables, only : error_stop

   implicit none

   private

   public :: init_array_point3d
   public :: init_array_real_dp
   public :: init_array_real
   public :: init_array_int 
   public :: init_array_complex
   public :: init_array_logical
   public :: init_type_double_couple_source
   public :: memory_consumption
   public :: memory_deallocate_all_array

!>@brief
!!Interface init_array_point3d to redirect allocation to n-rank arrays for type 'point3d'.
   interface init_array_point3d

      module procedure init_array_rank0_point3d
      module procedure init_array_rank1_point3d
      module procedure init_array_rank2_point3d

   end interface init_array_point3d


!>@brief
!!Interface init_array_real_dp to redirect allocation to n-rank arrays.
   interface init_array_real_dp

      module procedure init_array_rank1_real_dp
      module procedure init_array_rank2_real_dp

   end interface init_array_real_dp

!>@brief
!!Interface init_array_real to redirect allocation to n-rank arrays.
   interface init_array_real

      module procedure init_array_rank1_real
      module procedure init_array_rank2_real
      module procedure init_array_rank3_real
      module procedure init_array_rank4_real
      module procedure init_array_rank5_real

   end interface init_array_real

!>@brief
!!Interface init_array_int to redirect allocation to n-rank arrays.
   interface init_array_int 

      module procedure init_array_rank1_int 
      module procedure init_array_rank1_int64 
      module procedure init_array_rank2_int 
      module procedure init_array_rank3_int 
      module procedure init_array_rank4_int 

   end interface init_array_int

!>@brief
!!Interface init_array_complex to redirect allocation to n-rank arrays.
   interface init_array_complex 

      module procedure init_array_rank1_complex
      module procedure init_array_rank2_complex
      module procedure init_array_rank3_complex

   end interface init_array_complex

!>@brief
!!Interface init_array_logical to redirect allocation to n-rank arrays.
   interface init_array_logical

      module procedure init_array_rank1_logical

   end interface init_array_logical

!>@brief
!!Interface init_type_double_couple_source to redirect allocation to n-rank arrays.
   interface init_type_double_couple_source

      module procedure init_type_double_couple_source_rank1

   end interface init_type_double_couple_source


   contains


!
!
!>@brief This subroutine allocates 'point3d' type array of rank 0
!>@param t     : point3d type variable to be initialized
!>@param tname : name of the variable
!***********************************************************************************************************************************************************************************
   function init_array_rank0_point3d(t,tname) result(err)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only : point3d

      implicit none

      type(point3d)    , intent(out) :: t
      character(len=*) , intent( in) :: tname

      integer(kind=IXP)              :: err
      integer(kind=IXP)              :: i

      t%x    = ZERO_RXP
      t%y    = ZERO_RXP
      t%z    = ZERO_RXP
      t%xi   = ZERO_RXP
      t%et   = ZERO_RXP
      t%ze   = ZERO_RXP
      t%dmin = ZERO_RXP
      t%cpu  = -ONE_IXP
      t%iel  = ZERO_IXP
      t%kgll = ZERO_IXP
      t%lgll = ZERO_IXP
      t%mgll = ZERO_IXP

      err = 0_IXP

      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank0_point3d
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates 'point3d' type array of rank 1
!>@param t     : point3d type array to be allocated
!>@param n1    : size of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank1_point3d(t,n1,tname) result(err)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only : point3d

      implicit none

      type(point3d)    , intent(out), allocatable, dimension(:) :: t
      integer(kind=IXP), intent( in)                            :: n1
      character(len=*) , intent( in)                            :: tname

      integer(kind=IXP)                                         :: err
      integer(kind=IXP)                                         :: i

      allocate(t(n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1

            t(i)%x    = ZERO_RXP
            t(i)%y    = ZERO_RXP
            t(i)%z    = ZERO_RXP
            t(i)%xi   = ZERO_RXP
            t(i)%et   = ZERO_RXP
            t(i)%ze   = ZERO_RXP
            t(i)%dmin = ZERO_RXP
            t(i)%cpu  = -ONE_IXP
            t(i)%iel  = ZERO_IXP
            t(i)%kgll = ZERO_IXP
            t(i)%lgll = ZERO_IXP
            t(i)%mgll = ZERO_IXP

         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank1_point3d
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates 'point3d' type array of rank 2
!>@param t     : point3d type array to be allocated
!>@param n1    : size of the dimension 1 of the array
!>@param n2    : size of the dimension 2 of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank2_point3d(t,n1,n2,tname) result(err)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only : point3d

      implicit none

      type(point3d)    , intent(out), allocatable, dimension(:,:) :: t
      integer(kind=IXP), intent( in)                              :: n1
      integer(kind=IXP), intent( in)                              :: n2
      character(len=*) , intent( in)                              :: tname

      integer(kind=IXP)                                           :: err
      integer(kind=IXP)                                           :: i
      integer(kind=IXP)                                           :: j

      allocate(t(n2,n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1

            do j = ONE_IXP,n2
          
               t(j,i)%x    = ZERO_RXP
               t(j,i)%y    = ZERO_RXP
               t(j,i)%z    = ZERO_RXP
               t(j,i)%xi   = ZERO_RXP
               t(j,i)%et   = ZERO_RXP
               t(j,i)%ze   = ZERO_RXP
               t(j,i)%dmin = ZERO_RXP
               t(j,i)%cpu  = -ONE_IXP
               t(j,i)%iel  = ZERO_IXP
               t(j,i)%kgll = ZERO_IXP
               t(j,i)%lgll = ZERO_IXP
               t(j,i)%mgll = ZERO_IXP
          
            enddo

         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank2_point3d
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates real-double-precision-value array of rank 1
!>@param t     : real-double-precision-value array to be allocated
!>@param n1    : size of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank1_real_dp(t,n1,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                            :: n1
      real   (kind=R64), intent(out), allocatable, dimension(:) :: t
      character(len=*) , intent( in)                            :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i

      allocate(t(n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1

            t(i) = ZERO_R64

         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank1_real_dp
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates real-double-precision-value array of rank 2
!>@param t  : real-double-precision-value array to be allocated --> t(n2,n1)
!>@param n1 : size of the dimension 1 of the array
!>@param n2 : size of the dimension 2 of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank2_real_dp(t,n1,n2,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                              :: n1
      integer(kind=IXP), intent( in)                              :: n2
      real   (kind=R64), intent(out), allocatable, dimension(:,:) :: t
      character(len=*) , intent( in)                              :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i
      integer(kind=IXP) :: j

      allocate(t(n2,n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            do j = ONE_IXP,n2
               t(j,i) = ZERO_R64
            enddo
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank2_real_dp
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates real-value array of rank 1
!>@param t     : real-value array to be allocated
!>@param n1    : size of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank1_real(t,n1,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                            :: n1
      real   (kind=RXP), intent(out), allocatable, dimension(:) :: t
      character(len=*) , intent( in)                            :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i

      allocate(t(n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            t(i) = ZERO_RXP
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank1_real
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine allocates real-value array of rank 2
!>@param t  : real-value array to be allocated --> t(n2,n1)
!>@param n1 : size of the dimension 1 of the array
!>@param n2 : size of the dimension 2 of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank2_real(t,n1,n2,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                              :: n1
      integer(kind=IXP), intent( in)                              :: n2
      real   (kind=RXP), intent(out), allocatable, dimension(:,:) :: t
      character(len=*) , intent( in)                              :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i
      integer(kind=IXP) :: j

      allocate(t(n2,n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            do j = ONE_IXP,n2
               t(j,i) = ZERO_RXP
            enddo
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank2_real
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine allocates real-value array of rank 3
!>@param t  : real-value array to be allocated --> t(n3,n2,n1)
!>@param n1 : size of the dimension 1 of the array
!>@param n2 : size of the dimension 2 of the array
!>@param n3 : size of the dimension 3 of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank3_real(t,n1,n2,n3,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                                :: n1
      integer(kind=IXP), intent( in)                                :: n2
      integer(kind=IXP), intent( in)                                :: n3
      real   (kind=RXP), intent(out), allocatable, dimension(:,:,:) :: t
      character(len=*) , intent( in)                                :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i
      integer(kind=IXP) :: j
      integer(kind=IXP) :: k

      allocate(t(n3,n2,n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            do j = ONE_IXP,n2
               do k = ONE_IXP,n3
                  t(k,j,i) = ZERO_RXP
               enddo
            enddo
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank3_real
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine allocates real-value array of order 4
!>@param t  : real-value array to be allocated
!>@param n1 : size of the dimension 1 of the array
!>@param n2 : size of the dimension 2 of the array
!>@param n3 : size of the dimension 3 of the array
!>@param n4 : size of the dimension 4 of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank4_real(t,n1,n2,n3,n4,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                                  :: n1
      integer(kind=IXP), intent( in)                                  :: n2
      integer(kind=IXP), intent( in)                                  :: n3
      integer(kind=IXP), intent( in)                                  :: n4
      real   (kind=RXP), intent(out), allocatable, dimension(:,:,:,:) :: t
      character(len=*) , intent(in)                                   :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i
      integer(kind=IXP) :: j
      integer(kind=IXP) :: k
      integer(kind=IXP) :: l

      allocate(t(n4,n3,n2,n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            do j = ONE_IXP,n2
               do k = ONE_IXP,n3
                  do l = ONE_IXP,n4
                     t(l,k,j,i) = ZERO_RXP
                  enddo
               enddo
            enddo
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank4_real
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine allocates real-value array of order 5
!>@param t  : real-value array to be allocated
!>@param n1 : size of the dimension 1 of the array
!>@param n2 : size of the dimension 2 of the array
!>@param n3 : size of the dimension 3 of the array
!>@param n4 : size of the dimension 4 of the array
!>@param n5 : size of the dimension 5 of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank5_real(t,n1,n2,n3,n4,n5,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                                    :: n1
      integer(kind=IXP), intent( in)                                    :: n2
      integer(kind=IXP), intent( in)                                    :: n3
      integer(kind=IXP), intent( in)                                    :: n4
      integer(kind=IXP), intent( in)                                    :: n5
      real   (kind=RXP), intent(out), allocatable, dimension(:,:,:,:,:) :: t
      character(len=*) , intent(in)                                     :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i
      integer(kind=IXP) :: j
      integer(kind=IXP) :: k
      integer(kind=IXP) :: l
      integer(kind=IXP) :: m

      allocate(t(n5,n4,n3,n2,n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            do j = ONE_IXP,n2
               do k = ONE_IXP,n3
                  do l = ONE_IXP,n4
                     do m = ONE_IXP,n5
                        t(m,l,k,j,i) = ZERO_RXP
                     enddo
                  enddo
               enddo
            enddo
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank5_real
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine allocates integer-value array of rank 1
!>@param t     : integer-value array to be allocated
!>@param n1    : size of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank1_int(t,n1,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                            :: n1
      integer(kind=IXP), intent(out), allocatable, dimension(:) :: t
      character(len=*) , intent( in)                            :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i

      allocate(t(n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            t(i) = ZERO_IXP
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank1_int
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates 64 bits integer-value array of rank 1
!>@param t     : integer-value array to be allocated
!>@param n1    : size of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank1_int64(t,n1,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                            :: n1
      integer(kind=I64), intent(out), allocatable, dimension(:) :: t
      character(len=*) , intent( in)                            :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i

      allocate(t(n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            t(i) = ZERO_I64
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank1_int64
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates integer-value array of rank 2
!>@param t     : integer-value array to be allocated
!>@param n1    : size of dimension 1 of the array
!>@param n2    : size of dimension 2 of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank2_int(t,n1,n2,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                              :: n1
      integer(kind=IXP), intent( in)                              :: n2
      integer(kind=IXP), intent(out), allocatable, dimension(:,:) :: t
      character(len=*) , intent( in)                              :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i
      integer(kind=IXP) :: j

      allocate(t(n2,n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            do j = ONE_IXP,n2
               t(j,i) = ZERO_IXP
            enddo
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank2_int
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine allocates int-value array of rank 3
!>@param t  : int-value array to be allocated --> t(n3,n2,n1)
!>@param n1 : size of the dimension 1 of the array
!>@param n2 : size of the dimension 2 of the array
!>@param n3 : size of the dimension 3 of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank3_int(t,n1,n2,n3,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                                :: n1
      integer(kind=IXP), intent( in)                                :: n2
      integer(kind=IXP), intent( in)                                :: n3
      integer(kind=IXP), intent(out), allocatable, dimension(:,:,:) :: t
      character(len=*) , intent( in)                                :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i
      integer(kind=IXP) :: j
      integer(kind=IXP) :: k

      allocate(t(n3,n2,n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            do j = ONE_IXP,n2
               do k = ONE_IXP,n3
                  t(k,j,i) = ZERO_IXP
               enddo
            enddo
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank3_int
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine allocates int-value array of order 4
!>@param t  : int-value array to be allocated
!>@param n1 : size of the dimension 1 of the array
!>@param n2 : size of the dimension 2 of the array
!>@param n3 : size of the dimension 3 of the array
!>@param n4 : size of the dimension 4 of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank4_int(t,n1,n2,n3,n4,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                                  :: n1
      integer(kind=IXP), intent( in)                                  :: n2
      integer(kind=IXP), intent( in)                                  :: n3
      integer(kind=IXP), intent( in)                                  :: n4
      integer(kind=IXP), intent(out), allocatable, dimension(:,:,:,:) :: t
      character(len=*) , intent(in)                                   :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i
      integer(kind=IXP) :: j
      integer(kind=IXP) :: k
      integer(kind=IXP) :: l

      allocate(t(n4,n3,n2,n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            do j = ONE_IXP,n2
               do k = ONE_IXP,n3
                  do l = ONE_IXP,n4
                     t(l,k,j,i) = ZERO_IXP
                  enddo
               enddo
            enddo
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank4_int
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates complex-value array of rank 1
!>@param t     : complex-value array to be allocated
!>@param n1    : size of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank1_complex(t,n1,tname) result(err)
!***********************************************************************************************************************************************************************************


      implicit none

      integer(kind=IXP), intent( in)                            :: n1
      complex(kind=RXP), intent(out), allocatable, dimension(:) :: t
      character(len=*),  intent( in)                            :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i

      allocate(t(n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            t(i) = cmplx(ZERO_RXP,ZERO_RXP)
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank1_complex
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates complex-value array of rank 2
!>@param t     : complex-value array to be allocated
!>@param n1    : size of the array
!>@param n2    : size of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank2_complex(t,n1,n2,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                              :: n1
      integer(kind=IXP), intent( in)                              :: n2
      complex(kind=RXP), intent(out), allocatable, dimension(:,:) :: t
      character(len=*),  intent( in)                              :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i
      integer(kind=IXP) :: j

      allocate(t(n2,n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            do j = ONE_IXP,n2
               t(j,i) = cmplx(ZERO_RXP,ZERO_RXP)
            enddo
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank2_complex
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates complex-value array of rank 3
!>@param t     : complex-value array to be allocated
!>@param n1    : size of the array
!>@param n2    : size of the array
!>@param n3    : size of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank3_complex(t,n1,n2,n3,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                                :: n1
      integer(kind=IXP), intent( in)                                :: n2
      integer(kind=IXP), intent( in)                                :: n3
      complex(kind=RXP), intent(out), allocatable, dimension(:,:,:) :: t
      character(len=*) , intent( in)                                :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i
      integer(kind=IXP) :: j
      integer(kind=IXP) :: k

      allocate(t(n3,n2,n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            do j = ONE_IXP,n2
               do k = ONE_IXP,n3
                  t(k,j,i) = cmplx(ZERO_RXP,ZERO_RXP)
               enddo
            enddo
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank3_complex
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates logical-value array of rank 1
!>@param t     : logical-value array to be allocated
!>@param n1    : size of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_array_rank1_logical(t,n1,tname) result(err)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)                            :: n1
      logical(kind=IXP), intent(out), allocatable, dimension(:) :: t
      character(len=*) , intent( in)                            :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i

      allocate(t(n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1
            t(i) = .false.
         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_array_rank1_logical
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine allocates type_double_couple_source array of rank 1
!>@param t     : type_double_couple_source array to be allocated
!>@param n1    : size of the array
!>@param tname : name of the array
!***********************************************************************************************************************************************************************************
   function init_type_double_couple_source_rank1(t,n1,tname) result(err)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only : type_double_couple_source

      implicit none

      integer(kind=IXP)              , intent( in)                            :: n1
      type(type_double_couple_source), intent(out), allocatable, dimension(:) :: t
      character(len=*)               , intent( in)                            :: tname

      integer(kind=IXP) :: err
      integer(kind=IXP) :: i

      allocate(t(n1),stat=err)

      if (err /= ZERO_IXP) then

         call error_stop("error while allocating array "//trim(adjustl(tname)))

      else

         do i = ONE_IXP,n1

            t(i)%p%x        = ZERO_RXP
            t(i)%p%y        = ZERO_RXP
            t(i)%p%z        = ZERO_RXP
            t(i)%mxx        = ZERO_RXP
            t(i)%myy        = ZERO_RXP
            t(i)%mzz        = ZERO_RXP
            t(i)%mxy        = ZERO_RXP
            t(i)%mxz        = ZERO_RXP
            t(i)%myz        = ZERO_RXP
            t(i)%shift_time = ZERO_RXP
            t(i)%rise_time  = ZERO_RXP
            t(i)%p%xi       = ZERO_RXP
            t(i)%p%et       = ZERO_RXP
            t(i)%p%ze       = ZERO_RXP
            t(i)%p%dmin     = ZERO_RXP
            t(i)%str        = ZERO_RXP
            t(i)%dip        = ZERO_RXP
            t(i)%rak        = ZERO_RXP
            t(i)%mw         = ZERO_RXP
            t(i)%slip       = ZERO_RXP
            t(i)%mu         = ZERO_RXP
            t(i)%icur       = ZERO_IXP
            t(i)%p%cpu      = -ONE_IXP
            t(i)%p%iel      = ZERO_IXP
            t(i)%p%kgll     = ZERO_IXP
            t(i)%p%lgll     = ZERO_IXP
            t(i)%p%mgll     = ZERO_IXP
            t(i)%rglo       = ZERO_IXP

         enddo
         
      endif
 
      return
!***********************************************************************************************************************************************************************************
   end function init_type_double_couple_source_rank1
!***********************************************************************************************************************************************************************************




!
!
!>@brief Subroutine to compute an approximation of total RAM used by global variables of EFISPEC3D.
!!See module mod_global_variables
!>@return approximation of total RAM used by global variables in listing file *.lst
!***********************************************************************************************************************************************************************************
   subroutine memory_consumption()
!***********************************************************************************************************************************************************************************
   
      use mpi

      use mod_global_variables
      
      implicit none
      
      real   (kind=RXP), parameter :: MO  = (1024.0_RXP)**TWO_IXP
      real   (kind=RXP)            :: total_memory
      real   (kind=RXP)            :: total_memory_all_cpu
      real   (kind=RXP)            :: size_cpu_world(ig_ncpu)
      integer(kind=IXP)            :: ios
      integer(kind=IXP)            :: i
      integer(kind=IXP)            :: j
      
      total_memory         = ZERO_RXP
      total_memory_all_cpu = ZERO_RXP

      total_memory = total_memory + real(sizeof(LG_VISCO),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(IG_NRELAX),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_OUTPUT_MEDIUM_VTK),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_SNAPSHOT_VTK),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_SNAPSHOT_GMT),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_SNAPSHOT_SURF_GLL),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_SNAPSHOT_SURF_SPA_DER),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_FIR_FILTER),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(IG_FIR_DECIMATION_FAC),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_SNAPSHOT_SURF_GLL_FILTER),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_LUSTRE_FILE_SYS),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(IG_MPI_WRITE_TYPE),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_PLANE_WAVE),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(IG_LAGRANGE_ORDER),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(IG_NGLL),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(IG_LST_UNIT),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(IG_NDOF),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(RG_NEWMARK_GAMMA),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(PI_RXP),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ZERO_RXP),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(EPSILON_MACHINE_RXP),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(TINY_REAL_RXP),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_ASYNC_MPI_COMM),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_OUTPUT_CPUTIME),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(LG_OUTPUT_DEBUG_FILE),kind=RXP)/MO
      
      total_memory = total_memory + real(sizeof(rg_hexa_gll_jacobian_det),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_dxidx),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_dxidy),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_dxidz),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_detdx),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_detdy),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_detdz),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_dzedx),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_dzedy),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_dzedz),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_rho),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_rhovs2),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_rhovp2),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(rg_quadp_gll_rhovs),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_quadp_gll_rhovp),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_quadp_gll_jaco_det),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_quadp_gll_normal),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(rg_quadf_disp_spatial_deriv),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(rg_hexa_gll_wkqs),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_wkqp),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_ksixx),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_ksiyy),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_ksizz),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_ksixy),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_ksixz),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_hexa_gll_ksiyz),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(RG_RELAX_COEFF),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_mem_var_exp),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_simu_times),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gll_displacement),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gll_velocity),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gll_acceleration),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gll_acctmp),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gnode_abscissa),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gnode_abscissa_dist),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gll_mass_matrix),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gnode_x),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gnode_y),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gnode_z),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_receiver_snapshot_z),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_receiver_snapshot_locnum),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_dcsource_user_func),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_sfsource_user_func),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_mpi_buffer_send),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_mpi_buffer_recv),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_dt),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_dt2),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_simu_current_time),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_simu_total_time),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_mesh_xmax),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_mesh_xmin),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_mesh_ymax),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_mesh_ymin),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_mesh_zmax),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_mesh_zmin),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_receiver_snapshot_dxdy),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_receiver_snapshot_dx),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_receiver_snapshot_dy),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_receiver_snapshot_nx),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_receiver_snapshot_ny),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_hexa_gll_glonum),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quadp_gll_glonum),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quadf_gll_glonum),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_gll_order_by_quadf),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_hexa_gnode_glonum),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quadp_gnode_glonum),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quadf_gnode_glonum),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_hexa_gnode_xiloc),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_hexa_gnode_etloc),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_hexa_gnode_zeloc),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quad_gnode_xiloc),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quad_gnode_etloc),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(ig_hexa_receiver_unit),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quad_receiver_unit),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_unit_snap_quad_gll_dis),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_unit_snap_quadf_dis_spa_der),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(ig_mpi_buffer_sizemax),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_mpi_nboctet_real),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_mpi_nboctet_int),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_pos_cpu_snap_surf_gll),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_mpi_request_send),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_mpi_request_recv),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_mpi_buffer_offset),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_hexa_material_number),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_receiver_snapshot_glonum),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_receiver_snapshot_mpi_shift),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_receiver_snapshot_total_number),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_ncpu_world),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_myrank_world),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_ncpu),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_myrank),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_ncpu_neighbor),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_cpu_neighbor_info),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nhexa),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nhexa_outer),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nhexa_inner),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nquad_parax),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nquad_fsurf),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nquad_fsurf_saved),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nquad_fsurf_all_cpu),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_mesh_nnode),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_hexa_nnode),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quad_nnode),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_line_nnode),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_hexa_node2gll),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_hexa_face2mid_gll),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_hexa_face_node),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_ndt),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_idt),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_receiver_saving_incr),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_snapshot_saving_incr),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_snapshot_volume_saving_incr),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_snapshot_surf_gll_nsave),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_boundary_absorption),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_ndcsource),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nsfsource),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nfault),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nreceiver_hexa),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nreceiver_quad),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nreceiver_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_ngll_order_by_quadf),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nmaterial),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nlayer),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_ninterface),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_layer_gradient),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_layer_random),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_layer_userdefined),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quadp_neighbor_hexa),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quadp_neighbor_hexaface),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quadf_neighbor_hexa),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_quadf_neighbor_hexaface),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nneighbor_all_kind),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_cpu_name_len),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(cg_cpu_name),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(cg_prefix),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(cg_myrank_world),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(cg_myrank),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(cg_ncpu_world),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(cg_ncpu),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_snapshot_displacement),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_snapshot_velocity),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_snapshot_acceleration),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_snapshot_volume),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_snapshot_volume_displacement),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_snapshot_volume_velocity),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(lg_snapshot_volume_acceleration),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(rg_gll_coordinate),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gll_weight),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gll_abscissa),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gll_lagrange_deriv),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_gll_abscissa_dist),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_ngll_total),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(ig_hexa_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_vtk_hexa_conn_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_vtk_node_gll_glonum_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_vtk_node_x_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_vtk_node_y_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_vtk_node_z_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_vtk_cell_type_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_vtk_offset_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_vtk_nhexa_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_vtk_nnode_snapshot),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_snapshot_volume_xmin),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_snapshot_volume_xmax),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_snapshot_volume_ymin),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_snapshot_volume_ymax),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_snapshot_volume_zmin),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_snapshot_volume_zmax),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(rg_fir_filter_cutoff_freq),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_fir_filter_transition_band),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_fir_filter_attenuation),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_fir_dt),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_fir_filter_order),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_fir_filter),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_fir_filter_buffer_dis),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_fir_filter_buffer_vel),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_fir_filter_buffer_acc),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_fir_filter_buffer_dis_spa_der),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(rg_plane_wave_z),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_plane_wave_std),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_plane_wave_dir),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_plane_wave_ngll),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_plane_wave_gll),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_plane_wave_nquadp_bottom),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_plane_wave_quadp_bottom),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_plane_wave_nquadp_side),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_plane_wave_quadp_side),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(rg_plane_wave_sft_user),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(ig_mpi_comm_fsurf),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_ncpu_fsurf),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_myrank_fsurf),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_type_vector_snap_fsurf),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(ig_uq_simu),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_uq_ndim),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_mpi_comm_simu),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_simu),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(ig_nsimu),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(cg_simu),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(cg_nsimu),kind=RXP)/MO
      total_memory = total_memory + real(sizeof(cg_simu_dir),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(tg_sfsource),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(tg_dcsource),kind=RXP)/MO
      if (allocated(tg_dcsource)) then
         do i = ONE_IXP,size(tg_dcsource)
            total_memory = total_memory + real(sizeof(tg_dcsource(i)%gll_force),kind=RXP)/MO
            total_memory = total_memory + real(sizeof(tg_dcsource(i)%stf),kind=RXP)/MO
         enddo
      endif

      total_memory = total_memory + real(sizeof(tg_fault),kind=RXP)/MO
      if (allocated(tg_fault)) then
         do i = ONE_IXP,size(tg_fault)
            total_memory = total_memory + real(sizeof(tg_fault(i)%rf),kind=RXP)/MO
            total_memory = total_memory + real(sizeof(tg_fault(i)%p),kind=RXP)/MO
            total_memory = total_memory + real(sizeof(tg_fault(i)%dcsource),kind=RXP)/MO
            do j = ONE_IXP,size(tg_fault(i)%dcsource)
               total_memory = total_memory + real(sizeof(tg_fault(i)%dcsource(j)%gll_force),kind=RXP)/MO
               total_memory = total_memory + real(sizeof(tg_fault(i)%dcsource(j)%stf),kind=RXP)/MO
            enddo
         enddo 
      endif

      total_memory = total_memory + real(sizeof(tg_receiver_hexa),kind=RXP)/MO
      if (allocated(tg_receiver_hexa)) then
         do i = ONE_IXP,size(tg_receiver_hexa)
            total_memory = total_memory + real(sizeof(tg_receiver_hexa(i)%lag),kind=RXP)/MO
            total_memory = total_memory + real(sizeof(tg_receiver_hexa(i)%gll),kind=RXP)/MO
         enddo
      endif

      total_memory = total_memory + real(sizeof(tg_receiver_quad),kind=RXP)/MO
      if (allocated(tg_receiver_quad)) then
         do i = ONE_IXP,size(tg_receiver_quad)
            total_memory = total_memory + real(sizeof(tg_receiver_quad(i)%lag),kind=RXP)/MO
            total_memory = total_memory + real(sizeof(tg_receiver_quad(i)%gll),kind=RXP)/MO
         enddo
      endif

      total_memory = total_memory + real(sizeof(tg_receiver_snapshot_quad),kind=RXP)/MO
      if (allocated(tg_receiver_snapshot_quad)) then
         do i = ONE_IXP,size(tg_receiver_snapshot_quad)
            total_memory = total_memory + real(sizeof(tg_receiver_snapshot_quad(i)%lag),kind=RXP)/MO
            total_memory = total_memory + real(sizeof(tg_receiver_snapshot_quad(i)%gll),kind=RXP)/MO
         enddo
      endif

      total_memory = total_memory + real(sizeof(tg_cpu_neighbor),kind=RXP)/MO
      if (allocated(tg_cpu_neighbor)) then
         do i = ONE_IXP,size(tg_cpu_neighbor)
            total_memory = total_memory + real(sizeof(tg_cpu_neighbor(i)%gll_send),kind=RXP)/MO
            total_memory = total_memory + real(sizeof(tg_cpu_neighbor(i)%gll_recv),kind=RXP)/MO
         enddo
      endif

      total_memory = total_memory + real(sizeof(tg_elastic_material),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(tg_anelastic_material),kind=RXP)/MO
      if (allocated(tg_anelastic_material)) then
         do i = ONE_IXP,size(tg_anelastic_material)
            total_memory = total_memory + real(sizeof(tg_anelastic_material(i)%wkqs),kind=RXP)/MO
            total_memory = total_memory + real(sizeof(tg_anelastic_material(i)%wkqp),kind=RXP)/MO
         enddo
      endif

      total_memory = total_memory + real(sizeof(tg_random_material),kind=RXP)/MO
      if (allocated(tg_random_material)) then
         do i = ONE_IXP,size(tg_random_material)
            total_memory = total_memory + real(sizeof(tg_random_material(i)%rd),kind=RXP)/MO
         enddo
      endif

      total_memory = total_memory + real(sizeof(cg_struture_name),kind=RXP)/MO

      total_memory = total_memory + real(sizeof(tg_layer),kind=RXP)/MO
      if (allocated(tg_layer)) then
         do i = ONE_IXP,size(tg_layer)
            total_memory = total_memory + real(sizeof(tg_layer(i)%modes_ind),kind=RXP)/MO
            total_memory = total_memory + real(sizeof(tg_layer(i)%elastic),kind=RXP)/MO
            total_memory = total_memory + real(sizeof(tg_layer(i)%anelastic),kind=RXP)/MO
            total_memory = total_memory + real(sizeof(tg_layer(i)%random),kind=RXP)/MO
         enddo
      endif

!
!---->cpu 0 gather all info

      call mpi_gather(total_memory,ONE_IXP,MPI_REAL,size_cpu_world,ONE_IXP,MPI_REAL,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!---->cpu 0 write info in *.lst

      if (ig_myrank == ZERO_IXP) then
      
         write(IG_LST_UNIT,'(" ",/,a)') "memory consumption approximation"
      
         do i = ONE_IXP,ig_ncpu
            write(IG_LST_UNIT,'(a,i8,a,f15.3,a)') "cpu ",i-ONE_IXP," : ",size_cpu_world(i)," Mo"
            total_memory_all_cpu = total_memory_all_cpu + size_cpu_world(i)
         enddo
      
         write(IG_LST_UNIT,'(a,9x,f15.3,a)') "Total ",total_memory_all_cpu," Mo"
      
      endif
      
      return

!***********************************************************************************************************************************************************************************
   end subroutine memory_consumption
!***********************************************************************************************************************************************************************************


!
!
!>@brief Subroutine to deallocate all arrays before post-processing files written on disk
!***********************************************************************************************************************************************************************************
   subroutine memory_deallocate_all_array()
!***********************************************************************************************************************************************************************************
   
      use mod_global_variables
      
      implicit none
      
      integer(kind=IXP) :: ios
      integer(kind=IXP) :: ier

      ier = ZERO_IXP

      if (allocated(rg_hexa_gll_jacobian_det))          deallocate(rg_hexa_gll_jacobian_det,stat=ios)         ; ier = ier + ios
      if (allocated(rg_hexa_gll_dxidx))                 deallocate(rg_hexa_gll_dxidx,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_dxidy))                 deallocate(rg_hexa_gll_dxidy,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_dxidz))                 deallocate(rg_hexa_gll_dxidz,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_detdx))                 deallocate(rg_hexa_gll_detdx,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_detdy))                 deallocate(rg_hexa_gll_detdy,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_detdz))                 deallocate(rg_hexa_gll_detdz,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_dzedx))                 deallocate(rg_hexa_gll_dzedx,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_dzedy))                 deallocate(rg_hexa_gll_dzedy,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_dzedz))                 deallocate(rg_hexa_gll_dzedz,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_rho))                   deallocate(rg_hexa_gll_rho,stat=ios)                  ; ier = ier + ios
      if (allocated(rg_hexa_gll_rhovs2))                deallocate(rg_hexa_gll_rhovs2,stat=ios)               ; ier = ier + ios
      if (allocated(rg_hexa_gll_rhovp2))                deallocate(rg_hexa_gll_rhovp2,stat=ios)               ; ier = ier + ios
      if (allocated(rg_quadp_gll_rhovs))                deallocate(rg_quadp_gll_rhovs,stat=ios)               ; ier = ier + ios
      if (allocated(rg_quadp_gll_rhovp))                deallocate(rg_quadp_gll_rhovp,stat=ios)               ; ier = ier + ios
      if (allocated(rg_quadp_gll_jaco_det))             deallocate(rg_quadp_gll_jaco_det,stat=ios)            ; ier = ier + ios
      if (allocated(rg_quadp_gll_normal))               deallocate(rg_quadp_gll_normal,stat=ios)              ; ier = ier + ios
      if (allocated(rg_quadf_disp_spatial_deriv))       deallocate(rg_quadf_disp_spatial_deriv,stat=ios)      ; ier = ier + ios
      if (allocated(rg_hexa_gll_wkqs))                  deallocate(rg_hexa_gll_wkqs,stat=ios)                 ; ier = ier + ios
      if (allocated(rg_hexa_gll_wkqp))                  deallocate(rg_hexa_gll_wkqp,stat=ios)                 ; ier = ier + ios
      if (allocated(rg_hexa_gll_ksixx))                 deallocate(rg_hexa_gll_ksixx,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_ksiyy))                 deallocate(rg_hexa_gll_ksiyy,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_ksizz))                 deallocate(rg_hexa_gll_ksizz,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_ksixy))                 deallocate(rg_hexa_gll_ksixy,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_ksixz))                 deallocate(rg_hexa_gll_ksixz,stat=ios)                ; ier = ier + ios
      if (allocated(rg_hexa_gll_ksiyz))                 deallocate(rg_hexa_gll_ksiyz,stat=ios)                ; ier = ier + ios
      if (allocated(rg_gll_displacement))               deallocate(rg_gll_displacement,stat=ios)              ; ier = ier + ios
      if (allocated(rg_gll_velocity))                   deallocate(rg_gll_velocity,stat=ios)                  ; ier = ier + ios
      if (allocated(rg_gll_acceleration))               deallocate(rg_gll_acceleration,stat=ios)              ; ier = ier + ios
      if (allocated(rg_gll_acctmp))                     deallocate(rg_gll_acctmp,stat=ios)                    ; ier = ier + ios
      if (allocated(rg_gnode_abscissa))                 deallocate(rg_gnode_abscissa,stat=ios)                ; ier = ier + ios
      if (allocated(rg_gnode_abscissa_dist))            deallocate(rg_gnode_abscissa_dist,stat=ios)           ; ier = ier + ios
      if (allocated(rg_gll_mass_matrix))                deallocate(rg_gll_mass_matrix,stat=ios)               ; ier = ier + ios
      if (allocated(rg_gnode_x))                        deallocate(rg_gnode_x,stat=ios)                       ; ier = ier + ios
      if (allocated(rg_gnode_y))                        deallocate(rg_gnode_y,stat=ios)                       ; ier = ier + ios
      if (allocated(rg_gnode_z))                        deallocate(rg_gnode_z,stat=ios)                       ; ier = ier + ios
      if (allocated(rg_receiver_snapshot_z))            deallocate(rg_receiver_snapshot_z,stat=ios)           ; ier = ier + ios
      if (allocated(ig_receiver_snapshot_locnum))       deallocate(ig_receiver_snapshot_locnum,stat=ios)      ; ier = ier + ios
      if (allocated(rg_dcsource_user_func))             deallocate(rg_dcsource_user_func,stat=ios)            ; ier = ier + ios
      if (allocated(rg_sfsource_user_func))             deallocate(rg_sfsource_user_func,stat=ios)            ; ier = ier + ios
      if (allocated(rg_mpi_buffer_send))                deallocate(rg_mpi_buffer_send,stat=ios)               ; ier = ier + ios
      if (allocated(rg_mpi_buffer_recv))                deallocate(rg_mpi_buffer_recv,stat=ios)               ; ier = ier + ios
      if (allocated(ig_hexa_gll_glonum))                deallocate(ig_hexa_gll_glonum,stat=ios)               ; ier = ier + ios
      if (allocated(ig_quadp_gll_glonum))               deallocate(ig_quadp_gll_glonum,stat=ios)              ; ier = ier + ios
      if (allocated(ig_quadf_gll_glonum))               deallocate(ig_quadf_gll_glonum,stat=ios)              ; ier = ier + ios
      if (allocated(ig_gll_order_by_quadf))             deallocate(ig_gll_order_by_quadf,stat=ios)            ; ier = ier + ios
      if (allocated(ig_hexa_gnode_glonum))              deallocate(ig_hexa_gnode_glonum,stat=ios)             ; ier = ier + ios
      if (allocated(ig_quadp_gnode_glonum))             deallocate(ig_quadp_gnode_glonum,stat=ios)            ; ier = ier + ios
      if (allocated(ig_quadf_gnode_glonum))             deallocate(ig_quadf_gnode_glonum,stat=ios)            ; ier = ier + ios
      if (allocated(ig_hexa_gnode_xiloc))               deallocate(ig_hexa_gnode_xiloc,stat=ios)              ; ier = ier + ios
      if (allocated(ig_hexa_gnode_etloc))               deallocate(ig_hexa_gnode_etloc,stat=ios)              ; ier = ier + ios
      if (allocated(ig_hexa_gnode_zeloc))               deallocate(ig_hexa_gnode_zeloc,stat=ios)              ; ier = ier + ios
      if (allocated(ig_quad_gnode_xiloc))               deallocate(ig_quad_gnode_xiloc,stat=ios)              ; ier = ier + ios
      if (allocated(ig_quad_gnode_etloc))               deallocate(ig_quad_gnode_etloc,stat=ios)              ; ier = ier + ios
      if (allocated(ig_hexa_receiver_unit))             deallocate(ig_hexa_receiver_unit,stat=ios)            ; ier = ier + ios
      if (allocated(ig_quad_receiver_unit))             deallocate(ig_quad_receiver_unit,stat=ios)            ; ier = ier + ios
      if (allocated(ig_mpi_request_send))               deallocate(ig_mpi_request_send,stat=ios)              ; ier = ier + ios
      if (allocated(ig_mpi_request_recv))               deallocate(ig_mpi_request_recv,stat=ios)              ; ier = ier + ios
      if (allocated(ig_mpi_buffer_offset ))             deallocate(ig_mpi_buffer_offset ,stat=ios)            ; ier = ier + ios
      if (allocated(ig_hexa_material_number))           deallocate(ig_hexa_material_number,stat=ios)          ; ier = ier + ios
      if (allocated(ig_receiver_snapshot_glonum))       deallocate(ig_receiver_snapshot_glonum,stat=ios)      ; ier = ier + ios
      if (allocated(ig_receiver_snapshot_mpi_shift))    deallocate(ig_receiver_snapshot_mpi_shift,stat=ios)   ; ier = ier + ios
      if (allocated(ig_receiver_snapshot_total_number)) deallocate(ig_receiver_snapshot_total_number,stat=ios); ier = ier + ios
      if (allocated(ig_cpu_neighbor_info))              deallocate(ig_cpu_neighbor_info,stat=ios)             ; ier = ier + ios
      if (allocated(ig_quadp_neighbor_hexa))            deallocate(ig_quadp_neighbor_hexa,stat=ios)           ; ier = ier + ios
      if (allocated(ig_quadp_neighbor_hexaface))        deallocate(ig_quadp_neighbor_hexaface,stat=ios)       ; ier = ier + ios
      if (allocated(ig_quadf_neighbor_hexa))            deallocate(ig_quadf_neighbor_hexa,stat=ios)           ; ier = ier + ios
      if (allocated(ig_quadf_neighbor_hexaface))        deallocate(ig_quadf_neighbor_hexaface,stat=ios)       ; ier = ier + ios
      if (allocated(rg_gll_coordinate))                 deallocate(rg_gll_coordinate,stat=ios)                ; ier = ier + ios
      if (allocated(ig_hexa_snapshot))                  deallocate(ig_hexa_snapshot,stat=ios)                 ; ier = ier + ios
      if (allocated(ig_vtk_hexa_conn_snapshot))         deallocate(ig_vtk_hexa_conn_snapshot,stat=ios)        ; ier = ier + ios
      if (allocated(ig_vtk_node_gll_glonum_snapshot))   deallocate(ig_vtk_node_gll_glonum_snapshot,stat=ios)  ; ier = ier + ios
      if (allocated(rg_vtk_node_x_snapshot))            deallocate(rg_vtk_node_x_snapshot,stat=ios)           ; ier = ier + ios
      if (allocated(rg_vtk_node_y_snapshot))            deallocate(rg_vtk_node_y_snapshot,stat=ios)           ; ier = ier + ios
      if (allocated(rg_vtk_node_z_snapshot))            deallocate(rg_vtk_node_z_snapshot,stat=ios)           ; ier = ier + ios
      if (allocated(ig_vtk_cell_type_snapshot))         deallocate(ig_vtk_cell_type_snapshot,stat=ios)        ; ier = ier + ios
      if (allocated(ig_vtk_offset_snapshot))            deallocate(ig_vtk_offset_snapshot,stat=ios)           ; ier = ier + ios
      if (allocated(rg_fir_filter))                     deallocate(rg_fir_filter,stat=ios)                    ; ier = ier + ios
      if (allocated(rg_fir_filter_buffer_dis))          deallocate(rg_fir_filter_buffer_dis,stat=ios)         ; ier = ier + ios
      if (allocated(rg_fir_filter_buffer_vel))          deallocate(rg_fir_filter_buffer_vel,stat=ios)         ; ier = ier + ios
      if (allocated(rg_fir_filter_buffer_acc))          deallocate(rg_fir_filter_buffer_acc,stat=ios)         ; ier = ier + ios
      if (allocated(rg_fir_filter_buffer_dis_div))      deallocate(rg_fir_filter_buffer_dis_div,stat=ios)     ; ier = ier + ios
      if (allocated(rg_fir_filter_buffer_dis_spa_der))  deallocate(rg_fir_filter_buffer_dis_spa_der,stat=ios) ; ier = ier + ios
      if (allocated(ig_plane_wave_gll))                 deallocate(ig_plane_wave_gll,stat=ios)                ; ier = ier + ios
      if (allocated(ig_plane_wave_quadp_bottom))        deallocate(ig_plane_wave_quadp_bottom,stat=ios)       ; ier = ier + ios
      if (allocated(ig_plane_wave_quadp_side))          deallocate(ig_plane_wave_quadp_side,stat=ios)         ; ier = ier + ios
      if (allocated(rg_plane_wave_sft_user))            deallocate(rg_plane_wave_sft_user,stat=ios)           ; ier = ier + ios
      if (allocated(tg_sfsource))                       deallocate(tg_sfsource,stat=ios)                      ; ier = ier + ios
      if (allocated(tg_dcsource))                       deallocate(tg_dcsource,stat=ios)                      ; ier = ier + ios
      if (allocated(tg_fault))                          deallocate(tg_fault,stat=ios)                         ; ier = ier + ios
      if (allocated(tg_receiver_hexa))                  deallocate(tg_receiver_hexa,stat=ios)                 ; ier = ier + ios
      if (allocated(tg_receiver_snapshot_quad))         deallocate(tg_receiver_snapshot_quad,stat=ios)        ; ier = ier + ios
      if (allocated(tg_receiver_quad))                  deallocate(tg_receiver_quad,stat=ios)                 ; ier = ier + ios
      if (allocated(tg_cpu_neighbor))                   deallocate(tg_cpu_neighbor,stat=ios)                  ; ier = ier + ios
      if (allocated(tg_elastic_material))               deallocate(tg_elastic_material,stat=ios)              ; ier = ier + ios
      if (allocated(tg_anelastic_material))             deallocate(tg_anelastic_material,stat=ios)            ; ier = ier + ios
      if (allocated(tg_random_material))                deallocate(tg_random_material,stat=ios)               ; ier = ier + ios
      if (allocated(tg_layer))                          deallocate(tg_layer,stat=ios)                         ; ier = ier + ios

      if (ier /= ZERO_IXP) then

         call error_stop("error in subroutine memory_deallocate_all_array")

      endif
      
      return

!***********************************************************************************************************************************************************************************
   end subroutine memory_deallocate_all_array
!***********************************************************************************************************************************************************************************

end module mod_init_memory
