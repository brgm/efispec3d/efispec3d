!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
module mod_signal_processing

   use mod_precision

   use mod_init_memory

   implicit none

   private
   
   public  :: ft          !<Fourier  transform
   public  :: ft_init     !<Init Fourier transform from given value (e.g., power spectral density, etc.)
   public  :: ft_symmetry !<Apply symmetry for Fourier  transform
   public  :: ft_shift    !<Shift zero-frequency component to center of spectrum
   public  :: ft_diff     !<Differentiate signal in frequency domain
   public  :: ft_mpi      !<MPI Parallel Fourier transform 
   public  :: ht          !<Hilbert  transform
   public  :: at          !<Analytic transform
   public  :: st          !<Stockwell transform
   public  :: wam         !<weighted arithmetic mean
   public  :: hanning     !<Hanning  window in time
   private :: gaussian    !<Gaussian window in freq
   public  :: spewin      !<Spectral smoothing (Parzen)
   private :: log2
   public  :: nextpow2
   public  :: psd
   public  :: csd
   public  :: cross_correlation
   public  :: cross_correlation_lag_axis
   public  :: variance

   interface ft

      module procedure ft_1d
      module procedure ft_2d

   end interface ft

   interface ft_init

      module procedure ft_init_2d
      module procedure ft_init_3d

   end interface ft_init


   interface ft_symmetry

      module procedure ft_symmetry_2d
      module procedure ft_symmetry_3d

   end interface ft_symmetry

   interface ft_shift

      module procedure ft_shift_2d

   end interface ft_shift

   interface ft_diff

      module procedure ft_1d_diff

   end interface ft_diff

   interface ft_mpi

      module procedure ft_mpi_2d

   end interface ft_mpi

   interface st

      module procedure st_real
      module procedure st_complex

   end interface st

   interface psd

      module procedure psd_1d
      module procedure psd_2d

   end interface psd

   interface csd

      module procedure csd_1d

   end interface csd

   interface cross_correlation

      module procedure cross_correlation_2d

   end interface

   interface variance

      module procedure variance_1d
      module procedure variance_2d

   end interface variance

     
   contains

!
!
!>@brief subroutine to compute an unnormalized fast fourier transform of a real-valued or a complex-valued vector x
!>@param  x   : data before fast fourier transform
!>@param  ind : -1 --> fourier forward transform,1 --> fourier inverse transform
!>@return x   : data after fast fourier transform. if ind=-1 -> x can be multiplied by time step to keep constant energy.
!                                                  if ind=+1 -> x can be multiplied by frequency step to keep constant energy.
!                                                  forward and backward transform will multiply the input by size(x).
!***********************************************************************************************************************************************************************************
   subroutine ft_1d(x,ind)
!***********************************************************************************************************************************************************************************

   implicit none

   complex(kind=RXP), dimension(:), intent(inout) :: x
   integer(kind=IXP)              , intent(in)    :: ind
   complex(kind=RXP)                              :: temp
   complex(kind=RXP)                              :: theta

   real   (kind=RXP), parameter                   :: PI = acos(-ONE_RXP)

   integer(kind=IXP)                              :: i
   integer(kind=IXP)                              :: j
   integer(kind=IXP)                              :: k
   integer(kind=IXP)                              :: m
   integer(kind=IXP)                              :: n
   integer(kind=IXP)                              :: kmax
   integer(kind=IXP)                              :: istep

   n  = size(x)
   j  = ONE_IXP

   do 40 i = ONE_IXP,n

      if(i.ge.j) go to 10

      temp = x(j)
      x(j) = x(i)
      x(i) = temp
10    m    = n/TWO_IXP
20    if(j.le.m) go to 30
      j    = j-m
      m    = m/TWO_IXP
      if(m.ge.TWO_IXP) go to 20
30    j    = j+m

40 continue

   kmax = ONE_IXP

50 if(kmax.ge.n) return

   istep = kmax*TWO_IXP

   do 70 k = ONE_IXP,kmax
      theta = cmplx(ZERO_RXP,PI*real(ind*(k-ONE_IXP),kind=RXP)/real(kmax,kind=RXP))
      do 60 i = k,n,istep
         j    = i+kmax
         temp = x(j)*cexp(theta)
         x(j) = x(i)-temp
         x(i) = x(i)+temp
60    continue
70 continue

   kmax = istep
   go to 50

!***********************************************************************************************************************************************************************************
   end subroutine ft_1d
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute an unnormalized fast fourier transform of a real-valued or a complex-valued matrix x
!>@param  x   : data before fast fourier transform
!>@param  ind : -1 --> fourier forward transform,1 --> fourier inverse transform
!>@return x   : data after fast fourier transform. cf. ft_1d for scaling convention
!***********************************************************************************************************************************************************************************
   subroutine ft_2d(x,ind)
!***********************************************************************************************************************************************************************************

   complex(kind=RXP), dimension(:,:), intent(inout) :: x
   integer(kind=IXP)                , intent(in)    :: ind

   complex(kind=RXP), dimension(:)  , allocatable   :: x_tmp

   integer(kind=IXP)                                :: i1
   integer(kind=IXP)                                :: i2
   integer(kind=IXP)                                :: n1
   integer(kind=IXP)                                :: n2
   integer(kind=IXP)                                :: ios
   
   n1 = size(x,1_IXP)
   n2 = size(x,2_IXP)

!
!->1D FFT of columns

   ios = init_array_complex(x_tmp,n1,"mod_signal_processing:ft2d:x_tmp")

   do i2 = 1_IXP,n2

      x_tmp(1_IXP:n1) = x(1_IXP:n1,i2)

      call ft(x_tmp,ind)

      x(1_IXP:n1,i2) = x_tmp(1_IXP:n1)

   enddo

   deallocate(x_tmp)

!
!->1D FFT of rows

   ios = init_array_complex(x_tmp,n2,"mod_signal_processing:ft2d:x_tmp")

   do i1 = 1_IXP,n1

      x_tmp(1:n2) = x(i1,1_IXP:n2)

      call ft(x_tmp,ind)

      x(i1,1_IXP:n2) = x_tmp(1_IXP:n2)

   enddo

   deallocate(x_tmp)

   return

!***********************************************************************************************************************************************************************************
   end subroutine ft_2d
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to initialize the first and second quadrant of a 2D FFT, given the power spectral density and a random phase
!>@param  i   : input array
!>@param  o   : array to be initialized
!>@return o   : initialized array
!***********************************************************************************************************************************************************************************
   subroutine ft_init_2d(i,o)
!***********************************************************************************************************************************************************************************

   use mpi

   use mod_global_variables, only : ig_mpi_comm_simu

   real   (kind=RXP), dimension(:,:)             , intent( in) :: i
   complex(kind=RXP), dimension(:,:), allocatable, intent(out) :: o

   real   (kind=RXP), dimension(:,:), allocatable              :: ph                    

   integer(kind=IXP)                                           :: nfold1
   integer(kind=IXP)                                           :: nfold2
   integer(kind=IXP)                                           :: nfft1
   integer(kind=IXP)                                           :: nfft2
   integer(kind=IXP)                                           :: i1
   integer(kind=IXP)                                           :: i2
   integer(kind=IXP)                                           :: ios

!
!->init values of the size of the FFT array and the corresponding folding values given from the input array

   nfold1 = size(i,1_IXP)
   nfold2 = size(i,2_IXP)

   nfft1 = 2_IXP*(nfold1 - 1_IXP)
   nfft2 = 2_IXP*(nfold2 - 1_IXP)
   
!
!->set random phase between 0 and 2*PI of Fourier spectrum

   ios = init_array_real(ph,nfold2,nfft1,"mod_signal_processing:ft_init_2d:ph")

   call random_number(ph)

   ph(:,:) = ph(:,:)*2.0_RXP*PI_RXP

!
!->cpu0 broadcast array 'ph' to all cpus to ensure that all cpus have the same random phase

   call mpi_bcast(ph(1_IXP,1_IXP),nfold2*nfft1,MPI_REAL,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!->fill FFT array from input array (supposed to be a power spectal density function) and random phase  

   ios = init_array_complex(o,nfft2,nfft1,"mod_signal_processing:ft_init_2d:o")


!->first 'full' quadrant (nfold1,nfold2)

   do i2 = 1_IXP,nfold2

      do i1 = 1_IXP,nfold1

         o(i1,i2) = sqrt(i(i1,i2))*cmplx(cos(ph(i1,i2)),sin(ph(i1,i2)))

      enddo

   enddo

!->second 'almost full' quadrant (2_IXP -> nfold2-1_IXP, nfold1+1_IXP -> nfft1). Other sectors of the quadrant are filled by the subroutine ft_symmetry

   do i2 = 2_IXP,nfold2-1_IXP

      do i1 = nfold1+1_IXP,nfft1

         o(i1,i2) = sqrt(i(2_IXP*nfold1-i1,i2))*cmplx(cos(ph(i1,i2)),sin(ph(i1,i2)))

      enddo

   enddo

!->set the mean in the spatial domain to zero and imaginary part of other specific sectors to zero

   o(1_IXP ,1_IXP ) = cmplx(0.0_RXP,0.0_RXP) ! sanity line wrt the initialization i(1_IXP,1_IXP) = ZERO_RXP

   o(nfold1,1_IXP ) = cmplx(real(o(nfold1,1_IXP)) ,0.0_RXP)

   o(1_IXP ,nfold2) = cmplx(real(o(1_IXP,nfold2)) ,0.0_RXP)

   o(nfold1,nfold2) = cmplx(real(o(nfold1,nfold2)),0.0_RXP)

   return

!***********************************************************************************************************************************************************************************
   end subroutine ft_init_2d
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to initialize 3D FFT, given the power spectral density and a random phase
!>@param  am  : input array containing amplitudes of the Fourier transform
!>@param  ph  : input array containing phases     of the Fourier transform (in radian)
!>@return ft  : initialized FFT array from amplitudes and phases
!***********************************************************************************************************************************************************************************
   subroutine ft_init_3d(am,ph,ft)
!***********************************************************************************************************************************************************************************

   use mpi

   use mod_global_variables, only : ig_mpi_comm_simu

   real   (kind=RXP), dimension(:,:,:)             , intent( in) :: am
   real   (kind=RXP), dimension(:,:,:), allocatable, intent( in) :: ph 
   complex(kind=RXP), dimension(:,:,:), allocatable, intent(out) :: ft

   integer(kind=IXP)                                             :: nfold1
   integer(kind=IXP)                                             :: nfold2
   integer(kind=IXP)                                             :: nfold3
   integer(kind=IXP)                                             :: nfft1
   integer(kind=IXP)                                             :: nfft2
   integer(kind=IXP)                                             :: nfft3
   integer(kind=IXP)                                             :: i1
   integer(kind=IXP)                                             :: i2
   integer(kind=IXP)                                             :: i3
   integer(kind=IXP)                                             :: ios

!
!->init values of the size of the FFT array and the corresponding folding values given from the input array

   nfold1 = size(am,1_IXP)
   nfold2 = size(am,2_IXP)
   nfold3 = size(am,3_IXP)

   nfft1 = 2_IXP*(nfold1 - 1_IXP)
   nfft2 = 2_IXP*(nfold2 - 1_IXP)
   nfft3 = 2_IXP*(nfold3 - 1_IXP)
   
!
!->cpu0 broadcast array 'ph' to all cpus to ensure that all cpus have the same random phase

   call mpi_bcast(ph(1_IXP,1_IXP,1_IXP),nfold3*nfft2*nfft1,MPI_REAL,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!->init and fill FFT array from input array: e.g. power spectal density function and random phase

   ios = init_array_complex(ft,nfft3,nfft2,nfft1,"mod_signal_processing:ft_init_3d:ft")

!
!quadrant convention in 3D
!
!quadrant 1:        1 -> nfold1 , 1        -> nfold2 ,        1 -> nfold3
!quadrant 2: nfold1+1 -> nfft1  , 1        -> nfold2 ,        1 -> nfold3
!quadrant 3: nfold1+1 -> nfft1  , nfold2+1 -> nfft2  ,        1 -> nfold3
!quadrant 4:        1 -> nfold1 , nfold2+1 -> nfft2  ,        1 -> nfold3
!quadrant 5:        1 -> nfold1 , 1        -> nfold2 , nfold3+1 -> nfft3
!quadrant 6: nfold1+1 -> nfft1  , 1        -> nfold2 , nfold3+1 -> nfft3
!quadrant 7: nfold1+1 -> nfft1  , nfold2+1 -> nfft2  , nfold3+1 -> nfft3
!quadrant 8:        1 -> nfold1 , nfold2+1 -> nfft2  , nfold3+1 -> nfft3

!->fill quadrant 1

   do i3 = 1_IXP,nfold3

      do i2 = 1_IXP,nfold2

         do i1 = 1_IXP,nfold1

            ft(i1,i2,i3) = sqrt(am(i1,i2,i3))*cmplx(cos(ph(i1,i2,i3)),sin(ph(i1,i2,i3)))

         enddo

      enddo

   enddo

!->fill quadrant 2 (symmetry used for the amplitude, not for the phase)

   do i3 = 1_IXP,nfold3

      do i2 = 1_IXP,nfold2
      
         do i1 = nfold1+1_IXP,nfft1
      
            ft(i1,i2,i3) = sqrt(am(nfft1-i1+2_IXP,i2,i3))*cmplx(cos(ph(i1,i2,i3)),sin(ph(i1,i2,i3)))
      
         enddo
      
      enddo

   enddo

!->fill quadrant 3 (symmetry used for the amplitude, not for the phase)

   do i3 = 1_IXP,nfold3

      do i2 = nfold2+1,nfft2
      
         do i1 = nfold1+1_IXP,nfft1
      
            ft(i1,i2,i3) = sqrt(am(nfft1-i1+2_IXP,nfft2-i2+2_IXP,i3))*cmplx(cos(ph(i1,i2,i3)),sin(ph(i1,i2,i3)))
      
         enddo
      
      enddo

   enddo

!->fill quadrant 4 (symmetry used for the amplitude, not for the phase)

   do i3 = 1_IXP,nfold3

      do i2 = nfold2+1,nfft2
      
         do i1 = 1_IXP,nfold1
      
            ft(i1,i2,i3) = sqrt(am(i1,nfft2-i2+2_IXP,i3))*cmplx(cos(ph(i1,i2,i3)),sin(ph(i1,i2,i3)))
      
         enddo
      
      enddo

   enddo

   call ft_symmetry(ft)

!!->set the mean in the spatial domain to zero and imaginary part of other specific sectors to zero
!
!   o(1_IXP ,1_IXP ) = cmplx(0.0_RXP,0.0_RXP) ! sanity line wrt the initialization i(1_IXP,1_IXP) = ZERO_RXP
!
!   o(nfold1,1_IXP ) = cmplx(real(o(nfold1,1_IXP)) ,0.0_RXP)
!
!   o(1_IXP ,nfold2) = cmplx(real(o(1_IXP,nfold2)) ,0.0_RXP)
!
!   o(nfold1,nfold2) = cmplx(real(o(nfold1,nfold2)),0.0_RXP)

   return

!***********************************************************************************************************************************************************************************
   end subroutine ft_init_3d
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to fill symmetries of 2D FFT. First quadrant must be already filled and second quadrant must be partially filled
!>@param  x   : array to be symmetrized
!>@return x   : array symmetrized
!***********************************************************************************************************************************************************************************
   subroutine ft_symmetry_2d(x)
!***********************************************************************************************************************************************************************************

   complex(kind=RXP), dimension(:,:), intent(inout) :: x

   integer(kind=IXP)                                :: i1
   integer(kind=IXP)                                :: i2
   integer(kind=IXP)                                :: nfft1
   integer(kind=IXP)                                :: nfft2
   integer(kind=IXP)                                :: n1
   integer(kind=IXP)                                :: n2
   
   nfft1 = size(x,1_IXP)
   nfft2 = size(x,2_IXP)

   n1 = nfft1/2_IXP + 1_IXP
   n2 = nfft2/2_IXP + 1_IXP

!->complete line i2=1

   do i1 = n1+1_IXP,nfft1

      x(i1,1_IXP) = conjg(x(2_IXP*n1-i1,1_IXP))

   enddo

!->complete line i2=n2

   do i1 = n1+1_IXP,nfft1

      x(i1,n2) = conjg(x(2_IXP*n1-i1,n2))

   enddo

!->complete line i1=1

   do i2 = n2+1_IXP,nfft2

      x(1_IXP,i2) = conjg(x(1_IXP,2_IXP*n2-i2))

   enddo

!->complete line i1=n1

   do i2 = n2+1_IXP,nfft2

      x(n1,i2) = conjg(x(n1,2_IXP*n2-i2))

   enddo

!->third quadrant

   do i2 = n2+1_IXP,nfft2

      do i1 = n1+1_IXP,nfft1

         x(i1,i2) = conjg(x(2_IXP*n1-i1,2_IXP*n2-i2))

      enddo

   enddo

!->fourth quadrant

   do i2 = n2+1_IXP,nfft2

      do i1 = n1-1_IXP,2_IXP,-1_IXP

         x(i1,i2) = conjg(x(2_IXP*n1-i1,2_IXP*n2-i2))

      enddo

   enddo

   return

!***********************************************************************************************************************************************************************************
   end subroutine ft_symmetry_2d
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to fill symmetries of 3D FFT. ft_init must be done beforehand
!>@param  x   : FFT array to be symmetrized
!>@return x   : symmetrized FFT array
!***********************************************************************************************************************************************************************************
   subroutine ft_symmetry_3d(x)
!***********************************************************************************************************************************************************************************

   complex(kind=RXP), dimension(:,:,:), intent(inout) :: x

   integer(kind=IXP)                                  :: i1
   integer(kind=IXP)                                  :: i2
   integer(kind=IXP)                                  :: i3
   integer(kind=IXP)                                  :: nfft1
   integer(kind=IXP)                                  :: nfft2
   integer(kind=IXP)                                  :: nfft3
   integer(kind=IXP)                                  :: nfold1
   integer(kind=IXP)                                  :: nfold2
   integer(kind=IXP)                                  :: nfold3
   
   nfft1 = size(x,1_IXP)
   nfft2 = size(x,2_IXP)
   nfft3 = size(x,3_IXP)

   nfold1 = nfft1/2_IXP + 1_IXP
   nfold2 = nfft2/2_IXP + 1_IXP
   nfold3 = nfft3/2_IXP + 1_IXP

!
!->see Pardo-Iguzquiza, E., & Chica-Olmo, M. (1993). The Fourier integral method: an efficient spectral method for simulation of random fields. Mathematical geology, 25(2), 177-217.

!
!->symmetry at i1=1 and i3=1

   do i2 = 2_IXP,nfft2/2_IXP

      x(1_IXP,nfft2-i2+2_IXP,1_IXP) = conjg(x(1_IXP,i2,1_IXP))

   enddo

!
!->symmetry at i2=1 and i3=1

   do i1 = 2_IXP,nfft1/2_IXP

      x(nfft1-i1+2_IXP,1_IXP,1_IXP) = conjg(x(i1,1_IXP,1_IXP))

   enddo

!
!->symmetry at i1=nfold1 and i3=1

   do i2 = 2_IXP,nfft2/2_IXP

      x(nfold1,nfft2-i2+2_IXP,1_IXP) = conjg(x(nfold1,i2,1_IXP))

   enddo

!
!->symmetry at i2=nfold2 and i3=1

   do i1 = 2_IXP,nfft1/2_IXP

      x(nfft1-i1+2_IXP,nfold2,1_IXP) = conjg(x(i1,nfold2,1_IXP))

   enddo

!
!->symmetry at i3=1

   do i2 = 2_IXP,nfft2/2_IXP

      do i1 = 2_IXP,nfft1/2_IXP

         x(nfft1-i1+2_IXP,nfft2-i2+2_IXP,1_IXP) = conjg(x(i1,i2,1_IXP))

      enddo

   enddo


!
!->symmetry at i1=1 and i3=nfold3

   do i2 = 2_IXP,nfft2/2_IXP

      x(1_IXP,nfft2-i2+2_IXP,nfold3) = conjg(x(1_IXP,i2,nfold3))

   enddo

!
!->symmetry at i2=1 and i3=nfold3

   do i1 = 2_IXP,nfft1/2_IXP

      x(nfft1-i1+2_IXP,1_IXP,nfold3) = conjg(x(i1,1_IXP,nfold3))

   enddo

!
!->symmetry at i1=nfold1 and i3=nfold3

   do i2 = 2_IXP,nfft2/2_IXP

      x(nfold1,nfft2-i2+2_IXP,nfold3) = conjg(x(nfold1,i2,nfold3))

   enddo

!
!->symmetry at i2=nfold2 and i3=nfold3

   do i1 = 2_IXP,nfft1/2_IXP

      x(nfft1-i1+2_IXP,nfold2,nfold3) = conjg(x(i1,nfold2,nfold3))

   enddo

!
!->symmetry at i3=nfold3

   do i2 = 2_IXP,nfft2/2_IXP

      do i1 = 2_IXP,nfft1/2_IXP

         x(nfft1-i1+2_IXP,nfft2-i2+2_IXP,nfold3) = conjg(x(i1,i2,nfold3))

      enddo

   enddo

!
!->symmetry between i3=[2,nfft3/2] and i3=[nfft3/2+2,nfft3]

   !i1=1 and i2=1
   do i3 = 2_IXP,nfft3/2_IXP

      x(1_IXP,1_IXP,nfft3-i3+2_IXP) = conjg(x(1_IXP,1_IXP,i3))

   enddo

   !i1=1 and i2=nfold2
   do i3 = 2_IXP,nfft3/2_IXP

      x(1_IXP,nfold2,nfft3-i3+2_IXP) = conjg(x(1_IXP,nfold2,i3))

   enddo

   !i1=nfold1 and i2=1
   do i3 = 2_IXP,nfft3/2_IXP

      x(nfold1,1_IXP,nfft3-i3+2_IXP) = conjg(x(nfold1,1_IXP,i3))

   enddo

   !i1=nfold1 and i2=nfold2
   do i3 = 2_IXP,nfft3/2_IXP

      x(nfold1,nfold2,nfft3-i3+2_IXP) = conjg(x(nfold1,nfold2,i3))

   enddo

   !i1=1
   do i3 = 2_IXP,nfft3/2_IXP

      do i2 = 2_IXP,nfft2/2_IXP

         x(1_IXP,nfft2-i2+2_IXP,nfft3-i3+2_IXP) = conjg(x(1_IXP,i2,i3))

      enddo

   enddo

   !i1=1 bis 
   do i3 = 2_IXP,nfft3/2_IXP

      do i2 = 2_IXP,nfft2/2_IXP

         x(1_IXP,i2,nfft3-i3+2_IXP) = conjg(x(1_IXP,nfft2-i2+2_IXP,i3))

      enddo

   enddo

   !i2=1
   do i3 = 2_IXP,nfft3/2_IXP

      do i1 = 2_IXP,nfft1/2_IXP

         x(nfft1-i1+2_IXP,1_IXP,nfft3-i3+2_IXP) = conjg(x(i1,1_IXP,i3))

      enddo

   enddo

   !i1=nfold1
   do i3 = 2_IXP,nfft3/2_IXP

      do i2 = 2_IXP,nfft2/2_IXP

         x(nfold1,nfft2-i2+2_IXP,nfft3-i3+2_IXP) = conjg(x(nfold1,i2,i3))

      enddo

   enddo

   !i1=nfold1 bis 
   do i3 = 2_IXP,nfft3/2_IXP

      do i2 = 2_IXP,nfft2/2_IXP

         x(nfold1,i2,nfft3-i3+2_IXP) = conjg(x(nfold1,nfft2-i2+2_IXP,i3))

      enddo

   enddo

   !i2=nfold2
   do i3 = 2_IXP,nfft3/2_IXP

      do i1 = 2_IXP,nfft1/2_IXP

         x(nfft1-i1+2_IXP,nfold2,nfft3-i3+2_IXP) = conjg(x(i1,nfold2,i3))

      enddo

   enddo

   !i2=nfold2 bis
   do i3 = 2_IXP,nfft3/2_IXP

      do i1 = 2_IXP,nfft1/2_IXP

         x(i1,nfold2,nfft3-i3+2_IXP) = conjg(x(nfft1-i1+2_IXP,nfold2,i3))

      enddo

   enddo

   !all indices run
   do i3 = 2_IXP,nfft3/2_IXP

      do i2 = 2_IXP,nfft2/2_IXP

         do i1 = 2_IXP,nfft1/2_IXP

            x(nfft1-i1+2_IXP,nfft2-i2+2_IXP,nfft3-i3+2_IXP) = conjg(x(i1,i2,i3))

         enddo

      enddo

   enddo 

   !all indices run bis
   do i3 = 2_IXP,nfft3/2_IXP

      do i2 = 2_IXP,nfft2/2_IXP

         do i1 = 2_IXP,nfft1/2_IXP

            x(i1,i2,nfft3-i3+2_IXP) = conjg(x(nfft1-i1+2_IXP,nfft2-i2+2_IXP,i3))

         enddo

      enddo

   enddo 

   return

!***********************************************************************************************************************************************************************************
   end subroutine ft_symmetry_3d
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to shift zero-frequency component to center of spectrum
!>@param  x   : array to be shifted
!>@return x   : array shifted
!***********************************************************************************************************************************************************************************
   subroutine ft_shift_2d(x)
!***********************************************************************************************************************************************************************************

   real   (kind=RXP), dimension(:,:), intent(inout) :: x

   real   (kind=RXP), dimension(:,:), allocatable   :: x_tmp

   integer(kind=IXP)                                :: i1
   integer(kind=IXP)                                :: i2
   integer(kind=IXP)                                :: nfft1
   integer(kind=IXP)                                :: nfft2
   integer(kind=IXP)                                :: n1
   integer(kind=IXP)                                :: n2
   integer(kind=IXP)                                :: ios
   
   nfft1 = size(x,1_IXP)
   nfft2 = size(x,2_IXP)

   n1 = nfft1/2_IXP
   n2 = nfft2/2_IXP

   ios = init_array_real(x_tmp,n2,n1,"mod_signal_processing:ft_shift_2d:x_tmp")

!
!->store first quadrant

   do i2 = 1_IXP,n2

      do i1 = 1_IXP,n1

         x_tmp(i1,i2) = x(i1,i2)

      enddo

   enddo

!
!->put third quadrant in place of the first

   do i2 = 1_IXP,n2

      do i1 = 1_IXP,n1

         x(i1,i2) = x(n1+i1,n2+i2)

      enddo

   enddo


!
!->put first quadrant in place of the third

   do i2 = 1_IXP,n2

      do i1 = 1_IXP,n1

         x(n1+i1,n2+i2) = x_tmp(i1,i2)

      enddo

   enddo

!
!->store second quadrant

   do i2 = 1_IXP,n2

      do i1 = 1_IXP,n1

         x_tmp(i1,i2) = x(i1+n1,i2)

      enddo

   enddo

!
!->fourth quadrant becomes second quadrant

   do i2 = 1_IXP,n2

      do i1 = 1_IXP,n1

         x(i1+n1,i2) = x(i1,n2+i2)

      enddo

   enddo


!
!->second quadrant becomes fourth quadrant

   do i2 = 1_IXP,n2

      do i1 = 1_IXP,n1

         x(i1,n2+i2) = x_tmp(i1,i2)

      enddo

   enddo

   deallocate(x_tmp)

   return

!***********************************************************************************************************************************************************************************
   end subroutine ft_shift_2d
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute time derivative of a signal in the frequency domain
!>@param  x   : data before fast fourier transform
!>@param  dt  : tim step
!>@return x   : time derivative of x
!***********************************************************************************************************************************************************************************
   subroutine ft_1d_diff(x,dt)
!***********************************************************************************************************************************************************************************

   implicit none

   real(kind=RXP), intent(inout), dimension(:)  :: x
   real(kind=RXP), intent(in)                   :: dt
                                                
   real(kind=RXP), allocatable  , dimension(:)  :: x2
   real(kind=RXP)                               :: pi
   real(kind=RXP)                               :: p

   complex(kind=RXP), allocatable, dimension(:) :: c

   integer(kind=IXP)                            :: i
   integer(kind=IXP)                            :: n
   integer(kind=IXP)                            :: n2
   integer(kind=IXP)                            :: nt
   integer(kind=IXP)                            :: nfold
   integer(kind=IXP)                            :: ios

   pi = acos(-ONE_RXP)
   n  = size(x)

!
!->initialization of time domain signal + applying symmetry at the end (useful when signal is not apodized)

   n2 = TWO_IXP*n

   ios = init_array_real(x2,n2,"mod_signal_processing:ft_1d_diff")

   do i = ONE_IXP,n-ONE_IXP

      x2(i)   = x(i)

      x2(n+i) = x(n-i)

   enddo

   x2(n) = x(n)

   nt = nextpow2(n2,4096_IXP)

   nfold = nt/TWO_IXP + ONE_IXP

   allocate(c(nt))

   c(:) = cmplx(ZERO_RXP,ZERO_RXP)

   do i = ONE_IXP,n2

      c(i) = cmplx(x2(i),ZERO_RXP)

   enddo

   call ft(c,-ONE_IXP)

!
!->frequency domain differentiation

   c(ONE_IXP) = cmplx(ZERO_RXP,ZERO_RXP)

   do i = TWO_IXP,nfold-ONE_IXP

      c(i)            = cmplx(ZERO_RXP,ONE_RXP)*real(i-ONE_IXP,kind=RXP)*c(i)

      c(nt-i+TWO_IXP) = conjg(c(i))

   enddo

   c(nfold) = cmplx(ZERO_RXP,ONE_IXP)*real(nfold-ONE_IXP,kind=RXP)*c(nfold)

!
!->fourier inverse transform

   call ft(c,+ONE_IXP)

   p = (TWO_RXP*pi)/real(nt,kind=RXP)**TWO_IXP/dt

   do i = ONE_IXP,n

      x(i) = real(c(i))*p

   enddo

   deallocate(c,x2)

   return
!*******************************************************************************
   end subroutine ft_1d_diff
!*******************************************************************************


!
!
!>@brief subroutine to compute MPI parallel Fourier transform of a 2D array. A cpu computes the FFT of a certain number of rows and columns
!>@param  x   : data to be transformed 
!>@param  ind : -1 for forward transform, +1 for inverse transform
!>@return x   : transformed data
!***********************************************************************************************************************************************************************************
   subroutine ft_mpi_2d(x,ind)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                      partition1d&
                                     ,ig_myrank&
                                     ,ig_ncpu&
                                     ,ig_mpi_comm_simu
 
      complex(kind=RXP), dimension(:,:), intent(inout)          :: x
      integer(kind=IXP)                , intent(   in)          :: ind
                                                                
      complex(kind=RXP), dimension(:,:), allocatable            :: x_tmp
 
      integer(kind=IXP), dimension(:)  , allocatable            :: block_len
      integer(kind=MPI_ADDRESS_KIND), dimension(:), allocatable :: block_dis
 
      integer(kind=IXP), dimension(ig_ncpu)                     :: istart_all_cpus
      integer(kind=IXP), dimension(ig_ncpu)                     :: npart_all_cpus
      integer(kind=IXP), dimension(ig_ncpu)                     :: sendcounts
      integer(kind=IXP), dimension(ig_ncpu)                     :: recvcounts
      integer(kind=IXP), dimension(ig_ncpu)                     :: senddispls
      integer(kind=IXP), dimension(ig_ncpu)                     :: recvdispls
      integer(kind=IXP), dimension(ig_ncpu)                     :: sendtype_a
      integer(kind=IXP), dimension(ig_ncpu)                     :: recvtype_a
      integer(kind=IXP)                                         :: nfft1
      integer(kind=IXP)                                         :: nfft2
      integer(kind=IXP)                                         :: ifft
      integer(kind=IXP)                                         :: sendtype
      integer(kind=IXP)                                         :: mpi_nboctet_complex
      integer(kind=IXP)                                         :: icpu
      integer(kind=IXP)                                         :: ipart
      integer(kind=IXP)                                         :: jpart 
      integer(kind=IXP)                                         :: npart
      integer(kind=IXP)                                         :: istart 
      integer(kind=IXP)                                         :: iend 
      integer(kind=IXP)                                         :: ios 


!
!---->get the size of the 2d array 'x'

      nfft1 = size(x,1_IXP)
      nfft2 = size(x,2_IXP)

!
!---->partition (among the available cpus) the FFTs to be performed along the first direction -> make partition along the second direction 'nfft2'

      call partition1d(ig_myrank,ig_ncpu,nfft2,istart,iend)

      if (istart > ZERO_IXP) then

         npart = iend - istart + 1_IXP

      else

         npart = ZERO_IXP

      endif

!
!---->cpus exchange 'npart' to prepare alltoallw

      call mpi_allgather(npart,1_IXP,MPI_INTEGER,npart_all_cpus,1_IXP,MPI_INTEGER,ig_mpi_comm_simu,ios)

!
!---->cpus exchange 'istart' to prepare alltoallw

      call mpi_allgather(istart,1_IXP,MPI_INTEGER,istart_all_cpus,1_IXP,MPI_INTEGER,ig_mpi_comm_simu,ios)

!
!---->cpus set counts to be sent and received

      do icpu = ONE_IXP,ig_ncpu

         sendcounts(icpu) = npart

      enddo

      do icpu = ONE_IXP,ig_ncpu

         recvcounts(icpu) = npart_all_cpus(icpu)

      enddo

!
!---->cpus create type to be sent

      call mpi_type_contiguous(nfft1,MPI_COMPLEX,sendtype,ios)

      call mpi_type_commit(sendtype,ios)

!
!---->cpus create senddispls (the displacement from which to take the outgoing data) and recvdispls (the displacement at which to place the incoming data)

      do icpu = ONE_IXP,ig_ncpu

         senddispls(icpu) = ZERO_IXP

      enddo

      do icpu = ONE_IXP,ig_ncpu
      
         recvdispls(icpu) = max(istart_all_cpus(icpu) - ONE_IXP , ZERO_IXP)
      
      enddo

!
!---->perform FFTs along the first direction

      if (npart > ZERO_IXP) then

         ios = init_array_complex(x_tmp,npart,nfft1,"mod_signal_processing:ft_mpi_2d:x_tmp")
       
         jpart = ZERO_IXP
       
         do ipart = istart,iend
       
            jpart = jpart + ONE_IXP
       
            x_tmp(1_IXP:nfft1,jpart) = x(1_IXP:nfft1,ipart)
       
            call ft(x_tmp(:,jpart),ind)

         enddo
       
      else

         ios = init_array_complex(x_tmp,1_IXP,1_IXP,"mod_signal_processing:ft_mpi_2d:x_tmp") !dummy not used

      endif

!
!---->s-direction: each cpu get the full matrix before partitioning and computing the d-direction. recvctype=sendtype, so only sendtype is defined

      call mpi_alltoallv(x_tmp,sendcounts,senddispls,sendtype,x,recvcounts,recvdispls,sendtype,ig_mpi_comm_simu,ios)

      deallocate(x_tmp)

      call mpi_type_free(sendtype,ios)

!
!---->partition (among the available cpus) the FFTs to perform along the d-direction -> make partition along nfft1

      call partition1d(ig_myrank,ig_ncpu,nfft1,istart,iend)

      if (istart > ZERO_IXP) then

         npart = iend - istart + 1_IXP

      else

         npart = ZERO_IXP

      endif

!
!---->cpus exchange 'npart' to prepare alltoallw

      call mpi_allgather(npart,1_IXP,MPI_INTEGER,npart_all_cpus,1_IXP,MPI_INTEGER,ig_mpi_comm_simu,ios)

!
!---->cpus exchange 'istart' to prepare alltoallw

      call mpi_allgather(istart,1_IXP,MPI_INTEGER,istart_all_cpus,1_IXP,MPI_INTEGER,ig_mpi_comm_simu,ios)

!
!---->cpus set counts to be sent and received

      if (npart > ZERO_IXP) then

         sendcounts(:) = ONE_IXP

      else

         sendcounts(:) = ZERO_IXP

      endif
      

      do icpu = ONE_IXP,ig_ncpu

         if (npart_all_cpus(icpu) > ZERO_IXP) then

            recvcounts(icpu) = ONE_IXP 

         else

            recvcounts(icpu) = ZERO_IXP

         endif

      enddo

!
!---->cpus create type to be sent and received

      call mpi_type_size(MPI_COMPLEX,mpi_nboctet_complex,ios)

      call mpi_type_vector(npart,ONE_IXP,nfft2,MPI_COMPLEX,sendtype,ios)

      call mpi_type_commit(sendtype,ios)

      ios = init_array_int(block_len,nfft2,"mod_signal_processing:ft_mpi_2d_par:block_len")

      ios = init_array_int(block_dis,nfft2,"mod_signal_processing:ft_mpi_2d_par:block_dis")

      block_len(:) = ONE_IXP

      do ifft = ONE_IXP,nfft2

         block_dis(ifft) = (int(ifft,kind=I64) - ONE_I64)*int(mpi_nboctet_complex,kind=I64)

      enddo

      do icpu = ONE_IXP,ig_ncpu

         call mpi_type_create_hindexed(nfft2,block_len,block_dis,sendtype,sendtype_a(icpu),ios)

         call mpi_type_commit(sendtype_a(icpu),ios)

      enddo

      do icpu = ONE_IXP,ig_ncpu

         call mpi_type_vector(nfft2,npart_all_cpus(icpu),nfft1,MPI_COMPLEX,recvtype_a(icpu),ios)

         call mpi_type_commit(recvtype_a(icpu),ios)

      enddo

!
!---->cpus create senddispls (the displacement from which to take the outgoing data) and recvdispls (the displacement at which to place the incoming data)

      do icpu = ONE_IXP,ig_ncpu

         senddispls  (icpu) = ZERO_IXP

      enddo

      do icpu = ONE_IXP,ig_ncpu
      
         recvdispls(icpu) = max((istart_all_cpus(icpu)-ONE_IXP)*mpi_nboctet_complex,ZERO_IXP)
      
      enddo

!
!---->perform FFTs along the second direction

      if (npart > ZERO_IXP) then

         ios = init_array_complex(x_tmp,npart,nfft2,"mod_signal_processing:ft_mpi_2d:x_tmp")
       
         jpart = ZERO_IXP
       
         do ipart = istart,iend
       
            jpart = jpart + ONE_IXP
       
            x_tmp(1_IXP:nfft2,jpart) = x(ipart,1_IXP:nfft2)
       
            call ft(x_tmp(:,jpart),ind)

         enddo
       
      else

         ios = init_array_complex(x_tmp,1_IXP,1_IXP,"mod_signal_processing:ft_mpi_2d:x_tmp") !dummy not used

      endif

!
!---->s-direction: each cpu get the full matrix before partitioning and computing the d-direction. 

      call mpi_alltoallw(x_tmp,sendcounts,senddispls,sendtype_a,x,recvcounts,recvdispls,recvtype_a,ig_mpi_comm_simu,ios)

      deallocate(x_tmp)

      do icpu = ONE_IXP,ig_ncpu

         call mpi_type_free(sendtype_a(icpu),ios)

      enddo

      do icpu = ONE_IXP,ig_ncpu

         call mpi_type_free(recvtype_a(icpu),ios)

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine ft_mpi_2d
!***********************************************************************************************************************************************************************************


!
!
!TODO FLO: WARNING, ALGO TO BE CHECKED!
!********************************************************************************************************************************************
   subroutine ht(d)
!********************************************************************************************************************************************
!WARNING: ALGO TO BE CHECKED!

      implicit none

      real   (kind=RXP), dimension(:), intent(inout) :: d

      real   (kind=RXP), dimension(:), allocatable   :: h
      complex(kind=RXP), dimension(:), allocatable   :: dtmp

      integer(kind=IXP)                              :: i
      integer(kind=IXP)                              :: n
      integer(kind=IXP)                              :: nt
      integer(kind=IXP)                              :: nf
      integer(kind=IXP)                              :: nr
      integer(kind=IXP)                              :: ios

      n = size(d)

      nt = nextpow2(n)

      nf = floor(real(nt-ONE_IXP,kind=RXP)/TWO_RXP)

      nr = (1_IXP-mod(nt,2_IXP))

      ios = init_array_real(h,nt,"mod_signal_processing:ht:h")

!
!---->create h function

      h(1_IXP) = ZERO_RXP

      do i = 2_IXP,nt/2_IXP+1_IXP

         h(i) = ONE_RXP

      enddo

      do i = nt/2_IXP+2_IXP,n

         h(i) = -ONE_RXP

      enddo

!
!---->compute fft of array dtmp

      ios = init_array_complex(dtmp,nt,"mod_signal_processing:ht:dtmp")

      do i = 1_IXP,n
         dtmp(i) = cmplx(d(i),ZERO_RXP)
      enddo

      call ft(dtmp,-1_IXP)

!
!---->compute Hilbert transform

      do i = 1,nt

         dtmp(i) = dtmp(i) * h(i) * cmplx(ZERO_RXP,-ONE_RXP)

      enddo

!
!---->compute inverse fft of array dtmp

      call ft(dtmp,+1_IXP)

      return

!********************************************************************************************************************************************
   end subroutine ht
!********************************************************************************************************************************************


!
!
!********************************************************************************************************************************************
   subroutine at(d,z)
!********************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:)             , intent(in ) :: d
      complex(kind=RXP), dimension(:), allocatable, intent(out) :: z

      real   (kind=RXP), dimension(:), allocatable              :: h

      complex(kind=RXP), dimension(:), allocatable              :: fd

      integer(kind=IXP)                                         :: i
      integer(kind=IXP)                                         :: n
      integer(kind=IXP)                                         :: nt
      integer(kind=IXP)                                         :: nf
      integer(kind=IXP)                                         :: ios

      n   = size(d)

      nt  = nextpow2(n)

      nf = nt/2_IXP + 1_IXP

      ios = init_array_real   (h ,nt,"mod_signal_processing:ht:h")
      ios = init_array_complex(fd,nt,"mod_signal_processing:ht:fd")
      ios = init_array_complex(z ,nt,"mod_signal_processing:ht:z")

!
!---->create h function

!---->freq 0
      h(1_IXP) = ONE_RXP

!---->freq > 0
      do i = 2_IXP,nf

         h(i) = TWO_RXP

      enddo

!---->freq < 0
      do i = nf+1_IXP,nt

         h(i) = ZERO_RXP

      enddo

!
!---->compute fft of d

      do i = 1_IXP,n

         fd(i) = cmplx(d(i),ZERO_RXP)

      enddo

      call ft(fd,-1_IXP)

!
!---->compute fft of analytical signal : z(t) = d(t) + i*H[s(t)]

      do i = 1_IXP,nt

         z(i) = fd(i) * h(i)

      enddo

!
!---->compute inverse fft z to come back to time domain

      call ft(z,+1_IXP)

      z = z/real(nt,kind=RXP)

      return

!********************************************************************************************************************************************
   end subroutine
!********************************************************************************************************************************************


!
!
!********************************************************************************************************************************************
   subroutine st_real(d,dt,fmax,s,df)
!********************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:)               , intent( in) :: d
      complex(kind=RXP), dimension(:,:), allocatable, intent(out) :: s
      real   (kind=RXP)                             , intent(out) :: df
      real   (kind=RXP)                             , intent( in) :: dt
      real   (kind=RXP)                , optional   , intent( in) :: fmax

      complex(kind=RXP), dimension(:)  , allocatable              :: fd
      complex(kind=RXP), dimension(:)  , allocatable              :: fd2
      complex(kind=RXP), dimension(:)  , allocatable              :: fd2g

      real   (kind=RXP), dimension(:)  , allocatable              :: g

      real   (kind=RXP)                                           :: dmean
      real   (kind=RXP)                                           :: sn 

      integer(kind=IXP)                                           :: i
      integer(kind=IXP)                                           :: j
      integer(kind=IXP)                                           :: n
      integer(kind=IXP)                                           :: nt
      integer(kind=IXP)                                           :: ntf
      integer(kind=IXP)                                           :: ios
      integer(kind=IXP)                                           :: imax

      print *,"ST of real signal"

!
!---->init variables

      n   = size(d)

      nt  = nextpow2(n)

      ntf = nt/2_IXP+1_IXP

      df = ONE_RXP/(dt*real(nt,kind=RXP))

      sn = ONE_RXP/(TWO_RXP*dt) * (PI_RXP/(PI_RXP+ONE_RXP))


!
!---->check fmax

      if (present(fmax)) then

         if ( fmax > sn ) then

            write(*,*) "warning in mod_signal_processing:st_real. fmax too large. fmax set to Stockwell frequency."

            imax = int(sn/df,kind=IXP)

         else

            imax = int(fmax/df,kind=IXP) 

         endif

      else

         imax = int(sn/df,kind=IXP)

      endif

!
!---->init memory

      ios = init_array_complex(fd,nt,"mod_signal_processing:st:fd")

      ios = init_array_complex(fd2,nt*2_IXP,"mod_signal_processing:st:fd2")

      ios = init_array_complex(fd2g,nt,"mod_signal_processing:st:fd2g")

      ios = init_array_real(g,nt,"mod_signal_processing:st:g")

      ios = init_array_complex(s,imax+1_IXP,nt,"mod_signal_processing:st:s")

!
!---->ft array d

      do i = 1_IXP,n

         fd(i) = cmplx(d(i),ZERO_RXP)

      enddo

      call ft(fd,-ONE_IXP)

!
!---->duplicate array fd two times in a row in array fd2

      do i = 1_IXP,nt

         fd2(i) = fd(i)

      enddo
      
      do i = 1_IXP,nt

         fd2(nt+i) = fd(i)

      enddo

!
!********************************************************
!---->compute s-transform
!********************************************************

!
!---->freq 0

      dmean = sum(d)/real(nt,kind=RXP)

      do j = 1_IXP,nt

         s(j,1_IXP) = dmean

      enddo

!
!---->freq 1 to imax

      do i = 1_IXP,imax

         g = gaussian(nt,real(i,kind=RXP),ONE_RXP)

!
!------->convolve fd2 with gaussian

         do j = 1_IXP,nt

            fd2g(j) = fd2(i+j)*g(j)

         enddo

!
!------->compute inverse ft fd2g = stockwell transform (of voice i)

         call ft(fd2g,ONE_IXP)

         fd2g = fd2g/real(nt,kind=RXP)

         do j = 1_IXP,nt
   
            s(j,i+1_IXP) = conjg(fd2g(j)) !take the conjugate to match with the sign of matlab routine

         enddo

      enddo

      return

!********************************************************************************************************************************************
   end subroutine
!********************************************************************************************************************************************


!
!
!********************************************************************************************************************************************
   subroutine st_complex(d,dt,fmax,s,df)
!********************************************************************************************************************************************

      implicit none

      complex(kind=RXP), dimension(:)               , intent( in) :: d
      complex(kind=RXP), dimension(:,:), allocatable, intent(out) :: s
      real   (kind=RXP)                             , intent(out) :: df
      real   (kind=RXP)                             , intent( in) :: dt
      real   (kind=RXP)                , optional   , intent( in) :: fmax

      complex(kind=RXP), dimension(:)  , allocatable              :: fd
      complex(kind=RXP), dimension(:)  , allocatable              :: fd2
      complex(kind=RXP), dimension(:)  , allocatable              :: fd2g

      real   (kind=RXP), dimension(:)  , allocatable              :: g

      real   (kind=RXP)                                           :: dmean

      integer(kind=IXP)                                           :: i
      integer(kind=IXP)                                           :: j
      integer(kind=IXP)                                           :: n
      integer(kind=IXP)                                           :: nt
      integer(kind=IXP)                                           :: ntf
      integer(kind=IXP)                                           :: ios
      integer(kind=IXP)                                           :: imax

      print *,"ST of analytic signal"

!
!---->init variables

      n   = size(d)

      nt  = nextpow2(n,16384_IXP)

      ntf = nt/2_IXP+1_IXP

      df = ONE_RXP/(dt*real(nt,kind=RXP))

!
!---->check fmax

      if (present(fmax)) then

         imax = ceiling(fmax/df,kind=IXP)

         if (imax > nt/2_IXP) then

            write(*,*) "warning in mod_signal_processing:st_real. fmax too large. fmax set to Nyquist frequency."

            imax = nt/2_IXP

         endif

      else

         imax = nt/2_IXP

      endif

!
!---->init memory

      ios = init_array_complex(fd,nt,"mod_signal_processing:st:fd")

      ios = init_array_complex(fd2,nt*2_IXP,"mod_signal_processing:st:fd2")

      ios = init_array_complex(fd2g,nt,"mod_signal_processing:st:fd2g")

      ios = init_array_real(g,nt,"mod_signal_processing:st:g")

      ios = init_array_complex(s,imax+1_IXP,nt,"mod_signal_processing:st:s")


!
!---->ft array d

      do i = 1_IXP,n

         fd(i) = d(i)

      enddo

      call ft(fd,-ONE_IXP)

!
!---->duplicate array fd two times in a row in array fd2

      do i = 1_IXP,nt

         fd2(i) = fd(i)

      enddo
      
      do i = 1_IXP,nt

         fd2(nt+i) = fd(i)

      enddo

!
!********************************************************
!---->compute s-transform
!********************************************************

!
!---->freq 0

      dmean = sum(d)/real(nt,kind=RXP)

      do j = 1_IXP,nt

         s(j,1_IXP) = dmean

      enddo

!
!---->freq 1 to imax

      do i = 1_IXP,imax

         g = gaussian(nt,real(i,kind=RXP),ONE_RXP)

!
!------->convolve fd2 with gaussian

         do j = 1_IXP,nt

            fd2g(j) = fd2(i+j)*g(j)

         enddo

!
!------->compute inverse ft fd2g = stockwell transform (of voice i)

         call ft(fd2g,ONE_IXP)

         fd2g = fd2g/real(nt,kind=RXP)

         do j = 1_IXP,nt
   
            s(j,i+1_IXP) = conjg(fd2g(j)) !take the conjugate to match with the sign of matlab routine

         enddo

      enddo

      return

!********************************************************************************************************************************************
   end subroutine
!********************************************************************************************************************************************


!
!
!******************************************************************************
   real(kind=RXP) function wam(x, h) result(c)
!******************************************************************************

   implicit none
   
   real   (kind=RXP), dimension(:), intent(in) :: x!signal array
   real   (kind=RXP), dimension(:), intent(in) :: h!window array

   real   (kind=RXP)                           :: s1
   real   (kind=RXP)                           :: s2
                                                  
   integer(kind=IXP)                           :: n
   integer(kind=IXP)                           :: i
   
   n = size(h)

   s1 = ZERO_RXP

   do i = ONE_IXP,n

      s1 = s1 + x(i)*h(i)

   enddo

   s2 = sum(h(:))

   c = s1/s2

   return
   
!******************************************************************************
   end function wam
!******************************************************************************


!
!
!******************************************************************************
   subroutine hanning(n,h)
!******************************************************************************

   implicit none
   
   integer(kind=IXP)                           , intent( in) :: n

   real   (kind=RXP), dimension(:), allocatable, intent(out) :: h

   real   (kind=RXP)                                         :: nr
   real   (kind=RXP)                                         :: ir
                                                            
   integer(kind=IXP)                                         :: i
   integer(kind=IXP)                                         :: ios
   
   ios = init_array_real(h,n,"mod_signal_processing:hanning:h")

   nr  = real(n-ONE_IXP,kind=RXP)

   do i = ONE_IXP,n

      ir = real(i-ONE_IXP,kind=RXP)

      h(i) = ( sin((PI_RXP*ir)/nr) )**2_IXP

   enddo

   return

!******************************************************************************
   end subroutine hanning
!******************************************************************************




!
!
!>@brief This function return the gaussian window used by Stockwell transform
!>@author Programmed by Eric Tittley.
!********************************************************************************************************************************************
   function gaussian(n,fr,f) result(g)
!********************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent(in)                :: n  ! size of gaussian function array
      real   (kind=RXP), intent(in)                :: fr ! frequency
      real   (kind=RXP), intent(in)                :: f  ! factor

      real   (kind=RXP), dimension(:), allocatable :: a
      real   (kind=RXP), dimension(:), allocatable :: b
      real   (kind=RXP), dimension(:), allocatable :: g

      real   (kind=RXP)                            :: PI = acos(-ONE_RXP)
      integer(kind=IXP)                            :: i
      integer(kind=IXP)                            :: ios

!
!---->memory allocation
      ios = init_array_real(a,n,"mod_signal_processing:gaussian:a")
      ios = init_array_real(b,n,"mod_signal_processing:gaussian:b")
      ios = init_array_real(g,n,"mod_signal_processing:gaussian:g")
      
!
!---->pre-computation

      do i = 1_IXP,n
         a(i) = real((i-ONE_IXP)**2_IXP,kind=RXP) * (-f*TWO_RXP*PI**2_IXP/fr**2_IXP)
      enddo

      do i = 1_IXP,n
         b(i) = real((-n+i-ONE_IXP)**2_IXP,kind=RXP) * (-f*TWO_RXP*PI**2_IXP/fr**2_IXP)
      enddo

      do i = 1_IXP,n
         g(i) = exp(a(i)) + exp(b(i))
      enddo

      return

!********************************************************************************************************************************************
   end function
!********************************************************************************************************************************************


!
!
!>@brief This subroutine smooths a frequency domain function
!>@param  x   : array to be smoothed
!>@param  n   : size of array
!>@param  df  : frequency step
!>@param  b   : size of smoothing window (in frequency unit)
!>@return x   : array smoothed
!********************************************************************************************************************************************
   subroutine spewin(x,n,df,b)
!********************************************************************************************************************************************

     implicit none
     
     integer(kind=IXP), intent(in)                  :: n
     real   (kind=RXP), intent(inout), dimension(n) :: x
     real   (kind=RXP), intent(inout)               :: b
     real   (kind=RXP), intent(in)                  :: df
     
     real   (kind=RXP), allocatable  , dimension(:) :: w 
     real   (kind=RXP), allocatable  , dimension(:) :: x1
     real   (kind=RXP), allocatable  , dimension(:) :: x2 

     real   (kind=RXP)                              :: bmin
     real   (kind=RXP)                              :: bmax1
     real   (kind=RXP)                              :: bmax2
     real   (kind=RXP)                              :: bmax
     real   (kind=RXP)                              :: t
     real   (kind=RXP)                              :: udf
     real   (kind=RXP)                              :: dif
     real   (kind=RXP)                              :: s
     
     integer(kind=IXP)                              :: k
     integer(kind=IXP)                              :: l
     integer(kind=IXP)                              :: lmax
     integer(kind=IXP)                              :: ll
     integer(kind=IXP)                              :: ln
     integer(kind=IXP)                              :: lt
     integer(kind=IXP)                              :: le
     integer(kind=IXP)                              :: nd
     
     nd = 2_IXP*(n-1_IXP)
     
     allocate(x1(nd+1_IXP),x2(nd+1_IXP))
     
     bmin  =   560.0_RXP/151.0_RXP*df
     bmax1 =   140.0_RXP*real(n-1_IXP,kind=RXP)/151.0_RXP*df
     bmax2 = 14000.0_RXP/150.0_RXP*df
     bmax  = min(bmax1,bmax2)
     
     if( (b.lt.bmin) .or. (b.gt.bmax) ) then

        if(nd >= 65536_IXP) then
           b = bmax*1.0_RXP
        elseif(nd.eq.32768_IXP) then
           b = bmax*0.5_RXP
        elseif(nd.eq.16384_IXP) then
           b = bmax*0.25_RXP
        elseif(nd.eq.8192_IXP) then
           b = bmax*0.125_RXP
        else
           b = bmax*0.125_RXP
        endif

     endif
     
     t   = 1.0_RXP/df
     udf = 1.854305_RXP/b*df
     
     if (udf.gt.0.5_RXP) then

        write(error_unit,*                ) "***error in spewin. bandwidth is too narrow"
        write(error_unit,'(e15.7,a,e15.7)') bmin," < band width (hz) < ",bmax

     endif
     
     lmax = int(2.0_RXP/udf,kind=IXP)+1_IXP

     if (lmax.gt.101) then

        write(error_unit,*                ) "***error in spewin. bandwidth is too wide"
        write(error_unit,'(e15.7,a,e15.7)') bmin," < band width (hz) < ",bmax

     endif

!    
!--->spectral window

     allocate(w(lmax))

     w(1_IXP) = 0.75_RXP*udf

     do l = 2_IXP,lmax

        dif  = 1.570796_RXP*real(l-1_IXP,kind=RXP)*udf

        w(l) = w(1_IXP)*(sin(dif)/dif)**4_IXP

     enddo

!    
!--->smoothing of fourier spectrum

     ll = lmax*2_IXP-1_IXP
     ln =  ll-1_IXP    + n
     lt = (ll-1_IXP)*2_IXP + n
     le = lt-lmax+1_IXP
     
     do k = 1_IXP,lt

        x1(k) = 0.0_RXP

     enddo

     do k = 1_IXP,n

        x1(ll-1_IXP+k) = x(k)

     enddo
     
     do k = lmax,le

        s = w(1_IXP)*x1(k)

        do l = 2_IXP,lmax

           s = s + w(l)*(x1(k-l+1_IXP) + x1(k+l-1_IXP))

        enddo

        x2(k) = s

     enddo
     
     do l = 2_IXP,lmax

        x2(ll+l-1_IXP) = x2(ll+l-1_IXP) + x2(ll-l+1_IXP)
        x2(ln-l+1_IXP) = x2(ln-l+1_IXP) + x2(ln+l-1_IXP)

     enddo
     
     do k = 1_IXP,n

        x(k) = x2(ll-1_IXP+k)

     enddo
     
     deallocate(x1,x2,w)
     
     return

!********************************************************************************************************************************************
   end subroutine spewin
!********************************************************************************************************************************************


!
!
!********************************************************************************************************************************************
   real function log2(x)
!********************************************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in) :: x

      log2 = log(x) / log(TWO_RXP)

!********************************************************************************************************************************************
   end function
!********************************************************************************************************************************************


!
!
!>@brief subroutine to compute the next power 2 of number 'n'
!>@param  n   : number     for which next power two is computed
!>@param  nmin: number min for which next power two is computed
!********************************************************************************************************************************************
   integer(kind=IXP) function nextpow2(n,nmin)
!********************************************************************************************************************************************

   implicit none

   integer(kind=IXP), intent(in)           :: n
   integer(kind=IXP), intent(in), optional :: nmin

   integer(kind=IXP)                       :: m

   nextpow2 = TWO_IXP

   if ( present(nmin) .and. (n < nmin) ) then

      m = nmin

   else

      m = n

   endif

   do while (nextpow2 < m)

      nextpow2 = nextpow2*TWO_IXP

   enddo

   return

!********************************************************************************************************************************************
   end function nextpow2
!********************************************************************************************************************************************


!
!
!>@brief subroutine to compute estimate of 1D power spectral density from positive half of fourier spectrum
!>@param f  : fourier transform
!>@param dt : discrete step
!>@param nd : number of discrete step
!>@return p : power spectral density
!********************************************************************************************************************************************
   subroutine psd_1d(f,dt,nd,p)
!********************************************************************************************************************************************

      implicit none
 
      complex(kind=RXP), dimension(:), intent(in)    :: f
      real   (kind=RXP), dimension(:), intent(out)   :: p
      real   (kind=RXP)              , intent(in)    :: dt
      integer(kind=IXP)              , intent(in)    :: nd

      integer(kind=IXP)                              :: n

      n = size(f)/2_IXP + 1_IXP

      p(1_IXP:n) = 1.0_RXP*dt/real(nd,kind=RXP) * abs(f(1_IXP:n))**2_IXP 

      p(2_IXP:n-1_IXP) = 2.0_RXP*p(2_IXP:n-1_IXP)
 
      return

!********************************************************************************************************************************************
   end subroutine psd_1d
!********************************************************************************************************************************************


!
!
!>@brief subroutine to compute estimate of 2D power spectral density from positive half of fourier spectrum
!>@param f  : 2D fourier transform
!>@return p : power spectral density
!********************************************************************************************************************************************
   subroutine psd_2d(f,p)
!********************************************************************************************************************************************

      implicit none
 
      complex(kind=RXP), dimension(:,:), intent(in ) :: f
      complex(kind=RXP), dimension(:,:), intent(out) :: p

      integer(kind=IXP)                              :: nfft1
      integer(kind=IXP)                              :: nfft2
      integer(kind=IXP)                              :: n1
      integer(kind=IXP)                              :: n2
      integer(kind=IXP)                              :: i1
      integer(kind=IXP)                              :: i2

      nfft1 = size(f,1_IXP)
      nfft2 = size(f,2_IXP)
 
      n1 = nfft1/2_IXP + 1_IXP
      n2 = nfft2/2_IXP + 1_IXP

!
!
!********************************************************************************************************************************************
!---->first: compute partial power spectrum
!********************************************************************************************************************************************

!
!---->first quadrant

      do i2 = 1_IXP,n2

         do i1 = 1_IXP,n1

            p(i1,i2) = f(i1,i2) * conjg(f(i1,i2))
       
         enddo

      enddo

!
!---->second quadrant (partially because subroutine 'ft_symmetry' takes care of the rest. see below)

      do i2 = 2_IXP,n2-1_IXP

         do i1 = n1+1_IXP,nfft1

            p(i1,i2) = f(i1,i2) * conjg(f(i1,i2))
       
         enddo

      enddo

!
!
!********************************************************************************************************************************************
!---->second: compute full power spectral density using hermitian symmetries
!********************************************************************************************************************************************

!
!---->symmetries

      call ft_symmetry(p)

      return

!********************************************************************************************************************************************
   end subroutine psd_2d
!********************************************************************************************************************************************


!
!
!>@brief subroutine to compute estimate of 1D cross spectral density from positive half of fourier spectrum
!>@param  f : 1D fourier transform
!>@param  g : 1D fourier transform
!>@param dt : discrete step
!>@param nd : number of discrete step
!>@return c : cross spectral density
!********************************************************************************************************************************************
   subroutine csd_1d(f,g,dt,nd,c)
!********************************************************************************************************************************************

      implicit none
 
      complex(kind=RXP), dimension(:), intent(in)    :: f
      complex(kind=RXP), dimension(:), intent(in)    :: g
      complex(kind=RXP), dimension(:), intent(out)   :: c
      real   (kind=RXP)              , intent(in)    :: dt
      integer(kind=IXP)              , intent(in)    :: nd

      integer(kind=IXP)                              :: n
      integer(kind=IXP)                              :: i
 
      n = size(f)/2_IXP + 1_IXP

      do i = 1_IXP,n

         c(i) = 1.0_RXP*dt/real(nd,kind=RXP) * conjg(f(i))*g(i)

      enddo

      c(2_IXP:n-1_IXP) = 2.0_RXP*c(2_IXP:n-1_IXP)
 
      return

!********************************************************************************************************************************************
   end subroutine csd_1d
!********************************************************************************************************************************************


!
!
!>@brief subroutine to compute the 2D cross-correlation between a real m-by-n matrix a and a real p-by-q matrix b
!>@param  a : m-by-n matrix
!>@param  b : p-by-q matrix
!>@return c : (m+p-1)-by-(n+q-1) cross-correlation matrix 
!********************************************************************************************************************************************
   subroutine cross_correlation_2d(a,b,c)
!********************************************************************************************************************************************

      implicit none
 
      real   (kind=RXP), dimension(:,:)             , intent( in) :: a
      real   (kind=RXP), dimension(:,:)             , intent( in) :: b
      real   (kind=RXP), dimension(:,:), allocatable, intent(out) :: c

      real   (kind=RXP), dimension(:,:), allocatable              :: at
      real   (kind=RXP), dimension(:,:), allocatable              :: bkl

      integer(kind=IXP)                                           :: m
      integer(kind=IXP)                                           :: n
      integer(kind=IXP)                                           :: p
      integer(kind=IXP)                                           :: q
      integer(kind=IXP)                                           :: k
      integer(kind=IXP)                                           :: l
      integer(kind=IXP)                                           :: r
      integer(kind=IXP)                                           :: s
      integer(kind=IXP)                                           :: im
      integer(kind=IXP)                                           :: in
      integer(kind=IXP)                                           :: ip
      integer(kind=IXP)                                           :: iq
      integer(kind=IXP)                                           :: ik
      integer(kind=IXP)                                           :: il
      integer(kind=IXP)                                           :: ir
      integer(kind=IXP)                                           :: is

      integer(kind=IXP)                                           :: ios

      m = size(a,1_IXP) !minor fortran index
      n = size(a,2_IXP) !major fortran index

      p = size(b,1_IXP)
      q = size(b,2_IXP)

      k = m+p-1_IXP
      l = n+q-1_IXP

      r = m+2_IXP*(p-1_IXP)
      s = n+2_IXP*(q-1_IXP)

      ios = init_array_real(c,l,k,"mod_signal_processing:cross_correlation_2d:c") !l=major fortran index, k=minor

      ios = init_array_real(at,s,r,"mod_signal_processing:cross_correlation_2d:at")

      ios = init_array_real(bkl,s,r,"mod_signal_processing:cross_correlation_2d:bkl")

!
!---->transfering matrix 'a' to matrix 'at'

      do im = 1_IXP,m

         do in = 1_IXP,n

            at(im+p-1_IXP,in+q-1_IXP) = a(im,in)

         enddo

      enddo

!
!---->computing cross-correlation

      do il = 1_IXP,l

         do ik = 1_IXP,k


!---------->transfering matrix 'b' to matrix 'bkl'

            bkl(:,:) = ZERO_RXP

            do ip = 1_IXP,p

               do iq = 1_IXP,q

                  bkl(ip+ik-1_IXP,iq+il-1_IXP) = b(ip,iq)

               enddo

            enddo

!---------->computing 'at' times 'bkl'

            do is = 1_IXP,s

               do ir = 1_IXP,r

                  bkl(ir,is) = at(ir,is) * bkl(ir,is) !since bkl is real, no need to take its complex conjugate

               enddo

            enddo

!---------->computing cross-correlation

            c(ik,il) = sum(sum(bkl,1_IXP))

         enddo

      enddo

      return

!********************************************************************************************************************************************
   end subroutine cross_correlation_2d
!********************************************************************************************************************************************


!
!
!>@brief subroutine to compute the lag axis of a cross-correlation
!>@param  n : number of elements
!>@param  d : size of one element
!>@return a : lag axis
!********************************************************************************************************************************************
   subroutine cross_correlation_lag_axis(n,d,a)
!********************************************************************************************************************************************

   implicit none

   integer(kind=IXP), intent(in)                             :: n
   real   (kind=RXP), intent(in)                             :: d
   real   (kind=RXP), dimension(:), allocatable, intent(out) :: a

   integer(kind=IXP)                                         :: i
   integer(kind=IXP)                                         :: ios

   ios = init_array_real(a,n,"mod_signal_processing:cross_correlation_lag_axis:a")

   a(1_IXP:n) = -real(n,kind=RXP)*d/2.0_RXP + [ (real(i-1_IXP,kind=RXP)*d,i=1_IXP,n) ]

   return

!********************************************************************************************************************************************
   end subroutine cross_correlation_lag_axis
!********************************************************************************************************************************************

!
!
!>@brief subroutine to compute the variance of a 1D array
!>@param  x : array of rank 1
!>@return v : variance
!********************************************************************************************************************************************
   real(kind=RXP) function variance_1d(x) result(v)
!********************************************************************************************************************************************

      implicit none
 
      real   (kind=RXP), dimension(:), intent(in) :: x

      real   (kind=RXP)                           :: m

      integer(kind=IXP)                           :: i1
      integer(kind=IXP)                           :: n1

      n1 = size(x,1_IXP)

      m = sum(x)/real(n1,kind=RXP)

      v = ZERO_RXP

      do i1 = 1_IXP,n1

         v = v + (x(i1) - m)**2_IXP
      
      enddo

      v = v/real(n1,kind=RXP)

      return

!********************************************************************************************************************************************
   end function variance_1d
!********************************************************************************************************************************************


!
!
!>@brief subroutine to compute the variance of a 2D array
!>@param  x : array of rank 2
!>@return v : variance
!********************************************************************************************************************************************
   real(kind=RXP) function variance_2d(x) result(v)
!********************************************************************************************************************************************

      implicit none
 
      real   (kind=RXP), dimension(:,:), intent(in) :: x

      real   (kind=RXP)                             :: m

      integer(kind=IXP)                             :: i1
      integer(kind=IXP)                             :: i2
      integer(kind=IXP)                             :: n1
      integer(kind=IXP)                             :: n2
      integer(kind=IXP)                             :: n1n2

      n1 = size(x,1_IXP)
      n2 = size(x,2_IXP)

      n1n2 = n1*n2
 
      m = sum(x)/real(n1n2,kind=RXP)

      v = ZERO_RXP

      do i2 = 1_IXP,n2

         do i1 = 1_IXP,n1

            v = v + (x(i1,i2) - m)**2_IXP
       
         enddo

      enddo

      v = v/real(n1n2,kind=RXP)

      return

!********************************************************************************************************************************************
   end function variance_2d
!********************************************************************************************************************************************

end module
