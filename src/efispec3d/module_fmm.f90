!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module for solving the eikonal equation by a fast marching method 
!!based on Yang, J., & Stern, F. (2017). A highly scalable massively parallel fast marching method for the Eikonal equation. Journal of Computational Physics, 332, 333-362.

!>@author F. De Martin

!>@brief
!!This module contains subroutines for making operations necessary for the fast marching method

!***********************************************************************************************************************************************************************************
module mod_fmm
!***********************************************************************************************************************************************************************************
   
   use mod_precision

   use mod_heap

   private

   public  :: fmm_init_heap
   public  :: fmm_update_neighbors
   public  :: fmm_march_narrow_band
   private :: fmm_solve_quadratic
   private :: func_min_heap

   contains


!
!
!>@brief subroutine to initialize heap 
!***********************************************************************************************************************************************************************************
   subroutine fmm_init_heap(vs,u,ds,dd,l,h)
!***********************************************************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:,:), intent(   in) :: vs
      real   (kind=RXP), dimension(:,:), intent(inout) :: u
      real   (kind=RXP)                , intent(   in) :: ds
      real   (kind=RXP)                , intent(   in) :: dd
      integer(kind=IXP), dimension(:,:), intent(inout) :: l
      type   (theap)                   , intent(inout) :: h

      integer(kind=IXP)                                :: nd
      integer(kind=IXP)                                :: ns
      integer(kind=IXP)                                :: id
      integer(kind=IXP)                                :: is

      ns = size(u,1_IXP)
      nd = size(u,2_IXP)

!
!---->init heap size. half of the cells grid may be enough (to be checked)

      call h%init((ns*nd)/2_IXP, 1_IXP, func_min_heap)
     !call h%init(        ns*nd, 1_IXP, func_min_heap)

      do id = ONE_IXP,nd

         do is = ONE_IXP,ns

            if (l(is,id) == ZERO_IXP) call fmm_update_neighbors(vs,u,ds,dd,l,h,is,id)

         enddo

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine fmm_init_heap
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to update neighbors
!***********************************************************************************************************************************************************************************
   subroutine fmm_update_neighbors(vs,u,ds,dd,l,h,is,id)
!***********************************************************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:,:), intent(   in) :: vs
      real   (kind=RXP), dimension(:,:), intent(inout) :: u
      real   (kind=RXP)                , intent(   in) :: ds
      real   (kind=RXP)                , intent(   in) :: dd
      integer(kind=IXP), dimension(:,:), intent(inout) :: l
      type   (theap)                   , intent(inout) :: h
      integer(kind=IXP)                , intent(   in) :: is
      integer(kind=IXP)                , intent(   in) :: id

      real   (kind=RXP)                                :: utmp
      integer(kind=IXP)                                :: iid
      integer(kind=IXP)                                :: iis
      integer(kind=IXP)                                :: jd
      integer(kind=IXP)                                :: js
      integer(kind=IXP)                                :: nd
      integer(kind=IXP)                                :: ns

      ns = size(u,1_IXP)
      nd = size(u,2_IXP)

      do iid = -1_IXP,1_IXP

         jd = id+iid

         if ( (jd > 1_IXP) .and. (jd < nd) ) then!check boundary. strict inegality because jd +/- 1 is accessed in 'fmm_solve_quadratic'

            do iis = -1_IXP,1_IXP

               js = is+iis
          
               if ( (js > 1_IXP) .and. (js < ns) ) then!check boundary. strict inegality because js +/- 1 is accessed in 'fmm_solve_quadratic'

                  if ( (abs(iid) + abs(iis)) == 1_IXP) then
                
                     if (l(js,jd) /= 0_IXP) then!check if not known
                
                        utmp = fmm_solve_quadratic(vs,u,ds,dd,l,js,jd)

                        if (utmp < u(js,jd)) then
                
                           u(js,jd) = utmp
                
                           l(js,jd) = 1_IXP! set grid point js,jd as 'band'
   
                           if (.not.h%belong([js,jd])) then

                              call h%insert([u(js,jd)],[js,jd])
                
                           else
                
                              call h%percolate_down([u(js,jd)],[js,jd])

                           endif

                        endif
                
                     endif
                
                  endif

               endif
          
            enddo

         endif

      enddo
      
      return

!***********************************************************************************************************************************************************************************
   end subroutine fmm_update_neighbors
!***********************************************************************************************************************************************************************************

!
!
!>@brief subroutine to march the narrow band 
!***********************************************************************************************************************************************************************************
   subroutine fmm_march_narrow_band(vs,u,ds,dd,l,h)
!***********************************************************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:,:), intent(   in) :: vs
      real   (kind=RXP), dimension(:,:), intent(inout) :: u
      real   (kind=RXP)                , intent(   in) :: ds
      real   (kind=RXP)                , intent(   in) :: dd
      integer(kind=IXP), dimension(:,:), intent(inout) :: l
      type   (theap)                   , intent(inout) :: h

      integer(kind=IXP)                                :: id
      integer(kind=IXP)                                :: is

      do

         if (h%size() == ZERO_IXP) exit

         call h%peek_indx2grid(ONE_IXP,is,id)

         l(is,id) = ZERO_IXP

         call h%pop()

         call fmm_update_neighbors(vs,u,ds,dd,l,h,is,id)

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine fmm_march_narrow_band
!***********************************************************************************************************************************************************************************


!
!
!***********************************************************************************************************************************************************************************
   real(kind=RXP) function fmm_solve_quadratic(vs,u,ds,dd,l,js,jd) 
!***********************************************************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:,:), intent(in) :: vs
      real   (kind=RXP), dimension(:,:), intent(in) :: u
      real   (kind=RXP)                , intent(in) :: ds
      real   (kind=RXP)                , intent(in) :: dd
      integer(kind=IXP), dimension(:,:), intent(in) :: l
      
      integer(kind=IXP)                , intent(in) :: jd
      integer(kind=IXP)                , intent(in) :: js

      real   (kind=RXP)                             :: ut
      real   (kind=RXP)                             :: us
      real   (kind=RXP)                             :: ud
      real   (kind=RXP)                             :: hs
      real   (kind=RXP)                             :: hd
      real   (kind=RXP)                             :: a
      real   (kind=RXP)                             :: b
      real   (kind=RXP)                             :: c
      real   (kind=RXP)                             :: b24ac

      integer(kind=IXP)                             :: d
      integer(kind=IXP)                             :: jsp1
      integer(kind=IXP)                             :: jsm1
      integer(kind=IXP)                             :: jdp1
      integer(kind=IXP)                             :: jdm1
      integer(kind=IXP)                             :: ns
      integer(kind=IXP)                             :: nd

      ns = size(u,1_IXP)
      nd = size(u,2_IXP)

      fmm_solve_quadratic = huge(fmm_solve_quadratic)

!
!---->s-direction along strike

      d = ZERO_IXP

      jsm1 = js-1_IXP
      jsp1 = js+1_IXP

      if (jsm1 >= 1_IXP .and. jsm1 <= ns) then

         if (l(jsm1,jd) == ZERO_IXP) d = -ONE_IXP

      endif
      
      if (jsp1 >= 1_IXP .and. jsp1 <= ns) then

         if (l(jsp1,jd) == ZERO_IXP) then

            if (d /= ZERO_IXP) then

               d = ONE_IXP

            elseif ( u(jsp1,jd) < u(jsm1,jd) ) then

               d = ONE_IXP

            endif

         endif

      endif

      if (d /= ZERO_IXP) then

         us = u(js+d,jd)
         hs = ONE_RXP/ds

      else

         us = ZERO_RXP
         hs = ZERO_RXP

      endif

!
!---->d-direction along dip

      d = ZERO_IXP

      jdm1 = jd-1_IXP
      jdp1 = jd+1_IXP

      if (jdm1 >= 1_IXP .and. jdm1 <= nd) then

         if (l(js,jdm1) == ZERO_IXP) d = -ONE_IXP

      endif
      
      if (jdp1 >= 1_IXP .and. jdp1 <= nd) then

         if (l(js,jdp1) == ZERO_IXP) then

            if (d /= ZERO_IXP) then

               d = ONE_IXP

            elseif ( u(js,jdp1) < u(js,jdm1) ) then

               d = ONE_IXP

            endif

         endif

      endif

      if (d /= ZERO_IXP) then

         ud = u(js,jd+d)
         hd = ONE_RXP/dd

      else

         ud = ZERO_RXP
         hd = ZERO_RXP

      endif

!
!---->solve

      a = hs*hs + hd*hd

      b = -2.0_RXP*(hs*hs*us + hd*hd*ud)

      c = (hs*hs*us*us + hd*hd*ud*ud) - (vs(js,jd))**(-2_IXP)

      b24ac = b**2_IXP - 4.0_RXP*a*c

      if (b24ac >= ZERO_RXP) then

         ut = (-b+sqrt(b24ac))/(2.0_RXP*a)

         if ( (us < ut) .and. (ud < ut) ) fmm_solve_quadratic = ut

      endif

      return

!***********************************************************************************************************************************************************************************
   end function fmm_solve_quadratic
!***********************************************************************************************************************************************************************************


!
!
!***********************************************************************************************************************************************************************************
   logical(kind=IXP) function func_min_heap( node1, node2 )
!***********************************************************************************************************************************************************************************

      real(kind=RXP), dimension(:), intent(in) :: node1
      real(kind=RXP), dimension(:), intent(in) :: node2

      func_min_heap = node1(ONE_IXP) < node2(ONE_IXP)

!***********************************************************************************************************************************************************************************
   end function func_min_heap
!***********************************************************************************************************************************************************************************
 
        
!***********************************************************************************************************************************************************************************
end module mod_fmm
!***********************************************************************************************************************************************************************************
