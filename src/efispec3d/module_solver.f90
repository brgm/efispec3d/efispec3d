!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a modulme to compute Newmark explicit time marching scheme, external forces @f$ F^{ext} @f$, internal forces @f$ KU @f$ and boundary traction forces @f$ C\dot{U} @f$ of the system @f$ M\ddot{U} + C\dot{U} + KU = F^{ext} @f$.

!>@brief
!!This module contains subroutines to compute Newmark explicit time marching scheme, external forces @f$ F^{ext} @f$, internal forces @f$ KU @f$ and boundary traction forces @f$ C\dot{U} @f$ of the system @f$ M\ddot{U} + C\dot{U} + KU = F^{ext} @f$.

module mod_solver

   use mpi

   use mod_precision

   implicit none

   public :: newmark_ini
   public :: newmark_end
   public :: compute_internal_forces_order4
   public :: compute_internal_forces_order5
   public :: compute_internal_forces_order6
   public :: compute_absorption_forces
   public :: compute_external_forces
   public :: compute_plane_wave
   public :: compute_absorption_forces_bottom_only
   public :: compute_dirichlet_conditions
   public :: compute_div_curl_gll

   contains

!
!
!>@brief This subroutine initializes Newmark time marching scheme at step n+1
!>@return displacements at step n+1     : see global variable mod_global_variables::rg_gll_displacement
!>@return external forces flush to zero : see global variable mod_global_variables::rg_gll_acceleration
!***********************************************************************************************************************************************************************************
      subroutine newmark_ini()
!***********************************************************************************************************************************************************************************

         use mpi

         use mod_global_variables, only :&
                                         ig_ngll_total&
                                        ,rg_gll_displacement&
                                        ,rg_gll_velocity&
                                        ,rg_gll_acceleration&
                                        ,rg_dt&
                                        ,rg_dt2&
                                        ,RG_NEWMARK_GAMMA
   
         implicit none
   
         integer(kind=IXP) :: igll

         do igll = ONE_IXP,ig_ngll_total
            rg_gll_displacement(ONE_IXP  ,igll) = rg_gll_displacement(ONE_IXP  ,igll) + rg_dt*rg_gll_velocity(ONE_IXP  ,igll) + rg_dt2*0.5_RXP*rg_gll_acceleration(ONE_IXP  ,igll) !displacement x
            rg_gll_displacement(TWO_IXP  ,igll) = rg_gll_displacement(TWO_IXP  ,igll) + rg_dt*rg_gll_velocity(TWO_IXP  ,igll) + rg_dt2*0.5_RXP*rg_gll_acceleration(TWO_IXP  ,igll) !dispalcement y
            rg_gll_displacement(THREE_IXP,igll) = rg_gll_displacement(THREE_IXP,igll) + rg_dt*rg_gll_velocity(THREE_IXP,igll) + rg_dt2*0.5_RXP*rg_gll_acceleration(THREE_IXP,igll) !displacement z
         enddo

         do igll = ONE_IXP,ig_ngll_total
            rg_gll_acceleration(ONE_IXP  ,igll) = ZERO_RXP !flush to zero acceleration x
            rg_gll_acceleration(TWO_IXP  ,igll) = ZERO_RXP !flush to zero acceleration y
            rg_gll_acceleration(THREE_IXP,igll) = ZERO_RXP !flush to zero acceleration z
         enddo
   
         return
!***********************************************************************************************************************************************************************************
      end subroutine newmark_ini
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine finalizes Newmark time marching scheme at step n+1
!***********************************************************************************************************************************************************************************
      subroutine newmark_end()
!***********************************************************************************************************************************************************************************

         use mpi

         use mod_global_variables, only :&
                                         ig_ngll_total&
                                        ,rg_gll_velocity&
                                        ,rg_gll_acceleration&
                                        ,rg_gll_acctmp&
                                        ,rg_dt&
                                        ,RG_NEWMARK_GAMMA&
                                        ,rg_gll_mass_matrix
         implicit none
   
         integer(kind=IXP) :: igll

         do igll = ONE_IXP,ig_ngll_total
            rg_gll_acceleration(ONE_IXP  ,igll) = rg_gll_acceleration(ONE_IXP  ,igll)*rg_gll_mass_matrix(igll) !acceleration x step n+1
            rg_gll_acceleration(TWO_IXP  ,igll) = rg_gll_acceleration(TWO_IXP  ,igll)*rg_gll_mass_matrix(igll) !acceleration y step n+1
            rg_gll_acceleration(THREE_IXP,igll) = rg_gll_acceleration(THREE_IXP,igll)*rg_gll_mass_matrix(igll) !acceleration z step n+1
         enddo

         do igll = ONE_IXP,ig_ngll_total
            rg_gll_velocity(ONE_IXP  ,igll) = rg_gll_velocity(ONE_IXP  ,igll) + rg_dt*((ONE_RXP-RG_NEWMARK_GAMMA)*rg_gll_acctmp(ONE_IXP  ,igll) + RG_NEWMARK_GAMMA*rg_gll_acceleration(ONE_IXP  ,igll)) !velocity x step n+1
            rg_gll_velocity(TWO_IXP  ,igll) = rg_gll_velocity(TWO_IXP  ,igll) + rg_dt*((ONE_RXP-RG_NEWMARK_GAMMA)*rg_gll_acctmp(TWO_IXP  ,igll) + RG_NEWMARK_GAMMA*rg_gll_acceleration(TWO_IXP  ,igll)) !velocity y step n+1
            rg_gll_velocity(THREE_IXP,igll) = rg_gll_velocity(THREE_IXP,igll) + rg_dt*((ONE_RXP-RG_NEWMARK_GAMMA)*rg_gll_acctmp(THREE_IXP,igll) + RG_NEWMARK_GAMMA*rg_gll_acceleration(THREE_IXP,igll)) !velocity z step n+1
         enddo

         do igll = ONE_IXP,ig_ngll_total
            rg_gll_acctmp(ONE_IXP  ,igll)   = rg_gll_acceleration(ONE_IXP,igll) !store tmp acceleration at step n for next step n+1
            rg_gll_acctmp(TWO_IXP  ,igll)   = rg_gll_acceleration(TWO_IXP,igll) !store tmp acceleration at step n for next step n+1
            rg_gll_acctmp(THREE_IXP,igll)   = rg_gll_acceleration(THREE_IXP,igll) !store tmp acceleration at step n for next step n+1
         enddo

         return
!***********************************************************************************************************************************************************************************
      end subroutine newmark_end
!***********************************************************************************************************************************************************************************
   
!
!
!>@brief
!!This subroutine computes internal forces @f$ \int _{\Omega}  \boldsymbol{\epsilon}(\mathbf{v}) ^{T} \colon \boldsymbol{\tau} \, d\Omega @f$ for spectral-elements of order 4.
!!Stress-strain relationship can be linear elastic (general isotropic fourth-order Hooke's law for continuous media) or viscoelastic (memory variables method).
!>@param elt_start : first hexahedron element of the loop 
!>@param elt_end   : last  hexahedron element of the loop 
!***********************************************************************************************************************************************************************************
      subroutine compute_internal_forces_order4(elt_start,elt_end)
!***********************************************************************************************************************************************************************************

         use mpi

         use mod_global_variables, only :&
                                         IG_NGLL&
                                        ,IG_NDOF&
                                        ,rg_gll_lagrange_deriv&
                                        ,rg_gll_displacement&
                                        ,rg_gll_acceleration&
                                        ,rg_gll_weight&
                                        ,ig_hexa_gll_glonum&
                                        ,rg_hexa_gll_dxidx&
                                        ,rg_hexa_gll_dxidy&
                                        ,rg_hexa_gll_dxidz&
                                        ,rg_hexa_gll_detdx&
                                        ,rg_hexa_gll_detdy&
                                        ,rg_hexa_gll_detdz&
                                        ,rg_hexa_gll_dzedx&
                                        ,rg_hexa_gll_dzedy&
                                        ,rg_hexa_gll_dzedz&
                                        ,rg_hexa_gll_jacobian_det&
                                        ,rg_hexa_gll_rhovs2&
                                        ,rg_hexa_gll_rhovp2&
                                        ,rg_hexa_gll_rhovs2&
                                        ,rg_hexa_gll_rhovp2&
                                        ,rg_hexa_gll_wkqs&
                                        ,rg_hexa_gll_wkqp&
                                        ,rg_hexa_gll_ksixx&
                                        ,rg_hexa_gll_ksiyy&
                                        ,rg_hexa_gll_ksizz&
                                        ,rg_hexa_gll_ksixy&
                                        ,rg_hexa_gll_ksixz&
                                        ,rg_hexa_gll_ksiyz&
                                        ,RG_RELAX_COEFF&
                                        ,rg_mem_var_exp&
                                        ,IG_NRELAX&
                                        ,LG_VISCO
         
         implicit none
         
         integer(kind=IXP), intent(in)                                 :: elt_start
         integer(kind=IXP), intent(in)                                 :: elt_end

         real   (kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: rl_displacement_gll
         real   (kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: rl_acceleration_gll
         
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpx1
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpx2
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpx3
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpy1
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpy2
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpy3
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpz1
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpz2
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpz3

         real   (kind=RXP)                                             :: duxdxi
         real   (kind=RXP)                                             :: duxdet
         real   (kind=RXP)                                             :: duxdze
         real   (kind=RXP)                                             :: duydxi
         real   (kind=RXP)                                             :: duydet
         real   (kind=RXP)                                             :: duydze
         real   (kind=RXP)                                             :: duzdxi
         real   (kind=RXP)                                             :: duzdet
         real   (kind=RXP)                                             :: duzdze
                                                                       
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: duxdx
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: duydy
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: duzdz
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: duxdy
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: duxdz
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: duydx
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: duydz
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: duzdx
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: duzdy
                                                                       
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: dxidx
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: dxidy
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: dxidz
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: detdx
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: detdy
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: detdz
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: dzedx
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: dzedy
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: dzedz
                                                                       
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: tauxx
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: tauyy
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: tauzz
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: tauxy
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: tauxz
         real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: tauyz
                                                                       
         real   (kind=RXP)                                             :: tauxx_n12
         real   (kind=RXP)                                             :: tauyy_n12
         real   (kind=RXP)                                             :: tauzz_n12
         real   (kind=RXP)                                             :: tauxy_n12
         real   (kind=RXP)                                             :: tauxz_n12
         real   (kind=RXP)                                             :: tauyz_n12
         real   (kind=RXP)                                             :: trace_tau
                                                                       
         real   (kind=RXP)                                             :: tmpx1
         real   (kind=RXP)                                             :: tmpx2
         real   (kind=RXP)                                             :: tmpx3
         real   (kind=RXP)                                             :: tmpx4
         real   (kind=RXP)                                             :: tmpy1
         real   (kind=RXP)                                             :: tmpy2
         real   (kind=RXP)                                             :: tmpy3
         real   (kind=RXP)                                             :: tmpz1
         real   (kind=RXP)                                             :: tmpz2
         real   (kind=RXP)                                             :: tmpz3
         real   (kind=RXP)                                             :: fac1
         real   (kind=RXP)                                             :: fac2
         real   (kind=RXP)                                             :: fac3
                                                                    
         integer(kind=IXP)                                             :: iel
         integer(kind=IXP)                                             :: k
         integer(kind=IXP)                                             :: l
         integer(kind=IXP)                                             :: m
         integer(kind=IXP)                                             :: igll
         integer(kind=IXP)                                             :: imem_var


         do iel = elt_start,elt_end
   
!
!---------->fill local displacement

            do k = 1_IXP,IG_NGLL        !zeta
               do l = 1_IXP,IG_NGLL     !eta
                  do m = 1_IXP,IG_NGLL  !xi
   
                     igll                             = ig_hexa_gll_glonum(m,l,k,iel)
   
                     rl_displacement_gll(1_IXP,m,l,k) = rg_gll_displacement(1_IXP,igll)
                     rl_displacement_gll(2_IXP,m,l,k) = rg_gll_displacement(2_IXP,igll)
                     rl_displacement_gll(3_IXP,m,l,k) = rg_gll_displacement(3_IXP,igll)
   
                  enddo
               enddo
            enddo
!
!
!*********************************************************************************************
!           compute integrale at GLL nodes + assemble forces in global gll grid for hexa
!*********************************************************************************************

            do k = 1_IXP,IG_NGLL        !zeta
               do l = 1_IXP,IG_NGLL     !eta
                  do m = 1_IXP,IG_NGLL  !xi
   
!
!------------------->derivative of displacement with respect to local coordinate xi, eta and zeta at the gll node klm
   
                     duxdxi = rl_displacement_gll(1_IXP,1_IXP,l,k)*rg_gll_lagrange_deriv(1_IXP,m) &
                            + rl_displacement_gll(1_IXP,2_IXP,l,k)*rg_gll_lagrange_deriv(2_IXP,m) &
                            + rl_displacement_gll(1_IXP,3_IXP,l,k)*rg_gll_lagrange_deriv(3_IXP,m) &
                            + rl_displacement_gll(1_IXP,4_IXP,l,k)*rg_gll_lagrange_deriv(4_IXP,m) &
                            + rl_displacement_gll(1_IXP,5_IXP,l,k)*rg_gll_lagrange_deriv(5_IXP,m) 
   
                     duydxi = rl_displacement_gll(2_IXP,1_IXP,l,k)*rg_gll_lagrange_deriv(1_IXP,m) &
                            + rl_displacement_gll(2_IXP,2_IXP,l,k)*rg_gll_lagrange_deriv(2_IXP,m) &
                            + rl_displacement_gll(2_IXP,3_IXP,l,k)*rg_gll_lagrange_deriv(3_IXP,m) &
                            + rl_displacement_gll(2_IXP,4_IXP,l,k)*rg_gll_lagrange_deriv(4_IXP,m) &
                            + rl_displacement_gll(2_IXP,5_IXP,l,k)*rg_gll_lagrange_deriv(5_IXP,m) 
   
                     duzdxi = rl_displacement_gll(3_IXP,1_IXP,l,k)*rg_gll_lagrange_deriv(1_IXP,m) &
                            + rl_displacement_gll(3_IXP,2_IXP,l,k)*rg_gll_lagrange_deriv(2_IXP,m) &
                            + rl_displacement_gll(3_IXP,3_IXP,l,k)*rg_gll_lagrange_deriv(3_IXP,m) &
                            + rl_displacement_gll(3_IXP,4_IXP,l,k)*rg_gll_lagrange_deriv(4_IXP,m) &
                            + rl_displacement_gll(3_IXP,5_IXP,l,k)*rg_gll_lagrange_deriv(5_IXP,m)
   
                     duxdet = rl_displacement_gll(1_IXP,m,1_IXP,k)*rg_gll_lagrange_deriv(1_IXP,l) &
                            + rl_displacement_gll(1_IXP,m,2_IXP,k)*rg_gll_lagrange_deriv(2_IXP,l) &
                            + rl_displacement_gll(1_IXP,m,3_IXP,k)*rg_gll_lagrange_deriv(3_IXP,l) &
                            + rl_displacement_gll(1_IXP,m,4_IXP,k)*rg_gll_lagrange_deriv(4_IXP,l) &
                            + rl_displacement_gll(1_IXP,m,5_IXP,k)*rg_gll_lagrange_deriv(5_IXP,l)
   
                     duydet = rl_displacement_gll(2_IXP,m,1_IXP,k)*rg_gll_lagrange_deriv(1_IXP,l) &
                            + rl_displacement_gll(2_IXP,m,2_IXP,k)*rg_gll_lagrange_deriv(2_IXP,l) &
                            + rl_displacement_gll(2_IXP,m,3_IXP,k)*rg_gll_lagrange_deriv(3_IXP,l) &
                            + rl_displacement_gll(2_IXP,m,4_IXP,k)*rg_gll_lagrange_deriv(4_IXP,l) &
                            + rl_displacement_gll(2_IXP,m,5_IXP,k)*rg_gll_lagrange_deriv(5_IXP,l)
   
                     duzdet = rl_displacement_gll(3_IXP,m,1_IXP,k)*rg_gll_lagrange_deriv(1_IXP,l) &
                            + rl_displacement_gll(3_IXP,m,2_IXP,k)*rg_gll_lagrange_deriv(2_IXP,l) &
                            + rl_displacement_gll(3_IXP,m,3_IXP,k)*rg_gll_lagrange_deriv(3_IXP,l) &
                            + rl_displacement_gll(3_IXP,m,4_IXP,k)*rg_gll_lagrange_deriv(4_IXP,l) &
                            + rl_displacement_gll(3_IXP,m,5_IXP,k)*rg_gll_lagrange_deriv(5_IXP,l)
   
                     duxdze = rl_displacement_gll(1_IXP,m,l,1_IXP)*rg_gll_lagrange_deriv(1_IXP,k) &
                            + rl_displacement_gll(1_IXP,m,l,2_IXP)*rg_gll_lagrange_deriv(2_IXP,k) &
                            + rl_displacement_gll(1_IXP,m,l,3_IXP)*rg_gll_lagrange_deriv(3_IXP,k) &
                            + rl_displacement_gll(1_IXP,m,l,4_IXP)*rg_gll_lagrange_deriv(4_IXP,k) &
                            + rl_displacement_gll(1_IXP,m,l,5_IXP)*rg_gll_lagrange_deriv(5_IXP,k) 
   
                     duydze = rl_displacement_gll(2_IXP,m,l,1_IXP)*rg_gll_lagrange_deriv(1_IXP,k) &
                            + rl_displacement_gll(2_IXP,m,l,2_IXP)*rg_gll_lagrange_deriv(2_IXP,k) &
                            + rl_displacement_gll(2_IXP,m,l,3_IXP)*rg_gll_lagrange_deriv(3_IXP,k) &
                            + rl_displacement_gll(2_IXP,m,l,4_IXP)*rg_gll_lagrange_deriv(4_IXP,k) &
                            + rl_displacement_gll(2_IXP,m,l,5_IXP)*rg_gll_lagrange_deriv(5_IXP,k) 
   
                     duzdze = rl_displacement_gll(3_IXP,m,l,1_IXP)*rg_gll_lagrange_deriv(1_IXP,k) &
                            + rl_displacement_gll(3_IXP,m,l,2_IXP)*rg_gll_lagrange_deriv(2_IXP,k) &
                            + rl_displacement_gll(3_IXP,m,l,3_IXP)*rg_gll_lagrange_deriv(3_IXP,k) &
                            + rl_displacement_gll(3_IXP,m,l,4_IXP)*rg_gll_lagrange_deriv(4_IXP,k) &
                            + rl_displacement_gll(3_IXP,m,l,5_IXP)*rg_gll_lagrange_deriv(5_IXP,k)
   
!      
!------------------->derivative of displacement at step n+1 with respect to global coordinate x, y and z at the gll node klm

                     dxidx(m,l,k) = rg_hexa_gll_dxidx(m,l,k,iel)
                     dxidy(m,l,k) = rg_hexa_gll_dxidy(m,l,k,iel)
                     dxidz(m,l,k) = rg_hexa_gll_dxidz(m,l,k,iel)
                     detdx(m,l,k) = rg_hexa_gll_detdx(m,l,k,iel)
                     detdy(m,l,k) = rg_hexa_gll_detdy(m,l,k,iel)
                     detdz(m,l,k) = rg_hexa_gll_detdz(m,l,k,iel)
                     dzedx(m,l,k) = rg_hexa_gll_dzedx(m,l,k,iel)
                     dzedy(m,l,k) = rg_hexa_gll_dzedy(m,l,k,iel)
                     dzedz(m,l,k) = rg_hexa_gll_dzedz(m,l,k,iel)
   
                     duxdx(m,l,k) = duxdxi*dxidx(m,l,k) + duxdet*detdx(m,l,k) + duxdze*dzedx(m,l,k)
                     duxdy(m,l,k) = duxdxi*dxidy(m,l,k) + duxdet*detdy(m,l,k) + duxdze*dzedy(m,l,k)
                     duxdz(m,l,k) = duxdxi*dxidz(m,l,k) + duxdet*detdz(m,l,k) + duxdze*dzedz(m,l,k)
                     duydx(m,l,k) = duydxi*dxidx(m,l,k) + duydet*detdx(m,l,k) + duydze*dzedx(m,l,k)
                     duydy(m,l,k) = duydxi*dxidy(m,l,k) + duydet*detdy(m,l,k) + duydze*dzedy(m,l,k)
                     duydz(m,l,k) = duydxi*dxidz(m,l,k) + duydet*detdz(m,l,k) + duydze*dzedz(m,l,k)
                     duzdx(m,l,k) = duzdxi*dxidx(m,l,k) + duzdet*detdx(m,l,k) + duzdze*dzedx(m,l,k)
                     duzdy(m,l,k) = duzdxi*dxidy(m,l,k) + duzdet*detdy(m,l,k) + duzdze*dzedy(m,l,k)
                     duzdz(m,l,k) = duzdxi*dxidz(m,l,k) + duzdet*detdz(m,l,k) + duzdze*dzedz(m,l,k)
!
!------------------->compute elastic stress (elastic simulation) or unrelaxed elastic stress (viscoelastic simulation)

                     trace_tau    = (rg_hexa_gll_rhovp2(m,l,k,iel) - TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel))*(duxdx(m,l,k)+duydy(m,l,k)+duzdz(m,l,k))
                     tauxx(m,l,k) = trace_tau + TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*duxdx(m,l,k)
                     tauyy(m,l,k) = trace_tau + TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*duydy(m,l,k)
                     tauzz(m,l,k) = trace_tau + TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*duzdz(m,l,k)
                     tauxy(m,l,k) =                     rg_hexa_gll_rhovs2(m,l,k,iel)*(duxdy(m,l,k)+duydx(m,l,k))
                     tauxz(m,l,k) =                     rg_hexa_gll_rhovs2(m,l,k,iel)*(duxdz(m,l,k)+duzdx(m,l,k))
                     tauyz(m,l,k) =                     rg_hexa_gll_rhovs2(m,l,k,iel)*(duydz(m,l,k)+duzdy(m,l,k))

                  enddo !xi
               enddo    !eta
            enddo       !zeta

!
!---------->compute viscoelastic stress
           
            if (LG_VISCO) then

               do k = 1_IXP,IG_NGLL
                  do l = 1_IXP,IG_NGLL
                     do m = 1_IXP,IG_NGLL

                        do imem_var = 1_IXP,IG_NRELAX
                      
                           tmpx1 = rg_mem_var_exp(imem_var)
                      
                           tmpx2 =         rg_hexa_gll_rhovp2(m,l,k,iel)*rg_hexa_gll_wkqp(imem_var,m,l,k,iel)
                           tmpx3 = TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*rg_hexa_gll_wkqs(imem_var,m,l,k,iel)
                      
                           tmpx4 = (duxdx(m,l,k)+duydy(m,l,k)+duzdz(m,l,k))*(tmpx2 - tmpx3)
                      
!                     
!------------------------->anelastic stress at step n+1/2 following s. ma and p. liu (2006) using epsnp1 and unrelaxed material modulus
                      
                           tauxx_n12 = tmpx1*rg_hexa_gll_ksixx(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*duxdx(m,l,k) + tmpx4)
                           tauyy_n12 = tmpx1*rg_hexa_gll_ksiyy(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*duydy(m,l,k) + tmpx4)
                           tauzz_n12 = tmpx1*rg_hexa_gll_ksizz(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*duzdz(m,l,k) + tmpx4)
                           tauxy_n12 = tmpx1*rg_hexa_gll_ksixy(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*ONE_HALF_RXP*(duxdy(m,l,k)+duydx(m,l,k)))
                           tauxz_n12 = tmpx1*rg_hexa_gll_ksixz(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*ONE_HALF_RXP*(duxdz(m,l,k)+duzdx(m,l,k)))
                           tauyz_n12 = tmpx1*rg_hexa_gll_ksiyz(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*ONE_HALF_RXP*(duydz(m,l,k)+duzdy(m,l,k)))
!                     
!------------------------->compute final stress at step n+1 according to day and minster (1984) + ma and liu (2006) : tauxx - SUM anelastic stress at step n+1
                      
                           tauxx(m,l,k) = tauxx(m,l,k) - ONE_HALF_RXP*(tauxx_n12 + rg_hexa_gll_ksixx(imem_var,m,l,k,iel))
                           tauyy(m,l,k) = tauyy(m,l,k) - ONE_HALF_RXP*(tauyy_n12 + rg_hexa_gll_ksiyy(imem_var,m,l,k,iel))
                           tauzz(m,l,k) = tauzz(m,l,k) - ONE_HALF_RXP*(tauzz_n12 + rg_hexa_gll_ksizz(imem_var,m,l,k,iel))
                           tauxy(m,l,k) = tauxy(m,l,k) - ONE_HALF_RXP*(tauxy_n12 + rg_hexa_gll_ksixy(imem_var,m,l,k,iel))
                           tauxz(m,l,k) = tauxz(m,l,k) - ONE_HALF_RXP*(tauxz_n12 + rg_hexa_gll_ksixz(imem_var,m,l,k,iel))
                           tauyz(m,l,k) = tauyz(m,l,k) - ONE_HALF_RXP*(tauyz_n12 + rg_hexa_gll_ksiyz(imem_var,m,l,k,iel))
!                     
!------------------------->update memory for stress (step n+1/2)
                      
                           rg_hexa_gll_ksixx(imem_var,m,l,k,iel) = tauxx_n12
                           rg_hexa_gll_ksiyy(imem_var,m,l,k,iel) = tauyy_n12
                           rg_hexa_gll_ksizz(imem_var,m,l,k,iel) = tauzz_n12
                           rg_hexa_gll_ksixy(imem_var,m,l,k,iel) = tauxy_n12
                           rg_hexa_gll_ksixz(imem_var,m,l,k,iel) = tauxz_n12
                           rg_hexa_gll_ksiyz(imem_var,m,l,k,iel) = tauyz_n12
                      
                        enddo

                     enddo
                  enddo
               enddo

            endif

            do k = 1_IXP,IG_NGLL        !zeta
               do l = 1_IXP,IG_NGLL     !eta
                  do m = 1_IXP,IG_NGLL  !xi

!      
!------------------->store members of integration of the gll node klm

                     intpx1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx(m,l,k)*dxidx(m,l,k)+tauxy(m,l,k)*dxidy(m,l,k)+tauxz(m,l,k)*dxidz(m,l,k))
                     intpx2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx(m,l,k)*detdx(m,l,k)+tauxy(m,l,k)*detdy(m,l,k)+tauxz(m,l,k)*detdz(m,l,k))
                     intpx3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx(m,l,k)*dzedx(m,l,k)+tauxy(m,l,k)*dzedy(m,l,k)+tauxz(m,l,k)*dzedz(m,l,k))
   
                     intpy1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy(m,l,k)*dxidx(m,l,k)+tauyy(m,l,k)*dxidy(m,l,k)+tauyz(m,l,k)*dxidz(m,l,k))
                     intpy2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy(m,l,k)*detdx(m,l,k)+tauyy(m,l,k)*detdy(m,l,k)+tauyz(m,l,k)*detdz(m,l,k))
                     intpy3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy(m,l,k)*dzedx(m,l,k)+tauyy(m,l,k)*dzedy(m,l,k)+tauyz(m,l,k)*dzedz(m,l,k))
   
                     intpz1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz(m,l,k)*dxidx(m,l,k)+tauyz(m,l,k)*dxidy(m,l,k)+tauzz(m,l,k)*dxidz(m,l,k))
                     intpz2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz(m,l,k)*detdx(m,l,k)+tauyz(m,l,k)*detdy(m,l,k)+tauzz(m,l,k)*detdz(m,l,k))
                     intpz3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz(m,l,k)*dzedx(m,l,k)+tauyz(m,l,k)*dzedy(m,l,k)+tauzz(m,l,k)*dzedz(m,l,k))
   
                  enddo !xi
               enddo    !eta
            enddo       !zeta
   
!
!---------->finish integration for hexa (internal forces at step n+1)

            do k = 1_IXP,IG_NGLL
               do l = 1_IXP,IG_NGLL
                  do m = 1_IXP,IG_NGLL
   
                     tmpx1 = intpx1(1_IXP,l,k)*rg_gll_lagrange_deriv(m,1_IXP)*rg_gll_weight(1_IXP) &
                           + intpx1(2_IXP,l,k)*rg_gll_lagrange_deriv(m,2_IXP)*rg_gll_weight(2_IXP) &
                           + intpx1(3_IXP,l,k)*rg_gll_lagrange_deriv(m,3_IXP)*rg_gll_weight(3_IXP) &
                           + intpx1(4_IXP,l,k)*rg_gll_lagrange_deriv(m,4_IXP)*rg_gll_weight(4_IXP) &
                           + intpx1(5_IXP,l,k)*rg_gll_lagrange_deriv(m,5_IXP)*rg_gll_weight(5_IXP)
   
                     tmpy1 = intpy1(1_IXP,l,k)*rg_gll_lagrange_deriv(m,1_IXP)*rg_gll_weight(1_IXP) &
                           + intpy1(2_IXP,l,k)*rg_gll_lagrange_deriv(m,2_IXP)*rg_gll_weight(2_IXP) &
                           + intpy1(3_IXP,l,k)*rg_gll_lagrange_deriv(m,3_IXP)*rg_gll_weight(3_IXP) &
                           + intpy1(4_IXP,l,k)*rg_gll_lagrange_deriv(m,4_IXP)*rg_gll_weight(4_IXP) &
                           + intpy1(5_IXP,l,k)*rg_gll_lagrange_deriv(m,5_IXP)*rg_gll_weight(5_IXP)
   
                     tmpz1 = intpz1(1_IXP,l,k)*rg_gll_lagrange_deriv(m,1_IXP)*rg_gll_weight(1_IXP) &
                           + intpz1(2_IXP,l,k)*rg_gll_lagrange_deriv(m,2_IXP)*rg_gll_weight(2_IXP) &
                           + intpz1(3_IXP,l,k)*rg_gll_lagrange_deriv(m,3_IXP)*rg_gll_weight(3_IXP) &
                           + intpz1(4_IXP,l,k)*rg_gll_lagrange_deriv(m,4_IXP)*rg_gll_weight(4_IXP) &
                           + intpz1(5_IXP,l,k)*rg_gll_lagrange_deriv(m,5_IXP)*rg_gll_weight(5_IXP)
   
                     tmpx2 = intpx2(m,1_IXP,k)*rg_gll_lagrange_deriv(l,1_IXP)*rg_gll_weight(1_IXP) &
                           + intpx2(m,2_IXP,k)*rg_gll_lagrange_deriv(l,2_IXP)*rg_gll_weight(2_IXP) &
                           + intpx2(m,3_IXP,k)*rg_gll_lagrange_deriv(l,3_IXP)*rg_gll_weight(3_IXP) &
                           + intpx2(m,4_IXP,k)*rg_gll_lagrange_deriv(l,4_IXP)*rg_gll_weight(4_IXP) &
                           + intpx2(m,5_IXP,k)*rg_gll_lagrange_deriv(l,5_IXP)*rg_gll_weight(5_IXP)
   
                     tmpy2 = intpy2(m,1_IXP,k)*rg_gll_lagrange_deriv(l,1_IXP)*rg_gll_weight(1_IXP) &
                           + intpy2(m,2_IXP,k)*rg_gll_lagrange_deriv(l,2_IXP)*rg_gll_weight(2_IXP) &
                           + intpy2(m,3_IXP,k)*rg_gll_lagrange_deriv(l,3_IXP)*rg_gll_weight(3_IXP) &
                           + intpy2(m,4_IXP,k)*rg_gll_lagrange_deriv(l,4_IXP)*rg_gll_weight(4_IXP) &
                           + intpy2(m,5_IXP,k)*rg_gll_lagrange_deriv(l,5_IXP)*rg_gll_weight(5_IXP)
   
                     tmpz2 = intpz2(m,1_IXP,k)*rg_gll_lagrange_deriv(l,1_IXP)*rg_gll_weight(1_IXP) &
                           + intpz2(m,2_IXP,k)*rg_gll_lagrange_deriv(l,2_IXP)*rg_gll_weight(2_IXP) &
                           + intpz2(m,3_IXP,k)*rg_gll_lagrange_deriv(l,3_IXP)*rg_gll_weight(3_IXP) &
                           + intpz2(m,4_IXP,k)*rg_gll_lagrange_deriv(l,4_IXP)*rg_gll_weight(4_IXP) &
                           + intpz2(m,5_IXP,k)*rg_gll_lagrange_deriv(l,5_IXP)*rg_gll_weight(5_IXP)
   
                     tmpx3 = intpx3(m,l,1_IXP)*rg_gll_lagrange_deriv(k,1_IXP)*rg_gll_weight(1_IXP) &
                           + intpx3(m,l,2_IXP)*rg_gll_lagrange_deriv(k,2_IXP)*rg_gll_weight(2_IXP) &
                           + intpx3(m,l,3_IXP)*rg_gll_lagrange_deriv(k,3_IXP)*rg_gll_weight(3_IXP) &
                           + intpx3(m,l,4_IXP)*rg_gll_lagrange_deriv(k,4_IXP)*rg_gll_weight(4_IXP) &
                           + intpx3(m,l,5_IXP)*rg_gll_lagrange_deriv(k,5_IXP)*rg_gll_weight(5_IXP)
   
                     tmpy3 = intpy3(m,l,1_IXP)*rg_gll_lagrange_deriv(k,1_IXP)*rg_gll_weight(1_IXP) &
                           + intpy3(m,l,2_IXP)*rg_gll_lagrange_deriv(k,2_IXP)*rg_gll_weight(2_IXP) &
                           + intpy3(m,l,3_IXP)*rg_gll_lagrange_deriv(k,3_IXP)*rg_gll_weight(3_IXP) &
                           + intpy3(m,l,4_IXP)*rg_gll_lagrange_deriv(k,4_IXP)*rg_gll_weight(4_IXP) &
                           + intpy3(m,l,5_IXP)*rg_gll_lagrange_deriv(k,5_IXP)*rg_gll_weight(5_IXP)
   
                     tmpz3 = intpz3(m,l,1_IXP)*rg_gll_lagrange_deriv(k,1_IXP)*rg_gll_weight(1_IXP) &
                           + intpz3(m,l,2_IXP)*rg_gll_lagrange_deriv(k,2_IXP)*rg_gll_weight(2_IXP) &
                           + intpz3(m,l,3_IXP)*rg_gll_lagrange_deriv(k,3_IXP)*rg_gll_weight(3_IXP) &
                           + intpz3(m,l,4_IXP)*rg_gll_lagrange_deriv(k,4_IXP)*rg_gll_weight(4_IXP) &
                           + intpz3(m,l,5_IXP)*rg_gll_lagrange_deriv(k,5_IXP)*rg_gll_weight(5_IXP) 
   
                     fac1 = rg_gll_weight(l)*rg_gll_weight(k)
                     fac2 = rg_gll_weight(m)*rg_gll_weight(k)
                     fac3 = rg_gll_weight(m)*rg_gll_weight(l)
   
                     rl_acceleration_gll(1_IXP,m,l,k) = (fac1*tmpx1 + fac2*tmpx2 + fac3*tmpx3)
                     rl_acceleration_gll(2_IXP,m,l,k) = (fac1*tmpy1 + fac2*tmpy2 + fac3*tmpy3)
                     rl_acceleration_gll(3_IXP,m,l,k) = (fac1*tmpz1 + fac2*tmpz2 + fac3*tmpz3)
   
                  enddo
               enddo
            enddo

!
!---------->local to global indirection
   
            do k = 1_IXP,IG_NGLL        !zeta
               do l = 1_IXP,IG_NGLL     !eta
                  do m = 1_IXP,IG_NGLL  !xi
   
                     igll                            = ig_hexa_gll_glonum(m,l,k,iel)
   
                     rg_gll_acceleration(1_IXP,igll) = rg_gll_acceleration(1_IXP,igll) - rl_acceleration_gll(1_IXP,m,l,k)
                     rg_gll_acceleration(2_IXP,igll) = rg_gll_acceleration(2_IXP,igll) - rl_acceleration_gll(2_IXP,m,l,k)
                     rg_gll_acceleration(3_IXP,igll) = rg_gll_acceleration(3_IXP,igll) - rl_acceleration_gll(3_IXP,m,l,k)
   
                  enddo
               enddo
            enddo
   
         enddo !loop on hexahedron elements
   
         return
!***********************************************************************************************************************************************************************************
      end subroutine compute_internal_forces_order4
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine computes internal forces @f$ \int _{\Omega}  \boldsymbol{\epsilon}(\mathbf{v}) ^{T} \colon \boldsymbol{\tau} \, d\Omega @f$ for spectral-elements of order 5.
!!Stress-strain relationship can be linear elastic (general isotropic fourth-order Hooke's law for continuous media) or viscoelastic (memory variables method).
!>@param elt_start : first hexahedron element of the loop 
!>@param elt_end   : last  hexahedron element of the loop 
!***********************************************************************************************************************************************************************************
   subroutine compute_internal_forces_order5(elt_start,elt_end)
!***********************************************************************************************************************************************************************************

         use mpi

         use mod_global_variables, only :&
                                         IG_NGLL&
                                        ,IG_NDOF&
                                        ,rg_gll_lagrange_deriv&
                                        ,rg_gll_displacement&
                                        ,rg_gll_acceleration&
                                        ,rg_gll_weight&
                                        ,ig_hexa_gll_glonum&
                                        ,rg_hexa_gll_dxidx&
                                        ,rg_hexa_gll_dxidy&
                                        ,rg_hexa_gll_dxidz&
                                        ,rg_hexa_gll_detdx&
                                        ,rg_hexa_gll_detdy&
                                        ,rg_hexa_gll_detdz&
                                        ,rg_hexa_gll_dzedx&
                                        ,rg_hexa_gll_dzedy&
                                        ,rg_hexa_gll_dzedz&
                                        ,rg_hexa_gll_jacobian_det&
                                        ,rg_hexa_gll_rhovs2&
                                        ,rg_hexa_gll_rhovp2&
                                        ,rg_hexa_gll_rhovs2&
                                        ,rg_hexa_gll_rhovp2&
                                        ,rg_hexa_gll_wkqs&
                                        ,rg_hexa_gll_wkqp&
                                        ,rg_hexa_gll_ksixx&
                                        ,rg_hexa_gll_ksiyy&
                                        ,rg_hexa_gll_ksizz&
                                        ,rg_hexa_gll_ksixy&
                                        ,rg_hexa_gll_ksixz&
                                        ,rg_hexa_gll_ksiyz&
                                        ,RG_RELAX_COEFF&
                                        ,rg_mem_var_exp&
                                        ,IG_NRELAX&
                                        ,LG_VISCO
         
         implicit none
         
         integer(kind=IXP), intent(in) :: elt_start
         integer(kind=IXP), intent(in) :: elt_end
         
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpx1
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpx2
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpx3
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpy1
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpy2
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpy3
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpz1
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpz2
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpz3
         real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: rl_displacement_gll
         real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: rl_acceleration_gll

         real(kind=RXP) :: duxdxi
         real(kind=RXP) :: duxdet
         real(kind=RXP) :: duxdze
         real(kind=RXP) :: duydxi
         real(kind=RXP) :: duydet
         real(kind=RXP) :: duydze
         real(kind=RXP) :: duzdxi
         real(kind=RXP) :: duzdet
         real(kind=RXP) :: duzdze
         real(kind=RXP) :: duxdx
         real(kind=RXP) :: duydy
         real(kind=RXP) :: duzdz
         real(kind=RXP) :: duxdy
         real(kind=RXP) :: duxdz
         real(kind=RXP) :: duydx
         real(kind=RXP) :: duydz
         real(kind=RXP) :: duzdx
         real(kind=RXP) :: duzdy
         real(kind=RXP) :: dxidx
         real(kind=RXP) :: dxidy
         real(kind=RXP) :: dxidz
         real(kind=RXP) :: detdx
         real(kind=RXP) :: detdy
         real(kind=RXP) :: detdz
         real(kind=RXP) :: dzedx
         real(kind=RXP) :: dzedy
         real(kind=RXP) :: dzedz
         real(kind=RXP) :: tauxx
         real(kind=RXP) :: tauyy
         real(kind=RXP) :: tauzz
         real(kind=RXP) :: tauxy
         real(kind=RXP) :: tauxz
         real(kind=RXP) :: tauyz
         real(kind=RXP) :: tauxx_n12
         real(kind=RXP) :: tauyy_n12
         real(kind=RXP) :: tauzz_n12
         real(kind=RXP) :: tauxy_n12
         real(kind=RXP) :: tauxz_n12
         real(kind=RXP) :: tauyz_n12
         real(kind=RXP) :: trace_tau
         real(kind=RXP) :: tmpx1
         real(kind=RXP) :: tmpx2
         real(kind=RXP) :: tmpx3
         real(kind=RXP) :: tmpx4
         real(kind=RXP) :: tmpy1
         real(kind=RXP) :: tmpy2
         real(kind=RXP) :: tmpy3
         real(kind=RXP) :: tmpz1
         real(kind=RXP) :: tmpz2
         real(kind=RXP) :: tmpz3
         real(kind=RXP) :: fac1
         real(kind=RXP) :: fac2
         real(kind=RXP) :: fac3
         
         integer(kind=IXP) :: iel
         integer(kind=IXP) :: k
         integer(kind=IXP) :: l
         integer(kind=IXP) :: m
         integer(kind=IXP) :: igll
         integer(kind=IXP) :: imem_var
         
         
         do iel = elt_start,elt_end
   
            !
            !------->flush to zero local acceleration
            do k = ONE_IXP,IG_NGLL
               do l = ONE_IXP,IG_NGLL
                  do m = ONE_IXP,IG_NGLL 
   
                     rl_acceleration_gll(ONE_IXP,m,l,k) = ZERO_RXP
                     rl_acceleration_gll(TWO_IXP,m,l,k) = ZERO_RXP
                     rl_acceleration_gll(THREE_IXP,m,l,k) = ZERO_RXP
   
                  enddo
               enddo
            enddo
   
            !
            !------->fill local displacement
            do k = ONE_IXP,IG_NGLL        !zeta
               do l = ONE_IXP,IG_NGLL     !eta
                  do m = ONE_IXP,IG_NGLL  !xi
   
                     igll                         = ig_hexa_gll_glonum(m,l,k,iel)
   
                     rl_displacement_gll(ONE_IXP,m,l,k) = rg_gll_displacement(ONE_IXP,igll)
                     rl_displacement_gll(TWO_IXP,m,l,k) = rg_gll_displacement(TWO_IXP,igll)
                     rl_displacement_gll(THREE_IXP,m,l,k) = rg_gll_displacement(THREE_IXP,igll)
   
                  enddo
               enddo
            enddo
         !
         !
         !******************************************************************************
         !->compute integrale at gll nodes + assemble force in global gll grid for hexa
         !******************************************************************************
            do k = ONE_IXP,IG_NGLL        !zeta
               do l = ONE_IXP,IG_NGLL     !eta
                  do m = ONE_IXP,IG_NGLL  !xi
         !
         !---------->derivative of displacement with respect to local coordinate xi, eta and zeta at the gll node klm
   
   
                     duxdxi = rl_displacement_gll(ONE_IXP,ONE_IXP,l,k)*rg_gll_lagrange_deriv(ONE_IXP,m) &
                            + rl_displacement_gll(ONE_IXP,TWO_IXP,l,k)*rg_gll_lagrange_deriv(TWO_IXP,m) &
                            + rl_displacement_gll(ONE_IXP,THREE_IXP,l,k)*rg_gll_lagrange_deriv(THREE_IXP,m) &
                            + rl_displacement_gll(ONE_IXP,FOUR_IXP,l,k)*rg_gll_lagrange_deriv(FOUR_IXP,m) &
                            + rl_displacement_gll(ONE_IXP,FIVE_IXP,l,k)*rg_gll_lagrange_deriv(FIVE_IXP,m) &
                            + rl_displacement_gll(ONE_IXP,SIX_IXP,l,k)*rg_gll_lagrange_deriv(SIX_IXP,m) 
   
                     duydxi = rl_displacement_gll(TWO_IXP,ONE_IXP,l,k)*rg_gll_lagrange_deriv(ONE_IXP,m) &
                            + rl_displacement_gll(TWO_IXP,TWO_IXP,l,k)*rg_gll_lagrange_deriv(TWO_IXP,m) &
                            + rl_displacement_gll(TWO_IXP,THREE_IXP,l,k)*rg_gll_lagrange_deriv(THREE_IXP,m) &
                            + rl_displacement_gll(TWO_IXP,FOUR_IXP,l,k)*rg_gll_lagrange_deriv(FOUR_IXP,m) &
                            + rl_displacement_gll(TWO_IXP,FIVE_IXP,l,k)*rg_gll_lagrange_deriv(FIVE_IXP,m) &
                            + rl_displacement_gll(TWO_IXP,SIX_IXP,l,k)*rg_gll_lagrange_deriv(SIX_IXP,m) 
   
                     duzdxi = rl_displacement_gll(THREE_IXP,ONE_IXP,l,k)*rg_gll_lagrange_deriv(ONE_IXP,m) &
                            + rl_displacement_gll(THREE_IXP,TWO_IXP,l,k)*rg_gll_lagrange_deriv(TWO_IXP,m) &
                            + rl_displacement_gll(THREE_IXP,THREE_IXP,l,k)*rg_gll_lagrange_deriv(THREE_IXP,m) &
                            + rl_displacement_gll(THREE_IXP,FOUR_IXP,l,k)*rg_gll_lagrange_deriv(FOUR_IXP,m) &
                            + rl_displacement_gll(THREE_IXP,FIVE_IXP,l,k)*rg_gll_lagrange_deriv(FIVE_IXP,m) &
                            + rl_displacement_gll(THREE_IXP,SIX_IXP,l,k)*rg_gll_lagrange_deriv(SIX_IXP,m)
   
                     duxdet = rl_displacement_gll(ONE_IXP,m,ONE_IXP,k)*rg_gll_lagrange_deriv(ONE_IXP,l) &
                            + rl_displacement_gll(ONE_IXP,m,TWO_IXP,k)*rg_gll_lagrange_deriv(TWO_IXP,l) &
                            + rl_displacement_gll(ONE_IXP,m,THREE_IXP,k)*rg_gll_lagrange_deriv(THREE_IXP,l) &
                            + rl_displacement_gll(ONE_IXP,m,FOUR_IXP,k)*rg_gll_lagrange_deriv(FOUR_IXP,l) &
                            + rl_displacement_gll(ONE_IXP,m,FIVE_IXP,k)*rg_gll_lagrange_deriv(FIVE_IXP,l) &
                            + rl_displacement_gll(ONE_IXP,m,SIX_IXP,k)*rg_gll_lagrange_deriv(SIX_IXP,l)
   
                     duydet = rl_displacement_gll(TWO_IXP,m,ONE_IXP,k)*rg_gll_lagrange_deriv(ONE_IXP,l) &
                            + rl_displacement_gll(TWO_IXP,m,TWO_IXP,k)*rg_gll_lagrange_deriv(TWO_IXP,l) &
                            + rl_displacement_gll(TWO_IXP,m,THREE_IXP,k)*rg_gll_lagrange_deriv(THREE_IXP,l) &
                            + rl_displacement_gll(TWO_IXP,m,FOUR_IXP,k)*rg_gll_lagrange_deriv(FOUR_IXP,l) &
                            + rl_displacement_gll(TWO_IXP,m,FIVE_IXP,k)*rg_gll_lagrange_deriv(FIVE_IXP,l) &
                            + rl_displacement_gll(TWO_IXP,m,SIX_IXP,k)*rg_gll_lagrange_deriv(SIX_IXP,l)
   
                     duzdet = rl_displacement_gll(THREE_IXP,m,ONE_IXP,k)*rg_gll_lagrange_deriv(ONE_IXP,l) &
                            + rl_displacement_gll(THREE_IXP,m,TWO_IXP,k)*rg_gll_lagrange_deriv(TWO_IXP,l) &
                            + rl_displacement_gll(THREE_IXP,m,THREE_IXP,k)*rg_gll_lagrange_deriv(THREE_IXP,l) &
                            + rl_displacement_gll(THREE_IXP,m,FOUR_IXP,k)*rg_gll_lagrange_deriv(FOUR_IXP,l) &
                            + rl_displacement_gll(THREE_IXP,m,FIVE_IXP,k)*rg_gll_lagrange_deriv(FIVE_IXP,l) &
                            + rl_displacement_gll(THREE_IXP,m,SIX_IXP,k)*rg_gll_lagrange_deriv(SIX_IXP,l)
   
                     duxdze = rl_displacement_gll(ONE_IXP,m,l,ONE_IXP)*rg_gll_lagrange_deriv(ONE_IXP,k) &
                            + rl_displacement_gll(ONE_IXP,m,l,TWO_IXP)*rg_gll_lagrange_deriv(TWO_IXP,k) &
                            + rl_displacement_gll(ONE_IXP,m,l,THREE_IXP)*rg_gll_lagrange_deriv(THREE_IXP,k) &
                            + rl_displacement_gll(ONE_IXP,m,l,FOUR_IXP)*rg_gll_lagrange_deriv(FOUR_IXP,k) &
                            + rl_displacement_gll(ONE_IXP,m,l,FIVE_IXP)*rg_gll_lagrange_deriv(FIVE_IXP,k) &
                            + rl_displacement_gll(ONE_IXP,m,l,SIX_IXP)*rg_gll_lagrange_deriv(SIX_IXP,k) 
   
                     duydze = rl_displacement_gll(TWO_IXP,m,l,ONE_IXP)*rg_gll_lagrange_deriv(ONE_IXP,k) &
                            + rl_displacement_gll(TWO_IXP,m,l,TWO_IXP)*rg_gll_lagrange_deriv(TWO_IXP,k) &
                            + rl_displacement_gll(TWO_IXP,m,l,THREE_IXP)*rg_gll_lagrange_deriv(THREE_IXP,k) &
                            + rl_displacement_gll(TWO_IXP,m,l,FOUR_IXP)*rg_gll_lagrange_deriv(FOUR_IXP,k) &
                            + rl_displacement_gll(TWO_IXP,m,l,FIVE_IXP)*rg_gll_lagrange_deriv(FIVE_IXP,k) &
                            + rl_displacement_gll(TWO_IXP,m,l,SIX_IXP)*rg_gll_lagrange_deriv(SIX_IXP,k) 
   
                     duzdze = rl_displacement_gll(THREE_IXP,m,l,ONE_IXP)*rg_gll_lagrange_deriv(ONE_IXP,k) &
                            + rl_displacement_gll(THREE_IXP,m,l,TWO_IXP)*rg_gll_lagrange_deriv(TWO_IXP,k) &
                            + rl_displacement_gll(THREE_IXP,m,l,THREE_IXP)*rg_gll_lagrange_deriv(THREE_IXP,k) &
                            + rl_displacement_gll(THREE_IXP,m,l,FOUR_IXP)*rg_gll_lagrange_deriv(FOUR_IXP,k) &
                            + rl_displacement_gll(THREE_IXP,m,l,FIVE_IXP)*rg_gll_lagrange_deriv(FIVE_IXP,k) &
                            + rl_displacement_gll(THREE_IXP,m,l,SIX_IXP)*rg_gll_lagrange_deriv(SIX_IXP,k)
   
         !      
         !---------->derivative of displacement at step n+1 with respect to global coordinate x, y and z at the gll node klm
                     dxidx = rg_hexa_gll_dxidx  (m,l,k,iel)
                     dxidy = rg_hexa_gll_dxidy  (m,l,k,iel)
                     dxidz = rg_hexa_gll_dxidz  (m,l,k,iel)
                     detdx = rg_hexa_gll_detdx (m,l,k,iel)
                     detdy = rg_hexa_gll_detdy (m,l,k,iel)
                     detdz = rg_hexa_gll_detdz (m,l,k,iel)
                     dzedx = rg_hexa_gll_dzedx(m,l,k,iel)
                     dzedy = rg_hexa_gll_dzedy(m,l,k,iel)
                     dzedz = rg_hexa_gll_dzedz(m,l,k,iel)
   
                     duxdx = duxdxi*dxidx + duxdet*detdx + duxdze*dzedx
                     duxdy = duxdxi*dxidy + duxdet*detdy + duxdze*dzedy
                     duxdz = duxdxi*dxidz + duxdet*detdz + duxdze*dzedz
                     duydx = duydxi*dxidx + duydet*detdx + duydze*dzedx
                     duydy = duydxi*dxidy + duydet*detdy + duydze*dzedy
                     duydz = duydxi*dxidz + duydet*detdz + duydze*dzedz
                     duzdx = duzdxi*dxidx + duzdet*detdx + duzdze*dzedx
                     duzdy = duzdxi*dxidy + duzdet*detdy + duzdze*dzedy
                     duzdz = duzdxi*dxidz + duzdet*detdz + duzdze*dzedz
         !
         !---------->compute elastic stress (elastic simulation) or unrelaxed elastic stress (viscoelastic simulation)
                     trace_tau = (rg_hexa_gll_rhovp2(m,l,k,iel) - TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel))*(duxdx+duydy+duzdz)
                     tauxx     = trace_tau + TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*duxdx
                     tauyy     = trace_tau + TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*duydy
                     tauzz     = trace_tau + TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*duzdz
                     tauxy     =                 rg_hexa_gll_rhovs2(m,l,k,iel)*(duxdy+duydx)
                     tauxz     =                 rg_hexa_gll_rhovs2(m,l,k,iel)*(duxdz+duzdx)
                     tauyz     =                 rg_hexa_gll_rhovs2(m,l,k,iel)*(duydz+duzdy)
         !
         !---------->compute viscoelastic stress
   
                 if(LG_VISCO) then
   
                     do imem_var = ONE_IXP,IG_NRELAX
   
                        tmpx1 = rg_mem_var_exp(imem_var)
   
                        tmpx2 =     rg_hexa_gll_rhovp2(m,l,k,iel)*rg_hexa_gll_wkqp(imem_var,m,l,k,iel)
                        tmpx3 = TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*rg_hexa_gll_wkqs(imem_var,m,l,k,iel)
   
                        tmpx4 = (duxdx+duydy+duzdz)*(tmpx2 - tmpx3)
   
         !
         !------------->anelastic stress at step n+1/2 following s. ma and p. liu (2006) using epsnp1 and unrelaxed material modulus
                        tauxx_n12 = tmpx1*rg_hexa_gll_ksixx(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*duxdx + tmpx4)
                        tauyy_n12 = tmpx1*rg_hexa_gll_ksiyy(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*duydy + tmpx4)
                        tauzz_n12 = tmpx1*rg_hexa_gll_ksizz(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*duzdz + tmpx4)
                        tauxy_n12 = tmpx1*rg_hexa_gll_ksixy(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*ONE_HALF_RXP*(duxdy+duydx))
                        tauxz_n12 = tmpx1*rg_hexa_gll_ksixz(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*ONE_HALF_RXP*(duxdz+duzdx))
                        tauyz_n12 = tmpx1*rg_hexa_gll_ksiyz(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*ONE_HALF_RXP*(duydz+duzdy))
         !        
         !------------->compute final stress at step n+1 according to day and minster (1984) + ma and liu (2006) : tauxx - SUM anelastic stress at step n+1
                        tauxx = tauxx - ONE_HALF_RXP*(tauxx_n12 + rg_hexa_gll_ksixx(imem_var,m,l,k,iel))
                        tauyy = tauyy - ONE_HALF_RXP*(tauyy_n12 + rg_hexa_gll_ksiyy(imem_var,m,l,k,iel))
                        tauzz = tauzz - ONE_HALF_RXP*(tauzz_n12 + rg_hexa_gll_ksizz(imem_var,m,l,k,iel))
                        tauxy = tauxy - ONE_HALF_RXP*(tauxy_n12 + rg_hexa_gll_ksixy(imem_var,m,l,k,iel))
                        tauxz = tauxz - ONE_HALF_RXP*(tauxz_n12 + rg_hexa_gll_ksixz(imem_var,m,l,k,iel))
                        tauyz = tauyz - ONE_HALF_RXP*(tauyz_n12 + rg_hexa_gll_ksiyz(imem_var,m,l,k,iel))
         !        
         !------------->update memory for stress (step n+1/2)
                        rg_hexa_gll_ksixx(imem_var,m,l,k,iel) = tauxx_n12
                        rg_hexa_gll_ksiyy(imem_var,m,l,k,iel) = tauyy_n12
                        rg_hexa_gll_ksizz(imem_var,m,l,k,iel) = tauzz_n12
                        rg_hexa_gll_ksixy(imem_var,m,l,k,iel) = tauxy_n12
                        rg_hexa_gll_ksixz(imem_var,m,l,k,iel) = tauxz_n12
                        rg_hexa_gll_ksiyz(imem_var,m,l,k,iel) = tauyz_n12
   
                     enddo
   
                 endif
         !      
         !---------->store members of integration of the gll node klm
                     intpx1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx*dxidx+tauxy*dxidy+tauxz*dxidz)
                     intpx2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx*detdx+tauxy*detdy+tauxz*detdz)
                     intpx3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx*dzedx+tauxy*dzedy+tauxz*dzedz)
   
                     intpy1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy*dxidx+tauyy*dxidy+tauyz*dxidz)
                     intpy2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy*detdx+tauyy*detdy+tauyz*detdz)
                     intpy3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy*dzedx+tauyy*dzedy+tauyz*dzedz)
   
                     intpz1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz*dxidx+tauyz*dxidy+tauzz*dxidz)
                     intpz2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz*detdx+tauyz*detdy+tauzz*detdz)
                     intpz3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz*dzedx+tauyz*dzedy+tauzz*dzedz)
                  enddo !xi
               enddo    !eta
            enddo       !zeta
   
         !
         !->finish integration for hexa (internal forces at step n+1)
            do k = ONE_IXP,IG_NGLL
               do l = ONE_IXP,IG_NGLL
                  do m = ONE_IXP,IG_NGLL
   
                     tmpx1 = intpx1(ONE_IXP,l,k)*rg_gll_lagrange_deriv(m,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpx1(TWO_IXP,l,k)*rg_gll_lagrange_deriv(m,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpx1(THREE_IXP,l,k)*rg_gll_lagrange_deriv(m,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpx1(FOUR_IXP,l,k)*rg_gll_lagrange_deriv(m,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpx1(FIVE_IXP,l,k)*rg_gll_lagrange_deriv(m,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpx1(SIX_IXP,l,k)*rg_gll_lagrange_deriv(m,SIX_IXP)*rg_gll_weight(SIX_IXP)
   
                     tmpy1 = intpy1(ONE_IXP,l,k)*rg_gll_lagrange_deriv(m,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpy1(TWO_IXP,l,k)*rg_gll_lagrange_deriv(m,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpy1(THREE_IXP,l,k)*rg_gll_lagrange_deriv(m,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpy1(FOUR_IXP,l,k)*rg_gll_lagrange_deriv(m,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpy1(FIVE_IXP,l,k)*rg_gll_lagrange_deriv(m,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpy1(SIX_IXP,l,k)*rg_gll_lagrange_deriv(m,SIX_IXP)*rg_gll_weight(SIX_IXP) 
   
                     tmpz1 = intpz1(ONE_IXP,l,k)*rg_gll_lagrange_deriv(m,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpz1(TWO_IXP,l,k)*rg_gll_lagrange_deriv(m,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpz1(THREE_IXP,l,k)*rg_gll_lagrange_deriv(m,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpz1(FOUR_IXP,l,k)*rg_gll_lagrange_deriv(m,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpz1(FIVE_IXP,l,k)*rg_gll_lagrange_deriv(m,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpz1(SIX_IXP,l,k)*rg_gll_lagrange_deriv(m,SIX_IXP)*rg_gll_weight(SIX_IXP)
   
                     tmpx2 = intpx2(m,ONE_IXP,k)*rg_gll_lagrange_deriv(l,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpx2(m,TWO_IXP,k)*rg_gll_lagrange_deriv(l,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpx2(m,THREE_IXP,k)*rg_gll_lagrange_deriv(l,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpx2(m,FOUR_IXP,k)*rg_gll_lagrange_deriv(l,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpx2(m,FIVE_IXP,k)*rg_gll_lagrange_deriv(l,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpx2(m,SIX_IXP,k)*rg_gll_lagrange_deriv(l,SIX_IXP)*rg_gll_weight(SIX_IXP)
   
                     tmpy2 = intpy2(m,ONE_IXP,k)*rg_gll_lagrange_deriv(l,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpy2(m,TWO_IXP,k)*rg_gll_lagrange_deriv(l,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpy2(m,THREE_IXP,k)*rg_gll_lagrange_deriv(l,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpy2(m,FOUR_IXP,k)*rg_gll_lagrange_deriv(l,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpy2(m,FIVE_IXP,k)*rg_gll_lagrange_deriv(l,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpy2(m,SIX_IXP,k)*rg_gll_lagrange_deriv(l,SIX_IXP)*rg_gll_weight(SIX_IXP)
   
                     tmpz2 = intpz2(m,ONE_IXP,k)*rg_gll_lagrange_deriv(l,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpz2(m,TWO_IXP,k)*rg_gll_lagrange_deriv(l,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpz2(m,THREE_IXP,k)*rg_gll_lagrange_deriv(l,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpz2(m,FOUR_IXP,k)*rg_gll_lagrange_deriv(l,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpz2(m,FIVE_IXP,k)*rg_gll_lagrange_deriv(l,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpz2(m,SIX_IXP,k)*rg_gll_lagrange_deriv(l,SIX_IXP)*rg_gll_weight(SIX_IXP)
   
                     tmpx3 = intpx3(m,l,ONE_IXP)*rg_gll_lagrange_deriv(k,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpx3(m,l,TWO_IXP)*rg_gll_lagrange_deriv(k,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpx3(m,l,THREE_IXP)*rg_gll_lagrange_deriv(k,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpx3(m,l,FOUR_IXP)*rg_gll_lagrange_deriv(k,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpx3(m,l,FIVE_IXP)*rg_gll_lagrange_deriv(k,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpx3(m,l,SIX_IXP)*rg_gll_lagrange_deriv(k,SIX_IXP)*rg_gll_weight(SIX_IXP)
   
                     tmpy3 = intpy3(m,l,ONE_IXP)*rg_gll_lagrange_deriv(k,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpy3(m,l,TWO_IXP)*rg_gll_lagrange_deriv(k,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpy3(m,l,THREE_IXP)*rg_gll_lagrange_deriv(k,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpy3(m,l,FOUR_IXP)*rg_gll_lagrange_deriv(k,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpy3(m,l,FIVE_IXP)*rg_gll_lagrange_deriv(k,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpy3(m,l,SIX_IXP)*rg_gll_lagrange_deriv(k,SIX_IXP)*rg_gll_weight(SIX_IXP)
   
                     tmpz3 = intpz3(m,l,ONE_IXP)*rg_gll_lagrange_deriv(k,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpz3(m,l,TWO_IXP)*rg_gll_lagrange_deriv(k,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpz3(m,l,THREE_IXP)*rg_gll_lagrange_deriv(k,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpz3(m,l,FOUR_IXP)*rg_gll_lagrange_deriv(k,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpz3(m,l,FIVE_IXP)*rg_gll_lagrange_deriv(k,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpz3(m,l,SIX_IXP)*rg_gll_lagrange_deriv(k,SIX_IXP)*rg_gll_weight(SIX_IXP) 
   
                     fac1 = rg_gll_weight(l)*rg_gll_weight(k)
                     fac2 = rg_gll_weight(m)*rg_gll_weight(k)
                     fac3 = rg_gll_weight(m)*rg_gll_weight(l)
   
                     rl_acceleration_gll(ONE_IXP,m,l,k) = rl_acceleration_gll(ONE_IXP,m,l,k) + (fac1*tmpx1 + fac2*tmpx2 + fac3*tmpx3)
                     rl_acceleration_gll(TWO_IXP,m,l,k) = rl_acceleration_gll(TWO_IXP,m,l,k) + (fac1*tmpy1 + fac2*tmpy2 + fac3*tmpy3)
                     rl_acceleration_gll(THREE_IXP,m,l,k) = rl_acceleration_gll(THREE_IXP,m,l,k) + (fac1*tmpz1 + fac2*tmpz2 + fac3*tmpz3)
   
                  enddo
               enddo
            enddo
   
            do k = ONE_IXP,IG_NGLL        !zeta
               do l = ONE_IXP,IG_NGLL     !eta
                  do m = ONE_IXP,IG_NGLL  !xi
   
                     igll                        = ig_hexa_gll_glonum(m,l,k,iel)
   
                     rg_gll_acceleration(ONE_IXP,igll) = rg_gll_acceleration(ONE_IXP,igll) - rl_acceleration_gll(ONE_IXP,m,l,k)
                     rg_gll_acceleration(TWO_IXP,igll) = rg_gll_acceleration(TWO_IXP,igll) - rl_acceleration_gll(TWO_IXP,m,l,k)
                     rg_gll_acceleration(THREE_IXP,igll) = rg_gll_acceleration(THREE_IXP,igll) - rl_acceleration_gll(THREE_IXP,m,l,k)
   
                  enddo
               enddo
            enddo
   
         enddo !loop on hexahedron elements
   
         return
!***********************************************************************************************************************************************************************************
      end subroutine compute_internal_forces_order5
!***********************************************************************************************************************************************************************************
   
!
!
!>@brief
!!This subroutine computes internal forces @f$ \int _{\Omega}  \boldsymbol{\epsilon}(\mathbf{v}) ^{T} \colon \boldsymbol{\tau} \, d\Omega @f$ for spectral-elements of order 6.
!!Stress-strain relationship can be linear elastic (general isotropic fourth-order Hooke's law for continuous media) or viscoelastic (memory variables method).
!>@param elt_start : first hexahedron element of the loop 
!>@param elt_end   : last  hexahedron element of the loop 
!***********************************************************************************************************************************************************************************
      subroutine compute_internal_forces_order6(elt_start,elt_end)
!***********************************************************************************************************************************************************************************

         use mpi

         use mod_global_variables, only :&
                                         IG_NGLL&
                                        ,IG_NDOF&
                                        ,rg_gll_lagrange_deriv&
                                        ,rg_gll_displacement&
                                        ,rg_gll_acceleration&
                                        ,rg_gll_weight&
                                        ,ig_hexa_gll_glonum&
                                        ,rg_hexa_gll_dxidx&
                                        ,rg_hexa_gll_dxidy&
                                        ,rg_hexa_gll_dxidz&
                                        ,rg_hexa_gll_detdx&
                                        ,rg_hexa_gll_detdy&
                                        ,rg_hexa_gll_detdz&
                                        ,rg_hexa_gll_dzedx&
                                        ,rg_hexa_gll_dzedy&
                                        ,rg_hexa_gll_dzedz&
                                        ,rg_hexa_gll_jacobian_det&
                                        ,rg_hexa_gll_rhovs2&
                                        ,rg_hexa_gll_rhovp2&
                                        ,rg_hexa_gll_rhovs2&
                                        ,rg_hexa_gll_rhovp2&
                                        ,rg_hexa_gll_wkqs&
                                        ,rg_hexa_gll_wkqp&
                                        ,rg_hexa_gll_ksixx&
                                        ,rg_hexa_gll_ksiyy&
                                        ,rg_hexa_gll_ksizz&
                                        ,rg_hexa_gll_ksixy&
                                        ,rg_hexa_gll_ksixz&
                                        ,rg_hexa_gll_ksiyz&
                                        ,RG_RELAX_COEFF&
                                        ,rg_mem_var_exp&
                                        ,IG_NRELAX&
                                        ,LG_VISCO
         
         implicit none
         
         integer(kind=IXP), intent(in) :: elt_start
         integer(kind=IXP), intent(in) :: elt_end
         
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpx1
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpx2
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpx3
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpy1
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpy2
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpy3
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpz1
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpz2
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)         :: intpz3
         real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: rl_displacement_gll
         real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: rl_acceleration_gll

         real(kind=RXP) :: duxdxi
         real(kind=RXP) :: duxdet
         real(kind=RXP) :: duxdze
         real(kind=RXP) :: duydxi
         real(kind=RXP) :: duydet
         real(kind=RXP) :: duydze
         real(kind=RXP) :: duzdxi
         real(kind=RXP) :: duzdet
         real(kind=RXP) :: duzdze
         real(kind=RXP) :: duxdx
         real(kind=RXP) :: duydy
         real(kind=RXP) :: duzdz
         real(kind=RXP) :: duxdy
         real(kind=RXP) :: duxdz
         real(kind=RXP) :: duydx
         real(kind=RXP) :: duydz
         real(kind=RXP) :: duzdx
         real(kind=RXP) :: duzdy
         real(kind=RXP) :: dxidx
         real(kind=RXP) :: dxidy
         real(kind=RXP) :: dxidz
         real(kind=RXP) :: detdx
         real(kind=RXP) :: detdy
         real(kind=RXP) :: detdz
         real(kind=RXP) :: dzedx
         real(kind=RXP) :: dzedy
         real(kind=RXP) :: dzedz
         real(kind=RXP) :: tauxx
         real(kind=RXP) :: tauyy
         real(kind=RXP) :: tauzz
         real(kind=RXP) :: tauxy
         real(kind=RXP) :: tauxz
         real(kind=RXP) :: tauyz
         real(kind=RXP) :: tauxx_n12
         real(kind=RXP) :: tauyy_n12
         real(kind=RXP) :: tauzz_n12
         real(kind=RXP) :: tauxy_n12
         real(kind=RXP) :: tauxz_n12
         real(kind=RXP) :: tauyz_n12
         real(kind=RXP) :: trace_tau
         real(kind=RXP) :: tmpx1
         real(kind=RXP) :: tmpx2
         real(kind=RXP) :: tmpx3
         real(kind=RXP) :: tmpx4
         real(kind=RXP) :: tmpy1
         real(kind=RXP) :: tmpy2
         real(kind=RXP) :: tmpy3
         real(kind=RXP) :: tmpz1
         real(kind=RXP) :: tmpz2
         real(kind=RXP) :: tmpz3
         real(kind=RXP) :: fac1
         real(kind=RXP) :: fac2
         real(kind=RXP) :: fac3
         
         integer(kind=IXP) :: iel
         integer(kind=IXP) :: k
         integer(kind=IXP) :: l
         integer(kind=IXP) :: m
         integer(kind=IXP) :: igll
         integer(kind=IXP) :: imem_var
         
         do iel = elt_start,elt_end
   
            !
            !------->flush to zero local acceleration
            do k = ONE_IXP,IG_NGLL
               do l = ONE_IXP,IG_NGLL
                  do m = ONE_IXP,IG_NGLL 
   
                     rl_acceleration_gll(ONE_IXP,m,l,k) = ZERO_RXP
                     rl_acceleration_gll(TWO_IXP,m,l,k) = ZERO_RXP
                     rl_acceleration_gll(THREE_IXP,m,l,k) = ZERO_RXP
   
                  enddo
               enddo
            enddo
   
            !
            !------->fill local displacement
            do k = ONE_IXP,IG_NGLL        !zeta
               do l = ONE_IXP,IG_NGLL     !eta
                  do m = ONE_IXP,IG_NGLL  !xi
   
                     igll                         = ig_hexa_gll_glonum(m,l,k,iel)
   
                     rl_displacement_gll(ONE_IXP,m,l,k) = rg_gll_displacement(ONE_IXP,igll)
                     rl_displacement_gll(TWO_IXP,m,l,k) = rg_gll_displacement(TWO_IXP,igll)
                     rl_displacement_gll(THREE_IXP,m,l,k) = rg_gll_displacement(THREE_IXP,igll)
   
                  enddo
               enddo
            enddo
         !
         !
         !******************************************************************************
         !->compute integrale at gll nodes + assemble force in global gll grid for hexa
         !******************************************************************************
            do k = ONE_IXP,IG_NGLL        !zeta
               do l = ONE_IXP,IG_NGLL     !eta
                  do m = ONE_IXP,IG_NGLL  !xi
         !
         !---------->derivative of displacement with respect to local coordinate xi, eta and zeta at the gll node klm
   
   
                     duxdxi = rl_displacement_gll(ONE_IXP,ONE_IXP,l,k)*rg_gll_lagrange_deriv(ONE_IXP,m) &
                            + rl_displacement_gll(ONE_IXP,TWO_IXP,l,k)*rg_gll_lagrange_deriv(TWO_IXP,m) &
                            + rl_displacement_gll(ONE_IXP,THREE_IXP,l,k)*rg_gll_lagrange_deriv(THREE_IXP,m) &
                            + rl_displacement_gll(ONE_IXP,FOUR_IXP,l,k)*rg_gll_lagrange_deriv(FOUR_IXP,m) &
                            + rl_displacement_gll(ONE_IXP,FIVE_IXP,l,k)*rg_gll_lagrange_deriv(FIVE_IXP,m) & 
                            + rl_displacement_gll(ONE_IXP,SIX_IXP,l,k)*rg_gll_lagrange_deriv(SIX_IXP,m) &
                            + rl_displacement_gll(ONE_IXP,SEVEN_IXP,l,k)*rg_gll_lagrange_deriv(SEVEN_IXP,m) 
   
                     duydxi = rl_displacement_gll(TWO_IXP,ONE_IXP,l,k)*rg_gll_lagrange_deriv(ONE_IXP,m) &
                            + rl_displacement_gll(TWO_IXP,TWO_IXP,l,k)*rg_gll_lagrange_deriv(TWO_IXP,m) &
                            + rl_displacement_gll(TWO_IXP,THREE_IXP,l,k)*rg_gll_lagrange_deriv(THREE_IXP,m) &
                            + rl_displacement_gll(TWO_IXP,FOUR_IXP,l,k)*rg_gll_lagrange_deriv(FOUR_IXP,m) &
                            + rl_displacement_gll(TWO_IXP,FIVE_IXP,l,k)*rg_gll_lagrange_deriv(FIVE_IXP,m) &
                            + rl_displacement_gll(TWO_IXP,SIX_IXP,l,k)*rg_gll_lagrange_deriv(SIX_IXP,m) &
                            + rl_displacement_gll(TWO_IXP,SEVEN_IXP,l,k)*rg_gll_lagrange_deriv(SEVEN_IXP,m) 
   
                     duzdxi = rl_displacement_gll(THREE_IXP,ONE_IXP,l,k)*rg_gll_lagrange_deriv(ONE_IXP,m) &
                            + rl_displacement_gll(THREE_IXP,TWO_IXP,l,k)*rg_gll_lagrange_deriv(TWO_IXP,m) &
                            + rl_displacement_gll(THREE_IXP,THREE_IXP,l,k)*rg_gll_lagrange_deriv(THREE_IXP,m) &
                            + rl_displacement_gll(THREE_IXP,FOUR_IXP,l,k)*rg_gll_lagrange_deriv(FOUR_IXP,m) &
                            + rl_displacement_gll(THREE_IXP,FIVE_IXP,l,k)*rg_gll_lagrange_deriv(FIVE_IXP,m) &
                            + rl_displacement_gll(THREE_IXP,SIX_IXP,l,k)*rg_gll_lagrange_deriv(SIX_IXP,m) &
                            + rl_displacement_gll(THREE_IXP,SEVEN_IXP,l,k)*rg_gll_lagrange_deriv(SEVEN_IXP,m)
   
                     duxdet = rl_displacement_gll(ONE_IXP,m,ONE_IXP,k)*rg_gll_lagrange_deriv(ONE_IXP,l) &
                            + rl_displacement_gll(ONE_IXP,m,TWO_IXP,k)*rg_gll_lagrange_deriv(TWO_IXP,l) &
                            + rl_displacement_gll(ONE_IXP,m,THREE_IXP,k)*rg_gll_lagrange_deriv(THREE_IXP,l) &
                            + rl_displacement_gll(ONE_IXP,m,FOUR_IXP,k)*rg_gll_lagrange_deriv(FOUR_IXP,l) &
                            + rl_displacement_gll(ONE_IXP,m,FIVE_IXP,k)*rg_gll_lagrange_deriv(FIVE_IXP,l) &
                            + rl_displacement_gll(ONE_IXP,m,SIX_IXP,k)*rg_gll_lagrange_deriv(SIX_IXP,l) &
                            + rl_displacement_gll(ONE_IXP,m,SEVEN_IXP,k)*rg_gll_lagrange_deriv(SEVEN_IXP,l)
   
                     duydet = rl_displacement_gll(TWO_IXP,m,ONE_IXP,k)*rg_gll_lagrange_deriv(ONE_IXP,l) &
                            + rl_displacement_gll(TWO_IXP,m,TWO_IXP,k)*rg_gll_lagrange_deriv(TWO_IXP,l) &
                            + rl_displacement_gll(TWO_IXP,m,THREE_IXP,k)*rg_gll_lagrange_deriv(THREE_IXP,l) &
                            + rl_displacement_gll(TWO_IXP,m,FOUR_IXP,k)*rg_gll_lagrange_deriv(FOUR_IXP,l) &
                            + rl_displacement_gll(TWO_IXP,m,FIVE_IXP,k)*rg_gll_lagrange_deriv(FIVE_IXP,l) &
                            + rl_displacement_gll(TWO_IXP,m,SIX_IXP,k)*rg_gll_lagrange_deriv(SIX_IXP,l) &
                            + rl_displacement_gll(TWO_IXP,m,SEVEN_IXP,k)*rg_gll_lagrange_deriv(SEVEN_IXP,l)
   
                     duzdet = rl_displacement_gll(THREE_IXP,m,ONE_IXP,k)*rg_gll_lagrange_deriv(ONE_IXP,l) &
                            + rl_displacement_gll(THREE_IXP,m,TWO_IXP,k)*rg_gll_lagrange_deriv(TWO_IXP,l) &
                            + rl_displacement_gll(THREE_IXP,m,THREE_IXP,k)*rg_gll_lagrange_deriv(THREE_IXP,l) &
                            + rl_displacement_gll(THREE_IXP,m,FOUR_IXP,k)*rg_gll_lagrange_deriv(FOUR_IXP,l) &
                            + rl_displacement_gll(THREE_IXP,m,FIVE_IXP,k)*rg_gll_lagrange_deriv(FIVE_IXP,l) &
                            + rl_displacement_gll(THREE_IXP,m,SIX_IXP,k)*rg_gll_lagrange_deriv(SIX_IXP,l) &
                            + rl_displacement_gll(THREE_IXP,m,SEVEN_IXP,k)*rg_gll_lagrange_deriv(SEVEN_IXP,l)
   
                     duxdze = rl_displacement_gll(ONE_IXP,m,l,ONE_IXP)*rg_gll_lagrange_deriv(ONE_IXP,k) &
                            + rl_displacement_gll(ONE_IXP,m,l,TWO_IXP)*rg_gll_lagrange_deriv(TWO_IXP,k) &
                            + rl_displacement_gll(ONE_IXP,m,l,THREE_IXP)*rg_gll_lagrange_deriv(THREE_IXP,k) &
                            + rl_displacement_gll(ONE_IXP,m,l,FOUR_IXP)*rg_gll_lagrange_deriv(FOUR_IXP,k) &
                            + rl_displacement_gll(ONE_IXP,m,l,FIVE_IXP)*rg_gll_lagrange_deriv(FIVE_IXP,k) &
                            + rl_displacement_gll(ONE_IXP,m,l,SIX_IXP)*rg_gll_lagrange_deriv(SIX_IXP,k) &
                            + rl_displacement_gll(ONE_IXP,m,l,SEVEN_IXP)*rg_gll_lagrange_deriv(SEVEN_IXP,k) 
   
                     duydze = rl_displacement_gll(TWO_IXP,m,l,ONE_IXP)*rg_gll_lagrange_deriv(ONE_IXP,k) &
                            + rl_displacement_gll(TWO_IXP,m,l,TWO_IXP)*rg_gll_lagrange_deriv(TWO_IXP,k) &
                            + rl_displacement_gll(TWO_IXP,m,l,THREE_IXP)*rg_gll_lagrange_deriv(THREE_IXP,k) &
                            + rl_displacement_gll(TWO_IXP,m,l,FOUR_IXP)*rg_gll_lagrange_deriv(FOUR_IXP,k) &
                            + rl_displacement_gll(TWO_IXP,m,l,FIVE_IXP)*rg_gll_lagrange_deriv(FIVE_IXP,k) &
                            + rl_displacement_gll(TWO_IXP,m,l,SIX_IXP)*rg_gll_lagrange_deriv(SIX_IXP,k) &
                            + rl_displacement_gll(TWO_IXP,m,l,SEVEN_IXP)*rg_gll_lagrange_deriv(SEVEN_IXP,k) 
   
                     duzdze = rl_displacement_gll(THREE_IXP,m,l,ONE_IXP)*rg_gll_lagrange_deriv(ONE_IXP,k) &
                            + rl_displacement_gll(THREE_IXP,m,l,TWO_IXP)*rg_gll_lagrange_deriv(TWO_IXP,k) &
                            + rl_displacement_gll(THREE_IXP,m,l,THREE_IXP)*rg_gll_lagrange_deriv(THREE_IXP,k) &
                            + rl_displacement_gll(THREE_IXP,m,l,FOUR_IXP)*rg_gll_lagrange_deriv(FOUR_IXP,k) &
                            + rl_displacement_gll(THREE_IXP,m,l,FIVE_IXP)*rg_gll_lagrange_deriv(FIVE_IXP,k) &
                            + rl_displacement_gll(THREE_IXP,m,l,SIX_IXP)*rg_gll_lagrange_deriv(SIX_IXP,k) &
                            + rl_displacement_gll(THREE_IXP,m,l,SEVEN_IXP)*rg_gll_lagrange_deriv(SEVEN_IXP,k)
   
         !      
         !---------->derivative of displacement at step n+1 with respect to global coordinate x, y and z at the gll node klm
                     dxidx = rg_hexa_gll_dxidx(m,l,k,iel)
                     dxidy = rg_hexa_gll_dxidy(m,l,k,iel)
                     dxidz = rg_hexa_gll_dxidz(m,l,k,iel)
                     detdx = rg_hexa_gll_detdx(m,l,k,iel)
                     detdy = rg_hexa_gll_detdy(m,l,k,iel)
                     detdz = rg_hexa_gll_detdz(m,l,k,iel)
                     dzedx = rg_hexa_gll_dzedx(m,l,k,iel)
                     dzedy = rg_hexa_gll_dzedy(m,l,k,iel)
                     dzedz = rg_hexa_gll_dzedz(m,l,k,iel)
   
                     duxdx = duxdxi*dxidx + duxdet*detdx + duxdze*dzedx
                     duxdy = duxdxi*dxidy + duxdet*detdy + duxdze*dzedy
                     duxdz = duxdxi*dxidz + duxdet*detdz + duxdze*dzedz
                     duydx = duydxi*dxidx + duydet*detdx + duydze*dzedx
                     duydy = duydxi*dxidy + duydet*detdy + duydze*dzedy
                     duydz = duydxi*dxidz + duydet*detdz + duydze*dzedz
                     duzdx = duzdxi*dxidx + duzdet*detdx + duzdze*dzedx
                     duzdy = duzdxi*dxidy + duzdet*detdy + duzdze*dzedy
                     duzdz = duzdxi*dxidz + duzdet*detdz + duzdze*dzedz
         !
         !---------->compute elastic stress (elastic simulation) or unrelaxed elastic stress (viscoelastic simulation)
                     trace_tau = (rg_hexa_gll_rhovp2(m,l,k,iel) - TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel))*(duxdx+duydy+duzdz)
                     tauxx     = trace_tau + TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*duxdx
                     tauyy     = trace_tau + TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*duydy
                     tauzz     = trace_tau + TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*duzdz
                     tauxy     =                 rg_hexa_gll_rhovs2(m,l,k,iel)*(duxdy+duydx)
                     tauxz     =                 rg_hexa_gll_rhovs2(m,l,k,iel)*(duxdz+duzdx)
                     tauyz     =                 rg_hexa_gll_rhovs2(m,l,k,iel)*(duydz+duzdy)
         !
         !---------->compute viscoelastic stress
   
                 if(LG_VISCO) then
   
                     do imem_var = ONE_IXP,IG_NRELAX
   
                        tmpx1 = rg_mem_var_exp(imem_var)
   
                        tmpx2 =     rg_hexa_gll_rhovp2(m,l,k,iel)*rg_hexa_gll_wkqp(imem_var,m,l,k,iel)
                        tmpx3 = TWO_RXP*rg_hexa_gll_rhovs2(m,l,k,iel)*rg_hexa_gll_wkqs(imem_var,m,l,k,iel)
   
                        tmpx4 = (duxdx+duydy+duzdz)*(tmpx2 - tmpx3)
   
         !
         !------------->anelastic stress at step n+1/2 following s. ma and p. liu (2006) using epsnp1 and unrelaxed material modulus
                        tauxx_n12 = tmpx1*rg_hexa_gll_ksixx(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*duxdx + tmpx4)
                        tauyy_n12 = tmpx1*rg_hexa_gll_ksiyy(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*duydy + tmpx4)
                        tauzz_n12 = tmpx1*rg_hexa_gll_ksizz(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*duzdz + tmpx4)
                        tauxy_n12 = tmpx1*rg_hexa_gll_ksixy(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*ONE_HALF_RXP*(duxdy+duydx))
                        tauxz_n12 = tmpx1*rg_hexa_gll_ksixz(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*ONE_HALF_RXP*(duxdz+duzdx))
                        tauyz_n12 = tmpx1*rg_hexa_gll_ksiyz(imem_var,m,l,k,iel) + (ONE_RXP - tmpx1) * (tmpx3*ONE_HALF_RXP*(duydz+duzdy))
         !        
         !------------->compute final stress at step n+1 according to day and minster (1984) + ma and liu (2006) : tauxx - SUM anelastic stress at step n+1
                        tauxx = tauxx - ONE_HALF_RXP*(tauxx_n12 + rg_hexa_gll_ksixx(imem_var,m,l,k,iel))
                        tauyy = tauyy - ONE_HALF_RXP*(tauyy_n12 + rg_hexa_gll_ksiyy(imem_var,m,l,k,iel))
                        tauzz = tauzz - ONE_HALF_RXP*(tauzz_n12 + rg_hexa_gll_ksizz(imem_var,m,l,k,iel))
                        tauxy = tauxy - ONE_HALF_RXP*(tauxy_n12 + rg_hexa_gll_ksixy(imem_var,m,l,k,iel))
                        tauxz = tauxz - ONE_HALF_RXP*(tauxz_n12 + rg_hexa_gll_ksixz(imem_var,m,l,k,iel))
                        tauyz = tauyz - ONE_HALF_RXP*(tauyz_n12 + rg_hexa_gll_ksiyz(imem_var,m,l,k,iel))
         !        
         !------------->update memory for stress (step n+1/2)
                        rg_hexa_gll_ksixx(imem_var,m,l,k,iel) = tauxx_n12
                        rg_hexa_gll_ksiyy(imem_var,m,l,k,iel) = tauyy_n12
                        rg_hexa_gll_ksizz(imem_var,m,l,k,iel) = tauzz_n12
                        rg_hexa_gll_ksixy(imem_var,m,l,k,iel) = tauxy_n12
                        rg_hexa_gll_ksixz(imem_var,m,l,k,iel) = tauxz_n12
                        rg_hexa_gll_ksiyz(imem_var,m,l,k,iel) = tauyz_n12
   
                     enddo
   
                 endif
         !      
         !---------->store members of integration of the gll node klm
                     intpx1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx*dxidx+tauxy*dxidy+tauxz*dxidz)
                     intpx2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx*detdx+tauxy*detdy+tauxz*detdz)
                     intpx3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxx*dzedx+tauxy*dzedy+tauxz*dzedz)
   
                     intpy1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy*dxidx+tauyy*dxidy+tauyz*dxidz)
                     intpy2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy*detdx+tauyy*detdy+tauyz*detdz)
                     intpy3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxy*dzedx+tauyy*dzedy+tauyz*dzedz)
   
                     intpz1(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz*dxidx+tauyz*dxidy+tauzz*dxidz)
                     intpz2(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz*detdx+tauyz*detdy+tauzz*detdz)
                     intpz3(m,l,k) = rg_hexa_gll_jacobian_det(m,l,k,iel)*(tauxz*dzedx+tauyz*dzedy+tauzz*dzedz)
                  enddo !xi
               enddo    !eta
            enddo       !zeta
   
         !
         !->finish integration for hexa (internal forces at step n+1)
            do k = ONE_IXP,IG_NGLL
               do l = ONE_IXP,IG_NGLL
                  do m = ONE_IXP,IG_NGLL
   
                     tmpx1 = intpx1(ONE_IXP,l,k)*rg_gll_lagrange_deriv(m,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpx1(TWO_IXP,l,k)*rg_gll_lagrange_deriv(m,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpx1(THREE_IXP,l,k)*rg_gll_lagrange_deriv(m,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpx1(FOUR_IXP,l,k)*rg_gll_lagrange_deriv(m,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpx1(FIVE_IXP,l,k)*rg_gll_lagrange_deriv(m,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpx1(SIX_IXP,l,k)*rg_gll_lagrange_deriv(m,SIX_IXP)*rg_gll_weight(SIX_IXP) &
                           + intpx1(SEVEN_IXP,l,k)*rg_gll_lagrange_deriv(m,SEVEN_IXP)*rg_gll_weight(SEVEN_IXP)
   
                     tmpy1 = intpy1(ONE_IXP,l,k)*rg_gll_lagrange_deriv(m,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpy1(TWO_IXP,l,k)*rg_gll_lagrange_deriv(m,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpy1(THREE_IXP,l,k)*rg_gll_lagrange_deriv(m,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpy1(FOUR_IXP,l,k)*rg_gll_lagrange_deriv(m,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpy1(FIVE_IXP,l,k)*rg_gll_lagrange_deriv(m,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpy1(SIX_IXP,l,k)*rg_gll_lagrange_deriv(m,SIX_IXP)*rg_gll_weight(SIX_IXP) &
                           + intpy1(SEVEN_IXP,l,k)*rg_gll_lagrange_deriv(m,SEVEN_IXP)*rg_gll_weight(SEVEN_IXP)
   
                     tmpz1 = intpz1(ONE_IXP,l,k)*rg_gll_lagrange_deriv(m,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpz1(TWO_IXP,l,k)*rg_gll_lagrange_deriv(m,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpz1(THREE_IXP,l,k)*rg_gll_lagrange_deriv(m,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpz1(FOUR_IXP,l,k)*rg_gll_lagrange_deriv(m,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpz1(FIVE_IXP,l,k)*rg_gll_lagrange_deriv(m,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpz1(SIX_IXP,l,k)*rg_gll_lagrange_deriv(m,SIX_IXP)*rg_gll_weight(SIX_IXP) &
                           + intpz1(SEVEN_IXP,l,k)*rg_gll_lagrange_deriv(m,SEVEN_IXP)*rg_gll_weight(SEVEN_IXP)
   
                     tmpx2 = intpx2(m,ONE_IXP,k)*rg_gll_lagrange_deriv(l,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpx2(m,TWO_IXP,k)*rg_gll_lagrange_deriv(l,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpx2(m,THREE_IXP,k)*rg_gll_lagrange_deriv(l,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpx2(m,FOUR_IXP,k)*rg_gll_lagrange_deriv(l,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpx2(m,FIVE_IXP,k)*rg_gll_lagrange_deriv(l,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpx2(m,SIX_IXP,k)*rg_gll_lagrange_deriv(l,SIX_IXP)*rg_gll_weight(SIX_IXP) &
                           + intpx2(m,SEVEN_IXP,k)*rg_gll_lagrange_deriv(l,SEVEN_IXP)*rg_gll_weight(SEVEN_IXP)
   
                     tmpy2 = intpy2(m,ONE_IXP,k)*rg_gll_lagrange_deriv(l,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpy2(m,TWO_IXP,k)*rg_gll_lagrange_deriv(l,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpy2(m,THREE_IXP,k)*rg_gll_lagrange_deriv(l,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpy2(m,FOUR_IXP,k)*rg_gll_lagrange_deriv(l,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpy2(m,FIVE_IXP,k)*rg_gll_lagrange_deriv(l,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpy2(m,SIX_IXP,k)*rg_gll_lagrange_deriv(l,SIX_IXP)*rg_gll_weight(SIX_IXP) &
                           + intpy2(m,SEVEN_IXP,k)*rg_gll_lagrange_deriv(l,SEVEN_IXP)*rg_gll_weight(SEVEN_IXP)
   
                     tmpz2 = intpz2(m,ONE_IXP,k)*rg_gll_lagrange_deriv(l,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpz2(m,TWO_IXP,k)*rg_gll_lagrange_deriv(l,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpz2(m,THREE_IXP,k)*rg_gll_lagrange_deriv(l,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpz2(m,FOUR_IXP,k)*rg_gll_lagrange_deriv(l,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpz2(m,FIVE_IXP,k)*rg_gll_lagrange_deriv(l,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpz2(m,SIX_IXP,k)*rg_gll_lagrange_deriv(l,SIX_IXP)*rg_gll_weight(SIX_IXP) &
                           + intpz2(m,SEVEN_IXP,k)*rg_gll_lagrange_deriv(l,SEVEN_IXP)*rg_gll_weight(SEVEN_IXP)
   
                     tmpx3 = intpx3(m,l,ONE_IXP)*rg_gll_lagrange_deriv(k,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpx3(m,l,TWO_IXP)*rg_gll_lagrange_deriv(k,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpx3(m,l,THREE_IXP)*rg_gll_lagrange_deriv(k,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpx3(m,l,FOUR_IXP)*rg_gll_lagrange_deriv(k,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpx3(m,l,FIVE_IXP)*rg_gll_lagrange_deriv(k,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpx3(m,l,SIX_IXP)*rg_gll_lagrange_deriv(k,SIX_IXP)*rg_gll_weight(SIX_IXP) &
                           + intpx3(m,l,SEVEN_IXP)*rg_gll_lagrange_deriv(k,SEVEN_IXP)*rg_gll_weight(SEVEN_IXP)
   
                     tmpy3 = intpy3(m,l,ONE_IXP)*rg_gll_lagrange_deriv(k,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpy3(m,l,TWO_IXP)*rg_gll_lagrange_deriv(k,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpy3(m,l,THREE_IXP)*rg_gll_lagrange_deriv(k,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpy3(m,l,FOUR_IXP)*rg_gll_lagrange_deriv(k,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpy3(m,l,FIVE_IXP)*rg_gll_lagrange_deriv(k,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpy3(m,l,SIX_IXP)*rg_gll_lagrange_deriv(k,SIX_IXP)*rg_gll_weight(SIX_IXP) &
                           + intpy3(m,l,SEVEN_IXP)*rg_gll_lagrange_deriv(k,SEVEN_IXP)*rg_gll_weight(SEVEN_IXP)
   
                     tmpz3 = intpz3(m,l,ONE_IXP)*rg_gll_lagrange_deriv(k,ONE_IXP)*rg_gll_weight(ONE_IXP) &
                           + intpz3(m,l,TWO_IXP)*rg_gll_lagrange_deriv(k,TWO_IXP)*rg_gll_weight(TWO_IXP) &
                           + intpz3(m,l,THREE_IXP)*rg_gll_lagrange_deriv(k,THREE_IXP)*rg_gll_weight(THREE_IXP) &
                           + intpz3(m,l,FOUR_IXP)*rg_gll_lagrange_deriv(k,FOUR_IXP)*rg_gll_weight(FOUR_IXP) &
                           + intpz3(m,l,FIVE_IXP)*rg_gll_lagrange_deriv(k,FIVE_IXP)*rg_gll_weight(FIVE_IXP) &
                           + intpz3(m,l,SIX_IXP)*rg_gll_lagrange_deriv(k,SIX_IXP)*rg_gll_weight(SIX_IXP) &
                           + intpz3(m,l,SEVEN_IXP)*rg_gll_lagrange_deriv(k,SEVEN_IXP)*rg_gll_weight(SEVEN_IXP) 
   
                     fac1 = rg_gll_weight(l)*rg_gll_weight(k)
                     fac2 = rg_gll_weight(m)*rg_gll_weight(k)
                     fac3 = rg_gll_weight(m)*rg_gll_weight(l)
   
                     rl_acceleration_gll(ONE_IXP,m,l,k) = rl_acceleration_gll(ONE_IXP,m,l,k) + (fac1*tmpx1 + fac2*tmpx2 + fac3*tmpx3)
                     rl_acceleration_gll(TWO_IXP,m,l,k) = rl_acceleration_gll(TWO_IXP,m,l,k) + (fac1*tmpy1 + fac2*tmpy2 + fac3*tmpy3)
                     rl_acceleration_gll(THREE_IXP,m,l,k) = rl_acceleration_gll(THREE_IXP,m,l,k) + (fac1*tmpz1 + fac2*tmpz2 + fac3*tmpz3)
   
                  enddo
               enddo
            enddo
   
            do k = ONE_IXP,IG_NGLL        !zeta
               do l = ONE_IXP,IG_NGLL     !eta
                  do m = ONE_IXP,IG_NGLL  !xi
   
                     igll                        = ig_hexa_gll_glonum(m,l,k,iel)
   
                     rg_gll_acceleration(ONE_IXP,igll) = rg_gll_acceleration(ONE_IXP,igll) - rl_acceleration_gll(ONE_IXP,m,l,k)
                     rg_gll_acceleration(TWO_IXP,igll) = rg_gll_acceleration(TWO_IXP,igll) - rl_acceleration_gll(TWO_IXP,m,l,k)
                     rg_gll_acceleration(THREE_IXP,igll) = rg_gll_acceleration(THREE_IXP,igll) - rl_acceleration_gll(THREE_IXP,m,l,k)
   
                  enddo
               enddo
            enddo
   
         enddo !loop on hexahedron elements
   
         return
!***********************************************************************************************************************************************************************************
      end subroutine compute_internal_forces_order6
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine computes absorption forces @f$ \int _{\Gamma}  \mathbf{v} ^{T} \cdot \mathbf{T} \, d\Gamma @f$ for any spectral-elements order.
!!A so-called 'P1' explicit paraxial formulation is used to approximate the traction.
!***********************************************************************************************************************************************************************************
   subroutine compute_absorption_forces()
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only : &
                                       rg_gll_velocity&
                                      ,rg_gll_acceleration&
                                      ,rg_gll_acctmp&
                                      ,rg_dt&
                                      ,RG_NEWMARK_GAMMA&
                                      ,IG_NGLL&
                                      ,rg_gll_weight&
                                      ,ig_nquad_parax&
                                      ,rg_quadp_gll_jaco_det&
                                      ,rg_quadp_gll_normal&
                                      ,ig_quadp_gll_glonum&
                                      ,rg_quadp_gll_rhovp&
                                      ,rg_quadp_gll_rhovs

      implicit none

      real   (kind=RXP) :: vx
      real   (kind=RXP) :: vy
      real   (kind=RXP) :: vz
      real   (kind=RXP) :: jaco
      real   (kind=RXP) :: nx
      real   (kind=RXP) :: ny
      real   (kind=RXP) :: nz
      real   (kind=RXP) :: tx
      real   (kind=RXP) :: ty
      real   (kind=RXP) :: tz
      real   (kind=RXP) :: vn

      integer(kind=IXP) :: iquad
      integer(kind=IXP) :: k
      integer(kind=IXP) :: l
      integer(kind=IXP) :: igll

      do iquad = ONE_IXP,ig_nquad_parax

         do k = ONE_IXP,IG_NGLL
            do l = ONE_IXP,IG_NGLL
    
               jaco   =  rg_quadp_gll_jaco_det(l,k,iquad)

               nx     =  rg_quadp_gll_normal(1_IXP,l,k,iquad)
               ny     =  rg_quadp_gll_normal(2_IXP,l,k,iquad)
               nz     =  rg_quadp_gll_normal(3_IXP,l,k,iquad)
   
               igll   = ig_quadp_gll_glonum(l,k,iquad)
      
               vx     = rg_gll_velocity(1_IXP,igll) + rg_dt*(RG_NEWMARK_GAMMA*rg_gll_acctmp(1_IXP,igll)) 
               vy     = rg_gll_velocity(2_IXP,igll) + rg_dt*(RG_NEWMARK_GAMMA*rg_gll_acctmp(2_IXP,igll)) 
               vz     = rg_gll_velocity(3_IXP,igll) + rg_dt*(RG_NEWMARK_GAMMA*rg_gll_acctmp(3_IXP,igll)) 
   
               vn     = vx*nx+vy*ny+vz*nz
    
               tx     =  rg_quadp_gll_rhovp(l,k,iquad)*vn*nx + rg_quadp_gll_rhovs(l,k,iquad)*(vx-vn*nx)
               ty     =  rg_quadp_gll_rhovp(l,k,iquad)*vn*ny + rg_quadp_gll_rhovs(l,k,iquad)*(vy-vn*ny)
               tz     =  rg_quadp_gll_rhovp(l,k,iquad)*vn*nz + rg_quadp_gll_rhovs(l,k,iquad)*(vz-vn*nz)
       
               rg_gll_acceleration(1_IXP,igll) = rg_gll_acceleration(1_IXP,igll) - jaco*rg_gll_weight(l)*rg_gll_weight(k)*tx
               rg_gll_acceleration(2_IXP,igll) = rg_gll_acceleration(2_IXP,igll) - jaco*rg_gll_weight(l)*rg_gll_weight(k)*ty
               rg_gll_acceleration(3_IXP,igll) = rg_gll_acceleration(3_IXP,igll) - jaco*rg_gll_weight(l)*rg_gll_weight(k)*tz
   
            enddo
         enddo

      enddo
   
      return
!***********************************************************************************************************************************************************************************
      end subroutine compute_absorption_forces
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine sets external forces @f$ F^{ext} @f$ of the system @f$ M\ddot{U} + C\dot{U} + KU = F^{ext} @f$ for double couple and single force point sources.
!>@return : filled external force vector : see global variable mod_global_variables::rg_gll_acceleration
!***********************************************************************************************************************************************************************************
   subroutine compute_external_forces()
!***********************************************************************************************************************************************************************************

      use mpi
     
      use mod_global_variables, only :& 
                                      IG_NGLL&
                                     ,rg_simu_current_time&
                                     ,rg_gll_acceleration&
                                     ,ig_ndcsource&
                                     ,ig_nsfsource&
                                     ,ig_nfault&
                                     ,tg_dcsource&
                                     ,tg_sfsource&
                                     ,tg_fault&
                                     ,ig_hexa_gll_glonum&
                                     ,ig_idt&
                                     ,rg_dt&
                                     ,rg_dcsource_user_func&
                                     ,rg_sfsource_user_func
     
      
      use mod_source_function
      
      implicit none
      
      real   (kind=R64)       :: s_dp
      real   (kind=R64), save :: val_dc_dp
      real   (kind=R64), save :: val_pf_dp
     
      real   (kind=RXP)       :: fac
      real   (kind=RXP)       :: val
      
      integer(kind=IXP)       :: ifault
      integer(kind=IXP)       :: iso
      integer(kind=IXP)       :: ipf
      integer(kind=IXP)       :: k
      integer(kind=IXP)       :: l
      integer(kind=IXP)       :: m
      integer(kind=IXP)       :: igll
      integer(kind=IXP)       :: idir

!
!
!*****************************************************************************************************
!---->extended seismic fault
!*****************************************************************************************************

      do ifault = ONE_IXP,ig_nfault

         do iso = ONE_IXP,tg_fault(ifault)%ndcsource

            val = tg_fault(ifault)%dcsource(iso)%stf(ig_idt)
  
            do k = ONE_IXP,IG_NGLL

               do l = ONE_IXP,IG_NGLL

                  do m = ONE_IXP,IG_NGLL
          
                     igll                            = ig_hexa_gll_glonum(m,l,k,tg_fault(ifault)%dcsource(iso)%p%iel)
          
                     rg_gll_acceleration(1_IXP,igll) = rg_gll_acceleration(1_IXP,igll) + tg_fault(ifault)%dcsource(iso)%gll_force(1_IXP,m,l,k)*val
                     rg_gll_acceleration(2_IXP,igll) = rg_gll_acceleration(2_IXP,igll) + tg_fault(ifault)%dcsource(iso)%gll_force(2_IXP,m,l,k)*val
                     rg_gll_acceleration(3_IXP,igll) = rg_gll_acceleration(3_IXP,igll) + tg_fault(ifault)%dcsource(iso)%gll_force(3_IXP,m,l,k)*val
          
                  enddo

               enddo

            enddo

         enddo

      enddo

!
!
!*****************************************************************************************************
!---->double couple point source located at any location
!*****************************************************************************************************

      do iso = ONE_IXP,ig_ndcsource
     
         if (tg_dcsource(iso)%icur == 2_IXP) then
     
            val = dirac(ig_idt,rg_dt,tg_dcsource(iso)%shift_time,ONE_RXP)
     
         elseif (tg_dcsource(iso)%icur == 3_IXP) then
     
            val = gabor(rg_simu_current_time,tg_dcsource(iso)%shift_time,tg_dcsource(iso)%rise_time,ONE_RXP)
      
         elseif (tg_dcsource(iso)%icur == 4_IXP) then
     
            val = expcos(rg_simu_current_time,tg_dcsource(iso)%shift_time,tg_dcsource(iso)%rise_time)
     
         elseif (tg_dcsource(iso)%icur == 5_IXP) then
     
            if (ig_idt == ONE_IXP) val_dc_dp = ZERO_R64
     
            call ispli3_dp(rg_simu_current_time,tg_dcsource(iso)%rise_time,s_dp)
     
            val_dc_dp = val_dc_dp + s_dp*real(rg_dt,kind=R64)
            val       = real(val_dc_dp,kind=RXP)
     
         elseif (tg_dcsource(iso)%icur == 6_IXP) then
     
            val = ricker(rg_simu_current_time,tg_dcsource(iso)%shift_time,tg_dcsource(iso)%rise_time,ONE_RXP)
     
         elseif (tg_dcsource(iso)%icur == 7_IXP) then
     
            val = spiexp(rg_simu_current_time,tg_dcsource(iso)%rise_time,ONE_RXP)
     
         elseif (tg_dcsource(iso)%icur == 8_IXP) then
     
            val = fctanh(rg_simu_current_time,tg_dcsource(iso)%shift_time,tg_dcsource(iso)%rise_time,ONE_RXP)
     
         elseif (tg_dcsource(iso)%icur == 9_IXP) then
     
            val = fctanh_dt(rg_simu_current_time,tg_dcsource(iso)%shift_time,tg_dcsource(iso)%rise_time,ONE_RXP)
     
         elseif (tg_dcsource(iso)%icur == 10_IXP) then
     
            val = rg_dcsource_user_func(ig_idt)
     
         elseif (tg_dcsource(iso)%icur == 11_IXP) then
     
            val = f11(rg_simu_current_time,tg_dcsource(iso)%shift_time,tg_dcsource(iso)%rise_time,2.0_RXP)

         endif
      
         do k = ONE_IXP,IG_NGLL

            do l = ONE_IXP,IG_NGLL

               do m = ONE_IXP,IG_NGLL

                  igll                            = ig_hexa_gll_glonum(m,l,k,tg_dcsource(iso)%p%iel)

                  rg_gll_acceleration(1_IXP,igll) = rg_gll_acceleration(1_IXP,igll) + tg_dcsource(iso)%gll_force(1_IXP,m,l,k)*val
                  rg_gll_acceleration(2_IXP,igll) = rg_gll_acceleration(2_IXP,igll) + tg_dcsource(iso)%gll_force(2_IXP,m,l,k)*val
                  rg_gll_acceleration(3_IXP,igll) = rg_gll_acceleration(3_IXP,igll) + tg_dcsource(iso)%gll_force(3_IXP,m,l,k)*val

               enddo

            enddo

         enddo
     
      enddo
     
     
!
!
!*****************************************************************************************************
!---->single force point source located at gll nodes
!*****************************************************************************************************   
      do ipf = ONE_IXP,ig_nsfsource
 
         if (tg_sfsource(ipf)%icur == 2_IXP) then
     
            val = dirac(ig_idt,rg_dt,tg_sfsource(ipf)%shift_time,ONE_RXP)
     
         elseif (tg_sfsource(ipf)%icur == 3_IXP) then
     
            val = gabor(rg_simu_current_time,tg_sfsource(ipf)%shift_time,tg_sfsource(ipf)%rise_time,ONE_RXP)
     
         elseif (tg_sfsource(ipf)%icur == 4_IXP) then
     
            val = expcos(rg_simu_current_time,tg_sfsource(ipf)%shift_time,tg_sfsource(ipf)%rise_time)
     
         elseif (tg_sfsource(ipf)%icur == 5_IXP) then
     
            if (ig_idt == 1_IXP) val_pf_dp = ZERO_R64
     
            call ispli3_dp(rg_simu_current_time,tg_sfsource(ipf)%rise_time,s_dp)
     
            val_pf_dp = val_pf_dp + s_dp*real(rg_dt,kind=R64)
            val       = real(val_pf_dp,kind=RXP)
     
         elseif (tg_sfsource(ipf)%icur == 6_IXP) then
     
            val = ricker(rg_simu_current_time,tg_sfsource(ipf)%shift_time,tg_sfsource(ipf)%rise_time,ONE_RXP)
     
         elseif (tg_sfsource(ipf)%icur == 7_IXP) then
     
            val = spiexp(rg_simu_current_time,tg_sfsource(ipf)%rise_time,ONE_RXP)
     
         elseif (tg_sfsource(ipf)%icur == 8_IXP) then
     
            val = fctanh(rg_simu_current_time,tg_sfsource(ipf)%shift_time,tg_sfsource(ipf)%rise_time,ONE_RXP)
     
         elseif (tg_sfsource(ipf)%icur == 9_IXP) then
     
            val = fctanh_dt(rg_simu_current_time,tg_sfsource(ipf)%shift_time,tg_sfsource(ipf)%rise_time,ONE_RXP)
     
         elseif (tg_sfsource(ipf)%icur == 10_IXP) then
     
            val = rg_sfsource_user_func(ig_idt)
     
         elseif (tg_sfsource(ipf)%icur == 11_IXP) then
     
            val = f11(rg_simu_current_time,tg_sfsource(ipf)%shift_time,tg_sfsource(ipf)%rise_time,2.0_RXP)
     
         endif
      
         igll                           = tg_sfsource(ipf)%iequ
         idir                           = tg_sfsource(ipf)%idir
         fac                            = tg_sfsource(ipf)%fac 
         rg_gll_acceleration(idir,igll) = rg_gll_acceleration(idir,igll) + val*fac
      
      enddo
      
      return

!***********************************************************************************************************************************************************************************
   end subroutine compute_external_forces
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine imposes a displacement located at mod_global_variables::rg_plane_wave_z.
!***********************************************************************************************************************************************************************************
      subroutine compute_plane_wave()
!***********************************************************************************************************************************************************************************

         use mpi

         use mod_global_variables, only :&
                                         IG_NDOF&
                                        ,ig_idt&
                                        ,ig_plane_wave_ngll&
                                        ,ig_plane_wave_gll&
                                        ,rg_gll_displacement&
                                        ,rg_plane_wave_sft_user&
                                        ,ig_plane_wave_dir
   
         implicit none
   
         integer(kind=IXP) :: idof
         integer(kind=IXP) :: igll
         integer(kind=IXP) :: jgll

         do igll = ONE_IXP,ig_plane_wave_ngll

            jgll = ig_plane_wave_gll(igll)

            do idof = ONE_IXP,IG_NDOF

               if (idof == ig_plane_wave_dir) then

                  rg_gll_displacement(idof,jgll) = rg_plane_wave_sft_user(ig_idt)

               else

                  rg_gll_displacement(idof,jgll) = ZERO_RXP

               endif

            enddo

         enddo

         return
!***********************************************************************************************************************************************************************************
      end subroutine compute_plane_wave
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine computes absorption forces @f$ \int _{\Gamma}  \mathbf{v} ^{T} \cdot \mathbf{T} \, d\Gamma @f$ on the bottom boundary for any spectral-elements order.
!***********************************************************************************************************************************************************************************
   subroutine compute_absorption_forces_bottom_only()
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only : &
                                       rg_gll_velocity&
                                      ,rg_gll_acceleration&
                                      ,rg_gll_acctmp&
                                      ,rg_dt&
                                      ,RG_NEWMARK_GAMMA&
                                      ,IG_NGLL&
                                      ,rg_gll_weight&
                                      ,ig_plane_wave_nquadp_bottom&
                                      ,ig_plane_wave_quadp_bottom&
                                      ,rg_quadp_gll_jaco_det&
                                      ,rg_quadp_gll_normal&
                                      ,ig_quadp_gll_glonum&
                                      ,rg_quadp_gll_rhovp&
                                      ,rg_quadp_gll_rhovs

      implicit none

      real   (kind=RXP) :: vx
      real   (kind=RXP) :: vy
      real   (kind=RXP) :: vz
      real   (kind=RXP) :: jaco
      real   (kind=RXP) :: nx
      real   (kind=RXP) :: ny
      real   (kind=RXP) :: nz
      real   (kind=RXP) :: tx
      real   (kind=RXP) :: ty
      real   (kind=RXP) :: tz
      real   (kind=RXP) :: vn

      integer(kind=IXP) :: jquad
      integer(kind=IXP) :: iquad
      integer(kind=IXP) :: k
      integer(kind=IXP) :: l
      integer(kind=IXP) :: igll

!
!---->compute wave absorption for the bottom-most boundary elements

      do jquad = ONE_IXP,ig_plane_wave_nquadp_bottom

         iquad = ig_plane_wave_quadp_bottom(jquad)

         do k = ONE_IXP,IG_NGLL
            do l = ONE_IXP,IG_NGLL
    
               jaco   =  rg_quadp_gll_jaco_det(l,k,iquad)

               nx     =  rg_quadp_gll_normal(1_IXP,l,k,iquad)
               ny     =  rg_quadp_gll_normal(2_IXP,l,k,iquad)
               nz     =  rg_quadp_gll_normal(3_IXP,l,k,iquad)
   
               igll   = ig_quadp_gll_glonum(l,k,iquad)
      
               vx     = rg_gll_velocity(1_IXP,igll) + rg_dt*(RG_NEWMARK_GAMMA*rg_gll_acctmp(1_IXP,igll)) 
               vy     = rg_gll_velocity(2_IXP,igll) + rg_dt*(RG_NEWMARK_GAMMA*rg_gll_acctmp(2_IXP,igll)) 
               vz     = rg_gll_velocity(3_IXP,igll) + rg_dt*(RG_NEWMARK_GAMMA*rg_gll_acctmp(3_IXP,igll)) 
   
               vn     = vx*nx+vy*ny+vz*nz
    
               tx     =  rg_quadp_gll_rhovp(l,k,iquad)*vn*nx + rg_quadp_gll_rhovs(l,k,iquad)*(vx-vn*nx)
               ty     =  rg_quadp_gll_rhovp(l,k,iquad)*vn*ny + rg_quadp_gll_rhovs(l,k,iquad)*(vy-vn*ny)
               tz     =  rg_quadp_gll_rhovp(l,k,iquad)*vn*nz + rg_quadp_gll_rhovs(l,k,iquad)*(vz-vn*nz)
       
               rg_gll_acceleration(1_IXP,igll) = rg_gll_acceleration(1_IXP,igll) - jaco*rg_gll_weight(l)*rg_gll_weight(k)*tx
               rg_gll_acceleration(2_IXP,igll) = rg_gll_acceleration(2_IXP,igll) - jaco*rg_gll_weight(l)*rg_gll_weight(k)*ty
               rg_gll_acceleration(3_IXP,igll) = rg_gll_acceleration(3_IXP,igll) - jaco*rg_gll_weight(l)*rg_gll_weight(k)*tz
   
            enddo
         enddo

      enddo
   
      return
!***********************************************************************************************************************************************************************************
      end subroutine compute_absorption_forces_bottom_only
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine imposes Dirichlet boundary condition on the vertical boundaries
!!and computes absorption forces @f$ \int _{\Gamma}  \mathbf{v} ^{T} \cdot \mathbf{T} \, d\Gamma @f$ on the bottom boundary for any spectral-elements order.
!***********************************************************************************************************************************************************************************
   subroutine compute_dirichlet_conditions()
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only : &
                                       IG_NGLL&
                                      ,IG_NDOF&
                                      ,rg_gll_displacement&
                                      ,ig_plane_wave_nquadp_side&
                                      ,ig_quadp_gll_glonum&
                                      ,ig_plane_wave_dir&
                                      ,ig_plane_wave_quadp_side

      implicit none

      integer(kind=IXP) :: idof
      integer(kind=IXP) :: jquad
      integer(kind=IXP) :: iquad
      integer(kind=IXP) :: k
      integer(kind=IXP) :: l
      integer(kind=IXP) :: igll

!TODO: it is possible to absorb the z-component of motion using z-only paraxial boundary condition on the vertical boundaries
!
!---->imposes Dirichlet boundary condition on the vertical boundaries

      do jquad = ONE_IXP,ig_plane_wave_nquadp_side

         iquad = ig_plane_wave_quadp_side(jquad)

         do k = ONE_IXP,IG_NGLL

            do l = ONE_IXP,IG_NGLL

               igll = ig_quadp_gll_glonum(l,k,iquad)

                  do idof = ONE_IXP,IG_NDOF
                  
                     if (.not.(idof == ig_plane_wave_dir)) then
                  
                        rg_gll_displacement(idof,igll) = ZERO_RXP
                  
                     endif
                  
                  enddo
    
            enddo

         enddo

      enddo
  
      return
!***********************************************************************************************************************************************************************************
      end subroutine compute_dirichlet_conditions
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine computes the divergence of a vector field at the middle gll o surface quandrangle
!***********************************************************************************************************************************************************************************
      subroutine compute_div_curl_gll()
!***********************************************************************************************************************************************************************************

         use mod_global_variables, only :&
                                         IG_NGLL&
                                        ,IG_NDOF&
                                        ,IG_LAGRANGE_ORDER&
                                        ,ig_hexa_face2mid_gll&
                                        ,ig_hexa_gll_glonum&
                                        ,ig_nquad_fsurf&
                                        ,ig_quadf_neighbor_hexa&
                                        ,ig_quadf_neighbor_hexaface&
                                        ,rg_quadf_disp_spatial_deriv&
                                        ,rg_gll_displacement&
                                        ,rg_hexa_gll_dxidx&
                                        ,rg_hexa_gll_dxidy&
                                        ,rg_hexa_gll_dxidz&
                                        ,rg_hexa_gll_detdx&
                                        ,rg_hexa_gll_detdy&
                                        ,rg_hexa_gll_detdz&
                                        ,rg_hexa_gll_dzedx&
                                        ,rg_hexa_gll_dzedy&
                                        ,rg_hexa_gll_dzedz&
                                        ,rg_gll_lagrange_deriv

         implicit none

         real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: rl_displacement_gll

         integer(kind=IXP)                                          :: ihexa
         integer(kind=IXP)                                          :: iquad
         integer(kind=IXP)                                          :: iface
         integer(kind=IXP)                                          :: k
         integer(kind=IXP)                                          :: l
         integer(kind=IXP)                                          :: m
         integer(kind=IXP)                                          :: n
         integer(kind=IXP)                                          :: igll

         real   (kind=RXP)                                          :: duxdxi
         real   (kind=RXP)                                          :: duxdet
         real   (kind=RXP)                                          :: duxdze
         real   (kind=RXP)                                          :: duydxi
         real   (kind=RXP)                                          :: duydet
         real   (kind=RXP)                                          :: duydze
         real   (kind=RXP)                                          :: duzdxi
         real   (kind=RXP)                                          :: duzdet
         real   (kind=RXP)                                          :: duzdze
         real   (kind=RXP)                                          :: duxdx
         real   (kind=RXP)                                          :: duydy
         real   (kind=RXP)                                          :: duzdz
         real   (kind=RXP)                                          :: duxdy
         real   (kind=RXP)                                          :: duxdz
         real   (kind=RXP)                                          :: duydx
         real   (kind=RXP)                                          :: duydz
         real   (kind=RXP)                                          :: duzdx
         real   (kind=RXP)                                          :: duzdy
         real   (kind=RXP)                                          :: dxidx
         real   (kind=RXP)                                          :: dxidy
         real   (kind=RXP)                                          :: dxidz
         real   (kind=RXP)                                          :: detdx
         real   (kind=RXP)                                          :: detdy
         real   (kind=RXP)                                          :: detdz
         real   (kind=RXP)                                          :: dzedx
         real   (kind=RXP)                                          :: dzedy
         real   (kind=RXP)                                          :: dzedz


         do iquad = ONE_IXP,ig_nquad_fsurf

            ihexa = ig_quadf_neighbor_hexa    (iquad)
            iface = ig_quadf_neighbor_hexaface(iquad)

!
!---------->fill local displacement
            do k = ONE_IXP,IG_NGLL        !zeta
               do l = ONE_IXP,IG_NGLL     !eta
                  do m = ONE_IXP,IG_NGLL  !xi
           
                     igll                         = ig_hexa_gll_glonum(m,l,k,ihexa)
           
                     rl_displacement_gll(1_IXP,m,l,k) = rg_gll_displacement(1_IXP,igll)
                     rl_displacement_gll(2_IXP,m,l,k) = rg_gll_displacement(2_IXP,igll)
                     rl_displacement_gll(3_IXP,m,l,k) = rg_gll_displacement(3_IXP,igll)
           
                  enddo
               enddo
            enddo

!
!---------->select middle gll node of face 'iface'
            k = ig_hexa_face2mid_gll(1_IXP,iface)
            l = ig_hexa_face2mid_gll(2_IXP,iface)
            m = ig_hexa_face2mid_gll(3_IXP,iface)
           
! 
!---------->derivative of displacement with respect to local coordinate xi, eta and zeta at the gll node klm

            duxdxi = ZERO_RXP
            duydxi = ZERO_RXP
            duzdxi = ZERO_RXP
            duxdet = ZERO_RXP
            duydet = ZERO_RXP
            duzdet = ZERO_RXP
            duxdze = ZERO_RXP
            duydze = ZERO_RXP
            duzdze = ZERO_RXP

            do n = ONE_IXP,IG_NGLL
               duxdxi = duxdxi + rl_displacement_gll(ONE_IXP,n,l,k)*rg_gll_lagrange_deriv(n,m)
            enddo

            do n = ONE_IXP,IG_NGLL
               duydxi = duydxi + rl_displacement_gll(TWO_IXP,n,l,k)*rg_gll_lagrange_deriv(n,m)
            enddo

            do n = ONE_IXP,IG_NGLL
               duzdxi = duzdxi + rl_displacement_gll(THREE_IXP,n,l,k)*rg_gll_lagrange_deriv(n,m)
            enddo
               
            do n = ONE_IXP,IG_NGLL
               duxdet = duxdet + rl_displacement_gll(ONE_IXP,m,n,k)*rg_gll_lagrange_deriv(n,l)
            enddo
               
            do n = ONE_IXP,IG_NGLL
               duydet = duydet + rl_displacement_gll(TWO_IXP,m,n,k)*rg_gll_lagrange_deriv(n,l)
            enddo
               
            do n = ONE_IXP,IG_NGLL
               duzdet = duzdet + rl_displacement_gll(THREE_IXP,m,n,k)*rg_gll_lagrange_deriv(n,l)
            enddo
               
            do n = ONE_IXP,IG_NGLL
               duxdze = duxdze + rl_displacement_gll(ONE_IXP,m,l,n)*rg_gll_lagrange_deriv(n,k)
            enddo
               
            do n = ONE_IXP,IG_NGLL
               duydze = duydze + rl_displacement_gll(TWO_IXP,m,l,n)*rg_gll_lagrange_deriv(n,k)
            enddo
               
            do n = ONE_IXP,IG_NGLL
               duzdze = duzdze + rl_displacement_gll(THREE_IXP,m,l,n)*rg_gll_lagrange_deriv(n,k)
            enddo
         
!       
!---------->derivative of displacement at step n+1 with respect to global coordinate x, y and z at the gll node klm
            dxidx = rg_hexa_gll_dxidx(m,l,k,ihexa)
            dxidy = rg_hexa_gll_dxidy(m,l,k,ihexa)
            dxidz = rg_hexa_gll_dxidz(m,l,k,ihexa)
            detdx = rg_hexa_gll_detdx(m,l,k,ihexa)
            detdy = rg_hexa_gll_detdy(m,l,k,ihexa)
            detdz = rg_hexa_gll_detdz(m,l,k,ihexa)
            dzedx = rg_hexa_gll_dzedx(m,l,k,ihexa)
            dzedy = rg_hexa_gll_dzedy(m,l,k,ihexa)
            dzedz = rg_hexa_gll_dzedz(m,l,k,ihexa)
         
            duxdx = duxdxi*dxidx + duxdet*detdx + duxdze*dzedx
            duxdy = duxdxi*dxidy + duxdet*detdy + duxdze*dzedy
            duxdz = duxdxi*dxidz + duxdet*detdz + duxdze*dzedz
            duydx = duydxi*dxidx + duydet*detdx + duydze*dzedx
            duydy = duydxi*dxidy + duydet*detdy + duydze*dzedy
            duydz = duydxi*dxidz + duydet*detdz + duydze*dzedz
            duzdx = duzdxi*dxidx + duzdet*detdx + duzdze*dzedx
            duzdy = duzdxi*dxidy + duzdet*detdy + duzdze*dzedy
            duzdz = duzdxi*dxidz + duzdet*detdz + duzdze*dzedz

!
!---------->store spatial derivative of displacement field at the middle gll of each free surface quad
            rg_quadf_disp_spatial_deriv(1_IXP,iquad) = duxdx
            rg_quadf_disp_spatial_deriv(2_IXP,iquad) = duxdy
            rg_quadf_disp_spatial_deriv(3_IXP,iquad) = duxdz
            rg_quadf_disp_spatial_deriv(4_IXP,iquad) = duydx
            rg_quadf_disp_spatial_deriv(5_IXP,iquad) = duydy
            rg_quadf_disp_spatial_deriv(6_IXP,iquad) = duydz
            rg_quadf_disp_spatial_deriv(7_IXP,iquad) = duzdx
            rg_quadf_disp_spatial_deriv(8_IXP,iquad) = duzdy
            rg_quadf_disp_spatial_deriv(9_IXP,iquad) = duzdz
           
         enddo

         return
!***********************************************************************************************************************************************************************************
      end subroutine compute_div_curl_gll
!***********************************************************************************************************************************************************************************

end module mod_solver
