!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to design low-pass finite impulse response or infinite impulse response filters.

!>@brief
!!This module contains subroutines to design low-pass filters used to filter and decimate receivers' time series in real-time (i.e., at simulation runtime)
!!or to filter time series after a simulation.
!!Most of the subroutines have been programmed from the book "Schaum's outline of theory and problems of digital signal processing", by Monson H. Hayes.
module mod_filter
   
   use mod_precision

   use mod_global_variables, only : CIL
   
   implicit none

   private

   public  :: init_fir_filter
   private :: init_fir_filter_kaiser
   private :: ideal_lowpass_odd
   private :: kaiser_odd
   private :: in0 
   private :: fir_to_freq
   public  :: apply_fir_filter
   public  :: filter_snapshot_surface_gll
   public  :: butterworth_lowpass
   public  :: butterworth_highpass
   public  :: butterworth_bandpass
   private :: recursive_filtering
   public  :: tandem
   public  :: tandem_one_pass
   public  :: decimate
   private :: find_nprimes
   private :: find_factors

   contains

!  
!
!>@brief subroutine to initialize finite impulse response filter filter
!***********************************************************************************************************************************************************************************
   subroutine init_fir_filter()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      rg_dt&
                                     ,ig_ndt&
                                     ,ig_snapshot_surf_gll_nsave&
                                     ,IG_FIR_DECIMATION_FAC&
                                     ,rg_fir_dt&
                                     ,rg_fir_filter_cutoff_freq&
                                     ,rg_fir_filter_transition_band&
                                     ,rg_fir_filter_attenuation&
                                     ,ig_fir_filter_order&
                                     ,rg_fir_filter

      use mod_write_listing, only : write_filter_info

      implicit none

      real(kind=RXP), parameter :: TRANSITION_BAND_PERC = 50.0_RXP
      real(kind=RXP)            :: fr_nyquist

      rg_fir_dt = rg_dt * real(IG_FIR_DECIMATION_FAC,kind=RXP)

      fr_nyquist = ONE_RXP/(TWO_RXP * rg_fir_dt)

      rg_fir_filter_cutoff_freq = fr_nyquist/(ONE_RXP + ONE_HALF_RXP*TRANSITION_BAND_PERC/100.0_RXP)

      rg_fir_filter_transition_band = rg_fir_filter_cutoff_freq*TRANSITION_BAND_PERC/100.0_RXP

      rg_fir_filter_attenuation = 100.0_RXP

      call init_fir_filter_kaiser(rg_dt,rg_fir_filter_cutoff_freq,rg_fir_filter_transition_band,rg_fir_filter_attenuation,ig_fir_filter_order,rg_fir_filter)

      ig_snapshot_surf_gll_nsave = int((ig_ndt-ONE_IXP)/IG_FIR_DECIMATION_FAC,kind=IXP) + ONE_IXP

      call write_filter_info("fir",rg_fir_filter_cutoff_freq,rg_fir_filter_transition_band,rg_fir_filter_attenuation,ig_fir_filter_order,IG_FIR_DECIMATION_FAC,rg_fir_dt,ig_snapshot_surf_gll_nsave)

      return

!***********************************************************************************************************************************************************************************
   end subroutine init_fir_filter
!***********************************************************************************************************************************************************************************


!  
!
!>@brief subroutine to design odd-order low-pass filter with linear phase by windows the unit sample response by a Kaiser window.
!>@param  dt  : time step of the simulation: @f$ \Delta t @f$ (see \link mod_global_variables::rg_dt \endlink)
!>@param  fc  : ideal cutoff frequency (in Hz).
!>@param  tr  : transition band (in Hz) from the passband cutoff frequency (fpass) to the stopband cutoff frequency (fstop).  The ideal cutoff frequency fc is in the middle of fpass and fstop.
!>@param  att : peak sidelobe of the stopband ripple of low-pass filter (i.e., attenuation in decibels)
!>@param  m   : filter order
!>@return finite_impulse_response : finite impluse response filter = (unit sample response) * (kaiser window)
!***********************************************************************************************************************************************************************************
   subroutine init_fir_filter_kaiser(dt,fc,tr,att,m,finite_impulse_response)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      cg_prefix&
                                     ,ig_myrank&
                                     ,get_newunit

      use mod_init_memory

      implicit none

      real   (kind=RXP), intent( in)                            :: dt
      real   (kind=RXP), intent( in)                            :: fc
      real   (kind=RXP), intent( in)                            :: tr
      real   (kind=RXP), intent( in)                            :: att
      integer(kind=IXP), intent(out)                            :: m
      real   (kind=RXP), intent(out), allocatable, dimension(:) :: finite_impulse_response

      integer(kind=IXP)                                         :: i
      integer(kind=IXP)                                         :: ios
      integer(kind=IXP)                                         :: np
      integer(kind=IXP)                                         :: np1
      integer(kind=IXP)                                         :: m2
      integer(kind=IXP)                                         :: myunit
      real   (kind=RXP)                                         :: fs
      real   (kind=RXP)             , allocatable, dimension(:) :: unit_sample_response
      real   (kind=RXP)             , allocatable, dimension(:) :: kaiser_window
      complex(kind=RXP)             , allocatable, dimension(:) :: cfir

!
!---->simulation frequency sampling

      fs = ONE_RXP/dt

!
!---->compute kaiser filter order

      m = int((att-7.95_RXP)/(14.36_RXP*(tr/fs)),kind=IXP)

!
!---->make the order odd if it is not

      if (mod(m,TWO_IXP) == ZERO_IXP) m = m + ONE_IXP
      
      np  = (m-ONE_IXP)/TWO_IXP
      np1 = np + ONE_IXP
      m2  = TWO_IXP*m

      ios = init_array_real   (unit_sample_response   ,np1,"unit_sample_response")
      ios = init_array_real   (kaiser_window          ,np1,"kaiser_window")
      ios = init_array_real   (finite_impulse_response,m  ,"finite_impulse_response")
      ios = init_array_complex(cfir                   ,m2 ,"cfir") !size arbitrary set to 2*m. can be changed to get better frequency sampling

      call ideal_lowpass_odd(unit_sample_response,np1,fc,fs)

      call kaiser_odd(kaiser_window,np1,att)

!
!---->compute finite impluse response filter = (unit sample response) * (kaiser window)

      do i = ONE_IXP,np1
         finite_impulse_response(np+i) = unit_sample_response(i)*kaiser_window(i)
      enddo

      do i = ONE_IXP,np
         finite_impulse_response(i) = finite_impulse_response(m-i+ONE_IXP)
      enddo

!
!---->compute frequency response

      call fir_to_freq(finite_impulse_response,m,m2,cfir)

!
!---->write finite impulse response and its frequency response to file

      if (ig_myrank == ZERO_IXP) then

         open(unit=get_newunit(myunit),file=trim(cg_prefix)//".fir.time",action='write')
         do i = ONE_IXP,m
            write(unit=myunit,fmt='(2(E15.7,1X))') real(i-np-ONE_IXP,kind=RXP)*dt,finite_impulse_response(i)
         enddo
         close(myunit)

         open(unit=get_newunit(myunit),file=trim(cg_prefix)//".fir.freq",action='write')
         do i = ONE_IXP,m2
            write(unit=myunit,fmt='(2(E15.7,1X))') real(i-ONE_IXP,kind=RXP)*(ONE_HALF_RXP*fs/real(m2,kind=RXP)),abs(cfir(i))
         enddo
         close(myunit)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_fir_filter_kaiser
!***********************************************************************************************************************************************************************************

!  
!
!>@brief subroutine to compute a truncated impulse response of an ideal odd-order low-pass filter with linear phase
!>@return a  : unit sample response
!>@param  n  : half-length of an odd order filter. if m = order, then n = (m-1)/2.
!>@param  fh : ideal cutoff frequency (in Hz)
!>@param  fs : sampling frequency of the simulation: fs = 1.0/@f$ \Delta t @f$ (see \link mod_global_variables::rg_dt \endlink)
!***********************************************************************************************************************************************************************************
   subroutine ideal_lowpass_odd(a,n,fh,fs)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)               :: n
      real   (kind=RXP), intent(out), dimension(n) :: a
      real   (kind=RXP), intent( in)               :: fh
      real   (kind=RXP), intent( in)               :: fs
                                        
      integer(kind=IXP)                            :: i
      real   (kind=RXP)                            :: ri

      a(ONE_IXP) = TWO_RXP*fh/fs

      do i = TWO_IXP,n

         ri   = real(i-ONE_IXP,kind=RXP)

         a(i) = ( sin(TWO_RXP*ri*PI_RXP*fh/fs) )/(ri*PI_RXP)

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine ideal_lowpass_odd
!***********************************************************************************************************************************************************************************

!
!
!>@brief subroutine to compute half of a symetric kaiser window discretized over an odd number of point
!!
!>@reference : J. F. Kaiser, "Nonrecursive digital filter design using I0-sinh window function".
!!
!>@return k : half of kaiser window
!>@param  n : half-length of an odd order filter. if m = order, then n = (m-1)/2.
!>@param  a : peak sidelobe of the stopband ripple of low-pass filter (i.e., attenuation in decibels)
!***********************************************************************************************************************************************************************************
   subroutine kaiser_odd(k,n,a)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)               :: n
      real   (kind=RXP), intent( in)               :: a
      real   (kind=RXP), intent(out), dimension(n) :: k

      integer(kind=IXP)                            :: i

      real   (kind=RXP)                            :: alpha
      real   (kind=RXP)                            :: in0alpha
      real   (kind=RXP)                            :: ri
      real   (kind=RXP)                            :: rn
      real   (kind=RXP)                            :: num
      

      if (a < 21.0_RXP) then

         alpha = ZERO_RXP

      elseif(a > 50.0_RXP) then

         alpha = 0.1102_RXP*(a-8.7_RXP)

      else

         alpha = 0.5842_RXP*(a-21.0_RXP)**0.4_RXP + 0.07886_RXP*(a-21.0_RXP)

      endif

      in0alpha = in0(alpha)
      rn       = real(n,kind=RXP)

!
!---->kaiser windows is symetric -> compute only the part from n to n+1 -> simplification in the square root

      do i = ONE_IXP,n

         ri   = real(i-ONE_IXP,kind=RXP)

         num  = in0(alpha*sqrt(ONE_RXP - (ri**TWO_IXP)/(rn**TWO_IXP)))

         k(i) = num/in0alpha

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine kaiser_odd
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute the modified Bessel function of order zero for real(kind=RXP)numbers (I0). we use the recurrence relation between n and n+1 terms to compute ds.
!>@param  x   : abscissa where to compute I0.
!>@return in0 : modified Bessel function of order zero
!***********************************************************************************************************************************************************************************
   real(kind=RXP) function in0(x)
!***********************************************************************************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in) :: x

      real(kind=RXP)             :: d
      real(kind=RXP)             :: ds

      d   = ZERO_RXP
      ds  = ONE_RXP
      in0 = ONE_RXP

      do while (ds > in0*EPSILON_MACHINE_RXP)

         d   = d + TWO_RXP

         ds  = ds * (x*x)/(d*d)

         in0 = in0 + ds

      enddo

      return

!***********************************************************************************************************************************************************************************
   end function in0
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute the frequency response H(exp(iw) of a FIR system:
!!       ch(z) = h(ONE_IXP)+h(TWO_IXP)z^(-1)+ ... + h(m)z^(-m))
!>@param h   : finite impluse response filter = (unit sample response) * (kaiser window)
!>@param m   : filter order
!>@param n   : number of frequency to discretize ch
!>@return ch : frequency response
!***********************************************************************************************************************************************************************************
   subroutine fir_to_freq(h,m,n,ch) 
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in)               :: m
      integer(kind=IXP), intent( in)               :: n
      real   (kind=RXP), intent( in), dimension(m) :: h
      complex(kind=RXP), intent(out), dimension(n) :: ch

      integer(kind=IXP)                            :: i
      integer(kind=IXP)                            :: k
      real   (kind=RXP)                            :: s
      real   (kind=RXP)                            :: sf
      real   (kind=RXP)                            :: sh
      real   (kind=RXP)                            :: csh
      real   (kind=RXP)                            :: ssh
      complex(kind=RXP)                            :: z
      complex(kind=RXP)                            :: hsum

      s = PI_RXP/real(n,kind=RXP)

      do k = ONE_IXP,n

         sf   = s*real(k-ONE_IXP,kind=RXP)

         hsum = cmplx(ZERO_RXP,ZERO_RXP)

         do i = TWO_IXP,m

            sh   = sf*real(i-ONE_IXP,kind=RXP)
            csh  =  cos(sh)
            ssh  = -sin(sh)
            z    = cmplx(csh,ssh)
            hsum = hsum + h(i)*z

         enddo

         ch(k) = hsum + h(ONE_IXP)

      enddo

      return
 
!***********************************************************************************************************************************************************************************
   end subroutine fir_to_freq
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute the output of a FIR filtered time series
!***********************************************************************************************************************************************************************************
   subroutine apply_fir_filter(h,m,buffer,x) 
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent(in)                  :: m
      real   (kind=RXP), intent(in)   , dimension(m) :: h
      real   (kind=RXP), intent(inout), dimension(m) :: buffer
      real   (kind=RXP), intent(inout)               :: x

      integer(kind=IXP)                              :: i
      real   (kind=RXP)                              :: sx

!
!---->update buffer with newly computed x,y,z

      buffer(ONE_IXP) = x

      sx = ZERO_RXP

!
!---->apply filter

      do i = ONE_IXP,m

         sx = sx + h(i)*buffer(i)

      enddo

!
!---->overwrite initial x,y,z values by filtered values

      x = sx

!
!---->prepare buffer for next time step

      do i = m,TWO_IXP,-ONE_IXP

         buffer(i) = buffer(i-ONE_IXP)

      enddo

      return 

!***********************************************************************************************************************************************************************************
   end subroutine apply_fir_filter
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to filter time series stored in file *.snapshot.gll.*
!***********************************************************************************************************************************************************************************
   subroutine filter_snapshot_surface_gll() 
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only:&
                                     IG_NGLL&
                                    ,IG_NDOF&
                                    ,LG_FIR_FILTER&
                                    ,LG_LUSTRE_FILE_SYS&
                                    ,LG_SNAPSHOT_SURF_SPA_DER&
                                    ,info_all_cpu&
                                    ,ig_ncpu&
                                    ,get_newunit&
                                    ,cg_prefix&
                                    ,cg_iir_filter_output_motion&
                                    ,ig_myrank&
                                    ,ig_snapshot_surf_gll_nsave&
                                    ,ig_ndt&
                                    ,rg_dt&
                                    ,rg_fir_dt&
                                    ,rg_iir_filter_cutoff_freq&
                                    ,rg_iir_filter_transition_band&
                                    ,ig_fir_filter_order&
                                    ,ig_mpi_nboctet_real&
                                    ,ig_mpi_comm_simu&
                                    ,ig_nquad_fsurf_saved

      use mod_init_memory, only :&
                                 init_array_real&
                                ,init_array_complex&
                                ,init_array_int

      use mod_write_listing, only : write_filter_info

      use mod_io_array     , only :&
                                   efi_mpi_file_open&
                                  ,get_file_rank_elt_offset&
                                  ,efi_mpi_file_write_at_all

      use mod_signal_processing, only :&
                                       ft&
                                      ,nextpow2

      implicit none

      integer(kind=IXP), parameter                         :: NMAXABS = 5_IXP
                                              
      real   (kind=R64), allocatable, dimension(:)         :: butter_coeff
      real   (kind=RXP), allocatable, dimension(:)         :: dirac
      complex(kind=RXP), allocatable, dimension(:)         :: dirac_fft
      real   (kind=R64)                                    :: butter_gain
      integer(kind=IXP)                                    :: butter_order
      integer(kind=IXP)                                    :: butter_func_order
      integer(kind=IXP)                                    :: nfft
      real   (kind=RXP)                                    :: ideal_cutoff
      real   (kind=RXP)                                    :: transition_band
      real   (kind=RXP)                                    :: freq_pass
      real   (kind=RXP)                                    :: freq_stop
      real   (kind=RXP)                                    :: ap
      real   (kind=RXP)                                    :: as
                                                         
      real   (kind=RXP)                                    :: dt
      real   (kind=RXP)                                    :: dt_dec
      real   (kind=RXP)                                    :: dt_shift
      real   (kind=RXP)                                    :: sampling_after_decimation
      real   (kind=RXP)                                    :: sampling_simulation
      integer(kind=IXP)                                    :: decimation_factor
!     integer(kind=IXP)                                    :: idec
      integer(kind=IXP)                                    :: idum
!     integer(kind=IXP)                                    :: nprimes
!     integer(kind=IXP), allocatable, dimension(:)         :: primes
                                                      
      real   (kind=RXP), allocatable, dimension(:,:,:,:,:) :: all_quad_gll_xyz_filter
      real   (kind=RXP), allocatable, dimension(:,:,:,:,:) :: all_quad_gll_xyz
      real   (kind=RXP), allocatable, dimension(:,:,:)     :: all_quad_spa_der_filter
      real   (kind=RXP), allocatable, dimension(:,:,:)     :: all_quad_spa_der
      real   (kind=RXP), allocatable, dimension(:,:,:,:)   :: all_quad_gll_maxabs
      real   (kind=RXP), allocatable, dimension(:,:)       :: xyz_raw
      real   (kind=RXP), allocatable, dimension(:)         :: x_filter
      real   (kind=RXP), allocatable, dimension(:)         :: y_filter
      real   (kind=RXP), allocatable, dimension(:)         :: z_filter
      real   (kind=RXP), allocatable, dimension(:)         :: duxdx_filter
      real   (kind=RXP), allocatable, dimension(:)         :: duxdy_filter
      real   (kind=RXP), allocatable, dimension(:)         :: duxdz_filter
      real   (kind=RXP), allocatable, dimension(:)         :: duydx_filter
      real   (kind=RXP), allocatable, dimension(:)         :: duydy_filter
      real   (kind=RXP), allocatable, dimension(:)         :: duydz_filter
      real   (kind=RXP), allocatable, dimension(:)         :: duzdx_filter
      real   (kind=RXP), allocatable, dimension(:)         :: duzdy_filter
      real   (kind=RXP), allocatable, dimension(:)         :: duzdz_filter
      real   (kind=RXP), allocatable, dimension(:)         :: time_dec
      real   (kind=RXP)                                    :: module_xy  = ZERO_RXP
      real   (kind=RXP)                                    :: module_xyz = ZERO_RXP
      integer(kind=I64)                                    :: pos_dis
                                                      
      integer(kind=I64)                                    :: ngll_int64
      integer(kind=I64)                                    :: ndof_int64
      integer(kind=I64)                                    :: mpi_nboctet_real_int64
      integer(kind=I64)                                    :: iquad_offset_int64
                                                      
      integer(kind=IXP)                                    :: myunit
      integer(kind=IXP)                                    :: unit_snap_gll_dis
      integer(kind=IXP)                                    :: unit_snap_quadf_dis_spa_der
      integer(kind=IXP)                                    :: type_vector_quad_level
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)        :: statut
                                                      
      integer(kind=IXP)                                    :: i
      integer(kind=IXP)                                    :: idof
      integer(kind=IXP)                                    :: idt
      integer(kind=IXP)                                    :: ndt
      integer(kind=IXP)                                    :: ndt_dec
      integer(kind=IXP)                                    :: ndt_tmp
      integer(kind=IXP)                                    :: iquad
      integer(kind=IXP)                                    :: iquad_offset
      integer(kind=IXP)                                    :: nquad
      integer(kind=IXP)                                    :: nquad_floor
      integer(kind=IXP)                                    :: nquad_fsurf_all_cpu
      integer(kind=IXP)                                    :: kgll
      integer(kind=IXP)                                    :: lgll
      integer(kind=IXP)                                    :: ios
                                                      
      character(len=CIL)                                   :: fname
      character(len=CIL)                                   :: fname_dec
      character(len=CIL)                                   :: fname_max
      character(len=CIL)                                   :: fname_sd
      character(len=CIL)                                   :: info
                                              
!
!
!**************************************************************************************************************************
!---->initialize appropriate time step if a runtime fir filter as been used
!**************************************************************************************************************************
      if (LG_FIR_FILTER) then

         dt       = rg_fir_dt
         dt_shift = real((ig_fir_filter_order-ONE_IXP)/TWO_IXP,kind=RXP) * rg_dt
         ndt      = ig_snapshot_surf_gll_nsave
   
      else

         dt       = rg_dt
         dt_shift = ZERO_RXP
         ndt      = ig_ndt

      endif

!
!
!**************************************************************************************************************************
!---->design IIR filter and decimation
!**************************************************************************************************************************

!
!---->butterworth filter properties
      ideal_cutoff    =   rg_iir_filter_cutoff_freq
      transition_band =   rg_iir_filter_transition_band
      freq_pass       =   ideal_cutoff - transition_band/TWO_RXP
      freq_stop       =   ideal_cutoff + transition_band/TWO_RXP
      ap              =   0.01_RXP
      as              = 100.00_RXP

!
!---->decimation factor
      sampling_after_decimation = (2.0_RXP*freq_stop)*2.0_RXP !freq_stop is the Nyquist frequency, so 'sampling_after_decimation' must be at least two times higher.
                                                              !we use 2*'Nyquist frequency' to keep sharp max values

      sampling_simulation       = ONE_RXP/dt

      decimation_factor         = int(sampling_simulation/sampling_after_decimation,kind=IXP)

      if (decimation_factor == ZERO_IXP) decimation_factor = ONE_IXP

!
!---->check that the int function gave the same value among all cpus

      call mpi_allreduce(decimation_factor,idum,ONE_IXP,MPI_INTEGER,MPI_MIN,ig_mpi_comm_simu,ios)

      decimation_factor = idum

!!
!!---->if decimation_factor is too large, then find prime factors of decimation_factor to do the smaller decimation several times

!      call find_nprimes(decimation_factor,nprimes)
!
!      if ( (decimation_factor > 8) .and. (nprimes == 1) ) then
!
!         decimation_factor = decimation_factor - ONE_IXP
!
!         call find_nprimes(decimation_factor,nprimes)
!
!      endif
!
!      ios = init_array_int(primes,nprimes,"primes")
!
!      call find_factors(decimation_factor,primes)

!
!---->compute the number of time step saved after decimation and corresponding size of time step

      ndt_dec = int((ndt-ONE_IXP)/decimation_factor,kind=IXP) + ONE_IXP !must be equal to formula in subroutine decimate

      dt_dec = dt * real(decimation_factor,kind=RXP)

!
!---->design low-pass butterworth filter

      call butterworth_lowpass(butter_coeff,butter_order,butter_gain,butter_func_order,freq_pass,freq_stop,dt,ap,as)

!
!---->compute time and frequency infinite impulse response of butterworth_lowpass by lowpass filtering a dirac

      nfft = nextpow2(ndt)
      ios = init_array_real(dirac       ,nfft,"dirac")
      ios = init_array_complex(dirac_fft,nfft,"dirac_fft")

      dirac(int(ndt/2_IXP,kind=IXP)+ONE_IXP) = ONE_RXP*real(ndt,kind=RXP)

      call tandem(dirac,nfft,butter_coeff,butter_order,butter_gain)

      dirac_fft(:) = dirac(:)

      call ft(dirac_fft,-ONE_IXP)

      dirac_fft(:) = dirac_fft(:)/real(ndt,kind=RXP)

!
!---->write infinite impulse response to file

      if (ig_myrank == ZERO_IXP) then

         open(unit=get_newunit(myunit),file=trim(cg_prefix)//".iir.butter.coef",action='write')
         do i = ONE_IXP,size(butter_coeff)
            write(unit=myunit,fmt='(2(E15.7,1X))') real(i-ONE_IXP,kind=RXP)*dt,butter_coeff(i)
         enddo
         close(myunit)

         open(unit=get_newunit(myunit),file=trim(cg_prefix)//".iir.butter.time",action='write')
         do i = ONE_IXP,nfft
            write(unit=myunit,fmt='(2(E15.7,1X))') real(i-ONE_IXP,kind=RXP)*dt,dirac(i)
         enddo
         close(myunit)

         open(unit=get_newunit(myunit),file=trim(cg_prefix)//".iir.butter.freq",action='write')
         do i = ONE_IXP,nfft/TWO_IXP+ONE_IXP
            write(unit=myunit,fmt='(2(E15.7,1X))') real(i-ONE_IXP,kind=RXP)*(ONE_RXP/(nfft*dt)),abs(dirac_fft(i))
         enddo
         close(myunit)

      endif

      call write_filter_info("iir",ideal_cutoff,transition_band,as,butter_order,decimation_factor,dt_dec,ndt_dec)

!
!
!**************************************************************************************************************************
!---->cpu0 writes time discretization
!**************************************************************************************************************************

      ios = init_array_real(time_dec,ndt_dec,"time_dec")

      do idt = ONE_IXP,ndt_dec

         time_dec(idt) = real(idt-ONE_IXP,kind=RXP)*dt_dec - dt_shift

      enddo

      if (ig_myrank == ZERO_IXP) then

         fname = trim(cg_prefix)//".snapshot.time.dec"

         open(unit=get_newunit(myunit),file=trim(fname),status='replace',action='write',access='stream',form='unformatted',iostat=ios)

         write(unit=myunit) time_dec(:)

         close(myunit) 

      endif

      deallocate(time_dec)

!
!
!*****************************************************************************************************************************
!---->distribute quad elements to filter over all cpus (even over cpus that did not compute free surface quadrangle elements)
!*****************************************************************************************************************************

!
!---->all cpus need to know the total number of quandrangle elements of the free surface

      call mpi_allreduce(ig_nquad_fsurf_saved,nquad_fsurf_all_cpu,ONE_IXP,MPI_INTEGER,MPI_SUM,ig_mpi_comm_simu,ios)

      call get_file_rank_elt_offset(ig_myrank,ig_ncpu,nquad_fsurf_all_cpu,nquad,nquad_floor,iquad_offset)

      write(info,'(a)') "free surface quadrangle in post-proccessing"

      call info_all_cpu(nquad,info)

!
!--->allocate the array that retrieve one quad all gll all components from file

      ios = init_array_real(all_quad_gll_xyz,ndt,nquad,IG_NGLL,IG_NGLL,IG_NDOF,"all_quad_gll_xyz")

!
!--->allocate the array containing raw data of gll ordered by time steps

      ios = init_array_real(xyz_raw,IG_NDOF,ndt,"filter_snapshot_surface:xyz_raw")

!
!--->allocate array containing filtered time histories of all quads all gll all components

      ios = init_array_real(all_quad_gll_xyz_filter,nquad,IG_NGLL,IG_NGLL,IG_NDOF,ndt_dec,"all_quad_gll_xyz_filter")

!
!--->allocate array containing maximum absolute values of filtered time histories of all quads all gll

      ios = init_array_real(all_quad_gll_maxabs,nquad,IG_NGLL,IG_NGLL,NMAXABS,"all_quad_gll_maxabs")

!
!
!**************************************************************************************************************************
!---->create a vector datatype to read all time steps of all gll of all componenents of a quad in one call
!**************************************************************************************************************************

      call MPI_TYPE_VECTOR(ndt,nquad*IG_NGLL*IG_NGLL*IG_NDOF,nquad_fsurf_all_cpu*IG_NGLL*IG_NGLL*IG_NDOF,MPI_REAL,type_vector_quad_level,ios)

      call MPI_TYPE_COMMIT(type_vector_quad_level,ios)

!
!
!**************************************************************************************************************************
!---->open file (see mod_snapshot_surface::init_snapshot_surface_gll)
!**************************************************************************************************************************

      selectcase(trim(cg_iir_filter_output_motion))

         case("dis")
            fname = trim(cg_prefix)//".snapshot.quadf.gll.uxyz"

         case("vel")
            fname = trim(cg_prefix)//".snapshot.quadf.gll.vxyz"

         case("acc")
            fname = trim(cg_prefix)//".snapshot.quadf.gll.axyz"

      endselect

      call efi_mpi_file_open(ig_mpi_comm_simu,fname,MPI_MODE_RDONLY,LG_LUSTRE_FILE_SYS,unit_snap_gll_dis)

!
!---->ensure pos_dis to be coded on 64 bits
      ngll_int64             = int(IG_NGLL            ,kind=I64)
      ndof_int64             = int(IG_NDOF            ,kind=I64)
      mpi_nboctet_real_int64 = int(ig_mpi_nboctet_real,kind=I64)
      iquad_offset_int64     = int(iquad_offset       ,kind=I64)

!
!---->create mpi_file_view to get the full time series (three components) of all gll of free surface quadrangle

      pos_dis = (iquad_offset_int64 - ONE_I64)*ngll_int64*ngll_int64*ndof_int64*mpi_nboctet_real_int64

      call mpi_file_set_view(unit_snap_gll_dis,pos_dis,MPI_REAL,type_vector_quad_level,"native",MPI_INFO_NULL,ios)

!
!---->read all time steps of gll of all quad

      call mpi_file_read_all(unit_snap_gll_dis,all_quad_gll_xyz,ndt*nquad*IG_NGLL*IG_NGLL*IG_NDOF,MPI_REAL,statut,ios)

      do iquad = ONE_IXP,nquad

         do kgll = ONE_IXP,IG_NGLL

            do lgll = ONE_IXP,IG_NGLL
         
!             
!------------->get x,y,z components of gll 'igll'

               do idof = ONE_IXP,IG_NDOF
         
                  do idt = ONE_IXP,ndt
                 
                     xyz_raw(idt,idof) = all_quad_gll_xyz(idof,lgll,kgll,iquad,idt)
                  
                  enddo
                  
               enddo
         
!             
!------------->filter and decimate
         
               ndt_tmp = ndt
         
               ios = init_array_real(x_filter,ndt_tmp,"filter_snapshot_surface:init:x_filter")
               ios = init_array_real(y_filter,ndt_tmp,"filter_snapshot_surface:init:y_filter")
               ios = init_array_real(z_filter,ndt_tmp,"filter_snapshot_surface:init:z_filter")

               x_filter(:) = xyz_raw(:,ONE_IXP)
               y_filter(:) = xyz_raw(:,TWO_IXP)
               z_filter(:) = xyz_raw(:,THREE_IXP)
         
              !do idec = ONE_IXP,nprimes
         
                  call tandem(x_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
                  call tandem(y_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
                  call tandem(z_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
         
!        
!---------------->collect peak ground motion before decimation
                  all_quad_gll_maxabs(ONE_IXP  ,lgll,kgll,iquad) = maxval(abs(x_filter(:)))
                  all_quad_gll_maxabs(TWO_IXP  ,lgll,kgll,iquad) = maxval(abs(y_filter(:)))
                  all_quad_gll_maxabs(THREE_IXP,lgll,kgll,iquad) = maxval(abs(z_filter(:)))

                  module_xy = ZERO_RXP
         
                  do idt = ONE_IXP,ndt_tmp
         
                     module_xy  = max(module_xy,sqrt(x_filter(idt)**TWO_IXP + y_filter(idt)**TWO_IXP))
         
                  enddo
         
                  module_xyz = ZERO_RXP

                  do idt = ONE_IXP,ndt_tmp
         
                     module_xyz = max(module_xyz,sqrt(x_filter(idt)**TWO_IXP + y_filter(idt)**TWO_IXP + z_filter(idt)**TWO_IXP))
         
                  enddo
         
                  all_quad_gll_maxabs(FOUR_IXP,lgll,kgll,iquad) = module_xy
                  all_quad_gll_maxabs(FIVE_IXP,lgll,kgll,iquad) = module_xyz
         
                 !call decimate(x_filter,ndt_tmp,primes(idec))
                 !call decimate(y_filter,ndt_tmp,primes(idec))
                 !call decimate(z_filter,ndt_tmp,primes(idec))
         
                  call decimate(x_filter,ndt_tmp,decimation_factor)
                  call decimate(y_filter,ndt_tmp,decimation_factor)
                  call decimate(z_filter,ndt_tmp,decimation_factor)
         
                ! ndt_tmp = int((ndt_tmp-1)/primes(idec),kind=IXP) + 1
         
              !enddo
         
!        
!------------->save decimated time history to memory
         
               do idt = ONE_IXP,size(x_filter)
         
                  all_quad_gll_xyz_filter(idt,ONE_IXP,lgll,kgll,iquad) = x_filter(idt)
                                               
               enddo                           
                                               
               do idt = ONE_IXP,size(y_filter)       
                                               
                  all_quad_gll_xyz_filter(idt,TWO_IXP,lgll,kgll,iquad) = y_filter(idt)
                                               
               enddo                           
                                               
               do idt = ONE_IXP,size(z_filter)       
                                               
                  all_quad_gll_xyz_filter(idt,THREE_IXP,lgll,kgll,iquad) = z_filter(idt)
         
               enddo
         
               deallocate(x_filter)
               deallocate(y_filter)
               deallocate(z_filter)
         
            enddo
         enddo

      enddo

!
!---->close file

      call mpi_file_close(unit_snap_gll_dis,ios)

      deallocate(all_quad_gll_xyz)

      deallocate(xyz_raw)

      call mpi_type_free(type_vector_quad_level,ios)

!
!
!**************************************************************************************************************************
!---->write decimated displacement time histories to disk
!**************************************************************************************************************************
      fname_dec = trim(fname)//".dec"

      call efi_mpi_file_write_at_all(ig_mpi_comm_simu,all_quad_gll_xyz_filter,fname_dec,ig_myrank,iquad_offset,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS)

      deallocate(all_quad_gll_xyz_filter)

!
!
!**************************************************************************************************************************
!---->write maximum values of time histories to disk
!**************************************************************************************************************************
      fname_max = trim(fname)//".max"

      call efi_mpi_file_write_at_all(ig_mpi_comm_simu,all_quad_gll_maxabs,fname_max,ig_myrank,iquad_offset,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS)

      deallocate(all_quad_gll_maxabs)

!
!
!**************************************************************************************************************************
!---->SPATIAL DERIVATIVE
!**************************************************************************************************************************

      if (LG_SNAPSHOT_SURF_SPA_DER) then
         
         fname_sd = trim(fname)//".sd"

         call efi_mpi_file_open(ig_mpi_comm_simu,fname_sd,MPI_MODE_RDONLY,LG_LUSTRE_FILE_SYS,unit_snap_quadf_dis_spa_der)

!
!------->allocate the array that retrieve one quad all gll all components from file

         ios = init_array_real(all_quad_spa_der,ndt,nquad,IG_NDOF*IG_NDOF,"all_quad_spa_der")

!
!------>allocate the array containing raw data of gll ordered by time steps

         ios = init_array_real(xyz_raw,IG_NDOF*IG_NDOF,ndt,"filter_snapshot_surface:xyz_raw")

!     
!------->allocate array containing filtered time histories of all quads all gll all components

         ios = init_array_real(all_quad_spa_der_filter,nquad,IG_NDOF*IG_NDOF,ndt_dec,"all_quad_spa_der_filter")
      
         call MPI_TYPE_VECTOR(ndt,nquad*IG_NDOF*IG_NDOF,nquad_fsurf_all_cpu*IG_NDOF*IG_NDOF,MPI_REAL,type_vector_quad_level,ios)
 
         call MPI_TYPE_COMMIT(type_vector_quad_level,ios)

!
!
!*****************************************************************************************************************************
!------->loop over all free surface quadrangle elements of cpu 'myrank' using mpi collective read
!        the collective read must be done the same number of times (i.e., 'nquad_floor' times) by all cpus.
!*****************************************************************************************************************************

!   
!------->create mpi_file_view to get the full time series (three components) of all gll of free surface quadrangle
     
         pos_dis = (iquad_offset_int64 - ONE_I64)*ndof_int64*ndof_int64*mpi_nboctet_real_int64
     
         call mpi_file_set_view(unit_snap_quadf_dis_spa_der,pos_dis,MPI_REAL,type_vector_quad_level,"native",MPI_INFO_NULL,ios)
     
!   
!------->read all time steps of gll of element iquad
     
         call mpi_file_read_all(unit_snap_quadf_dis_spa_der,all_quad_spa_der,ndt*nquad*IG_NDOF*IG_NDOF,MPI_REAL,statut,ios)
     
         do iquad = ONE_IXP,nquad
     
!          
!---------->get duxdx, duxdy, etc.

            do idof = ONE_IXP,IG_NDOF*IG_NDOF
      
               do idt = ONE_IXP,ndt
              
                  xyz_raw(idt,idof) = all_quad_spa_der(idof,iquad,idt)
               
               enddo
               
            enddo

!          
!---------->filter and decimate
     
            ndt_tmp = ndt
     
            ios = init_array_real(duxdx_filter,ndt_tmp,"filter_snapshot_surface:init:duxdx_filter")
            ios = init_array_real(duxdy_filter,ndt_tmp,"filter_snapshot_surface:init:duxdy_filter")
            ios = init_array_real(duxdz_filter,ndt_tmp,"filter_snapshot_surface:init:duxdz_filter")
            ios = init_array_real(duydx_filter,ndt_tmp,"filter_snapshot_surface:init:duydx_filter")
            ios = init_array_real(duydy_filter,ndt_tmp,"filter_snapshot_surface:init:duydy_filter")
            ios = init_array_real(duydz_filter,ndt_tmp,"filter_snapshot_surface:init:duydz_filter")
            ios = init_array_real(duzdx_filter,ndt_tmp,"filter_snapshot_surface:init:duzdx_filter")
            ios = init_array_real(duzdy_filter,ndt_tmp,"filter_snapshot_surface:init:duzdy_filter")
            ios = init_array_real(duzdz_filter,ndt_tmp,"filter_snapshot_surface:init:duzdz_filter")
     
            duxdx_filter(:) = xyz_raw(:,1_IXP)
            duxdy_filter(:) = xyz_raw(:,2_IXP)
            duxdz_filter(:) = xyz_raw(:,3_IXP)
            duydx_filter(:) = xyz_raw(:,4_IXP)
            duydy_filter(:) = xyz_raw(:,5_IXP)
            duydz_filter(:) = xyz_raw(:,6_IXP)
            duzdx_filter(:) = xyz_raw(:,7_IXP)
            duzdy_filter(:) = xyz_raw(:,8_IXP)
            duzdz_filter(:) = xyz_raw(:,9_IXP)
     
           !do idec = ONE_IXP,nprimes
     
               call tandem(duxdx_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
               call tandem(duxdy_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
               call tandem(duxdz_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
               call tandem(duydx_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
               call tandem(duydy_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
               call tandem(duydz_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
               call tandem(duzdx_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
               call tandem(duzdy_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
               call tandem(duzdz_filter,ndt_tmp,butter_coeff,butter_order,butter_gain)
     
              !call decimate(x_filter,ndt_tmp,primes(idec))
     
               call decimate(duxdx_filter,ndt_tmp,decimation_factor)
               call decimate(duxdy_filter,ndt_tmp,decimation_factor)
               call decimate(duxdz_filter,ndt_tmp,decimation_factor)
               call decimate(duydx_filter,ndt_tmp,decimation_factor)
               call decimate(duydy_filter,ndt_tmp,decimation_factor)
               call decimate(duydz_filter,ndt_tmp,decimation_factor)
               call decimate(duzdx_filter,ndt_tmp,decimation_factor)
               call decimate(duzdy_filter,ndt_tmp,decimation_factor)
               call decimate(duzdz_filter,ndt_tmp,decimation_factor)
     
             ! ndt_tmp = int((ndt_tmp-1)/primes(idec),kind=IXP) + 1
     
           !enddo
     
!   
!---------->save decimated time history to memory
     
            do idt = ONE_IXP,size(duxdx_filter)
     
               all_quad_spa_der_filter(idt,1_IXP,iquad) = duxdx_filter(idt)
                                            
            enddo

            do idt = ONE_IXP,size(duxdy_filter)
     
               all_quad_spa_der_filter(idt,2_IXP,iquad) = duxdy_filter(idt)
                                            
            enddo

            do idt = ONE_IXP,size(duxdz_filter)
     
               all_quad_spa_der_filter(idt,3_IXP,iquad) = duxdz_filter(idt)
                                            
            enddo

            do idt = ONE_IXP,size(duydx_filter)
     
               all_quad_spa_der_filter(idt,4_IXP,iquad) = duydx_filter(idt)
                                            
            enddo

            do idt = ONE_IXP,size(duydy_filter)
     
               all_quad_spa_der_filter(idt,5_IXP,iquad) = duydy_filter(idt)
                                            
            enddo

            do idt = ONE_IXP,size(duydz_filter)
     
               all_quad_spa_der_filter(idt,6_IXP,iquad) = duydz_filter(idt)
                                            
            enddo

            do idt = ONE_IXP,size(duzdx_filter)
     
               all_quad_spa_der_filter(idt,7_IXP,iquad) = duzdx_filter(idt)
                                            
            enddo

            do idt = ONE_IXP,size(duzdy_filter)
     
               all_quad_spa_der_filter(idt,8_IXP,iquad) = duzdy_filter(idt)
                                            
            enddo

            do idt = ONE_IXP,size(duzdz_filter)
     
               all_quad_spa_der_filter(idt,9_IXP,iquad) = duzdz_filter(idt)
                                            
            enddo

            deallocate(duxdx_filter)
            deallocate(duxdy_filter)
            deallocate(duxdz_filter)
            deallocate(duydx_filter)
            deallocate(duydy_filter)
            deallocate(duydz_filter)
            deallocate(duzdx_filter)
            deallocate(duzdy_filter)
            deallocate(duzdz_filter)
     
         enddo

         call mpi_file_close(unit_snap_quadf_dis_spa_der,ios)

         call mpi_type_free(type_vector_quad_level,ios)

!
!
!**************************************************************************************************************************
!------->write decimated divergence time hisotries to disk
!**************************************************************************************************************************
         fname_sd = trim(fname_sd)//".dec"
       
         call efi_mpi_file_write_at_all(ig_mpi_comm_simu,all_quad_spa_der_filter,fname_sd,ig_myrank,iquad_offset,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS)
       
         deallocate(xyz_raw)

         deallocate(all_quad_spa_der_filter)

         deallocate(all_quad_spa_der)

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine filter_snapshot_surface_gll
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute butterworth lowpass filter coefficient (by m. saito butsuritanko vol31 no.4 pp.112)
!>@param  fp  : pass band frequency (in Hz and then converted to non-dimensional by the subroutine)
!>@param  fs  : stop band frequency (in Hz and then converted to non-dimensional by the subroutine)
!>@param  dt  : time step
!>@param  ap  : pass part amplitude controller (amp=1/1+ap**2)
!>@param  as  : cut part amplitude controller (amp=1/1+as**2)
!>@return h   : filter coefficients
!>@return m   : order of filter (m=(n+1)/2)
!>@return gn  : gain factor
!>@return n   : order of butterworth function
!***********************************************************************************************************************************************************************************
   subroutine butterworth_lowpass(h,m,gn,n,fp,fs,dt,ap,as)
!***********************************************************************************************************************************************************************************

      use mod_precision

      use mod_global_variables, only : TINY_REAL_R64

      use mod_init_memory, only : init_array_real_dp

      implicit none

      real   (kind=RXP), intent(in )                            :: as
      real   (kind=RXP), intent(in )                            :: ap
      real   (kind=RXP), intent(in )                            :: fs
      real   (kind=RXP), intent(in )                            :: fp
      real   (kind=RXP), intent(in )                            :: dt
      real   (kind=R64), intent(out)                            :: gn
      real   (kind=R64), intent(out), allocatable, dimension(:) :: h
      integer(kind=IXP), intent(out)                            :: m
      integer(kind=IXP), intent(out)                            :: n

      real   (kind=R64), parameter                              :: PI  = acos(-1.0_R64)
      real   (kind=R64), parameter                              :: HP  = acos(-1.0_R64)/2.0_R64
      real   (kind=R64), parameter                              :: PI2 = acos(-1.0_R64)*2.0_R64

      real   (kind=R64)                                         :: dt_64
      real   (kind=R64)                                         :: as_64
      real   (kind=R64)                                         :: ap_64
      real   (kind=R64)                                         :: fs_64
      real   (kind=R64)                                         :: fp_64
      real   (kind=R64)                                         :: tp
      real   (kind=R64)                                         :: ts
      real   (kind=R64)                                         :: ws
      real   (kind=R64)                                         :: wp
      real   (kind=R64)                                         :: pa
      real   (kind=R64)                                         :: sa
      real   (kind=R64)                                         :: c
      real   (kind=R64)                                         :: c2
      real   (kind=R64)                                         :: cc
      real   (kind=R64)                                         :: dp
      real   (kind=R64)                                         :: g
      real   (kind=R64)                                         :: fj
      real   (kind=R64)                                         :: sj
      real   (kind=R64)                                         :: tj
      real   (kind=R64)                                         :: a

      integer(kind=IXP)                                         :: j
      integer(kind=IXP)                                         :: k
      integer(kind=IXP)                                         :: nh
      integer(kind=IXP)                                         :: ios

!
!---->precision conversion

      dt_64 = real(dt,kind=R64)
      fp_64 = real(fp,kind=R64)
      fs_64 = real(fs,kind=R64)
      ap_64 = real(ap,kind=R64)
      as_64 = real(as,kind=R64)

!
!---->convert frequency (in Hz) to non-dimensional frequency

      fp_64 = fp_64*dt_64
      fs_64 = fs_64*dt_64

!
!---->in case fp and fs would be inverted

      wp = min(abs(fp_64),abs(fs_64))*PI
      ws = max(abs(fp_64),abs(fs_64))*PI

      tp = tan(wp)
      ts = tan(ws)

!
!---->in case ap and as would be inverted

      pa = min(abs(ap_64),abs(as_64))
      sa = max(abs(ap_64),abs(as_64))

      if( pa < TINY_REAL_R64 ) pa = 0.5_R64
      if( sa < TINY_REAL_R64 ) sa = 5.0_R64

!
!---->determine n & c

      n  = max(TWO_IXP,int(abs(log(pa/sa)/log(tp/ts))+0.5_R64,kind=IXP))

      cc = exp(log(pa*sa)/real(n,kind=R64))/(tp*ts)
      c  = sqrt(cc)

      dp = HP/real(n,kind=R64)
      m  = n/TWO_IXP
      k  = m*FOUR_IXP
      g  = ONE_R64
      fj = ONE_R64
      c2 = TWO_R64*(ONE_R64 - c)*(ONE_R64 + c)

!
!---->allocate array h. its size depends on the parity of n

      if (mod(n,TWO_IXP) == ZERO_IXP) then

         nh  = k

      else

         nh  = k + FOUR_IXP

      endif

      ios = init_array_real_dp(h,nh,"mod_filter:butterworth_highpass:h")

!
!---->compute h

      do j = ONE_IXP,k,FOUR_IXP

         sj             = cos(dp*fj)**TWO_I64
         tj             = sin(dp*fj)
         fj             = fj + TWO_R64
         a              = ONE_R64/( (c + tj)**TWO_I64 + sj )
         g              = g*a
         h(j          ) = TWO_R64
         h(j+ONE_IXP  ) = ONE_R64
         h(j+TWO_IXP  ) = c2*a
         h(j+THREE_IXP) = ( (c-tj)**TWO_I64 + sj ) * a

      enddo

!
!---->for even n

      gn = g
      if(mod(n,TWO_IXP) == ZERO_IXP) return

!
!---->for odd n

      m              = m + ONE_IXP
      gn             = gn/(ONE_R64 + c)
      h(k+ONE_IXP  ) = ONE_R64
      h(k+TWO_IXP  ) = ZERO_R64
      h(k+THREE_IXP) = (ONE_R64 - c)/(ONE_R64 + c)
      h(k+FOUR_IXP ) = ZERO_R64

      return

!***********************************************************************************************************************************************************************************
   end subroutine butterworth_lowpass
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute butterworth highpass filter coefficient (by m. saito butsuritanko vol31 no.4 pp.112)
!>@param  fp  : pass band frequency (in Hz and then converted to non-dimensional by the subroutine)
!>@param  fs  : stop band frequency (in Hz and then converted to non-dimensional by the subroutine)
!>@param  dt  : time step
!>@param  ap  : pass part amplitude controller (amp=1/1+ap**2)
!>@param  as  : cut part amplitude controller (amp=1/1+as**2)
!>@return h   : filter coefficients
!>@return m   : order of filter (m=(n+1)/2)
!>@return gn  : gain factor
!>@return n   : order of butterworth function
!***********************************************************************************************************************************************************************************
   subroutine butterworth_highpass(h,m,gn,n,fp,fs,dt,ap,as)
!***********************************************************************************************************************************************************************************

      use mod_precision

      use mod_init_memory, only : init_array_real_dp

      implicit none

      real   (kind=RXP), intent(in )                            :: as
      real   (kind=RXP), intent(in )                            :: ap
      real   (kind=RXP), intent(in )                            :: fs
      real   (kind=RXP), intent(in )                            :: fp
      real   (kind=RXP), intent(in )                            :: dt
      real   (kind=R64), intent(out)                            :: gn
      real   (kind=R64), intent(out), allocatable, dimension(:) :: h
      integer(kind=IXP), intent(out)                            :: m
      integer(kind=IXP), intent(out)                            :: n

      real   (kind=R64), parameter                              :: PI  = acos(-1.0_R64)
      real   (kind=R64), parameter                              :: HP  = acos(-1.0_R64)/2.0_R64
      real   (kind=R64), parameter                              :: PI2 = acos(-1.0_R64)*2.0_R64

      real   (kind=R64)                                         :: dt_64
      real   (kind=R64)                                         :: as_64
      real   (kind=R64)                                         :: ap_64
      real   (kind=R64)                                         :: fs_64
      real   (kind=R64)                                         :: fp_64
      real   (kind=R64)                                         :: tp
      real   (kind=R64)                                         :: ts
      real   (kind=R64)                                         :: ws
      real   (kind=R64)                                         :: wp
      real   (kind=R64)                                         :: pa
      real   (kind=R64)                                         :: sa
      real   (kind=R64)                                         :: c
      real   (kind=R64)                                         :: c2
      real   (kind=R64)                                         :: cc
      real   (kind=R64)                                         :: dp
      real   (kind=R64)                                         :: g
      real   (kind=R64)                                         :: fj
      real   (kind=R64)                                         :: sj
      real   (kind=R64)                                         :: tj
      real   (kind=R64)                                         :: a

      integer(kind=IXP)                                         :: j
      integer(kind=IXP)                                         :: k
      integer(kind=IXP)                                         :: nh
      integer(kind=IXP)                                         :: ios

!
!---->precision conversion

      dt_64 = real(dt,kind=R64)
      fp_64 = real(fp,kind=R64)
      fs_64 = real(fs,kind=R64)
      ap_64 = real(ap,kind=R64)
      as_64 = real(as,kind=R64)

!
!---->convert frequency (in Hz) to non-dimensional frequency

      fp_64 = fp_64*dt_64
      fs_64 = fs_64*dt_64

!
!---->in case fp and fs would be inverted

      wp = min(abs(fp_64),abs(fs_64))*PI
      ws = max(abs(fp_64),abs(fs_64))*PI

      tp = tan(wp)
      ts = tan(ws)

!
!---->in case ap and as would be inverted

      pa = min(abs(ap_64),abs(as_64))
      sa = max(abs(ap_64),abs(as_64))

      if( pa < TINY_REAL_R64 ) pa = 0.5_R64
      if( sa < TINY_REAL_R64 ) sa = 5.0_R64

!
!---->determine n & c

      n  = max(TWO_IXP,int(abs(log(sa/pa)/log(tp/ts))+0.5_R64,kind=IXP))

      cc = exp(log(pa*sa)/real(n,kind=R64))*tp*ts
      c  = sqrt(cc)

      dp = HP/real(n,kind=R64)
      m  = n/TWO_IXP
      k  = m*FOUR_IXP
      g  = ONE_R64
      fj = ONE_R64
      c2 = -TWO_R64*(ONE_R64 - c)*(ONE_R64 + c)

!
!---->allocate array h. its size depends on the parity of n

      if (mod(n,TWO_IXP) == ZERO_IXP) then

         nh  = k

      else

         nh  = k + FOUR_IXP

      endif

      ios = init_array_real_dp(h,nh,"mod_filter:butterworth_highpass:h")

!
!---->compute h

      do j = ONE_IXP,k,FOUR_IXP

         sj             = cos(dp*fj)**TWO_I64
         tj             = sin(dp*fj)
         fj             = fj + TWO_R64
         a              = ONE_R64/( (c + tj)**TWO_I64 + sj )
         g              = g*a
         h(j          ) = -TWO_R64
         h(j+ONE_IXP  ) =  ONE_R64
         h(j+TWO_IXP  ) = c2*a
         h(j+THREE_IXP) = ( (c-tj)**TWO_I64 + sj ) * a

      enddo

!
!---->for even n

      gn = g
      if(mod(n,TWO_IXP) == ZERO_IXP) return

!
!---->for odd n

      m              = m + ONE_IXP
      gn             = gn/(c + ONE_R64)
      h(k+ONE_IXP  ) = -ONE_R64
      h(k+TWO_IXP  ) = ZERO_R64
      h(k+THREE_IXP) = (c - ONE_R64)/(c + ONE_R64)
      h(k+FOUR_IXP ) = ZERO_R64

      return

!***********************************************************************************************************************************************************************************
   end subroutine butterworth_highpass
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute butterworth bandpass filter coefficient (by m. saito butsuritanko vol31 no.4 pp.112)
!>@param  fl  : low  frequency cut-off (in Hz and then converted to non-dimensional by the subroutine)
!>@param  fh  : high frequency cut-off (in Hz and then converted to non-dimensional by the subroutine)
!>@param  fs  : stop band frequency (in Hz and then converted to non-dimensional by the subroutine)
!>@param  dt  : time step
!>@param  ap  : pass part amplitude controller (amp=1/1+ap**2)
!>@param  as  : cut part amplitude controller (amp=1/1+as**2)
!>@return h   : filter coefficients
!>@return m   : order of filter (m=(n+1)/2)
!>@return gn  : gain factor
!>@return n   : order of butterworth function
!***********************************************************************************************************************************************************************************
   subroutine butterworth_bandpass(h,m,gn,n,fl,fh,fs,dt,ap,as)
!***********************************************************************************************************************************************************************************

      use mod_precision

      implicit none

      real   (kind=RXP), intent(in )                            :: as
      real   (kind=RXP), intent(in )                            :: ap
      real   (kind=RXP), intent(in )                            :: fl
      real   (kind=RXP), intent(in )                            :: fh
      real   (kind=RXP), intent(in )                            :: fs
      real   (kind=RXP), intent(in )                            :: dt
      real   (kind=R64), intent(out)                            :: gn
      real   (kind=R64), intent(out), allocatable, dimension(:) :: h
      integer(kind=IXP), intent(out)                            :: m
      integer(kind=IXP), intent(out)                            :: n

      real   (kind=R64), parameter                              :: PI  = acos(-1.0_R64)
      real   (kind=R64), parameter                              :: HP  = acos(-1.0_R64)/2.0_R64

      complex(kind=R64), dimension(2)                           :: r
      complex(kind=R64)                                         :: oj
      complex(kind=R64)                                         :: cq

      real   (kind=R64)                                         :: dt_64
      real   (kind=R64)                                         :: as_64
      real   (kind=R64)                                         :: ap_64
      real   (kind=R64)                                         :: fl_64
      real   (kind=R64)                                         :: fh_64
      real   (kind=R64)                                         :: fs_64
      real   (kind=R64)                                         :: ts
      real   (kind=R64)                                         :: os
      real   (kind=R64)                                         :: op
      real   (kind=R64)                                         :: wl
      real   (kind=R64)                                         :: wh
      real   (kind=R64)                                         :: ws
      real   (kind=R64)                                         :: ww
      real   (kind=R64)                                         :: wpc
      real   (kind=R64)                                         :: wmc
      real   (kind=R64)                                         :: pa
      real   (kind=R64)                                         :: sa
      real   (kind=R64)                                         :: c
      real   (kind=R64)                                         :: cc
      real   (kind=R64)                                         :: dp
      real   (kind=R64)                                         :: g
      real   (kind=R64)                                         :: fj
      real   (kind=R64)                                         :: a
      real   (kind=R64)                                         :: clh
      real   (kind=R64)                                         :: re
      real   (kind=R64)                                         :: ri

      integer(kind=IXP)                                         :: i
      integer(kind=IXP)                                         :: j
      integer(kind=IXP)                                         :: k
      integer(kind=IXP)                                         :: l
      integer(kind=IXP)                                         :: nh
      integer(kind=IXP)                                         :: ios

!
!---->precision conversion

      dt_64 = real(dt,kind=R64)
      fl_64 = real(fl,kind=R64)
      fh_64 = real(fh,kind=R64)
      fs_64 = real(fs,kind=R64)
      ap_64 = real(ap,kind=R64)
      as_64 = real(as,kind=R64)

!
!---->convert frequency (in Hz) to non-dimensional frequency

      fl_64 = fl_64*dt_64
      fh_64 = fh_64*dt_64
      fs_64 = fs_64*dt_64

!
!---->in case fp and fs would be inverted

      wl = min(abs(fl_64),abs(fh_64))*PI
      wh = max(abs(fl_64),abs(fh_64))*PI
      ws = abs(fs_64)*PI

      clh = 1.0_R64/cos(wl)*cos(wh)
      op  = sin(wh-wl)*clh
      ww  = tan(wl)*tan(wh)
      ts  = tan(ws)
      os  = abs(ts-ww/ts)
      pa  = min(abs(ap),abs(as))
      sa  = max(abs(ap),abs(as))

      if( pa < TINY_REAL_R64 ) pa = 0.5_R64
      if( sa < TINY_REAL_R64 ) sa = 5.0_R64
                 
      n   = max(2_IXP,int(abs(log(pa/sa)/log(op/os))+0.5_R64,kind=IXP))
      cc  = exp(log(pa*sa)/real(n,kind=R64))/(op*os) 
      c   = sqrt(cc)

      ww  = ww*cc      
      dp  = HP/real(n,kind=R64)

      k   = n/2_IXP
      m   = k*2_IXP    
      l   = 0_IXP 

      g   = 1.0_R64
      fj  = 1.0_R64

!
!---->allocate array h. its size depends on the parity of n

      if (mod(n,TWO_IXP) == ZERO_IXP) then

         nh  = k*4_IXP*2_IXP

      else

         nh  = k*4_IXP*2_IXP + FOUR_IXP

      endif

      if (allocated(h)) deallocate(h)

      allocate(h(nh),stat=ios)

      h(:) = ZERO_R64

!
!---->init h

      do j = 1_IXP,k

        oj   = cmplx(cos(dp*fj),sin(dp*fj),kind=R64)*0.5_R64
        fj   = fj+2.0_R64
        cq   = sqrt(oj**2_IXP + ww) 
        r(1) = oj+cq
        r(2) = oj-cq
        g    = g*cc
                               
        do i=1_IXP,2_IXP

          re     = real(r(i))**2_IXP
          ri     = aimag(r(i))

          a      = 1.0_R64/((c+ri)**2_IXP+re)
          g      = g*a

          h(l+1) = 0.0_R64 
          h(l+2) =-1.0_R64
          h(l+3) = 2.0_R64*((ri-c)*(ri+c)+re)*a     
          h(l+4) = ((ri-c)**2_IXP+re)*a

          l      = l+4_IXP

         enddo

      enddo

      gn = g

!
!---->even n

      if(mod(n,TWO_IXP) == ZERO_IXP) return 

!
!---->odd n
 
      m      = m + 1_IXP
      wpc    = cc*cos(wh-wl)*clh
      wmc    =-cc*cos(wh+wl)*clh

      a      = 1.0_R64/(wpc+c)
      gn     = g*c*a

      h(l+1) = 0.0_R64
      h(l+2) =-1.0_R64
      h(l+3) = 2.0_R64*wmc*a
      h(l+4) = (wpc-c)*a

      return

!***********************************************************************************************************************************************************************************
   end subroutine butterworth_bandpass
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute recursive filtering : f(z) = (1+a*z+aa*z**2)/(1+b*z+bb*z**2) (saito butsuritanko 1978 august vol.31 no.4 pp.115)
!>@param  x   : input time series
!>@param  n   : length of x & y
!>@param  h   : filter coefficients : h(ONE_IXP)=a,h(TWO_IXP)=aa,h(THREE_IXP)=b,h(FOUR_IXP)=bb
!>@param  nr  : >0 for normal direction filtering, <0 for reverse direction filtering
!>@return y   : output time series
!***********************************************************************************************************************************************************************************
   subroutine recursive_filtering(x,y,n,a,aa,b,bb,nr)
!***********************************************************************************************************************************************************************************

      use mod_precision

      implicit none

      integer(kind=IXP), intent( in)               :: n
      integer(kind=IXP), intent( in)               :: nr
      real   (kind=R64), intent( in)               :: a
      real   (kind=R64), intent( in)               :: aa
      real   (kind=R64), intent( in)               :: b
      real   (kind=R64), intent( in)               :: bb
      real   (kind=R64), intent( in), dimension(n) :: x
      real   (kind=R64), intent(out), dimension(n) :: y

      real   (kind=R64)                            :: u1
      real   (kind=R64)                            :: v1
      real   (kind=R64)                            :: u2
      real   (kind=R64)                            :: v2
      real   (kind=R64)                            :: u3
      real   (kind=R64)                            :: v3

      integer(kind=IXP)                            :: i
      integer(kind=IXP)                            :: j
      integer(kind=IXP)                            :: jd

      if (nr >= ZERO_IXP) then

         j  = ONE_IXP
         jd = ONE_IXP

      else

         j  =  n
         jd = -ONE_IXP

      endif

      u1 = ZERO_R64
      u2 = ZERO_R64
      v1 = ZERO_R64
      v2 = ZERO_R64

!
!---->filtering
      do i = ONE_IXP,n

        u3   = u2
        u2   = u1
        u1   = x(j)
        v3   = v2
        v2   = v1
        v1   = u1 + a*u2 + aa*u3 - b*v2 - bb*v3
        y(j) = v1
        j    = j + jd

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine recursive_filtering
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute recursive filtering in series (saito butsuritanko 1978 august vol.31 no.4 pp.115)
!>@param  x   : input time series
!>@param  n   : length of x
!>@param  m   : order of filter
!>@param  h   : filter coefficients : h(ONE_IXP)=a,h(TWO_IXP)=aa,h(THREE_IXP)=b,h(FOUR_IXP)=bb
!>@return x   : output time series
!***********************************************************************************************************************************************************************************
   subroutine tandem(x,n,h,m,g)
!***********************************************************************************************************************************************************************************

      use mod_precision

      implicit none

      integer(kind=IXP), intent(in)                  :: n
      integer(kind=IXP), intent(in)                  :: m
      real   (kind=R64), intent(in)                  :: g
      real   (kind=RXP), intent(inout), dimension(n) :: x
      real   (kind=R64), intent(in)   , dimension(:) :: h

      real   (kind=R64), dimension(n)                :: x_64
      real   (kind=R64), dimension(n)                :: y_64
      integer(kind=IXP)                              :: i
      integer(kind=IXP)                              :: nr

!
!
!***********************************************************************************************
!---->precision conversion to avoid NaN during recursive_filtering subroutine
!***********************************************************************************************
      x_64(:) = real(x(:),kind=R64)

!
!
!***********************************************************************************************
!---->forward filtering
!***********************************************************************************************

      nr = +ONE_IXP

!
!---->1-st call
      call recursive_filtering(x_64,y_64,n,h(ONE_IXP),h(TWO_IXP),h(THREE_IXP),h(FOUR_IXP),nr)

      if(m <= ONE_IXP) return

!---->2-nd and other calls
      do i = TWO_IXP,m
 
        call recursive_filtering(y_64,y_64,n,h(i*FOUR_IXP-THREE_IXP),h(i*FOUR_IXP-TWO_IXP),h(i*FOUR_IXP-ONE_IXP),h(i*FOUR_IXP),nr)

      enddo

!---->apply gain
      do i = ONE_IXP,n

         y_64(i) = y_64(i)*g

      enddo

!
!
!***********************************************************************************************
!---->reverse filtering
!***********************************************************************************************

      nr = -ONE_IXP

!
!---->1-st call
      call recursive_filtering(y_64,x_64,n,h(ONE_IXP),h(TWO_IXP),h(THREE_IXP),h(FOUR_IXP),nr)

      if(m <= ONE_IXP) return

!---->2-nd and other calls
      do i = TWO_IXP,m

        call recursive_filtering(x_64,x_64,n,h(i*FOUR_IXP-THREE_IXP),h(i*FOUR_IXP-TWO_IXP),h(i*FOUR_IXP-ONE_IXP),h(i*FOUR_IXP),nr)

      enddo

!---->apply gain
      do i = ONE_IXP,n

         x_64(i) = x_64(i)*g

      enddo

!
!---->precision conversion
      x(:) = real(x_64(:),kind=R32)

      return

!***********************************************************************************************************************************************************************************
   end subroutine tandem
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute recursive filtering in series (saito butsuritanko 1978 august vol.31 no.4 pp.115)
!>@param  x   : input time series
!>@param  n   : length of x
!>@param  m   : order of filter
!>@param  h   : filter coefficients : h(ONE_IXP)=a,h(TWO_IXP)=aa,h(THREE_IXP)=b,h(FOUR_IXP)=bb
!>@return x   : output time series
!***********************************************************************************************************************************************************************************
   subroutine tandem_one_pass(x,n,h,m,g)
!***********************************************************************************************************************************************************************************

      use mod_precision

      implicit none

      integer(kind=IXP), intent(in)                  :: n
      integer(kind=IXP), intent(in)                  :: m
      real   (kind=R64), intent(in)                  :: g
      real   (kind=RXP), intent(inout), dimension(n) :: x
      real   (kind=R64), intent(in)   , dimension(:) :: h

      real   (kind=R64), dimension(n)                :: x_64
      real   (kind=R64), dimension(n)                :: y_64
      integer(kind=IXP)                              :: i
      integer(kind=IXP)                              :: nr

!
!
!***********************************************************************************************
!---->precision conversion to avoid NaN during recursive_filtering subroutine
!***********************************************************************************************
      x_64(:) = real(x(:),kind=R64)

!
!
!***********************************************************************************************
!---->forward filtering
!***********************************************************************************************

      nr = +ONE_IXP

!
!---->1-st call
      call recursive_filtering(x_64,y_64,n,h(ONE_IXP),h(TWO_IXP),h(THREE_IXP),h(FOUR_IXP),nr)

      if(m <= ONE_IXP) return

!---->2-nd and other calls
      do i = TWO_IXP,m
 
        call recursive_filtering(y_64,y_64,n,h(i*FOUR_IXP-THREE_IXP),h(i*FOUR_IXP-TWO_IXP),h(i*FOUR_IXP-ONE_IXP),h(i*FOUR_IXP),nr)

      enddo

!---->apply gain
      do i = ONE_IXP,n

         y_64(i) = y_64(i)*g

      enddo

!
!---->precision conversion
      x(:) = real(y_64(:),kind=R32)

      return

!***********************************************************************************************************************************************************************************
   end subroutine tandem_one_pass
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to decimate time series
!>@param  x : input time series
!>@param  n : length of x
!>@param  f : factor of decimation
!>@return x : output time series
!***********************************************************************************************************************************************************************************
   subroutine decimate(x,n,f)
!***********************************************************************************************************************************************************************************

      use mod_init_memory, only : init_array_real

      implicit none

      integer(kind=IXP), intent(in)                               :: n
      integer(kind=IXP), intent(in)                               :: f
      real   (kind=RXP), intent(inout), allocatable, dimension(:) :: x

      real   (kind=RXP)               , allocatable ,dimension(:) :: y
      integer(kind=IXP)                                           :: nd
      integer(kind=IXP)                                           :: i
      integer(kind=IXP)                                           :: id
      integer(kind=IXP)                                           :: ios

      nd  = int((n-ONE_IXP)/f,kind=IXP) + ONE_IXP !must be equal to formula in subroutine filter_snapshot_surface_gll

      ios = init_array_real(y,nd,"decimate:y")

      id  = ZERO_IXP

      do i = ONE_IXP,n,f

         id    = id + ONE_IXP
         y(id) = x(i) 

      enddo

      deallocate(x)

      ios = init_array_real(x,nd,"decimate:x")

      x(:) = y(:)

      return

!***********************************************************************************************************************************************************************************
   end subroutine decimate
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to find the number of prime factor used to decompose an integer
!>@param  n  : integer(kind=IXP)to be decomposed
!>@return np : number of prime factors needed to decompose n
!***********************************************************************************************************************************************************************************
   subroutine find_nprimes(n,np)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in) :: n
      integer(kind=IXP), intent(out) :: np
      
      integer(kind=IXP)              :: div
      integer(kind=IXP)              :: next
      integer(kind=IXP)              :: rest
      
      np   = ONE_IXP
      div  = TWO_IXP
      next = THREE_IXP
      rest = n
      
      do while ( rest /= ONE_IXP )

         do while ( mod(rest, div) == ZERO_IXP ) 

            np   = np + ONE_IXP
            rest = rest / div

         enddo

         div  = next
         next = next + TWO_IXP

      enddo

      np = np - ONE_IXP

      return
 
!***********************************************************************************************************************************************************************************
  end subroutine find_nprimes
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to decompose an integer(kind=IXP)into its prime factor
!>@param  n : integer(kind=IXP)to be decomposed
!>@return d : prime factors decomposition of n
!***********************************************************************************************************************************************************************************
   subroutine find_factors(n,d)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP)              , intent( in) :: n
      integer(kind=IXP), dimension(:), intent(out) :: d
      
      integer(kind=IXP)                            :: div
      integer(kind=IXP)                            :: next
      integer(kind=IXP)                            :: rest
      integer(kind=IXP)                            :: i
      
      i    = ONE_IXP
      div  = TWO_IXP
      next = THREE_IXP
      rest = n
      
      do while ( rest /= ONE_IXP )

         do while ( mod(rest, div) == ZERO_IXP ) 

            d(i) = div
            i    = i + ONE_IXP
            rest = rest / div

         enddo

         div  = next
         next = next + TWO_IXP

      enddo

      return
 
!***********************************************************************************************************************************************************************************
  end subroutine find_factors
!***********************************************************************************************************************************************************************************

end module mod_filter
