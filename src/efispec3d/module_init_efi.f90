!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to initialize some global variable vectors and matrices.

!>@brief
!!This module contains subroutines to initialize some global variable vectors and matrices.
module mod_init_efi

   use mod_precision

   use mod_global_variables, only : CIL

   use mod_init_memory, only : init_type_double_couple_source

   use mod_efi_mpi

   implicit none

   private

   public :: init_input_variables
   public :: init_temporal_domain
   public :: init_temporal_saving
   public :: init_gll_nodes
   public :: init_gll_nodes_coordinates
   public :: init_mass_matrix
   public :: init_jacobian_matrix_hexa
   public :: init_jacobian_matrix_quad
   private:: read_buffer

   contains

!
!
!>@brief This subroutine initializes the simulation by writing header of listing file *.lst and reading variables of configuration file *.cfg
!>@param cl_prefix : full prefix of the simulation (i.e., including directory path)
!>@return global variables read from file *.cfg needed to initialize a simulation
!***********************************************************************************************************************************************************************************
   subroutine init_input_variables(cl_prefix)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables

      use mod_write_listing

      use mod_random_field, only : init_random_seed

      implicit none

      character(len=CIL), intent(in) :: cl_prefix

      integer(kind=IXP)              :: unit_input_file
      integer(kind=IXP)              :: ios
      integer(kind=IXP)              :: imat
      integer(kind=IXP)              :: ilay
      integer(kind=IXP)              :: idcs
      integer(kind=IXP)              :: ifault
      logical(kind=IXP)              :: is_layer_defined    = .false.
      logical(kind=IXP)              :: is_material_defined = .false.
      logical(kind=IXP)              :: is_tomo_defined     = .false.
      logical(kind=IXP)              :: is_dcs_defined      = .false. 
      logical(kind=IXP)              :: is_sfs_defined      = .false.
      logical(kind=IXP)              :: is_fault_defined    = .false.
      logical(kind=IXP)              :: is_random           = .false.
      

      character(len=100)             :: buffer_value
      character(len=100)             :: keyword
      character(len=100)             :: buffer_value_2
      character(len=100)             :: keyword_2
      character(len=CIL)             :: info

!
!---->Check if order of polynomial is correct

      if (IG_LAGRANGE_ORDER < FOUR_IXP .or. IG_LAGRANGE_ORDER > SIX_IXP) then

         write(info,'(a)') "error while checking order of lagrange polynomial"
         call error_stop(info)

      endif
   
!
!---->write header of simulation in listing file *.lst

      call write_header()

!
!
!*************************************************************************************************************
!---->process configuration file *.cfg
!*************************************************************************************************************

      open(unit=get_newunit(unit_input_file),file=trim(cl_prefix)//".cfg",status='old',iostat=ios)

      if (ios /= ZERO_IXP) then

         write(info,'(a)') "error in subroutine init_input_variables while opening file *.cfg"

         call error_stop(info)

      endif

!
!---->search for keywords in file *.cfg

l1:   do

         call read_line(unit_input_file,keyword,buffer_value,ios)

         if (ios /= ZERO_IXP) exit

         selectcase(trim(sweep_blanks(keyword)))

            case("durationofsimulation")
               read(buffer_value,*) rg_simu_total_time
      
            case("timestep")
               read(buffer_value,*) rg_dt

            case("maximumfrequency", "maximumfrequencyofsimulation", "maximumfrequencyofthesimulation")
               read(buffer_value,*) rg_fmax_simu

            case("receiversavingincrement")
               read(buffer_value,*) ig_receiver_saving_incr

            case("receiver1dvelocitystructure")
               read(buffer_value,*) lg_receiver_1d_velocity_structure,rg_receiver_1d_velocity_structure_dz

            case("filtercutofffrequency")
               read(buffer_value,*) rg_iir_filter_cutoff_freq

            case("filtertransitionband")
               read(buffer_value,*) rg_iir_filter_transition_band

            case("filteroutputmotion")
               read(buffer_value,*) cg_iir_filter_output_motion

            case("planewavemotiondirection")
               read(buffer_value,*) ig_plane_wave_dir

            case("planewaveinputdepth")
               read(buffer_value,*) rg_plane_wave_z

            case("planewavesourcetimeduration")
               read(buffer_value,*) rg_plane_wave_std

            case("snapshotsavingincrement", "surfacesnapshotsavingincrement")
               read(buffer_value,*) ig_snapshot_saving_incr

            case("snapshotspaceincrement", "surfacesnapshotspaceincrement")
               read(buffer_value,*) rg_receiver_snapshot_dxdy

            case("snapshotdisplacement", "surfacesnapshotdisplacement") 
               read(buffer_value,*) lg_snapshot_displacement

            case("snapshotvelocity", "surfacesnapshotvelocity")
               read(buffer_value,*) lg_snapshot_velocity

            case("snapshotacceleration", "surfacesnapshotacceleration")
               read(buffer_value,*) lg_snapshot_acceleration

            case("volumesnapshotsavingincrement")
               read(buffer_value,*) ig_snapshot_volume_saving_incr

            case("volumesnapshotdisplacement") 
               read(buffer_value,*) lg_snapshot_volume_displacement

            case("volumesnapshotvelocity") 
               read(buffer_value,*) lg_snapshot_volume_velocity

            case("volumesnapshotacceleration") 
               read(buffer_value,*) lg_snapshot_volume_acceleration

            case("volumesnapshotxmin") 
               read(buffer_value,*) rg_snapshot_volume_xmin

            case("volumesnapshotxmax") 
               read(buffer_value,*) rg_snapshot_volume_xmax

            case("volumesnapshotymin") 
               read(buffer_value,*) rg_snapshot_volume_ymin

            case("volumesnapshotymax") 
               read(buffer_value,*) rg_snapshot_volume_ymax

            case("volumesnapshotzmin") 
               read(buffer_value,*) rg_snapshot_volume_zmin

            case("volumesnapshotzmax") 
               read(buffer_value,*) rg_snapshot_volume_zmax

            case("boundaryabsorption")
               read(buffer_value,*) lg_boundary_absorption

!
!---------->medium type -> materials (defined in CUBIT mesh)
 
            case("numberofmaterial","numberofmaterials")

               read(buffer_value,*) ig_nmaterial

               if (LG_VISCO) then

                  allocate(tg_elastic_material     (ig_nmaterial))
                  allocate(tg_anelastic_material(ig_nmaterial))

               else

                  allocate(tg_elastic_material(ig_nmaterial))

               endif

            case("material")

               read(buffer_value,*) imat

               is_material_defined = .true.

               if (imat > ig_nmaterial) then

                  write(info,'(a)') "error in subroutine module_init_efi:init_input_variables: material number larger than number of materials. please check *.cfg file"

                  call error_stop(info)

               endif

lm:            do

                  call read_line(unit_input_file,keyword_2,buffer_value_2,ios)

                  if (ios /= ZERO_IXP) exit

                  selectcase(trim(sweep_blanks(keyword_2)))

                     case("vs")
                        call read_buffer(keyword_2,buffer_value_2,tg_elastic_material(imat)%svel,rg_uq_coeff)
                     
                     case("vp")
                        call read_buffer(keyword_2,buffer_value_2,tg_elastic_material(imat)%pvel,rg_uq_coeff)
                     
                     case("rho")
                        call read_buffer(keyword_2,buffer_value_2,tg_elastic_material(imat)%dens,rg_uq_coeff)

                     case("qf")
                        if (LG_VISCO) then
                           call read_buffer(keyword_2,buffer_value_2,tg_anelastic_material(imat)%freq,rg_uq_coeff)
                        endif
                     
                     case("qs")
                        if (LG_VISCO) then
                           call read_buffer(keyword_2,buffer_value_2,tg_anelastic_material(imat)%qs,rg_uq_coeff)
                        endif
                     
                     case("qp")
                        if (LG_VISCO) then
                           call read_buffer(keyword_2,buffer_value_2,tg_anelastic_material(imat)%qp,rg_uq_coeff)
                        endif

                     case("numberofmaterial","numberofmaterials","material","numberoffault","numberoffaults","fault","numberoflayer","numberoflayers","layer","numberofdoublecouplepointsource","numberofdoublecouplepointsources","dcps","doublecouplepointsource")

                        backspace(unit=unit_input_file)

                        cycle l1

                  endselect
  
               enddo lm


!
!---------->medium type -> tomography

            case("tomographyfolder")

               is_tomo_defined = .true.

               read(buffer_value,*) tg_tomo%input_folder

!
!---------->medium type -> layers (defined by interfaces)

            case("numberoflayer","numberoflayers")

               read(buffer_value,*) ig_nlayer

               ig_ninterface = ig_nlayer !NOT = ig_nlayer+1 because bottommost interface is implicitely defined by the mesh (no need to define it in a file *.if*)

               allocate(tg_layer(ig_nlayer))

            case("layer")

               is_layer_defined = .true.

               read(buffer_value,*) ilay

               if (ilay > ig_nlayer) then
                  write(info,'(a)') "error in subroutine module_init_efi:init_input_variables: layer number larger than number of layers. please check *.cfg file"
                  call error_stop(info)
               endif

ll:            do

                  call read_line(unit_input_file,keyword_2,buffer_value_2,ios)

                  if (ios /= ZERO_IXP) exit
             
                  selectcase(trim(sweep_blanks(keyword_2)))

                     case("structuretype")

                        selectcase(trim(sweep_blanks(buffer_value_2)))

                           case("constant")
                              tg_layer(ilay)%structure_type = 1_IXP
                              tg_layer(ilay)%nmat = 1_IXP
                              allocate(tg_layer(ilay)%elastic(ONE_IXP))
                              if (LG_VISCO) allocate(tg_layer(ilay)%anelastic(ONE_IXP))

                           case("gradient")
                              tg_layer(ilay)%structure_type = 2_IXP
                              tg_layer(ilay)%nmat = 2_IXP
                              allocate(tg_layer(ilay)%elastic(TWO_IXP))
                              if (LG_VISCO) allocate(tg_layer(ilay)%anelastic(TWO_IXP))

                           case("random")
                              tg_layer(ilay)%structure_type = 3_IXP
                              tg_layer(ilay)%nmat = 1_IXP
                              allocate(tg_layer(ilay)%elastic(ONE_IXP))
                              if (LG_VISCO) allocate(tg_layer(ilay)%anelastic(ONE_IXP))
                              allocate(tg_layer(ilay)%random(ONE_IXP))
                              is_random = .true.

                           case("user")
                              tg_layer(ilay)%structure_type = 4_IXP
                              tg_layer(ilay)%nmat = 1_IXP
                              allocate(tg_layer(ilay)%elastic(ONE_IXP))
                              if (LG_VISCO) allocate(tg_layer(ilay)%anelastic(ONE_IXP))

                           case default
                              write(info,'(a)') "error in subroutine module_init_efi:init_input_variables: unknown structure type"
                              call error_stop(info)

                        endselect

!
!------------------->constant properties. used also for random and user, but NOT for gradient

                     case("vs")
                        if (.not.tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%elastic(ONE_IXP)%svel,rg_uq_coeff)
                     
                     case("vp")
                        if (.not.tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%elastic(ONE_IXP)%pvel,rg_uq_coeff)
                     
                     case("rho")
                        if (.not.tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%elastic(ONE_IXP)%dens,rg_uq_coeff)

                     case("qf")
                        if (LG_VISCO) then
                           if (.not.tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%anelastic(ONE_IXP)%freq,rg_uq_coeff)
                        endif
                     
                     case("qs")
                        if (LG_VISCO) then
                           if (.not.tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%anelastic(ONE_IXP)%qs,rg_uq_coeff)
                        endif
                     
                     case("qp")
                        if (LG_VISCO) then
                           if (.not.tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%anelastic(ONE_IXP)%qp,rg_uq_coeff)
                        endif

!
!------------------->linear gradient properties

                     case("vs+","vst","vstop")
                        if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%elastic(ONE_IXP)%svel,rg_uq_coeff)
                     
                     case("vs-","vsb","vsbot","vsbottom")
                        if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%elastic(TWO_IXP)%svel,rg_uq_coeff)
                     
                     case("vp+","vpt","vptop")
                        if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%elastic(ONE_IXP)%pvel,rg_uq_coeff)
                     
                     case("vp-","vpb","vpbot","vpbottom")
                        if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%elastic(TWO_IXP)%pvel,rg_uq_coeff)
                     
                     case("rho+","rhot","rhotop")
                        if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%elastic(ONE_IXP)%dens,rg_uq_coeff)
                     
                     case("rho-","rhob","rhobot","rhobottom")
                        if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%elastic(TWO_IXP)%dens,rg_uq_coeff)
                     
                     case("qs+","qst","qstop")
                        if (LG_VISCO) then
                           if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%anelastic(ONE_IXP)%qs,rg_uq_coeff)
                        endif
                     
                     case("qs-","qsb","qsbot","qsbottom")
                        if (LG_VISCO) then
                           if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%anelastic(TWO_IXP)%qs,rg_uq_coeff)
                        endif
                     
                     case("qp+","qpt","qptop")
                        if (LG_VISCO) then
                           if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%anelastic(ONE_IXP)%qp,rg_uq_coeff)
                        endif
                     
                     case("qp-","qpb","qpbot","qpbottom")
                        if (LG_VISCO) then
                           if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%anelastic(TWO_IXP)%qp,rg_uq_coeff)
                        endif
                     
                     case("qf+","qft","qftop")
                        if (LG_VISCO) then
                           if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%anelastic(ONE_IXP)%freq,rg_uq_coeff)
                        endif
                     
                     case("qf-","qfb","qfbot","qfbottom")
                        if (LG_VISCO) then
                           if (tg_layer(ilay)%structure_type == 2_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%anelastic(TWO_IXP)%freq,rg_uq_coeff)
                        endif

!
!------------------->random properties

                     case("lcx")
                        if (tg_layer(ilay)%structure_type == 3_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%random(ONE_IXP)%lcx,rg_uq_coeff)
                     
                     case("lcy")
                        if (tg_layer(ilay)%structure_type == 3_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%random(ONE_IXP)%lcy,rg_uq_coeff)
                     
                     case("lcz")
                        if (tg_layer(ilay)%structure_type == 3_IXP) call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%random(ONE_IXP)%lcz,rg_uq_coeff)
                     
                     case("eps","e")
                        if (tg_layer(ilay)%structure_type == 3_IXP) then
                           call read_buffer(keyword_2,buffer_value_2,tg_layer(ilay)%random(ONE_IXP)%e,rg_uq_coeff)
                        endif
                     
                     case("numberofmaterial","numberofmaterials","material","numberoffault","numberoffaults","fault","numberoflayer","numberoflayers","layer","numberofdoublecouplepointsource","numberofdoublecouplepointsources","dcps","doublecouplepointsource")

                        backspace(unit=unit_input_file)

                        cycle l1

                  endselect
  
               enddo ll

!
!---------->double couple point source

            case("numberofdoublecouplepointsource","numberofdoublecouplepointsources")

               read(buffer_value,*) ig_ndcsource

               ios = init_type_double_couple_source(tg_dcsource_init,ig_ndcsource,"module_init_efi:tg_dcsource_init")

            case("dcps","doublecouplepointsource")

               read(buffer_value,*) idcs

               is_dcs_defined = .true.

               if (idcs > ig_ndcsource) then
               
                  write(info,'(a)') "error in subroutine module_init_efi:init_input_variables: double couple point source number larger than number of double couple source. please check *.cfg file"
               
                  call error_stop(info)
               
               endif

ldcps:         do

                  call read_line(unit_input_file,keyword_2,buffer_value_2,ios)

                  if (ios /= ZERO_IXP) exit

                  selectcase(trim(sweep_blanks(keyword_2)))

                     case("x")
                        call read_buffer(keyword_2,buffer_value_2,tg_dcsource_init(idcs)%p%x,rg_uq_coeff)
                     
                     case("y")
                        call read_buffer(keyword_2,buffer_value_2,tg_dcsource_init(idcs)%p%y,rg_uq_coeff)
                     
                     case("z")
                        call read_buffer(keyword_2,buffer_value_2,tg_dcsource_init(idcs)%p%z,rg_uq_coeff)

                     case("mw")
                        call read_buffer(keyword_2,buffer_value_2,tg_dcsource_init(idcs)%mw,rg_uq_coeff)

                     case("str","strike")
                        call read_buffer(keyword_2,buffer_value_2,tg_dcsource_init(idcs)%str,rg_uq_coeff)

                     case("dip")
                        call read_buffer(keyword_2,buffer_value_2,tg_dcsource_init(idcs)%dip,rg_uq_coeff)

                     case("rak","rake")
                        call read_buffer(keyword_2,buffer_value_2,tg_dcsource_init(idcs)%rak,rg_uq_coeff)

                     case("stf")
                        read(buffer_value_2,*) tg_dcsource_init(idcs)%icur

                     case("rt")
                        call read_buffer(keyword_2,buffer_value_2,tg_dcsource_init(idcs)%rise_time,rg_uq_coeff)

                     case("ts")
                        call read_buffer(keyword_2,buffer_value_2,tg_dcsource_init(idcs)%shift_time,rg_uq_coeff)

                     case("numberofmaterial","numberofmaterials","material","numberoffault","numberoffaults","fault","numberoflayer","numberoflayers","layer","numberofdoublecouplepointsource","numberofdoublecouplepointsources","dcps","doublecouplepointsource")

                        backspace(unit=unit_input_file)

                        cycle l1

                  endselect

               enddo ldcps

!
!---------->fault (double couple seismic sources)
 
            case("numberoffault","numberoffaults")

               read(buffer_value,*) ig_nfault

               allocate(tg_fault(ig_nfault))

            case("fault")

               read(buffer_value,*) ifault

               is_fault_defined = .true.

               if (ifault > ig_nfault) then

                  write(info,'(a)') "error in subroutine module_init_efi:init_input_variables: fault number larger than number of faults. please check *.cfg file"

                  call error_stop(info)

               else

                  tg_fault(ifault)%ifault = ifault

               endif

lf:            do

                  call read_line(unit_input_file,keyword_2,buffer_value_2,ios)

                  if (ios /= ZERO_IXP) exit

                  selectcase(trim(sweep_blanks(keyword_2)))

                     case("centerx-coord","xc")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%xc,rg_uq_coeff)
                     
                     case("centery-coord","yc")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%yc,rg_uq_coeff)
                     
                     case("centerz-coord","zc")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%zc,rg_uq_coeff)
                     
                     case("str","strike")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%str,rg_uq_coeff)
                     
                     case("dip")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%dip,rg_uq_coeff)
                     
                     case("rak","rake")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%rak,rg_uq_coeff)
                     
                     case("strikelength","ls")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%ls,rg_uq_coeff)
                     
                     case("diplength","ld")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%ld,rg_uq_coeff)
                     
                     case("xhypo","hypocenters-coord")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%sh,rg_uq_coeff)
                     
                     case("yhypo","hypocenterd-coord")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%dh,rg_uq_coeff)
                     
                     case("acf","autocorrelationfunction")
                        tg_fault(ifault)%acf = trim(sweep_blanks(buffer_value_2))

                     case("lcx","lcs","lcs-coord")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%lcs,rg_uq_coeff)
                     
                     case("lcy","lcd","lcd-coord")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%lcd,rg_uq_coeff)
                     
                     case("kappa","k")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%k,rg_uq_coeff)
                     
                     case("velocityruptureratio","vrr")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%vrr,rg_uq_coeff)
                     
                     case("mw")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%mw,rg_uq_coeff)
                     
                     case("sourcetimefunction","stf")
                        read(buffer_value_2,*) tg_fault(ifault)%icur
                     
                     case("rt","risetime")
                        call read_buffer(keyword_2,buffer_value_2,tg_fault(ifault)%rt,rg_uq_coeff)
                     
                     case("ts","timeshift")
                        read(buffer_value_2,*) tg_fault(ifault)%ts
                     
                     case("numberofmaterial","numberofmaterials","material","numberoffault","numberoffaults","fault","numberoflayer","numberoflayers","layer","numberofdoublecouplepointsource","numberofdoublecouplepointsources","dcps","doublecouplepointsource")

                        backspace(unit=unit_input_file)

                        cycle l1

                  endselect
  
               enddo lf

         endselect
      
      enddo l1 
   
      close(unit_input_file)

!
!
!*************************************************************************************************************
!---->init random seed if any random material
!*************************************************************************************************************

      if (is_random) call init_random_seed("rsm")

!
!
!*************************************************************************************************************
!---->check for illegal value of input variables
!*************************************************************************************************************

      if (rg_simu_total_time <= TINY_REAL_RXP) then
         write(info,'(a)') "error in subroutine init_input_variables: duration of simulation must be greater than TINY_REAL_RXP"
         call error_stop(info)
      endif
      
      do imat = ONE_IXP,ig_nmaterial

         if (tg_elastic_material(imat)%svel <= TINY_REAL_RXP) then
            write(info,'(a)') "error in subroutine init_input_variables: S-wave velocity must be greater than TINY_REAL_RXP"
            call error_stop(info)
         endif

         if (tg_elastic_material(imat)%pvel <= TINY_REAL_RXP) then
            write(info,'(a)') "error in subroutine init_input_variables: P-wave velocity must be greater than TINY_REAL_RXP"
            call error_stop(info)
         endif

         if (tg_elastic_material(imat)%dens <= TINY_REAL_RXP) then
            write(info,'(a)') "error in subroutine init_input_variables: density must be greater than TINY_REAL_RXP"
            call error_stop(info)
         endif

         if (LG_VISCO) then
            if (tg_anelastic_material(imat)%freq <= TINY_REAL_RXP) then
               write(info,'(a)') "error in subroutine init_input_variables: frequency for viscoelastic material must be greater than TINY_REAL_RXP"
               call error_stop(info)
            endif
            if (tg_anelastic_material(imat)%qs   <= TINY_REAL_RXP) then
               write(info,'(a)') "error in subroutine init_input_variables: shear-wave quality factor for viscoelastic material must be greater than TINY_REAL_RXP"
               call error_stop(info)
            endif
            if (tg_anelastic_material(imat)%qp   <= TINY_REAL_RXP) then
               write(info,'(a)') "error in subroutine init_input_variables: pressure-wave quality factor for viscoelastic material must be greater than TINY_REAL_RXP"
               call error_stop(info)
            endif
         endif

      enddo

!
!---->check if a material is defined 

      if (.not.(is_material_defined) .and. .not.(is_layer_defined) .and. .not.(is_tomo_defined)) then
         write(info,'(a)') "error in subroutine init_input_variables: no material, no layer and no tomo defined. Define at least one material, one layer, or one tomography folder."
         call error_stop(info)
      endif

!
!---->check if a single material type is defined

      if ( (is_material_defined .and. is_layer_defined) .or. (is_material_defined .and. is_tomo_defined) .or. (is_layer_defined .and. is_tomo_defined ) ) then
         write(info,'(a)') "error in subroutine init_input_variables: material, layer and tomo types are mutually exclusive. Define only one of them."
         call error_stop(info)
      endif

!
!---->store is_tomo_defined in tg_tomo structure
      tg_tomo%is_tomo_defined = is_tomo_defined

!
!---->if IIR filter is used, then check cutoff frequency, transition band and attenuation

      if (LG_SNAPSHOT_SURF_GLL_FILTER) then
         
         if (rg_iir_filter_cutoff_freq <= TINY_REAL_RXP) then
            write(info,'(a)') "error in subroutine init_input_variables: iir cutoff frequency must be greater than TINY_REAL_RXP"
            call error_stop(info)
         endif

         if (rg_iir_filter_transition_band <= TINY_REAL_RXP) then
            write(info,'(a)') "error in subroutine init_input_variables: iir filter transition band must be greater than TINY_REAL_RXP"
            call error_stop(info)
         endif

         if ( (rg_iir_filter_cutoff_freq - rg_iir_filter_transition_band/TWO_RXP) < ZERO_RXP) then
            write(info,'(a)') "error in subroutine init_input_variables: (iir filter cutoff frequency - transition band/2) < 0.0"
            call error_stop(info)
         endif

         if ( (rg_dt > TINY_REAL_RXP) .and. (rg_iir_filter_cutoff_freq + rg_iir_filter_transition_band/2.0_RXP) > (1.0_RXP/(rg_dt*2.0_RXP)) ) then
            write(info,'(a)') "error in subroutine init_input_variables: (iir filter cutoff frequency + transition band/2) > nyquist frequency"
            call error_stop(info)
         endif

      endif

!
!---->if LG_PLANE_WAVE is true, check input values

      if (LG_PLANE_WAVE) then

         if ( (ig_plane_wave_dir <= ZERO_IXP) .or. (ig_plane_wave_dir >= FOUR_IXP) ) then
            write(info,'(a)') "error in subroutine init_input_variables: direction of motion of the particle of plane wave must be between 1 and 3"
            call error_stop(info)
         endif

      endif


!
!
!*************************************************************************************************************
!set default value of input variables
!*************************************************************************************************************

!
!---->if rg_dt is defined then init the temporal domain. If rg_dt not defined, then subroutine init_time_step will take care of it.

      if (rg_dt > TINY_REAL_RXP) then

         call init_temporal_domain(rg_simu_total_time,rg_dt)

         call init_temporal_saving(ig_ndt)

      endif

      call write_temporal_domain_info()

!
!---->check rg_fmax_simu

      if (abs(rg_fmax_simu) < TINY_REAL_RXP) then

         rg_fmax_simu = 0.1_RXP

         if (ig_myrank == ZERO_IXP) then

            write(IG_LST_UNIT,'("",/,a)') "warning: rg_fmax_simu set to 0.1 Hz by default"

         endif

      endif

!
!---->check if a source is defined. If plane wave is activated, then double couple and single force sources are not activated.

      if (ig_myrank == ZERO_IXP) then
      
         write(IG_LST_UNIT,'(/,a)') "type of seismic source"
      
      endif

      if (.not.LG_PLANE_WAVE) then

         if (ig_ndcsource == 0_IXP) inquire(file=trim(cl_prefix)//".dcs",exist=is_dcs_defined)
         
         if (ig_nsfsource == 0_IXP) inquire(file=trim(cl_prefix)//".sfs",exist=is_sfs_defined)

         if (.not.( any((/is_dcs_defined, is_sfs_defined, is_fault_defined/)) )) then
         
            write(info,'(a)') "error in subroutine init_input_variables: no source defined"
         
            call error_stop(info)
         
         endif

         if (ig_myrank == ZERO_IXP) then

            if(is_dcs_defined)   write(IG_LST_UNIT,'(a)') " --> double couple source"
            if(is_sfs_defined)   write(IG_LST_UNIT,'(a)') " --> single force  source"
            if(is_fault_defined) write(IG_LST_UNIT,'(a)') " --> extended fault"

         endif

      else

         if (ig_myrank == ZERO_IXP) then

            write(IG_LST_UNIT,'(a)') " --> plane wave - vertical incidence"

         endif

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_input_variables
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine initializes the number of time step of the simulation and the squared time step.
!>@return mod_global_variables::ig_ndt
!>@return mod_global_variables::rg_dt2 = @f$ \Delta t^{2} @f$
!***********************************************************************************************************************************************************************************
   subroutine init_temporal_domain(t,dt)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                      ig_ndt&
                                     ,rg_dt2&
                                     ,ig_mpi_comm_simu

      implicit none

      real   (kind=RXP), intent(in) :: t
      real   (kind=RXP), intent(in) :: dt

      integer(kind=IXP)            :: idum
      integer(kind=IXP)            :: ios


      ig_ndt = int(t/dt,kind=IXP) + ONE_IXP

!
!---->force all cpus to have the same number of time steps
      call mpi_allreduce(ig_ndt,idum,ONE_IXP,MPI_INTEGER,MPI_MAX,ig_mpi_comm_simu,ios)

      ig_ndt = idum

!
!---->pre-compute dt**2
      rg_dt2 = dt**TWO_IXP

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_temporal_domain
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine set saving information for receivers and snapshots.
!>@return mod_global_variables::ig_receiver_saving_incr
!>@return mod_global_variables::ig_snapshot_saving_incr
!>@return mod_global_variables::lg_snapshot_displacement
!>@return mod_global_variables::lg_snapshot_velocity
!>@return mod_global_variables::lg_snapshot_acceleration
!>@return mod_global_variables::lg_snapshot_volume_displacement
!>@return mod_global_variables::lg_snapshot_volume_velocity
!>@return mod_global_variables::lg_snapshot_volume_acceleration
!***********************************************************************************************************************************************************************************
   subroutine init_temporal_saving(ndt)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      ig_receiver_saving_incr&
                                     ,ig_snapshot_saving_incr&
                                     ,lg_snapshot&
                                     ,lg_snapshot_displacement&
                                     ,lg_snapshot_velocity&
                                     ,lg_snapshot_acceleration&
                                     ,ig_snapshot_volume_saving_incr&
                                     ,lg_snapshot_volume&
                                     ,lg_snapshot_volume_displacement&
                                     ,lg_snapshot_volume_velocity&
                                     ,lg_snapshot_volume_acceleration

      implicit none

      integer(kind=IXP), intent(in) :: ndt

      if (ig_receiver_saving_incr == ZERO_IXP) then

         ig_receiver_saving_incr = ndt + 10_IXP
   
      endif

      if ( (ig_snapshot_saving_incr == ZERO_IXP) .or. (.not.lg_snapshot_displacement .and. .not.lg_snapshot_velocity .and. .not.lg_snapshot_acceleration) ) then

         ig_snapshot_saving_incr  = ndt + 10_IXP
         lg_snapshot              = .false.
         lg_snapshot_displacement = .false.
         lg_snapshot_velocity     = .false.
         lg_snapshot_acceleration = .false.
   
      else

         lg_snapshot  = .true.

      endif

      if ( (ig_snapshot_volume_saving_incr == ZERO_IXP) .or. (.not.lg_snapshot_volume_displacement .and. .not.lg_snapshot_volume_velocity .and. .not.lg_snapshot_volume_acceleration) ) then

         ig_snapshot_volume_saving_incr  = ndt + 10_IXP
         lg_snapshot_volume              = .false.
         lg_snapshot_volume_displacement = .false.
         lg_snapshot_volume_velocity     = .false.
         lg_snapshot_volume_acceleration = .false.
   
      else

         lg_snapshot_volume  = .true.

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_temporal_saving
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine computes GLL nodes abscissa and weight in the reference domain [-1:1] as well as derivative of Lagrange polynomials at GLL nodes.
!!
!>@reference C. Canuto, M. Y. Hussaini, A. Quarteroni, T. A. Tang, "Spectral Methods in Fluid Dynamics," Section 2.3. Springer-Verlag 1987
!!
!>@authors Initially written for Matlab by G. von Winckel - 04/17/2004. Translated in FORTRAN for EFISPEC3D by F. De Martin
!!
!>@return mod_global_variables::rg_gll_abscissa       : GLL node coordinates @f$\xi,\eta,\zeta@f$ in the reference domain [-1:1]
!>@return mod_global_variables::rg_gll_weight         : GLL node weights
!>@return mod_global_variables::rg_gll_lagrange_deriv : derivative of Lagrange polynomials at GLL nodes
!>@return mod_global_variables::rg_gll_abscissa_dist  : inverse of distance between GLL nodes
!***********************************************************************************************************************************************************************************
   subroutine init_gll_nodes()
!***********************************************************************************************************************************************************************************
   
      use mpi
      
      use mod_global_variables, only : &
                                       rg_gll_abscissa_dist&
                                      ,rg_gll_abscissa&
                                      ,rg_gll_weight&
                                      ,rg_gll_lagrange_deriv&
                                      ,IG_NGLL&
                                      ,IG_LST_UNIT&
                                      ,ig_myrank&
                                      ,IG_LAGRANGE_ORDER
      
      use mod_lagrange        , only : lagrad
      
      implicit none

      real   (kind=RXP), parameter                  :: EPS = 1.0E-7_RXP
      real   (kind=RXP), allocatable ,dimension(:)  :: xold
      real   (kind=RXP), dimension(IG_NGLL,IG_NGLL) :: vandermonde_matrix
      integer(kind=IXP)                             :: ngll
      integer(kind=IXP)                             :: i
      integer(kind=IXP)                             :: j
      integer(kind=IXP)                             :: k
      integer(kind=IXP)                             :: n
      
      !
      !
      !higher integration order among different group is taken as the integration order
      n = IG_LAGRANGE_ORDER

 
      !
      !
      !ngll is the number of integration node in 1d along the segment [-1;1]
      ngll = n + ONE_IXP


      !
      !
      !*******************************
      !find gll abscissa and weight
      !*******************************
      !
      !use the chebyshev-gauss-lobatto nodes as the first guess
      do i = ONE_IXP,ngll
         rg_gll_abscissa(i) = cos(PI_RXP*real(i-ONE_IXP,kind=RXP)/real(n,kind=RXP))
      enddo
      !
      !initialize the legendre vandermonde matrix
      vandermonde_matrix(:,:) = ZERO_RXP
      !
      !compute p_{n} using the recursion relation. compute its first and second derivatives and update rg_gll_abscissa using the newton-raphson method
      allocate(xold(ngll))
      xold(:) = TWO_RXP
      do while (maxval(abs(rg_gll_abscissa-xold)) > EPS)
      
         xold(:)                       = rg_gll_abscissa(:)
         vandermonde_matrix(:,ONE_IXP) = ONE_RXP
         vandermonde_matrix(:,TWO_IXP) = rg_gll_abscissa(:)
      
         do k = TWO_IXP,n
            vandermonde_matrix(:,k+ONE_IXP) = ( (TWO_RXP*real(k,kind=RXP)-ONE_RXP)*rg_gll_abscissa(:)*vandermonde_matrix(:,k)-(real(k,kind=RXP)-ONE_RXP)*vandermonde_matrix(:,k-ONE_IXP) )/real(k,kind=RXP)
         enddo
      
         rg_gll_abscissa(:) = xold(:)-( rg_gll_abscissa(:)*vandermonde_matrix(:,ngll)-vandermonde_matrix(:,n) )/( ngll*vandermonde_matrix(:,ngll) )
      
      enddo
      !
      !compute corresponding weight
      rg_gll_weight(:) = TWO_RXP/(n*ngll*vandermonde_matrix(:,ngll)**TWO_IXP)
      
      !
      !
      !*******************************************************************************
      !write results rg_gll_abscissa,rg_gll_weight in file *.lst
      !*******************************************************************************
      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'("",/,a)') "abscissa of GLLs - weight of GLLs"
         do i = ONE_IXP,ngll
            write(IG_LST_UNIT,fmt='(2(7X,f10.7))') rg_gll_abscissa(i),rg_gll_weight(i)
         enddo

      endif
      
      !
      !
      !*******************************************************************************
      !compute subtraction between gll nodes (used in subroutines lagrap and lagrad)
      !*******************************************************************************
      do i = ONE_IXP,IG_NGLL
         do j = ONE_IXP,IG_NGLL
            rg_gll_abscissa_dist(j,i) = ZERO_RXP
            if (i /= j) rg_gll_abscissa_dist(j,i) = ONE_RXP/(rg_gll_abscissa(i) - rg_gll_abscissa(j))
         enddo
      enddo
      
      !
      !
      !***************************************************
      !compute derivative of lagrange poly at GLL nodes
      !***************************************************
      !
      !notation convention: rg_gll_lagrange_deriv(j,i) = h'_j(xgll_i)
      do i = ONE_IXP,IG_NGLL
         do j = ONE_IXP,IG_NGLL
           rg_gll_lagrange_deriv(j,i) = lagrad(j,rg_gll_abscissa(i),IG_NGLL)
         enddo
      enddo
      
      return

!***********************************************************************************************************************************************************************************
   end subroutine init_gll_nodes
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine computes GLL nodes @f$x,y,z@f$-coordinates in the physical domain, cartesian right-handed coordinate system.
!>@return mod_global_variables::rg_gll_coordinate : GLL nodes @f$x,y,z@f$-coordinates in cpu myrank
!***********************************************************************************************************************************************************************************
   subroutine init_gll_nodes_coordinates()
!***********************************************************************************************************************************************************************************
   
      use mpi
   
      use mod_global_variables, only :&
                                      rg_gll_coordinate&
                                     ,ig_ngll_total&
                                     ,ig_nhexa&
                                     ,IG_NGLL&
                                     ,IG_NDOF&
                                     ,ig_hexa_gll_glonum&
                                     ,ig_hexa_nnode&
                                     ,ig_hexa_gnode_xiloc&
                                     ,ig_hexa_gnode_etloc&
                                     ,ig_hexa_gnode_zeloc&
                                     ,rg_gll_abscissa&
                                     ,ig_line_nnode&
                                     ,ig_hexa_gnode_glonum&
                                     ,rg_gnode_x&
                                     ,rg_gnode_y&
                                     ,rg_gnode_z&
                                     ,cg_prefix&
                                     ,cg_myrank&
                                     ,LG_OUTPUT_DEBUG_FILE&
                                     ,error_stop

      use mod_init_memory
      
      use mod_lagrange, only : lagrap_geom
      
      implicit none

      real   (kind=RXP)                          :: lag_xi
      real   (kind=RXP)                          :: lag_eta
      real   (kind=RXP)                          :: lag_zeta
                                                 
      real   (kind=RXP)                          :: xgnode
      real   (kind=RXP)                          :: ygnode
      real   (kind=RXP)                          :: zgnode
                                                
      integer(kind=IXP)                          :: ihexa
      integer(kind=IXP)                          :: k
      integer(kind=IXP)                          :: l
      integer(kind=IXP)                          :: m
      integer(kind=IXP)                          :: n
      integer(kind=IXP)                          :: igll
      integer(kind=IXP)                          :: ios
                                              
      integer(kind=I8), dimension(ig_ngll_total) :: maskeq
                                              
      !
      !
      !*************************************************************************************
      !compute global x,y,z-coordinates of gll nodes once and for all
      !*************************************************************************************

      ios = init_array_real(rg_gll_coordinate,ig_ngll_total,IG_NDOF,"rg_gll_coordinate")
   
      maskeq(:) = 0_I8 !-->coordinate not computed yet
      
      do ihexa = ONE_IXP,ig_nhexa

         do k = ONE_IXP,IG_NGLL        !zeta
            do l = ONE_IXP,IG_NGLL     !eta
               do m = ONE_IXP,IG_NGLL  !xi
   
                  igll = ig_hexa_gll_glonum(m,l,k,ihexa)
   
                  if (maskeq(igll) == 0_I8) then
   
                     do n = ONE_IXP,ig_hexa_nnode
   
                        lag_xi   = lagrap_geom(ig_hexa_gnode_xiloc(n),rg_gll_abscissa(m),ig_line_nnode)
                        lag_eta  = lagrap_geom(ig_hexa_gnode_etloc(n),rg_gll_abscissa(l),ig_line_nnode)
                        lag_zeta = lagrap_geom(ig_hexa_gnode_zeloc(n),rg_gll_abscissa(k),ig_line_nnode)
   
                        xgnode   = rg_gnode_x(ig_hexa_gnode_glonum(n,ihexa))
                        ygnode   = rg_gnode_y(ig_hexa_gnode_glonum(n,ihexa))
                        zgnode   = rg_gnode_z(ig_hexa_gnode_glonum(n,ihexa))
      
                        rg_gll_coordinate(ONE_IXP,igll) = rg_gll_coordinate(ONE_IXP,igll) + lag_xi*lag_eta*lag_zeta*xgnode
            
                        rg_gll_coordinate(TWO_IXP,igll) = rg_gll_coordinate(TWO_IXP,igll) + lag_xi*lag_eta*lag_zeta*ygnode
   
                        rg_gll_coordinate(THREE_IXP,igll) = rg_gll_coordinate(THREE_IXP,igll) + lag_xi*lag_eta*lag_zeta*zgnode
   
                     enddo
   
                  endif
   
                  maskeq(igll) = 1_I8
   
               enddo
            enddo
         enddo

      enddo
   
      if (LG_OUTPUT_DEBUG_FILE) then
         open(unit=100,file=trim(cg_prefix)//".global.element.gll.coordinates.cpu."//trim(cg_myrank)//".dbg")
   
         do ihexa = ONE_IXP,ig_nhexa
            do k = ONE_IXP,IG_NGLL
               do l = ONE_IXP,IG_NGLL
                  do m = ONE_IXP,IG_NGLL
   
                     igll = ig_hexa_gll_glonum(m,l,k,ihexa)
   
                     write(100,'(3(f15.3,1x),i10)') rg_gll_coordinate(ONE_IXP,igll)&
                                                   ,rg_gll_coordinate(TWO_IXP,igll)&
                                                   ,rg_gll_coordinate(THREE_IXP,igll)&
                                                   ,ig_hexa_gll_glonum(m,l,k,ihexa)
                  enddo
               enddo
            enddo
         enddo
   
         close(100)
      endif
   
      return
   
!***********************************************************************************************************************************************************************************
   end subroutine init_gll_nodes_coordinates
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine computes and assembles the mass matrix @f$M@f$ of the system @f$ M\ddot{U} + C\dot{U} + KU = F^{ext} @f$.
!>@return mod_global_variables::rg_gll_mass_matrix : diagonal mass matrix in cpu myrank
!***********************************************************************************************************************************************************************************
   subroutine init_mass_matrix()
!***********************************************************************************************************************************************************************************
      
      use mpi
      
      use mod_global_variables, only : &
                                       IG_NGLL&
                                      ,ig_ngll_total&
                                      ,rg_gll_mass_matrix&
                                      ,rg_gll_weight&
                                      ,ig_hexa_gll_glonum&
                                      ,ig_nhexa&
                                      ,rg_hexa_gll_jacobian_det&
                                      ,tg_cpu_neighbor&
                                      ,ig_myrank&
                                      ,rg_hexa_gll_rho&
                                      ,error_stop&
                                      ,ig_mpi_buffer_sizemax&
                                      ,ig_ncpu_neighbor&
                                      ,ig_mpi_comm_simu&
                                      ,IG_LST_UNIT

      use mod_init_memory
      
      implicit none

      real   (kind=RXP), dimension(ig_mpi_buffer_sizemax)                  :: dummyr
      real   (kind=RXP), dimension(ig_mpi_buffer_sizemax,ig_ncpu_neighbor) :: dlxmas
                                                 
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)                        :: statut
      integer(kind=IXP)                                                    :: ios
      integer(kind=IXP)                                                    :: ngll
      integer(kind=IXP)                                                    :: icon
      integer(kind=IXP)                                                    :: ihexa
      integer(kind=IXP)                                                    :: igll
      integer(kind=IXP)                                                    :: kgll
      integer(kind=IXP)                                                    :: lgll
      integer(kind=IXP)                                                    :: mgll
                                                                 
      if (ig_myrank == ZERO_IXP) then
         write(IG_LST_UNIT,'(" ",/,a)') "computing mass matrix..."
         call flush(IG_LST_UNIT)
      endif

      !
      !
      !*****************************************************************************************************************************************
      !allocate mass matrix array
      !*****************************************************************************************************************************************
      ios = init_array_real(rg_gll_mass_matrix,ig_ngll_total,"rg_gll_mass_matrix")

      !
      !
      !*****************************************************************************************************************************************
      !compute mass matrix for cpu 'myrank'
      !*****************************************************************************************************************************************
      do ihexa = ONE_IXP,ig_nhexa
         do kgll = ONE_IXP,IG_NGLL        !zeta
            do lgll = ONE_IXP,IG_NGLL     !eta
               do mgll = ONE_IXP,IG_NGLL  !xi
   
                  igll = ig_hexa_gll_glonum(mgll,lgll,kgll,ihexa)
   
                  rg_gll_mass_matrix(igll) = rg_gll_mass_matrix(igll)&
                                           + rg_hexa_gll_jacobian_det(mgll,lgll,kgll,ihexa)*rg_hexa_gll_rho(mgll,lgll,kgll,ihexa)&
                                           * rg_gll_weight(mgll)*rg_gll_weight(lgll)*rg_gll_weight(kgll)
               enddo
            enddo
         enddo
      enddo
      !
      !
      !*****************************************************************************************************************************************
      !fill buffer dlxmas with gll to be send
      !*****************************************************************************************************************************************
      do icon = ONE_IXP,ig_ncpu_neighbor
         do igll = ONE_IXP,tg_cpu_neighbor(icon)%ngll
   
            kgll = tg_cpu_neighbor(icon)%gll_send(igll)
            dlxmas(igll,icon) = rg_gll_mass_matrix(kgll)
   
         enddo
      enddo
      !
      !
      !*****************************************************************************************************************************************
      !assemble mass matrix on all cpus once and for all
      !*****************************************************************************************************************************************
      do icon = ONE_IXP,ig_ncpu_neighbor
      
         ngll = tg_cpu_neighbor(icon)%ngll
      
         call mpi_sendrecv(dlxmas(ONE_IXP,icon),ngll,MPI_REAL,tg_cpu_neighbor(icon)%icpu,100_IXP,dummyr,ngll,MPI_REAL,tg_cpu_neighbor(icon)%icpu,100_IXP,ig_mpi_comm_simu,statut,ios)
      
         do igll = ONE_IXP,ngll
   
            kgll = tg_cpu_neighbor(icon)%gll_recv(igll)
            rg_gll_mass_matrix(kgll) = rg_gll_mass_matrix(kgll) + dummyr(igll)
   
         enddo
      
      enddo
      !
      !
      !
      !******************************************************************************************************************
      !Compute the inverse of mass matrix
      !******************************************************************************************************************
      rg_gll_mass_matrix(:) = ONE_RXP/rg_gll_mass_matrix(:)
      
      if (ig_myrank == ZERO_IXP) then
         write(IG_LST_UNIT,'(a)') "done"
         call flush(IG_LST_UNIT)
      endif
      
!     deallocate(rg_hexa_gll_rho)

      return
      
!***********************************************************************************************************************************************************************************
   end subroutine init_mass_matrix
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine computes Jacobian matrix and its determinant for hexahedron elements.
!>@return rg_hexa_gll_jacobian_det : determinant of jacobian matrix at GLL nodes of hexahedron elements in cpu myrank
!>@return rg_hexa_gll_dxidx        : @f$ \frac{\partial \xi  }{\partial x} @f$ at GLL nodes of hexahedron elements
!>@return rg_hexa_gll_dxidy        : @f$ \frac{\partial \xi  }{\partial y} @f$ at GLL nodes of hexahedron elements
!>@return rg_hexa_gll_dxidz        : @f$ \frac{\partial \xi  }{\partial z} @f$ at GLL nodes of hexahedron elements
!>@return rg_hexa_gll_detdx        : @f$ \frac{\partial \eta }{\partial x} @f$ at GLL nodes of hexahedron elements 
!>@return rg_hexa_gll_detdy        : @f$ \frac{\partial \eta }{\partial y} @f$ at GLL nodes of hexahedron elements
!>@return rg_hexa_gll_detdz        : @f$ \frac{\partial \eta }{\partial z} @f$ at GLL nodes of hexahedron elements
!>@return rg_hexa_gll_dzedx        : @f$ \frac{\partial \zeta}{\partial x} @f$ at GLL nodes of hexahedron elements  
!>@return rg_hexa_gll_dzedy        : @f$ \frac{\partial \zeta}{\partial y} @f$ at GLL nodes of hexahedron elements
!>@return rg_hexa_gll_dzedz        : @f$ \frac{\partial \zeta}{\partial z} @f$ at GLL nodes of hexahedron elements
!***********************************************************************************************************************************************************************************
   subroutine init_jacobian_matrix_hexa()
!***********************************************************************************************************************************************************************************

      use mpi
   
      use mod_global_variables, only : IG_NGLL&
                                      ,rg_gll_abscissa&
                                      ,ig_nhexa&
                                      ,rg_hexa_gll_jacobian_det&
                                      ,rg_hexa_gll_dxidx&
                                      ,rg_hexa_gll_dxidy&
                                      ,rg_hexa_gll_dxidz&
                                      ,rg_hexa_gll_detdx&
                                      ,rg_hexa_gll_detdy&
                                      ,rg_hexa_gll_detdz&
                                      ,rg_hexa_gll_dzedx&
                                      ,rg_hexa_gll_dzedy&
                                      ,rg_hexa_gll_dzedz&
                                      ,ig_myrank&
                                      ,error_stop&
                                      ,ig_hexa_gnode_xiloc&
                                      ,ig_hexa_gnode_etloc&
                                      ,ig_hexa_gnode_zeloc&
                                      ,ig_line_nnode&
                                      ,rg_gnode_x&
                                      ,rg_gnode_y&
                                      ,rg_gnode_z&
                                      ,ig_hexa_nnode&
                                      ,ig_hexa_gnode_glonum&
                                      ,ig_mpi_comm_simu&
                                      ,IG_LST_UNIT

      use mod_init_memory

      use mod_jacobian

      use mod_lagrange      , only :&
                                    lagrap_geom&
                                   ,lagrad_geom

      implicit none
 
      real(kind=RXP),parameter :: EPS = 1.0e-8_RXP
      real(kind=RXP)           :: xi
      real(kind=RXP)           :: eta
      real(kind=RXP)           :: zeta
      real(kind=RXP)           :: dxidx 
      real(kind=RXP)           :: dxidy
      real(kind=RXP)           :: dxidz
      real(kind=RXP)           :: detdx
      real(kind=RXP)           :: detdy
      real(kind=RXP)           :: detdz
      real(kind=RXP)           :: dzedx
      real(kind=RXP)           :: dzedy
      real(kind=RXP)           :: dzedz
      real(kind=RXP)           :: det

      real(kind=RXP)           :: inv_det
      real(kind=RXP)           :: xxi
      real(kind=RXP)           :: xet
      real(kind=RXP)           :: xze
      real(kind=RXP)           :: yxi
      real(kind=RXP)           :: yet
      real(kind=RXP)           :: yze
      real(kind=RXP)           :: zxi
      real(kind=RXP)           :: zet
      real(kind=RXP)           :: zze
      real(kind=RXP)           :: lag_deriv_xi
      real(kind=RXP)           :: lag_deriv_et
      real(kind=RXP)           :: lag_deriv_ze
      real(kind=RXP)           :: lag_xi
      real(kind=RXP)           :: lag_et
      real(kind=RXP)           :: lag_ze
      real(kind=RXP)           :: xgnode
      real(kind=RXP)           :: ygnode
      real(kind=RXP)           :: zgnode
      
      integer(kind=IXP)        :: ihexa
      integer(kind=IXP)        :: k
      integer(kind=IXP)        :: l
      integer(kind=IXP)        :: m
      integer(kind=IXP)        :: ios
      integer(kind=IXP)        :: n



      if (ig_myrank == ZERO_IXP) then
         write(IG_LST_UNIT,'(" ",/,a)') "computing jacobian matrix in hexa..."
         call flush(IG_LST_UNIT)
      endif

      !
      !
      !********************************************************************************************************************
      !initialize memory
      !********************************************************************************************************************
      ios = init_array_real(rg_hexa_gll_jacobian_det,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_jacobian_det")

      ios = init_array_real(rg_hexa_gll_dxidx,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_dxidx")

      ios = init_array_real(rg_hexa_gll_dxidy,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_dxidy")

      ios = init_array_real(rg_hexa_gll_dxidz,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_dxidz")

      ios = init_array_real(rg_hexa_gll_detdx,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_detdx")

      ios = init_array_real(rg_hexa_gll_detdy,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_detdy")

      ios = init_array_real(rg_hexa_gll_detdz,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_detdz")

      ios = init_array_real(rg_hexa_gll_dzedx,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_dzedx")

      ios = init_array_real(rg_hexa_gll_dzedy,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_dzedy")

      ios = init_array_real(rg_hexa_gll_dzedz,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_dzedz")


      !
      !
      !********************************************************************************************************************
      !initialize jacobian matrix at each GLL nodes of each hexahedron elements
      !********************************************************************************************************************
      do ihexa = ONE_IXP,ig_nhexa
   
         do k = ONE_IXP,IG_NGLL
            do l = ONE_IXP,IG_NGLL
               do m = ONE_IXP,IG_NGLL
   
                  xi   = rg_gll_abscissa(m)
                  eta  = rg_gll_abscissa(l)
                  zeta = rg_gll_abscissa(k)

                 !this call has been replaced by the content of the subroutine compute_hexa_jacobian to obtain better cputime performance
                 !call compute_hexa_jacobian(ihexa,xi,eta,zeta,dxidx,dxidy,dxidz,detdx,detdy,detdz,dzedx,dzedy,dzedz,det)

!
!
!---------------->initialize partial derivatives

                  xxi = ZERO_RXP
                  xet = ZERO_RXP
                  xze = ZERO_RXP
                  yxi = ZERO_RXP
                  yet = ZERO_RXP
                  yze = ZERO_RXP
                  zxi = ZERO_RXP
                  zet = ZERO_RXP
                  zze = ZERO_RXP
               
!
!
!---------------->compute partial derivatives

                  do n = ONE_IXP,ig_hexa_nnode
               
                     lag_deriv_xi = lagrad_geom(ig_hexa_gnode_xiloc(n),xi  ,ig_line_nnode)
                     lag_deriv_et = lagrad_geom(ig_hexa_gnode_etloc(n),eta ,ig_line_nnode)
                     lag_deriv_ze = lagrad_geom(ig_hexa_gnode_zeloc(n),zeta,ig_line_nnode)
                     
                     lag_xi       = lagrap_geom(ig_hexa_gnode_xiloc(n),xi  ,ig_line_nnode)
                     lag_et       = lagrap_geom(ig_hexa_gnode_etloc(n),eta ,ig_line_nnode)
                     lag_ze       = lagrap_geom(ig_hexa_gnode_zeloc(n),zeta,ig_line_nnode)
                     
                     xgnode       = rg_gnode_x(ig_hexa_gnode_glonum(n,ihexa))
                     ygnode       = rg_gnode_y(ig_hexa_gnode_glonum(n,ihexa))
                     zgnode       = rg_gnode_z(ig_hexa_gnode_glonum(n,ihexa))
               
                     xxi          = xxi + lag_deriv_xi*lag_et*lag_ze*xgnode
                     yxi          = yxi + lag_deriv_xi*lag_et*lag_ze*ygnode
                     zxi          = zxi + lag_deriv_xi*lag_et*lag_ze*zgnode
                                  
                     xet          = xet + lag_xi*lag_deriv_et*lag_ze*xgnode
                     yet          = yet + lag_xi*lag_deriv_et*lag_ze*ygnode
                     zet          = zet + lag_xi*lag_deriv_et*lag_ze*zgnode
                                  
                     xze          = xze + lag_xi*lag_et*lag_deriv_ze*xgnode
                     yze          = yze + lag_xi*lag_et*lag_deriv_ze*ygnode
                     zze          = zze + lag_xi*lag_et*lag_deriv_ze*zgnode
               
                  enddo
!
!
!---------------->compute determinant and its inverse

                  det     = xxi*yet*zze - xxi*yze*zet + xet*yze*zxi - xet*yxi*zze + xze*yxi*zet - xze*yet*zxi
                  inv_det = ONE_RXP/det
               
                  if (det.lt.EPS) then
                     write(*,*) "***error in compute_hexa_jacobian: det null or negative, det = ",det
                     write(*,*) "***element ",ihexa, "cpu ",ig_myrank
                     call mpi_abort(ig_mpi_comm_simu,100_IXP,ios)
                     stop
                  endif
            
!
!
!---------------->compute partial derivatives

                  dxidx = (yet*zze-yze*zet)*inv_det
                  dxidy = (xze*zet-xet*zze)*inv_det
                  dxidz = (xet*yze-xze*yet)*inv_det
                  detdx = (yze*zxi-yxi*zze)*inv_det
                  detdy = (xxi*zze-xze*zxi)*inv_det
                  detdz = (xze*yxi-xxi*yze)*inv_det
                  dzedx = (yxi*zet-yet*zxi)*inv_det
                  dzedy = (xet*zxi-xxi*zet)*inv_det
                  dzedz = (xxi*yet-xet*yxi)*inv_det

!
!
!---------------->store local variables to global arrays

                  rg_hexa_gll_jacobian_det(m,l,k,ihexa) = det

                  rg_hexa_gll_dxidx(m,l,k,ihexa) = dxidx 
                  rg_hexa_gll_dxidy(m,l,k,ihexa) = dxidy
                  rg_hexa_gll_dxidz(m,l,k,ihexa) = dxidz

                  rg_hexa_gll_detdx(m,l,k,ihexa) = detdx
                  rg_hexa_gll_detdy(m,l,k,ihexa) = detdy
                  rg_hexa_gll_detdz(m,l,k,ihexa) = detdz

                  rg_hexa_gll_dzedx(m,l,k,ihexa) = dzedx
                  rg_hexa_gll_dzedy(m,l,k,ihexa) = dzedy
                  rg_hexa_gll_dzedz(m,l,k,ihexa) = dzedz
   
               enddo
            enddo
         enddo
   
      enddo

      if (ig_myrank == ZERO_IXP) then
         write(IG_LST_UNIT,'(a)') "done"
         call flush(IG_LST_UNIT)
      endif
   
      return
   
!***********************************************************************************************************************************************************************************
   end subroutine init_jacobian_matrix_hexa
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine computes the determinant of Jacobian matrix and normal unit vector of quadrangle elements.
!>@return rg_quadp_gll_jaco_det : determinant of jacobian matrix at GLL nodes of quadrangle elements in cpu myrank
!>@return rg_quadp_gll_normal   : normal unit vector at GLL nodes of quadrangle elements
!***********************************************************************************************************************************************************************************
   subroutine init_jacobian_matrix_quad()
!***********************************************************************************************************************************************************************************

      use mpi
   
      use mod_global_variables, only : IG_NGLL&
                                      ,rg_gll_abscissa&
                                      ,rg_gnode_x&
                                      ,rg_gnode_y&
                                      ,rg_gnode_z&
                                      ,ig_line_nnode&
                                      ,ig_quad_gnode_xiloc&
                                      ,ig_quad_gnode_etloc&
                                      ,ig_nquad_parax&
                                      ,rg_quadp_gll_jaco_det&
                                      ,rg_quadp_gll_normal&
                                      ,ig_quad_nnode&
                                      ,ig_myrank&
                                      ,error_stop&
                                      ,ig_quadp_gnode_glonum&
                                      ,ig_mpi_comm_simu&
                                      ,IG_LST_UNIT&
                                      ,lg_boundary_absorption

      use mod_init_memory

      use mod_lagrange      , only :&
                                    lagrap_geom&
                                   ,lagrad_geom

      implicit none
 
      real(kind=RXP),parameter    :: EPS = 1.0e-8_RXP
      real(kind=RXP)              :: xxi
      real(kind=RXP)              :: xet
      real(kind=RXP)              :: yxi
      real(kind=RXP)              :: yet
      real(kind=RXP)              :: zxi
      real(kind=RXP)              :: zet
      real(kind=RXP)              :: det
      real(kind=RXP)              :: inv_det
      real(kind=RXP)              :: crprx
      real(kind=RXP)              :: crpry
      real(kind=RXP)              :: crprz
      real(kind=RXP)              :: vn(THREE_IXP)
      real(kind=RXP)              :: lag_deriv_xi
      real(kind=RXP)              :: lag_deriv_et
      real(kind=RXP)              :: lag_xi
      real(kind=RXP)              :: lag_et
      real(kind=RXP)              :: xgnode
      real(kind=RXP)              :: ygnode
      real(kind=RXP)              :: zgnode
      
      integer(kind=IXP)           :: iquad
      integer(kind=IXP)           :: k
      integer(kind=IXP)           :: l
      integer(kind=IXP)           :: m
      integer(kind=IXP)           :: ios

      !
      !
      !*********************************************************************************************************************
      !if there are quad (eg, paraxial elt): compute jacobian and derivative of local coordinate with respect to global ones
      !*********************************************************************************************************************
   
      if (lg_boundary_absorption .and. ig_nquad_parax > ZERO_IXP) then

         if (ig_myrank == ZERO_IXP) then
            write(IG_LST_UNIT,'(" ",/,a)') "computing jacobian matrix in quad..."
            call flush(IG_LST_UNIT)
         endif

         !
         !
         !********************************************************************************************************************
         !initialize memory
         !********************************************************************************************************************
         ios = init_array_real(rg_quadp_gll_jaco_det,ig_nquad_parax,IG_NGLL,IG_NGLL,"rg_quadp_gll_jaco_det")

         ios = init_array_real(rg_quadp_gll_normal,ig_nquad_parax,IG_NGLL,IG_NGLL,THREE_IXP,"rg_quadp_gll_normal")
      

         !
         !
         !********************************************************************************************************************
         !initialize jacobian matrix and normal vector at each GLL nodes of each quadrangle elements
         !********************************************************************************************************************
         do iquad = ONE_IXP,ig_nquad_parax
            do k = ONE_IXP,IG_NGLL
               do l = ONE_IXP,IG_NGLL
   
                  xxi = ZERO_RXP
                  xet = ZERO_RXP
                  yxi = ZERO_RXP
                  yet = ZERO_RXP
                  zxi = ZERO_RXP
                  zet = ZERO_RXP
   
                  do m = ONE_IXP,ig_quad_nnode
   
                     lag_deriv_xi = lagrad_geom(ig_quad_gnode_xiloc(m),rg_gll_abscissa(l),ig_line_nnode)
                     lag_deriv_et = lagrad_geom(ig_quad_gnode_etloc(m),rg_gll_abscissa(k),ig_line_nnode)
   
                     lag_xi       = lagrap_geom(ig_quad_gnode_xiloc(m),rg_gll_abscissa(l),ig_line_nnode)
                     lag_et       = lagrap_geom(ig_quad_gnode_etloc(m),rg_gll_abscissa(k),ig_line_nnode)
   
                     xgnode       = rg_gnode_x(ig_quadp_gnode_glonum(m,iquad))
                     ygnode       = rg_gnode_y(ig_quadp_gnode_glonum(m,iquad))
                     zgnode       = rg_gnode_z(ig_quadp_gnode_glonum(m,iquad))
   
                     xxi          = xxi + lag_deriv_xi*lag_et*xgnode
                     yxi          = yxi + lag_deriv_xi*lag_et*ygnode
                     zxi          = zxi + lag_deriv_xi*lag_et*zgnode
                                  
                     xet          = xet + lag_xi*lag_deriv_et*xgnode
                     yet          = yet + lag_xi*lag_deriv_et*ygnode
                     zet          = zet + lag_xi*lag_deriv_et*zgnode
   
                  enddo
   
                  crprx   = yxi*zet - zxi*yet
                  crpry   = zxi*xet - xxi*zet
                  crprz   = xxi*yet - yxi*xet
   
                  det     = sqrt(crprx**TWO_IXP + crpry**TWO_IXP + crprz**TWO_IXP) !the determinant of the quad element is the magnitude of the cross-product
                  inv_det = ONE_RXP/det
   
                  if (det < EPS) then
                     write(*,*) "***error in jacobi quad: det null or negative, det = ",det
                     write(*,*) "***element ",iquad, "cpu ",ig_myrank
                     call mpi_abort(ig_mpi_comm_simu,100_IXP,ios)
                     stop
                  endif
!
!---------------->determinant of the quad element

                  rg_quadp_gll_jaco_det(l,k,iquad) = det
!
!---------------->unit vector normal to the quad at the node k,l

                  vn(ONE_IXP  ) = crprx*inv_det
                  vn(TWO_IXP  ) = crpry*inv_det
                  vn(THREE_IXP) = crprz*inv_det
   
                  rg_quadp_gll_normal(ONE_IXP,l,k,iquad)   = vn(ONE_IXP  ) !x
                  rg_quadp_gll_normal(TWO_IXP,l,k,iquad)   = vn(TWO_IXP  ) !y
                  rg_quadp_gll_normal(THREE_IXP,l,k,iquad) = vn(THREE_IXP) !z
      
               enddo
            enddo
         enddo

         if (ig_myrank == ZERO_IXP) then
            write(IG_LST_UNIT,'(a)') "done"
            call flush(IG_LST_UNIT)
         endif

      endif
   
      return
   
!***********************************************************************************************************************************************************************************
   end subroutine init_jacobian_matrix_quad
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine reads a buffer line that contains a real kind=RXP
!>@param  k : keyword of the parameter
!>@param  b : buffer containing values to be read
!>@param  c : coefficients of the uncertainty quantification experience plan
!>@return v : value read from buffer line (single simulation mode or uq mode)
!***********************************************************************************************************************************************************************************
   subroutine read_buffer(k,b,v,c)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only : nitems&
                                     , error_stop&
                                     , cg_efi_mode

      use mod_uq              , only : init_uq_simu_param

      implicit none

      character(len=100), intent( in)               :: k
      character(len=100), intent( in)               :: b
      real(kind=RXP)    , intent(out)               :: v
      real(kind=RXP)    , intent( in), dimension(:) :: c

      real   (kind=RXP)                             :: w
      integer(kind=IXP)                             :: i
      character(len=CIL)                            :: info

!
!---->single simulation mode or uq mode with certain parameter

      if ( nitems(b) == ONE_IXP ) then

         read(b,*) v

!
!---->uq mode

      elseif ( nitems(b) == THREE_IXP .and. .not.(cg_efi_mode == "ss") ) then

         read(b,*) v,w,i !min, max, colum number of file *.uqc

         call init_uq_simu_param(v,w,i,c)

      else

         write(info,'(a)') "error in subroutine module_init_efi:init_input_variables:read_buffer while reading keyword "//trim(k)//" in single simulation mode. Check in file *.cfg that only one velocity is defined for keyword "//trim(k)
         call error_stop(info)

      endif

      return
   
!***********************************************************************************************************************************************************************************
   end subroutine read_buffer
!***********************************************************************************************************************************************************************************

end module mod_init_efi
