!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains the module to define all global variables of EFISPEC3D.

!>@brief
!!This module defines all global variables of EFISPEC3D.
!!Scalar variables are initialized directly in this module.
!!This module also contains some general purpose subroutines.
module mod_global_variables
   
   use mpi

   use mod_precision
   
   implicit none

!
!
!***********************************************************************************************************************************************************************************
!rheology and medium parameters 
!***********************************************************************************************************************************************************************************

!>if .true. --> activate viscoelasticity in the solver module: see mod_solver
#if VISCO
   logical(kind=IXP), parameter :: LG_VISCO = .true.
#else
   logical(kind=IXP), parameter :: LG_VISCO = .false.
#endif

!>number of relaxation times and weights for viscoelastic stress-strain law (following liu and archuleta, 2006)
   integer(kind=IXP), parameter :: IG_NRELAX = 8_IXP

!>if .true. --> write medium in VTK format
   logical(kind=IXP), parameter :: LG_OUTPUT_MEDIUM_VTK = .false.

!
!
!***********************************************************************************************************************************************************************************
!snapshot parameters 
!***********************************************************************************************************************************************************************************

!>if .true. --> write surface snapshots in VTK format
   logical(kind=IXP), parameter :: LG_SNAPSHOT_VTK = .false.

!>if .true. --> write surface snapshots in gmt format
   logical(kind=IXP), parameter :: LG_SNAPSHOT_GMT = .true.

!>if .true. --> write surface snapshots at all GLL of free surface quadrangles
   logical(kind=IXP), parameter :: LG_SNAPSHOT_SURF_GLL = .false.

!>if .true. --> compute and output divergence and curl in the middle of free surface quadrangle
   logical(kind=IXP), parameter :: LG_SNAPSHOT_SURF_SPA_DER = .false.

!>if .true. --> activate runtime finite impulse response filtering and decimation of GLL of free surface quadrangles
   logical(kind=IXP), parameter :: LG_FIR_FILTER = .false.

!>decimation factor of the FIR filter (used if LG_FIR_FILTER = .true.)
   integer(kind=IXP), parameter :: IG_FIR_DECIMATION_FAC = 10_IXP

!>if .true. --> after computation is done, filter time histories saved at all GLL of free surface quadrangles
   logical(kind=IXP), parameter :: LG_SNAPSHOT_SURF_GLL_FILTER = .false.

!
!
!***********************************************************************************************************************************************************************************
!file system and writing parameters 
!***********************************************************************************************************************************************************************************

!>if .true. --> set 'striping_factor' and 'striping_unit' of free surface files (see mod_io_array::efi_mpi_file_open)
   logical(kind=IXP), parameter :: LG_LUSTRE_FILE_SYS = .false.

!>set how to write surface snapshots at all GLL of free surface quadrangles (see mod_snapshot_surface::write_snapshot_surface_gll)
   integer(kind=IXP), parameter :: IG_MPI_WRITE_TYPE = 2_IXP

!
!
!***********************************************************************************************************************************************************************************
!other parameters 
!***********************************************************************************************************************************************************************************

!>if .true. --> activate plane wave injection by specifing Dirichlet conditions at rg_plane_wave_z and by setting vertical boundary absorption elements to 1D wave propagation
   logical(kind=IXP), parameter :: LG_PLANE_WAVE = .false.

!>order of Lagrange's polynomials. Should be set from 4 to 6.
   integer(kind=IXP), parameter :: IG_LAGRANGE_ORDER = 4_IXP

!>number of GLL nodes in the reference domain [-1:1]
   integer(kind=IXP), parameter :: IG_NGLL = IG_LAGRANGE_ORDER + ONE_IXP

!>unit for listing file *.lst
   integer(kind=IXP), parameter :: IG_LST_UNIT = 10_IXP

!>number of degrees of freedom of the problem (i.e., 3)
   integer(kind=IXP), parameter :: IG_NDOF = THREE_IXP

!>RG_NEWMARK_GAMMA : gamma coefficient of Newmark method                                                                                                         
   real(kind=RXP), parameter :: RG_NEWMARK_GAMMA = 0.5_RXP

!>if .true. --> activate asynchrone MPI communications between cpu
   logical(kind=IXP), parameter :: LG_ASYNC_MPI_COMM = .false.

!>if .true. --> output cpu_time information in file *.time.cpu.*
   logical(kind=IXP), parameter :: LG_OUTPUT_CPUTIME = .false.

!>if .true. --> output debug files (*.dbg)
   logical(kind=IXP), parameter :: LG_OUTPUT_DEBUG_FILE = .false.

!
!
!***********************************************************************************************************************************************************************************
!variables (scalars, or n-order tensors)
!***********************************************************************************************************************************************************************************

!>jacobian determinant at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_jacobian_det

!>derivative of local coordinate (@f$\xi@f$) with respect to global coordinate @f$x@f$ at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_dxidx

!>derivative of local coordinate (@f$\xi@f$) with respect to global coordinate @f$y@f$ at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_dxidy

!>derivative of local coordinate (@f$\xi@f$) with respect to global coordinate @f$z@f$ at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_dxidz

!>derivative of local coordinate (@f$\eta@f$) with respect to global coordinate @f$x@f$ at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_detdx

!>derivative of local coordinate (@f$\eta@f$) with respect to global coordinate @f$y@f$ at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_detdy

!>derivative of local coordinate (@f$\eta@f$) with respect to global coordinate @f$z@f$ at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_detdz

!>derivative of local coordinate (@f$\zeta@f$) with respect to global coordinate @f$x@f$ at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_dzedx

!>derivative of local coordinate (@f$\zeta@f$) with respect to global coordinate @f$y@f$ at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_dzedy

!>derivative of local coordinate (@f$\zeta@f$) with respect to global coordinate @f$z@f$ at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_dzedz

!>density (@f$ \rho @f$) at GLL nodes for hexahedron elements in cpu myrank
   real(kind=RXP), dimension(:,:,:,:), allocatable :: rg_hexa_gll_rho

!>density times squared shear-wave velocity (@f$\rho \beta^{2}@f$) at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_rhovs2

!>density times squared pressure-wave velocity (@f$\rho \alpha^{2}@f$) at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), target, dimension(:,:,:,:), allocatable :: rg_hexa_gll_rhovp2

!>density times shear-wave velocity (@f$\rho \beta@f$) at GLL nodes of absorbing quadrangle elements in cpu myrank
   real(kind=RXP), dimension(:,:,:), allocatable :: rg_quadp_gll_rhovs

!>density times shear-wave velocity (@f$\rho \alpha@f$) at GLL nodes of absorbing quadrangle elements in cpu myrank
   real(kind=RXP), dimension(:,:,:), allocatable :: rg_quadp_gll_rhovp

!>jacobian determinant at GLL nodes of quadrangle elements in cpu myrank
   real(kind=RXP), dimension(:,:,:), allocatable :: rg_quadp_gll_jaco_det

!>vector normal to GLL nodes of quadrangle elements in x,y,z coordinate system in cpu myrank
   real(kind=RXP), dimension(:,:,:,:), allocatable :: rg_quadp_gll_normal

!>spatial derivative of displacement field at the middle gll of free surface quadrangle elements
   real(kind=RXP), dimension(:,:), allocatable :: rg_quadf_disp_spatial_deriv

!>weights @f$ w^{S}_{k} @f$ for a given quality factor @f$ Q_{S} @f$ at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), dimension(:,:,:,:,:), allocatable :: rg_hexa_gll_wkqs

!>weights @f$ w^{P}_{k} @f$ for a given quality factor @f$ Q_{P} @f$ at GLL nodes of hexahedron elements in cpu myrank
   real(kind=RXP), dimension(:,:,:,:,:), allocatable :: rg_hexa_gll_wkqp

!>memory variables @f$ ksi_{xx} @f$ at GLL nodes of hexahedron elements in cpu myrank used for viscoelastic simulation 
   real(kind=RXP), dimension(:,:,:,:,:), allocatable :: rg_hexa_gll_ksixx

!>memory variables @f$ ksi_{yy} @f$ at GLL nodes of hexahedron elements in cpu myrank used for viscoelastic simulation 
   real(kind=RXP), dimension(:,:,:,:,:), allocatable :: rg_hexa_gll_ksiyy

!>memory variables @f$ ksi_{zz} @f$ at GLL nodes of hexahedron elements in cpu myrank used for viscoelastic simulation 
   real(kind=RXP), dimension(:,:,:,:,:), allocatable :: rg_hexa_gll_ksizz

!>memory variables @f$ ksi_{xy} @f$ at GLL nodes of hexahedron elements in cpu myrank used for viscoelastic simulation 
   real(kind=RXP), dimension(:,:,:,:,:), allocatable :: rg_hexa_gll_ksixy

!>memory variables @f$ ksi_{xz} @f$ at GLL nodes of hexahedron elements in cpu myrank used for viscoelastic simulation 
   real(kind=RXP), dimension(:,:,:,:,:), allocatable :: rg_hexa_gll_ksixz

!>memory variables @f$ ksi_{yz} @f$ at GLL nodes of hexahedron elements in cpu myrank used for viscoelastic simulation 
   real(kind=RXP), dimension(:,:,:,:,:), allocatable :: rg_hexa_gll_ksiyz

!>coefficients for viscoelastic simulation
   real(kind=RXP), dimension(IG_NRELAX,THREE_IXP), parameter :: RG_RELAX_COEFF = reshape(       &
                                                                      (/                &
                                                                        1.72333e-3_RXP, & 
                                                                        1.80701e-3_RXP, &
                                                                        5.38887e-3_RXP, &
                                                                        1.99322e-2_RXP, &
                                                                        8.49833e-2_RXP, &
                                                                        4.09335e-1_RXP, &
                                                                        2.05951e+0_RXP, &
                                                                       13.26290e+0_RXP, &
                                                                        1.66958e-2_RXP, &
                                                                        3.81644e-2_RXP, &
                                                                        9.84666e-3_RXP, &
                                                                       -1.36803e-2_RXP, &
                                                                       -2.85125e-2_RXP, &
                                                                       -5.37309e-2_RXP, &
                                                                       -6.65035e-2_RXP, &
                                                                       -1.33696e-1_RXP, &
                                                                        8.98758e-2_RXP, &
                                                                        6.84635e-2_RXP, &
                                                                        9.67052e-2_RXP, &
                                                                        1.20172e-1_RXP, &
                                                                        1.30728e-1_RXP, &
                                                                        1.38746e-1_RXP, &
                                                                        1.40705e-1_RXP, &
                                                                        2.14647e-1_RXP  &
                                                                       /) , (/IG_NRELAX,THREE_IXP/) &
                                                                       )

!
!>exponential used for viscoelastic simulation
   real(kind=RXP), dimension(IG_NRELAX) :: rg_mem_var_exp

!>discrete times of the simulation. time step = mod_global_variables::rg_dt. number of time steps = mod_global_variables::ig_ndt
   real(kind=RXP), dimension(:), allocatable :: rg_simu_times

!>@f$x,y,z@f$ displacement at step n or n+1 at the GLL nodes (global numbering) in cpu myrank
   real(kind=RXP), target, dimension(:,:), allocatable :: rg_gll_displacement

!>@f$x,y,z@f$ velocity at step n or n+1 at the GLL nodes (global numbering) in cpu myrank
   real(kind=RXP), dimension(:,:), allocatable :: rg_gll_velocity

!>@f$x,y,z@f$ acceleration or external forces at step n+1 at the GLL nodes (global numbering) in cpu myrank
   real(kind=RXP), target, dimension(:,:), allocatable :: rg_gll_acceleration

!>@f$x,y,z@f$ acceleration or external forces at step n at the GLL nodes (global numbering) in cpu myrank
   real(kind=RXP), dimension(:,:), allocatable :: rg_gll_acctmp

!>local coordinate of geometric nodes in the reference domain [-1:1] 
   real(kind=RXP), dimension(:), allocatable :: rg_gnode_abscissa

!>denominator for computing lagrange polynomial for geometric nodes in the reference domain [-1:1]
   real(kind=RXP), dimension(:,:), allocatable :: rg_gnode_abscissa_dist

!>diagonal mass matrix at GLL nodes (global numbering) in cpu myrank
   real(kind=RXP), dimension(:), allocatable :: rg_gll_mass_matrix

!>@f$x@f$ coordinate of the geometric nodes in cpu myrank
   real(kind=RXP), dimension(:), allocatable :: rg_gnode_x

!>@f$y@f$ coordinate of the geometric nodes in cpu myrank
   real(kind=RXP), dimension(:), allocatable :: rg_gnode_y

!>@f$z@f$ coordinate of the geometric nodes in cpu myrank
   real(kind=RXP), dimension(:), allocatable :: rg_gnode_z

!>@f$z@f$ coordinate of all receivers used for snapshot (only in cpu0)
   real(kind=RXP), dimension(:), allocatable :: rg_receiver_snapshot_z

!>local number of receiver used for snapshot
   integer(kind=IXP), dimension(:), allocatable :: ig_receiver_snapshot_locnum

!>user defined source function for double couple source
   real(kind=RXP), dimension(:), allocatable :: rg_dcsource_user_func

!>user defined source function for single force point source
   real(kind=RXP), dimension(:), allocatable :: rg_sfsource_user_func

!>send buffer for asynchrone communications
   real(kind=RXP), dimension(:), allocatable :: rg_mpi_buffer_send

!>receive buffer for asynchrone communications
   real(kind=RXP), dimension(:), allocatable :: rg_mpi_buffer_recv

!>time step increment @f$ \Delta t @f$
   real(kind=RXP):: rg_dt = ZERO_RXP

!>squared time step increment @f$ \Delta t^{2} @f$
   real(kind=RXP):: rg_dt2 = ZERO_RXP

!>current time of simulation @f$ t_{simu} @f$ (i.e. rg_dt*ig_idt)
   real(kind=RXP):: rg_simu_current_time = ZERO_RXP

!>total time of simulation @f$ tt_{simu} @f$
   real(kind=RXP):: rg_simu_total_time = ZERO_RXP

!>maximum frequency of the simulation (used to check nb of grid points per wavelength and avoid numerical dispersion)
   real(kind=RXP):: rg_fmax_simu = ZERO_RXP

!>maximum @f$x@f$ coordinate of the physical domain of study (over all cpus)
   real(kind=RXP):: rg_mesh_xmax = ZERO_RXP

!>minimum @f$x@f$ coordinate of the physical domain of study (over all cpus) 
   real(kind=RXP):: rg_mesh_xmin = ZERO_RXP

!>maximum @f$y@f$ coordinate of the physical domain of study (over all cpus)
   real(kind=RXP):: rg_mesh_ymax = ZERO_RXP

!>minimum @f$y@f$ coordinate of the physical domain of study (over all cpus)
   real(kind=RXP):: rg_mesh_ymin = ZERO_RXP

!>maximum @f$z@f$ coordinate of the physical domain of study (over all cpus)
   real(kind=RXP):: rg_mesh_zmax = ZERO_RXP

!>minimum @f$z@f$ coordinate of the physical domain of study (over all cpus)
   real(kind=RXP):: rg_mesh_zmin = ZERO_RXP

!>space increment of the grid of receivers for snapshot given in the configuration file *.cfg
   real(kind=RXP):: rg_receiver_snapshot_dxdy = ZERO_RXP

!>space increment @f$dx@f$ of the grid of receivers for snapshot
   real(kind=RXP):: rg_receiver_snapshot_dx   = ZERO_RXP

!>space increment @f$dy@f$ of the grid of receivers for snapshot
   real(kind=RXP):: rg_receiver_snapshot_dy   = ZERO_RXP

!>number of receivers for snapshot in the @f$x@f$-direction
   integer(kind=IXP):: ig_receiver_snapshot_nx = ZERO_IXP

!>number of receivers for snapshot in the @f$y@f$-direction
   integer(kind=IXP):: ig_receiver_snapshot_ny = ZERO_IXP

!>global GLL nodes of hexahedron elements from local gll nodes
   integer(kind=IXP), target, dimension(:,:,:,:), allocatable :: ig_hexa_gll_glonum

!>global GLL nodes of paraxial quadrangle elements from local gll nodes
   integer(kind=IXP), dimension(:,:,:), allocatable :: ig_quadp_gll_glonum

!>global GLL nodes of free surface quadrangle elements from local gll nodes
   integer(kind=IXP), dimension(:,:,:), allocatable :: ig_quadf_gll_glonum

!>global all GLL nodes ordered by free surface quadrangle used for snapshot
   integer(kind=IXP), dimension(:), allocatable :: ig_gll_order_by_quadf

!>global geometric nodes of hexahedron elements in cpu myrank 
   integer(kind=IXP), dimension(:,:), allocatable :: ig_hexa_gnode_glonum

!>global geometric nodes of paraxial quadrangle elements in cpu myrank
   integer(kind=IXP), dimension(:,:), allocatable :: ig_quadp_gnode_glonum

!>global geometric nodes of free surface quadrangle elements in cpu myrank
   integer(kind=IXP), dimension(:,:), allocatable :: ig_quadf_gnode_glonum

!>local position @f$ \xi @f$ of geometric nodes in hexahedron elements
   integer(kind=IXP), dimension(:), allocatable :: ig_hexa_gnode_xiloc

!>local position @f$ \eta @f$ of geometric nodes in hexahedron elements
   integer(kind=IXP), dimension(:), allocatable :: ig_hexa_gnode_etloc

!>local position @f$ \zeta @f$ of geometric nodes in hexahedron elements
   integer(kind=IXP), dimension(:), allocatable :: ig_hexa_gnode_zeloc

!>local position @f$ \xi @f$ of geometric nodes in quadrangle elements
   integer(kind=IXP), dimension(:), allocatable :: ig_quad_gnode_xiloc

!>local position @f$ \eta @f$ of geometric nodes in quadrangle elements
   integer(kind=IXP), dimension(:), allocatable :: ig_quad_gnode_etloc

!>fortran unit number of receivers in hexahedron elements in cpu myrank
   integer(kind=IXP), dimension(:), allocatable :: ig_hexa_receiver_unit

!>fortran unit number of receivers in quadrangle elements in cpu myrank
   integer(kind=IXP), dimension(:), allocatable :: ig_quad_receiver_unit

!>fortran unit number of file used to store displacement at all GLL of free surface quadrangle elements
   integer(kind=IXP):: ig_unit_snap_quad_gll_dis = ZERO_IXP

!>fortran unit number of file used to store spatial derivative of displacement at the midGLL of free surface quadrangle elements
   integer(kind=IXP):: ig_unit_snap_quadf_dis_spa_der = ZERO_IXP

!>maximum number of GLL nodes in all mpi buffers for cpu myrank
   integer(kind=IXP):: ig_mpi_buffer_sizemax = ZERO_IXP

!>size in octect of MPI_REAL
   integer(kind=IXP):: ig_mpi_nboctet_real = ZERO_IXP

!>size in octect of mpi_integer
   integer(kind=IXP):: ig_mpi_nboctet_int = ZERO_IXP

!>position of writing in mpi_file snapshot surface gll
   integer(kind=IXP):: ig_pos_cpu_snap_surf_gll = ZERO_IXP

!>mpi send request for asynchrone communications
   integer(kind=IXP), dimension(:), allocatable :: ig_mpi_request_send

!>mpi receive request for asynchrone communications
   integer(kind=IXP), dimension(:), allocatable :: ig_mpi_request_recv

!>offset buffer for asynchrone communications
   integer(kind=IXP), dimension(:), allocatable :: ig_mpi_buffer_offset 

!>material number of hexahedron elements
   integer(kind=IXP), dimension(:), allocatable :: ig_hexa_material_number

!>global numbering of all receivers of all cpus to write *.grd files (gathered by cpu0 only)
   integer(kind=IXP), dimension(:), allocatable :: ig_receiver_snapshot_glonum

!>shift for gatherv for snapshot receivers
   integer(kind=IXP), dimension(:), allocatable :: ig_receiver_snapshot_mpi_shift

!>number of receivers in each cpu (gathered by cpu0 only)
   integer(kind=IXP), dimension(:), allocatable :: ig_receiver_snapshot_total_number

!>total number of cpu used for computation in communicator MPI_COMM_WORLD
   integer(kind=IXP):: ig_ncpu_world = ZERO_IXP

!>rank of the cpu myrank in communicator MPI_COMM_WORLD
   integer(kind=IXP):: ig_myrank_world = ZERO_IXP

!>total number of cpu used for computation in communicator MPI_COMM_SIMU
   integer(kind=IXP):: ig_ncpu = ZERO_IXP

!>rank of the cpu myrank in communicator MPI_COMM_SIMU
   integer(kind=IXP):: ig_myrank = ZERO_IXP

!>number of cpus neighbor to cpu myrank
   integer(kind=IXP):: ig_ncpu_neighbor = ZERO_IXP

!>info about cpus neighbor to cpu myrank: ig_cpu_neighbor_info(ONE_IXP,:) --> element number, ig_cpu_neighbor_info(TWO_IXP,:) --> face number, ig_cpu_neighbor_info(THREE_IXP,:) --> cpu number
   integer(kind=IXP), dimension(:,:), allocatable :: ig_cpu_neighbor_info

!>number of hexahedron elements in cpu myrank
   integer(kind=IXP):: ig_nhexa = ZERO_IXP

!>number of outer hexahedron elements in cpu myrank
   integer(kind=IXP):: ig_nhexa_outer = ZERO_IXP

!>number of inner hexahedron elements in cpu myrank
   integer(kind=IXP):: ig_nhexa_inner = ZERO_IXP

!>number of paraxial quadrangle elements in cpu myrank
   integer(kind=IXP):: ig_nquad_parax = ZERO_IXP

!>number of free surface quadrangle elements in cpu myrank
   integer(kind=IXP):: ig_nquad_fsurf = ZERO_IXP

!>number of free surface quadrangle elements in cpu myrank saved for snapshot gll
   integer(kind=IXP):: ig_nquad_fsurf_saved = ZERO_IXP

!>number of free surface quadrangle elements in all cpus
   integer(kind=IXP):: ig_nquad_fsurf_all_cpu = ZERO_IXP

!>total number of geometric nodes in cpu myrank
   integer(kind=IXP):: ig_mesh_nnode = ZERO_IXP

!>number of geometric nodes to define a hexahedron element
   integer(kind=IXP):: ig_hexa_nnode = ZERO_IXP

!>number of geometric nodes to define a quadrangle element
   integer(kind=IXP):: ig_quad_nnode = ZERO_IXP

!>number of geometric nodes to define a line element
   integer(kind=IXP):: ig_line_nnode = ZERO_IXP

!>indirection from hexahedron geometrical nodes to local GLL nodes (only for vertex nodes)
   integer(kind=IXP), dimension(3_IXP,8_IXP) :: ig_hexa_node2gll

!>indirection from hexahedron face number to the GLL located in the middle of this face
   integer(kind=IXP), dimension(IG_NDOF,SIX_IXP) :: ig_hexa_face2mid_gll

!>indirection from face's node indices to hexa's node indices
   integer(kind=IXP), dimension(4_IXP,6_IXP) :: ig_hexa_face_node

!>total number of time steps of the analysis
   integer(kind=IXP):: ig_ndt = ZERO_IXP

!>current time step of analysis
   integer(kind=IXP):: ig_idt = ZERO_IXP

!>saving increment for receivers time history
   integer(kind=IXP):: ig_receiver_saving_incr = ONE_IXP

!>if .true. :write receiver 1d velocity structure
   logical(kind=IXP):: lg_receiver_1d_velocity_structure = .false.

!>vertical spatial step  (delta z) used to write receiver 1d velocity structure
   real(kind=RXP):: rg_receiver_1d_velocity_structure_dz = HUGE(ZERO_RXP)

!>saving increment for free surface grid snapshot
   integer(kind=IXP):: ig_snapshot_saving_incr = ZERO_IXP 

!>saving increment for volume snapshot
   integer(kind=IXP):: ig_snapshot_volume_saving_incr = ZERO_IXP 

!>total number of time steps saved for free surface quadrangle snapshot (used if LG_SNAPSHOT_SURF_GLL = .true.)
   integer(kind=IXP):: ig_snapshot_surf_gll_nsave = ZERO_IXP

!>if .true. --> boundary absorption is activated
   logical(kind=IXP) :: lg_boundary_absorption = .true.

!>number of double couple point sources in cpu myrank 
   integer(kind=IXP):: ig_ndcsource = ZERO_IXP

!>number of single force point sources in cpu myrank
   integer(kind=IXP):: ig_nsfsource = ZERO_IXP

!>number of faults in the domain of study
   integer(kind=IXP):: ig_nfault = ZERO_IXP

!>number of receivers in hexahedron elements in cpu myrank
   integer(kind=IXP):: ig_nreceiver_hexa = ZERO_IXP

!>number of receivers in quadrangle elements in cpu myrank
   integer(kind=IXP):: ig_nreceiver_quad = ZERO_IXP

!>number of receivers in quadrangle elements for free surface snapshot in cpu myrank
   integer(kind=IXP):: ig_nreceiver_snapshot = ZERO_IXP

!>number of GLL in array ig_gll_order_by_quadf
   integer(kind=IXP):: ig_ngll_order_by_quadf = ZERO_IXP

!>number of materials used to defined the medium
   integer(kind=IXP):: ig_nmaterial = ZERO_IXP

!>number of layers used to defined the medium
   integer(kind=IXP):: ig_nlayer = ZERO_IXP

!>number of interfaces used to defined the medium (=ig_nlayer*2-1 if medium is defined by layers)
   integer(kind=IXP):: ig_ninterface = ZERO_IXP

!>if .true. --> linear gradient within layers is activated
   logical(kind=IXP) :: lg_layer_gradient = .false.

!>return .true. if one of the material is defined by random porperties
   logical(kind=IXP) :: lg_layer_random = .false.

!>if .true. --> user defined layer properties
   logical(kind=IXP) :: lg_layer_userdefined = .false.

!>hexahedron element number attached to paraxial quadrangle elements
   integer(kind=IXP), dimension(:), allocatable :: ig_quadp_neighbor_hexa

!>face number of hexahedron element attached to paraxial quadrangle elements
   integer(kind=IXP), dimension(:), allocatable :: ig_quadp_neighbor_hexaface

!>hexahedron element number attached to free surface quadrangle elements
   integer(kind=IXP), dimension(:), allocatable :: ig_quadf_neighbor_hexa

!>face number of hexahedron element attached to free surface quadrangle elements
   integer(kind=IXP), dimension(:), allocatable :: ig_quadf_neighbor_hexaface

!>number of neighbors (either face, edge and node) between cpu myrank and its neighbors
   integer(kind=IXP):: ig_nneighbor_all_kind = ZERO_IXP

!>length of the name of cpu myrank
   integer(kind=IXP):: ig_cpu_name_len = ZERO_IXP

!>name of cpu myrank
   character(len=MPI_MAX_PROCESSOR_NAME) :: cg_cpu_name

!>prefix of all input files
   character(len=CIL) :: cg_prefix

!>myrank number inside WORLD COMMUNICATOR written in character format
   character(len= 6) :: cg_myrank_world

!>myrank number inside SIMU COMMUNICATOR written in character format
   character(len= 6) :: cg_myrank

!>total number of cpu inside WORLD COMMUNICATOR written in character format
   character(len= 6) :: cg_ncpu_world

!>total number of cpu inside SIMU COMMUNICATOR written in character format
   character(len= 6) :: cg_ncpu

!>if .true. --> snapshot of free surface is output either in gmt or VTK format
   logical(kind=IXP) :: lg_snapshot = .false.

!>if .true. --> activation of displacement free surface snapshot
   logical(kind=IXP) :: lg_snapshot_displacement = .false.

!>if .true. --> activation of velocity free surface snapshot
   logical(kind=IXP) :: lg_snapshot_velocity = .false.

!>if .true. --> activation of acceleration free surface snapshot
   logical(kind=IXP) :: lg_snapshot_acceleration = .false.

!>if .true. --> snapshot of volume is output either in VTK format
   logical(kind=IXP) :: lg_snapshot_volume = .false.

!>if .true. --> activation of displacement volume snapshot
   logical(kind=IXP) :: lg_snapshot_volume_displacement = .false.

!>if .true. --> activation of velocity volume snapshot
   logical(kind=IXP) :: lg_snapshot_volume_velocity = .false.

!>if .true. --> activation of acceleration volume snapshot
   logical(kind=IXP) :: lg_snapshot_volume_acceleration = .false.

!>GLL nodes @f$x,y,z@f$coordinates in cpu myrank
   real(kind=RXP), dimension(:,:), allocatable :: rg_gll_coordinate

!>weight of GLL nodes for numerical integration
   real(kind=RXP), target, dimension(IG_NGLL) :: rg_gll_weight

!>abscissa of GLL nodes in reference domain [-1:1]
   real(kind=RXP), dimension(IG_NGLL) :: rg_gll_abscissa

!>derivative of lagrange polynomial of node p at GLL node i in [-1:1] --> rg_gll_lagrange_deriv(p,i)
   real(kind=RXP), target, dimension(IG_NGLL,IG_NGLL) :: rg_gll_lagrange_deriv

!>denominator for computing lagrange polynomial for GLL nodes in the reference domain [-1:1]
   real(kind=RXP), dimension(IG_NGLL,IG_NGLL) :: rg_gll_abscissa_dist

!>total number of unique global GLL nodes in cpu myrank
   integer(kind=IXP):: ig_ngll_total = ZERO_IXP

!>EFISPEC's hexahedron element used for volume snapshot
   integer(kind=IXP), dimension(:), allocatable :: ig_hexa_snapshot

!>VTK's hexahedron element connectivity used for volume snapshot
   integer(kind=IXP), dimension(:), allocatable :: ig_vtk_hexa_conn_snapshot

!>Indirection array used for volume snapshot: VTK's geometric nodes to EFISPEC's GLL nodes
   integer(kind=IXP), dimension(:), allocatable :: ig_vtk_node_gll_glonum_snapshot

!>@f$x@f$-coordinate of VTK mesh nodes used for volume snapshot
   real(kind=RXP), dimension(:), allocatable :: rg_vtk_node_x_snapshot

!>@f$y@f$-coordinate of VTK mesh nodes used for volume snapshot
   real(kind=RXP), dimension(:), allocatable :: rg_vtk_node_y_snapshot

!>@f$z@f$-coordinate of VTK mesh nodes used for volume snapshot
   real(kind=RXP), dimension(:), allocatable :: rg_vtk_node_z_snapshot

!>VTK's hexahedron element cell type used for volume snapshot
   integer(kind=I8), dimension(:), allocatable :: ig_vtk_cell_type_snapshot

!>VTK's hexahedron element offset used for volume snapshot
   integer(kind=IXP), dimension(:), allocatable :: ig_vtk_offset_snapshot

!>number of hexahadron element in VTK mesh 
   integer(kind=IXP):: ig_vtk_nhexa_snapshot = ZERO_IXP

!>number of geometric nodes in VTK mesh 
   integer(kind=IXP):: ig_vtk_nnode_snapshot = ZERO_IXP

!>minimal @f$x@f$-coordinate of the box of volume snapshot
   real(kind=RXP):: rg_snapshot_volume_xmin = ZERO_RXP

!>maximal @f$x@f$-coordinate of the box of volume snapshot
   real(kind=RXP):: rg_snapshot_volume_xmax = ZERO_RXP

!>minimal @f$y@f$-coordinate of the box of volume snapshot
   real(kind=RXP):: rg_snapshot_volume_ymin = ZERO_RXP

!>maximal @f$y@f$-coordinate of the box of volume snapshot
   real(kind=RXP):: rg_snapshot_volume_ymax = ZERO_RXP

!>minimal @f$z@f$-coordinate of the box of volume snapshot
   real(kind=RXP):: rg_snapshot_volume_zmin = ZERO_RXP

!>maximal @f$z@f$-coordinate of the box of volume snapshot
   real(kind=RXP):: rg_snapshot_volume_zmax = ZERO_RXP

!>cutoff frequency (cycle/sec) of the infinite impulse response filter. cutoff frequency is in the middle of the transition band (used if LG_SNAPSHOT_SURF_GLL_FILTER = .true.)
   real(kind=RXP):: rg_iir_filter_cutoff_freq = ZERO_RXP

!>transition band (cycle/sec) of the infinite impulse response filter (used if LG_SNAPSHOT_SURF_GLL_FILTER = .true.)
   real(kind=RXP):: rg_iir_filter_transition_band = ZERO_RXP

!>output motion: dis = displacement, vel = velocity, acc = acceleration (used if LG_SNAPSHOT_SURF_GLL_FILTER = .true.)
   character(len=3_IXP):: cg_iir_filter_output_motion = "vel"

!>cutoff frequency (cycle/sec) of the finite impulse response filter. cutoff frequency is in the middle of the transition band (used if LG_FIR_FILTER = .true.)
   real(kind=RXP):: rg_fir_filter_cutoff_freq = ZERO_RXP

!>transition band (cycle/sec) of the finite impulse response filter (used if LG_FIR_FILTER = .true.)
   real(kind=RXP):: rg_fir_filter_transition_band = ZERO_RXP

!>attenuation (positive decibels) of the finite impulse response filter. It is the peak sidelobe of the stopband ripple of the fir filter (used if LG_FIR_FILTER = .true.)
   real(kind=RXP):: rg_fir_filter_attenuation = ZERO_RXP

!>time step after fir filter and decimation (used if LG_FIR_FILTER = .true.)
   real(kind=RXP):: rg_fir_dt = ZERO_RXP

!>order of finite impluse response filter (used if LG_FIR_FILTER = .true.)
   integer(kind=IXP):: ig_fir_filter_order = ZERO_IXP

!>finite impluse response filter (used if LG_FIR_FILTER = .true.)
   real(kind=RXP), dimension(:), allocatable :: rg_fir_filter

!>circular buffer containing @f$x,y,z@f displacement used when the finite impluse response filter is applied over the time histories (used if LG_FIR_FILTER = .true.)
   real(kind=RXP), dimension(:,:,:), allocatable :: rg_fir_filter_buffer_dis

!>circular buffer containing @f$x,y,z@f velocity used when the finite impluse response filter is applied over the time histories (used if LG_FIR_FILTER = .true.)
   real(kind=RXP), dimension(:,:,:), allocatable :: rg_fir_filter_buffer_vel

!>circular buffer containing @f$x,y,z@f acceleration used when the finite impluse response filter is applied over the time histories (used if LG_FIR_FILTER = .true.)
   real(kind=RXP), dimension(:,:,:), allocatable :: rg_fir_filter_buffer_acc

!>circular buffer containing divergence of displacement used when the finite impluse response filter is applied over the time histories (used if LG_FIR_FILTER = .true. and LG_SNAPSHOT_SURF_SPA_DER = .true.)
   real(kind=RXP), dimension(:,:), allocatable :: rg_fir_filter_buffer_dis_div

!>circular buffer containing curl of displacement used when the finite impluse response filter is applied over the time histories (used if LG_FIR_FILTER = .true. and LG_SNAPSHOT_SURF_SPA_DER = .true.)
   real(kind=RXP), dimension(:,:,:), allocatable :: rg_fir_filter_buffer_dis_spa_der

!>@f$z@f$ = constant plane where the plane wave is injected (used if LG_PLANE_WAVE = .true.)
   real(kind=RXP):: rg_plane_wave_z = ZERO_RXP

!>source time duration during which the plane wave is injected (used if LG_PLANE_WAVE = .true.)
   real(kind=RXP):: rg_plane_wave_std = ZERO_RXP

!>direction of motion of the plane wave (used if LG_PLANE_WAVE = .true.)
   integer(kind=IXP):: ig_plane_wave_dir = ZERO_IXP

!>total number of gll nodes in cpu 'myrank' located at mod_global_variables::rg_plane_wave_z (used if LG_PLANE_WAVE = .true.)
   integer(kind=IXP):: ig_plane_wave_ngll = ZERO_IXP

!>gll nodes' number in cpu 'myrank' where the plane wave is injected (used if LG_PLANE_WAVE = .true.)
   integer(kind=IXP), dimension(:), allocatable :: ig_plane_wave_gll

!>total number of bottom-most paraxial quadrangle elements in cpu 'myrank' (used if LG_PLANE_WAVE = .true.)
   integer(kind=IXP):: ig_plane_wave_nquadp_bottom = ZERO_IXP

!>bottom-most paraxial quadrangle elements in cpu 'myrank' used to absorb waves at any time during the simulation (used if LG_PLANE_WAVE = .true.)
   integer(kind=IXP), dimension(:), allocatable :: ig_plane_wave_quadp_bottom

!>total number of domain-side paraxial quadrangle elements in cpu 'myrank' (used if LG_PLANE_WAVE = .true.)
   integer(kind=IXP):: ig_plane_wave_nquadp_side = ZERO_IXP

!>domain-side paraxial quadrangle elements in cpu 'myrank' used to impose dirichlet conditions (used if LG_PLANE_WAVE = .true.)
   integer(kind=IXP), dimension(:), allocatable :: ig_plane_wave_quadp_side

!>user source time function of the plane wave (used if LG_PLANE_WAVE = .true.)
   real(kind=RXP), dimension(:), allocatable :: rg_plane_wave_sft_user

!>local mpi communicator for cpu that computes free surface quadrangle elements (used if LG_SNAPSHOT_SURF_GLL = .true.)
   integer(kind=IXP):: ig_mpi_comm_fsurf = ZERO_IXP

!>total number of cpu used in communicator ig_mpi_comm_fsurf (used if LG_SNAPSHOT_SURF_GLL = .true.)
   integer(kind=IXP):: ig_ncpu_fsurf = ZERO_IXP

!>rank of the cpu in communicator ig_mpi_comm_fsurf (used if LG_SNAPSHOT_SURF_GLL = .true.)
   integer(kind=IXP):: ig_myrank_fsurf = ZERO_IXP

!>type for writing free surface snapshot (ordered by quad-gll-component-time) (used if LG_SNAPSHOT_SURF_GLL = .true.)
   integer(kind=IXP):: ig_type_vector_snap_fsurf

!>simulation number of the experience plan (use in case of multiple simulations based on Quasi Monte-Carlo or Polynomial Chaos experience plan)
   real(kind=RXP), allocatable, dimension(:) :: rg_uq_coeff

!>simulation number of the experience plan (use in case of multiple simulations based on Quasi Monte-Carlo or Polynomial Chaos experience plan)
   integer(kind=IXP):: ig_uq_simu = ZERO_IXP

!>uncertainy quantification simulation number in character
   character(len=6) :: cg_uq_simu

!>number of uncertain parameters in the experience plan
   integer(kind=IXP):: ig_uq_ndim = ZERO_IXP

!>local communicator for a simulation
   integer(kind=IXP) :: ig_mpi_comm_simu = ZERO_IXP

!>simulation number
   integer(kind=IXP) :: ig_simu = ZERO_IXP

!>total number of simulations
   integer(kind=IXP) :: ig_nsimu = ZERO_IXP

!>efispec simulation mode (ss = single simulation, uqss = uncertainty quantification single simulation, uqms = uncertainty quantification multi simulations, post = post-processing)
   character(len=4) :: cg_efi_mode = "ss"

!>simulation number in character
   character(len=6) :: cg_simu

!>total number of simulations written in character
   character(len=6) :: cg_nsimu

!>directory of different simulations
   character(len=CIL) :: cg_simu_dir = "./"

!>default listing file extension. can be changed before the call of subroutine mod_write_listing::write_header
   character(len=CIL) :: cg_lst_ext = ".lst"

!
!
!***********************************************************************************************************************************************************************************
!3D points
!***********************************************************************************************************************************************************************************

!>type for points in the three-dimensional coordinate system of the domain of simulation
   type point3d

      real   (kind=RXP) :: x    !<@f$x@f$ coordinate
      real   (kind=RXP) :: y    !<@f$y@f$ coordinate
      real   (kind=RXP) :: z    !<@f$z@f$ coordinate
      real   (kind=RXP) :: xi   !<local coordinate @f$\xi@f$ in hexahedron element 'iel'
      real   (kind=RXP) :: et   !<local coordinate @f$\eta@f$ in hexahedron element 'iel'
      real   (kind=RXP) :: ze   !<local coordinate @f$\zeta@f$ in hexahedron element 'iel'
      real   (kind=RXP) :: dmin !<minimal distance of point source to a GLL node
      integer(kind=IXP) :: cpu  !<cpu which contains the point
      integer(kind=IXP) :: iel  !<hexahedron element which contains the point
      integer(kind=IXP) :: kgll !<closest local GLL node in the @f$\zeta@f$-direction
      integer(kind=IXP) :: lgll !<closest local GLL node in @f$\eta@f$-direction
      integer(kind=IXP) :: mgll !<closest local GLL node in @f$\xi@f$-direction

   end type

!
!
!***********************************************************************************************************************************************************************************
!single force point sources
!***********************************************************************************************************************************************************************************

!>type for single force point sources
   type type_single_force_source 

      real   (kind=RXP) :: x          !<@f$x@f$ coordinate of single force point source
      real   (kind=RXP) :: y          !<@f$y@f$ coordinate of single force point source
      real   (kind=RXP) :: z          !<@f$z@f$ coordinate of single force point source
      real   (kind=RXP) :: fac        !<factor on single force point source function
      real   (kind=RXP) :: rise_time  !<rise time of source function
      real   (kind=RXP) :: shift_time !<time shift of source function
      real   (kind=RXP) :: var1       !<other variable needed by some functions
      real   (kind=RXP) :: dmin       !<minimal distance of point source to a GLL node
      integer(kind=IXP) :: icur       !<function's number associated to single force source. See mod_source_function to see the definition of functions' number.
      integer(kind=IXP) :: idir       !<direction (@f$x@f$=1, @f$y@f$=2, @f$z@f$=3) in which the force is applied
      integer(kind=IXP) :: cpu        !<cpu that computes the single force point source
      integer(kind=IXP) :: iel        !<hexahedron element that contains the single force point source
      integer(kind=IXP) :: iequ       !<GLL node (global numbering) on which the force is applied
      integer(kind=IXP) :: kgll       !<closest local GLL node in the @f$\zeta@f$-direction
      integer(kind=IXP) :: lgll       !<closest local GLL node in the @f$\eta @f$-direction
      integer(kind=IXP) :: mgll       !<closest local GLL node in the @f$\xi  @f$-direction

   end type

!>data structure for single force point sources in cpu myrank 
   type(type_single_force_source), allocatable, dimension(:) :: tg_sfsource

!
!                                                      
!***********************************************************************************************************************************************************************************
!type for double couple sources
!***********************************************************************************************************************************************************************************

!>type for double couple point sources
   type type_double_couple_source

      type(point3d)     :: p

      real   (kind=RXP) :: mxx        !<@f$xx@f$ component of the source tensor
      real   (kind=RXP) :: myy        !<@f$yy@f$ component of the source tensor
      real   (kind=RXP) :: mzz        !<@f$zz@f$ component of the source tensor
      real   (kind=RXP) :: mxy        !<@f$xy@f$ component of the source tensor
      real   (kind=RXP) :: mxz        !<@f$xz@f$ component of the source tensor
      real   (kind=RXP) :: myz        !<@f$yz@f$ component of the source tensor
      real   (kind=RXP) :: shift_time !<time shift of double couple source function
      real   (kind=RXP) :: rise_time  !<rise time of double couple source function                            
      real   (kind=RXP) :: str        !<strike angle in degree of double couple source (starting clockwise from the @f$y@f$-axis (=north)). @f$ 0 \le str \le 360 @f$
      real   (kind=RXP) :: dip        !<dip angle in degree of double couple source @f$ 0 \le dip \le 90 @f$
      real   (kind=RXP) :: rak        !<rake angle in degree of double couple source @f$ -180 \le rak \le -180 @f$
      real   (kind=RXP) :: mw         !<moment magnitude @f$M_{\mathrm{w}}@f$ of double couple source
      real   (kind=RXP) :: slip       !<displacement slip associated with the double couple point source (from @f$ m0 = \mu D S@f$)
      real   (kind=RXP) :: mu         !<shear-wave modulus of the medium at the location of the double couple point source
      integer(kind=IXP) :: icur       !<function's number associated to double couple source. See mod_source_function to see the definition of functions' number.
      integer(kind=IXP) :: rglo       !<global number of source among all cpus

      real   (kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: gll_force !<distributed forces at GLL nodes to generate double couple point source in cpu myrank
      real   (kind=RXP), dimension(:), allocatable                  :: stf       !<source time function of a double couple point source discretized with the time step of the simulation mod_global_variables::rg_dt

   end type                   

!>data structure for double couple sources in cpu myrank                            
   type(type_double_couple_source), allocatable, dimension(:) :: tg_dcsource_init !<init parameters read by all cpu

   type(type_double_couple_source), allocatable, dimension(:) :: tg_dcsource      !<runtime parameters in cpu myrank

!
!                                                      
!***********************************************************************************************************************************************************************************
!type for seismic faults (extended sources)
!***********************************************************************************************************************************************************************************

!>type for seismic faults
   type type_fault

      integer(kind=IXP)                              :: ifault    !<fault number
      real   (kind=RXP)                              :: xc        !<@f$x@f$ coordinate of the center of the fault in the domain of simulation
      real   (kind=RXP)                              :: yc        !<@f$y@f$ coordinate of the center of the fault in the domain of simulation
      real   (kind=RXP)                              :: zc        !<@f$z@f$ coordinate of the center of the fault in the domain of simulation
      real   (kind=RXP)                              :: str       !<strike angle in degree (clockwise from north (=@f$y@f$-axis))
      real   (kind=RXP)                              :: dip       !<dip angle in degree (rotation around the strike axis)
      real   (kind=RXP)                              :: rak       !<rake angle in degree (rotation around the fault normal axis)
      real   (kind=RXP)                              :: ls        !<length of the fault along strike
      real   (kind=RXP)                              :: ld        !<length of the fault along dip
      real   (kind=RXP)                              :: sh        !<@f$x@f$ coordinate of the hypocenter in the local strike(@f$x@f$)-dip(@f$y@f$) coordinate system
      real   (kind=RXP)                              :: dh        !<@f$y@f$ coordinate of the hypocenter in the local strike(@f$x@f$)-dip(@f$y@f$) coordinate system
      real   (kind=RXP)                              :: lcs       !<correlation length along the strike
      real   (kind=RXP)                              :: lcd       !<correlation length along the dip
      character(len=2)                               :: acf       !<autocorrelation function of the random field (gaussian, von Karman)
      real   (kind=RXP)                              :: k         !<kappa
      real   (kind=RXP)                              :: vrr       !<velocity rupture ratio (to S-wave velocity of the medium)
      real   (kind=RXP)                              :: mw        !<moment magnitude @f$M_{\mathrm{w}}@f$ associated to the fault
      real   (kind=R64)                              :: m0        !<seismic moment associated to the fault
      integer(kind=IXP)                              :: icur      !<function's number associated to the source time function. See mod_source_function to see the definition of the functions.
      real   (kind=RXP)                              :: ts        !<time shift of the source time function of the fault
      real   (kind=RXP)                              :: rt        !<rise time of the source time function of the fault  
      integer(kind=IXP)                              :: ndcsource !<number of double couple sources to describe the extended fault in cpu myrank
      real   (kind=RXP), dimension(:,:), allocatable :: rf        !<random field of the slip distribution
      real   (kind=RXP)                              :: ds        !<discretization step of the random field 'rf' along the strike
      real   (kind=RXP)                              :: dd        !<discretization step of the random field 'rf' along the dip
      integer(kind=IXP)                              :: ns        !<number sub-fault along strike
      integer(kind=IXP)                              :: nd        !<number sub-fault along dip
      integer(kind=IXP)                              :: mpi_comm  !>local communicator for faults in simulation ig_mpi_comm_simu
      integer(kind=IXP)                              :: mpi_rank  !>rank of cpu in local communicator 'mpi_comm'
      integer(kind=IXP)                              :: mpi_ncpu  !>total number cpu in local communicator 'mpi_comm'

      type(point3d), dimension(:,:), allocatable     :: p !<@f$x,y,z@f$ coordinate of the grid points of the fault in the coordinate system of the the domain of simulation. size = (ns,nd)

      type(type_double_couple_source), dimension(:), allocatable :: dcsource !>data structure for double couple sources in cpu myrank.

   end type                   

!>data structure for seismic faults
   type(type_fault), dimension(:), allocatable :: tg_fault

!
!
!***********************************************************************************************************************************************************************************
!type for receivers inside hexahedron
!***********************************************************************************************************************************************************************************

!>type for receivers (i.e., stations) located inside hexahedron elements
   type type_receiver_hexa

      real   (kind=RXP)                                     :: x       !<@f$x@f$ coordinate of receiver
      real   (kind=RXP)                                     :: y       !<@f$y@f$ coordinate of receiver
      real   (kind=RXP)                                     :: z       !<@f$z@f$ coordinate of receiver
      real   (kind=RXP)                                     :: xi      !<local coordinate @f$\xi@f$ of receiver
      real   (kind=RXP)                                     :: eta     !<local coordinate @f$\eta@f$ of receiver
      real   (kind=RXP)                                     :: zeta    !<local coordinate @f$\zeta@f$ of receiver
      real   (kind=RXP)                                     :: dmin    !<minimal distance of receiver to a GLL node
      real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL) :: lag     !<Lagrange polynomial at receiver location (local coordinates) in hexahedron element iel
      integer(kind=IXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL) :: gll     !<global GLL nodes number used for Lagrange interpolation at the receiver location
      integer(kind=IXP)                                     :: cpu     !<cpu that contains the receiver
      integer(kind=IXP)                                     :: iel     !<hexahedron element that contains the receiver
      integer(kind=IXP)                                     :: kgll    !<closest local GLL node in @f$\zeta@f$-direction
      integer(kind=IXP)                                     :: lgll    !<closest local GLL node in @f$\eta@f$-direction
      integer(kind=IXP)                                     :: mgll    !<closest local GLL node in @f$\xi@f$-direction
      integer(kind=IXP)                                     :: rglo    !<global number of receiver among all cpus

   end type

!>data structure for receivers located inside hexahedron elements
   type(type_receiver_hexa), allocatable, dimension(:) :: tg_receiver_hexa

!
!
!***********************************************************************************************************************************************************************************
!type for receivers inside quad
!***********************************************************************************************************************************************************************************

!>type for receivers (i.e., stations) located inside quadrangle elements
   type type_receiver_quad

      real   (kind=RXP)                             :: x       !<@f$x@f$ coordinate of receiver
      real   (kind=RXP)                             :: y       !<@f$y@f$ coordinate of receiver
      real   (kind=RXP)                             :: z       !<@f$z@f$ coordinate of receiver
      real   (kind=RXP)                             :: xi      !<local coordinate @f$\xi@f$ of receiver
      real   (kind=RXP)                             :: eta     !<local coordinate @f$\eta@f$ of receiver
      real   (kind=RXP)                             :: dmin    !<minimal distance of receiver to GLL node
      real   (kind=RXP), dimension(IG_NGLL,IG_NGLL) :: lag     !<Lagrange polynomial at receiver location (local coordinates) in quadrangle element iel
      real   (kind=RXP)                             :: pgd_x   !<Peak Ground Displacement (PGD) in @f$x@f$-direction
      real   (kind=RXP)                             :: pgd_y   !<Peak Ground Displacement (PGD) in @f$y@f$-direction
      real   (kind=RXP)                             :: pgd_z   !<Peak Ground Displacement (PGD) in @f$z@f$-direction
      real   (kind=RXP)                             :: pgv_x   !<Peak Ground Velocity (PGV) in @f$x@f$ direction
      real   (kind=RXP)                             :: pgv_y   !<Peak Ground Velocity (PGV) in @f$y@f$ direction
      real   (kind=RXP)                             :: pgv_z   !<Peak Ground Velocity (PGV) in @f$z@f$ direction
      real   (kind=RXP)                             :: pgv_xyz !<PGV module = @f$max_{0 \le t_{simu} \le tt_{simu}}\left[ \sqrt{v_x(t)^2 + v_y(t)^2 + v_z(t)^2} \right] @f$
      real   (kind=RXP)                             :: pga_x   !<Peak Ground Acceleration (PGA) in @f$x@f$ direction
      real   (kind=RXP)                             :: pga_y   !<Peak Ground Acceleration (PGA) in @f$y@f$ direction
      real   (kind=RXP)                             :: pga_z   !<Peak Ground Acceleration (PGA) in @f$z@f$ direction
      integer(kind=IXP), dimension(IG_NGLL,IG_NGLL) :: gll     !<global GLL nodes number used for Lagrange interpolation at the receiver location
      integer(kind=IXP)                             :: cpu     !<cpu that contains the receiver
      integer(kind=IXP)                             :: iel     !<quadrangle element that contains the receiver
      integer(kind=IXP)                             :: lgll    !<closest local GLL node in @f$\eta@f$-direction
      integer(kind=IXP)                             :: mgll    !<closest local GLL node in @f$\xi@f$-direction
      integer(kind=IXP)                             :: rglo    !<global number of receiver among all cpus

   end type

!>data structure for receivers used for snapshot
   type(type_receiver_quad), allocatable, dimension(:) :: tg_receiver_snapshot_quad

!>data structure for receivers located on free surface quadrangle elements
   type(type_receiver_quad), allocatable, dimension(:) :: tg_receiver_quad

!
!
!***********************************************************************************************************************************************************************************
!type for cpus connected to cpu myrank
!***********************************************************************************************************************************************************************************

!>type that gathers information about cpus connected to cpu myrank (cpus not connected to cpu myrank are not stored)
   type type_cpu_neighbor

      integer(kind=IXP)                            :: ngll     !<total number of unique common GLL nodes between cpu myrank and cpu icon
      integer(kind=IXP)                            :: icpu     !<global cpu number (from call mpi_comm_rank) connected to cpu myrank
      integer(kind=IXP), allocatable, dimension(:) :: gll_send !<GLL nodes number to be sent to cpu icon
      integer(kind=IXP), allocatable, dimension(:) :: gll_recv !<GLL nodes number to be received from cpu icon

   end type

!>data structure of cpu myrank that contains information about connected cpu icon.
   type(type_cpu_neighbor), allocatable, dimension(:) :: tg_cpu_neighbor

!
!
!***********************************************************************************************************************************************************************************
!type for linear elastic properties of materials
!***********************************************************************************************************************************************************************************

!>type for linear elastic properties of materials
   type type_elastic_material

      real   (kind=RXP) :: dens !<density @f$\rho@f$
      real   (kind=RXP) :: svel !<S-wave velocity @f$\beta@f$
      real   (kind=RXP) :: pvel !<P-wave veocity @f$\alpha@f$
      real   (kind=RXP) :: rvel !<Rayleigh wave velocity (approximated)
      real   (kind=RXP) :: pois !<Poisson coefficient @f$\nu@f$
      real   (kind=RXP) :: lam1 !<Lame parameter @f$\lambda@f$
      real   (kind=RXP) :: lam2 !<Lame parameter @f$\mu = \rho \beta^2@f$ (S-wave modulus)
      real   (kind=RXP) :: youn !<Young modulus
      real   (kind=RXP) :: bulk !<Bulk modulus
      real   (kind=RXP) :: pwmo !<P-wave modulus @f$\rho \alpha^2@f$

   end type

!>data structure for linear elastic properties of materials 
   type(type_elastic_material), allocatable, dimension(:) :: tg_elastic_material

!
!
!***********************************************************************************************************************************************************************************
!type for viscoelastic properties of materials
!***********************************************************************************************************************************************************************************

!>type for viscoelastic properties of materials
   type type_anelastic_material

      real(kind=RXP)                            :: freq  !<reference frequency @f$ f_r @f$ for which unrelaxed s-wave and p-wave modulus are defined
      real(kind=RXP)                            :: qs    !<s-wave quality factor @f$ Q_s @f$
      real(kind=RXP)                            :: qp    !<p-wave quality factor @f$ Q_p @f$
      real(kind=RXP)                            :: qssf  !<s-wave scaling factor @f$ Q_{ssf} @f$ to compute @f$ Q_s = \alpha / Q_{ssf} @f$
      real(kind=RXP)                            :: qpsf  !<p-wave scaling factor @f$ Q_{psf} @f$ to compute @f$ Q_p = \beta  / Q_{psf} @f$
      real(kind=RXP)                            :: uswm  !<unrelaxed s-wave modulus @f$ M^{S}_{u} @f$
      real(kind=RXP)                            :: upwm  !<unrelaxed p-wave modulus @f$ M^{P}_{u} @f$
      real(kind=RXP), dimension(:), allocatable :: wkqs  !<weight coefficients @f$ w^{S}_{k} @f$ associated to @f$ Q_s @f$
      real(kind=RXP), dimension(:), allocatable :: wkqp  !<weight coefficients @f$ w^{P}_{k} @f$ associated to @f$ Q_p @f$

   end type

!>data structure for viscoelastic properties of materials
   type(type_anelastic_material), allocatable, dimension(:) :: tg_anelastic_material

!
!
!***********************************************************************************************************************************************************************************
!type for random properties of materials
!***********************************************************************************************************************************************************************************

!>type for random properties of materials
   type type_random_material

      real   (kind=RXP), dimension(:), allocatable :: rd                  !<value of random distribution (e.g., normal distribution)
      real   (kind=RXP)                            :: lcx                 !<correlation length along the @f$x@f$ direction
      real   (kind=RXP)                            :: lcy                 !<correlation length along the @f$y@f$ direction
      real   (kind=RXP)                            :: lcz                 !<correlation length along the @f$z@f$ direction
      real   (kind=RXP)                            :: e                   !<fractional fluctuaction of wave velocity: @f$ e = \delta V / V_0@f$

   end type

!>data structure for random properties of materials
   type(type_random_material), allocatable, dimension(:) :: tg_random_material

!>type for random properties of materials
   type type_random_material_KL

      real   (kind=RXP), dimension(:), allocatable :: r    !<roots of the characteristic function used for Karuhnen-Loeve expansion (KLE) 
      real   (kind=RXP), dimension(:), allocatable :: l    !<eigenvalues used for KLE
      integer(kind=IXP)                            :: n    !<number of terms used for the KLE

   end type


!
!
!***********************************************************************************************************************************************************************************
!type for layer properties
!***********************************************************************************************************************************************************************************

   character(len=15), dimension(4_IXP), parameter :: cg_struture_name = (/"constant       ",&
                                                                          "linear gradient",&
                                                                          "random         ",&
                                                                          "user defined   "/) !<name of the structure type for data structure type_layer

!>type for layers
   type type_layer

      integer(kind=IXP)                                        :: structure_type  !< 1=constant material properties, 2=linear gradient material properties, 3=random material properties, 4=user defined
      integer(kind=IXP)                                        :: nmat            !<nb of materials
      real   (kind=RXP)                                        :: xl              !<@f$x@f$-length of the boxshoe containing the layer
      real   (kind=RXP)                                        :: yl              !<@f$y@f$-length of the boxshoe containing the layer
      real   (kind=RXP)                                        :: zl              !<@f$z@f$-length of the boxshoe containing the layer
      real   (kind=RXP)                                        :: xmin            !<minimum of @f$x@f$-coordinate of the rectangular box embrassing the material
      real   (kind=RXP)                                        :: xmax            !<maximum of @f$x@f$-coordinate of the rectangular box embrassing the material
      real   (kind=RXP)                                        :: ymin            !<minimum of @f$y@f$-coordinate of the rectangular box embrassing the material
      real   (kind=RXP)                                        :: ymax            !<maximum of @f$y@f$-coordinate of the rectangular box embrassing the material
      real   (kind=RXP)                                        :: zmin            !<minimum of @f$z@f$-coordinate of the rectangular box embrassing the material
      real   (kind=RXP)                                        :: zmax            !<maximum of @f$z@f$-coordinate of the rectangular box embrassing the material
      real   (kind=RXP)                                        :: zminh           !<minimum of @f$z@f$-coordinate of the rectangular box embrassing the material plus one homogenization length
      real   (kind=RXP)                                        :: zmaxh           !<maximum of @f$z@f$-coordinate of the rectangular box embrassing the material plus one homogenization length
      integer(kind=IXP)            , allocatable, dimension(:) :: modes_ind       !<indirection array after sorting the 3D modes (lx*ly*lz)
      type(type_elastic_material)  , allocatable, dimension(:) :: elastic
      type(type_anelastic_material), allocatable, dimension(:) :: anelastic
      type(type_random_material)   , allocatable, dimension(:) :: random
      type(type_random_material_KL)                            :: KLx
      type(type_random_material_KL)                            :: KLy
      type(type_random_material_KL)                            :: KLz

   end type

!>data structure for layers
   type(type_layer), allocatable, dimension(:) :: tg_layer


!
!
!***********************************************************************************************************************************************************************************
!type for tomography
!***********************************************************************************************************************************************************************************

   type type_tomo

      character(len=CIL)                                       :: input_folder    !<name of input folder for tomography (containing files 'vp.bin', 'vs.bin', 'rho.bin', 'qp.bin', and 'qs.bin')
      logical(kind=IXP)                                        :: is_tomo_defined = .false.
      integer(kind=IXP)                                        :: nx, ny, nz      !<size of input cartesian grid for tomography
      real   (kind=RXP)                                        :: dx, dy, dz      !<discretization steps of cartesian grid
      real   (kind=RXP)                                        :: x0, y0, z0      !<origin of cartesian grid
      real   (kind=RXP), allocatable, dimension(:,:,:)         :: vp, vs, rho, qp, qs !<arrays of seismic properties (of size [ny,nx,nz])
      real   (kind=RXP)                                        :: qf              !<reference frequency @f$ f_r @f$ for which unrelaxed s-wave and p-wave modulii are defined

   end type

!>data structure for tomo
   type(type_tomo) :: tg_tomo


!***********************************************************************************************************************************************************************************
!***********************************************************************************************************************************************************************************
!***********************************************************************************************************************************************************************************
!***********************************************************************************************************************************************************************************


   public  :: get_prefix
   public  :: get_number_of_simulations
   public  :: get_folder_of_simulations
   public  :: get_newunit!TODO FLO: could be replaced by the 'newunit' fortran intrinsic
   public  :: error_stop
   public  :: info_all_cpu
   public  :: sweep_blanks
   public  :: strupcase
   public  :: strlowcase
   public  :: int2str
   public  :: nitems
   public  :: read_line
   public  :: count_file_line
   public  :: partition1d

   contains

!>@cond
!@return p
!***********************************************************************************************************************************************************************************
   character(len=CIL) function get_prefix() result(p)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP)  :: myunit
      integer(kind=IXP)  :: ios
      character(len=CIL) :: info

      if (ig_myrank_world == ZERO_IXP) then

         open(unit=get_newunit(myunit),file="prefix",status='old',iostat=ios)
       
         if (ios /= ZERO_IXP) then
       
            write(info,'(a)') "error while opening file prefix"
       
            call error_stop(info)
       
         endif
       
         read(unit=myunit,fmt=*) p
        
         close(myunit)

      endif

      call mpi_bcast(p,len(p),MPI_CHARACTER,ZERO_IXP,MPI_COMM_WORLD,ios)

      return 

!***********************************************************************************************************************************************************************************
   end function get_prefix
!***********************************************************************************************************************************************************************************

!
!
!@param  f : filename containing the folder tree in multiple simulations mode
!@return n : number of simulations (computed by reading non-blank lines)
!***********************************************************************************************************************************************************************************
   integer(kind=IXP) function get_number_of_simulations(f) result(n)
!***********************************************************************************************************************************************************************************

      use mod_precision

      implicit none

      character(len=*), intent(in) :: f

      integer(kind=IXP)  :: myunit
      integer(kind=IXP)  :: ios

      character(len=  1) :: c1

!
!---->cpu0_world get the number of simulation by parsing file 'f'

      if (ig_myrank_world == ZERO_IXP) then

         open(newunit=myunit,file=trim(adjustl(f)),action="read",iostat=ios)
       
         if (ios /= 0_IXP) then
       
            write(IG_LST_UNIT,fmt='(a)') "file "//trim(adjustl(f))//" not found"

            n = 1_IXP
       
         else
       
            n = 0_IXP
       
            do
       
               read(unit=myunit,fmt='(a)',iostat=ios) c1
       
               if (ios /= 0_IXP) exit
       
               if (c1 /= "") n = n + 1_IXP
       
            enddo
       
            close(myunit)
       
         endif
       
         write(output_unit,'(A,I0)') "Number of simulations found: ",n

      endif

!
!---->cpu0_world broadcast n to other cpus

      call mpi_bcast(n,ONE_IXP,MPI_INTEGER,ZERO_IXP,MPI_COMM_WORLD,ios)

      return 

!***********************************************************************************************************************************************************************************
   end function get_number_of_simulations
!***********************************************************************************************************************************************************************************


!
!
!@param  f : filename containing the folder tree in multiple simulations mode
!@return d : array containing the list of folder in multiple simulations mode
!***********************************************************************************************************************************************************************************
   function get_folder_of_simulations(f,n) result(d)
!***********************************************************************************************************************************************************************************

      use mod_precision

      implicit none

      character(len=*) , intent(in) :: f
      integer(kind=IXP), intent(in) :: n

      character(len=CIL), dimension(:), allocatable :: d

      integer(kind=IXP)  :: i
      integer(kind=IXP)  :: myunit
      integer(kind=IXP)  :: ios

      character(len=CIL) :: c

      allocate(d(n))
!
!---->cpu0_world get the folder name of simulations

      if (ig_myrank_world == ZERO_IXP) then

         open(newunit=myunit,file=trim(adjustl(f)),action="read",iostat=ios)
       
         if (ios /= 0_IXP) then
       
            write(IG_LST_UNIT,fmt='(a)') "file "//trim(adjustl(f))//" not found"

            return
       
         else

            i = ZERO_IXP
       
            do
       
               read(unit=myunit,fmt='(a)',iostat=ios) c
       
               if (ios /= ZERO_IXP) exit
       
               if (c /= "") then

                  i = i + 1_IXP
   
                  d(i) = c

               endif
       
            enddo
       
            close(myunit)
       
         endif
       
      endif

!
!---->cpu0_world broadcast n to other cpus

      call mpi_bcast(d,n*len(d(1_IXP)),MPI_CHARACTER,ZERO_IXP,MPI_COMM_WORLD,ios)

      return 

!***********************************************************************************************************************************************************************************
   end function get_folder_of_simulations
!***********************************************************************************************************************************************************************************


!
!
!>@brief function to search for an available unit.
!!lun_min and lun_max define the range of possible luns to check.
!!the unit value is returned by the function, and also by the optional
!!argument. this allows the function to be used directly in an open
!!statement, and optionally save the result in a local variable.
!!if no units are available, -1 is returned.
!>@param myunit : free unit 
!***********************************************************************************************************************************************************************************
   integer(kind=IXP)function get_newunit(myunit)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent(out), optional :: myunit
      integer(kind=IXP), parameter             :: lun_min=101_IXP
      integer(kind=IXP), parameter             :: lun_max=20101_IXP
      logical(kind=IXP)                        :: is_opened
      integer(kind=IXP)                        :: lun

      get_newunit=-ONE_IXP

      do lun=lun_min,lun_max
         inquire(unit=lun,opened=is_opened)
         if (.not. is_opened) then
            get_newunit=lun
            exit
         endif
      enddo

      if (present(myunit)) myunit=get_newunit

!***********************************************************************************************************************************************************************************
   end function get_newunit
!***********************************************************************************************************************************************************************************

!
!
!>@brief subroutine to abort and stop simulation with an error message
!>@param info : error message printed to standard output
!>@param r    : real(kind=RXP)value associated to the error message
!***********************************************************************************************************************************************************************************
   subroutine error_stop(info,r)
!***********************************************************************************************************************************************************************************

      use mpi

      implicit none

      character(len=*) , intent(in)           :: info
      real   (kind=RXP), intent(in), optional :: r
      integer(kind=IXP)                       :: ios

      write(*,'(a)') " "
      if (present(r)) then
         write(*,'(a,i5,1x,a,E14.7)') "cpu ",ig_myrank,trim(adjustl(info))//" ",r
      else
         write(*,'(a,i5,1x,a)') "cpu ",ig_myrank,trim(adjustl(info))
      endif

      call mpi_abort(ig_mpi_comm_simu,100_IXP,ios)
      stop

!***********************************************************************************************************************************************************************************
   end subroutine error_stop
!***********************************************************************************************************************************************************************************

!
!
!>@brief subroutine to gather and sum integer(kind=IXP)variables (done by cpu0 only)
!>@param i    : variable gathered by cpu0
!>@param info : name of gathered variables
!***********************************************************************************************************************************************************************************
   subroutine info_all_cpu(i,info)
!***********************************************************************************************************************************************************************************

      use mpi

      implicit none

      integer(kind=IXP) , intent(in)        :: i
      character(len=CIL), intent(in)        :: info

      integer(kind=IXP), dimension(ig_ncpu) :: buffer
      integer(kind=IXP)                     :: ios
      integer(kind=IXP)                     :: icpu
      integer(kind=I64)                     :: isum

      isum = ZERO_I64

      call mpi_gather(i,ONE_IXP,MPI_INTEGER,buffer,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(a)') " "

         do icpu = ONE_IXP,ig_ncpu

            if (buffer(icpu) > ZERO_IXP) write(IG_LST_UNIT,'(a,i6,a,i12,1x,a)') "cpu ",icpu-1," computes      ",buffer(icpu),trim(info)
            isum = isum + int(buffer(icpu),kind=I64)

         enddo

         if (isum > ZERO_IXP) write(IG_LST_UNIT,'("-------------------------------------",/,a,i29)') "total = ",isum

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine info_all_cpu
!***********************************************************************************************************************************************************************************

!
!
!>@brief function to sweep blank characters in string
!>@param in_str       : input string to be swept
!***********************************************************************************************************************************************************************************
   character(len=100) function sweep_blanks(in_str)
!***********************************************************************************************************************************************************************************

      implicit none
      
      character(len=100), intent(in) :: in_str
      character(len=100)             :: out_str
      character(len=1)               :: ch
      character(len=1)               :: tab
      integer  (kind=IXP)            :: j
      
      out_str = " "
      tab     = char(9) !horizontal tabulation
    
      do j = ONE_IXP,len_trim(in_str)
!     
!------>get j-th char
        ch = in_str(j:j)
      
        if ( (ch /= " ") .and. (ch /= tab) ) then
          out_str = trim(out_str) // ch
        endif
     
      enddo

      sweep_blanks = out_str 
!***********************************************************************************************************************************************************************************
  end function sweep_blanks
!***********************************************************************************************************************************************************************************

!
!
!>@brief function to convert lowercase characters to uppercase characters
!>@param input_string : lowercase input string 
!***********************************************************************************************************************************************************************************
   function strupcase (input_string) result (output_string)
!***********************************************************************************************************************************************************************************

      implicit none

      character(len=*), parameter      :: lower_case = 'abcdefghijklmnopqrstuvwxyz'
      character(len=*), parameter      :: upper_case = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

      character(len=*), intent( in )   :: input_string
      character(len=len(input_string)) :: output_string

      integer(kind=IXP)                :: i
      integer(kind=IXP)                :: n
      
      output_string = input_string

      do i = ONE_IXP,len(output_string)

         n = index(lower_case,output_string(i:i))

         if (n /= ZERO_IXP) output_string(i:i) = upper_case(n:n)

      enddo

!***********************************************************************************************************************************************************************************
   end function strupcase
!***********************************************************************************************************************************************************************************

!
!
!>@brief function to convert uppercase characters to lowercase characters
!>@param input_string : uppercase input string 
!***********************************************************************************************************************************************************************************
   function strlowcase (input_string) result (output_string)
!***********************************************************************************************************************************************************************************

      implicit none

      character(len=*), parameter      :: lower_case = 'abcdefghijklmnopqrstuvwxyz'
      character(len=*), parameter      :: upper_case = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

      character(len=*), intent(in)     :: input_string
      character(len=len(input_string)) :: output_string

      integer(kind=IXP)                :: i
      integer(kind=IXP)                :: n
      
      output_string = input_string

      do i = ONE_IXP,len(output_string)

         n = index(upper_case,output_string(i:i))

         if (n /= ZERO_IXP) output_string(i:i) = lower_case(n:n)

      enddo

!***********************************************************************************************************************************************************************************
   end function strlowcase
!***********************************************************************************************************************************************************************************

!
!
!>@brief function to convert an integer into a string of character
!>@param i : integer
!>@return : 
!***********************************************************************************************************************************************************************************
   character(len=CIL) function int2str(k) result(str)
!***********************************************************************************************************************************************************************************

    integer(kind=IXP), intent(in) :: k

    write (str,'(I0)') k

    str = adjustl(str)

!***********************************************************************************************************************************************************************************
   end function int2str
!***********************************************************************************************************************************************************************************

!
!
!>@brief function to count the number of space-separated items in a line
!>@param line : input string
!***********************************************************************************************************************************************************************************
   integer function nitems(line)
!***********************************************************************************************************************************************************************************

      character,intent(in) :: line*(*)
      integer(kind=IXP)    :: i
      integer(kind=IXP)    :: n
      integer(kind=IXP)    :: toks

      i      = ONE_IXP
      n      = len_trim(line)
      toks   = ZERO_IXP
      nitems = ZERO_IXP

      do while(i <= n)

         do while(line(i:i) == ' ') 
           i = i + ONE_IXP
           if (n < i) return
         enddo

         toks   = toks + ONE_IXP
         nitems = toks

         do
           i = i + ONE_IXP
           if (n < i) return
           if (line(i:i) == ' ') exit
         enddo

      enddo

!***********************************************************************************************************************************************************************************
   end function nitems 
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine reads the keyword and value of a buffer line
!>@param  u : unit to be read
!>@return k : keyword
!>@return v : value
!>@return e : error status
!***********************************************************************************************************************************************************************************
   subroutine read_line(u,k,v,e)
!***********************************************************************************************************************************************************************************

      implicit none

      integer  (kind=IXP), intent(in ) :: u
      character(len=100) , intent(out) :: k
      character(len=100) , intent(out) :: v
      integer  (kind=IXP), intent(out) :: e

      character(len=100)               :: buffer_line
      integer  (kind=IXP)              :: pos

      read(unit=u,fmt='(a)',iostat=e) buffer_line
      if (e /= ZERO_IXP) return
      
      buffer_line = trim(adjustl(buffer_line))
      pos         = scan(buffer_line,'=')

      if (pos >= TWO_IXP) then

         k = strlowcase(buffer_line(ONE_IXP:pos-ONE_IXP))
         v = buffer_line(pos+ONE_IXP:100_IXP)

      else

         k = "na"

      endif

      return
   
!***********************************************************************************************************************************************************************************
   end subroutine read_line
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This function counts the number of line in a file
!>@param  f : file to be processed
!>@return n : total number of line in file 'f'
!***********************************************************************************************************************************************************************************
   integer(kind=IXP) function count_file_line(f) result(n)
!***********************************************************************************************************************************************************************************

      implicit none

      character(len=*)   , intent(in ) :: f

      integer  (kind=IXP)              :: u
      integer  (kind=IXP)              :: ios

      character(len=1)                 :: c1


      open(newunit=u,file=trim(f),status='old',form='formatted',action='read',iostat=ios)

      n = ZERO_IXP

      do 

         read(unit=u,fmt=*,iostat=ios) c1

         if (ios /= ZERO_IXP) exit

         n = n + 1

      enddo

      close(u)

      return
   
!***********************************************************************************************************************************************************************************
   end function count_file_line
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine sets the global starting and ending indexes of NM ensembles which divide NN cells
!>@param  im : index of ensemble number: 1 <= im <= nm
!>@param  nm : total number of ensemble
!>@param  nn : number of cells
!>@return is : start index (global numbering)
!>@return ie : end   index (global numbeing)
!***********************************************************************************************************************************************************************************
   subroutine partition1d(im,nm,nn,is,ie)
!***********************************************************************************************************************************************************************************

         integer(kind=IXP), intent( in) :: im
         integer(kind=IXP), intent( in) :: nm
         integer(kind=IXP), intent( in) :: nn
         integer(kind=IXP), intent(out) :: is
         integer(kind=IXP), intent(out) :: ie

         integer(kind=IXP)              :: ncf_m !floor number of cell in ensemble m
         integer(kind=IXP)              :: ncr_m !remainder of the division

         ncf_m = floor(real(nn,kind=RXP)/real(nm,kind=RXP),kind=IXP)
         ncr_m = mod(nn,nm)

         is = (im      )*ncf_m + 1_IXP
         ie = (im+1_IXP)*ncf_m

         if (im+1_IXP <= ncr_m) then

            is = (im      )*ncf_m + 1_IXP + im
            ie = (im+1_IXP)*ncf_m + 1_IXP + im

         else

            is = ncr_m*(ncf_m+1_IXP) + (im-ncr_m      )*ncf_m + 1_IXP
            ie = ncr_m*(ncf_m+1_IXP) + (im-ncr_m+1_IXP)*ncf_m

         endif

!
!------->set 'is' and 'ie' to zero if nm > nn. In this case, 'is' always larger than 'ie' for the  ensemble indices larger than 'nn'

         if (is > ie) then

            is = ZERO_IXP

            ie = ZERO_IXP

         endif

      return
   
!***********************************************************************************************************************************************************************************
   end subroutine partition1d
!***********************************************************************************************************************************************************************************
!>@endcond
   
end module mod_global_variables
