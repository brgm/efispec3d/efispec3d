!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!<b>This file contains the main spectral finite element program: EFISPEC3D</b>.
!!
!>@brief
!!This program is composed by :
!!- an initialization phase;
!!- a time loop;
!!- a finalization phase.

!>@return void
program EFISPEC3D

   use mpi

   use mod_precision
   
   use mod_global_variables, only :&
                                    ig_ndt&
                                   ,rg_simu_current_time&
                                   ,ig_idt&
                                   ,rg_simu_times&
                                   ,rg_plane_wave_std&
                                   ,IG_LST_UNIT&
                                   ,LG_OUTPUT_CPUTIME&
                                   ,LG_SNAPSHOT_VTK&
                                   ,LG_SNAPSHOT_SURF_GLL&
                                   ,LG_SNAPSHOT_SURF_GLL_FILTER&
                                   ,LG_FIR_FILTER&
                                   ,LG_PLANE_WAVE&
                                   ,LG_SNAPSHOT_SURF_SPA_DER&
                                   ,IG_LAGRANGE_ORDER&
                                   ,lg_snapshot&
                                   ,lg_snapshot_volume&
                                   ,ig_unit_snap_quad_gll_dis&
                                   ,ig_unit_snap_quadf_dis_spa_der&
                                   ,ig_mpi_comm_fsurf&
                                   ,ig_ncpu&
                                   ,ig_nhexa&
                                   ,ig_myrank&
                                   ,cg_myrank&
                                   ,cg_prefix&
                                   ,get_newunit&
                                   ,error_stop&
                                   ,lg_boundary_absorption&
                                   ,ig_nhexa_outer&
                                   ,ig_mpi_comm_simu&
                                   ,ig_receiver_saving_incr&
                                   ,LG_ASYNC_MPI_COMM

   use mod_efi_mpi

   use mod_init_efi

   use mod_init_mesh
   
   use mod_init_mpibuffer
   
   use mod_solver
   
   use mod_init_medium

   use mod_init_time_step

   use mod_filter

   use mod_snapshot_surface  , only :&
                                     init_snapshot_surface&
                                    ,init_snapshot_surface_gll&
                                    ,write_snapshot_surface&
                                    ,write_snapshot_surface_gll&
                                    ,write_peak_ground_motion&
                                    ,write_collection_vtk_surf

   use mod_snapshot_volume   , only:&
                                    init_snapshot_volume&
                                   ,write_snapshot_volume&
                                   ,write_collection_vtk_vol

   use mod_init_memory

   use mod_mpi_assemble

   use mod_receiver          , only :&
                                     write_receiver_output&
                                    ,init_hexa_receiver&
                                    ,init_quad_receiver

   use mod_source            , only :&
                                     init_fault&
                                    ,init_double_couple_source&
                                    ,init_single_force_source&
                                    ,init_plane_wave_source&
                                    ,compute_double_couple_source

   use mod_random_field_sp
   
   implicit none
   
   real   (kind=R64), allocatable, dimension(:) :: buffer_double
   real   (kind=R64), allocatable, dimension(:) :: buffer_double_all_cpu

   real   (kind=R64)                            :: start_time
   real   (kind=R64)                            :: end_time
                                                  
   real   (kind=R64)                            :: dltim1
   real   (kind=R64)                            :: dltim2
                                                  
   real   (kind=R64)                            :: time_init_mesh
   real   (kind=R64)                            :: time_init_gll
   real   (kind=R64)                            :: time_init_mpi_buffers
   real   (kind=R64)                            :: time_init_medium
   real   (kind=R64)                            :: time_init_time_step
   real   (kind=R64)                            :: time_init_fir_filter
   real   (kind=R64)                            :: time_init_source
   real   (kind=R64)                            :: time_init_receiver
   real   (kind=R64)                            :: time_init_jacobian
   real   (kind=R64)                            :: time_init_mass_matrix
   real   (kind=R64)                            :: time_compute_dcsource
   real   (kind=R64)                            :: time_init_snapshot
   real   (kind=R64)                            :: time_memory_consumption
                                                  
   real   (kind=R64)                            :: t_tloop
   real   (kind=R64)                            :: t_newmark
   real   (kind=R64)                            :: t_forext
   real   (kind=R64)                            :: t_forabs
   real   (kind=R64)                            :: t_forint_inner
   real   (kind=R64)                            :: t_forint_outer
   real   (kind=R64)                            :: t_resol
   real   (kind=R64)                            :: t_snafsu
   real   (kind=R64)                            :: t_outavd
   real   (kind=R64)                            :: t_linkfo
   real   (kind=R64)                            :: t_link_init
                                               
   integer(kind=IXP), parameter                 :: NTIME_INIT = 12_IXP
   integer(kind=IXP), allocatable, dimension(:) :: buffer_integer
   integer(kind=IXP)                            :: ios
   integer(kind=IXP)                            :: icpu
   integer(kind=IXP)                            :: itime
   integer(kind=IXP)                            :: unit_time
                                               
!
!
!*************************************************************************************************************************
!*************************************************************************************************************************
!phase 1: initialization
!*************************************************************************************************************************
!*************************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   call init_mpi()

   call init_simu_folder()

   start_time = mpi_wtime()
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   call init_input_variables(cg_prefix)
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_mesh = mpi_wtime()

   call init_mesh()

   time_init_mesh = mpi_wtime() - time_init_mesh
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_gll = mpi_wtime()

   call init_gll_nodes()

   call init_gll_nodes_coordinates()

   time_init_gll = mpi_wtime() - time_init_gll
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_mpi_buffers = mpi_wtime()

   call init_mpi_buffers()

   time_init_mpi_buffers = mpi_wtime() - time_init_mpi_buffers
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_medium = mpi_wtime()

   call init_hexa_medium()

   if (lg_boundary_absorption) call init_quadp_medium()

   time_init_medium = mpi_wtime() - time_init_medium
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_time_step = mpi_wtime()

   call init_time_step()

   time_init_time_step = mpi_wtime() - time_init_time_step
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_fir_filter = mpi_wtime()

   if (LG_FIR_FILTER) call init_fir_filter()

   time_init_fir_filter = mpi_wtime() - time_init_fir_filter
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_source = mpi_wtime()

   if (LG_PLANE_WAVE) then

      call init_plane_wave_source()

   else

      call init_fault()

      call init_double_couple_source()

      call init_single_force_source()
   
   endif

   time_init_source = mpi_wtime() - time_init_source
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_receiver = mpi_wtime()

   call init_hexa_receiver()

   call init_quad_receiver()

   time_init_receiver = mpi_wtime() - time_init_receiver
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_jacobian = mpi_wtime()

   call init_jacobian_matrix_hexa()

   call init_jacobian_matrix_quad()
   
   time_init_jacobian = mpi_wtime() - time_init_jacobian
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_mass_matrix = mpi_wtime()

   call init_mass_matrix()
   
   time_init_mass_matrix = mpi_wtime() - time_init_mass_matrix
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_compute_dcsource = mpi_wtime()
   
   if (.not.LG_PLANE_WAVE) then

      call compute_double_couple_source()

   endif
   
   time_compute_dcsource = mpi_wtime() - time_compute_dcsource
   !**********************************************************************************************************************


   !
   !
   !**********************************************************************************************************************
   time_init_snapshot = mpi_wtime()
   
   if ( (lg_snapshot) .and. (.not.LG_SNAPSHOT_SURF_GLL) ) call init_snapshot_surface()

   if (lg_snapshot_volume)   call init_snapshot_volume()

   if (LG_SNAPSHOT_SURF_GLL) call init_snapshot_surface_gll()
   
   time_init_snapshot = mpi_wtime() - time_init_snapshot
   !**********************************************************************************************************************

   
   !
   !
   !**********************************************************************************************************************
   time_memory_consumption = mpi_wtime()

   call memory_consumption()

   time_memory_consumption = mpi_wtime() - time_memory_consumption
   !**********************************************************************************************************************


   end_time = (mpi_wtime() - start_time)

   call mpi_reduce(end_time,dltim1,ONE_IXP,MPI_DOUBLE_PRECISION,MPI_MAX,ZERO_IXP,ig_mpi_comm_simu,ios)


   !
   !
   !**********************************************************************************************************************
   !summarize elapsed time in subroutines for initialization
   !**********************************************************************************************************************
   allocate(buffer_double(NTIME_INIT))

   allocate(buffer_double_all_cpu(NTIME_INIT*ig_ncpu))

   buffer_double( 1_IXP) = time_init_mesh
   buffer_double( 2_IXP) = time_init_gll
   buffer_double( 3_IXP) = time_init_mpi_buffers
   buffer_double( 4_IXP) = time_init_medium
   buffer_double( 5_IXP) = time_init_source
   buffer_double( 6_IXP) = time_init_receiver
   buffer_double( 7_IXP) = time_init_jacobian
   buffer_double( 8_IXP) = time_init_mass_matrix
   buffer_double( 9_IXP) = time_compute_dcsource
   buffer_double(10_IXP) = time_init_snapshot
   buffer_double(11_IXP) = time_memory_consumption
   buffer_double(12_IXP) = time_init_time_step

   call mpi_gather(buffer_double,NTIME_INIT,MPI_DOUBLE_PRECISION,buffer_double_all_cpu,NTIME_INIT,MPI_DOUBLE_PRECISION,ZERO_IXP,ig_mpi_comm_simu,ios)

   if (ig_myrank == ZERO_IXP) then

      write(unit=IG_LST_UNIT,fmt='(" ",/,a)') "elapsed time for initialization"
      write(unit=IG_LST_UNIT,fmt='(      a)') "             init_mesh    init_gll_nodes  init_mpi_buff  init_medium    init_source    init_receiver    init_jaco     init_mass      init_dcs      init_snapshot   init_memory   init_time_step"

      do icpu = ONE_IXP,ig_ncpu
         write(unit=IG_LST_UNIT,fmt='(a,i6,1x,12(e14.7,1x))') "cpu ",icpu-ONE_IXP,(buffer_double_all_cpu((icpu-ONE_IXP)*NTIME_INIT+itime),itime=ONE_IXP,NTIME_INIT)
      enddo

   endif

   deallocate(buffer_double)
   deallocate(buffer_double_all_cpu)

!
!
!***********************************************************************************************************************
!***********************************************************************************************************************
!phase 2: time loop
!***********************************************************************************************************************
!***********************************************************************************************************************
   call mpi_barrier(ig_mpi_comm_simu,ios)

   start_time = mpi_wtime()

   if (LG_OUTPUT_CPUTIME) then 
      open(unit=get_newunit(unit_time),file=trim(cg_prefix)//".time.cpu."//trim(cg_myrank),status='replace')
   endif

   if (ig_myrank == ZERO_IXP) then
      write(IG_LST_UNIT,'(" ",/,a)') "starting time loop"
      write(IG_LST_UNIT,'(" ",/,a)') " -->time of simulation"
   endif
   
   do ig_idt = ONE_IXP,ig_ndt

      !
      !
      !*****************************************************************************************************************
      !->time of the simulation
      !*****************************************************************************************************************
      rg_simu_current_time = rg_simu_times(ig_idt)

      !
      !
      !*****************************************************************************************************************
      !->output time step in file *.lst
      !*****************************************************************************************************************
      if ( (ig_myrank == ZERO_IXP) .and. mod(ig_idt-ONE_IXP,1000_IXP) == ZERO_IXP ) then

         write(IG_LST_UNIT,'(7X,E14.7)') rg_simu_current_time
         call flush(IG_LST_UNIT)

      endif

      !
      !
      !*****************************************************************************************************************
      !->compute cpu time for each cpu for each time steps
      !*****************************************************************************************************************
      if (LG_OUTPUT_CPUTIME) then 
        t_tloop = mpi_wtime()
      endif

      !
      !
      !*****************************************************************************************************************
      !->compute displacements at step n+1
      !*****************************************************************************************************************
      if (LG_OUTPUT_CPUTIME) then 
        t_newmark = mpi_wtime()
      endif
   
      call newmark_ini()
   
      if (LG_OUTPUT_CPUTIME) then
         t_newmark = mpi_wtime() - t_newmark
      endif

      !
      !
      !*****************************************************************************************************************
      !->compute plane wave or external forces (i.e., double couple or single force sources)
      !*****************************************************************************************************************
      if (LG_OUTPUT_CPUTIME) then
        t_forext = mpi_wtime()
      endif

      if (LG_PLANE_WAVE) then

        if (rg_simu_current_time <= rg_plane_wave_std) call compute_plane_wave()

      else
   
         call compute_external_forces()

      endif
   
      if (LG_OUTPUT_CPUTIME) then
        t_forext = mpi_wtime() - t_forext
      endif      

      !
      !
      !*****************************************************************************************************************
      !->compute dirichlet boundary conditions or absorption boundary forces
      !*****************************************************************************************************************
      if (LG_OUTPUT_CPUTIME) then 
        t_forabs = mpi_wtime()
      endif
      
      if (LG_PLANE_WAVE) then

         call compute_absorption_forces_bottom_only()
         call compute_dirichlet_conditions()

      else

         if (lg_boundary_absorption) call compute_absorption_forces()

      endif
      
      if (LG_OUTPUT_CPUTIME) then
         t_forabs = mpi_wtime() - t_forabs
      endif

      !
      !
      !*****************************************************************************************************************
      !->compute internal forces for inner and outer elements
      !*****************************************************************************************************************
      if (LG_OUTPUT_CPUTIME) then
        t_forint_outer = mpi_wtime()
      endif
     
      if (IG_LAGRANGE_ORDER == 4_IXP) call compute_internal_forces_order4(ONE_IXP,ig_nhexa_outer)
      if (IG_LAGRANGE_ORDER == 5_IXP) call compute_internal_forces_order5(ONE_IXP,ig_nhexa_outer)
      if (IG_LAGRANGE_ORDER == 6_IXP) call compute_internal_forces_order6(ONE_IXP,ig_nhexa_outer)
   
      if (LG_OUTPUT_CPUTIME) then
         t_forint_outer = mpi_wtime() - t_forint_outer
      endif
     
      if (LG_OUTPUT_CPUTIME) then
         t_link_init = mpi_wtime()
      endif
    
      if (LG_ASYNC_MPI_COMM) call assemble_force_async_comm_init()
   
      if (LG_OUTPUT_CPUTIME) then
         t_link_init = mpi_wtime() - t_link_init
      endif 
      
      if (LG_OUTPUT_CPUTIME) then
         t_forint_inner = mpi_wtime()
      endif
      
      if (IG_LAGRANGE_ORDER == 4_IXP) call compute_internal_forces_order4(ig_nhexa_outer+ONE_IXP,ig_nhexa)
      if (IG_LAGRANGE_ORDER == 5_IXP) call compute_internal_forces_order5(ig_nhexa_outer+ONE_IXP,ig_nhexa)
      if (IG_LAGRANGE_ORDER == 6_IXP) call compute_internal_forces_order6(ig_nhexa_outer+ONE_IXP,ig_nhexa)
   
      if (LG_OUTPUT_CPUTIME) then
         t_forint_inner = mpi_wtime() - t_forint_inner
      endif

      !
      !
      !*****************************************************************************************************************
      !->assemble forces for linked cpu
      !*****************************************************************************************************************
      if (LG_OUTPUT_CPUTIME) then 
         t_linkfo = mpi_wtime()
      endif
      
      if (LG_ASYNC_MPI_COMM) then

         call assemble_force_async_comm_end()

      else

         call assemble_force_sync_comm()

      endif  
      
      if (LG_OUTPUT_CPUTIME) then 
         t_linkfo = mpi_wtime() - t_linkfo
      endif

      !      
      !      
      !*****************************************************************************************************************
      !->solve for acceleration and velocity at step n+1
      !*****************************************************************************************************************
      if (LG_OUTPUT_CPUTIME) then 
         t_resol = mpi_wtime()
      endif
   
      call newmark_end()
   
      if (LG_OUTPUT_CPUTIME) then 
         t_resol = mpi_wtime() - t_resol
      endif

      !      
      !      
      !*****************************************************************************************************************
      !->compute divergence and curl at free surface
      !*****************************************************************************************************************
      if (LG_SNAPSHOT_SURF_SPA_DER) then

         call compute_div_curl_gll()

      endif


      !
      !
      !*****************************************************************************************************************
      !->output snapshot of the surface of the domain + compute PGD, PGV and PGA
      !*****************************************************************************************************************
      if (LG_OUTPUT_CPUTIME) then 
         t_snafsu = mpi_wtime()
      endif

      if ( (lg_snapshot) .and. (.not.LG_SNAPSHOT_SURF_GLL) ) call write_snapshot_surface()

      if (lg_snapshot_volume)   call write_snapshot_volume()

      if (LG_SNAPSHOT_SURF_GLL .and. (ig_mpi_comm_fsurf /= MPI_COMM_NULL) ) call write_snapshot_surface_gll()

      if (LG_OUTPUT_CPUTIME) then
          t_snafsu = mpi_wtime() - t_snafsu
      endif

      !
      !
      !*****************************************************************************************************************
      !->output acceleration, velocity and displacement time history of receivers
      !*****************************************************************************************************************
      if (LG_OUTPUT_CPUTIME) then 
         t_outavd = mpi_wtime()
      endif

      if ( (ig_idt == 1) .or. mod((ig_idt-1),ig_receiver_saving_incr) == ZERO_IXP ) then

         call write_receiver_output()

      endif

      if (LG_OUTPUT_CPUTIME) then 
         t_outavd = mpi_wtime() - t_outavd
      endif

      !
      !
      !*****************************************************************************************************************
      !->output cpu time for each cpu for each time step + detail of different subroutines
      !*****************************************************************************************************************
      if (LG_OUTPUT_CPUTIME) then

         t_tloop = mpi_wtime() - t_tloop

         write(unit_time,'(i10,1x,11(e22.15,1x))') ig_idt                & !1  time step
                                                  ,t_tloop               & !2  computation of entire time loop
                                                  ,t_newmark             & !3  computation of displacement (step n+1)
                                                  ,t_forext              & !4  computation of external forces
                                                  ,t_forabs              & !5  computation of boundary absorption forces
                                                  ,t_forint_outer        & !6  computation of internal forces of outer elements
                                                  ,t_forint_inner        & !7  computation of internal forces of inner elements
                                                  ,t_linkfo              & !8  computation of assemble forces
                                                  ,t_resol               & !9  computation of acceleration and velocity (step n+1)
                                                  ,t_snafsu              & !10 computation of free surface snapshot
                                                  ,t_outavd              & !11 computation of receiver time history
                                                  ,t_link_init             !12 assemble_force_async_comm_init

         call flush(unit_time)

      endif
   
   enddo!loop on time step

!
!
!*********************************************************************************************************************
!*********************************************************************************************************************
!phase 3: output elapsed time info and finalize computation
!*********************************************************************************************************************
!*********************************************************************************************************************


   if ( (lg_snapshot) .and. (.not.LG_SNAPSHOT_SURF_GLL)) then

      !***************************************************************************************************************
      !write peak ground motion at the free surface
      !***************************************************************************************************************
      call write_peak_ground_motion()

      !***************************************************************************************************************
      !write VTK collection file if surface snapshots are saved in VTK format
      !***************************************************************************************************************
      if (LG_SNAPSHOT_VTK) call write_collection_vtk_surf()

   endif

   !
   !
   !***************************************************************************************************************
   !write VTK collection file if volume snapshot is activated
   !***************************************************************************************************************
   if (lg_snapshot_volume) then

      call write_collection_vtk_vol()

   endif

   if (LG_OUTPUT_CPUTIME) then 
      close(unit_time)
   endif

   if (LG_SNAPSHOT_SURF_GLL) then

      call mpi_file_close(ig_unit_snap_quad_gll_dis,ios)

      if (LG_SNAPSHOT_SURF_SPA_DER) then

         call mpi_file_close(ig_unit_snap_quadf_dis_spa_der,ios)
      
      endif

      if (ig_mpi_comm_fsurf /= MPI_COMM_NULL) then

        call mpi_comm_free(ig_mpi_comm_fsurf,ios)

      endif

   endif

   !
   !
   !***************************************************************************************************************
   !compute elapsed time for time loop for all cpu
   !***************************************************************************************************************
   end_time = (mpi_wtime() - start_time)
   call mpi_reduce(end_time,dltim2,ONE_IXP,MPI_DOUBLE_PRECISION,MPI_MAX,ZERO_IXP,ig_mpi_comm_simu,ios)

   !
   !
   !***************************************************************************************************************
   !cpu 0 gather elapsed time for time loop of each cpu
   !***************************************************************************************************************
   allocate(buffer_double_all_cpu(ig_ncpu))
   call mpi_gather(end_time,ONE_IXP,MPI_DOUBLE_PRECISION,buffer_double_all_cpu,ONE_IXP,MPI_DOUBLE_PRECISION,ZERO_IXP,ig_mpi_comm_simu,ios)

   !
   !
   !***************************************************************************************************************
   !cpu 0 gather number of hexa of each cpu
   !***************************************************************************************************************
   allocate(buffer_integer(ig_ncpu))
   call mpi_gather(ig_nhexa,ONE_IXP,MPI_INTEGER,buffer_integer,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)

   !
   !
   !***************************************************************************************************************
   !output results in file *.lst
   !***************************************************************************************************************
   if (ig_myrank == ZERO_IXP) then

      write(IG_LST_UNIT,'(" ",/,a,e15.7,a)') "elapsed time for initialization        = ",dltim1," s"
      write(IG_LST_UNIT,'(a,e15.7,a)')       "elapsed time for time loop computation = ",dltim2," s"
      write(IG_LST_UNIT,'(a,e15.7,a)')       "total elapsed time for computation     = ",dltim1+dltim2," s"
   
      write(IG_LST_UNIT,'("",/,a,i0,a)'    ) "average time per time step and per hexa (order ",IG_LAGRANGE_ORDER,") for the simulation"

      do icpu = ONE_IXP,ig_ncpu

         write(IG_LST_UNIT,'(a,i8,1x,e15.7,a)') " -->cpu ",icpu-ONE_IXP,buffer_double_all_cpu(icpu)/(real(ig_ndt,kind=R64)*real(buffer_integer(icpu),kind=R64))," s"

      enddo

   endif

   !
   !
   !***************************************************************************************************************
   !clear memory
   !***************************************************************************************************************
   call memory_deallocate_all_array()


!
!
!*********************************************************************************************************************
!*********************************************************************************************************************
!phase 4: post-processing
!*********************************************************************************************************************
!*********************************************************************************************************************
   if ( (LG_SNAPSHOT_SURF_GLL) .and. (LG_SNAPSHOT_SURF_GLL_FILTER) ) then

      call mpi_barrier(ig_mpi_comm_simu,ios)
 
      start_time = mpi_wtime()

      call filter_snapshot_surface_gll()

      end_time = (mpi_wtime() - start_time)

      call mpi_reduce(end_time,dltim2,ONE_IXP,mpi_double_precision,mpi_max,ZERO_IXP,ig_mpi_comm_simu,ios)

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'("",/a,e15.7,a)') "elapsed time for post-processing = ",dltim2," s"

      endif

   endif
  
   call mpi_comm_free(ig_mpi_comm_simu,ios) 
   call mpi_finalize(ios)

end program EFISPEC3D
