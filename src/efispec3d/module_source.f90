!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to compute information about sources.

!>@brief
!!This module contains subroutines to compute information about sources.
module mod_source

   use mpi

   use mod_precision

   use mod_global_variables, only : CIL

   implicit none

   public  :: compute_double_couple_source
   private :: convert_dcs_to_force
   public  :: init_fault
   private :: init_fault_coordinates
   private :: init_fault_magnitude
   private :: init_fault_rupture_time_serial
   private :: init_fault_stf
   private :: init_fault_user
   private :: write_fault_info
   public  :: init_double_couple_source
   public  :: init_single_force_source
   public  :: init_plane_wave_source
   private :: compute_source_local_coordinate
   private :: init_moment_tensor
   public  :: deg2rad

   contains

!
!
!>@brief
!!This subroutine check if double couple sources need to be computed
!***********************************************************************************************************************************************************************************
   subroutine compute_double_couple_source()
!***********************************************************************************************************************************************************************************
      
      use mod_global_variables, only : &
                                       tg_dcsource&
                                      ,tg_fault&
                                      ,ig_ndcsource&
                                      ,ig_nfault&
                                      ,ig_myrank&
                                      ,error_stop&
                                      ,IG_LST_UNIT

      implicit none

      integer(kind=IXP) :: ifault

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(" ",/,a)') "computing double-couple sources if any..."
         call flush(IG_LST_UNIT)

      endif

!
!---->(if any) compute double couple sources in the *.dcs file

      if (ig_ndcsource > ZERO_IXP) call convert_dcs_to_force(tg_dcsource)

!
!---->(if any) compute double couple sources of the faults

      do ifault = ONE_IXP,ig_nfault

         call convert_dcs_to_force(tg_fault(ifault)%dcsource)

      enddo

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(a)') "done"

         call flush(IG_LST_UNIT)

      endif
      
      return
!***********************************************************************************************************************************************************************************
   end subroutine compute_double_couple_source
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine pre-computes all double couple point sources defined by type mod_global_variables::type_double_couple_source as equivalent single force point sources (@f$ F^{ext}_{n+1} @f$)
!!distributed over hexahedron elements (see mod_global_variables::type_double_couple_source).
!>@return tl_dcsource%gll_force
!>@reference : Komatitsch and Tromp, 1999 \cite Komatitsch1999a; Komatitsch and Tromp, 2002 \cite Komatitsch2002b.
!***********************************************************************************************************************************************************************************
   subroutine convert_dcs_to_force(tl_dcsource)
!***********************************************************************************************************************************************************************************
      
      use mod_global_variables, only : &
                                       IG_NGLL&
                                      ,IG_NDOF&
                                      ,rg_hexa_gll_dxidx&
                                      ,rg_hexa_gll_dxidy&
                                      ,rg_hexa_gll_dxidz&
                                      ,rg_hexa_gll_detdx&
                                      ,rg_hexa_gll_detdy&
                                      ,rg_hexa_gll_detdz&
                                      ,rg_hexa_gll_dzedx&
                                      ,rg_hexa_gll_dzedy&
                                      ,rg_hexa_gll_dzedz&
                                      ,type_double_couple_source

      use mod_init_memory

      use mod_lagrange, only : lagrad,lagrap

      implicit none

      type(type_double_couple_source), dimension(:), intent(inout) :: tl_dcsource

      real   (kind=RXP)                                            :: ftmp(THREE_IXP,THREE_IXP,IG_NGLL,IG_NGLL,IG_NGLL)
                                                                   
      integer(kind=IXP)                                            :: nso
      integer(kind=IXP)                                            :: iso
      integer(kind=IXP)                                            :: iel
      integer(kind=IXP)                                            :: k
      integer(kind=IXP)                                            :: l
      integer(kind=IXP)                                            :: m
      integer(kind=IXP)                                            :: n
      integer(kind=IXP)                                            :: o
      integer(kind=IXP)                                            :: p



!**************************************************************************************************************!
!References: Komatitsch and Tromp, 1999 \cite Komatitsch1999a; Komatitsch and Tromp, 2002 \cite Komatitsch2002b!
!**************************************************************************************************************!

!
!
!********************************************************************************
!---->implementation of the double couple point source as the external forces
!********************************************************************************

      nso = size(tl_dcsource)

! 
!---->compute first part at gll node first, then an interpolation (cf. below) is done at the xi, eta, zeta of the source

      do iso = ONE_IXP,nso

         iel = tl_dcsource(iso)%p%iel
     
         do k = ONE_IXP,IG_NGLL
     
            do l = ONE_IXP,IG_NGLL
     
               do m = ONE_IXP,IG_NGLL
     
                  ftmp(1_IXP,1_IXP,m,l,k) = tl_dcsource(iso)%mxx*rg_hexa_gll_dxidx(m,l,k,iel) &
                                          + tl_dcsource(iso)%mxy*rg_hexa_gll_dxidy(m,l,k,iel) &
                                          + tl_dcsource(iso)%mxz*rg_hexa_gll_dxidz(m,l,k,iel)
                  ftmp(2_IXP,1_IXP,m,l,k) = tl_dcsource(iso)%mxx*rg_hexa_gll_detdx(m,l,k,iel) &
                                          + tl_dcsource(iso)%mxy*rg_hexa_gll_detdy(m,l,k,iel) &
                                          + tl_dcsource(iso)%mxz*rg_hexa_gll_detdz(m,l,k,iel)
                  ftmp(3_IXP,1_IXP,m,l,k) = tl_dcsource(iso)%mxx*rg_hexa_gll_dzedx(m,l,k,iel) &
                                          + tl_dcsource(iso)%mxy*rg_hexa_gll_dzedy(m,l,k,iel) &
                                          + tl_dcsource(iso)%mxz*rg_hexa_gll_dzedz(m,l,k,iel)
       
                  ftmp(1_IXP,2_IXP,m,l,k) = tl_dcsource(iso)%mxy*rg_hexa_gll_dxidx(m,l,k,iel) &
                                          + tl_dcsource(iso)%myy*rg_hexa_gll_dxidy(m,l,k,iel) &
                                          + tl_dcsource(iso)%myz*rg_hexa_gll_dxidz(m,l,k,iel)
                  ftmp(2_IXP,2_IXP,m,l,k) = tl_dcsource(iso)%mxy*rg_hexa_gll_detdx(m,l,k,iel) &
                                          + tl_dcsource(iso)%myy*rg_hexa_gll_detdy(m,l,k,iel) &
                                          + tl_dcsource(iso)%myz*rg_hexa_gll_detdz(m,l,k,iel)
                  ftmp(3_IXP,2_IXP,m,l,k) = tl_dcsource(iso)%mxy*rg_hexa_gll_dzedx(m,l,k,iel) &
                                          + tl_dcsource(iso)%myy*rg_hexa_gll_dzedy(m,l,k,iel) &
                                          + tl_dcsource(iso)%myz*rg_hexa_gll_dzedz(m,l,k,iel)
       
                  ftmp(1_IXP,3_IXP,m,l,k) = tl_dcsource(iso)%mxz*rg_hexa_gll_dxidx(m,l,k,iel) &
                                          + tl_dcsource(iso)%myz*rg_hexa_gll_dxidy(m,l,k,iel) &
                                          + tl_dcsource(iso)%mzz*rg_hexa_gll_dxidz(m,l,k,iel)
                  ftmp(2_IXP,3_IXP,m,l,k) = tl_dcsource(iso)%mxz*rg_hexa_gll_detdx(m,l,k,iel) &
                                          + tl_dcsource(iso)%myz*rg_hexa_gll_detdy(m,l,k,iel) &
                                          + tl_dcsource(iso)%mzz*rg_hexa_gll_detdz(m,l,k,iel)
                  ftmp(3_IXP,3_IXP,m,l,k) = tl_dcsource(iso)%mxz*rg_hexa_gll_dzedx(m,l,k,iel) &
                                          + tl_dcsource(iso)%myz*rg_hexa_gll_dzedy(m,l,k,iel) &
                                          + tl_dcsource(iso)%mzz*rg_hexa_gll_dzedz(m,l,k,iel)
     
               enddo
     
            enddo
     
         enddo
     
! 
!------->interpolation at the xi, eta, zeta of the source + multiplication by the test function
     
         do k = ONE_IXP,IG_NGLL
            do l = ONE_IXP,IG_NGLL
               do m = ONE_IXP,IG_NGLL
     
                  tl_dcsource(iso)%gll_force(:,m,l,k) = ZERO_RXP
     
                  do n = ONE_IXP,IG_NGLL
                     do o = ONE_IXP,IG_NGLL
                        do p = ONE_IXP,IG_NGLL
     
                           tl_dcsource(iso)%gll_force(1_IXP,m,l,k) = tl_dcsource(iso)%gll_force(1_IXP,m,l,k) &
                                           +                           lagrap(p,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrap(o,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrap(n,tl_dcsource(iso)%p%ze,IG_NGLL) &
                                           *(ftmp(1_IXP,1_IXP,p,o,n) * lagrad(m,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrap(l,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrap(k,tl_dcsource(iso)%p%ze,IG_NGLL) &
                                           + ftmp(2_IXP,1_IXP,p,o,n) * lagrap(m,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrad(l,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrap(k,tl_dcsource(iso)%p%ze,IG_NGLL) &
                                           + ftmp(3_IXP,1_IXP,p,o,n) * lagrap(m,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrap(l,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrad(k,tl_dcsource(iso)%p%ze,IG_NGLL))
        
                           tl_dcsource(iso)%gll_force(2_IXP,m,l,k) = tl_dcsource(iso)%gll_force(2_IXP,m,l,k) &
                                           +                           lagrap(p,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrap(o,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrap(n,tl_dcsource(iso)%p%ze,IG_NGLL) &
                                           *(ftmp(1_IXP,2_IXP,p,o,n) * lagrad(m,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrap(l,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrap(k,tl_dcsource(iso)%p%ze,IG_NGLL) &
                                           + ftmp(2_IXP,2_IXP,p,o,n) * lagrap(m,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrad(l,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrap(k,tl_dcsource(iso)%p%ze,IG_NGLL) &
                                           + ftmp(3_IXP,2_IXP,p,o,n) * lagrap(m,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrap(l,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrad(k,tl_dcsource(iso)%p%ze,IG_NGLL)) 
       
                           tl_dcsource(iso)%gll_force(3_IXP,m,l,k) = tl_dcsource(iso)%gll_force(3_IXP,m,l,k) &
                                           +                           lagrap(p,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrap(o,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrap(n,tl_dcsource(iso)%p%ze,IG_NGLL) &
                                           *(ftmp(1_IXP,3_IXP,p,o,n) * lagrad(m,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrap(l,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrap(k,tl_dcsource(iso)%p%ze,IG_NGLL) &
                                           + ftmp(2_IXP,3_IXP,p,o,n) * lagrap(m,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrad(l,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrap(k,tl_dcsource(iso)%p%ze,IG_NGLL) &
                                           + ftmp(3_IXP,3_IXP,p,o,n) * lagrap(m,tl_dcsource(iso)%p%xi,IG_NGLL) &
                                                                     * lagrap(l,tl_dcsource(iso)%p%et,IG_NGLL) &
                                                                     * lagrad(k,tl_dcsource(iso)%p%ze,IG_NGLL)) 
     
                        enddo
                     enddo
                  enddo
     
               enddo
            enddo
         enddo

      enddo
      
      return
!***********************************************************************************************************************************************************************************
   end subroutine convert_dcs_to_force
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine initializes extended fault; sets double couple point sources moment tensor; determines to which cpu belong double couple point sources and
!!computes local coordinates @f$\xi,\eta,\zeta@f$ of double couple point sources inside the hexahedron element it belongs.
!***********************************************************************************************************************************************************************************
   subroutine init_fault()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      ig_myrank&
                                     ,ig_nfault&
                                     ,ig_mpi_comm_simu&
                                     ,error_stop&
                                     ,tg_fault

      use mod_random_field_sp , only :&
                                      random_field_sp_2d&
                                     ,random_field_sp_2d_par&
                                     ,random_field_sp_2d_lift_zero&
                                     ,random_field_sp_2d_norm_sum

      use mod_efi_mpi         , only :&
                                      efi_mpi_comm_split

      use mod_init_memory     , only :&
                                      init_array_int

      use mod_receiver        , only :&
                                      locate_point_in_hexa

      implicit none

      integer(kind=IXP)  :: ifault
      integer(kind=IXP)  :: idcs
      integer(kind=IXP)  :: ndcs
      integer(kind=IXP)  :: id
      integer(kind=IXP)  :: is
      integer(kind=IXP)  :: ios

      character(len=CIL) :: info

!
!
!*******************************************************************************************************************
!---->exit subroutine if no fault defined
!*******************************************************************************************************************

      if (ig_nfault == ZERO_RXP) return

      
!
!
!*******************************************************************************************************************
!---->generate a 2D random field for each fault (all cpu does it)
!*******************************************************************************************************************

      do ifault = ONE_IXP,ig_nfault

         if (tg_fault(ifault)%acf == "vk") then !von Karman power spectral density

            call random_field_sp_2d_par(ls = tg_fault(ifault)%ls  &
                                       ,ld = tg_fault(ifault)%ld  &
                                       ,as = tg_fault(ifault)%lcs &
                                       ,ad = tg_fault(ifault)%lcd &
                                       ,k  = tg_fault(ifault)%k   &
                                       ,t  = tg_fault(ifault)%acf &
                                       ,ds = tg_fault(ifault)%ds  &
                                       ,dd = tg_fault(ifault)%dd  &
                                       ,rf = tg_fault(ifault)%rf)


         elseif (tg_fault(ifault)%acf == "gs") then !gaussian power spectral density

            call random_field_sp_2d(ls = tg_fault(ifault)%ls  & 
                                   ,ld = tg_fault(ifault)%ld  & 
                                   ,as = tg_fault(ifault)%lcs & 
                                   ,ad = tg_fault(ifault)%lcd & 
                                   ,t  = tg_fault(ifault)%acf & 
                                   ,ds = tg_fault(ifault)%ds  & 
                                   ,dd = tg_fault(ifault)%dd  & 
                                   ,rf = tg_fault(ifault)%rf)  

         elseif (tg_fault(ifault)%acf == "ct") then !no random slip. slip is spatially equal over the fault

            call random_field_sp_2d(ls = tg_fault(ifault)%ls  &
                                   ,ld = tg_fault(ifault)%ld  &
                                   ,t  = tg_fault(ifault)%acf &
                                   ,ds = tg_fault(ifault)%ds  &
                                   ,dd = tg_fault(ifault)%dd  &
                                   ,rf = tg_fault(ifault)%rf)

         elseif (tg_fault(ifault)%acf == "us") then !user slip given in file *.slp

            call init_fault_user(tg_fault(ifault))

         else

            write(info,'(a)') "error in mod_source:init_fault: unknown autocorrelation function"

            call error_stop(info)

         endif

      enddo

!
!
!*******************************************************************************************************************
!---->rotate and translate the faults according to their strike, dip and center. 
!     call after the subroutine 'random_field_sp_2d' in which the discretization step is initialized.
!*******************************************************************************************************************

      call init_fault_coordinates(tg_fault)

!
!
!*******************************************************************************************************************
!---->find which cpu and which hexa in this cpu contain the sub-faults
!*******************************************************************************************************************

      do ifault = ONE_IXP,ig_nfault

         ndcs = ZERO_IXP

         do id = ONE_IXP,tg_fault(ifault)%nd

            do is = ONE_IXP,tg_fault(ifault)%ns

               call locate_point_in_hexa(tg_fault(ifault)%p(is,id),ndcs)

            enddo

         enddo

!
!------->transfer of points data structure to 'dcsource'

         if (ndcs > ZERO_IXP) then

           !init double couple source array in cpu 'myrank'
            allocate(tg_fault(ifault)%dcsource(ndcs))

            idcs = ZERO_IXP

            do id = ONE_IXP,tg_fault(ifault)%nd
            
               do is = ONE_IXP,tg_fault(ifault)%ns
            
                  if (tg_fault(ifault)%p(is,id)%cpu == ig_myrank+ONE_IXP) then
                
                     idcs = idcs + ONE_IXP

                     tg_fault(ifault)%dcsource(idcs)%p = tg_fault(ifault)%p(is,id)

                     tg_fault(ifault)%dcsource(idcs)%rglo = (id-ONE_IXP)*tg_fault(ifault)%ns + is

                  endif
            
               enddo
            
            enddo

            tg_fault(ifault)%ndcsource = ndcs

         else
   
            tg_fault(ifault)%ndcsource = ZERO_IXP

         endif

         deallocate(tg_fault(ifault)%p)

      enddo

!
!---->create local mpi communicator for each fault

      do ifault = ONE_IXP,ig_nfault

         call efi_mpi_comm_split(tg_fault(ifault)%ndcsource,ifault,ig_mpi_comm_simu,tg_fault(ifault)%mpi_comm)

         if (tg_fault(ifault)%mpi_comm /= MPI_COMM_NULL) then

            call mpi_comm_rank(tg_fault(ifault)%mpi_comm,tg_fault(ifault)%mpi_rank,ios)
            call mpi_comm_size(tg_fault(ifault)%mpi_comm,tg_fault(ifault)%mpi_ncpu,ios)

         else

            tg_fault(ifault)%mpi_ncpu = MPI_PROC_NULL
            tg_fault(ifault)%mpi_rank = MPI_PROC_NULL
            tg_fault(ifault)%mpi_comm = MPI_COMM_NULL

         endif

      enddo

!
!
!*******************************************************************************************************************
!---->compute information for sources that belong to cpu myrank
!*******************************************************************************************************************

      do ifault = ONE_IXP,ig_nfault

         do idcs = ONE_IXP,tg_fault(ifault)%ndcsource

            call compute_source_local_coordinate(tg_fault(ifault)%dcsource(idcs))

         enddo

      enddo

!
!
!*******************************************************************************************************************
!---->set the seismic moment (and moment magnitude) of each subfault
!*******************************************************************************************************************

!
!---->set up the random field so that it is larger than zero and normalized

      do ifault = ONE_IXP,ig_nfault

         call random_field_sp_2d_lift_zero(tg_fault(ifault)%rf)

         call random_field_sp_2d_norm_sum(tg_fault(ifault)%rf)

      enddo

      call init_fault_magnitude(tg_fault)

      call init_fault_rupture_time_serial(tg_fault)

      call init_fault_stf(tg_fault)

      call write_fault_info(tg_fault)

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_fault
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine initializes the 2D fault coordinates in the coordinates system of the domain of simulation 
!***********************************************************************************************************************************************************************************
   subroutine init_fault_coordinates(tl_fault)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      type_fault&
                                     ,ig_myrank&
                                     ,cg_prefix&
                                     ,IG_LST_UNIT

      use mod_init_memory

      implicit none

      type(type_fault) , dimension(:), intent(inout) :: tl_fault

      real   (kind=RXP), dimension(3,3)              :: m
      real   (kind=RXP), dimension(3,3)              :: m1
      real   (kind=RXP), dimension(3,3)              :: m2
      real   (kind=RXP), dimension(3,3)              :: m3
      real   (kind=RXP), dimension(3)                :: s
                                                     
      real   (kind=RXP)                              :: x0
      real   (kind=RXP)                              :: y0
      real   (kind=RXP)                              :: ls
      real   (kind=RXP)                              :: ld
      real   (kind=RXP)                              :: ds
      real   (kind=RXP)                              :: dd
      real   (kind=RXP)                              :: st
      real   (kind=RXP)                              :: st_rad
      real   (kind=RXP)                              :: di
      real   (kind=RXP)                              :: di_rad
      real   (kind=RXP)                              :: xc
      real   (kind=RXP)                              :: yc
      real   (kind=RXP)                              :: zc
                                                     
      integer(kind=IXP)                              :: nfault
      integer(kind=IXP)                              :: ifault
      integer(kind=IXP)                              :: is
      integer(kind=IXP)                              :: id
      integer(kind=IXP)                              :: ns
      integer(kind=IXP)                              :: nd
      integer(kind=IXP)                              :: ios
      integer(kind=IXP)                              :: myunit

      character(len=6)                               :: cfault

      nfault = size(tl_fault)

      do ifault = ONE_IXP,nfault

!
!------->set up local variable
     
         ls = tl_fault(ifault)%ls
         ld = tl_fault(ifault)%ld
         ds = tl_fault(ifault)%ds
         dd = tl_fault(ifault)%dd
         st = tl_fault(ifault)%str 
         di = tl_fault(ifault)%dip
         xc = tl_fault(ifault)%xc
         yc = tl_fault(ifault)%yc
         zc = tl_fault(ifault)%zc
     
         st_rad = st*PI_RXP/180.0_RXP
         di_rad = di*PI_RXP/180.0_RXP
     
!   
!------->re-compute ns and nd from lengths and discretization steps
     
         ns = int(ls/ds)
         nd = int(ld/dd)
     
         tl_fault(ifault)%ns = ns
         tl_fault(ifault)%nd = nd
     
!   
!------->init memory for coordinates
     
         ios = init_array_point3d(tl_fault(ifault)%p,nd,ns,"mod_source:init_fault_coordinates:tl_fault(ifault)%p")
     
!   
!------->clockwise rotation around the z-axis of angle 'azimuth' (zero from north = y-axis)
         
         do id = ONE_IXP,nd
     
            x0 = -ld/2.0_RXP + dd/2.0_RXP + real(id-ONE_IXP,kind=RXP)*dd
     
            do is = ONE_IXP,ns
     
               y0 = -ls/2.0_RXP + ds/2.0_RXP + real(is-ONE_IXP,kind=RXP)*ds
     
               tl_fault(ifault)%p(is,id)%x = + x0*cos(-st_rad) - y0*sin(-st_rad)
               tl_fault(ifault)%p(is,id)%y = + x0*sin(-st_rad) + y0*cos(-st_rad)
     
            enddo
     
         enddo
     
!   
!------->rotation around strike-axis of angle 'dip' (Rodrigues rotation formula)
     
         !unit vector along strike (in the horizontal plane)
         s(1) = -sin(-st_rad)
         s(2) = +cos(-st_rad)
         s(3) = 0.0_RXP
     
         m1 = transpose(reshape([1.0_RXP, 0.0_RXP, 0.0_RXP, 0.0_RXP, 1.0_RXP, 0.0_RXP, 0.0_RXP, 0.0_RXP, 1.0_RXP],shape(m1)))
     
         m2 = transpose(reshape([s(1)*s(1), s(1)*s(2), s(1)*s(3), s(1)*s(2), s(2)*s(2), s(2)*s(3), s(1)*s(3), s(2)*s(3), s(3)*s(3)],shape(m2)))
     
         m3 = transpose(reshape([0.0_RXP, -s(3), s(2), s(3), 0.0_RXP, -s(1), -s(2), s(1), 0.0_RXP],shape(m3)))
     
         m  = cos(di_rad)*m1 + (1.0_RXP - cos(di_rad))*m2 + sin(di_rad)*m3
     
         do id = ONE_IXP,nd
     
            do is = ONE_IXP,ns
     
               x0 = tl_fault(ifault)%p(is,id)%x
               y0 = tl_fault(ifault)%p(is,id)%y
     
               tl_fault(ifault)%p(is,id)%x =  x0*m(1,1) + y0*m(1,2)
               tl_fault(ifault)%p(is,id)%y =  x0*m(2,1) + y0*m(2,2)
               tl_fault(ifault)%p(is,id)%z =  x0*m(3,1) + y0*m(3,2)
     
            enddo
     
         enddo
     
!   
!------->translation to the center of the fault in the coordinates system of the domain of simulation
     
         do id = ONE_IXP,nd
     
            do is = ONE_IXP,ns
     
               tl_fault(ifault)%p(is,id)%x = tl_fault(ifault)%p(is,id)%x + xc
               tl_fault(ifault)%p(is,id)%y = tl_fault(ifault)%p(is,id)%y + yc  
               tl_fault(ifault)%p(is,id)%z = tl_fault(ifault)%p(is,id)%z + zc 
     
            enddo
     
         enddo

!
!------->cpu0 writes in listing file the minmax of the fault

         if (ig_myrank == ZERO_IXP) then

            write(IG_LST_UNIT,'(/,a,i0   )') "fault ",ifault
            write(IG_LST_UNIT,'(a,2(E15.7,1X)  )') " --> min/max sub-fault center along x-direction = ",minval(tl_fault(ifault)%p(:,:)%x),maxval(tl_fault(ifault)%p(:,:)%x)
            write(IG_LST_UNIT,'(a,2(E15.7,1X)  )') " --> min/max sub-fault center along y-direction = ",minval(tl_fault(ifault)%p(:,:)%y),maxval(tl_fault(ifault)%p(:,:)%y)
            write(IG_LST_UNIT,'(a,2(E15.7,1X)  )') " --> min/max sub-fault center along z-direction = ",minval(tl_fault(ifault)%p(:,:)%z),maxval(tl_fault(ifault)%p(:,:)%z)

!
!---------->cpu0 writes fault coordinates into file *fault.*.dcs

            write(cfault,'(i6.6)') ifault
            
            open(newunit=myunit,file=trim(cg_prefix)//".fault."//trim(cfault)//".dcs",action="write")
            
            write(unit=myunit,fmt='(i0)') nd*ns
            
            do id = ONE_IXP,nd
            
               do is = ONE_IXP,ns
            
                  write(unit=myunit,fmt='(3(e15.7,1x))') tl_fault(ifault)%p(is,id)%x,tl_fault(ifault)%p(is,id)%y,tl_fault(ifault)%p(is,id)%z
            
               enddo
            
            enddo
            
            close(myunit)         

         endif

      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_fault_coordinates
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine initializes the 2D fault coordinates in the coordinates system of the domain of simulation 
!***********************************************************************************************************************************************************************************
   subroutine init_fault_magnitude(tl_fault)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      type_fault&
                                     ,ig_myrank&
                                     ,ig_mpi_comm_simu&
                                     ,ig_ncpu&
                                     ,rg_hexa_gll_rhovs2&
                                     ,IG_NGLL&
                                     ,cg_prefix

      use mod_init_memory

      use mod_lagrange, only :&
                              hexa_lagrange_interp&
                             ,lagrap

      implicit none

      type(type_fault), dimension(:), intent(inout)         :: tl_fault
                                                  
      real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL) :: mu_gll
      real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL) :: la_gll

      real   (kind=R64)                                     :: m64
      real   (kind=R64)                                     :: m0
      real   (kind=R64)                                     :: m0_sum
      real   (kind=R64)                                     :: m0_sum_all_cpus
      real   (kind=R64)                                     :: mw
                                                            
      real   (kind=RXP)                                     :: mxp
      real   (kind=RXP)                                     :: s
      real   (kind=RXP)                                     :: d
      real   (kind=RXP)                                     :: r
      real   (kind=RXP)                                     :: ds
      real   (kind=RXP)                                     :: dd
      real   (kind=RXP)                                     :: mu
      real   (kind=RXP)                                     :: xi
      real   (kind=RXP)                                     :: et
      real   (kind=RXP)                                     :: ze
                                                            
      integer(kind=IXP)                                     :: nfault
      integer(kind=IXP)                                     :: ifault
      integer(kind=IXP)                                     :: ihexa
      integer(kind=IXP)                                     :: g
      integer(kind=IXP)                                     :: idcs
      integer(kind=IXP)                                     :: is
      integer(kind=IXP)                                     :: id
      integer(kind=IXP)                                     :: ns
      integer(kind=IXP)                                     :: nd
      integer(kind=IXP)                                     :: myunit
      integer(kind=IXP)                                     :: ios
      integer(kind=IXP)                                     :: kgll
      integer(kind=IXP)                                     :: lgll
      integer(kind=IXP)                                     :: mgll
                                                            
      character(len=6)                                      :: cfault

      nfault = size(tl_fault)

      do ifault = ONE_IXP,nfault

         ns = tl_fault(ifault)%ns
         nd = tl_fault(ifault)%nd
       
         ds = tl_fault(ifault)%ds
         dd = tl_fault(ifault)%dd

         m0 = real(10_RXP**(1.5_RXP*tl_fault(ifault)%mw + 9.1_RXP),kind=R64)

         m0_sum = 0.0_R64
       
         s  = deg2rad(tl_fault(ifault)%str) 
         d  = deg2rad(tl_fault(ifault)%dip) 
         r  = deg2rad(tl_fault(ifault)%rak)

         do idcs = ONE_IXP,tl_fault(ifault)%ndcsource
      
!
!---------->compute local seismic moment and moment magnitude of each sub-fault
 
            g  = tl_fault(ifault)%dcsource(idcs)%rglo 
       
            id = int( (g-1_IXP)/ns ) + 1_IXP !local numbering from global
            is = mod( (g-1_IXP),ns ) + 1_IXP !local numbering from global
       
            m64 = real(tl_fault(ifault)%rf(is,id),kind=R64)*m0 !scale m0 wrt the random field 'rf'

            m0_sum = m0_sum + m64
       
            mxp = real(m64,kind=RXP)
       
            mw  = (log10(m64) - 9.1_R64)/1.5_R64
       
            tl_fault(ifault)%dcsource(idcs)%mw  = real(mw,kind=RXP)
       
            call init_moment_tensor(s,d,r,mxp&
                                   ,tl_fault(ifault)%dcsource(idcs)%mxx&
                                   ,tl_fault(ifault)%dcsource(idcs)%mxy&
                                   ,tl_fault(ifault)%dcsource(idcs)%mxz&
                                   ,tl_fault(ifault)%dcsource(idcs)%myy&
                                   ,tl_fault(ifault)%dcsource(idcs)%myz&
                                   ,tl_fault(ifault)%dcsource(idcs)%mzz)
       
            tl_fault(ifault)%dcsource(idcs)%str = tl_fault(ifault)%str
            tl_fault(ifault)%dcsource(idcs)%dip = tl_fault(ifault)%dip
            tl_fault(ifault)%dcsource(idcs)%rak = tl_fault(ifault)%rak

!
!---------->init array containing shear-wave modulus for interpolation

            xi = tl_fault(ifault)%dcsource(idcs)%p%xi
            et = tl_fault(ifault)%dcsource(idcs)%p%et
            ze = tl_fault(ifault)%dcsource(idcs)%p%ze

            ihexa = tl_fault(ifault)%dcsource(idcs)%p%iel

            do kgll = ONE_IXP,IG_NGLL

               do lgll = ONE_IXP,IG_NGLL

                  do mgll = ONE_IXP,IG_NGLL

                     mu_gll(mgll,lgll,kgll) = rg_hexa_gll_rhovs2(mgll,lgll,kgll,ihexa)
              
                  enddo

               enddo

            enddo

!
!---------->init array containing lagrange polynomial products for interpolation

            do kgll = ONE_IXP,IG_NGLL

               do lgll = ONE_IXP,IG_NGLL

                  do mgll = ONE_IXP,IG_NGLL

                     la_gll(mgll,lgll,kgll) = lagrap(mgll,xi,IG_NGLL)*lagrap(lgll,et,IG_NGLL)*lagrap(kgll,ze,IG_NGLL)

                  enddo

               enddo

            enddo

!
!---------->interpolation at the element level and store shear wave modulus into 'tl_fault(ifault)%dcsource(idcs)%mu'

            call hexa_lagrange_interp(mu_gll,la_gll,mu)

            tl_fault(ifault)%dcsource(idcs)%mu = mu

!
!---------->compute displacement slip of each sub-fault 

            tl_fault(ifault)%dcsource(idcs)%slip = mxp/(mu*ds*dd)

         enddo

!
!------->cpu 0 sum 'm0_sum' and bcast

         call mpi_reduce(m0_sum,m0_sum_all_cpus,ONE_IXP,MPI_DOUBLE_PRECISION,MPI_SUM,ZERO_IXP,ig_mpi_comm_simu,ios)

         call mpi_bcast(m0_sum_all_cpus,ONE_IXP,MPI_DOUBLE_PRECISION,ZERO_IXP,ig_mpi_comm_simu,ios)

         tl_fault(ifault)%m0 = m0_sum_all_cpus

!
!------->last cpu write the random field to disk
   
         write(cfault,'(I6.6)') ifault

         if (ig_myrank == ig_ncpu-ONE_IXP) then

            open(newunit=myunit,file=trim(cg_prefix)//".fault."//trim(cfault)//".rf",form="unformatted",access="stream",action="write")

            write(myunit) ns,nd,ds,dd

            write(myunit) tl_fault(ifault)%rf(:,:)

            close(myunit)

         endif

         deallocate(tl_fault(ifault)%rf)

      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_fault_magnitude
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine initializes rupture time by solving the eikonal equation in the fault plane
!***********************************************************************************************************************************************************************************
   subroutine init_fault_rupture_time_serial(tl_fault)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,type_fault&
                                     ,rg_hexa_gll_rho&
                                     ,rg_hexa_gll_rhovs2

      use mod_init_memory

      use mod_lagrange, only :&
                              hexa_lagrange_interp&
                             ,lagrap

      use mod_efi_mpi

      use mod_fmm

      use mod_heap

      use mod_io_array, only : efi_mpi_file_open

      implicit none

      type(type_fault) , dimension(:), target, intent(inout) :: tl_fault

      type(theap)                                            :: h

      real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)  :: vs_gll
      real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL)  :: la_gll
      real   (kind=RXP), dimension(:)  , allocatable, target :: vs
      real   (kind=RXP), dimension(:)  , pointer             :: p_vs
      real   (kind=RXP), dimension(:)  , allocatable         :: vs_all_cpus
      real   (kind=RXP), dimension(:,:), allocatable         :: u
      real   (kind=RXP), dimension(:,:), allocatable         :: vs_fmm
      real   (kind=RXP)                                      :: xi
      real   (kind=RXP)                                      :: et
      real   (kind=RXP)                                      :: ze
      real   (kind=RXP)                                      :: ds
      real   (kind=RXP)                                      :: dd
                                                             
      integer(kind=IXP), dimension(:,:), allocatable         :: l
      integer(kind=IXP), dimension(:)  , allocatable         :: rglo_all_cpus
      integer(kind=IXP), dimension(:)  , pointer             :: p_rglo

      integer(kind=IXP), parameter                           :: NDIMS = 2_IXP
     !integer(kind=IXP), dimension(NDIMS)                    :: dims 
     !integer(kind=IXP), dimension(NDIMS)                    :: coords 
     !logical(kind=IXP), dimension(NDIMS)                    :: periods
      integer(kind=IXP)                                      :: nfault
      integer(kind=IXP)                                      :: ifault
      integer(kind=IXP)                                      :: idcs
      integer(kind=IXP)                                      :: ihexa
     !integer(kind=IXP)                                      :: il_mpi_comm_fault
      integer(kind=IXP)                                      :: ios
      integer(kind=IXP)                                      :: g
      integer(kind=IXP)                                      :: is
      integer(kind=IXP)                                      :: id
      integer(kind=IXP)                                      :: ns
      integer(kind=IXP)                                      :: nd
     !integer(kind=IXP)                                      :: is_start
     !integer(kind=IXP)                                      :: is_end
     !integer(kind=IXP)                                      :: id_start
     !integer(kind=IXP)                                      :: id_end
     !integer(kind=IXP)                                      :: tag_glonum
      integer(kind=IXP)                                      :: kgll
      integer(kind=IXP)                                      :: lgll
      integer(kind=IXP)                                      :: mgll
      integer(kind=IXP)                                      :: nfmm_s
      integer(kind=IXP)                                      :: nfmm_d
      integer(kind=IXP)                                      :: ish
      integer(kind=IXP)                                      :: idh
      integer(kind=IXP)                                      :: rank_fmm
                                                             
     !logical(kind=IXP)                                      :: reorder


!!
!!---->create local mpi communicator associated with the resolution of the eikonal equation in the fault 
!
!      dims(1_IXP) = ZERO_IXP !let mpi_dims_create choose the number of cpu in each direction of the grid 
!      dims(2_IXP) = ZERO_IXP
!
!      call mpi_dims_create(ig_ncpu,NDIMS,dims,ios)
!
!      print *,'dims = ',dims(:)
!
!      periods(1_IXP) = .false.
!      periods(2_IXP) = .false.
!
!      reorder = .false. !keep the rank ordering of ig_mpi_comm_simu
!
!      call mpi_cart_create(ig_mpi_comm_simu,NDIMS,dims,periods,reorder,il_mpi_comm_fault,ios)
!
!!
!!---->get the local coordinates in the cartesian grid of the rank 'ig_myrank'
!
!      call mpi_cart_coords(il_mpi_comm_fault,ig_myrank,NDIMS,coords,ios)
!
!      print *,"coords = ",ig_myrank,coords(:)


!
!---->loop on the faults

      nfault = size(tl_fault)

      do ifault = ONE_IXP,nfault

     
!
!------->only cpus in the fault communicator "tl_fault(ifault)%mpi_comm" works
  
         if (tl_fault(ifault)%mpi_comm /= MPI_COMM_NULL) then
       
!
!---------->set which cpu in mpi_communicator of the fault (tg_fault%mpi_comm) solves the eikonal equation using the fast marching method

            if (tl_fault(ifault)%mpi_rank == ZERO_IXP) rank_fmm = tl_fault(ifault)%mpi_rank

!
!---------->cpu 'rank_fmm' gathers subfault global numbering from cpus computing part of the fault.
!           tl_fault(ifault)%dcsource(:) can be not allocated if no dcs is computed by cpu myrank. To avoid compiler complains, a pointer is used as arguement of
!           the subroutine instead
      
            p_rglo => tl_fault(ifault)%dcsource(:)%rglo
      
            call efi_mpi_gatherv(tl_fault(ifault)%mpi_ncpu,tl_fault(ifault)%mpi_rank,rank_fmm,tl_fault(ifault)%ndcsource,p_rglo,tl_fault(ifault)%mpi_comm,rglo_all_cpus)
      
!     
!---------->cpus which have double couple point sources compute the shear wave velocity of the medium at the location of the point source
      
            ios = init_array_real(vs,tl_fault(ifault)%ndcsource,"mod_source:init_fault_rupture_time_serial:vs")
      
            do idcs = ONE_IXP,tl_fault(ifault)%ndcsource
      
               xi = tl_fault(ifault)%dcsource(idcs)%p%xi
               et = tl_fault(ifault)%dcsource(idcs)%p%et
               ze = tl_fault(ifault)%dcsource(idcs)%p%ze
      
               ihexa = tl_fault(ifault)%dcsource(idcs)%p%iel
      
!     
!------------->init array containing s-wave velocity for interpolation
      
               do kgll = ONE_IXP,IG_NGLL
      
                  do lgll = ONE_IXP,IG_NGLL
      
                     do mgll = ONE_IXP,IG_NGLL
      
                        vs_gll(mgll,lgll,kgll) = sqrt(rg_hexa_gll_rhovs2(mgll,lgll,kgll,ihexa)/rg_hexa_gll_rho(mgll,lgll,kgll,ihexa))
                 
                     enddo
      
                  enddo
      
               enddo
      
!     
!------------->init array containing lagrange polynomial products for interpolation
      
               do kgll = ONE_IXP,IG_NGLL
      
                  do lgll = ONE_IXP,IG_NGLL
      
                     do mgll = ONE_IXP,IG_NGLL
      
                        la_gll(mgll,lgll,kgll) = lagrap(mgll,xi,IG_NGLL)*lagrap(lgll,et,IG_NGLL)*lagrap(kgll,ze,IG_NGLL)
      
                     enddo
      
                  enddo
      
               enddo
      
!     
!------------->interpolation at the element level
      
               call hexa_lagrange_interp(vs_gll,la_gll,vs(idcs))
      
            enddo
      
!     
!---------->scale shear-wave velocity by velocity rupture ratio before solving the eikonal equation
      
            do idcs = ONE_IXP,tl_fault(ifault)%ndcsource
      
               vs(idcs) = vs(idcs)*tl_fault(ifault)%vrr
      
            enddo
      
!     
!---------->cpu 'rank_fmm' gather 'vs' from cpus computing part of the fault
      
            p_vs => vs(:)
      
            call efi_mpi_gatherv(tl_fault(ifault)%mpi_ncpu,tl_fault(ifault)%mpi_rank,rank_fmm,tl_fault(ifault)%ndcsource,p_vs,tl_fault(ifault)%mpi_comm,vs_all_cpus)
      
            deallocate(vs)
      
!     
!*************************************************************************************************************************************************************
!---------->solve eikonal equation with the fast-marching method using the shear-wave velocity on the fault
!*************************************************************************************************************************************************************

            ns = tl_fault(ifault)%ns
            nd = tl_fault(ifault)%nd
      
            ds = tl_fault(ifault)%ds
            dd = tl_fault(ifault)%dd
      
            nfmm_s = ns + 2_IXP !create a ghost zone of two cells around the physical domain
            nfmm_d = nd + 2_IXP !create a ghost zone of two cells around the physical domain
                   
!
!---------->u = rupture times array

            ios = init_array_real(u,nfmm_d,nfmm_s,"mod_source:init_fault_rupture_time_serial:u")

!     
!---------->only cpu 'rank_fmm' solves the eikonal equation. after solving, solution is sent to cpus computing the fault
      
            if (tl_fault(ifault)%mpi_rank == rank_fmm) then
      
               ios = init_array_real(vs_fmm,nfmm_d,nfmm_s,"mod_source:init_fault_rupture_time:vs_fmm")
               ios = init_array_int (l     ,nfmm_d,nfmm_s,"mod_source:init_fault_rupture_time_serial:l")
      
               do idcs = ONE_IXP,size(vs_all_cpus)
      
                  g  = rglo_all_cpus(idcs)
      
                  id = int( (g-1_IXP)/ns ) + 1_IXP
                  is = mod( (g-1_IXP),ns ) + 1_IXP
      
                  vs_fmm(is+1_IXP,id+1_IXP) = vs_all_cpus(idcs)
      
               enddo
      
               deallocate(vs_all_cpus)
      
!     
!------------->fill in the ghost zone with the shear-wave velocity of the neighbours
           
               !corners
               vs_fmm(1_IXP , 1_IXP) = vs_fmm(2_IXP       ,       2_IXP)
               vs_fmm(nfmm_s, 1_IXP) = vs_fmm(nfmm_s-1_IXP,       2_IXP)
               vs_fmm(nfmm_s,nfmm_d) = vs_fmm(nfmm_s-1_IXP,nfmm_d-1_IXP)
               vs_fmm(1_IXP ,nfmm_d) = vs_fmm(       2_IXP,nfmm_d-1_IXP)
              
               !lines
               vs_fmm(2_IXP:nfmm_s-1_IXP, 1_IXP) = vs_fmm(2_IXP:nfmm_s-1_IXP,       2_IXP)  
               vs_fmm(2_IXP:nfmm_s-1_IXP,nfmm_d) = vs_fmm(2_IXP:nfmm_s-1_IXP,nfmm_d-1_IXP)
               vs_fmm(1_IXP,2_IXP:nfmm_d-1_IXP ) = vs_fmm(2_IXP,2_IXP:nfmm_d-1_IXP       ) 
               vs_fmm(nfmm_s,2_IXP:nfmm_d-1_IXP) = vs_fmm(nfmm_s-1_IXP,2_IXP:nfmm_d-1_IXP)
           
               !row and column of the hypocenter
               ish = int(tl_fault(ifault)%sh/ds,kind=IXP) + 1_IXP !adding one for ghost zone (same below for min/max)
               idh = int(tl_fault(ifault)%dh/dd,kind=IXP) + 1_IXP !adding one for ghost zone (same below for min/max)
      
               ish = max(ish,2_IXP) 
               ish = min(ish,tl_fault(ifault)%ns+1_IXP)
           
               idh = max(idh,2_IXP)
               idh = min(idh,tl_fault(ifault)%ns+1_IXP)
           
!          
!------------->step 1: init source 'interface' (just one cell in this case)
           
               u(:,:) = huge(u)
               l(:,:) = 2_IXP
           
               u(ish,idh) = 0.0_RXP
               l(ish,idh) = 0_IXP
           
!          
!------------->step 2: init heap
           
               call fmm_init_heap(vs_fmm,u,ds,dd,l,h)
      
!          
!------------->step 3: march narrow band
           
               call fmm_march_narrow_band(vs_fmm,u,ds,dd,l,h)
           
!          
!------------->free memory required by fault 'ifault'
           
               deallocate(vs_fmm,l)
           
               call h%release()
      
            endif
      
!     
!---------->cpu 'rank_fmm' broadcast all rupture times to the cpus computing part of the fault

            call mpi_bcast(u,nfmm_d*nfmm_s,MPI_REAL,rank_fmm,tl_fault(ifault)%mpi_comm,ios)

!
!---------->cpus in communicator "tl_fault(ifault)%mpi_comm" fill their array tl_fault()%dcsource()%shift_time given from fast marching method solution 'u'

            do idcs = ONE_IXP,tl_fault(ifault)%ndcsource
            
               g  = tl_fault(ifault)%dcsource(idcs)%rglo
               
               id = int( (g-1_IXP)/ns ) + 1_IXP
               is = mod( (g-1_IXP),ns ) + 1_IXP
            
               tl_fault(ifault)%dcsource(idcs)%shift_time = tl_fault(ifault)%ts + u(is+1_IXP,id+1_IXP) !+1 in each dimension needed to go out from the ghost zone
            
            enddo
 
!
!---------->cpus in communicator "tl_fault(ifault)%mpi_comm" fill their array tl_fault()%dcsource()%rise_time

            do idcs = ONE_IXP,tl_fault(ifault)%ndcsource
      
               tl_fault(ifault)%dcsource(idcs)%rise_time = tl_fault(ifault)%rt
      
            enddo

            deallocate(rglo_all_cpus)
            deallocate(u)

!!           if (tl_fault(ifault)%mpi_rank == rank_fmm) then
!      
!               ios = init_array_int(ndcs_all_cpus,tl_fault(ifault)%mpi_ncpu,"mod_source:init_fault_rupture_time_serial:ndcs_all_cpus")
!      
!!           endif
!      
!            call mpi_gather(tl_fault(ifault)%ndcsource,ONE_IXP,MPI_INTEGER,ndcs_all_cpus,ONE_IXP,MPI_INTEGER,rank_fmm,tl_fault(ifault)%mpi_comm,ios)
!      
!            if (tl_fault(ifault)%mpi_rank == rank_fmm) then
!      
!               ndcs_offset = ZERO_IXP
!      
!               do icpu = ONE_IXP,tl_fault(ifault)%mpi_ncpu
!      
!                  ndcs = ndcs_all_cpus(icpu)
!      
!                  if (ndcs > ZERO_IXP) then
!      
!                     ios = init_array_real(rupt_time,ndcs,"mod_source:init_fault_rupture_time_serial:rupt_time")
!                   
!                     do idcs = ONE_IXP,ndcs
!                   
!                        g  = rglo_all_cpus(ndcs_offset + idcs)
!                        
!                        id = int( (g-1_IXP)/ns ) + 1_IXP
!                        is = mod( (g-1_IXP),ns ) + 1_IXP
!                   
!                        rupt_time(idcs) = u(is+1_IXP,id+1_IXP) !+1 in each dimension needed to go out from the ghost zone
!                   
!                     enddo
!                                   
!                     call mpi_send(rupt_time,ndcs,MPI_REAL,icpu-ONE_IXP,10_IXP,tl_fault(ifault)%mpi_comm,ios)
!                   
!                     deallocate(rupt_time)
!      
!                     ndcs_offset = ndcs_offset + ndcs
!      
!                  endif
!      
!               enddo
!      
!               deallocate(ndcs_all_cpus)
!               deallocate(rglo_all_cpus)
!               deallocate(u)
!      
!            endif
!      
!!     
!!---------->cpus computing the double couple sources of the fault fill in their values 'shift_time' and 'rise_time'
!      
!            ndcs = tl_fault(ifault)%ndcsource
!      
!            ios = init_array_real(rupt_time,ndcs,"mod_source:init_fault_rupture_time_serial:rupt_time")
!      
!            call mpi_recv(rupt_time,ndcs,MPI_REAL,rank_fmm,10_IXP,tl_fault(ifault)%mpi_comm,stat,ios)
!      
!            do idcs = ONE_IXP,ndcs
!      
!               tl_fault(ifault)%dcsource(idcs)%shift_time = tl_fault(ifault)%ts + rupt_time(idcs)
!      
!            enddo
!      
!            deallocate(rupt_time)
!      
!            do idcs = ONE_IXP,ndcs
!      
!               tl_fault(ifault)%dcsource(idcs)%rise_time = tl_fault(ifault)%rt
!      
!            enddo
      
!!   
!!---------->distribute [ns x nd] sub-faults over the cartesian grid defined in il_mpi_comm_fault
!     
!            ns = tl_fault(ifault)%ns
!            nd = tl_fault(ifault)%nd
!     
!            print *,'nd = ',nd
!            print *,'ns = ',ns
!            
!            call partition1d(coords(1_IXP),dims(1_IXP),ns,is_start,is_end)
!            print *,'s= ',ig_myrank,is_start,is_end
!     
!            call partition1d(coords(2_IXP),dims(2_IXP),nd,id_start,id_end)
!            print *,'d = ',ig_myrank,id_start,id_end
!     
!!   
!!---------->fill in local array 'vs' on the cartesian grid
!     
!            do id = id_start,id_end
!     
!               do is = is_start,is_end
!     
!                  if (tl_fault(ifault)%p%(is,id)%cpu == ig_myrank+ONE_IXP) then !cpu 'ig_myrank' contains the cell is,id
!     
!                     !vs(is,id) = 
!     
!                  else!receive vs_gll value from the cpu containing cell is,id.
!     
!                     call mpi_recv
!     
!!   
!!------------------->init gll values to be sent
!     
!!   
!!------------------->send values. tag is the global sub-fault number which is unique.
!                     tag_glonum = (id-ONE_IXP)*ns + is
!     
!                     call mpi_send(vs_gll,IG_NGLL**3_IXP,MPI_REAL,tl_fault(ifault)%p%(is,id)%cpu-ONE_IXP,tag_glonum,ig_mpi_comm_simu,ios)
!     
!!   
!!------------------->
!     
!                  endif
!             
!               enddo
!     
!            enddo

         endif !if fault communicator

      enddo                  
           
      return
!***********************************************************************************************************************************************************************************
   end subroutine init_fault_rupture_time_serial
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine initializes the source time function of the faults 
!***********************************************************************************************************************************************************************************
   subroutine init_fault_stf(tl_fault)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      type_fault&
                                     ,ig_ndt&
                                     ,rg_simu_times&
                                     ,error_stop

      use mod_init_memory, only : init_array_real

      use mod_source_function

      implicit none

      type(type_fault), dimension(:), intent(inout) :: tl_fault

      real   (kind=RXP)                             :: ts
      real   (kind=RXP)                             :: rt
                                                  
      integer(kind=IXP)                             :: nfault
      integer(kind=IXP)                             :: ifault
      integer(kind=IXP)                             :: idcs
      integer(kind=IXP)                             :: icur
      integer(kind=IXP)                             :: ios

      character(len=CIL)                            :: info

      nfault = size(tl_fault)

      do ifault = ONE_IXP,nfault

!
!------->allocate memory of source time function for each double couple source

         do idcs = ONE_IXP,tl_fault(ifault)%ndcsource

            ios = init_array_real(tl_fault(ifault)%dcsource(idcs)%stf,ig_ndt,"mod_source:init_fault_stf:stf")

         enddo

!
!------->pre-compute source time function used in mod_solver

         icur = tl_fault(ifault)%icur

!
!------->gabor

         if (icur == 3_IXP) then

            do idcs = ONE_IXP,tl_fault(ifault)%ndcsource

               ts = tl_fault(ifault)%dcsource(idcs)%shift_time

               rt = tl_fault(ifault)%dcsource(idcs)%rise_time

               tl_fault(ifault)%dcsource(idcs)%stf = init_gabor(rg_simu_times,ts,rt)

            enddo

!
!------->tanh

         elseif (icur == 8_IXP) then

            do idcs = ONE_IXP,tl_fault(ifault)%ndcsource

               ts = tl_fault(ifault)%dcsource(idcs)%shift_time

               rt = tl_fault(ifault)%dcsource(idcs)%rise_time

               tl_fault(ifault)%dcsource(idcs)%stf = init_tanh(rg_simu_times,ts,rt)

            enddo

!
!------->f11

         elseif (icur == 11_IXP) then

            do idcs = ONE_IXP,tl_fault(ifault)%ndcsource

               ts = tl_fault(ifault)%dcsource(idcs)%shift_time

               rt = tl_fault(ifault)%dcsource(idcs)%rise_time

               tl_fault(ifault)%dcsource(idcs)%stf = init_f11(rg_simu_times,ts,rt,2.0_RXP)

            enddo
!
!------->error, source not included yet

         else

            write(info,'(a)') "error in subroutine module_source:init_fault_stf. source time function not included yet"

            call error_stop(info)

         endif

       enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_fault_stf
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine initializes user-defined slip distribution
!***********************************************************************************************************************************************************************************
   subroutine init_fault_user(tl_fault)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      type_fault&
                                     ,cg_prefix&
                                     ,error_stop

      use mod_init_memory, only : init_array_real

      implicit none

      type(type_fault), intent(inout) :: tl_fault

      integer(kind=IXP)               :: uslip
      integer(kind=IXP)               :: is
      integer(kind=IXP)               :: id
      integer(kind=IXP)               :: ios

      character(len=6_IXP)            :: cfault
      character(len=CIL)              :: info

      write(cfault,'(I6.6)') tl_fault%ifault

      open(newunit=uslip,file=trim(cg_prefix)//".fault."//trim(cfault)//".user.slp",action='read',status='old',iostat=ios)

      if (ios /= 0_IXP) then

         write(info,'(a)')  "error in subroutine init_fault_user: file "//trim(cg_prefix)//".*.user.slp not found"
         call error_stop(info)

      endif

!
!---->read user slip distribution

      read(uslip,*) tl_fault%ns,tl_fault%nd

      tl_fault%ds = tl_fault%ls/real(tl_fault%ns,kind=RXP)

      tl_fault%dd = tl_fault%ld/real(tl_fault%nd,kind=RXP)

      ios = init_array_real(tl_fault%rf,tl_fault%nd,tl_fault%ns,"mod_source:init_fault_user:tl_fault%rf")

      do id = 1_IXP,tl_fault%nd

         read(uslip,*) (tl_fault%rf(is,id),is=1_IXP,tl_fault%ns)

      enddo

      close(uslip)

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_fault_user
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine writes info about the faults (sub-faults magnitude, rupture time)
!***********************************************************************************************************************************************************************************
   subroutine write_fault_info(tl_fault)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      type_fault&
                                     ,rg_simu_times&
                                     ,ig_mpi_nboctet_real&
                                     ,ig_mpi_comm_simu&
                                     ,LG_LUSTRE_FILE_SYS&
                                     ,cg_prefix

      use mod_io_array       , only : efi_mpi_file_write_at_all

      use mod_write_listing  , only : write_lst_fault_info

      implicit none

      type(type_fault) , dimension(:), target, intent(in) :: tl_fault

      real   (kind=RXP), dimension(:), pointer            :: p_v
      real   (kind=RXP)                                   :: ds
      real   (kind=RXP)                                   :: dd
                                                          
      integer(kind=IXP), dimension(:), pointer            :: p_o
      integer(kind=IXP)                                   :: nfault
      integer(kind=IXP)                                   :: ifault
      integer(kind=IXP)                                   :: ns
      integer(kind=IXP)                                   :: nd
      integer(kind=IXP)                                   :: idt
      integer(kind=IXP)                                   :: unit_stf
                                                          
      character(len=CIL)                                  :: fname
      character(len=  6)                                  :: cfault

      nfault = size(tl_fault)

      do ifault = ONE_IXP,nfault

         ns = tl_fault(ifault)%ns
         nd = tl_fault(ifault)%nd

         ds = tl_fault(ifault)%ds
         dd = tl_fault(ifault)%dd

         write(cfault,'(I6.6)') ifault

!
!------->write sub-faults rupture times

         fname = trim(cg_prefix)//".fault."//trim(cfault)//".rt"

         p_v => tl_fault(ifault)%dcsource(:)%shift_time

         p_o => tl_fault(ifault)%dcsource(:)%rglo

         call efi_mpi_file_write_at_all(ig_mpi_comm_simu,p_v,fname,p_o,ns,nd,ds,dd,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS)

         nullify(p_v)

!
!------->write sub-faults moment magnitudes

         fname = trim(cg_prefix)//".fault."//trim(cfault)//".mw"

         p_v => tl_fault(ifault)%dcsource(:)%mw

         call efi_mpi_file_write_at_all(ig_mpi_comm_simu,p_v,fname,p_o,ns,nd,ds,dd,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS)

         nullify(p_v)

!
!------->write sub-faults slip

         fname = trim(cg_prefix)//".fault."//trim(cfault)//".sl"

         p_v => tl_fault(ifault)%dcsource(:)%slip

         call efi_mpi_file_write_at_all(ig_mpi_comm_simu,p_v,fname,p_o,ns,nd,ds,dd,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS)

         nullify(p_v,p_o)

!
!------->cpu0 of local communicator 'tl_fault%mpi_comm' writes the source time function

         if (tl_fault(ifault)%mpi_comm /= MPI_COMM_NULL) then

            if (tl_fault(ifault)%mpi_rank == ZERO_IXP) then

               open(newunit=unit_stf,file=trim(cg_prefix)//".fault."//trim(cfault)//".stf",action="write")

               do idt = 1_IXP,size(rg_simu_times)

                  write(unit_stf,'(2(E15.7,1X))') rg_simu_times(idt),tl_fault(ifault)%dcsource(1_IXP)%stf(idt)

               enddo

               close(unit_stf)

            endif

         endif

      enddo

!
!---->writes info in listing file

      call write_lst_fault_info(tl_fault)

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_fault_info
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine reads all double couple point sources in file *.dcs; sets double couple point sources moment tensor; determines to which cpu belong double couple point sources and
!!computes local coordinates @f$\xi,\eta,\zeta@f$ of double couple point sources inside the hexahedron element it belongs.
!>@return mod_global_variables::tg_dcsource
!>@return mod_global_variables::ig_ndcsource
!>@return mod_global_variables::rg_dcsource_user_func
!***********************************************************************************************************************************************************************************
   subroutine init_double_couple_source()
!***********************************************************************************************************************************************************************************
      
      use mod_global_variables, only :&
                                      IG_LST_UNIT&
                                     ,tg_dcsource_init&
                                     ,tg_dcsource&
                                     ,ig_ndcsource&
                                     ,ig_ncpu&
                                     ,ig_myrank&
                                     ,cg_myrank&
                                     ,type_double_couple_source&
                                     ,cg_prefix&
                                     ,error_stop&
                                     ,rg_dt&
                                     ,ig_ndt&
                                     ,ig_mpi_comm_simu&
                                     ,rg_dcsource_user_func
      
      use mod_source_function , only : interp_linear, write_stf

      use mod_init_memory     , only : init_type_double_couple_source
     
      use mod_receiver        , only : locate_point_in_hexa

      implicit none

      real(kind=RXP), allocatable, dimension(:)       :: dum1
      real(kind=RXP), allocatable, dimension(:)       :: dum2
      real(kind=RXP), allocatable, dimension(:)       :: dum3
      real(kind=RXP)                                  :: st
      real(kind=RXP)                                  :: di
      real(kind=RXP)                                  :: ra
      real(kind=RXP)                                  :: m0
      real(kind=RXP)                                  :: maxdmin
      real(kind=RXP)            , dimension(ig_ncpu)  :: maxdmin_all_cpu

      integer(kind=IXP)         , dimension(ig_ncpu)  :: ilrcpu
      integer(kind=IXP)                               :: i
      integer(kind=IXP)                               :: j
      integer(kind=IXP)                               :: iso
      integer(kind=IXP)                               :: ios
      integer(kind=IXP)                               :: ios1
      integer(kind=IXP)                               :: ilndco
      integer(kind=IXP)                               :: itempo
      integer(kind=IXP)                               :: unit_dcs
                                                    
      character(len=1)                                :: ctempo

      logical(kind=IXP)                               :: is_dcs_file_exist

!
!
!**********************************************************************************************
!---->read double couple source information from .dcs file
!**********************************************************************************************

      if (ig_myrank == ZERO_IXP) write(IG_LST_UNIT,'(/,a)') "subroutine init_double_couple_source"

      inquire(file=trim(cg_prefix)//".dcs",exist=is_dcs_file_exist)

      if ( is_dcs_file_exist .and. (ig_ndcsource > 0_IXP) ) then

         if (ig_myrank == ZERO_IXP) write(IG_LST_UNIT,'(a)') " --> double couple point source already defined in .cfg file, dcs file ignored"

      endif

      if (ig_ndcsource == 0_IXP) then !dc source not defined in .cfg file or not defined at all
       
         open(newunit=unit_dcs,file=trim(cg_prefix)//".dcs",status='old',iostat=ios)
    
         if (ios /= ZERO_IXP) then

            if (ig_myrank == ZERO_IXP) write(IG_LST_UNIT,'(a)') " --> no double couple point source computed"

            return

         endif
 
         read(unit=unit_dcs,fmt=*) ig_ndcsource

         ios = init_type_double_couple_source(tg_dcsource_init,ig_ndcsource,"module_source:init_double_couple_source:tg_dcsource_init") 

         do iso = ONE_IXP,ig_ndcsource

            read(unit=unit_dcs,fmt=*) tg_dcsource_init(iso)%p%x,tg_dcsource_init(iso)%p%y,tg_dcsource_init(iso)%p%z,tg_dcsource_init(iso)%mw,tg_dcsource_init(iso)%str,tg_dcsource_init(iso)%dip,tg_dcsource_init(iso)%rak,tg_dcsource_init(iso)%shift_time,tg_dcsource_init(iso)%rise_time,tg_dcsource_init(iso)%icur

         enddo

         close(unit_dcs)

      endif

!
!
!**********************************************************************************************
!---->find element containing double couple point source in cpu 'myrank'
!**********************************************************************************************

      ilndco = ZERO_IXP

      do iso = ONE_IXP,ig_ndcsource
   
         st = deg2rad(tg_dcsource_init(iso)%str)
         di = deg2rad(tg_dcsource_init(iso)%dip)
         ra = deg2rad(tg_dcsource_init(iso)%rak)

         m0 = 10_RXP**(1.5_RXP*tg_dcsource_init(iso)%mw + 9.1_RXP) !TODO FLO: used real64 variable for m0

         call init_moment_tensor(st,di,ra,m0,tg_dcsource_init(iso)%mxx,tg_dcsource_init(iso)%mxy,tg_dcsource_init(iso)%mxz,tg_dcsource_init(iso)%myy,tg_dcsource_init(iso)%myz,tg_dcsource_init(iso)%mzz)

         if ( (iso == ONE_IXP) .and. (tg_dcsource_init(iso)%icur == 10_IXP) ) then

            itempo = ZERO_IXP
            open(unit=99,file=trim(cg_prefix)//".dcf")
            do while (.true.)
               read(unit= 99,fmt=*,iostat=ios1) ctempo
               if (ios1 /= ZERO_IXP) exit
               itempo = itempo + ONE_IXP
            enddo

            rewind(99)

            allocate(rg_dcsource_user_func(ig_ndt+ONE_IXP),dum3(ig_ndt+ONE_IXP),dum1(itempo),dum2(itempo))

            do i = ONE_IXP,itempo
               read(unit=99,fmt=*) dum1(i),dum2(i)
            enddo
            close(99)

            do i = ONE_IXP,ig_ndt+ONE_IXP
               dum3(i) = real(i-ONE_IXP,kind=RXP)*rg_dt
            enddo

            call interp_linear(ONE_IXP,itempo,dum1,dum2,ig_ndt+ONE_IXP,dum3,rg_dcsource_user_func)

            if (ig_myrank == ZERO_IXP) then
               open(unit=90,file=trim(cg_prefix)//".dcf.interp")
               do i = ONE_IXP,ig_ndt+ONE_IXP
                  write(unit=90,fmt='(2(E14.7,1X))') dum3(i),rg_dcsource_user_func(i)
               enddo
               close(90)
            endif

         endif

!
!------->find which cpu and which hexa in this cpu contain the source iso

         call locate_point_in_hexa(tg_dcsource_init(iso)%p,ilndco)

      enddo

!
!---->cpu0 writes the source time functions

      if (ig_myrank == ZERO_IXP) then

         do iso = ONE_IXP,ig_ndcsource
       
            call write_stf("dcs",iso,tg_dcsource_init(iso)%icur,tg_dcsource_init(iso)%shift_time,tg_dcsource_init(iso)%rise_time)
       
         enddo

      endif

!
!---->transfer array tg_dcsource_init which knows all the sources to array tg_dcsource that needs only to know the sources of cpu 'ig_myrank'

      if (ilndco > ZERO_IXP) then

         allocate(tg_dcsource(ilndco))

         j = ZERO_IXP

         do iso = ONE_IXP,ig_ndcsource

            if (tg_dcsource_init(iso)%p%cpu == ig_myrank+ONE_IXP) then

               j = j + ONE_IXP
               tg_dcsource(j)      = tg_dcsource_init(iso)
               tg_dcsource(j)%rglo = iso

            endif

         enddo

         ig_ndcsource = ilndco

      else

         ig_ndcsource = ZERO_IXP

      endif

      deallocate(tg_dcsource_init)

!
!---->cpu 0 gather the number of source (ig_ndcsource) of the other cpus

      call mpi_gather(ig_ndcsource,ONE_IXP,mpi_integer,ilrcpu,ONE_IXP,mpi_integer,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!---->compute information for sources that belong to cpu myrank and send 'dmin' to cpu 0

      do iso = ONE_IXP,ig_ndcsource

         call compute_source_local_coordinate(tg_dcsource(iso))

      enddo

!
!---->all cpus compute their maximum dmin

      if (ig_ndcsource > ZERO_IXP) then

         maxdmin = -huge(maxdmin)

         do iso = ONE_IXP,ig_ndcsource
            maxdmin = max(maxdmin,tg_dcsource(iso)%p%dmin)
         enddo

      else

         maxdmin = ZERO_RXP

      endif

!
!---->cpu 0 gathers dmin of all cpus and write the maximum dmin in *.lst

      call mpi_gather(maxdmin,ONE_IXP,MPI_REAL,maxdmin_all_cpu,ONE_IXP,MPI_REAL,ZERO_IXP,ig_mpi_comm_simu,ios)

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(a,e14.7)') " --> maximum localisation error of all sources inside volume of computation = ",maxval(maxdmin_all_cpu)
         call flush(IG_LST_UNIT)

      endif


      if ( ig_ndcsource > ZERO_IXP ) then

!
!------->output source info for each CPU

         open(unit=100,file=trim(cg_prefix)//".dcs.info.cpu."//trim(cg_myrank))

         do iso = ONE_IXP,ig_ndcsource

            write(unit=100,fmt='(/,a,i10    )') "Source       ",tg_dcsource(iso)%rglo
            write(unit=100,fmt='(  a,i10    )') "    in core  ",tg_dcsource(iso)%p%cpu-ONE_IXP
            write(unit=100,fmt='(  a,i10    )') "    in hexa  ",tg_dcsource(iso)%p%iel
            write(unit=100,fmt='(  a,E15.7  )') "X found        = ",tg_dcsource(iso)%p%x
            write(unit=100,fmt='(  a,E15.7  )') "Y found        = ",tg_dcsource(iso)%p%y
            write(unit=100,fmt='(  a,E15.7  )') "Z found        = ",tg_dcsource(iso)%p%z
            write(unit=100,fmt='(  a,E15.7  )') "xi             = ",tg_dcsource(iso)%p%xi
            write(unit=100,fmt='(  a,E15.7  )') "eta            = ",tg_dcsource(iso)%p%et
            write(unit=100,fmt='(  a,E15.7  )') "zeta           = ",tg_dcsource(iso)%p%ze
            write(unit=100,fmt='(  a,E15.7  )') "distance error = ",tg_dcsource(iso)%p%dmin
            write(unit=100,fmt='(  a,E15.7  )') "mw             = ",tg_dcsource(iso)%mw
            write(unit=100,fmt='(  a,E15.7  )') "strike         = ",tg_dcsource(iso)%str
            write(unit=100,fmt='(  a,E15.7  )') "dip            = ",tg_dcsource(iso)%dip
            write(unit=100,fmt='(  a,E15.7  )') "rake           = ",tg_dcsource(iso)%rak
            write(unit=100,fmt='(  a,E15.7  )') "mxx            = ",tg_dcsource(iso)%mxx
            write(unit=100,fmt='(  a,E15.7  )') "myy            = ",tg_dcsource(iso)%myy
            write(unit=100,fmt='(  a,E15.7  )') "mzz            = ",tg_dcsource(iso)%mzz
            write(unit=100,fmt='(  a,E15.7  )') "mxy            = ",tg_dcsource(iso)%mxy
            write(unit=100,fmt='(  a,E15.7  )') "mxz            = ",tg_dcsource(iso)%mxz
            write(unit=100,fmt='(  a,E15.7  )') "myz            = ",tg_dcsource(iso)%myz

         enddo

         close(100)

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine init_double_couple_source
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine reads all single force point sources in file *.sfs; determines to which cpu belong single force point sources and
!!computes local coordinates @f$\xi,\eta,\zeta@f$ of single force point sources inside the hexahedron element it belongs.
!>@return mod_global_variables::tg_sfsource
!>@return mod_global_variables::ig_nsfsource
!>@return mod_global_variables::rg_sfsource_user_func
!***********************************************************************************************************************************************************************************
   subroutine init_single_force_source()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_LST_UNIT&
                                     ,get_newunit&
                                     ,tg_sfsource&
                                     ,ig_nsfsource&
                                     ,ig_ncpu&
                                     ,ig_myrank&
                                     ,type_single_force_source&
                                     ,cg_prefix&
                                     ,rg_dt&
                                     ,ig_ndt&
                                     ,rg_sfsource_user_func&
                                     ,ig_mpi_comm_simu&
                                     ,ig_hexa_gll_glonum

      use mod_source_function , only : interp_linear, write_stf

      use mod_receiver        , only : search_closest_hexa_gll 

      implicit none

      real(kind=RXP), allocatable, dimension(:)                 :: dum1,dum2,dum3
      real(kind=RXP), dimension(ig_ncpu)                        :: dummy
      real(kind=RXP)                                            :: maxdmin
      real(kind=RXP)                                            :: rldmin

      integer(kind=IXP), dimension(mpi_status_size)             :: statut
      integer(kind=IXP), dimension(ONE_IXP)                     :: min_loc
      integer(kind=IXP), dimension(ig_ncpu)                     :: ilrcpu
      integer(kind=IXP)                                         :: i
      integer(kind=IXP)                                         :: j
      integer(kind=IXP)                                         :: ipf
      integer(kind=IXP)                                         :: ios
      integer(kind=IXP)                                         :: ios1
      integer(kind=IXP)                                         :: ilnpfo
      integer(kind=IXP)                                         :: itempo
      integer(kind=IXP)                                         :: unit_sfs

      character(len=1)                                          :: ctempo

      type(type_single_force_source), allocatable, dimension(:) :: tlpofo

!
!
!**********************************************************************************************
!---->find element containing single force point source in cpu myrank
!**********************************************************************************************
      
      ilnpfo = ZERO_IXP

      open(newunit=unit_sfs,file=trim(cg_prefix)//".sfs",status='old',iostat=ios)

      if (ios == ZERO_IXP) then

         if (ig_myrank == ZERO_IXP) write(IG_LST_UNIT,'(a)') " "

         !read nb of single force sources
         read(unit=unit_sfs,fmt='(i10)') ig_nsfsource

         allocate(tlpofo(ig_nsfsource))

         do ipf = ONE_IXP,ig_nsfsource

            !initialize tlpofo
            tlpofo(ipf)%x          = ZERO_RXP
            tlpofo(ipf)%y          = ZERO_RXP
            tlpofo(ipf)%z          = ZERO_RXP
            tlpofo(ipf)%fac        = ZERO_RXP
            tlpofo(ipf)%rise_time  = ZERO_RXP
            tlpofo(ipf)%shift_time = ZERO_RXP
            tlpofo(ipf)%var1       = ZERO_RXP
            tlpofo(ipf)%dmin       = ZERO_RXP
            tlpofo(ipf)%icur       = ZERO_RXP
            tlpofo(ipf)%idir       = ZERO_IXP
            tlpofo(ipf)%cpu        = -ONE_IXP
            tlpofo(ipf)%iel        = ZERO_IXP
            tlpofo(ipf)%iequ       = ZERO_IXP
            tlpofo(ipf)%kgll       = ZERO_IXP
            tlpofo(ipf)%lgll       = ZERO_IXP
            tlpofo(ipf)%mgll       = ZERO_IXP

            read(unit=unit_sfs,fmt=*) tlpofo(ipf)%x,tlpofo(ipf)%y,tlpofo(ipf)%z,tlpofo(ipf)%fac,tlpofo(ipf)%idir,tlpofo(ipf)%shift_time,tlpofo(ipf)%rise_time,tlpofo(ipf)%icur

            !read user-defined source time function
            if ( (ipf == ONE_IXP) .and. (tlpofo(ipf)%icur == 10_IXP) ) then

               itempo = ZERO_IXP

               open(unit=99,file=trim(cg_prefix)//".sff")

               do while (.true.)

                  read(unit= 99,fmt=*,iostat=ios1) ctempo
                  if (ios1 /= ZERO_IXP) exit
                  itempo = itempo + ONE_IXP

               enddo

               rewind(99)

               allocate(rg_sfsource_user_func(ig_ndt+ONE_IXP),dum3(ig_ndt+ONE_IXP),dum1(itempo),dum2(itempo))

               do i = ONE_IXP,itempo

                  read(unit=99,fmt=*) dum1(i),dum2(i)

               enddo

               close(99)

               do i = ONE_IXP,ig_ndt+ONE_IXP

                  dum3(i) = real(i-ONE_IXP,kind=RXP)*rg_dt

               enddo

               call interp_linear(ONE_IXP,itempo,dum1,dum2,ig_ndt+ONE_IXP,dum3,rg_sfsource_user_func)

               if (ig_myrank == ZERO_IXP) then

                  open(unit=90,file=trim(cg_prefix)//".sff.interp")

                  do i = ONE_IXP,ig_ndt+ONE_IXP
                     write(unit=90,fmt='(2(E14.7,1X))') dum3(i),rg_sfsource_user_func(i)
                  enddo

                  close(90)

               endif

            endif   !end read user-defined source time function

!
!---------->find the closest element and gll point to the source ipf

            call search_closest_hexa_gll(tlpofo(ipf)%x,tlpofo(ipf)%y,tlpofo(ipf)%z,tlpofo(ipf)%dmin,tlpofo(ipf)%kgll,tlpofo(ipf)%lgll,tlpofo(ipf)%mgll,tlpofo(ipf)%iel)

!
!---------->check which cpu has the smallest dmin. this cpu will compute this source and the other won't

            call mpi_allgather(tlpofo(ipf)%dmin,ONE_IXP,MPI_REAL,dummy,ONE_IXP,MPI_REAL,ig_mpi_comm_simu,ios)

            min_loc = minloc(dummy(ONE_IXP:ig_ncpu))

            if ( (min_loc(ONE_IXP)-ONE_IXP) == ig_myrank) then
               tlpofo(ipf)%cpu  = ig_myrank
               ilnpfo = ilnpfo + ONE_IXP
            else
               tlpofo(ipf)%cpu  = -ONE_IXP
            endif
            if (ig_myrank == ZERO_IXP) then
               write(IG_LST_UNIT,'(a,i8,a,i0)') "single force point source ",ipf," computed by cpu ",min_loc(ONE_IXP)-ONE_IXP
            endif

         enddo

         close(unit_sfs)

!
!------->cpu0 writes the source time functions

         if (ig_myrank == ZERO_IXP) then

            do ipf = ONE_IXP,ig_nsfsource
          
               call write_stf("sfs",ipf,tlpofo(ipf)%icur,tlpofo(ipf)%shift_time,tlpofo(ipf)%rise_time)
          
            enddo

         endif

!
!------->transfer array tlpofo which knows all the sources to array tg_sfsource that needs only to know the sources of cpu 'ig_myrank'

         if (ilnpfo > ZERO_IXP) then

            allocate(tg_sfsource(ilnpfo))

            j = ZERO_IXP

            do ipf = ONE_IXP,ig_nsfsource

               if (tlpofo(ipf)%cpu == ig_myrank) then

                  j = j + ONE_IXP
                  tg_sfsource(j) = tlpofo(ipf)

               endif

            enddo

            ig_nsfsource = ilnpfo

         else

            ig_nsfsource = ZERO_IXP

         endif

         deallocate(tlpofo)

!
!------->cpu 0 gather the number of single force point source (ig_nsfsource) of the other cpus

         call mpi_gather(ig_nsfsource,ONE_IXP,mpi_integer,ilrcpu,ONE_IXP,mpi_integer,ZERO_IXP,ig_mpi_comm_simu,ios)
         
         do ipf = ONE_IXP,ig_nsfsource

            tg_sfsource(ipf)%iequ = ig_hexa_gll_glonum(tg_sfsource(ipf)%mgll,tg_sfsource(ipf)%lgll,tg_sfsource(ipf)%kgll,tg_sfsource(ipf)%iel)
            if (ig_myrank /= ZERO_IXP) call mpi_send(tg_sfsource(ipf)%dmin,ONE_IXP,MPI_REAL,ZERO_IXP,100_IXP,ig_mpi_comm_simu,ios)

         enddo

!
!------->cpu 0 gathers dmin of all cpus and write the maximum dmin in *.lst

         if (ig_myrank == ZERO_IXP) then

            maxdmin = ZERO_RXP

            do i = ONE_IXP,ig_ncpu
               if (i == ONE_IXP) then
                  do ipf = ONE_IXP,ig_nsfsource
                     maxdmin = max(maxdmin,tg_sfsource(ipf)%dmin)
                  enddo
               else
                  do ipf = ONE_IXP,ilrcpu(i)
                     call mpi_recv(rldmin,ONE_IXP,MPI_REAL,i-ONE_IXP,100_IXP,ig_mpi_comm_simu,statut,ios)
                     maxdmin = max(maxdmin,rldmin)
                  enddo 
               endif
            enddo

            write(IG_LST_UNIT,'(a,e14.7)') "maximum localisation error of all single force point sources = ",maxdmin
            call flush(IG_LST_UNIT)

         endif

      else

         if (ig_myrank == ZERO_IXP) write(IG_LST_UNIT,'(/,a)') "no single force point source computed"
         ig_nsfsource = ZERO_IXP

      endif
      
      return
!***********************************************************************************************************************************************************************************
   end subroutine init_single_force_source
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine finds and stores the gll nodes' number located at mod_global_variables::rg_plane_wave_z. These gll nodes are used to inject a plane wave.
!>@return mod_global_variables::ig_plane_wave_ngll
!>@return mod_global_variables::ig_plane_wave_gll
!>@return mod_global_variables::rg_plane_wave_sft_user
!***********************************************************************************************************************************************************************************
   subroutine init_plane_wave_source()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      ig_ngll_total&
                                     ,rg_gll_coordinate&
                                     ,ig_ndt&
                                     ,rg_dt&
                                     ,ig_nquad_parax&
                                     ,ig_quadp_gnode_glonum&
                                     ,ig_myrank&
                                     ,cg_prefix&
                                     ,get_newunit&
                                     ,rg_plane_wave_z&
                                     ,rg_mesh_zmin&
                                     ,ig_plane_wave_ngll&
                                     ,ig_plane_wave_gll&
                                     ,ig_plane_wave_nquadp_bottom&
                                     ,ig_plane_wave_quadp_bottom&
                                     ,ig_plane_wave_nquadp_side&
                                     ,ig_plane_wave_quadp_side&
                                     ,rg_plane_wave_sft_user&
                                     ,ig_mpi_comm_simu&
                                     ,error_stop&
                                     ,ZERO_RXP

      use mod_coordinate, only : compute_quad_point_coord_z

      use mod_init_memory

      use mod_source_function, only : interp_linear

      real(kind=RXP), allocatable, dimension(:) :: dum1
      real(kind=RXP), allocatable, dimension(:) :: dum2
      real(kind=RXP), allocatable, dimension(:) :: dum3
      real(kind=RXP)                            :: zquad

      integer(kind=IXP)                         :: myunit
      integer(kind=IXP)                         :: ngll
      integer(kind=IXP)                         :: ngll_sum
      integer(kind=IXP)                         :: i
      integer(kind=IXP)                         :: itempo
      integer(kind=IXP)                         :: igll
      integer(kind=IXP)                         :: iquad
      integer(kind=IXP)                         :: nquad_bottom
      integer(kind=IXP)                         :: nquad_side
      integer(kind=IXP)                         :: nquad_sum
      integer(kind=IXP)                         :: ios

      character(len=CIL)                        :: info
      character(len=  1)                        :: ctempo

!
!
!**************************************************************************************************************
!---->find gll nodes located at the z-coordinate 'rg_plane_wave_z'
!**************************************************************************************************************

!
!---->first pass to count the number of gll nodes used to inject the plane wave

      ngll = ZERO_IXP

      do igll = ONE_IXP,ig_ngll_total

         if ( abs(rg_gll_coordinate(THREE_IXP,igll) - rg_plane_wave_z) <= abs(EPSILON_MACHINE_RXP*rg_plane_wave_z) ) then

            ngll = ngll + ONE_IXP

         endif

      enddo

!
!---->check if some gll nodes have been found (if not: abort computation)

      call mpi_reduce(ngll,ngll_sum,ONE_IXP,MPI_INTEGER,MPI_SUM,ZERO_IXP,ig_mpi_comm_simu,ios)

      if ( (ig_myrank == ZERO_IXP) .and. (ngll_sum <= ZERO_IXP) ) then

         write(info,'(a)') "error in subroutine init_plane_wave_source: over all cpus, no gll nodes found to inject plane wave"
         call error_stop(info)

      endif

!
!---->memory allocation to store gll nodes' number where the plane wave is injected

      ig_plane_wave_ngll = ngll

      ios = init_array_int(ig_plane_wave_gll,ig_plane_wave_ngll,"ig_plane_wave_gll")

!
!---->second pass to store the gll nodes' number used to inject the plane wave in cpu 'myrank'

      ngll = ZERO_IXP

      do igll = ONE_IXP,ig_ngll_total

         if ( abs(rg_gll_coordinate(THREE_IXP,igll) - rg_plane_wave_z) <= abs(EPSILON_MACHINE_RXP*rg_plane_wave_z) ) then

            ngll                    = ngll + ONE_IXP
            ig_plane_wave_gll(ngll) = igll

         endif

      enddo

!!
!!---->cpu 'myrank' output the gll nodes where it will inject the plane wave
!      if (ig_plane_wave_ngll > ZERO_IXP) then
!
!         open(unit=get_newunit(myunit),file=trim(cg_prefix)//".plane.wave.gll.cpu."//trim(cg_myrank))
!
!         do igll = ONE_IXP,ig_plane_wave_ngll
!
!            ngll = ig_plane_wave_gll(igll)
!
!            write(unit=myunit,fmt='(I10,3(1X,E15.7))') ngll,rg_gll_coordinate(ONE_IXP,ngll),rg_gll_coordinate(TWO_IXP,ngll),rg_gll_coordinate(THREE_IXP,ngll)
!
!         enddo
!
!         close(myunit)
!
!      endif


!
!
!**********************************************************************************************************************************
!find quadrangle elements used as boundary absorption located close to rg_mesh_zmin (i.e., bottom of the domain of computation)
!find quadrangle elements used to impose dirichlet condition located on the vertical sides of the domain on computation
!**********************************************************************************************************************************

!
!---->first pass to count the number of paraxial quadrangle elements located at rg_mesh_zmin

      nquad_bottom  = ZERO_IXP

      do iquad = ONE_IXP,ig_nquad_parax

         zquad = compute_quad_point_coord_z(ig_quadp_gnode_glonum,iquad,ZERO_RXP,ZERO_RXP)

         if ( abs(zquad - rg_mesh_zmin) <= abs(EPSILON_MACHINE_RXP*rg_mesh_zmin) ) then

            nquad_bottom  = nquad_bottom  + ONE_IXP

         endif

      enddo


!
!---->check if paraxial quadrangle elements have been found (if not: abort computation)

      call mpi_reduce(nquad_bottom ,nquad_sum,ONE_IXP,MPI_INTEGER,MPI_SUM,ZERO_IXP,ig_mpi_comm_simu,ios)

      if ( (ig_myrank == ZERO_IXP) .and. (nquad_sum <= ZERO_IXP) ) then

         write(info,'(a)') "error in subroutine init_plane_wave_source: over all cpus, no paraxial quadrangle element found at the bottom of the domain of computation"
         call error_stop(info)

      endif

!
!---->memory allocation to store paraxial quadrangle elements at the bottom and at the sides of the domain of computation

      ig_plane_wave_nquadp_bottom = nquad_bottom

      ios = init_array_int(ig_plane_wave_quadp_bottom,ig_plane_wave_nquadp_bottom,"ig_plane_wave_quadp_bottom")

      ig_plane_wave_nquadp_side = ig_nquad_parax - ig_plane_wave_nquadp_bottom

      if (ig_plane_wave_nquadp_side < ZERO_IXP) then

         write(info,'(a)') "error in subroutine init_plane_wave_source: number of quadrangle element at the bottom of the domain of computation < 0"
         call error_stop(info)

      endif

      ios = init_array_int(ig_plane_wave_quadp_side,ig_plane_wave_nquadp_side,"ig_plane_wave_quadp_side")

!
!---->second pass to store the paraxial quadrangle elements located at rg_mesh_zmin

      nquad_bottom = ZERO_IXP
      nquad_side   = ZERO_IXP

      do iquad = ONE_IXP,ig_nquad_parax

         zquad = compute_quad_point_coord_z(ig_quadp_gnode_glonum,iquad,ZERO_RXP,ZERO_RXP)

         if ( abs(zquad - rg_mesh_zmin) <= abs(EPSILON_MACHINE_RXP*rg_mesh_zmin) ) then

            nquad_bottom  = nquad_bottom  + ONE_IXP

            ig_plane_wave_quadp_bottom(nquad_bottom) = iquad

         else

            nquad_side = nquad_side + ONE_IXP

            ig_plane_wave_quadp_side(nquad_side) = iquad

         endif

      enddo


!
!
!**************************************************************************************************************
!---->read and interpolate user source time function
!**************************************************************************************************************

      itempo = ZERO_IXP

      open(unit=get_newunit(myunit),file=trim(cg_prefix)//".pwf",form='formatted',action='read',status='old',iostat=ios)

      if (ios /= 0_IXP) then

         write(info,'(a)')  "error in subroutine init_plane_wave_source: file "//trim(cg_prefix)//".pwf not found"
         call error_stop(info)

      endif

      do while (.true.)

         read(unit=myunit,fmt=*,iostat=ios) ctempo
         if (ios /= ZERO_IXP) exit
         itempo = itempo + ONE_IXP

      enddo

      if (itempo <= 1_IXP) then

         write(info,'(a)')  "error in subroutine init_plane_wave_source: file "//trim(cg_prefix)//".pwf seems empty"
         call error_stop(info)

      endif

      rewind(myunit)

      allocate(rg_plane_wave_sft_user(ig_ndt+ONE_IXP),dum3(ig_ndt+ONE_IXP),dum1(itempo),dum2(itempo))

      do i = ONE_IXP,itempo
         read(unit=myunit,fmt=*) dum1(i),dum2(i)
      enddo

      close(myunit)

      do i = ONE_IXP,ig_ndt+ONE_IXP
         dum3(i) = real(i-ONE_IXP,kind=RXP)*rg_dt
      enddo

      call interp_linear(ONE_IXP,itempo,dum1,dum2,ig_ndt+ONE_IXP,dum3,rg_plane_wave_sft_user)

      if (ig_myrank == ZERO_IXP) then

         open(unit=get_newunit(myunit),file=trim(cg_prefix)//".pwf.interp")

         do i = ONE_IXP,ig_ndt+ONE_IXP
            write(unit=myunit,fmt='(2(E14.7,1X))') dum3(i),rg_plane_wave_sft_user(i)
         enddo

         close(myunit)

      endif

      deallocate(dum1,dum2,dum3)
      
      return
!***********************************************************************************************************************************************************************************
   end subroutine init_plane_wave_source
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine solves a nonlinear system by a gradient method to compute local coordinates @f$\xi,\eta,\zeta@f$ of a point source inside the hexahedron element it belongs.
!!The starting point of the gradient method is the closest GLL node.
!!Local coordinates are stored in mod_global_variables::tg_dcsource.
!>@param tlsrc : data type that contains information about point sources
!***********************************************************************************************************************************************************************************
   subroutine compute_source_local_coordinate(tlsrc)
!***********************************************************************************************************************************************************************************
      
      use mod_global_variables, only :&
                                      rg_gll_abscissa&
                                     ,type_double_couple_source

      use mod_jacobian        , only : compute_hexa_jacobian
   
      use mod_coordinate      , only : compute_hexa_point_coord
   
      
      implicit none
   
      type(type_double_couple_source), intent(inout) :: tlsrc
      
      real(kind=RXP)                                          :: eps
      real(kind=RXP)                                          :: dmin
      real(kind=RXP)                                          :: xisol
      real(kind=RXP)                                          :: etsol
      real(kind=RXP)                                          :: zesol
      real(kind=RXP)                                          :: newx
      real(kind=RXP)                                          :: newy
      real(kind=RXP)                                          :: newz
      real(kind=RXP)                                          :: dxidx
      real(kind=RXP)                                          :: dxidy
      real(kind=RXP)                                          :: dxidz
      real(kind=RXP)                                          :: detdx
      real(kind=RXP)                                          :: detdy
      real(kind=RXP)                                          :: detdz
      real(kind=RXP)                                          :: dzedx
      real(kind=RXP)                                          :: dzedy
      real(kind=RXP)                                          :: dzedz
      real(kind=RXP)                                          :: dx
      real(kind=RXP)                                          :: dy
      real(kind=RXP)                                          :: dz
      real(kind=RXP)                                          :: dxi
      real(kind=RXP)                                          :: det
      real(kind=RXP)                                          :: dze
      real(kind=RXP)                                          :: deter

      integer(kind=IXP)                                       :: ihexa
      integer(kind=IXP)                                       :: iter
                                                     
      integer(kind=IXP), parameter                             :: ITER_MAX=100_IXP
                              
!
!---->initialize element number and coordinates (needed if dmin < eps)

      ihexa = tlsrc%p%iel
      newx  = tlsrc%p%x
      newy  = tlsrc%p%y
      newz  = tlsrc%p%z
      ihexa = tlsrc%p%iel  
      eps  = 1.0e-3_RXP
      iter = ZERO_IXP

!
!---->set the first guessed solution 

      xisol = rg_gll_abscissa(tlsrc%p%mgll)
      etsol = rg_gll_abscissa(tlsrc%p%lgll)
      zesol = rg_gll_abscissa(tlsrc%p%kgll)
      dmin  = tlsrc%p%dmin
   
!      
!---->solve the nonlinear system to find local coordinates

      do while(dmin > eps)

         tlsrc%p%xi   = xisol
         tlsrc%p%et   = etsol
         tlsrc%p%ze   = zesol
   
         call compute_hexa_jacobian(ihexa,xisol,etsol,zesol,dxidx,dxidy,dxidz,detdx,detdy,detdz,dzedx,dzedy,dzedz,deter)
   
         call compute_hexa_point_coord(ihexa,xisol,etsol,zesol,newx,newy,newz)

         dmin  = sqrt( (newx-tlsrc%p%x)**TWO_IXP + (newy-tlsrc%p%y)**TWO_IXP + (newz-tlsrc%p%z)**TWO_IXP )

!
!------->prepare next iteration by keeping local solution within interval [-1:+1]

         iter  = iter + ONE_IXP
   
         dx    = tlsrc%p%x - newx
         dy    = tlsrc%p%y - newy
         dz    = tlsrc%p%z - newz

         dxi   = dxidx*dx + dxidy*dy + dxidz*dz
         det   = detdx*dx + detdy*dy + detdz*dz
         dze   = dzedx*dx + dzedy*dy + dzedz*dz

         xisol = xisol + dxi
         etsol = etsol + det
         zesol = zesol + dze

         if (xisol > +ONE_RXP) xisol = +ONE_RXP
         if (xisol < -ONE_RXP) xisol = -ONE_RXP
         if (etsol > +ONE_RXP) etsol = +ONE_RXP
         if (etsol < -ONE_RXP) etsol = -ONE_RXP
         if (zesol > +ONE_RXP) zesol = +ONE_RXP
         if (zesol < -ONE_RXP) zesol = -ONE_RXP   
   
         if (mod(iter,ITER_MAX) == ZERO_IXP) then

            eps = eps*TWO_RXP
            xisol = rg_gll_abscissa(tlsrc%p%mgll)
            etsol = rg_gll_abscissa(tlsrc%p%lgll)
            zesol = rg_gll_abscissa(tlsrc%p%kgll)
            dmin  = tlsrc%p%dmin

         endif
   
      enddo
   
!
!---->update tlsrc

      tlsrc%p%dmin = dmin
      tlsrc%p%x    = newx
      tlsrc%p%y    = newy
      tlsrc%p%z    = newz
      
      return
   
!***********************************************************************************************************************************************************************************
   end subroutine compute_source_local_coordinate
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine computes the moment tensor given the strike, dip, rake and seismic moment of a double couple point source
!>@param  s   : strike angle in radian
!>@param  d   : dip angle in radian
!>@param  r   : rake angle in radian
!>@param  m0  : seismic moment
!>@return mxx : moment tensor xx
!>@return mxy : moment tensor xy
!>@return mxz : moment tensor xz
!>@return myy : moment tensor yy 
!>@return myz : moment tensor yz 
!>@return mzz : moment tensor zz 
!***********************************************************************************************************************************************************************************
   subroutine init_moment_tensor(s,d,r,m0,mxx,mxy,mxz,myy,myz,mzz)
!***********************************************************************************************************************************************************************************

      implicit none
      
      real(kind=RXP), intent( in) :: s
      real(kind=RXP), intent( in) :: d
      real(kind=RXP), intent( in) :: r
      real(kind=RXP), intent( in) :: m0
      real(kind=RXP), intent(out) :: mxx
      real(kind=RXP), intent(out) :: mxy
      real(kind=RXP), intent(out) :: mxz
      real(kind=RXP), intent(out) :: myy
      real(kind=RXP), intent(out) :: myz
      real(kind=RXP), intent(out) :: mzz

      mxx = +m0*(sin(d)*cos(r)*sin(TWO_RXP*s) -              sin(TWO_RXP*d)*sin(r)*(cos(s))**TWO_IXP)  !+myy for aki
      mxy = +m0*(sin(d)*cos(r)*cos(TWO_RXP*s) + ONE_HALF_RXP*sin(TWO_RXP*d)*sin(r)* sin(TWO_RXP*s))    !+mxy 
      mxz = +m0*(cos(d)*cos(r)*sin(        s) -              cos(TWO_RXP*d)*sin(r)* cos(s))            !-myz 
      myy = -m0*(sin(d)*cos(r)*sin(TWO_RXP*s) +              sin(TWO_RXP*d)*sin(r)*(sin(s))**TWO_IXP)  !+mxx
      myz = +m0*(cos(d)*cos(r)*cos(        s) +              cos(TWO_RXP*d)*sin(r)* sin(s))            !-mxz
      mzz = +m0*sin(TWO_RXP*d)*sin(r)                                                                  !+mzz

      return
   
!***********************************************************************************************************************************************************************************
   end subroutine init_moment_tensor
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This function converts angle in degree to radian
!>@param  d : angle in degree
!>@return r : angle in radian
!***********************************************************************************************************************************************************************************
   real(kind=RXP) function deg2rad(d) result(r)
!***********************************************************************************************************************************************************************************

      implicit none
      
      real(kind=RXP), intent(in) :: d

      r  = d*PI_RXP/180.0_RXP

      return
   
!***********************************************************************************************************************************************************************************
   end function deg2rad
!***********************************************************************************************************************************************************************************

end module mod_source
