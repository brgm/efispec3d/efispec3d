!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to compute source functions' time history.

!>@brief
!!This module contains functions and subroutines to compute tsource functions's time history. 
module mod_source_function
   
   use mpi

   use mod_precision

   use mod_global_variables, only : CIL

   use mod_init_memory
   
   implicit none

   private

   public  :: init_gabor
   public  :: init_tanh
   public  :: init_f11

   public  :: dirac
   public  :: gabor
   public  :: expcos
   public  :: ispli3_sp
   public  :: ispli3_dp
   public  :: ricker
   public  :: spiexp
   public  :: fctanh
   public  :: fctanh_dp
   public  :: fctanh_dt
   public  :: f11
   public  :: interp_linear
   private :: rvec_ascends_strictly
   private :: rvec_bracket

   public  :: write_stf

   contains

!
!
!>@brief initialize array containing gabor function
!>@param t   : discrete times of the simulation
!>@param ts  : time shift
!>@param rt  : rise time
!>@return f  : discretized function
!***********************************************************************************************************************************************************************************
      function init_gabor(t,ts,rt) result(f)
!***********************************************************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:)             , intent( in) :: t
      real   (kind=RXP)                           , intent( in) :: ts
      real   (kind=RXP)                           , intent( in) :: rt
      real   (kind=RXP), dimension(:), allocatable              :: f

      integer(kind=IXP)                                         :: i
      integer(kind=IXP)                                         :: n
      real   (kind=RXP)                                         :: ios

      n = size(t)

      ios = init_array_real(f,n,"mod_source_function:init_gabor:f")

      do i = ONE_IXP,n

         f(i) = gabor(t(i),ts,rt,ONE_RXP)

      enddo
      
!***********************************************************************************************************************************************************************************
   end function init_gabor
!***********************************************************************************************************************************************************************************


!
!
!>@brief initialize array containing tanh function
!>@param t   : discrete times of the simulation
!>@param ts  : time shift
!>@param rt  : rise time
!>@return f  : discretized function
!***********************************************************************************************************************************************************************************
      function init_tanh(t,ts,rt) result(f)
!***********************************************************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:)             , intent( in) :: t
      real   (kind=RXP)                           , intent( in) :: ts
      real   (kind=RXP)                           , intent( in) :: rt
      real   (kind=RXP), dimension(:), allocatable              :: f

      integer(kind=IXP)                                         :: i
      integer(kind=IXP)                                         :: n
      real   (kind=RXP)                                         :: ios

      n = size(t)

      ios = init_array_real(f,n,"mod_source_function:init_tanh:f")

      do i = ONE_IXP,n

         f(i) = fctanh(t(i),ts,rt,ONE_RXP)

      enddo
      
!***********************************************************************************************************************************************************************************
   end function init_tanh
!***********************************************************************************************************************************************************************************


!
!
!>@brief initialize array containing f11 function
!>@param t   : discrete times of the simulation
!>@param ts  : time shift
!>@param rt  : rise time
!>@param m   : exponent 
!>@return f  : discretized function
!***********************************************************************************************************************************************************************************
      function init_f11(t,ts,rt,m) result(f)
!***********************************************************************************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:)             , intent( in) :: t
      real   (kind=RXP)                           , intent( in) :: ts
      real   (kind=RXP)                           , intent( in) :: rt
      real   (kind=RXP)                           , intent( in) :: m
      real   (kind=RXP), dimension(:), allocatable              :: f

      integer(kind=IXP)                                         :: i
      integer(kind=IXP)                                         :: n
      real   (kind=RXP)                                         :: ios

      n = size(t)

      ios = init_array_real(f,n,"mod_source_function:init_tanh:f")

      do i = ONE_IXP,n

         f(i) = f11(t(i),ts,rt,m)

      enddo
      
!***********************************************************************************************************************************************************************************
   end function init_f11
!***********************************************************************************************************************************************************************************



!
!
!>@brief function @f$N^{\circ}2@f$ to compute Dirac impulse. Time location is fixed to three time step to optimize the length of time series.
!>@param idt : current time step of computation
!>@param dt  : time step size
!>@param ts  : time shift
!>@param a   : amplitude
!***********************************************************************************************************************************************************************************
   real(kind=RXP) function dirac(idt,dt,ts,a)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=RXP), intent(in) :: idt
      real   (kind=RXP), intent(in) :: dt
      real   (kind=RXP), intent(in) :: ts
      real   (kind=RXP), intent(in) :: a

      

      if ( idt == int(ts/dt,kind=IXP) ) then

         dirac = a*1.0_RXP

      else

         dirac = 0.0_RXP

      endif
      
!***********************************************************************************************************************************************************************************
   end function dirac
!***********************************************************************************************************************************************************************************


!
!
!>@brief function @f$N^{\circ}3@f$ to compute real part of Gabor wavelet : @f$ f(t) = a \times \exp \left[ \frac{t-ts}{2\sigma} \right]^{2} \times \cos \left[ \frac{2 \pi (t-ts)}{\lambda}  \right] @f$
!>@param t  : current time of computation @f$ t_{simu} @f$
!>@param ts : time shift of the function
!>@param l  : parameter @f$\lambda@f$. By default: @f$\sigma = \lambda/20@f$.
!>@param a  : amplitude
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function gabor(t,ts,l,a)
!***********************************************************************************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in) :: t
      real(kind=RXP), intent(in) :: ts
      real(kind=RXP), intent(in) :: l
      real(kind=RXP), intent(in) :: a

      real(kind=RXP)             :: s

      s = l/20.0_RXP
      
      gabor = a*exp(-((t-ts)**TWO_IXP)/(TWO_RXP*s**TWO_IXP))* ( cos(TWO_RXP*PI_RXP*(t-ts)/l) )

!***********************************************************************************************************************************************************************************
   end function gabor
!***********************************************************************************************************************************************************************************

!
!
!>@brief function @f$N^{\circ}4@f$ to compute euroseistest project source function for case can1 : @f$ f(t) = \exp \left[ - \frac{\omega(t-ts)}{\gamma} \right]^{2} \times \cos[\omega(t-ts)+\theta]  @f$
!>@param t  : current time of computation @f$ t_{simu} @f$
!>@param ts : time shift of the function
!>@param fp : pseudo-frequency of the function
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function expcos(t,ts,fp)
!***********************************************************************************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in)    :: t 
      real(kind=RXP), intent(in)    :: ts
      real(kind=RXP), intent(in)    :: fp

      real(kind=RXP)               :: ome
      real(kind=RXP)               :: gam
      real(kind=RXP)               :: the

      ome    = TWO_RXP*PI_RXP*fp
      gam    = TWO_RXP
      the    = PI_RXP/TWO_RXP
      expcos = exp(-(ome*(t-ts)/gam)**TWO_IXP)*cos(ome*(t-ts)+the)

!***********************************************************************************************************************************************************************************
   end function expcos
!***********************************************************************************************************************************************************************************

!
!
!>@brief function @f$N^{\circ}5@f$ to compute order 3 spline in simple precision : see <a href="http://en.wikipedia.org/wiki/Spline_%28mathematics%29" target="_blank">Wikipedia</a>
!>@param s  : spline value
!>@param t  : current time of computation @f$ t_{simu} @f$
!>@param du : source duration
!***********************************************************************************************************************************************************************************
   subroutine ispli3_sp(t,du,s)
!***********************************************************************************************************************************************************************************
      implicit none

      real(kind=RXP), intent(out) :: s  
      real(kind=RXP), intent( in) :: t  
      real(kind=RXP), intent( in) :: du 

      real(kind=RXP)             :: dtsp
      real(kind=RXP)             :: abst
      real(kind=RXP)             :: t0

      t0   = du/1.5_RXP
      dtsp = du/4.0_RXP

      abst = abs(t-t0)

      if (abst >= 2.0_RXP*dtsp) then

         s = ZERO_RXP

      elseif ( (abst < (2.0_RXP*dtsp)) .and. (abst >= dtsp) ) then

         s = ((2.0_RXP*dtsp-abst)**3_IXP)/(6.0_RXP*dtsp**3_IXP)

      else

         s = (3.0_RXP*abst**3_IXP - 6.0_RXP*dtsp*abst**2_IXP + 4.0_RXP*dtsp**3_IXP)/(6.0_RXP*dtsp**3_IXP)

      endif

      s = s/dtsp

      return
!***********************************************************************************************************************************************************************************
   end subroutine ispli3_sp
!***********************************************************************************************************************************************************************************

!
!
!>@brief function @f$N^{\circ}5@f$ to compute order 3 spline in real(kind=R64) : see <a href="http://en.wikipedia.org/wiki/Spline_%28mathematics%29" target="_blank">Wikipedia</a>
!>@param s  : spline value
!>@param t  : current time of computation @f$ t_{simu} @f$
!>@param du : source duration
!***********************************************************************************************************************************************************************************
   subroutine ispli3_dp(t,du,s)
!***********************************************************************************************************************************************************************************
      implicit none

      real(kind=R64), intent(out) :: s  
      real(kind=RXP)           , intent( in) :: t  
      real(kind=RXP)           , intent( in) :: du 

      real(kind=R64)              :: dtsp
      real(kind=R64)              :: abst
      real(kind=R64)              :: t0

      t0   = real(du,kind=R64)/1.5_R64
      dtsp = real(du,kind=R64)/4.0_R64

      abst = abs(real(t,kind=R64)-t0)

      if (abst >= 2.0_R64*dtsp) then

         s = ZERO_R64

      elseif ( (abst < (2.0_R64*dtsp)) .and. (abst >= dtsp) ) then

         s = ((2.0_R64*dtsp-abst)**3_IXP)/(6.0_R64*dtsp**3_IXP)

      else

         s = (3.0_R64*abst**3_IXP - 6.0_R64*dtsp*abst**2_IXP + 4.0_R64*dtsp**3_IXP)/(6.0_R64*dtsp**3_IXP)

      endif

      s = s/dtsp

      return
!***********************************************************************************************************************************************************************************
   end subroutine ispli3_dp
!***********************************************************************************************************************************************************************************
   
!
!
!>@brief function @f$N^{\circ}6@f$ to compute order 2 Ricker wavelet : @f$ f(t) = 2 a \left[ \pi^2 \frac{(t-ts)^2}{tp^2} -0.5 \right] \times \exp  \left[ -\pi^2 \frac{(t-ts)^2}{tp^2} \right]  @f$
!>@param t  : current time of computation @f$ t_{simu} @f$
!>@param ts : time shift of the function
!>@param tp : pseudo-period of the function
!>@param a  : amplitude factor
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function ricker(t,ts,tp,a)
!***********************************************************************************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in) :: t  
      real(kind=RXP), intent(in) :: ts 
      real(kind=RXP), intent(in) :: tp 
      real(kind=RXP), intent(in) :: a 

      ricker = TWO_RXP*a*((PI_RXP**TWO_IXP*(t-ts)**TWO_IXP)/(tp**TWO_IXP)-ONE_HALF_RXP)*exp((-PI_RXP**TWO_IXP*(t-ts)**TWO_IXP)/(tp**TWO_IXP)) ! max peak at -1.00
!
!***********************************************************************************************************************************************************************************
   end function ricker
!***********************************************************************************************************************************************************************************

!
!
!>@brief function @f$N^{\circ}7@f$ to compute spice project exponential source function : @f$ f(t) = a \left[1-\left(1+\frac{t}{tp}\right) \right] \times \exp \left[ -\frac{t}{tp} \right] @f$
!>@param t  : current time of computation @f$ t_{simu} @f$
!>@param tp : pseudo-period of the function
!>@param a  : amplitude factor
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function spiexp(t,tp,a)
!***********************************************************************************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in) :: t  
      real(kind=RXP), intent(in) :: tp 
      real(kind=RXP), intent(in) :: a 

      spiexp = a*(ONE_RXP-(ONE_RXP+t/tp)*exp(-t/tp))

!***********************************************************************************************************************************************************************************
   end function spiexp
!***********************************************************************************************************************************************************************************

!
!
!>@brief function @f$N^{\circ}8@f$ to compute hyperbolic tangent function (also called 'Bouchon pulse') : @f$ f(t) = 0.5 a  \left[ 1 + \tanh \left( \frac{t-ts}{2/5 tp} \right)  \right]  @f$
!>@param t  : current time of computation @f$ t_{simu} @f$
!>@param ts : time shift of the function
!>@param tp : pseudo-period of the function
!>@param a  : amplitude factor
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function fctanh(t,ts,tp,a)
!***********************************************************************************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in) :: t  
      real(kind=RXP), intent(in) :: ts 
      real(kind=RXP), intent(in) :: tp 
      real(kind=RXP), intent(in) :: a 

      real(kind=RXP)            :: tau
      real(kind=RXP)            :: tt0   

      tau    = TWO_RXP*tp
!     tt0    = 2.0*tau
      tt0    = ts
      fctanh = a*ONE_HALF_RXP*( ONE_RXP + tanh((t-tt0)/(tau/5.0_RXP)) )

!***********************************************************************************************************************************************************************************
   end function fctanh
!***********************************************************************************************************************************************************************************

!
!
!>@brief function @f$N^{\circ}8@f$ to compute hyperbolic tangent function (also called 'Bouchon pulse') : @f$ f(t) = 0.5 a  \left[ 1 + \tanh \left( \frac{t-ts}{2/5 tp} \right)  \right]  @f$
!>@param t  : current time of computation @f$ t_{simu} @f$
!>@param ts : time shift of the function
!>@param tp : pseudo-period of the function
!>@param a  : amplitude factor
!***********************************************************************************************************************************************************************************
   real(kind=R64) function fctanh_dp(t,ts,tp,a)
!***********************************************************************************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in) :: t  
      real(kind=RXP), intent(in) :: ts 
      real(kind=RXP), intent(in) :: tp 
      real(kind=RXP), intent(in) :: a 

      real(kind=R64)            :: tau
      real(kind=R64)            :: tt0
      real(kind=R64)            :: t_64

      tau       = TWO_R64*real(tp,kind=R64)
!     tt0       = 2.0*tau
      tt0       = real(ts,kind=R64)
      t_64      = real(t ,kind=R64)

      fctanh_dp = real(a,kind=R64)*0.5_R64*( 1.0_R64 + tanh((t_64-tt0)/(tau/5.0_R64)) )

!***********************************************************************************************************************************************************************************
   end function fctanh_dp
!***********************************************************************************************************************************************************************************


!
!
!>@brief function @f$N^{\circ}9@f$ to compute first derivative of hyperbolic tangent function : @f$ f(t) = 2 a \times f_c \left[ 1 - \tanh^{2} \left( 4 f_c (t-ts)  \right)   \right]  @f$ with @f$f_c = \frac{1}{8/5 tp}@f$
!>@param t  : current time of computation @f$ t_{simu} @f$
!>@param ts : time shift of the function
!>@param tp : pseudo-period of the function
!>@param a  : amplitude factor
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function fctanh_dt(t,ts,tp,a)
!***********************************************************************************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in) :: t 
      real(kind=RXP), intent(in) :: tp
      real(kind=RXP), intent(in) :: ts
      real(kind=RXP), intent(in) :: a

      real(kind=RXP)            :: tau
      real(kind=RXP)            :: fc
      real(kind=RXP)            :: tt0   

      tau       = TWO_R64*tp
      fc        = ONE_RXP/(4.0_RXP*(tau/5.0_RXP))
!     tt0       = 2.0*tau
      tt0       = ts
      fctanh_dt = +a*TWO_RXP*fc*(ONE_RXP-(tanh(4.0_RXP*fc*(t-tt0)))**TWO_IXP)

!***********************************************************************************************************************************************************************************
   end function fctanh_dt
!***********************************************************************************************************************************************************************************

!
!
!>@brief function @f$N^{\circ}11@f$ to compute @f$ 1 - 1/(1+((t-ts)/tp)^{n}) @f$ 
!>@param t  : current time of computation @f$ t_{simu} @f$
!>@param ts : time shift of the function
!>@param tp : pseudo-period of the function
!>@param n  : exponent
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function f11 (t,ts,tp,n)
!***********************************************************************************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in) :: t 
      real(kind=RXP), intent(in) :: tp
      real(kind=RXP), intent(in) :: ts
      real(kind=RXP), intent(in) :: n

      f11 = 1.0_RXP - 1.0_RXP/(1.0_RXP + ((t-ts)/tp)**n)

      return

!***********************************************************************************************************************************************************************************
   end function 
!***********************************************************************************************************************************************************************************

!
!
!
!>@brief INTERP_LINEAR applies piecewise linear interpolation to data.
!!
!!From a space of DIM_NUM dimensions, we are given a sequence of
!!DATA_NUM points, which are presumed to be successive samples
!!from a curve of points P.
!!
!!We are also given a parameterization of this data, that is,
!!an associated sequence of DATA_NUM values of a variable T.
!!The values of T are assumed to be strictly increasing.
!!
!!Thus, we have a sequence of values P(T), where T is a scalar,
!!and each value of P is of dimension DIM_NUM.
!!
!!We are then given INTERP_NUM values of T, for which values P
!!are to be produced, by linear interpolation of the data we are given.
!!
!!Note that the user may request extrapolation.  This occurs whenever
!!a T_INTERP value is less than the minimum T_DATA or greater than the
!!maximum T_DATA.  In that case, linear extrapolation is used.
!! 
!!Licensing: This code is distributed under the GNU LGPL license.
!!
!! @version 03 December 2007
!!
!! @author John Burkardt
!!
!>@param dim_num                    : the spatial dimension
!>@param data_num                   : the number of data points.
!>@param t_data(data_num)           : values of the independent variable at the sample points. Values of T_DATA must be strictly increasing.
!>@param p_data(dim_num,data_num)   : values of the dependent variables at the sample points.
!>@param interp_num                 : number of points at which interpolation is to be done.
!>@param t_interp(interp_num)       : values of the independent variable at the interpolation points.
!>@param p_interp(dim_num,data_num) : interpolated values of the dependent variables at the interpolation points.
!***********************************************************************************************************************************************************************************
   subroutine interp_linear( dim_num, data_num, t_data, p_data, interp_num, t_interp, p_interp )
!***********************************************************************************************************************************************************************************

   use mod_global_variables, only : error_stop

   implicit none
 
   integer(kind=IXP), intent( in) :: data_num
   integer(kind=IXP), intent( in) :: dim_num
   integer(kind=IXP), intent( in) :: interp_num
   real(kind=RXP)  , intent( in) :: t_data(data_num)
   real(kind=RXP)  , intent( in) :: t_interp(interp_num)
   real(kind=RXP)  , intent( in) :: p_data(dim_num,data_num)
   real(kind=RXP)  , intent(out) :: p_interp(dim_num,interp_num)
 
   real(kind=RXP)                :: t
   integer(kind=IXP)             :: interp
   integer(kind=IXP)             :: left
   integer(kind=IXP)             :: right

   character(len=CIL)   :: info
 
   if ( .not. rvec_ascends_strictly ( data_num, t_data ) ) then
     write (info,'(a)') 'error in subroutine interp_linear: Independent variable array T_DATA is not strictly increasing.'
     call error_stop(info)
   endif
 
   do interp = ONE_IXP, interp_num
 
      t = t_interp(interp)
!!
!!  Find the interval [ TDATA(LEFT), TDATA(RIGHT) ] that contains, or is
!!  nearest to, TVAL.
!!
      call rvec_bracket ( data_num, t_data, t, left, right )
 
      p_interp(ONE_IXP:dim_num,interp) = &
       ( ( t_data(right) - t                ) * p_data(ONE_IXP:dim_num,left)   &
       + (                 t - t_data(left) ) * p_data(ONE_IXP:dim_num,right) ) &
       / ( t_data(right)     - t_data(left) )
 
   enddo

   return
!***********************************************************************************************************************************************************************************
   end subroutine interp_linear
!***********************************************************************************************************************************************************************************

!
!
!
!>@brief This function determines if an R8VEC is strictly ascending.
!
!>@license GNU LGPL
!
!>@author John Burkardt
!
!>@param n : size of the array
!>@param x : array to be examined
!>@return TRUE if the entries of x strictly ascend.
!***********************************************************************************************************************************************************************************
   logical(kind=IXP) function rvec_ascends_strictly(n,x)
!***********************************************************************************************************************************************************************************

   implicit none
 
   integer(kind=IXP), intent(in)               :: n
   real(kind=RXP)  , intent(in), dimension(n) :: x

   integer(kind=IXP)            :: i
 
   do i = ONE_IXP, n - ONE_IXP
      if ( x(i+ONE_IXP) <= x(i) ) then
         rvec_ascends_strictly = .false.
         return
      endif
   enddo
 
   rvec_ascends_strictly = .true.
 
   return
!***********************************************************************************************************************************************************************************
   end function rvec_ascends_strictly 
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine searches a sorted R8VEC for successive brackets of a value.
!
!>@license GNU LGPL
!
!>@author John Burkardt
!
!>@param  n     : size of input array
!>@param  x     : an array sorted into ascending order
!>@param  xval  : a value to be bracketed
!>@param  left  : x(left) <= xval <= x(right). xval < x(ONE_IXP), when left = 1 , right = 2. x(n) < xval, when left = n-1, right = n
!>@param  right : x(left) <= xval <= x(right). xval < x(ONE_IXP), when left = 1 , right = 2. x(n) < xval, when left = n-1, right = n
!***********************************************************************************************************************************************************************************
   subroutine rvec_bracket(n,x,xval,left,right)
!***********************************************************************************************************************************************************************************

      implicit none
      
      integer(kind=IXP), intent( in)               :: n 
      real(kind=RXP)  , intent( in), dimension(n) :: x
      real(kind=RXP)  , intent( in)               :: xval
      integer(kind=IXP), intent(out)               :: left
      integer(kind=IXP), intent(out)               :: right
      
      integer(kind=IXP):: i

      do i = TWO_IXP,n-ONE_IXP

         if ( xval < x(i) ) then
            left  = i - ONE_IXP
            right = i
            return
         endif

      enddo
      
      left  = n - ONE_IXP
      right = n
      
      return
!***********************************************************************************************************************************************************************************
   end subroutine rvec_bracket
!***********************************************************************************************************************************************************************************

!
!
!>@brief This subroutine writes the source time functions to disk.
!>@param  ty: string defining the type of source ('sfs' or 'dcs')
!>@param  is: number of the source
!>@param  ic: number of the curve (i.e. source function type; ex: 2->dirac, 6->ricker, 10->user-defined)
!>@param  ts: shift time
!>@param  rt: rise time
!***********************************************************************************************************************************************************************************
   subroutine write_stf(ty,is,ic,ts,rt)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      cg_prefix&
                                     ,get_newunit&
                                     ,rg_simu_times&
                                     ,rg_dt

      implicit none

      character(len=3_IXP) , intent(in) :: ty
      integer(kind=IXP)    , intent(in) :: is
      integer(kind=IXP)    , intent(in) :: ic
      real   (kind=RXP)    , intent(in) :: ts
      real   (kind=RXP)    , intent(in) :: rt

      real   (kind=RXP)                 :: t
      real   (kind=RXP)                 :: v
      real   (kind=RXP)                 :: s

      integer(kind=IXP)                 :: idt
      integer(kind=IXP)                 :: u
      character(len=6_IXP)              :: cs

      if (ic == 10_IXP) return !user defined source is already written to disk

      write(cs,'(I6.6)') is
      
      open(unit=get_newunit(u),file=trim(cg_prefix)//"."//trim(ty)//"."//trim(cs)//".gpl",form='formatted',action='write')

      if (ic == 2_IXP) then   !dirac

         do idt = ONE_IXP,size(rg_simu_times)

            t = rg_simu_times(idt)

            write(unit=u,fmt='(2(E15.7,1X))') t,dirac(idt,rg_dt,ts,ONE_RXP)
       
         enddo

      elseif (ic == 3_IXP) then   !gabor

         do idt = ONE_IXP,size(rg_simu_times)

            t = rg_simu_times(idt)

            write(unit=u,fmt='(2(E15.7,1X))') t,gabor(t,ts,rt,ONE_RXP)
       
         enddo

      elseif (ic == 4_IXP) then

         do idt = ONE_IXP,size(rg_simu_times)

            t = rg_simu_times(idt)

            write(unit=u,fmt='(2(E15.7,1X))') t,expcos(t,ts,rt)
       
         enddo

      elseif (ic == 5_IXP) then

         v = ZERO_RXP

         do idt = ONE_IXP,size(rg_simu_times)

            t = rg_simu_times(idt)

            call ispli3_sp(t,rt,s)

            v = v + s*rg_dt

            write(unit=u,fmt='(2(E15.7,1X))') t,v
       
         enddo

      elseif (ic == 6_IXP) then   !ricker

         do idt = ONE_IXP,size(rg_simu_times)

            t = rg_simu_times(idt)

            write(unit=u,fmt='(2(E15.7,1X))') t,ricker(t,ts,rt,ONE_RXP)
       
         enddo

      elseif (ic == 7_IXP) then

         do idt = ONE_IXP,size(rg_simu_times)

            t = rg_simu_times(idt)

            write(unit=u,fmt='(2(E15.7,1X))') t,spiexp(t,rt,ONE_RXP)
       
         enddo

      elseif (ic == 8_IXP) then

         do idt = ONE_IXP,size(rg_simu_times)

            t = rg_simu_times(idt)

            write(unit=u,fmt='(2(E15.7,1X))') t,fctanh(t,ts,rt,ONE_RXP)
       
         enddo

      elseif (ic == 9_IXP) then

         do idt = ONE_IXP,size(rg_simu_times)

            t = rg_simu_times(idt)

            write(unit=u,fmt='(2(E15.7,1X))') t,fctanh_dt(t,ts,rt,ONE_RXP)
       
         enddo

      elseif (ic == 11_IXP) then

         do idt = ONE_IXP,size(rg_simu_times)

            t = rg_simu_times(idt)

            write(unit=u,fmt='(2(E15.7,1X))') t,f11(t,ts,rt,2.0_RXP)
       
         enddo

      endif

      close(u)
      
      return
!***********************************************************************************************************************************************************************************
   end subroutine write_stf
!***********************************************************************************************************************************************************************************

end module mod_source_function
