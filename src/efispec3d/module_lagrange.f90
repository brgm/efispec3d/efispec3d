!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to compute Lagrange polynomials and its derivatives.

!>@brief
!!This module contains functions to compute Lagrange polynomials and its derivatives; and to interpolate @f$x,y,z@f$-variables at @f$\xi,\eta,\zeta@f$-coordinates. 
module mod_lagrange
   
   use mpi

   use mod_precision
   
   implicit none

   private

   public :: lagrap
   public :: lagrap_geom
   public :: lagrad
   public :: lagrad_geom
   public :: hexa_lagrange_interp
   public :: quad_lagrange_interp
   public :: hexa_lagrange_set

!
!>Interface for redirection to subroutines mod_lagrange::quad_lagrange_interp_x or mod_lagrange::quad_lagrange_interp_xyz

   interface quad_lagrange_interp

      module procedure quad_lagrange_interp_x

      module procedure quad_lagrange_interp_xyz

   end interface quad_lagrange_interp

!
!>Interface for redirection to subroutines mod_lagrange::hexa_lagrange_interp_x or mod_lagrange::hexa_lagrange_interp_xyz

   interface hexa_lagrange_interp

      module procedure hexa_lagrange_interp_x

      module procedure hexa_lagrange_interp_xyz

   end interface hexa_lagrange_interp




   contains
   
!  
!
!>@brief function to compute value of order \link mod_global_variables::ig_lagrange_order @f$n@f$ \endlink Lagrange polynomial of the GLL node i at abscissa @f$x@f$: @f$ h_i(x) = \displaystyle\prod_{l=1,\, l\neq i}^{N_{GLL}} \frac{x-x_l}{x_i-x_l} @f$
!>@param x : abscissa where Lagrange polynomial is computed
!>@param i : local position (i.e. node) in the reference domain [-1:1] where lagrange polynomial = 1 (0 at others nodes)
!>@param n : number of GLL nodes in the reference domain [-1:1] (see \link mod_global_variables::ig_ngll \endlink)
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function lagrap(i,x,n)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      rg_gll_abscissa&
                                     ,rg_gll_abscissa_dist

      implicit none

      real   (kind=RXP), intent(in) :: x 
      integer(kind=IXP), intent(in) :: i 
      integer(kind=IXP), intent(in) :: n 

      integer(kind=IXP)             :: j

      lagrap = ONE_RXP

      do j = ONE_IXP,n

         if (j.ne.i) lagrap = lagrap*( (x-rg_gll_abscissa(j))*rg_gll_abscissa_dist(j,i) )

      enddo

!***********************************************************************************************************************************************************************************
   end function lagrap
!***********************************************************************************************************************************************************************************

!
!
!>@brief function to compute value of order @f$n_g@f$ Lagrange polynomial of geometric node i at abscissa @f$x@f$: @f$ h_i(x) = \displaystyle\prod_{l=1,\, l\neq i}^{n_{g}} \frac{x-x_l}{x_i-x_l} @f$
!>@param x : abscissa where Lagrange polynomial is computed
!>@param i : local position (i.e. node) in the reference domain [-1:1] where lagrange polynomial = 1 (0 at others nodes)
!>@param n : number of geometric nodes in the reference domain [-1:1]
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function lagrap_geom(i,x,n)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      rg_gnode_abscissa_dist&
                                     ,rg_gnode_abscissa

      implicit none

      real   (kind=RXP), intent(in) :: x 
      integer(kind=IXP), intent(in) :: i 
      integer(kind=IXP), intent(in) :: n 

      integer(kind=IXP)             :: j

      lagrap_geom = ONE_RXP

      do j = ONE_IXP,n

         if (j.ne.i) lagrap_geom = lagrap_geom*( (x-rg_gnode_abscissa(j))*rg_gnode_abscissa_dist(j,i) )

      enddo

!***********************************************************************************************************************************************************************************
   end function lagrap_geom
!***********************************************************************************************************************************************************************************

!
!
!>@brief function to compute value of the derivative of order \link mod_global_variables::ig_lagrange_order @f$n@f$ \endlink Lagrange polynomial of GLL node i at abscissa @f$x@f$: 
!!@f$ h'_{i}(x) = \displaystyle\sum^{N_{GLL}}_{j \ne i} \frac{1}{x_i - x_j} \displaystyle\prod^{N_{GLL}}_{k \ne i, k \ne j} \frac{x-x_k}{x_i - x_k} @f$
!>@param x : abscissa where the derivative of Lagrange polynomial is computed
!>@param i : local position (i.e. node) in the reference domain [-1:1] where lagrange polynomial = 1 (0 at others nodes). Note: lagrange polynomial = 1 at i location but not its derivative.
!>@param n : number of GLL nodes in the reference domain [-1:1]
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function lagrad(i,x,n)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only : rg_gll_abscissa&
                                      ,rg_gll_abscissa_dist

      implicit none

      real(kind=RXP)  , intent(in) :: x   
      integer(kind=IXP), intent(in) :: i   
      integer(kind=IXP), intent(in) :: n   

      real(kind=RXP)               :: ltmp
      real(kind=RXP)               :: lprod
      integer(kind=IXP)            :: j
      integer(kind=IXP)            :: k

      lagrad = ZERO_RXP
      lprod  = ZERO_RXP

      do j = ONE_IXP,n

         if (j.ne.i) then
            lprod = ONE_RXP
            do k = ONE_IXP,n
               if (k.ne.i) then
                  if(k.eq.j) then
                     ltmp = rg_gll_abscissa_dist(j,i)
                  else
                     ltmp = (x-rg_gll_abscissa(k))*rg_gll_abscissa_dist(k,i)
                  endif
                  lprod = lprod*ltmp
               endif
            enddo
         endif

         lagrad = lagrad + lprod
         lprod  = ZERO_RXP

      enddo
!
!***********************************************************************************************************************************************************************************
   end function lagrad
!***********************************************************************************************************************************************************************************

!
!
!>@brief function to compute value of the derivative of order @f$n_g@f$ Lagrange polynomial of geometric node i at abscissa @f$x@f$: 
!!@f$ h'_{i}(x) = \displaystyle\sum^{n_{g}}_{j \ne i} \frac{1}{x_i - x_j} \displaystyle\prod^{n_{g}}_{k \ne i, k \ne j} \frac{x-x_k}{x_i - x_k} @f$
!>@param x : abscissa where the derivative of Lagrange polynomial is computed
!>@param i : local position (i.e. node) in the reference domain [-1:1] where lagrange polynomial = 1 (0 at others nodes). Note: lagrange polynomial = 1 at i location but not its derivative.
!>@param n : number of geometric nodes in the reference domain [-1:1]
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function lagrad_geom(i,x,n)
!***********************************************************************************************************************************************************************************
     
      use mod_global_variables, only :&
                                      rg_gnode_abscissa&
                                     ,rg_gnode_abscissa_dist

      implicit none

      real(kind=RXP)  , intent(in) :: x   
      integer(kind=IXP), intent(in) :: i   
      integer(kind=IXP), intent(in) :: n   

      real(kind=RXP)               :: ltmp
      real(kind=RXP)               :: lprod
      integer(kind=IXP)            :: j
      integer(kind=IXP)            :: k

      lagrad_geom = ZERO_RXP
      lprod  = ZERO_RXP
      do j = ONE_IXP,n
         if (j.ne.i) then
            lprod = ONE_RXP
            do k = ONE_IXP,n
               if (k.ne.i) then
                  if(k.eq.j) then
                     ltmp = rg_gnode_abscissa_dist(j,i)
                  else
                     ltmp =  (x-rg_gnode_abscissa(k))*rg_gnode_abscissa_dist(k,i)
                  endif
                  lprod = lprod*ltmp
               endif
            enddo
         endif
         lagrad_geom = lagrad_geom + lprod
         lprod  = ZERO_RXP
      enddo
!
!***********************************************************************************************************************************************************************************
   end function lagrad_geom
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine interpolates GLL nodes @f$x@f$-values of a hexahedron element using Lagrange polynomials.
!>@param gll_values : GLL nodes @f$x@f$-values of a hexahedron element
!>@param lag        : pre-computed values of Lagrange polynomials at location @f$\xi,\eta,\zeta@f$ in a hexahedron element
!>@param x          : interpolated @f$x@f$-values at location @f$\xi,\eta,\zeta@f$
!***********************************************************************************************************************************************************************************
   subroutine hexa_lagrange_interp_x(gll_values,lag,x)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,IG_NDOF

      implicit none

      real(kind=RXP), intent( in), dimension(IG_NGLL,IG_NGLL,IG_NGLL) :: gll_values
      real(kind=RXP), intent( in), dimension(IG_NGLL,IG_NGLL,IG_NGLL) :: lag
      real(kind=RXP), intent(out)                                     :: x
                                                                      
      integer(kind=IXP)                                               :: k
      integer(kind=IXP)                                               :: l
      integer(kind=IXP)                                               :: m
 
      x = ZERO_RXP

      do k = ONE_IXP,IG_NGLL
         do l = ONE_IXP,IG_NGLL
            do m = ONE_IXP,IG_NGLL

               x = x + gll_values(m,l,k)*lag(m,l,k)
 
            enddo
         enddo
      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine hexa_lagrange_interp_x
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine interpolates GLL nodes @f$x,y,z@f$-values of a hexahedron element using Lagrange polynomials.
!>@param gll_values : GLL nodes @f$x,y,z@f$-values of a hexahedron element
!>@param lag        : pre-computed values of Lagrange polynomials at location @f$\xi,\eta,\zeta@f$ in a hexahedron element
!>@param x          : interpolated @f$x@f$-values at location @f$\xi,\eta,\zeta@f$
!>@param y          : interpolated @f$y@f$-values at location @f$\xi,\eta,\zeta@f$
!>@param z          : interpolated @f$z@f$-values at location @f$\xi,\eta,\zeta@f$
!***********************************************************************************************************************************************************************************
   subroutine hexa_lagrange_interp_xyz(gll_values,lag,x,y,z)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,IG_NDOF

      implicit none

      real(kind=RXP), intent( in), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: gll_values
      real(kind=RXP), intent( in), dimension(        IG_NGLL,IG_NGLL,IG_NGLL) :: lag
      real(kind=RXP), intent(out)                                             :: x
      real(kind=RXP), intent(out)                                             :: y
      real(kind=RXP), intent(out)                                             :: z
                                                                       
      integer(kind=IXP)                                                       :: k
      integer(kind=IXP)                                                       :: l
      integer(kind=IXP)                                                       :: m

 
      x = ZERO_RXP
      y = ZERO_RXP
      z = ZERO_RXP

      do k = ONE_IXP,IG_NGLL
         do l = ONE_IXP,IG_NGLL
            do m = ONE_IXP,IG_NGLL

               x = x + gll_values(ONE_IXP,m,l,k)*lag(m,l,k)
               y = y + gll_values(TWO_IXP,m,l,k)*lag(m,l,k)
               z = z + gll_values(THREE_IXP,m,l,k)*lag(m,l,k)
 
            enddo
         enddo
      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine hexa_lagrange_interp_xyz
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine interpolates GLL nodes @f$x@f$-values of a quadrangle element using Lagrange polynomials.
!>@param gll_values : GLL nodes @f$x@f$-values of a quadrangle element
!>@param lag        : pre-computed values of Lagrange polynomials at a location @f$\xi,\eta@f$ in a quadrangle element
!>@param x          : @f$x@f$-value interpolation
!***********************************************************************************************************************************************************************************
   subroutine quad_lagrange_interp_x(gll_values,lag,x)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,IG_NDOF

      implicit none

      real(kind=RXP), intent( in), dimension(IG_NGLL,IG_NGLL) :: gll_values
      real(kind=RXP), intent( in), dimension(IG_NGLL,IG_NGLL) :: lag
      real(kind=RXP), intent(out)                             :: x
                                                       
      integer(kind=IXP)                                      :: k
      integer(kind=IXP)                                      :: l

 
      x   = ZERO_RXP

      do k = ONE_IXP,IG_NGLL
         do l = ONE_IXP,IG_NGLL

            x = x + gll_values(l,k)*lag(l,k)

         enddo
      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine quad_lagrange_interp_x
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine interpolates GLL nodes @f$x,y,z@f$-values of a quadrangle element using Lagrange polynomials.
!>@param gll_values : GLL nodes @f$x,y,z@f$-values of a quadrangle element
!>@param lag        : pre-computed values of Lagrange polynomials at a location @f$\xi,\eta@f$ in a quadrangle element
!>@param x          : @f$x@f$-value interpolation
!>@param y          : @f$y@f$-value interpolation
!>@param z          : @f$z@f$-value interpolation
!***********************************************************************************************************************************************************************************
   subroutine quad_lagrange_interp_xyz(gll_values,lag,x,y,z)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,IG_NDOF

      implicit none

      real(kind=RXP), intent( in), dimension(IG_NDOF,IG_NGLL,IG_NGLL) :: gll_values
      real(kind=RXP), intent( in), dimension(        IG_NGLL,IG_NGLL) :: lag
      real(kind=RXP), intent(out)                                     :: x
      real(kind=RXP), intent(out)                                     :: y
      real(kind=RXP), intent(out)                                     :: z
                                                               
      integer(kind=IXP)                                              :: k
      integer(kind=IXP)                                              :: l

 
      x   = ZERO_RXP
      y   = ZERO_RXP
      z   = ZERO_RXP

      do k = ONE_IXP,IG_NGLL
         do l = ONE_IXP,IG_NGLL

            x = x + gll_values(ONE_IXP,l,k)*lag(l,k)
            y = y + gll_values(TWO_IXP,l,k)*lag(l,k)
            z = z + gll_values(THREE_IXP,l,k)*lag(l,k)

         enddo
      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine quad_lagrange_interp_xyz
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine compute Lagrange polynomial values at location  @f$\xi,\eta, \zeta@f$.
!>@param  xi         : @f$\xi@f$   local coordinates in a hexahedron element
!>@param  eta        : @f$\eta@f$  local coordinates in a hexahedron element
!>@param  zeta       : @f$\zeta@f$ local coordinates in a hexahedron element
!>@return lag        : Lagrange polynomial values at GLL m,l,k
!***********************************************************************************************************************************************************************************
   subroutine hexa_lagrange_set(xi,eta,zeta,lag)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_NGLL

      implicit none

      real(kind=RXP), intent( in)                                     :: xi
      real(kind=RXP), intent( in)                                     :: eta
      real(kind=RXP), intent( in)                                     :: zeta
      real(kind=RXP), intent(out), dimension(IG_NGLL,IG_NGLL,IG_NGLL) :: lag
                                                       
      integer(kind=IXP)                                               :: k
      integer(kind=IXP)                                               :: l
      integer(kind=IXP)                                               :: m

      do k = ONE_IXP,IG_NGLL
         do l = ONE_IXP,IG_NGLL
            do m = ONE_IXP,IG_NGLL
               lag(m,l,k) = lagrap(m,xi,IG_NGLL)*lagrap(l,eta,IG_NGLL)*lagrap(k,zeta,IG_NGLL)
            enddo
         enddo
      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine hexa_lagrange_set
!***********************************************************************************************************************************************************************************

end module mod_lagrange
