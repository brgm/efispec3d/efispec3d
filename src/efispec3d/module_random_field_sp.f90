!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
module mod_random_field_sp

   use mod_precision

   use mod_init_memory

   implicit none

   public  :: random_field_sp_2d
   public  :: random_field_sp_2d_lift_zero
   public  :: random_field_sp_2d_norm_sum
   public  :: random_field_sp_2d_par

   contains

!
!
!>@brief subroutine to compute 2D random field using a spectral method
!>@param  ls : fault length along the strike direction
!>@param  ld : fault length along the dip direction
!>@param  as : correlation length along the strike direction
!>@param  ad : correlation length along the dip direction
!>@param  k  : kappa (Hurst number)
!>@param  t  : type of autocorrelation
!>@return ds : discretization step along strike
!>@return dd : discretization step along dip
!>@return rf : 2D random field
!***************************************************************************************************************
   subroutine random_field_sp_2d(ls,ld,as,ad,k,t,ds,dd,rf)
!***************************************************************************************************************

      use mpi

      use mod_global_variables , only : ig_mpi_comm_simu

      use mod_signal_processing, only :&
                                        nextpow2&
                                       ,ft

      use mod_random_field     , only : init_random_seed

      implicit none

      character(len=2)                              , intent(in)           :: t
      real   (kind=RXP)                             , intent(in)           :: ls
      real   (kind=RXP)                             , intent(in)           :: ld
      real   (kind=RXP)                             , intent(in), optional :: as
      real   (kind=RXP)                             , intent(in), optional :: ad
      real   (kind=RXP)                             , intent(in), optional :: k
      real   (kind=RXP)                             , intent(out)          :: ds
      real   (kind=RXP)                             , intent(out)          :: dd
      real   (kind=RXP), dimension(:,:), allocatable, intent(out)          :: rf

      real   (kind=RXP), dimension(:)  , allocatable                       :: ks
      real   (kind=RXP), dimension(:)  , allocatable                       :: kd
      real   (kind=RXP), dimension(:,:), allocatable                       :: kr
      real   (kind=RXP), dimension(:,:), allocatable                       :: psd
      real   (kind=RXP), dimension(:,:), allocatable                       :: am
      real   (kind=RXP), dimension(:,:), allocatable                       :: ph
                                                                           
      complex(kind=RXP), dimension(:,:), allocatable                       :: rf_sp
      complex(kind=RXP), dimension(:)  , allocatable                       :: rf_tmp
                                                                           
      real   (kind=RXP)                                                    :: fs
      real   (kind=RXP)                                                    :: fd
                                                                           
                                                                           
      real   (kind=RXP)                                                    :: mean
      real   (kind=RXP)                                                    :: variance
      real   (kind=RXP)                                                    :: std
                                                                           
      integer(kind=IXP)                                                    :: is 
      integer(kind=IXP)                                                    :: id 
      integer(kind=IXP)                                                    :: ns !number of grid points along the strike
      integer(kind=IXP)                                                    :: nd !number of grid points along the dip
      integer(kind=IXP)                                                    :: ms !number of grid points in the spectral domain
      integer(kind=IXP)                                                    :: md !number of grid points in the spectral domain
      integer(kind=IXP)                                                    :: nfft_s
      integer(kind=IXP)                                                    :: nfft_d
      integer(kind=IXP)                                                    :: ios


!
!---->set the spatial discrtization size with respect to the correlation length

      if (t == "ct") then

         ds = ls/20.0_RXP
         dd = ld/20.0_RXP

      elseif (t == "gs") then

         ds = as/5.0_RXP
         dd = ad/5.0_RXP
      
      elseif (t == "vk") then

         ds = as/5.0_RXP
         dd = ad/5.0_RXP

      endif

!
!---->approximately set the number of "sub-fault"

      ns = int(ls/ds)
      nd = int(ld/dd)

!
!---->set the size of the spectral domain (power of two for FFT)

      nfft_s = nextpow2(ns,128_IXP)
      nfft_d = nextpow2(nd,128_IXP)

      ms = (nfft_s)/2_IXP + 1_IXP
      md = (nfft_d)/2_IXP + 1_IXP

!
!--->set ns=nfft_s and nd=nfft_d for convenience

      ns = nfft_s
      nd = nfft_d

!
!---->re-adjust ds and dd to match with ns and nd

      ds = ls/real(ns,kind=RXP)
      dd = ld/real(nd,kind=RXP)

!
!---->in case the source is not stochastic, the slip is constant over the fault. 

      if (t == "ct") then
         
         ios = init_array_real(rf,nd,ns,"mod_random_field_sp:random_field_sp_2d:rf")
         
         rf(:,:) = ONE_RXP

         return

      endif

!
!---->set the size of the discrete wavenumber

      fs = 1.0_RXP/(nfft_s*ds)
      fd = 1.0_RXP/(nfft_d*dd)

!
!---->set the wavenumber vectors in one quadrant [0:pi]

      ios = init_array_real(ks,ms,"mod_random_field_sp:random_field_sp_2d:ks")
      ios = init_array_real(kd,md,"mod_random_field_sp:random_field_sp_2d:kd")

      do is = 1_IXP,ms

         ks(is) = real(is-1_IXP)*fs*PI_RXP

      enddo

      do id = 1_IXP,md

         kd(id) = real(id-1_IXP)*fd*PI_RXP

      enddo

!
!---->set the modulus of the wavenumber vector

      ios = init_array_real(kr,md,ms,"mod_random_field_sp:random_field_sp_2d:kr")

      do id = 1_IXP,md

         do is = 1_IXP,ms

            kr(is,id) = sqrt((ad**2)*(kd(id)**2) + (as**2)*(ks(is)**2))

         enddo

      enddo

!
!---->compute the power spectral density

      ios = init_array_real(psd,md,ms,"mod_random_field_sp:random_field_sp_2d:psd")

      if (t == "vk") then ! von Karman power spectral density

         do id = 1_IXP,md
       
            do is = 1_IXP,ms
       
               psd(is,id) = (gamma(k+1.0_RXP)*as*ad)/(gamma(k)*(1.0_RXP+kr(is,id)**2_IXP)**(k+1_RXP))
       
            enddo
       
         enddo

      elseif (t == "gs") then !gaussian power spectral density

         do id = 1_IXP,md
       
            do is = 1_IXP,ms
       
               psd(is,id) = 0.5_RXP*as*ad*exp(-0.25_RXP*kr(is,id)**2_IXP)
       
            enddo
       
         enddo

      endif

!
!---->normalize the power spectral density

      psd(:,:) = psd(:,:)/maxval(psd)

!
!---->set amplitude

      ios = init_array_real(am,md,ms,"mod_random_field_sp:random_field_sp_2d:am")

      am (:,:) = sqrt(psd(:,:))

!
!---->set random phase between 0 and 2*PI

      ios = init_array_real(ph,md,nfft_s,"mod_random_field_sp:random_field_sp_2d:ph")

      call init_random_seed("rss")

      call random_number(ph)

      ph(:,:) = ph(:,:)*2.0_RXP*PI_RXP

!
!---->cpu0 broadcast array 'ph' to all cpus

      call mpi_bcast(ph(1_IXP,1_IXP),md*nfft_s,MPI_REAL,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!
!********************************************************************************************************************
!---->compute random field in the spectral domain 
!********************************************************************************************************************

      ios = init_array_complex(rf_sp,nfft_d,nfft_s,"mod_random_field_sp:random_field_sp_2d:rf_sp")

!---->first 'full' quadrant (md,ms)

      do id = 1_IXP,md

         do is = 1_IXP,ms

            rf_sp(is,id) = am(is,id)*cmplx(cos(ph(is,id)),sin(ph(is,id)))

         enddo

      enddo

!---->set the mean in the spatial domain to zero and imaginary part of other specific sectors to zero

      rf_sp(1_IXP,1_IXP) = cmplx(0.0_RXP,0.0_RXP)

      rf_sp(ms   ,1_IXP) = cmplx(real(rf_sp(ms,1_IXP)),0.0_RXP)

      rf_sp(1_IXP,   md) = cmplx(real(rf_sp(1_IXP,md)),0.0_RXP)

      rf_sp(ms   ,   md) = cmplx(real(rf_sp(ms   ,md)),0.0_RXP)

!---->complete line id=1

      do is = ms+1_IXP,nfft_s

         rf_sp(is,1_IXP) = conjg(rf_sp(2_IXP*ms-is,1_IXP))

      enddo

!---->complete line id=md

      do is = ms+1_IXP,nfft_s

         rf_sp(is,md) = conjg(rf_sp(2_IXP*ms-is,md))

      enddo

!---->complete line is=1

      do id = md+1_IXP,nfft_d

         rf_sp(1_IXP,id) = conjg(rf_sp(1_IXP,2_IXP*md-id))

      enddo

!---->complete line is=ms

      do id = md+1_IXP,nfft_d

         rf_sp(ms,id) = conjg(rf_sp(ms,2_IXP*md-id))

      enddo

!---->second quadrant

      do id = 2_IXP,md-1_IXP

         do is = ms+1_IXP,nfft_s

            rf_sp(is,id) = am(2_IXP*ms-is,id)*cmplx(cos(ph(is,id)),sin(ph(is,id)))

         enddo

      enddo

!---->thrid quadrant

      do id = md+1_IXP,nfft_d

         do is = ms+1_IXP,nfft_s

            rf_sp(is,id) = conjg(rf_sp(2_IXP*ms-is,2_IXP*md-id))

         enddo

      enddo

!---->fourth quadrant

      do id = md+1_IXP,nfft_d

         do is = ms-1_IXP,2_IXP,-1_IXP

            rf_sp(is,id) = conjg(rf_sp(2_IXP*ms-is,2_IXP*md-id))

         enddo

      enddo

!
!
!********************************************************************************************************************
!---->inverse Fourier transform
!********************************************************************************************************************

!---->s-direction

      ios = init_array_complex(rf_tmp,nfft_s,"mod_random_field_sp:random_field_sp_2d:rf_tmp")

      do id = 1_IXP,nfft_d

         rf_tmp(1_IXP:nfft_s) = rf_sp(1_IXP:nfft_s,id)

         call ft(rf_tmp,+1_IXP)

         rf_sp(1_IXP:nfft_s,id) = rf_tmp(1_IXP:nfft_s)

      enddo

      deallocate(rf_tmp)

!---->d-direction

      ios = init_array_complex(rf_tmp,nfft_d,"mod_random_field_sp:random_field_sp_2d:rf_tmp")

      do is = 1_IXP,nfft_s 

         rf_tmp(1_IXP:nfft_d) = rf_sp(is,1_IXP:nfft_d)

         call ft(rf_tmp,+1_IXP)

         rf_sp(is,1_IXP:nfft_d) = rf_tmp(1_IXP:nfft_d)

      enddo

      deallocate(rf_tmp)

!
!---->scale iFFT and store results into real array

      ios = init_array_real(rf,nd,ns,"mod_random_field_sp:random_field_sp_2d:rf")

      rf_sp(:,:) = rf_sp(:,:)/real(nfft_s*nfft_d,kind=RXP) !TODO FLO: normalization of the inverse FFT to be checked

      do id = 1_IXP,nd

         do is = 1_IXP,ns

            rf(is,id) = real(rf_sp(is,id),kind=RXP)

         enddo

      enddo

!
!---->compute mean, variance and standard deviation of rf

      mean = ZERO_RXP

      do id = 1_IXP,nd

         do is = 1_IXP,ns

            mean = mean + rf(is,id)

         enddo

      enddo

      mean = mean/real(ns*nd,kind=RXP)

      variance = ZERO_RXP

      do id = 1_IXP,nd

         do is = 1_IXP,ns

            variance = variance + (rf(is,id) - mean)**2_IXP

         enddo

      enddo

      variance = variance/real(ns*nd,kind=RXP)

      std = sqrt(variance)

!
!---->normalize the standard deviation

      do id = 1_IXP,nd

         do is = 1_IXP,ns

            rf(is,id) = (rf(is,id) - mean)/std

         enddo

      enddo

      return

!
!
!**************************************************************************************************************************
!---->sanity check of the spectrum
!**************************************************************************************************************************

      rf_sp(:,:) = cmplx(ZERO_RXP,ZERO_RXP)

      do id = 1_IXP,nd

         do is = 1_IXP,ns

            rf_sp(is,id) = cmplx(rf(is,id),ZERO_RXP)

         enddo

      enddo
     

!
!---->forward Fourier transform

!---->s-direction

      ios = init_array_complex(rf_tmp,nfft_s,"mod_random_field_sp:random_field_SP_2D:rf_tmp")

      do id = 1_IXP,nfft_d

         rf_tmp(1_IXP:nfft_s) = rf_sp(1_IXP:nfft_s,id)

         call ft(rf_tmp,-1_IXP)

         rf_sp(1_IXP:nfft_s,id) = rf_tmp(1_IXP:nfft_s)

      enddo

      deallocate(rf_tmp)

!---->d-direction

      ios = init_array_complex(rf_tmp,nfft_d,"mod_random_field_sp:random_field_SP_2D:rf_tmp")

      do is = 1_IXP,nfft_s

         rf_tmp(1_IXP:nfft_d) = rf_sp(is,1_IXP:nfft_d)

         call ft(rf_tmp,-1_IXP)

         rf_sp(is,1_IXP:nfft_d) = rf_tmp(1_IXP:nfft_d)

      enddo

      rf_sp(:,:) = rf_sp(:,:)/real(nfft_s*nfft_d,kind=RXP) !TODO FLO: normalization of the FFT to be checked. either by nfft_s or by ds.

      do id = 1_IXP,nfft_d

         write(602,'(1000(E15.7,1X))') (abs(rf_sp(is,id)),is=1_IXP,nfft_s)

      enddo

      do id = 1_IXP,nfft_d

         write(702,'(1000(E15.7,1X))') (real(rf_sp(is,id)),is=1_IXP,nfft_s)

      enddo

      do id = 1_IXP,nfft_d

         write(802,'(1000(E15.7,1X))') (aimag(rf_sp(is,id)),is=1_IXP,nfft_s)

      enddo
!**************************************************************************************************************************

      return

!***************************************************************************************************************
   end subroutine random_field_sp_2d
!***************************************************************************************************************


!
!
!>@brief subroutine to lift (shift) the random field 'rf' above zero
!>@param  rf : 2D random field
!>@return rf : 2D random field
!***************************************************************************************************************
   subroutine random_field_sp_2d_lift_zero(rf)
!***************************************************************************************************************

      implicit none

      real(kind=RXP), dimension(:,:), intent(out) :: rf
      real(kind=RXP)                              :: min_val

!
!---->shift random field above zero
!     for safety: adding 10*EPSILON_MACHINE_RXP to avoid -Infinity when taking log10 (see subroutine mod_source::init_fault_magnitude). No impact on the result.

      min_val = minval(rf(:,:))

      rf(:,:) = rf(:,:) - min_val + 10_RXP*EPSILON_MACHINE_RXP

      return

!***************************************************************************************************************
   end subroutine random_field_sp_2d_lift_zero
!***************************************************************************************************************


!
!
!>@brief subroutine to normalize the random field 'rf' by its sum
!>@param  rf : 2D random field
!>@return rf : 2D random field
!***************************************************************************************************************
   subroutine random_field_sp_2d_norm_sum(rf)
!***************************************************************************************************************

      implicit none

      real(kind=RXP), dimension(:,:), intent(out) :: rf
      real(kind=RXP)                              :: sum_val

!
!---->normalize with respect to the sum of its values.

      sum_val = sum(rf(:,:))

      rf(:,:) = rf(:,:)/sum_val

      return

!***************************************************************************************************************
   end subroutine random_field_sp_2d_norm_sum
!***************************************************************************************************************




!
!
!>@brief subroutine to compute 2D random field using a spectral method
!>@param  ls : fault length along the strike direction
!>@param  ld : fault length along the dip direction
!>@param  as : correlation length along the strike direction
!>@param  ad : correlation length along the dip direction
!>@param  k  : kappa (Hurst number)
!>@param  t  : type of autocorrelation
!>@return ds : discretization step along strike
!>@return dd : discretization step along dip
!>@return rf : 2D random field
!***************************************************************************************************************
   subroutine random_field_sp_2d_par(ls,ld,as,ad,k,t,ds,dd,rf)
!***************************************************************************************************************

      use mpi

      use mod_global_variables , only :&
                                       ig_mpi_comm_simu&
                                      ,partition1d&
                                      ,ig_myrank&
                                      ,ig_ncpu

      use mod_signal_processing, only :&
                                       nextpow2&
                                      ,ft

      use mod_random_field     , only : init_random_seed

      implicit none

      character(len=2)                              , intent(in)           :: t
      real   (kind=RXP)                             , intent(in)           :: ls
      real   (kind=RXP)                             , intent(in)           :: ld
      real   (kind=RXP)                             , intent(in), optional :: as
      real   (kind=RXP)                             , intent(in), optional :: ad
      real   (kind=RXP)                             , intent(in), optional :: k
      real   (kind=RXP)                             , intent(out)          :: ds
      real   (kind=RXP)                             , intent(out)          :: dd
      real   (kind=RXP), dimension(:,:), allocatable, intent(out)          :: rf

      real   (kind=RXP), dimension(:)  , allocatable                       :: ks
      real   (kind=RXP), dimension(:)  , allocatable                       :: kd
      real   (kind=RXP), dimension(:,:), allocatable                       :: kr
      real   (kind=RXP), dimension(:,:), allocatable                       :: psd
      real   (kind=RXP), dimension(:,:), allocatable                       :: am
      real   (kind=RXP), dimension(:,:), allocatable                       :: ph
                                                                           
      complex(kind=RXP), dimension(:,:), allocatable                       :: rf_sp
      complex(kind=RXP), dimension(:,:), allocatable                       :: rf_tmp
                                                                           
      real   (kind=RXP)                                                    :: fs
      real   (kind=RXP)                                                    :: fd
                                                                           
      real   (kind=RXP)                                                    :: mean
      real   (kind=RXP)                                                    :: variance
      real   (kind=RXP)                                                    :: std
                                                                           
      integer(kind=IXP), dimension(:), allocatable                         :: block_len
      integer(kind=MPI_ADDRESS_KIND), dimension(:), allocatable            :: block_dis

      integer(kind=IXP), dimension(ig_ncpu)                                :: istart_all_cpus
      integer(kind=IXP), dimension(ig_ncpu)                                :: npart_all_cpus
      integer(kind=IXP), dimension(ig_ncpu)                                :: sendcounts
      integer(kind=IXP), dimension(ig_ncpu)                                :: recvcounts
      integer(kind=IXP), dimension(ig_ncpu)                                :: senddispls
      integer(kind=IXP), dimension(ig_ncpu)                                :: recvdispls
      integer(kind=IXP), dimension(ig_ncpu)                                :: sendtype_a
      integer(kind=IXP), dimension(ig_ncpu)                                :: recvtype_a
      integer(kind=IXP)                                                    :: sendtype
      integer(kind=IXP)                                                    :: mpi_nboctet_complex
      integer(kind=IXP)                                                    :: icpu
      integer(kind=IXP)                                                    :: is 
      integer(kind=IXP)                                                    :: js 
      integer(kind=IXP)                                                    :: id 
      integer(kind=IXP)                                                    :: jd 
      integer(kind=IXP)                                                    :: istart 
      integer(kind=IXP)                                                    :: iend 
      integer(kind=IXP)                                                    :: npart 
      integer(kind=IXP)                                                    :: ns !number of grid points along the strike
      integer(kind=IXP)                                                    :: nd !number of grid points along the dip
      integer(kind=IXP)                                                    :: ms !number of grid points in the spectral domain
      integer(kind=IXP)                                                    :: md !number of grid points in the spectral domain
      integer(kind=IXP)                                                    :: nfft_s
      integer(kind=IXP)                                                    :: nfft_d
      integer(kind=IXP)                                                    :: ios

!
!---->set the spatial discrtization size with respect to the correlation length

      if (t == "ct") then

         ds = ls/5.0_RXP
         dd = ld/5.0_RXP

      elseif (t == "gs") then

         ds = as/5.0_RXP
         dd = ad/5.0_RXP
      
      elseif (t == "vk") then

         ds = as/5.0_RXP
         dd = ad/5.0_RXP

      endif

!
!---->approximately set the number of "sub-fault"

      ns = int(ls/ds)
      nd = int(ld/dd)

!
!---->set the size of the spectral domain (power of two for FFT)

      nfft_s = nextpow2(ns)
      nfft_d = nextpow2(nd)

      ms = (nfft_s)/2_IXP + 1_IXP
      md = (nfft_d)/2_IXP + 1_IXP

!
!--->set ns=nfft_s and nd=nfft_d for convenience

      ns = nfft_s
      nd = nfft_d

!
!---->re-adjust ds and dd to match with ns and nd

      ds = ls/real(ns,kind=RXP)
      dd = ld/real(nd,kind=RXP)

!
!---->in case the source is not stochastic, the slip is constant over the fault. 

      if (t == "ct") then
         
         ios = init_array_real(rf,nd,ns,"mod_random_field_sp:random_field_sp_2d:rf")
         
         rf(:,:) = ONE_RXP

         return

      endif

!
!---->set the size of the discrete wavenumber

      fs = 1.0_RXP/(nfft_s*ds)
      fd = 1.0_RXP/(nfft_d*dd)

!
!---->set the wavenumber vectors in one quadrant [0:pi]

      ios = init_array_real(ks,ms,"mod_random_field_sp:random_field_sp_2d:ks")
      ios = init_array_real(kd,md,"mod_random_field_sp:random_field_sp_2d:kd")

      do is = 1_IXP,ms

         ks(is) = real(is-1_IXP)*fs*PI_RXP

      enddo

      do id = 1_IXP,md

         kd(id) = real(id-1_IXP)*fd*PI_RXP

      enddo

!
!---->set the modulus of the wavenumber vector

      ios = init_array_real(kr,md,ms,"mod_random_field_sp:random_field_sp_2d:kr")

      do id = 1_IXP,md

         do is = 1_IXP,ms

            kr(is,id) = sqrt((ad**2)*(kd(id)**2) + (as**2)*(ks(is)**2))

         enddo

      enddo

!
!---->compute the power spectral density

      ios = init_array_real(psd,md,ms,"mod_random_field_sp:random_field_sp_2d:psd")

      if (t == "vk") then ! von Karman power spectral density

         do id = 1_IXP,md
       
            do is = 1_IXP,ms
       
               psd(is,id) = (gamma(k+1.0_RXP)*as*ad)/(gamma(k)*(1.0_RXP+kr(is,id)**2_IXP)**(k+1_RXP))
       
            enddo
       
         enddo

      elseif (t == "gs") then !gaussian power spectral density

         do id = 1_IXP,md
       
            do is = 1_IXP,ms
       
               psd(is,id) = 0.5_RXP*as*ad*exp(-0.25_RXP*kr(is,id)**2_IXP)
       
            enddo
       
         enddo

      endif

!
!---->normalize the power spectral density

      psd(:,:) = psd(:,:)/maxval(psd)

!
!---->set amplitude

      ios = init_array_real(am,md,ms,"mod_random_field_sp:random_field_sp_2d:am")

      am (:,:) = sqrt(psd(:,:))

!
!---->set random phase between 0 and 2*PI

      ios = init_array_real(ph,md,nfft_s,"mod_random_field_sp:random_field_sp_2d:ph")

      call init_random_seed("rss")

      call random_number(ph)

      ph(:,:) = ph(:,:)*2.0_RXP*PI_RXP

!
!---->cpu0 broadcast array 'ph' to all cpus

      call mpi_bcast(ph(1_IXP,1_IXP),md*nfft_s,MPI_REAL,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!
!********************************************************************************************************************
!---->compute random field in the spectral domain 
!********************************************************************************************************************

      ios = init_array_complex(rf_sp,nfft_d,nfft_s,"mod_random_field_sp:random_field_sp_2d:rf_sp")

!---->first 'full' quadrant (md,ms)

      do id = 1_IXP,md

         do is = 1_IXP,ms

            rf_sp(is,id) = am(is,id)*cmplx(cos(ph(is,id)),sin(ph(is,id)))

         enddo

      enddo

!---->set the mean in the spatial domain to zero and imaginary part of other specific sectors to zero

      rf_sp(1_IXP,1_IXP) = cmplx(0.0_RXP,0.0_RXP)

      rf_sp(ms   ,1_IXP) = cmplx(real(rf_sp(ms,1_IXP)),0.0_RXP)

      rf_sp(1_IXP,   md) = cmplx(real(rf_sp(1_IXP,md)),0.0_RXP)

      rf_sp(ms   ,   md) = cmplx(real(rf_sp(ms   ,md)),0.0_RXP)

!---->complete line id=1

      do is = ms+1_IXP,nfft_s

         rf_sp(is,1_IXP) = conjg(rf_sp(2_IXP*ms-is,1_IXP))

      enddo

!---->complete line id=md

      do is = ms+1_IXP,nfft_s

         rf_sp(is,md) = conjg(rf_sp(2_IXP*ms-is,md))

      enddo

!---->complete line is=1

      do id = md+1_IXP,nfft_d

         rf_sp(1_IXP,id) = conjg(rf_sp(1_IXP,2_IXP*md-id))

      enddo

!---->complete line is=ms

      do id = md+1_IXP,nfft_d

         rf_sp(ms,id) = conjg(rf_sp(ms,2_IXP*md-id))

      enddo

!---->second quadrant

      do id = 2_IXP,md-1_IXP

         do is = ms+1_IXP,nfft_s

            rf_sp(is,id) = am(2_IXP*ms-is,id)*cmplx(cos(ph(is,id)),sin(ph(is,id)))

         enddo

      enddo

!---->thrid quadrant

      do id = md+1_IXP,nfft_d

         do is = ms+1_IXP,nfft_s

            rf_sp(is,id) = conjg(rf_sp(2_IXP*ms-is,2_IXP*md-id))

         enddo

      enddo

!---->fourth quadrant

      do id = md+1_IXP,nfft_d

         do is = ms-1_IXP,2_IXP,-1_IXP

            rf_sp(is,id) = conjg(rf_sp(2_IXP*ms-is,2_IXP*md-id))

         enddo

      enddo

!
!
!********************************************************************************************************************
!---->inverse Fourier transform
!********************************************************************************************************************

!
!---->partition (among the available cpus) the FFTs to perform along the s-direction -> make partition along nfft_d

      call partition1d(ig_myrank,ig_ncpu,nfft_d,istart,iend)

      if (istart > ZERO_IXP) then

         npart = iend - istart + 1_IXP

      else

         npart = ZERO_IXP

      endif

!
!---->cpus exchange 'npart' to prepare alltoallw

      call mpi_allgather(npart,1_IXP,MPI_INTEGER,npart_all_cpus,1_IXP,MPI_INTEGER,ig_mpi_comm_simu,ios)

!
!---->cpus exchange 'istart' to prepare alltoallw

      call mpi_allgather(istart,1_IXP,MPI_INTEGER,istart_all_cpus,1_IXP,MPI_INTEGER,ig_mpi_comm_simu,ios)

!
!---->cpus set counts to be sent and received

      do icpu = ONE_IXP,ig_ncpu

         sendcounts(icpu) = npart

      enddo

      do icpu = ONE_IXP,ig_ncpu

         recvcounts(icpu) = npart_all_cpus(icpu)

      enddo

!
!---->cpus create type to be sent

      call mpi_type_contiguous(nfft_s,MPI_COMPLEX,sendtype,ios)

      call mpi_type_commit(sendtype,ios)

!
!---->cpus create senddispls (the displacement from which to take the outgoing data) and recvdispls (the displacement at which to place the incoming data)

      do icpu = ONE_IXP,ig_ncpu

         senddispls(icpu) = ZERO_IXP

      enddo

      do icpu = ONE_IXP,ig_ncpu
      
         recvdispls(icpu) = max(istart_all_cpus(icpu) - ONE_IXP , ZERO_IXP)
      
      enddo

!
!---->perform FFTs along the s-direction

      if (npart > ZERO_IXP) then

         ios = init_array_complex(rf_tmp,npart,nfft_s,"mod_random_field_sp:random_field_sp_2d:rf_tmp")
       
         jd = ZERO_IXP
       
         do id = istart,iend
       
            jd = jd + ONE_IXP
       
            rf_tmp(1_IXP:nfft_s,jd) = rf_sp(1_IXP:nfft_s,id)
       
            call ft(rf_tmp(:,jd),+1_IXP)

         enddo
       
      else

         ios = init_array_complex(rf_tmp,1_IXP,1_IXP,"mod_random_field_sp:random_field_sp_2d:rf_tmp") !dummy not used

      endif

!
!---->s-direction: each cpu get the full matrix before partitioning and computing the d-direction. recvctype=sendtype, so only sendtype is defined

      call mpi_alltoallv(rf_tmp,sendcounts,senddispls,sendtype,rf_sp,recvcounts,recvdispls,sendtype,ig_mpi_comm_simu,ios)

      deallocate(rf_tmp)

      call mpi_type_free(sendtype,ios)

!
!---->partition (among the available cpus) the FFTs to perform along the d-direction -> make partition along nfft_s

      call partition1d(ig_myrank,ig_ncpu,nfft_s,istart,iend)

      if (istart > ZERO_IXP) then

         npart = iend - istart + 1_IXP

      else

         npart = ZERO_IXP

      endif

!
!---->cpus exchange 'npart' to prepare alltoallw

      call mpi_allgather(npart,1_IXP,MPI_INTEGER,npart_all_cpus,1_IXP,MPI_INTEGER,ig_mpi_comm_simu,ios)

!
!---->cpus exchange 'istart' to prepare alltoallw

      call mpi_allgather(istart,1_IXP,MPI_INTEGER,istart_all_cpus,1_IXP,MPI_INTEGER,ig_mpi_comm_simu,ios)

!
!---->cpus set counts to be sent and received

      if (npart > ZERO_IXP) then

         sendcounts(:) = ONE_IXP

      else

         sendcounts(:) = ZERO_IXP

      endif
      

      do icpu = ONE_IXP,ig_ncpu

         if (npart_all_cpus(icpu) > ZERO_IXP) then

            recvcounts(icpu) = ONE_IXP 

         else

            recvcounts(icpu) = ZERO_IXP

         endif

      enddo

!
!---->cpus create type to be sent and received

      call mpi_type_size(MPI_COMPLEX,mpi_nboctet_complex,ios)

      call mpi_type_vector(npart,ONE_IXP,nfft_d,MPI_COMPLEX,sendtype,ios)

      call mpi_type_commit(sendtype,ios)

      ios = init_array_int(block_len,nfft_d,"mod_random_field_sp:random_field_sp_2d_par:block_len")

      ios = init_array_int(block_dis,nfft_d,"mod_random_field_sp:random_field_sp_2d_par:block_dis")

      block_len(:) = ONE_IXP

      do id = ONE_IXP,nfft_d

         block_dis(id) = (int(id,kind=I64) - ONE_I64)*int(mpi_nboctet_complex,kind=I64)

      enddo

      do icpu = ONE_IXP,ig_ncpu

         call mpi_type_create_hindexed(nfft_d,block_len,block_dis,sendtype,sendtype_a(icpu),ios)

         call mpi_type_commit(sendtype_a(icpu),ios)

      enddo

      do icpu = ONE_IXP,ig_ncpu

         call mpi_type_vector(nfft_d,npart_all_cpus(icpu),nfft_s,MPI_COMPLEX,recvtype_a(icpu),ios)

         call mpi_type_commit(recvtype_a(icpu),ios)

      enddo

!
!---->cpus create senddispls (the displacement from which to take the outgoing data) and recvdispls (the displacement at which to place the incoming data)

      do icpu = ONE_IXP,ig_ncpu

         senddispls  (icpu) = ZERO_IXP

      enddo

      do icpu = ONE_IXP,ig_ncpu
      
         recvdispls(icpu) = max((istart_all_cpus(icpu)-ONE_IXP)*mpi_nboctet_complex,ZERO_IXP)
      
      enddo

!
!---->perform FFTs along the s-direction

      if (npart > ZERO_IXP) then

         ios = init_array_complex(rf_tmp,npart,nfft_d,"mod_random_field_sp:random_field_sp_2d:rf_tmp")
       
         js = ZERO_IXP
       
         do is = istart,iend
       
            js = js + ONE_IXP
       
            rf_tmp(1_IXP:nfft_d,js) = rf_sp(is,1_IXP:nfft_d)
       
            call ft(rf_tmp(:,js),+1_IXP)

         enddo
       
      else

         ios = init_array_complex(rf_tmp,1_IXP,1_IXP,"mod_random_field_sp:random_field_sp_2d:rf_tmp") !dummy not used

      endif

!
!---->s-direction: each cpu get the full matrix before partitioning and computing the d-direction. 

      call mpi_alltoallw(rf_tmp,sendcounts,senddispls,sendtype_a,rf_sp,recvcounts,recvdispls,recvtype_a,ig_mpi_comm_simu,ios)

      deallocate(rf_tmp)

      do icpu = ONE_IXP,ig_ncpu

         call mpi_type_free(sendtype_a(icpu),ios)

      enddo

      do icpu = ONE_IXP,ig_ncpu

         call mpi_type_free(recvtype_a(icpu),ios)

      enddo

!
!---->scale iFFT and store results into real array

      ios = init_array_real(rf,nd,ns,"mod_random_field_sp:random_field_sp_2d:rf")

      rf_sp(:,:) = rf_sp(:,:)/real(nfft_s*nfft_d,kind=RXP)

      do id = 1_IXP,nd

         do is = 1_IXP,ns

            rf(is,id) = real(rf_sp(is,id),kind=RXP)

         enddo

      enddo

!
!---->compute mean, variance and standard deviation of rf

      mean = ZERO_RXP

      do id = 1_IXP,nd

         do is = 1_IXP,ns

            mean = mean + rf(is,id)

         enddo

      enddo

      mean = mean/real(ns*nd,kind=RXP)

      variance = ZERO_RXP

      do id = 1_IXP,nd

         do is = 1_IXP,ns

            variance = variance + (rf(is,id) - mean)**2_IXP

         enddo

      enddo

      variance = variance/real(ns*nd,kind=RXP)

      std = sqrt(variance)

!
!---->normalize the standard deviation

      do id = 1_IXP,nd

         do is = 1_IXP,ns

            rf(is,id) = (rf(is,id) - mean)/std

         enddo

      enddo

      return

!!
!!
!!**************************************************************************************************************************
!!---->sanity check of the spectrum
!!**************************************************************************************************************************
!
!      rf_sp(:,:) = cmplx(ZERO_RXP,ZERO_RXP)
!
!      do id = 1_IXP,nd
!
!         do is = 1_IXP,ns
!
!            rf_sp(is,id) = cmplx(rf(is,id),ZERO_RXP)
!
!         enddo
!
!      enddo
!     
!
!!
!!---->forward Fourier transform
!
!!---->s-direction
!
!      ios = init_array_complex(rf_tmp,nfft_s,"mod_random_field_sp:random_field_SP_2D:rf_tmp")
!
!      do id = 1_IXP,nfft_d
!
!         rf_tmp(1_IXP:nfft_s) = rf_sp(1_IXP:nfft_s,id)
!
!         call ft(rf_tmp,-1_IXP)
!
!         rf_sp(1_IXP:nfft_s,id) = rf_tmp(1_IXP:nfft_s)
!
!      enddo
!
!      deallocate(rf_tmp)
!
!!---->d-direction
!
!      ios = init_array_complex(rf_tmp,nfft_d,"mod_random_field_sp:random_field_SP_2D:rf_tmp")
!
!      do is = 1_IXP,nfft_s
!
!         rf_tmp(1_IXP:nfft_d) = rf_sp(is,1_IXP:nfft_d)
!
!         call ft(rf_tmp,-1_IXP)
!
!         rf_sp(is,1_IXP:nfft_d) = rf_tmp(1_IXP:nfft_d)
!
!      enddo
!
!      rf_sp(:,:) = rf_sp(:,:)/real(nfft_s*nfft_d,kind=RXP)
!
!      do id = 1_IXP,nfft_d
!
!         write(602,'(1000(E15.7,1X))') (abs(rf_sp(is,id)),is=1_IXP,nfft_s)
!
!      enddo
!
!      do id = 1_IXP,nfft_d
!
!         write(702,'(1000(E15.7,1X))') (real(rf_sp(is,id)),is=1_IXP,nfft_s)
!
!      enddo
!
!      do id = 1_IXP,nfft_d
!
!         write(802,'(1000(E15.7,1X))') (aimag(rf_sp(is,id)),is=1_IXP,nfft_s)
!
!      enddo
!!**************************************************************************************************************************
!
!      return

!***************************************************************************************************************
   end subroutine random_field_sp_2d_par
!***************************************************************************************************************



end module mod_random_field_sp 
