!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to compute jacobian matrix.

!>@brief
!!This module contains subroutines to compute jacobian matrix. 
module mod_jacobian
   
   use mpi

   use mod_precision
   
   implicit none

   private

   public  :: compute_hexa_jacobian
   public  :: compute_quad_jacobian

   contains
   
!
!
!>@brief
!!This subroutine computes jacobian matrix and its determinant at location @f$\xi,\eta,\zeta@f$ in hexahedron element ihexa
!>@param ihexa   : hexahedron element number in cpu myrank
!>@param xisol   : @f$\xi  @f$ local coordinate where to compute jacobian matrix and its determinant
!>@param etsol   : @f$\eta @f$ local coordinate where to compute jacobian matrix and its determinant
!>@param zesol   : @f$\zeta@f$ local coordinate where to compute jacobian matrix and its determinant
!>@param dxidx   : @f$\frac{\partial \xi  }{\partial x}@f$
!>@param dxidy   : @f$\frac{\partial \xi  }{\partial y}@f$
!>@param dxidz   : @f$\frac{\partial \xi  }{\partial z}@f$
!>@param detdx   : @f$\frac{\partial \eta }{\partial x}@f$
!>@param detdy   : @f$\frac{\partial \eta }{\partial y}@f$
!>@param detdz   : @f$\frac{\partial \eta }{\partial z}@f$
!>@param dzedx   : @f$\frac{\partial \zeta}{\partial x}@f$
!>@param dzedy   : @f$\frac{\partial \zeta}{\partial y}@f$
!>@param dzedz   : @f$\frac{\partial \zeta}{\partial z}@f$
!>@param det     : determinant of jacobian matrix
!***********************************************************************************************************************************************************************************
   subroutine compute_hexa_jacobian(ihexa,xisol,etsol,zesol,dxidx,dxidy,dxidz,detdx,detdy,detdz,dzedx,dzedy,dzedz,det)
!***********************************************************************************************************************************************************************************
   
      use mpi
   
      use mod_global_variables, only : ig_hexa_gnode_xiloc&
                                      ,ig_hexa_gnode_etloc&
                                      ,ig_hexa_gnode_zeloc&
                                      ,ig_line_nnode&
                                      ,rg_gnode_x&
                                      ,rg_gnode_y&
                                      ,rg_gnode_z&
                                      ,ig_hexa_nnode&
                                      ,ig_hexa_gnode_glonum&
                                      ,ig_myrank&
                                      ,ig_mpi_comm_simu

      use mod_lagrange        , only :&
                                      lagrap_geom&
                                     ,lagrad_geom
      
      implicit none
      
      real(kind=RXP)  , intent( in) :: xisol
      real(kind=RXP)  , intent( in) :: etsol
      real(kind=RXP)  , intent( in) :: zesol
      integer(kind=IXP), intent( in) :: ihexa
      real(kind=RXP)  , intent(out) :: dxidx
      real(kind=RXP)  , intent(out) :: dxidy
      real(kind=RXP)  , intent(out) :: dxidz
      real(kind=RXP)  , intent(out) :: detdx
      real(kind=RXP)  , intent(out) :: detdy
      real(kind=RXP)  , intent(out) :: detdz
      real(kind=RXP)  , intent(out) :: dzedx
      real(kind=RXP)  , intent(out) :: dzedy
      real(kind=RXP)  , intent(out) :: dzedz
      real(kind=RXP)  , intent(out) :: det
 
      real(kind=RXP)  , parameter   :: EPS = 1.0e-8_RXP
      real(kind=RXP)                :: inv_det
      real(kind=RXP)                :: xxi
      real(kind=RXP)                :: xet
      real(kind=RXP)                :: xze
      real(kind=RXP)                :: yxi
      real(kind=RXP)                :: yet
      real(kind=RXP)                :: yze
      real(kind=RXP)                :: zxi
      real(kind=RXP)                :: zet
      real(kind=RXP)                :: zze
      real(kind=RXP)                :: lag_deriv_xi
      real(kind=RXP)                :: lag_deriv_et
      real(kind=RXP)                :: lag_deriv_ze
      real(kind=RXP)                :: lag_xi
      real(kind=RXP)                :: lag_et
      real(kind=RXP)                :: lag_ze
      real(kind=RXP)                :: xgnode
      real(kind=RXP)                :: ygnode
      real(kind=RXP)                :: zgnode
      integer(kind=IXP)             :: m
      integer(kind=IXP)             :: ios

!
!
!---->initialize partial derivatives

      xxi = ZERO_RXP
      xet = ZERO_RXP
      xze = ZERO_RXP
      yxi = ZERO_RXP
      yet = ZERO_RXP
      yze = ZERO_RXP
      zxi = ZERO_RXP
      zet = ZERO_RXP
      zze = ZERO_RXP
   
!
!
!---->compute partial derivatives

      do m = ONE_IXP,ig_hexa_nnode
   
         lag_deriv_xi = lagrad_geom(ig_hexa_gnode_xiloc(m),xisol,ig_line_nnode)
         lag_deriv_et = lagrad_geom(ig_hexa_gnode_etloc(m),etsol,ig_line_nnode)
         lag_deriv_ze = lagrad_geom(ig_hexa_gnode_zeloc(m),zesol,ig_line_nnode)
         
         lag_xi       = lagrap_geom(ig_hexa_gnode_xiloc(m),xisol,ig_line_nnode)
         lag_et       = lagrap_geom(ig_hexa_gnode_etloc(m),etsol,ig_line_nnode)
         lag_ze       = lagrap_geom(ig_hexa_gnode_zeloc(m),zesol,ig_line_nnode)
         
         xgnode       = rg_gnode_x(ig_hexa_gnode_glonum(m,ihexa))
         ygnode       = rg_gnode_y(ig_hexa_gnode_glonum(m,ihexa))
         zgnode       = rg_gnode_z(ig_hexa_gnode_glonum(m,ihexa))
   
         xxi          = xxi + lag_deriv_xi*lag_et*lag_ze*xgnode
         yxi          = yxi + lag_deriv_xi*lag_et*lag_ze*ygnode
         zxi          = zxi + lag_deriv_xi*lag_et*lag_ze*zgnode
                      
         xet          = xet + lag_xi*lag_deriv_et*lag_ze*xgnode
         yet          = yet + lag_xi*lag_deriv_et*lag_ze*ygnode
         zet          = zet + lag_xi*lag_deriv_et*lag_ze*zgnode
                      
         xze          = xze + lag_xi*lag_et*lag_deriv_ze*xgnode
         yze          = yze + lag_xi*lag_et*lag_deriv_ze*ygnode
         zze          = zze + lag_xi*lag_et*lag_deriv_ze*zgnode
   
      enddo
!
!
!---->compute determinant and its inverse

      det     = xxi*yet*zze - xxi*yze*zet + xet*yze*zxi - xet*yxi*zze + xze*yxi*zet - xze*yet*zxi
      inv_det = ONE_RXP/det
   
      if (det.lt.EPS) then
         write(*,*) "***error in compute_hexa_jacobian: det null or negative, det = ",det
         write(*,*) "***element ",ihexa, "cpu ",ig_myrank
         call mpi_abort(ig_mpi_comm_simu,100_IXP,ios)
         stop
      endif

!
!
!---->compute partial derivatives

      dxidx = (yet*zze-yze*zet)*inv_det
      dxidy = (xze*zet-xet*zze)*inv_det
      dxidz = (xet*yze-xze*yet)*inv_det
      detdx = (yze*zxi-yxi*zze)*inv_det
      detdy = (xxi*zze-xze*zxi)*inv_det
      detdz = (xze*yxi-xxi*yze)*inv_det
      dzedx = (yxi*zet-yet*zxi)*inv_det
      dzedy = (xet*zxi-xxi*zet)*inv_det
      dzedz = (xxi*yet-xet*yxi)*inv_det

      return
!***********************************************************************************************************************************************************************************
   end subroutine compute_hexa_jacobian
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine computes jacobian matrix and its determinant at location @f$\xi,\eta@f$ in quadrangle element iquad
!>@param iquad   : quadrangle element number in cpu myrank
!>@param xisol   : @f$\xi  @f$ local coordinate where to compute jacobian matrix and its determinant
!>@param etsol   : @f$\eta @f$ local coordinate where to compute jacobian matrix and its determinant
!>@param dxidx   : @f$\frac{\partial \xi  }{\partial x}@f$
!>@param dxidy   : @f$\frac{\partial \xi  }{\partial y}@f$
!>@param detdx   : @f$\frac{\partial \eta }{\partial x}@f$
!>@param detdy   : @f$\frac{\partial \eta }{\partial y}@f$
!>@param det     : determinant of jacobian matrix
!***********************************************************************************************************************************************************************************
   subroutine compute_quad_jacobian(iquad,xisol,etsol,dxidx,dxidy,detdx,detdy,det)
!***********************************************************************************************************************************************************************************
   
      use mpi
   
      use mod_global_variables, only : ig_quad_gnode_xiloc&
                                      ,ig_quad_gnode_etloc&
                                      ,ig_line_nnode&
                                      ,rg_gnode_x&
                                      ,rg_gnode_y&
                                      ,ig_quad_nnode&
                                      ,ig_quadf_gnode_glonum&
                                      ,ig_myrank&
                                      ,ig_mpi_comm_simu
      
      use mod_lagrange        , only :&
                                      lagrap_geom&
                                     ,lagrad_geom
      implicit none
      
      real(kind=RXP)  , intent(out) :: dxidx
      real(kind=RXP)  , intent(out) :: dxidy
      real(kind=RXP)  , intent(out) :: detdx
      real(kind=RXP)  , intent(out) :: detdy
      real(kind=RXP)  , intent( in) :: xisol
      real(kind=RXP)  , intent( in) :: etsol
      integer(kind=IXP), intent( in) :: iquad
 
      real(kind=RXP), parameter     :: EPS = 1.0e-8_RXP
      real(kind=RXP)                :: xxi
      real(kind=RXP)                :: xet
      real(kind=RXP)                :: yxi
      real(kind=RXP)                :: yet
      real(kind=RXP)                :: det
      real(kind=RXP)                :: inv_det
      real(kind=RXP)                :: lag_deriv_xi
      real(kind=RXP)                :: lag_deriv_et
      real(kind=RXP)                :: lag_xi
      real(kind=RXP)                :: lag_et
      real(kind=RXP)                :: xgnode
      real(kind=RXP)                :: ygnode
      integer(kind=IXP)             :: m
      integer(kind=IXP)             :: ios

!
!
!---->initialize partial derivatives

      xxi = ZERO_RXP
      xet = ZERO_RXP
      yxi = ZERO_RXP
      yet = ZERO_RXP
   
!
!
!---->compute partial derivatives

      do m = ONE_IXP,ig_quad_nnode
   
         lag_deriv_xi = lagrad_geom(ig_quad_gnode_xiloc(m),xisol,ig_line_nnode)
         lag_deriv_et = lagrad_geom(ig_quad_gnode_etloc(m),etsol,ig_line_nnode)
         
         lag_xi       = lagrap_geom(ig_quad_gnode_xiloc(m),xisol,ig_line_nnode)
         lag_et       = lagrap_geom(ig_quad_gnode_etloc(m),etsol,ig_line_nnode)
         
         xgnode       = rg_gnode_x(ig_quadf_gnode_glonum(m,iquad))
         ygnode       = rg_gnode_y(ig_quadf_gnode_glonum(m,iquad))
   
         xxi          = xxi + lag_deriv_xi*lag_et*xgnode
         yxi          = yxi + lag_deriv_xi*lag_et*ygnode
                      
         xet          = xet + lag_xi*lag_deriv_et*xgnode
         yet          = yet + lag_xi*lag_deriv_et*ygnode
                       
      enddo

!
!
!---->compute determinant and its inverse

      det     = xxi*yet - xet*yxi
      inv_det = ONE_RXP/det
   
      if (det.lt.EPS) then
         write(*,*) "***error in compute_quad_jacobian: det null or negative, det = ",det
         write(*,*) "***element ",iquad, "cpu ",ig_myrank
         call mpi_abort(ig_mpi_comm_simu,100_IXP,ios)
         stop
      endif

!
!
!---->compute partial derivatives

      dxidx = (+yet)*inv_det
      dxidy = (-xet)*inv_det
      detdx = (-yxi)*inv_det
      detdy = (+xxi)*inv_det
   
      return
!***********************************************************************************************************************************************************************************
   end subroutine compute_quad_jacobian
!***********************************************************************************************************************************************************************************

end module mod_jacobian
