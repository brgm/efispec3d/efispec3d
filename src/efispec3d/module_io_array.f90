!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to read/write arrays to disk using mpi IO.

!>@brief
!!This module contains subroutines to write results to disk using mpi IO.
module mod_io_array

   use mod_precision

   use mod_global_variables, only :&
                                   CIL&
                                  ,error_stop

   implicit none

   private

   public :: get_file_rank_elt_offset
   public :: get_file_shape
   public :: efi_mpi_file_open
   public :: efi_mpi_file_write_at_all
   public :: efi_mpi_file_read_at_all

   interface efi_mpi_file_write_at_all

      module procedure efi_mpi_file_write_at_all_rank2
      module procedure efi_mpi_file_write_at_all_rank3
      module procedure efi_mpi_file_write_at_all_rank4
      module procedure efi_mpi_file_write_at_all_rank5

      module procedure efi_mpi_file_write_at_all_fault

   end interface efi_mpi_file_write_at_all


   interface efi_mpi_file_read_at_all

      module procedure efi_mpi_file_read_at_all_rank3
      module procedure efi_mpi_file_read_at_all_rank3_ixp
      module procedure efi_mpi_file_read_at_all_rank4
      module procedure efi_mpi_file_read_at_all_rank5

   end interface efi_mpi_file_read_at_all


   contains

!
!
!>@brief subroutine that returns the element offset for a rank to write in file
!>@param  rank       : current rank (starting from zero)
!>@param  nrank      : total number of ranks
!>@param  nelt_all   : total number of elements over all ranks
!>@return nelt_rank  : total number of elements over current rank
!>@return nelt_floor : floor value of nelt_all/nrank
!>@return nelt_offset: offset in terms of element
!***********************************************************************************************************************************************************************************
   subroutine get_file_rank_elt_offset(rank,nrank,nelt_all,nelt_rank,nelt_floor,ielt_offset)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in) :: rank !start from 0
      integer(kind=IXP), intent( in) :: nrank
      integer(kind=IXP), intent( in) :: nelt_all
      integer(kind=IXP), intent(out) :: nelt_rank
      integer(kind=IXP), intent(out) :: nelt_floor
      integer(kind=IXP), intent(out) :: ielt_offset

      integer(kind=IXP)             :: nelt_mod
      integer(kind=IXP)             :: ielt_end

      nelt_floor = floor(real(nelt_all,kind=RXP)/real(nrank,kind=RXP),kind=IXP)
      nelt_mod   = mod(nelt_all,nrank)

      ielt_offset = (rank        )*nelt_floor + ONE_IXP
      ielt_end    = (rank+ONE_IXP)*nelt_floor

      if (rank+ONE_IXP <= nelt_mod) then

         ielt_offset = (rank        )*nelt_floor + ONE_IXP + rank
         ielt_end    = (rank+ONE_IXP)*nelt_floor + ONE_IXP + rank
         nelt_rank   = ielt_end-ielt_offset+ ONE_IXP

      else

         ielt_offset = nelt_mod*(nelt_floor+ONE_IXP) + (rank-nelt_mod        )*nelt_floor + ONE_IXP
         ielt_end    = nelt_mod*(nelt_floor+ONE_IXP) + (rank-nelt_mod+ONE_IXP)*nelt_floor
         nelt_rank   = ielt_end-ielt_offset+ONE_IXP

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine get_file_rank_elt_offset
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine that returns the shape of the array stored in the file 'fname'
!>@param  fname    : filename
!>@param  comm     : mpi communicator
!>@param is_lustre : file system info
!>@param  r        : rank  of the array stored in file 'fname'
!>@return s        : shape of the array stored in file 'fname'
!***********************************************************************************************************************************************************************************
   subroutine get_file_shape(fname,comm,is_lustre,r,s)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_init_memory, only : init_array_int

      implicit none

      character(len=*)                            , intent( in) :: fname
      integer(kind=IXP)                           , intent( in) :: comm
      logical(kind=IXP)                           , intent( in) :: is_lustre
      integer(kind=IXP)                           , intent( in) :: r
      integer(kind=IXP), dimension(:), allocatable, intent(out) :: s

      integer(kind=IXP), dimension(MPI_STATUS_SIZE)             :: statut
      integer(kind=IXP)                                         :: funit
      integer(kind=IXP)                                         :: ios
      integer(kind=IXP)                                         :: ier
      character(len=CIL)                                        :: info

      ier = ZERO_IXP

      ios = init_array_int(s,r,"mod_io_array:get_file_shape:s")
      ier = ier + ios

      call efi_mpi_file_open(comm,fname,MPI_MODE_RDONLY,is_lustre,funit)
     
      call mpi_file_read_all(funit,s,r,MPI_INTEGER,statut,ios)
      ier = ier + ios

      call mpi_file_close(funit,ios)
      ier = ier + ios

      if (ier /= ZERO_IXP) then

         write(info,'(a)') "error in subroutine mod_io_array:get_file_shape while opening file "//trim(fname)

         call error_stop(info)

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine get_file_shape
!***********************************************************************************************************************************************************************************


!
!
!>@brief this subroutine opens a lustre or standard file 
!>@param comm        : mpi communicator
!>@param fname       : file name
!>@param mode        : mpi mode for opening file
!>@param is_lustre   : file system info
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_file_open(comm,fname,mode,is_lustre,funit)
!***********************************************************************************************************************************************************************************

      use mpi

      implicit none

      integer(kind=IXP) , intent(in)  :: comm
      character(len=*)  , intent(in)  :: fname
      integer(kind=IXP) , intent(in)  :: mode
      logical(kind=IXP) , intent(in)  :: is_lustre
      integer(kind=IXP) , intent(out) :: funit

      integer(kind=IXP)               :: ier
      integer(kind=IXP)               :: ios
      integer(kind=IXP)               :: finfo

      character(len=CIL)              :: info

      ier = ZERO_IXP

      if (is_lustre) then

         call mpi_info_create(finfo,ios)
         ier = ier + ios

         call mpi_info_set(finfo,"striping_factor","32",ios)
         ier = ier + ios

         call mpi_info_set(finfo,"striping_unit","1048576",ios)
         ier = ier + ios

         call mpi_file_open(comm,fname,mode,finfo,funit,ios)
         ier = ier + ios

      else

         call mpi_file_open(comm,fname,mode,MPI_INFO_NULL,funit,ios)
         ier = ier + ios

      endif

      if (ier /= ZERO_IXP) then

         write(info,'(a)') "error in subroutine efi_mpi_file_open while opening file "//trim(fname)

         call error_stop(info)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_file_open
!***********************************************************************************************************************************************************************************


!
!
!>@brief this subroutine writes to disk using mpi_file_write_at_all an array of rank 2 whose last dimension is the number of elements of myrank
!>@param comm        : mpi communicator
!>@param t           : array to be written
!>@param fname       : file name
!>@param rank        : rank of process
!>@param rank_offset : offset of the rank in terms of elements
!>@param byte_unit   : unit in byte of the element of the block
!>@param is_lustre   : file system info
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_file_write_at_all_rank2(comm,t,fname,rank,rank_offset,byte_unit,is_lustre)
!***********************************************************************************************************************************************************************************

      use mpi

      implicit none

      integer(kind=IXP), parameter                  :: T_RANK = TWO_IXP

      integer(kind=IXP), intent(in)                 :: comm
      real   (kind=RXP), intent(in), dimension(:,:) :: t
      character(len=*) , intent(in)                 :: fname
      integer(kind=IXP), intent(in)                 :: rank
      integer(kind=IXP), intent(in)                 :: rank_offset
      integer(kind=IXP), intent(in)                 :: byte_unit
      logical(kind=IXP), intent(in)                 :: is_lustre

      integer(kind=IXP)                             :: i
      integer(kind=IXP)                             :: ier
      integer(kind=IXP)                             :: ios
      integer(kind=IXP)                             :: funit
      integer(kind=IXP)                             :: nelt_all_cpu
      integer(kind=IXP)                             :: size_block
      integer(kind=IXP)                             :: size_total
      integer(kind=IXP), dimension(T_RANK)          :: profil
      integer(kind=IXP), dimension(MPI_STATUS_SIZE) :: statut

      integer(kind=I64)                             :: offset_int64
      integer(kind=I64)                             :: size_profil_int64
      integer(kind=I64)                             :: byte_header_int64
      integer(kind=I64)                             :: size_block_int64
      integer(kind=I64)                             :: rank_offset_int64
      integer(kind=I64)                             :: byte_unit_int64

      character(len=CIL)                            :: info


      ier = ZERO_IXP

      call efi_mpi_file_open(comm,fname,MPI_MODE_WRONLY + MPI_MODE_CREATE,is_lustre,funit)

!
!---->collective write by all cpus. cpu 0 of the communicator writes the header. the size of the last dimension is modified by its sum over all ranks

      do i = ONE_IXP,T_RANK

         profil(i) = size(t,i)

      enddo

      size_block = ONE_IXP

      do i = ONE_IXP,T_RANK-ONE_IXP

         size_block = size_block*profil(i)

      enddo

      size_total = ONE_IXP

      do i = ONE_IXP,T_RANK

         size_total = size_total*profil(i)

      enddo

      call mpi_reduce(profil(T_RANK),nelt_all_cpu,ONE_IXP,MPI_INTEGER,MPI_SUM,ZERO_IXP,comm,ios)

      if (rank == ZERO_IXP) then

         offset_int64 = ZERO_I64

         profil(T_RANK) = nelt_all_cpu

         call mpi_file_write_at(funit,offset_int64,profil,size(profil),MPI_INTEGER,statut,ios)

         ier = ier + ios

      endif

      size_profil_int64 = int(size(profil),kind=I64)

      byte_header_int64 =  size_profil_int64 * int(sizeof(profil(ONE_IXP)),kind=I64)

      size_block_int64  = int(size_block,kind=I64)

      rank_offset_int64 = int(rank_offset,kind=I64)

      byte_unit_int64   = int(byte_unit,kind=I64)

      offset_int64      = byte_header_int64 + (rank_offset_int64 - ONE_I64)*size_block_int64*byte_unit_int64

      call mpi_file_write_at_all(funit,offset_int64,t,size_total,MPI_REAL,statut,ios)

      ier = ier + ios

!
!---->close file

      call mpi_file_close(funit,ios)

      ier = ier + ios

      if (ier /= ZERO_IXP) then

         write(info,'(a)') "error in subroutine efi_mpi_file_write_at_all_rank2"

         call error_stop(info)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_file_write_at_all_rank2
!***********************************************************************************************************************************************************************************


!
!
!>@brief this subroutine writes to disk using mpi_file_write_at_all an array of rank 3 whose last dimension is the number of elements of myrank
!>@param comm        : mpi communicator
!>@param t           : array to be written
!>@param fname       : file name
!>@param rank        : rank of process
!>@param rank_offset : offset of the rank in terms of elements
!>@param byte_unit   : unit in byte of the element of the block
!>@param is_lustre   : file system info
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_file_write_at_all_rank3(comm,t,fname,rank,rank_offset,byte_unit,is_lustre)
!***********************************************************************************************************************************************************************************

      use mpi

      implicit none

      integer(kind=IXP), parameter                    :: T_RANK = THREE_IXP

      integer(kind=IXP), intent(in)                   :: comm
      real   (kind=RXP), intent(in), dimension(:,:,:) :: t
      character(len=*) , intent(in)                   :: fname
      integer(kind=IXP), intent(in)                   :: rank
      integer(kind=IXP), intent(in)                   :: rank_offset
      integer(kind=IXP), intent(in)                   :: byte_unit
      logical(kind=IXP), intent(in)                   :: is_lustre

      integer(kind=IXP)                               :: i
      integer(kind=IXP)                               :: ier
      integer(kind=IXP)                               :: ios
      integer(kind=IXP)                               :: funit
      integer(kind=IXP)                               :: nelt_all_cpu
      integer(kind=IXP)                               :: size_block
      integer(kind=IXP)                               :: size_total
      integer(kind=IXP), dimension(T_RANK)            :: profil
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)   :: statut

      integer(kind=I64)                               :: offset_int64
      integer(kind=I64)                               :: size_profil_int64
      integer(kind=I64)                               :: byte_header_int64
      integer(kind=I64)                               :: size_block_int64
      integer(kind=I64)                               :: rank_offset_int64
      integer(kind=I64)                               :: byte_unit_int64

      character(len=CIL)                              :: info


      ier = ZERO_IXP

      call efi_mpi_file_open(comm,fname,MPI_MODE_WRONLY + MPI_MODE_CREATE,is_lustre,funit)

!
!---->collective write by all cpus. cpu 0 of the communicator writes the header. the size of the last dimension is modified by its sum over all ranks

      do i = ONE_IXP,T_RANK

         profil(i) = size(t,i)

      enddo

      size_block = ONE_IXP

      do i = ONE_IXP,T_RANK-ONE_IXP

         size_block = size_block*profil(i)

      enddo

      size_total = ONE_IXP

      do i = ONE_IXP,T_RANK

         size_total = size_total*profil(i)

      enddo

      call mpi_reduce(profil(T_RANK),nelt_all_cpu,ONE_IXP,MPI_INTEGER,MPI_SUM,ZERO_IXP,comm,ios)

      offset_int64 = ZERO_I64

      if (rank == ZERO_IXP) then

         profil(T_RANK) = nelt_all_cpu

         call mpi_file_write_at(funit,offset_int64,profil,size(profil),MPI_INTEGER,statut,ios)

         ier = ier + ios

      endif

      size_profil_int64 = int(size(profil),kind=I64)

      byte_header_int64 =  size_profil_int64 * int(sizeof(profil(ONE_IXP)),kind=I64)

      size_block_int64  = int(size_block,kind=I64)

      rank_offset_int64 = int(rank_offset,kind=I64)

      byte_unit_int64   = int(byte_unit,kind=I64)

      offset_int64      = byte_header_int64 + (rank_offset_int64 - ONE_I64)*size_block_int64*byte_unit_int64

      call mpi_file_write_at_all(funit,offset_int64,t,size_total,MPI_REAL,statut,ios)
      ier = ier + ios

!
!---->close file

      call mpi_file_close(funit,ios)

      ier = ier + ios

      if (ier /= ZERO_IXP) then

         write(info,'(a)') "error in subroutine efi_mpi_file_write_at_all_rank3"

         call error_stop(info)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_file_write_at_all_rank3
!***********************************************************************************************************************************************************************************



!
!
!>@brief this subroutine writes an array of elements of rank 4 to disk using mpi_file_write_at_all
!>@param comm        : mpi communicator
!>@param t           : array to be written
!>@param fname       : file name
!>@param rank        : rank of process
!>@param rank_offset : offset of the rank in terms of elements
!>@param byte_unit   : unit in byte of the element of the block
!>@param is_lustre   : file system info
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_file_write_at_all_rank4(comm,t,fname,rank,rank_offset,byte_unit,is_lustre)
!***********************************************************************************************************************************************************************************

      use mpi

      implicit none

      integer(kind=IXP), parameter                      :: T_RANK = FOUR_IXP

      integer(kind=IXP), intent( in)                    :: comm
      real   (kind=RXP), intent(in), dimension(:,:,:,:) :: t
      character(len=*) , intent(in)                     :: fname
      integer(kind=IXP), intent(in)                     :: rank
      integer(kind=IXP), intent(in)                     :: rank_offset
      integer(kind=IXP), intent(in)                     :: byte_unit
      logical(kind=IXP), intent(in)                     :: is_lustre

      integer(kind=IXP)                                 :: i
      integer(kind=IXP)                                 :: ier
      integer(kind=IXP)                                 :: ios
      integer(kind=IXP)                                 :: funit
      integer(kind=IXP)                                 :: nelt_all_cpu
      integer(kind=IXP)                                 :: size_block
      integer(kind=IXP)                                 :: size_total
      integer(kind=IXP), dimension(T_RANK)              :: profil
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)     :: statut

      integer(kind=I64)                                 :: offset_int64
      integer(kind=I64)                                 :: size_profil_int64
      integer(kind=I64)                                 :: byte_header_int64
      integer(kind=I64)                                 :: size_block_int64
      integer(kind=I64)                                 :: rank_offset_int64
      integer(kind=I64)                                 :: byte_unit_int64

      character(len=CIL)                                :: info


      ier = ZERO_IXP

      call efi_mpi_file_open(comm,fname,MPI_MODE_WRONLY + MPI_MODE_CREATE,is_lustre,funit)

!
!---->collective write by all cpus. cpu 0 of world communicator writes the header

      do i = ONE_IXP,T_RANK

         profil(i) = size(t,i)

      enddo

      size_block = ONE_IXP

      do i = ONE_IXP,T_RANK-ONE_IXP

         size_block = size_block*profil(i)

      enddo

      size_total = ONE_IXP

      do i = ONE_IXP,T_RANK

         size_total = size_total*profil(i)

      enddo

      call mpi_reduce(profil(T_RANK),nelt_all_cpu,ONE_IXP,MPI_INTEGER,MPI_SUM,ZERO_IXP,comm,ios)

      if (rank == ZERO_IXP) then

         offset_int64 = ZERO_I64
         
         profil(T_RANK) = nelt_all_cpu

         call mpi_file_write_at(funit,offset_int64,profil,size(profil),MPI_INTEGER,statut,ios)

         ier = ier + ios

      endif

      size_profil_int64 = int(size(profil),kind=I64)

      byte_header_int64 =  size_profil_int64 * int(sizeof(profil(ONE_IXP)),kind=I64)

      size_block_int64  = int(size_block,kind=I64)

      rank_offset_int64 = int(rank_offset,kind=I64)

      byte_unit_int64   = int(byte_unit,kind=I64)

      offset_int64      = byte_header_int64 + (rank_offset_int64 - ONE_I64)*size_block_int64*byte_unit_int64

      call mpi_file_write_at_all(funit,offset_int64,t,size_total,MPI_REAL,statut,ios)

      ier = ier + ios

!
!---->close file

      call mpi_file_close(funit,ios)

      ier = ier + ios

      if (ier /= ZERO_IXP) then

         write(info,'(a)') "error in subroutine efi_mpi_file_write_at_all_rank4"

         call error_stop(info)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_file_write_at_all_rank4
!***********************************************************************************************************************************************************************************


!
!
!>@brief this subroutine writes an array of elements of rank 5 to disk using mpi_file_write_at_all
!>@param comm        : mpi communicator
!>@param t           : array to be written
!>@param fname       : file name
!>@param rank        : rank of process
!>@param rank_offset : offset of the rank in terms of elements
!>@param byte_unit   : unit in byte of the element of the block
!>@param is_lustre   : file system info
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_file_write_at_all_rank5(comm,t,fname,rank,rank_offset,byte_unit,is_lustre)
!***********************************************************************************************************************************************************************************

      use mpi

      implicit none

      integer(kind=IXP), parameter                        :: T_RANK = FIVE_IXP

      integer(kind=IXP), intent( in)                      :: comm
      real   (kind=RXP), intent(in), dimension(:,:,:,:,:) :: t
      character(len=*) , intent(in)                       :: fname
      integer(kind=IXP), intent(in)                       :: rank
      integer(kind=IXP), intent(in)                       :: rank_offset
      integer(kind=IXP), intent(in)                       :: byte_unit
      logical(kind=IXP), intent(in)                       :: is_lustre
                                                        
      integer(kind=IXP)                                   :: i
      integer(kind=IXP)                                   :: ier
      integer(kind=IXP)                                   :: ios
      integer(kind=IXP)                                   :: funit
      integer(kind=IXP)                                   :: nelt_all_cpu
      integer(kind=IXP)                                   :: size_block
      integer(kind=IXP)                                   :: size_total
      integer(kind=IXP), dimension(T_RANK)                :: profil
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)       :: statut
                                                        
      integer(kind=I64)                                   :: offset_int64
      integer(kind=I64)                                   :: size_profil_int64
      integer(kind=I64)                                   :: byte_header_int64
      integer(kind=I64)                                   :: size_block_int64
      integer(kind=I64)                                   :: rank_offset_int64
      integer(kind=I64)                                   :: byte_unit_int64
                                                           
      character(len=CIL)                                  :: info


      ier = ZERO_IXP

      call efi_mpi_file_open(comm,fname,MPI_MODE_WRONLY + MPI_MODE_CREATE,is_lustre,funit)

!
!---->collective write by all cpus. cpu 0 of world communicator writes the header

      do i = ONE_IXP,T_RANK

         profil(i) = size(t,i)

      enddo

      size_block = ONE_IXP

      do i = ONE_IXP,T_RANK-ONE_IXP

         size_block = size_block*profil(i)

      enddo

      size_total = ONE_IXP

      do i = ONE_IXP,T_RANK

         size_total = size_total*profil(i)

      enddo

      call mpi_reduce(profil(T_RANK),nelt_all_cpu,ONE_IXP,MPI_INTEGER,MPI_SUM,ZERO_IXP,comm,ios)

      if (rank == ZERO_IXP) then

         offset_int64 = ZERO_I64
         
         profil(T_RANK) = nelt_all_cpu

         call mpi_file_write_at(funit,offset_int64,profil,size(profil),MPI_INTEGER,statut,ios)

         ier = ier + ios

      endif

      size_profil_int64 = int(size(profil),kind=I64)

      byte_header_int64 =  size_profil_int64 * int(sizeof(profil(ONE_IXP)),kind=I64)

      size_block_int64  = int(size_block,kind=I64)

      rank_offset_int64 = int(rank_offset,kind=I64)

      byte_unit_int64   = int(byte_unit,kind=I64)

      offset_int64      = byte_header_int64 + (rank_offset_int64 - ONE_I64)*size_block_int64*byte_unit_int64

      call mpi_file_write_at_all(funit,offset_int64,t,size_total,MPI_REAL,statut,ios)

      ier = ier + ios

!
!---->close file

      call mpi_file_close(funit,ios)

      ier = ier + ios

      if (ier /= ZERO_IXP) then

         write(info,'(a)') "error in subroutine efi_mpi_file_write_at_all_rank5"

         call error_stop(info)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_file_write_at_all_rank5
!***********************************************************************************************************************************************************************************


!
!
!>@brief this subroutine writes a matrix reshaped into an array of elements of rank 1 using global offset 
!>@param comm        : mpi communicator
!>@param t           : array to be written
!>@param fname       : file name
!>@param o           : offset where to write
!>@param n1          : size of the matrix in the 1st dimension
!>@param n2          : size of the matrix in the 2nd dimension
!>@param d1          : discrete step in the 1st dimension
!>@param d2          : discrete step in the 2nd dimension
!>@param byte_unit   : unit in byte of the element of the block
!>@param is_lustre   : file system info
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_file_write_at_all_fault(comm,t,fname,o,n1,n2,d1,d2,byte_unit,is_lustre)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_efi_mpi, only : efi_mpi_comm_split

      implicit none

      integer(kind=IXP)                       , intent(in) :: comm
      real   (kind=RXP), dimension(:), pointer, intent(in) :: t
      character(len=*)                        , intent(in) :: fname
      integer(kind=IXP), dimension(:), pointer, intent(in) :: o
      integer(kind=IXP)                       , intent(in) :: n1
      integer(kind=IXP)                       , intent(in) :: n2
      real   (kind=RXP)                       , intent(in) :: d1
      real   (kind=RXP)                       , intent(in) :: d2
      integer(kind=IXP)                       , intent(in) :: byte_unit
      logical(kind=IXP)                       , intent(in) :: is_lustre
                                                        
      integer(kind=IXP)                                    :: myrank_write
      integer(kind=IXP)                                    :: comm_write
      integer(kind=IXP)                                    :: i
      integer(kind=IXP)                                    :: n
      integer(kind=IXP)                                    :: ier
      integer(kind=IXP)                                    :: ios
      integer(kind=IXP)                                    :: funit
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)        :: statut
                                                        
      integer(kind=MPI_OFFSET_KIND)                        :: offset_int64
      integer(kind=MPI_OFFSET_KIND)                        :: byte_header_int64
      integer(kind=MPI_OFFSET_KIND)                        :: byte_unit_int64
                                                            
      character(len=CIL)                                   :: info

      n = size(t)

      call efi_mpi_comm_split(n,10_IXP,comm,comm_write)

      if (comm_write /= MPI_COMM_NULL) then

         call mpi_comm_rank(comm_write,myrank_write,ios)

         ier = ZERO_IXP
      
         call efi_mpi_file_open(comm_write,fname,MPI_MODE_WRONLY + MPI_MODE_CREATE,is_lustre,funit)
      
!     
!------->cpu0 write the header
     
         if (myrank_write == ZERO_IXP) then
     
            offset_int64 = int(ZERO_IXP,kind=MPI_OFFSET_KIND)
            
            call mpi_file_write_at(funit,offset_int64,n1,ONE_IXP,MPI_INTEGER,statut,ios)
     
            ier = ier + ios
     
            offset_int64 = offset_int64 + int(sizeof(n1),kind=MPI_OFFSET_KIND)
     
            call mpi_file_write_at(funit,offset_int64,n2,ONE_IXP,MPI_INTEGER,statut,ios)
     
            ier = ier + ios
     
            offset_int64 = offset_int64 + int(sizeof(n2),kind=MPI_OFFSET_KIND)
     
            call mpi_file_write_at(funit,offset_int64,d1,ONE_IXP,MPI_REAL,statut,ios)
     
            ier = ier + ios
     
            offset_int64 = offset_int64 + int(sizeof(d1),kind=MPI_OFFSET_KIND)
     
            call mpi_file_write_at(funit,offset_int64,d2,ONE_IXP,MPI_REAL,statut,ios)
     
            ier = ier + ios
     
            offset_int64 = offset_int64 + int(sizeof(d2),kind=MPI_OFFSET_KIND)
     
         endif
     
! 
!------->parallel write (TODO: do loop could be remove using specific file view)
!        mpi_file_write_at_all cannot be used because same number of values must be written by all cpus

         call mpi_bcast(offset_int64,ONE_IXP,MPI_INTEGER,ZERO_IXP,comm_write,ios)

         byte_header_int64 = offset_int64

         byte_unit_int64 = int(byte_unit,kind=MPI_OFFSET_KIND)
     
         do i = ONE_IXP,n

            offset_int64 = byte_header_int64 + int(o(i)-ONE_IXP,kind=MPI_OFFSET_KIND)*byte_unit_int64
         
            call mpi_file_write_at(funit,offset_int64,t(i),ONE_IXP,MPI_REAL,statut,ios)

            ier = ier + ios
         
         enddo

! 
!------->close file
     
         call mpi_file_close(funit,ios)
     
         ier = ier + ios
     
         if (ier /= ZERO_IXP) then
     
            write(info,'(a)') "error in subroutine efi_mpi_file_write_at_all_fault"
     
            call error_stop(info)
     
         endif

         call mpi_comm_free(comm_write,ios)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_file_write_at_all_fault
!***********************************************************************************************************************************************************************************


!
!
!>@brief this subroutine reads from disk using mpi_file_read_at_all an array of rank 3 whose last dimension is the number of elements of all ranks
!>@param  comm        : mpi communicator
!>@param  fname       : file name
!>@param  rank        : rank of processus
!>@param  nrank       : total number of rank
!>@param  byte_unit   : unit in byte of the element of the block
!>@param  is_lustre   : file system info
!>@return t           : array to be read
!>@return nelt_all_cpu: total number of elements over all ranks
!>@return nelt_rank   : total number of elements over current rank
!>@return nelt_floor  : floor value of nelt_all/nrank
!>@return rank_offset : offset in terms of element
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_file_read_at_all_rank3(comm,fname,rank,nrank,byte_unit,is_lustre,t,nelt_all_cpu,nelt_rank,nelt_floor,rank_offset)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_init_memory     , only : init_array_real

      implicit none

      integer(kind=IXP), parameter                                  :: T_RANK = 3
                                                                  
      integer(kind=IXP), intent( in)                                :: comm
      character(len=*) , intent( in)                                :: fname
      integer(kind=IXP), intent( in)                                :: rank
      integer(kind=IXP), intent( in)                                :: nrank
      integer(kind=IXP), intent( in)                                :: byte_unit
      logical(kind=IXP), intent( in)                                :: is_lustre
      real   (kind=RXP), intent(out), allocatable, dimension(:,:,:) :: t
      integer(kind=IXP), intent(out)                                :: nelt_all_cpu
      integer(kind=IXP), intent(out)                                :: nelt_rank
      integer(kind=IXP), intent(out)                                :: nelt_floor
      integer(kind=IXP), intent(out)                                :: rank_offset
      
                                                                  
      integer(kind=IXP)                                             :: i
      integer(kind=IXP)                                             :: ier
      integer(kind=IXP)                                             :: ios
      integer(kind=IXP)                                             :: funit
      integer(kind=IXP)                                             :: size_block
      integer(kind=IXP)                                             :: size_total
      integer(kind=IXP), dimension(T_RANK)                          :: profil
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)                 :: statut
                                                                   
      integer(kind=I64)                                             :: offset_int64
      integer(kind=I64)                                             :: size_profil_int64
      integer(kind=I64)                                             :: byte_header_int64
      integer(kind=I64)                                             :: size_block_int64
      integer(kind=I64)                                             :: rank_offset_int64
      integer(kind=I64)                                             :: byte_unit_int64
                                                                      
      character(len=CIL)                                            :: info

      ier = ZERO_IXP

      call efi_mpi_file_open(comm,fname,MPI_MODE_RDONLY,is_lustre,funit)

!
!---->all cpus read the total number of quadrangle elements stored in file

      call mpi_file_read_all(funit,profil,T_RANK,MPI_INTEGER,statut,ios)

      nelt_all_cpu = profil(T_RANK)

      call get_file_rank_elt_offset(rank,nrank,nelt_all_cpu,nelt_rank,nelt_floor,rank_offset)

      profil(T_RANK) = nelt_rank

      ios = init_array_real(t,profil(THREE_IXP),profil(TWO_IXP),profil(ONE_IXP),"efi_mpi_file_read_at_all_rank3:t")

      size_profil_int64 = int(size(profil),kind=I64)

      byte_header_int64 =  size_profil_int64 * int(sizeof(profil(ONE_IXP)),kind=I64)

!
!---->compute size of block before last dimension

      size_block = ONE_IXP

      do i = ONE_IXP,T_RANK-ONE_IXP

         size_block = size_block*profil(i)

      enddo

      size_total = ONE_IXP

      do i = ONE_IXP,T_RANK

         size_total = size_total*profil(i)

      enddo

      size_block_int64  = int(size_block,kind=I64)

      rank_offset_int64 = int(rank_offset,kind=I64)

      byte_unit_int64   = int(byte_unit,kind=I64)

      offset_int64      = byte_header_int64 + (rank_offset_int64 - ONE_I64)*size_block_int64*byte_unit_int64

      call mpi_file_read_at_all(funit,offset_int64,t,size_total,MPI_REAL,statut,ios)

      ier = ier + ios

!
!---->close file

      call mpi_file_close(funit,ios)

      ier = ier + ios

      if (ier /= ZERO_IXP) then

         write(info,'(a)') "error in subroutine efi_mpi_file_read_at_all_rank3"

         call error_stop(info)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_file_read_at_all_rank3
!***********************************************************************************************************************************************************************************


!
!
!>@brief this subroutine reads from disk using mpi_file_read_at_all an array of rank 3 of integer whose last dimension is the number of elements of all ranks
!>@param  comm        : mpi communicator
!>@param  fname       : file name
!>@param  rank        : rank of processus
!>@param  nrank       : total number of rank
!>@param  byte_unit   : unit in byte of the element of the block
!>@param  is_lustre   : file system info
!>@return t           : array of integer(kind=IXP) to be read
!>@return nelt_all_cpu: total number of elements over all ranks
!>@return nelt_rank   : total number of elements over current rank
!>@return nelt_floor  : floor value of nelt_all/nrank
!>@return rank_offset : offset in terms of element
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_file_read_at_all_rank3_ixp(comm,fname,rank,nrank,byte_unit,is_lustre,t,nelt_all_cpu,nelt_rank,nelt_floor,rank_offset)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_init_memory     , only : init_array_int

      implicit none

      integer(kind=IXP)        , parameter                                  :: T_RANK = 3
                                                                  
      integer(kind=IXP)        , intent( in)                                :: comm
      character(len=*)         , intent( in)                                :: fname
      integer(kind=IXP)        , intent( in)                                :: rank
      integer(kind=IXP)        , intent( in)                                :: nrank
      integer(kind=IXP)        , intent( in)                                :: byte_unit
      logical(kind=IXP)        , intent( in)                                :: is_lustre
      integer(kind=IXP)        , intent(out), allocatable, dimension(:,:,:) :: t
      integer(kind=IXP)        , intent(out)                                :: nelt_all_cpu
      integer(kind=IXP)        , intent(out)                                :: nelt_rank
      integer(kind=IXP)        , intent(out)                                :: nelt_floor
      integer(kind=IXP)        , intent(out)                                :: rank_offset
      
                                                                  
      integer(kind=IXP)                                                     :: i
      integer(kind=IXP)                                                     :: ier
      integer(kind=IXP)                                                     :: ios
      integer(kind=IXP)                                                     :: funit
      integer(kind=IXP)                                                     :: size_block
      integer(kind=IXP)                                                     :: size_total
      integer(kind=IXP), dimension(T_RANK)                                   :: profil
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)                          :: statut
                                                                   
      integer(kind=I64)                                          :: offset_int64
      integer(kind=I64)                                          :: size_profil_int64
      integer(kind=I64)                                          :: byte_header_int64
      integer(kind=I64)                                          :: size_block_int64
      integer(kind=I64)                                          :: rank_offset_int64
      integer(kind=I64)                                          :: byte_unit_int64
                                                                   
      character(len=CIL)                                           :: info

      ier = ZERO_IXP

      call efi_mpi_file_open(comm,fname,MPI_MODE_RDONLY,is_lustre,funit)

!
!---->all cpus read the total number of quadrangle elements stored in file

      call mpi_file_read_all(funit,profil,T_RANK,MPI_INTEGER,statut,ios)

      nelt_all_cpu = profil(T_RANK)

      call get_file_rank_elt_offset(rank,nrank,nelt_all_cpu,nelt_rank,nelt_floor,rank_offset)

      profil(T_RANK) = nelt_rank

      ios = init_array_int(t,profil(THREE_IXP),profil(TWO_IXP),profil(ONE_IXP),"efi_mpi_file_read_at_all_rank3_ixp:t")

      size_profil_int64 = int(size(profil),kind=I64)

      byte_header_int64 =  size_profil_int64 * int(sizeof(profil(ONE_IXP)),kind=I64)

!
!---->compute size of block before last dimension

      size_block = ONE_IXP

      do i = ONE_IXP,T_RANK-ONE_IXP

         size_block = size_block*profil(i)

      enddo

      size_total = ONE_IXP

      do i = ONE_IXP,T_RANK

         size_total = size_total*profil(i)

      enddo

      size_block_int64  = int(size_block,kind=I64)

      rank_offset_int64 = int(rank_offset,kind=I64)

      byte_unit_int64   = int(byte_unit,kind=I64)

      offset_int64      = byte_header_int64 + (rank_offset_int64 - ONE_I64)*size_block_int64*byte_unit_int64

      call mpi_file_read_at_all(funit,offset_int64,t,size_total,MPI_INTEGER,statut,ios)

      ier = ier + ios

!
!---->close file

      call mpi_file_close(funit,ios)

      ier = ier + ios

      if (ier /= ZERO_IXP) then

         write(info,'(a)') "error in subroutine efi_mpi_file_read_at_all_rank3_ixp"

         call error_stop(info)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_file_read_at_all_rank3_ixp
!***********************************************************************************************************************************************************************************


!
!
!>@brief this subroutine reads from disk using mpi_file_read_at_all an array of rank 4 whose last dimension is the number of elements of all ranks
!>@param  comm        : mpi communicator
!>@param  fname       : file name
!>@param  rank        : rank of processus
!>@param  nrank       : total number of rank
!>@param  byte_unit   : unit in byte of the element of the block
!>@param  is_lustre   : file system info
!>@return t           : array to be read
!>@return nelt_all_cpu: total number of elements over all ranks
!>@return nelt_rank   : total number of elements over current rank
!>@return nelt_floor  : floor value of nelt_all/nrank
!>@return rank_offset : offset in terms of element
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_file_read_at_all_rank4(comm,fname,rank,nrank,byte_unit,is_lustre,t,nelt_all_cpu,nelt_rank,nelt_floor,rank_offset)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_init_memory     , only : init_array_real

      implicit none

      integer(kind=IXP), parameter                                      :: T_RANK = 4
                                                                  
      integer(kind=IXP), intent( in)                                    :: comm
      character(len=*) , intent( in)                                    :: fname
      integer(kind=IXP), intent( in)                                    :: rank
      integer(kind=IXP), intent( in)                                    :: nrank
      integer(kind=IXP), intent( in)                                    :: byte_unit
      logical(kind=IXP), intent( in)                                    :: is_lustre
      real   (kind=RXP), intent(out), allocatable, dimension(:,:,:,:)   :: t
      integer(kind=IXP), intent(out)                                    :: nelt_all_cpu
      integer(kind=IXP), intent(out)                                    :: nelt_rank
      integer(kind=IXP), intent(out)                                    :: nelt_floor
      integer(kind=IXP), intent(out)                                    :: rank_offset
                                                                  
      integer(kind=IXP)                                                 :: i
      integer(kind=IXP)                                                 :: ier
      integer(kind=IXP)                                                 :: ios
      integer(kind=IXP)                                                 :: funit
      integer(kind=IXP)                                                 :: size_block
      integer(kind=IXP)                                                 :: size_total
      integer(kind=IXP), dimension(T_RANK)                              :: profil
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)                     :: statut
                                                                       
      integer(kind=I64)                                                 :: offset_int64
      integer(kind=I64)                                                 :: size_profil_int64
      integer(kind=I64)                                                 :: byte_header_int64
      integer(kind=I64)                                                 :: size_block_int64
      integer(kind=I64)                                                 :: rank_offset_int64
      integer(kind=I64)                                                 :: byte_unit_int64
                                                                          
      character(len=CIL)                                                :: info

      ier = ZERO_IXP

      call efi_mpi_file_open(comm,fname,MPI_MODE_RDONLY,is_lustre,funit)

!
!---->all cpus read the total number of quadrangle elements stored in file

      call mpi_file_read_all(funit,profil,T_RANK,MPI_INTEGER,statut,ios)

      nelt_all_cpu = profil(T_RANK)

      call get_file_rank_elt_offset(rank,nrank,nelt_all_cpu,nelt_rank,nelt_floor,rank_offset)

      profil(T_RANK) = nelt_rank

      ios = init_array_real(t,profil(FOUR_IXP),profil(THREE_IXP),profil(TWO_IXP),profil(ONE_IXP),"efi_mpi_file_read_at_all_rank4:t")

      size_profil_int64 = int(size(profil),kind=I64)

      byte_header_int64 =  size_profil_int64 * int(sizeof(profil(ONE_IXP)),kind=I64)

!
!---->compute size of block before last dimension

      size_block = ONE_IXP

      do i = ONE_IXP,T_RANK-ONE_IXP

         size_block = size_block*profil(i)

      enddo

      size_total = ONE_IXP

      do i = ONE_IXP,T_RANK

         size_total = size_total*profil(i)

      enddo

      size_block_int64  = int(size_block,kind=I64)

      rank_offset_int64 = int(rank_offset,kind=I64)

      byte_unit_int64   = int(byte_unit,kind=I64)

      offset_int64      = byte_header_int64 + (rank_offset_int64 - ONE_I64)*size_block_int64*byte_unit_int64

      call mpi_file_read_at_all(funit,offset_int64,t,size_total,MPI_REAL,statut,ios)

      ier = ier + ios

!
!---->close file

      call mpi_file_close(funit,ios)

      ier = ier + ios

      if (ier /= ZERO_IXP) then

         write(info,'(a)') "error in subroutine efi_mpi_file_read_at_all_rank4"

         call error_stop(info)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_file_read_at_all_rank4
!***********************************************************************************************************************************************************************************



!
!
!>@brief this subroutine reads from disk using mpi_file_read_at_all an array of rank 5 whose last dimension is the number of elements of all ranks
!>@param  comm        : mpi communicator
!>@param  fname       : file name
!>@param  rank        : rank of processus
!>@param  nrank       : total number of rank
!>@param  byte_unit   : unit in byte of the element of the block
!>@param  is_lustre   : file system info
!>@return t           : array to be read
!>@return nelt_all_cpu: total number of elements over all ranks
!>@return nelt_rank   : total number of elements over current rank
!>@return nelt_floor  : floor value of nelt_all/nrank
!>@return rank_offset : offset in terms of element
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_file_read_at_all_rank5(comm,fname,rank,nrank,byte_unit,is_lustre,t,nelt_all_cpu,nelt_rank,nelt_floor,rank_offset)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_init_memory     , only : init_array_real

      implicit none

      integer(kind=IXP), parameter                                      :: T_RANK = 5
                                                                  
      integer(kind=IXP), intent( in)                                    :: comm
      character(len=*) , intent( in)                                    :: fname
      integer(kind=IXP), intent( in)                                    :: rank
      integer(kind=IXP), intent( in)                                    :: nrank
      integer(kind=IXP), intent( in)                                    :: byte_unit
      logical(kind=IXP), intent( in)                                    :: is_lustre
      real   (kind=RXP), intent(out), allocatable, dimension(:,:,:,:,:) :: t
      integer(kind=IXP), intent(out)                                    :: nelt_all_cpu
      integer(kind=IXP), intent(out)                                    :: nelt_rank
      integer(kind=IXP), intent(out)                                    :: nelt_floor
      integer(kind=IXP), intent(out)                                    :: rank_offset
                                                                  
      integer(kind=IXP)                                                 :: i
      integer(kind=IXP)                                                 :: ier
      integer(kind=IXP)                                                 :: ios
      integer(kind=IXP)                                                 :: funit
      integer(kind=IXP)                                                 :: size_block
      integer(kind=I64)                                                 :: size_total
      integer(kind=IXP), dimension(T_RANK)                              :: profil
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)                     :: statut
                                                                       
      integer(kind=I64)                                                 :: offset_int64
      integer(kind=I64)                                                 :: size_profil_int64
      integer(kind=I64)                                                 :: byte_header_int64
      integer(kind=I64)                                                 :: size_block_int64
      integer(kind=I64)                                                 :: rank_offset_int64
      integer(kind=I64)                                                 :: byte_unit_int64
                                                                          
      character(len=CIL)                                                :: info

      ier = ZERO_IXP

      call efi_mpi_file_open(comm,fname,MPI_MODE_RDONLY,is_lustre,funit)

!
!---->all cpus read the total number of quadrangle elements stored in file

      call mpi_file_read_all(funit,profil,T_RANK,MPI_INTEGER,statut,ios)

      nelt_all_cpu = profil(T_RANK)

      call get_file_rank_elt_offset(rank,nrank,nelt_all_cpu,nelt_rank,nelt_floor,rank_offset)

      profil(T_RANK) = nelt_rank

      ios = init_array_real(t,profil(FIVE_IXP),profil(FOUR_IXP),profil(THREE_IXP),profil(TWO_IXP),profil(ONE_IXP),"efi_mpi_file_read_at_all_rank5:t")

      size_profil_int64 = int(size(profil),kind=I64)

      byte_header_int64 =  size_profil_int64 * int(sizeof(profil(ONE_IXP)),kind=I64)

!
!---->compute size of block before last dimension

      size_block = ONE_IXP

      do i = ONE_IXP,T_RANK-ONE_IXP

         size_block = size_block*profil(i)

      enddo

      size_total = ONE_I64

      do i = ONE_IXP,T_RANK

         size_total = size_total*int(profil(i),kind=I64)

      enddo

      size_block_int64  = int(size_block,kind=I64)

      rank_offset_int64 = int(rank_offset,kind=I64)

      byte_unit_int64   = int(byte_unit,kind=I64)

      offset_int64      = byte_header_int64 + (rank_offset_int64 - ONE_I64)*size_block_int64*byte_unit_int64

      call mpi_file_read_at_all(funit,offset_int64,t,size_total,MPI_REAL,statut,ios)

      ier = ier + ios

!
!---->close file

      call mpi_file_close(funit,ios)

      ier = ier + ios

      if (ier /= ZERO_IXP) then

         write(info,'(a)') "error in subroutine efi_mpi_file_read_at_all_rank5"

         call error_stop(info)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_file_read_at_all_rank5
!***********************************************************************************************************************************************************************************

end module mod_io_array
