!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to manage snapshots of a volume composed by hexahedron elements in VTK format.
!!Those VTK hexahedron elements are generated from spectral elements GLL nodes.

!>@brief
!!This module contains subroutines to compute and write snapshots of a mesh composed by hexahedron elements (VTK XML formats).
module mod_snapshot_volume

   use mod_precision

   use mod_global_variables, only : CIL

   implicit none

   private

   public  :: init_snapshot_volume
   private :: init_vtk_mesh
   private :: get_efi_hexa
   private :: init_vtk_node
   private :: init_vtk_node_numbering
   private :: init_vtk_hexa_connectivity
   public  :: write_snapshot_volume
   public  :: write_snapshot_volume_vtk
   public  :: write_collection_vtk_vol
   private :: write_medium
   private :: collection_vtk_volume

   contains

!
!
!>@brief
!!This subroutine intializes the VTK mesh used for volume snapshots by calling 'get_efi_hexa' and 'init_vtk_mesh'.
!***********************************************************************************************************************************************************************************
   subroutine init_snapshot_volume()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only:&
                                     ig_hexa_gll_glonum&
                                    ,ig_nhexa&
                                    ,ig_hexa_snapshot&
                                    ,rg_snapshot_volume_xmin&
                                    ,rg_snapshot_volume_xmax&
                                    ,rg_snapshot_volume_ymin&
                                    ,rg_snapshot_volume_ymax&
                                    ,rg_snapshot_volume_zmin&
                                    ,rg_snapshot_volume_zmax&
                                    ,ig_vtk_hexa_conn_snapshot&
                                    ,ig_vtk_node_gll_glonum_snapshot&
                                    ,rg_vtk_node_x_snapshot&
                                    ,rg_vtk_node_y_snapshot&
                                    ,rg_vtk_node_z_snapshot&
                                    ,ig_vtk_cell_type_snapshot&
                                    ,ig_vtk_offset_snapshot&
                                    ,ig_vtk_nhexa_snapshot&
                                    ,ig_vtk_nnode_snapshot

      use mod_init_memory

      use mod_vtk_io

      implicit none

      call get_efi_hexa(ig_hexa_snapshot        &
                       ,ig_nhexa                &
                       ,rg_snapshot_volume_xmin &
                       ,rg_snapshot_volume_xmax &
                       ,rg_snapshot_volume_ymin &
                       ,rg_snapshot_volume_ymax &
                       ,rg_snapshot_volume_zmin &
                       ,rg_snapshot_volume_zmax )

      call init_vtk_mesh(ig_hexa_snapshot                &
                        ,ig_hexa_gll_glonum              &
                        ,ig_vtk_hexa_conn_snapshot       &
                        ,rg_vtk_node_x_snapshot          &
                        ,rg_vtk_node_y_snapshot          &
                        ,rg_vtk_node_z_snapshot          &
                        ,ig_vtk_node_gll_glonum_snapshot &
                        ,ig_vtk_cell_type_snapshot       &
                        ,ig_vtk_offset_snapshot          &
                        ,ig_vtk_nhexa_snapshot           &
                        ,ig_vtk_nnode_snapshot           )

      deallocate(ig_hexa_snapshot)

      return

!***********************************************************************************************************************************************************************************
   end subroutine init_snapshot_volume
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine finds and stores the number of EFISPEC's hexahedron elements used for generating VTK mesh.
!>@return efi_hexa : EFISPEC's hexahedron elements used for generating VTK mesh
!>@param efi_nhexa : number of hexahedron elements in EFISPEC mesh
!>@param xmin      : minimal @f$x@f$-coordinate of the box of volume snapshot
!>@param xmax      : maximal @f$x@f$-coordinate of the box of volume snapshot
!>@param ymin      : minimal @f$y@f$-coordinate of the box of volume snapshot
!>@param ymax      : maximal @f$y@f$-coordinate of the box of volume snapshot
!>@param zmin      : minimal @f$z@f$-coordinate of the box of volume snapshot
!>@param zmax      : maximal @f$z@f$-coordinate of the box of volume snapshot
!***********************************************************************************************************************************************************************************
   subroutine get_efi_hexa(efi_hexa,efi_nhexa,xmin,xmax,ymin,ymax,zmin,zmax)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only : info_all_cpu

      use mod_coordinate      , only : compute_hexa_point_coord

      use mod_init_memory

      implicit none

      integer(kind=IXP), intent(out), dimension(:), allocatable :: efi_hexa
      integer(kind=IXP), intent( in)                            :: efi_nhexa
      real   (kind=RXP), intent( in)                            :: xmin
      real   (kind=RXP), intent( in)                            :: xmax
      real   (kind=RXP), intent( in)                            :: ymin
      real   (kind=RXP), intent( in)                            :: ymax
      real   (kind=RXP), intent( in)                            :: zmin
      real   (kind=RXP), intent( in)                            :: zmax

      real   (kind=RXP)                                         :: x
      real   (kind=RXP)                                         :: y
      real   (kind=RXP)                                         :: z
                                                      
      integer(kind=IXP)                                         :: ios
      integer(kind=IXP)                                         :: ihexa
      integer(kind=IXP)                                         :: nhexa

      logical(kind=IXP)             , dimension(efi_nhexa)      :: is_snapshot

      character(len=CIL)                                        :: info

!
!---->first pass to get the number of EFISPEC's hexahedron elements located inside the volume used for snapshot. The centroid of hexahedron elements is used as reference for their location.

      nhexa          = ZERO_IXP
      is_snapshot(:) = .false.

      do ihexa = ONE_IXP,efi_nhexa

         call compute_hexa_point_coord(ihexa,ZERO_RXP,ZERO_RXP,ZERO_RXP,x,y,z)

         if ( x >= xmin .and. x <= xmax .and.&
              y >= ymin .and. y <= ymax .and.&
              z >= zmin .and. z <= zmax ) then

            is_snapshot(ihexa) = .true.
            nhexa              = nhexa + ONE_IXP

         endif

      enddo

!
!---->second pass to store hexahedron elements

      ios   = init_array_int(efi_hexa,nhexa,"efi_hexa")

      nhexa = ZERO_IXP

      do ihexa = ONE_IXP,efi_nhexa

         if (is_snapshot(ihexa)) then

            nhexa = nhexa + ONE_IXP

            efi_hexa(nhexa) = ihexa

         endif

      enddo

!
!---->list nhexa of each cpu in *.lst file

      write(info,'(a)') "hexa for volume snapshot"

      call info_all_cpu(nhexa,info)

      return

!***********************************************************************************************************************************************************************************
   end subroutine get_efi_hexa
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutines manages the generation of a VTK mesh from spectral elements GLL nodes.
!>@param  efi_hexa            : EFISPEC hexahedron elements whose GLL nodes will be used to generate a VTK mesh composed by 8-node hexahedron elements
!>@param  efi_hexa_gll_glonum : EFISPEC indirection array from local to global GLL nodes numbering
!>@param  vtk_hexa_conn       : VTK hexahedron elements connectivity array
!>@param  vtk_node_gll_glonum : VTK indirection array from VTK's geometric nodes to EFISPEC's GLL nodes
!>@param  vtk_cell_type       : VTK cell type
!>@param  vtk_offset          : VTK offset array
!>@param  vtk_node_x          : @f$x@f$-coordinate of VTK mesh nodes
!>@param  vtk_node_y          : @f$y@f$-coordinate of VTK mesh nodes
!>@param  vtk_node_z          : @f$z@f$-coordinate of VTK mesh nodes
!>@param  vtk_nhexa           : number of hexahadron element in VTK mesh
!>@param  vtk_nnode           : number of geometric nodes in VTK mesh
!>@return vtk_hexa_conn       : VTK hexahedron elements connectivity array
!>@return vtk_node_gll_glonum : VTK indirection array from VTK's geometric nodes to EFISPEC's GLL nodes
!>@return vtk_cell_type       : VTK cell type
!>@return vtk_offset          : VTK offset array
!>@return vtk_node_x          : @f$x@f$-coordinate of VTK mesh nodes
!>@return vtk_node_y          : @f$y@f$-coordinate of VTK mesh nodes
!>@return vtk_node_z          : @f$z@f$-coordinate of VTK mesh nodes
!>@return vtk_nhexa           : number of hexahadron element in VTK mesh
!>@return vtk_nnode           : number of geometric nodes in VTK mesh
!***********************************************************************************************************************************************************************************
   subroutine init_vtk_mesh(efi_hexa,efi_hexa_gll_glonum,vtk_hexa_conn,vtk_node_x,vtk_node_y,vtk_node_z,vtk_node_gll_glonum,vtk_cell_type,vtk_offset,vtk_nhexa,vtk_nnode)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,rg_gll_coordinate&
                                     ,ig_ngll_total&
                                     ,ig_nhexa

      use mod_gll_value       , only :get_node_gll_value

      use mod_init_memory

      implicit none

      integer(kind=IXP)       , intent( in), dimension(:)                                :: efi_hexa
      integer(kind=IXP)       , intent( in), dimension(IG_NGLL,IG_NGLL,IG_NGLL,ig_nhexa) :: efi_hexa_gll_glonum
      integer(kind=IXP)       , intent(out), dimension(:), allocatable                   :: vtk_hexa_conn
      integer(kind=IXP)       , intent(out), dimension(:), allocatable                   :: vtk_node_gll_glonum
      integer(kind=I8 )       , intent(out), dimension(:), allocatable                   :: vtk_cell_type
      integer(kind=IXP)       , intent(out), dimension(:), allocatable                   :: vtk_offset
      real   (kind=RXP)       , intent(out), dimension(:), allocatable                   :: vtk_node_x
      real   (kind=RXP)       , intent(out), dimension(:), allocatable                   :: vtk_node_y
      real   (kind=RXP)       , intent(out), dimension(:), allocatable                   :: vtk_node_z
      integer(kind=IXP)       , intent(out)                                              :: vtk_nhexa
      integer(kind=IXP)       , intent(out)                                              :: vtk_nnode

      integer(kind=IXP)       , dimension(ig_ngll_total)                                 :: vtk_node
      integer(kind=IXP)                                                                  :: efi_nhexa
      integer(kind=IXP)                                                                  :: ios
      integer(kind=IXP)                                                                  :: ihexa

      efi_nhexa  = size(efi_hexa)

!
!
!*************************************************************
!---->find which GLL nodes must be used to split hexa by GLL
!*************************************************************

      call init_vtk_node(efi_hexa,efi_nhexa,efi_hexa_gll_glonum,vtk_node,ig_ngll_total,vtk_nnode)

!
!
!***********************************************************************
!---->renumber array vtk_node
!***********************************************************************

      call init_vtk_node_numbering(vtk_node,vtk_node_gll_glonum,vtk_nnode,ig_ngll_total)

!
!
!****************************************************
!---->create hexa connectivity array 'vtk_hexa_conn' 
!****************************************************

      call init_vtk_hexa_connectivity(efi_hexa,efi_nhexa,vtk_node,ig_ngll_total,efi_hexa_gll_glonum,vtk_hexa_conn,vtk_nhexa)

!
!
!***********************************************
!---->create hexa coordinates
!***********************************************

      ios = init_array_real(vtk_node_x,vtk_nnode,"vtk_node_x")
      ios = init_array_real(vtk_node_y,vtk_nnode,"vtk_node_y")
      ios = init_array_real(vtk_node_z,vtk_nnode,"vtk_node_z")

      call get_node_gll_value(rg_gll_coordinate(ONE_IXP,:),vtk_node_gll_glonum,vtk_node_x)
      call get_node_gll_value(rg_gll_coordinate(TWO_IXP,:),vtk_node_gll_glonum,vtk_node_y)
      call get_node_gll_value(rg_gll_coordinate(THREE_IXP,:),vtk_node_gll_glonum,vtk_node_z)

!
!
!***********************************************
!---->create hexa cell type
!***********************************************

      allocate(vtk_cell_type(vtk_nhexa))

      do ihexa = ONE_IXP,vtk_nhexa
         vtk_cell_type(ihexa) = 12_I8
      enddo

!
!
!***********************************************
!---->create hexa offset
!***********************************************

      ios = init_array_int(vtk_offset,vtk_nhexa,"vtk_offset")

      do ihexa = ONE_IXP,vtk_nhexa
         vtk_offset(ihexa) = ihexa*8_IXP
      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine init_vtk_mesh
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine finds which GLL nodes to use for generating a VTK mesh
!>@param  hexa                : EFISPEC hexahedron elements used for
!>@param  nhexa               : number of EFISPEC hexahedron elements
!>@param  efi_hexa_gll_glonum : EFISPEC indirection array from local to global GLL nodes numbering
!>@param  ngll                : total number of unique GLL nodes in EFISPEC mesh
!>@param  gll                 : GLL nodes used for generating VTK nodes
!>@param  ngll_unique         : total number of unique GLL nodes used for generating VTK nodes
!>@return gll                 : GLL nodes used for generating VTK nodes
!>@return ngll_unique         : total number of unique GLL nodes used for generating VTK nodes
!***********************************************************************************************************************************************************************************
   subroutine init_vtk_node(hexa,nhexa,efi_hexa_gll_glonum,gll,ngll,ngll_unique)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,ig_nhexa

      implicit none

      integer(kind=IXP), intent( in)                                              :: nhexa
      integer(kind=IXP), intent( in)                                              :: ngll
      integer(kind=IXP), intent( in), dimension(nhexa)                            :: hexa
      integer(kind=IXP), intent( in), dimension(IG_NGLL,IG_NGLL,IG_NGLL,ig_nhexa) :: efi_hexa_gll_glonum
      integer(kind=IXP), intent(out), dimension(ngll)                             :: gll
      integer(kind=IXP), intent(out)                                              :: ngll_unique

      integer(kind=IXP):: myhexa
      integer(kind=IXP):: ihexa
      integer(kind=IXP):: igll
      integer(kind=IXP):: k
      integer(kind=IXP):: l
      integer(kind=IXP):: m


      do igll = ONE_IXP,ngll

         gll(igll) = ZERO_IXP

      enddo


      do ihexa = ONE_IXP,nhexa

         myhexa = hexa(ihexa)

         do k = ONE_IXP,IG_NGLL
            do l = ONE_IXP,IG_NGLL
               do m = ONE_IXP,IG_NGLL

                  igll = efi_hexa_gll_glonum(m,l,k,myhexa)
                  
                  gll(igll) = ONE_IXP

               enddo
            enddo
         enddo

      enddo

      ngll_unique = ZERO_IXP

      do igll = ONE_IXP,ngll

         ngll_unique = ngll_unique + gll(igll)

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine init_vtk_node
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine generates i) VTK nodes' numbering from 0 to n-1 and ii) indirection array from VTK's geometric nodes to EFISPEC's GLL nodes.
!>@param  node  : VTK nodes with numbering from 0 to nnode-ONE_IXP
!>@param  gll   : VTK indirection array from VTK's geometric nodes to EFISPEC's GLL nodes
!>@param  nnode : total number of unique geometric nodes in VTK mesh
!>@param  ngll  : total number of unique GLL nodes in EFISPEC mesh
!>@return node  : VTK nodes with numbering from 0 to nnode-ONE_IXP
!>@return gll   : VTK indirection array from VTK's geometric nodes to EFISPEC's GLL nodes
!***********************************************************************************************************************************************************************************
   subroutine init_vtk_node_numbering(node,gll,nnode,ngll)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_NGLL

      use mod_init_memory

      implicit none

      integer(kind=IXP), intent( in)                            :: ngll
      integer(kind=IXP), intent( in)                            :: nnode
      integer(kind=IXP), intent(out), dimension(ngll)           :: node
      integer(kind=IXP), intent(out), dimension(:), allocatable :: gll

      integer(kind=IXP):: ios
      integer(kind=IXP):: igll
      integer(kind=IXP):: jgll

      ios = init_array_int(gll,nnode,"gll")

      jgll = ZERO_IXP

      do igll = ONE_IXP,ngll

         if (node(igll) == ONE_IXP) then

            jgll       = jgll + ONE_IXP

            node(igll) = jgll - ONE_IXP

            gll (jgll) = igll 

         endif

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine init_vtk_node_numbering
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine creates the connectivity array for defining VTK hexahedron elements from VTK geometric nodes.
!>@param  hexa                : list of EFISPEC hexahedron elements used for generating VTK mesh
!>@param  efi_nhexa           : total number of element in array 'hexa'
!>@param  node                : VTK geometric nodes 
!>@param  ngll                : total number of unique GLL nodes in EFISPEC mesh
!>@param  efi_hexa_gll_glonum : EFISPEC indirection array from local to global GLL nodes numbering
!>@param  vtk_hexa            : connectivity array for defining VTK hexahedron elements from VTK geometric nodes
!>@param  vtk_nhexa           : total number of VTK hexahedron elements
!>@return vtk_hexa            : connectivity array for defining VTK hexahedron elements from VTK geometric nodes
!>@return vtk_nhexa           : total number of VTK hexahedron elements
!***********************************************************************************************************************************************************************************
   subroutine init_vtk_hexa_connectivity(hexa,efi_nhexa,node,ngll,efi_hexa_gll_glonum,vtk_hexa,vtk_nhexa)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,IG_NDOF&
                                     ,ig_nhexa

      use mod_init_memory

      implicit none

      integer(kind=IXP), intent( in)                                              :: efi_nhexa
      integer(kind=IXP), intent( in)                                              :: ngll
      integer(kind=IXP), intent( in), dimension(ngll)                             :: node
      integer(kind=IXP), intent( in), dimension(efi_nhexa)                        :: hexa
      integer(kind=IXP), intent( in), dimension(IG_NGLL,IG_NGLL,IG_NGLL,ig_nhexa) :: efi_hexa_gll_glonum
      integer(kind=IXP), intent(out), dimension(:), allocatable                   :: vtk_hexa
      integer(kind=IXP), intent(out)                                              :: vtk_nhexa

      integer(kind=IXP), dimension(8_IXP) :: mygll
      integer(kind=IXP)                   :: ihexa
      integer(kind=IXP)                   :: jhexa
      integer(kind=IXP)                   :: myhexa
      integer(kind=IXP)                   :: k
      integer(kind=IXP)                   :: l
      integer(kind=IXP)                   :: m
      integer(kind=IXP)                   :: ios

      vtk_nhexa = efi_nhexa*(IG_NGLL-ONE_IXP)**IG_NDOF

      ios       = init_array_int(vtk_hexa,vtk_nhexa*8_IXP,"vtk_hexa")

      jhexa     = ZERO_IXP

      do ihexa = ONE_IXP,efi_nhexa

         myhexa = hexa(ihexa)

         do k = ONE_IXP,IG_NGLL-ONE_IXP
            do l = ONE_IXP,IG_NGLL-ONE_IXP
               do m = ONE_IXP,IG_NGLL-ONE_IXP

                  jhexa    = jhexa + ONE_IXP

                  mygll(1_IXP) = efi_hexa_gll_glonum(m        ,l        ,k        ,myhexa) 
                  mygll(2_IXP) = efi_hexa_gll_glonum(m+ONE_IXP,l        ,k        ,myhexa)
                  mygll(3_IXP) = efi_hexa_gll_glonum(m+ONE_IXP,l+ONE_IXP,k        ,myhexa)
                  mygll(4_IXP) = efi_hexa_gll_glonum(m        ,l+ONE_IXP,k        ,myhexa)
                  mygll(5_IXP) = efi_hexa_gll_glonum(m        ,l        ,k+ONE_IXP,myhexa)
                  mygll(6_IXP) = efi_hexa_gll_glonum(m+ONE_IXP,l        ,k+ONE_IXP,myhexa)
                  mygll(7_IXP) = efi_hexa_gll_glonum(m+ONE_IXP,l+ONE_IXP,k+ONE_IXP,myhexa)
                  mygll(8_IXP) = efi_hexa_gll_glonum(m        ,l+ONE_IXP,k+ONE_IXP,myhexa)

                  vtk_hexa((jhexa-ONE_IXP)*8_IXP + 1_IXP) = node(mygll(1_IXP)) 
                  vtk_hexa((jhexa-ONE_IXP)*8_IXP + 2_IXP) = node(mygll(2_IXP))
                  vtk_hexa((jhexa-ONE_IXP)*8_IXP + 3_IXP) = node(mygll(3_IXP))
                  vtk_hexa((jhexa-ONE_IXP)*8_IXP + 4_IXP) = node(mygll(4_IXP))
                  vtk_hexa((jhexa-ONE_IXP)*8_IXP + 5_IXP) = node(mygll(5_IXP))
                  vtk_hexa((jhexa-ONE_IXP)*8_IXP + 6_IXP) = node(mygll(6_IXP))
                  vtk_hexa((jhexa-ONE_IXP)*8_IXP + 7_IXP) = node(mygll(7_IXP))
                  vtk_hexa((jhexa-ONE_IXP)*8_IXP + 8_IXP) = node(mygll(8_IXP))

               enddo
            enddo
         enddo


      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine init_vtk_hexa_connectivity
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine manages output format for volume snapshot. For now, only VTK format is available. Each cpu writes its own part of the mesh.
!***********************************************************************************************************************************************************************************
   subroutine write_snapshot_volume()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      ig_idt&
                                     ,cg_prefix&
                                     ,cg_myrank&
                                     ,rg_gll_displacement&
                                     ,rg_gll_velocity&
                                     ,rg_gll_acceleration&
                                     ,ig_vtk_nhexa_snapshot&
                                     ,ig_snapshot_volume_saving_incr&
                                     ,lg_snapshot_volume_displacement&
                                     ,lg_snapshot_volume_velocity&
                                     ,lg_snapshot_volume_acceleration

      implicit none

      character(len=CIL) :: fname
      character(len=  6) :: csnapshot


      write(csnapshot,'(i6.6)') ig_idt

!
!
!---->snapshot displacement

      if ( lg_snapshot_volume_displacement .and. (mod(ig_idt-ONE_IXP,ig_snapshot_volume_saving_incr) == ZERO_IXP) .and. (ig_vtk_nhexa_snapshot > ZERO_IXP) ) then

         fname = trim(cg_prefix)//".volume.snapshot.uxyz."//trim(csnapshot)//".cpu."//trim(cg_myrank)//".vtu"

         call write_snapshot_volume_vtk(fname,rg_gll_displacement(ONE_IXP,:),"ux",rg_gll_displacement(TWO_IXP,:),"uy",rg_gll_displacement(THREE_IXP,:),"uz")

      endif

!
!
!---->snapshot velocity

      if ( lg_snapshot_volume_velocity .and. (mod(ig_idt-ONE_IXP,ig_snapshot_volume_saving_incr) == ZERO_IXP) .and. (ig_vtk_nhexa_snapshot > ZERO_IXP) ) then

         fname = trim(cg_prefix)//".volume.snapshot.vxyz."//trim(csnapshot)//".cpu."//trim(cg_myrank)//".vtu"

         call write_snapshot_volume_vtk(fname,rg_gll_velocity(ONE_IXP,:),"vx",rg_gll_velocity(TWO_IXP,:),"vy",rg_gll_velocity(THREE_IXP,:),"vz")

      endif

!
!
!---->snapshot acceleration

      if ( lg_snapshot_volume_acceleration .and. (mod(ig_idt-ONE_IXP,ig_snapshot_volume_saving_incr) == ZERO_IXP) .and. (ig_vtk_nhexa_snapshot > ZERO_IXP) ) then

         fname = trim(cg_prefix)//".volume.snapshot.axyz."//trim(csnapshot)//".cpu."//trim(cg_myrank)//".vtu"

         call write_snapshot_volume_vtk(fname,rg_gll_acceleration(ONE_IXP,:),"ax",rg_gll_acceleration(TWO_IXP,:),"ay",rg_gll_acceleration(THREE_IXP,:),"az")

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine write_snapshot_volume
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine writes volume snapshot in a VTK XML file that contains a maximum of three vectors.
!>@param fname      : VTK file name
!>@param  gll_var_x : vector number 1 to be written in VTK file. The vector must be stored in an array ordered by unique GLL nodes number.
!>@param name_var_x : name of the variable in vector number 1.
!>@param  gll_var_y : vector number 2 to be written in VTK file. The vector must be stored in an array ordered by unique GLL nodes number.
!>@param name_var_y : name of the variable in vector number 2.
!>@param  gll_var_z : vector number 3 to be written in VTK file. The vector must be stored in an array ordered by unique GLL nodes number.
!>@param name_var_z : name of the variable in vector number 3.
!***********************************************************************************************************************************************************************************
   subroutine write_snapshot_volume_vtk(fname,gll_var_x,name_var_x,gll_var_y,name_var_y,gll_var_z,name_var_z)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only:&
                                     IG_NDOF&
                                    ,ig_ngll_total&
                                    ,ig_vtk_hexa_conn_snapshot&
                                    ,ig_vtk_node_gll_glonum_snapshot&
                                    ,rg_vtk_node_x_snapshot&
                                    ,rg_vtk_node_y_snapshot&
                                    ,rg_vtk_node_z_snapshot&
                                    ,ig_vtk_cell_type_snapshot&
                                    ,ig_vtk_offset_snapshot&
                                    ,ig_vtk_nhexa_snapshot&
                                    ,ig_vtk_nnode_snapshot

      use mod_gll_value

      use mod_vtk_io

      implicit none

      character(len =255)          , intent(in)                           :: fname
      real     (kind=RXP), optional, intent(in), dimension(ig_ngll_total) :: gll_var_x
      real     (kind=RXP), optional, intent(in), dimension(ig_ngll_total) :: gll_var_y
      real     (kind=RXP), optional, intent(in), dimension(ig_ngll_total) :: gll_var_z

      character(len=*)   , optional, intent(in)                           :: name_var_x
      character(len=*)   , optional, intent(in)                           :: name_var_y
      character(len=*)   , optional, intent(in)                           :: name_var_z

      real     (kind=RXP), dimension(ig_vtk_nnode_snapshot)               :: vtk_var_x
      real     (kind=RXP), dimension(ig_vtk_nnode_snapshot)               :: vtk_var_y
      real     (kind=RXP), dimension(ig_vtk_nnode_snapshot)               :: vtk_var_z

      integer  (kind=IXP)                                                 :: ios
                                               
!
!---->transfer GLL nodes values to VTK nodes

      if (present(gll_var_x)) call get_node_gll_value(gll_var_x,ig_vtk_node_gll_glonum_snapshot,vtk_var_x)
      if (present(gll_var_y)) call get_node_gll_value(gll_var_y,ig_vtk_node_gll_glonum_snapshot,vtk_var_y)
      if (present(gll_var_z)) call get_node_gll_value(gll_var_z,ig_vtk_node_gll_glonum_snapshot,vtk_var_z)

!   
!---->initialize VTK file
    
      ios = VTK_INI_XML(                                   &
                        output_format = 'BINARY'           &
                       ,filename      = trim(fname)        &
                       ,mesh_topology = 'UnstructuredGrid' )
    
      ios = VTK_GEO_XML(                            &
                        NN = ig_vtk_nnode_snapshot  &
                       ,NC = ig_vtk_nhexa_snapshot  &
                       ,X  = rg_vtk_node_x_snapshot &
                       ,Y  = rg_vtk_node_y_snapshot &
                       ,Z  = rg_vtk_node_z_snapshot )
    
      ios = VTK_CON_XML(                                      &
                        NC        = ig_vtk_nhexa_snapshot     &
                       ,connect   = ig_vtk_hexa_conn_snapshot &
                       ,offset    = ig_vtk_offset_snapshot    &
                       ,cell_type = ig_vtk_cell_type_snapshot )
    
      ios = VTK_DAT_XML(                          &
                        var_location     = 'NODE' &
                       ,var_block_action = 'OPEN' )
    
!   
!---->write variable values to VTK XML file

      if (present(gll_var_x)) then

         ios = VTK_VAR_XML(                                &
                           NC_NN   = ig_vtk_nnode_snapshot &
                          ,varname = name_var_x            &
                          ,var     = vtk_var_x             )

      endif

      if (present(gll_var_y)) then

         ios = VTK_VAR_XML(                                &
                           NC_NN   = ig_vtk_nnode_snapshot &
                          ,varname = name_var_y            &
                          ,var     = vtk_var_y             )

      endif

      if (present(gll_var_z)) then

         ios = VTK_VAR_XML(                                &
                           NC_NN   = ig_vtk_nnode_snapshot &
                          ,varname = name_var_z            &
                          ,var     = vtk_var_z             )

      endif
      
!   
!---->close VTK XML file

      ios = VTK_DAT_XML(                          &
                        var_location     = 'NODE' &
                       ,var_block_action = 'CLOSE')
    
      ios = VTK_GEO_XML()
    
      ios = VTK_END_XML()

      return

!***********************************************************************************************************************************************************************************
   end subroutine write_snapshot_volume_vtk
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine prepares the arrays for writing elastic properties of the medium.
!***********************************************************************************************************************************************************************************
   subroutine write_medium()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      ig_ngll_total&
                                     ,ig_nhexa&
                                     ,IG_NGLL&
                                     ,cg_prefix&
                                     ,cg_myrank&
                                     ,ig_vtk_nhexa_snapshot&
                                     ,ig_hexa_gll_glonum&
                                     ,rg_hexa_gll_rhovs2&
                                     ,rg_hexa_gll_rhovp2&
                                     ,rg_hexa_gll_rho

      implicit none

      real(kind=RXP), dimension(ig_ngll_total) :: gll_vs
      real(kind=RXP), dimension(ig_ngll_total) :: gll_vp
      real(kind=RXP), dimension(ig_ngll_total) :: gll_rho

      integer(kind=IXP)                       :: ihexa
      integer(kind=IXP)                       :: igll
      integer(kind=IXP)                       :: k
      integer(kind=IXP)                       :: l
      integer(kind=IXP)                       :: m

      character(len=CIL)             :: fname

      do ihexa = ONE_IXP,ig_nhexa

         do k = ONE_IXP,IG_NGLL
            do l = ONE_IXP,IG_NGLL
               do m = ONE_IXP,IG_NGLL

                  igll = ig_hexa_gll_glonum(m,l,k,ihexa)

                  gll_vs (igll) = sqrt(rg_hexa_gll_rhovs2(m,l,k,ihexa)/rg_hexa_gll_rho(m,l,k,ihexa))
                  gll_vp (igll) = sqrt(rg_hexa_gll_rhovp2(m,l,k,ihexa)/rg_hexa_gll_rho(m,l,k,ihexa))
                  gll_rho(igll) = rg_hexa_gll_rho(m,l,k,ihexa)

               enddo
            enddo
         enddo

      enddo


      if (ig_vtk_nhexa_snapshot > ZERO_IXP) then

         fname = trim(cg_prefix)//".medium.cpu."//trim(cg_myrank)//".vtu"
       
         call write_snapshot_volume_vtk(fname,gll_vs,"vs",gll_vp,"vp",gll_rho,"rho")

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine write_medium
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine selects which ParaView collection files should be written (displacement, velocity and/or acceleration collections)
!!depending on value of variables mod_global_variables::lg_snapshot_volume_displacement, mod_global_variables::lg_snapshot_volume_velocity
!!and mod_global_variables::lg_snapshot_volume_acceleration.
!***********************************************************************************************************************************************************************************
   subroutine write_collection_vtk_vol()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      lg_snapshot_volume_displacement&
                                     ,lg_snapshot_volume_velocity&
                                     ,lg_snapshot_volume_acceleration

      implicit none

      if (lg_snapshot_volume_displacement) then
         call collection_vtk_volume("volume.snapshot.uxyz")
      endif

      if (lg_snapshot_volume_velocity) then
         call collection_vtk_volume("volume.snapshot.vxyz")
      endif

      if (lg_snapshot_volume_acceleration) then
         call collection_vtk_volume("volume.snapshot.axyz")
      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_collection_vtk_vol
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine write Paraview collection file *.pvd of VTK XML volume snapshot files *.vtu.
!>@param varname : variable name
!***********************************************************************************************************************************************************************************
   subroutine collection_vtk_volume(varname)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                      get_newunit&
                                     ,cg_prefix&
                                     ,rg_dt&
                                     ,ig_ndt&
                                     ,ig_ncpu&
                                     ,ig_myrank&
                                     ,ig_snapshot_volume_saving_incr&
                                     ,ig_mpi_comm_simu&
                                     ,ig_vtk_nhexa_snapshot

      implicit none

      character(len=*), intent(in) :: varname

      real(kind=RXP)                        :: time

      integer(kind=IXP), dimension(ig_ncpu)  :: vtk_nhexa
      integer(kind=IXP)                     :: icpu
      integer(kind=IXP)                     :: istep
      integer(kind=IXP)                     :: myunit
      integer(kind=IXP)                     :: ios

      character(len=CIL)           :: fname
      character(len=6  )           :: csnapshot
      character(len=6  )           :: crank


!
!---->cpuO gets the number of hexa in VTK's mesh of other cpus

      call mpi_gather(ig_vtk_nhexa_snapshot,ONE_IXP,mpi_integer,vtk_nhexa,ONE_IXP,mpi_integer,ZERO_IXP,ig_mpi_comm_simu,ios)

      if (ig_myrank == ZERO_IXP) then

         open(unit=get_newunit(myunit),file=trim(cg_prefix)//".collection."//trim(varname)//".pvd")
       
         write(unit=myunit,fmt='(a)') "<?xml version=""1.0""?>"
         write(unit=myunit,fmt='(a)') "<VTKFile type=""Collection"" version=""0.1"" byte_order=""BigEndian"">"
         write(unit=myunit,fmt='(a)') "  <Collection>"
       
         do istep = ONE_IXP,ig_ndt,ig_snapshot_volume_saving_incr
       
            write(csnapshot,'(I6.6)') istep
       
            time  = real(istep-ONE_IXP,kind=RXP)*rg_dt
       
            do icpu = ONE_IXP,ig_ncpu
       
               if (vtk_nhexa(icpu) > ZERO_IXP) then
       
                  write(crank,'(I6.6)') icpu-ONE_IXP
                
                  fname = trim(cg_prefix)//"."//trim(varname)//"."//trim(csnapshot)//".cpu."//trim(crank)//".vtu"
                
                  write(unit=myunit,fmt='(a,E14.7,3a)') "    <DataSet timestep=""",time,""" group="""" part=""0"" file=""",trim(fname),"""/>"
       
               endif
       
            enddo
       
         enddo
       
         write(unit=myunit,fmt='(a)') "  </Collection>"
         write(unit=myunit,fmt='(a)') "</VTKFile>"
       
         close(myunit)

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine collection_vtk_volume
!***********************************************************************************************************************************************************************************

end module mod_snapshot_volume
