!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to initialize the medium where wave propagation takes place.

!>@brief
!!This module contains subroutines to initialize medium for hexahedron and quadrangle elements.
!!It also contains subroutines to write medium in binary VTK xml format readable by ParaView,
!!and to perform trilinear interpolation of tomographic models onto the GLL points.
module mod_init_medium
   
   use mpi

   use mod_precision

   use mod_global_variables, only : CIL

   implicit none

   private

   public  :: init_hexa_medium
   public  :: init_quadp_medium
   private :: init_medium_from_mat_file
   private :: write_medium_vtk_cell_xml
   private :: write_medium_vtk_gll_xml
   private :: write_medium_collection
   private :: info_medium
   private :: read_tomo
   private :: trilinear_interpolation
   
   contains


!
!
!>@brief
!!This subroutine fills medium properties (elastic or anelastic) at GLL nodes of hexahedron elements based on material number found in file *.inp generated by CUBIT mesh generator.
!!If files *.cpu.*.mat are present in the simulation's directory, then material number of hexahedron elements are read from files *.cpu*.mat.
!>@return mod_global_variables::rg_hexa_gll_rho    
!>@return mod_global_variables::rg_hexa_gll_rhovs2 
!>@return mod_global_variables::rg_hexa_gll_rhovp2
!>@return mod_global_variables::rg_hexa_gll_wkqs
!>@return mod_global_variables::rg_hexa_gll_wkqp
!>@return mod_global_variables::tg_elastic_material
!>@return mod_global_variables::tg_anelastic_material
!***********************************************************************************************************************************************************************************
      subroutine init_hexa_medium()
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                      ig_nmaterial&
                                     ,ig_nlayer&
                                     ,tg_layer&
                                     ,tg_elastic_material&
                                     ,tg_anelastic_material&
                                     ,cg_struture_name&
                                     ,ig_myrank&
                                     ,ig_ncpu&
                                     ,cg_prefix&
                                     ,cg_myrank&
                                     ,ig_mpi_comm_simu&
                                     ,IG_NGLL&
                                     ,ig_hexa_gll_glonum&
                                     ,ig_hexa_material_number&
                                     ,LG_VISCO&
                                     ,LG_OUTPUT_MEDIUM_VTK&
                                     ,ig_nhexa&
                                     ,rg_hexa_gll_rho&
                                     ,rg_hexa_gll_rhovs2&
                                     ,rg_hexa_gll_rhovp2&
                                     ,rg_hexa_gll_wkqs&
                                     ,rg_hexa_gll_wkqp&
                                     ,rg_hexa_gll_ksixx&
                                     ,rg_hexa_gll_ksiyy&
                                     ,rg_hexa_gll_ksizz&
                                     ,rg_hexa_gll_ksixy&
                                     ,rg_hexa_gll_ksixz&
                                     ,rg_hexa_gll_ksiyz&
                                     ,ig_vtk_nhexa_snapshot&
                                     ,IG_NRELAX&
                                     ,error_stop&
                                     ,IG_LST_UNIT&
                                     ,tg_tomo

      use mod_init_memory

      use mod_homogenization

      use mod_snapshot_volume, only : init_snapshot_volume

      implicit none

      integer(kind=IXP), dimension(ig_ncpu)  :: vtk_nhexa

      integer(kind=IXP)                      :: ios
      integer(kind=IXP)                      :: iel
      integer(kind=IXP)                      :: igll
      integer(kind=IXP)                      :: imat
      integer(kind=IXP)                      :: ilay
      integer(kind=IXP)                      :: k
      integer(kind=IXP)                      :: l
      integer(kind=IXP)                      :: m
      integer(kind=IXP)                      :: n

      real(kind=RXP)                         :: vp_gll
      real(kind=RXP)                         :: vs_gll
      real(kind=RXP)                         :: rho_gll
      real(kind=RXP)                         :: qp_gll
      real(kind=RXP)                         :: qs_gll

      real(kind=RXP)                         :: uswm
      real(kind=RXP)                         :: upwm
      real(kind=RXP), dimension(IG_NRELAX)   :: wkqs
      real(kind=RXP), dimension(IG_NRELAX)   :: wkqp

      logical(kind=IXP)                      :: is_file_exist

      character(len=CIL)                     :: info


      if (ig_myrank == ZERO_IXP) then
         write(IG_LST_UNIT,'(" ",/,a)') "initialization of medium's mechanical properties"
      endif

!
!
!************************************************************************************************
!---->initialize memory
!************************************************************************************************

      ios = init_array_real(rg_hexa_gll_rho,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_rho") 

      ios = init_array_real(rg_hexa_gll_rhovs2,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_rhovs2")

      ios = init_array_real(rg_hexa_gll_rhovp2,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"rg_hexa_gll_rhovp2")

      if (LG_VISCO) then

         ios = init_array_real(rg_hexa_gll_ksixx,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,IG_NRELAX,"rg_hexa_gll_ksixx")

         ios = init_array_real(rg_hexa_gll_ksiyy,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,IG_NRELAX,"rg_hexa_gll_ksiyy")

         ios = init_array_real(rg_hexa_gll_ksizz,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,IG_NRELAX,"rg_hexa_gll_ksizz")

         ios = init_array_real(rg_hexa_gll_ksixy,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,IG_NRELAX,"rg_hexa_gll_ksixy")

         ios = init_array_real(rg_hexa_gll_ksixz,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,IG_NRELAX,"rg_hexa_gll_ksixz")

         ios = init_array_real(rg_hexa_gll_ksiyz,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,IG_NRELAX,"rg_hexa_gll_ksiyz")

         ios = init_array_real(rg_hexa_gll_wkqs,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,IG_NRELAX,"rg_hexa_gll_wkqs")

         ios = init_array_real(rg_hexa_gll_wkqp,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,IG_NRELAX,"rg_hexa_gll_wkqp")

!
!------->allocation when material are defined (i.e., ig_nmaterial > 0)

         do imat = ONE_IXP,ig_nmaterial

            allocate(tg_anelastic_material(imat)%wkqs(IG_NRELAX),tg_anelastic_material(imat)%wkqp(IG_NRELAX))

         enddo

!
!------->allocation when layers are defined (i.e., ig_nlayer > 0)

         do ilay = ONE_IXP,ig_nlayer

            do imat = ONE_IXP,tg_layer(ilay)%nmat
          
               allocate(tg_layer(ilay)%anelastic(imat)%wkqs(IG_NRELAX),tg_layer(ilay)%anelastic(imat)%wkqp(IG_NRELAX))
          
            enddo

         enddo

      endif   !end if LG_VISCO


!
!
!***********************************************************************************************************
!---->if files *.mat exists, then the medium (ig_hexa_material_number) is read from such files
!***********************************************************************************************************
      inquire(file=trim(cg_prefix)//".cpu."//trim(cg_myrank)//".mat", exist=is_file_exist)

      if (is_file_exist) then

         if (ig_myrank == ZERO_IXP) then
            write(IG_LST_UNIT,'(a)') " --> medium generated from geomodeler's material (file *.mat)"
         endif

         call init_medium_from_mat_file(ig_hexa_material_number)

      elseif (ig_nlayer > ZERO_IXP) then

         if (ig_myrank == ZERO_IXP) then
            write(IG_LST_UNIT,'(a)') " --> medium generated using vertical homogenization method"
         endif

      elseif (tg_tomo%is_tomo_defined) then

         if (ig_myrank == ZERO_IXP) then
            write(IG_LST_UNIT,'(a)')     " --> medium generated from tomography files"
            write(IG_LST_UNIT,'(a,a,a)') "     (input folder: ",trim(tg_tomo%input_folder),")"
         endif

      else

         if (ig_myrank == ZERO_IXP) then
            write(IG_LST_UNIT,'(a)') " --> medium generated from CUBIT block's information"
         endif

      endif

!
!
!************************************************************************************************
!---->check if the medium (ig_hexa_material_number) is entirely defined
!************************************************************************************************

      if ( (.not. tg_tomo%is_tomo_defined) .and. (is_file_exist .or. (ig_nlayer == ZERO_IXP) ) ) then
 
         do iel = ONE_IXP,ig_nhexa
         
            imat = ig_hexa_material_number(iel)
         
            if ( (imat <= ZERO_IXP) .or. (imat > ig_nmaterial) ) then
               write(info,'(a)') "error in subroutine init_hexa_medium: undefined material for hexa"
               call error_stop(info)
            endif
         
         enddo

      endif


!
!
!************************************************************************************************
!---->compute elastic and anelastic parameters for materials
!************************************************************************************************
      do imat = ONE_IXP,ig_nmaterial
!
!
!------->elastic

         tg_elastic_material(imat)%lam2 = tg_elastic_material(imat)%dens*tg_elastic_material(imat)%svel**TWO_IXP
         tg_elastic_material(imat)%lam1 = tg_elastic_material(imat)%dens*tg_elastic_material(imat)%pvel**TWO_IXP - TWO_RXP*tg_elastic_material(imat)%lam2
         tg_elastic_material(imat)%pois = (tg_elastic_material(imat)%pvel**TWO_IXP - TWO_RXP*tg_elastic_material(imat)%svel**TWO_IXP)/(TWO_RXP*(tg_elastic_material(imat)%pvel**TWO_IXP-tg_elastic_material(imat)%svel**TWO_IXP))
         tg_elastic_material(imat)%youn = TWO_RXP*tg_elastic_material(imat)%lam2*(ONE_RXP + tg_elastic_material(imat)%pois)
         tg_elastic_material(imat)%bulk = tg_elastic_material(imat)%lam1+TWO_RXP/3.0_RXP*tg_elastic_material(imat)%lam2
         tg_elastic_material(imat)%rvel = (0.862_RXP + 1.14_RXP*tg_elastic_material(imat)%pois)/(ONE_RXP + tg_elastic_material(imat)%pois)*tg_elastic_material(imat)%svel
         tg_elastic_material(imat)%pwmo = tg_elastic_material(imat)%lam1 + TWO_RXP*tg_elastic_material(imat)%lam2

         if (tg_elastic_material(imat)%pois <= ZERO_RXP .or. tg_elastic_material(imat)%pois > 0.5_RXP) then

            write(info,'(a)') "error in subroutine module_init_medium:init_hexa_medium: Poisson ratio smaller than 0.0 or larger than 0.5"
            call error_stop(info)

         endif
   
         if (ig_myrank == ZERO_IXP) then

            if (.not.LG_VISCO) then

               write(IG_LST_UNIT,'(/,a,i0,/,a)') "material ",imat," --> elastic"

            else

               write(IG_LST_UNIT,'(/,a,i0,/,a)') "material ",imat," --> anelastic"

            endif

            write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> s-wave velocity           = ",tg_elastic_material(imat)%svel," m/s"
            write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> p-wave velocity           = ",tg_elastic_material(imat)%pvel," m/s"
            write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> poisson ratio             = ",tg_elastic_material(imat)%pois," "
            write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> density                   = ",tg_elastic_material(imat)%dens," kg/m3"
            write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> lambda (first lame coef.) = ",tg_elastic_material(imat)%lam1," Pa"
            write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> s-wave modulus            = ",tg_elastic_material(imat)%lam2," Pa"
            write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> p-wave modulus            = ",tg_elastic_material(imat)%pwmo," Pa"
            write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> bulk-modulus              = ",tg_elastic_material(imat)%bulk," Pa"

         endif

!
!
!------->anelastic

         if (LG_VISCO) then

               call init_medium_visco(tg_elastic_material(imat)%pvel,tg_elastic_material(imat)%svel,tg_elastic_material(imat)%dens,tg_anelastic_material(imat)%qp,tg_anelastic_material(imat)%qs,tg_anelastic_material(imat)%freq,tg_anelastic_material(imat)%wkqp,tg_anelastic_material(imat)%wkqs,tg_anelastic_material(imat)%upwm,tg_anelastic_material(imat)%uswm)
   
               if (ig_myrank == ZERO_IXP) then

                  write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> frequency for viscosity   = ",tg_anelastic_material(imat)%freq," hz"
                  write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> s-wave quality factor     = ",tg_anelastic_material(imat)%qs  ," "
                  write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> p-wave quality factor     = ",tg_anelastic_material(imat)%qp  ," "
                  write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> unrelaxed s-wave modulus  = ",tg_anelastic_material(imat)%uswm," pa"
                  write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> unrelaxed p-wave modulus  = ",tg_anelastic_material(imat)%upwm," pa"

               endif
   
         endif !endif of material type anelastic
   
      enddo !enddo of loop on material


!
!
!************************************************************************************************
!---->compute elastic and anelastic parameters for layers
!************************************************************************************************
      do ilay = ONE_IXP,ig_nlayer
!
!
!------->elastic

         do imat = ONE_IXP,tg_layer(ilay)%nmat

            tg_layer(ilay)%elastic(imat)%lam2 = tg_layer(ilay)%elastic(imat)%dens*tg_layer(ilay)%elastic(imat)%svel**TWO_IXP
            tg_layer(ilay)%elastic(imat)%lam1 = tg_layer(ilay)%elastic(imat)%dens*tg_layer(ilay)%elastic(imat)%pvel**TWO_IXP - TWO_RXP*tg_layer(ilay)%elastic(imat)%lam2
            tg_layer(ilay)%elastic(imat)%pois = (tg_layer(ilay)%elastic(imat)%pvel**TWO_IXP - TWO_RXP*tg_layer(ilay)%elastic(imat)%svel**TWO_IXP)/(TWO_RXP*(tg_layer(ilay)%elastic(imat)%pvel**TWO_IXP-tg_layer(ilay)%elastic(imat)%svel**TWO_IXP))
            tg_layer(ilay)%elastic(imat)%youn = TWO_RXP*tg_layer(ilay)%elastic(imat)%lam2*(ONE_RXP + tg_layer(ilay)%elastic(imat)%pois)
            tg_layer(ilay)%elastic(imat)%bulk = tg_layer(ilay)%elastic(imat)%lam1+TWO_RXP/3.0_RXP*tg_layer(ilay)%elastic(imat)%lam2
            tg_layer(ilay)%elastic(imat)%rvel = (0.862_RXP + 1.14_RXP*tg_layer(ilay)%elastic(imat)%pois)/(ONE_RXP + tg_layer(ilay)%elastic(imat)%pois)*tg_layer(ilay)%elastic(imat)%svel
            tg_layer(ilay)%elastic(imat)%pwmo = tg_layer(ilay)%elastic(imat)%lam1 + TWO_RXP*tg_layer(ilay)%elastic(imat)%lam2
          
            if (tg_layer(ilay)%elastic(imat)%pois <= ZERO_RXP .or. tg_layer(ilay)%elastic(imat)%pois > 0.5_RXP) then
          
               write(info,'(a)') "error in subroutine module_init_medium:init_hexa_medium: Poisson ratio smaller than 0.0 or larger than 0.5"
               call error_stop(info)
          
            endif

         enddo

         if (ig_myrank == ZERO_IXP) then

            if (.not.LG_VISCO) then

               write(IG_LST_UNIT,'(/,a,i0,/,a)') "layer ",ilay," --> elastic"
               write(IG_LST_UNIT,'(a         )') " --> structure type "//trim(cg_struture_name(tg_layer(ilay)%structure_type))

            else

               write(IG_LST_UNIT,'(/,a,i0,/,a)') "layer ",ilay," --> anelastic"
               write(IG_LST_UNIT,'(a         )') " --> structure type "//trim(cg_struture_name(tg_layer(ilay)%structure_type))

            endif

            do imat = ONE_IXP,tg_layer(ilay)%nmat

               write(IG_LST_UNIT,'(a,i0        )') " --> material number           = ",imat
               write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> s-wave velocity           = ",tg_layer(ilay)%elastic(imat)%svel," m/s"
               write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> p-wave velocity           = ",tg_layer(ilay)%elastic(imat)%pvel," m/s"
               write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> poisson ratio             = ",tg_layer(ilay)%elastic(imat)%pois," "
               write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> density                   = ",tg_layer(ilay)%elastic(imat)%dens," kg/m3"
               write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> lambda (first Lame coef.) = ",tg_layer(ilay)%elastic(imat)%lam1," Pa"
               write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> s-wave modulus            = ",tg_layer(ilay)%elastic(imat)%lam2," Pa"
               write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> p-wave modulus            = ",tg_layer(ilay)%elastic(imat)%pwmo," Pa"
               write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> bulk-modulus              = ",tg_layer(ilay)%elastic(imat)%bulk," Pa"
         
               if (tg_layer(ilay)%structure_type == 3) then

                  write(IG_LST_UNIT,'(a,f15.7,a)') " --> x-correlation length      = ",tg_layer(ilay)%random(imat)%lcx," m"
                  write(IG_LST_UNIT,'(a,f15.7,a)') " --> y-correlation length      = ",tg_layer(ilay)%random(imat)%lcy," m"
                  write(IG_LST_UNIT,'(a,f15.7,a)') " --> z-correlation length      = ",tg_layer(ilay)%random(imat)%lcz," m"
                  write(IG_LST_UNIT,'(a,f15.7  )') " --> frac. fluctuation s-wave  = ",tg_layer(ilay)%random(imat)%e

               endif

            enddo

         endif

!
!
!------->anelastic

         if (LG_VISCO) then

            do imat = ONE_IXP,tg_layer(ilay)%nmat

               call init_medium_visco(tg_layer(ilay)%elastic(imat)%pvel,tg_layer(ilay)%elastic(imat)%svel,tg_layer(ilay)%elastic(imat)%dens,tg_layer(ilay)%anelastic(imat)%qp,tg_layer(ilay)%anelastic(imat)%qs,tg_layer(ilay)%anelastic(imat)%freq,tg_layer(ilay)%anelastic(imat)%wkqp,tg_layer(ilay)%anelastic(imat)%wkqs,tg_layer(ilay)%anelastic(imat)%upwm,tg_layer(ilay)%anelastic(imat)%uswm)
             
               if (ig_myrank == ZERO_IXP) then
             
                  write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> frequency for viscosity   = ",tg_layer(ilay)%anelastic(imat)%freq," hz"
                  write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> s-wave quality factor     = ",tg_layer(ilay)%anelastic(imat)%qs  ," "
                  write(IG_LST_UNIT,'(a,f15.7   ,a)') " --> p-wave quality factor     = ",tg_layer(ilay)%anelastic(imat)%qp  ," "
                  write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> unrelaxed s-wave modulus  = ",tg_layer(ilay)%anelastic(imat)%uswm," pa"
                  write(IG_LST_UNIT,'(a,es15.7e3,a)') " --> unrelaxed p-wave modulus  = ",tg_layer(ilay)%anelastic(imat)%upwm," pa"
             
               endif

            enddo
   
         endif

      enddo !enddo of loop on layers

!
!
!************************************************************************************************
!---->distribute constant material properties of hexahedron elements to gll nodes
!************************************************************************************************
      if (ig_nlayer == ZERO_IXP) then !material or tomo case

!
!
!------->read tomographic model

         if (tg_tomo%is_tomo_defined) then

            call read_tomo()

         endif

!
!
!------->elastic

         if (.not.LG_VISCO) then

            do iel = ONE_IXP,ig_nhexa
               do k = ONE_IXP,IG_NGLL        !zeta
                  do l = ONE_IXP,IG_NGLL     !eta
                     do m = ONE_IXP,IG_NGLL  !xi

                        if (tg_tomo%is_tomo_defined) then   !tomo case

                           igll = ig_hexa_gll_glonum(m,l,k,iel)
                           call trilinear_interpolation(rho_gll,tg_tomo%rho,igll)
                           call trilinear_interpolation(vs_gll, tg_tomo%vs, igll)
                           call trilinear_interpolation(vp_gll, tg_tomo%vp, igll)

                        else   ! material case

                           imat    = ig_hexa_material_number(iel)
                           rho_gll = tg_elastic_material(imat)%dens
                           vs_gll  = tg_elastic_material(imat)%svel
                           vp_gll  = tg_elastic_material(imat)%pvel

                        endif

                        ! assign material properties
                        rg_hexa_gll_rho   (m,l,k,iel) = rho_gll
                        rg_hexa_gll_rhovs2(m,l,k,iel) = rho_gll*vs_gll**TWO_IXP
                        rg_hexa_gll_rhovp2(m,l,k,iel) = rho_gll*vp_gll**TWO_IXP

                     enddo
                  enddo
               enddo
            enddo

!
!
!------->anelastic

         else

            do iel = ONE_IXP,ig_nhexa
               do k = ONE_IXP,IG_NGLL        !zeta
                  do l = ONE_IXP,IG_NGLL     !eta
                     do m = ONE_IXP,IG_NGLL  !xi

                        if (tg_tomo%is_tomo_defined) then

                           igll = ig_hexa_gll_glonum(m,l,k,iel)
                           call trilinear_interpolation(vp_gll, tg_tomo%vp, igll)
                           call trilinear_interpolation(vs_gll, tg_tomo%vs, igll)
                           call trilinear_interpolation(rho_gll,tg_tomo%rho,igll)
                           call trilinear_interpolation(qp_gll, tg_tomo%qp, igll)
                           call trilinear_interpolation(qs_gll, tg_tomo%qs, igll)

                           call init_medium_visco(vp_gll,vs_gll,rho_gll,qp_gll,qs_gll,tg_tomo%qf,wkqp,wkqs,upwm,uswm)

                        else !material case

                           imat    = ig_hexa_material_number(iel)
                           rho_gll = tg_elastic_material(imat)%dens
                           uswm    = tg_anelastic_material(imat)%uswm
                           upwm    = tg_anelastic_material(imat)%upwm
                           wkqs    = tg_anelastic_material(imat)%wkqs
                           wkqp    = tg_anelastic_material(imat)%wkqp

                        endif

                        !assign material properties to GLL point
                        rg_hexa_gll_rho   (m,l,k,iel) = rho_gll
                        rg_hexa_gll_rhovs2(m,l,k,iel) = uswm
                        rg_hexa_gll_rhovp2(m,l,k,iel) = upwm

                        do n = ONE_IXP,IG_NRELAX
                           rg_hexa_gll_wkqs(n,m,l,k,iel) = wkqs(n)
                           rg_hexa_gll_wkqp(n,m,l,k,iel) = wkqp(n)
                        enddo

                     enddo
                  enddo
               enddo
            enddo

         endif   !end if LG_VISCO

         if (tg_tomo%is_tomo_defined) then

            !deallocate tomo arrays
            deallocate(tg_tomo%vp, tg_tomo%vs, tg_tomo%rho, tg_tomo%qp, tg_tomo%qs)

         endif

!
!
!************************************************************************************************
!---->homogenization of material properties at gll nodes (layer case)
!************************************************************************************************
      else

         call medium_homogenization()

         call mpi_barrier(ig_mpi_comm_simu,ios)

      endif


!
!
!*************************************************************************************************************************
!---->get medium information (Vs_min/max, etc.) and check if gll material properties are well defined (i.e., >0, etc.)
!*************************************************************************************************************************

      call info_medium()


!
!
!************************************************************************************************
!---->write material using all gll nodes in VTK xml format
!************************************************************************************************

      if (LG_OUTPUT_MEDIUM_VTK) then

         if (ig_myrank == ZERO_IXP) then

            write(IG_LST_UNIT,'(/,a)') "Saving medium properties to VTK files"

         endif

         call init_snapshot_volume()

         call write_medium_vtk_gll_xml()

         call mpi_gather(ig_vtk_nhexa_snapshot,ONE_IXP,MPI_INTEGER,vtk_nhexa,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)

         if (ig_myrank == ZERO_IXP) call write_medium_collection(vtk_nhexa)

      endif

      return
!***********************************************************************************************************************************************************************************
      end subroutine init_hexa_medium
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine fills medium properties at GLL nodes of paraxial quadrangle elements (i.e., absorbing boundary) based on material number of hexahedron elements it belongs.
!>@return mod_global_variables::rg_quadp_gll_rhovs 
!>@return mod_global_variables::rg_quadp_gll_rhovp 
!***********************************************************************************************************************************************************************************
      subroutine init_quadp_medium()
!***********************************************************************************************************************************************************************************
      use mpi
      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,ig_nquad_parax&
                                     ,ig_quadp_neighbor_hexa&
                                     ,ig_quadp_neighbor_hexaface&
                                     ,rg_quadp_gll_rhovs&
                                     ,rg_quadp_gll_rhovp&
                                     ,rg_hexa_gll_rho&
                                     ,rg_hexa_gll_rhovs2&
                                     ,rg_hexa_gll_rhovp2&
                                     ,error_stop&
                                     ,lg_boundary_absorption

      use mod_init_memory

      implicit none
   
      integer(kind=IXP)  :: ios
      integer(kind=IXP)  :: i
      integer(kind=IXP)  :: j
      integer(kind=IXP)  :: k
      integer(kind=IXP)  :: iquad
      integer(kind=IXP)  :: ihexa
      integer(kind=IXP)  :: iface

      character(len=CIL) :: info

      !
      !See numbering convention in subroutine propagate_gll_nodes_quad of module_init_mesh.f90
      !

      if ( (ig_nquad_parax > ZERO_IXP) .and. (lg_boundary_absorption) ) then

         ios = init_array_real(rg_quadp_gll_rhovs,ig_nquad_parax,IG_NGLL,IG_NGLL,"rg_quadp_gll_rhovs")

         ios = init_array_real(rg_quadp_gll_rhovp,ig_nquad_parax,IG_NGLL,IG_NGLL,"rg_quadp_gll_rhovp")

         do iquad = ONE_IXP,ig_nquad_parax
    
            ihexa = ig_quadp_neighbor_hexa    (iquad)
            iface = ig_quadp_neighbor_hexaface(iquad)
    
            select case(iface)
               case(ONE_IXP)
                  do j = ONE_IXP,IG_NGLL
                     do i = ONE_IXP,IG_NGLL
                        rg_quadp_gll_rhovs(i,j,iquad) = rg_hexa_gll_rho(i,j,ONE_IXP,ihexa)*sqrt(rg_hexa_gll_rhovs2(i,j,ONE_IXP,ihexa)/rg_hexa_gll_rho(i,j,ONE_IXP,ihexa))
                        rg_quadp_gll_rhovp(i,j,iquad) = rg_hexa_gll_rho(i,j,ONE_IXP,ihexa)*sqrt(rg_hexa_gll_rhovp2(i,j,ONE_IXP,ihexa)/rg_hexa_gll_rho(i,j,ONE_IXP,ihexa))
                     enddo
                  enddo

               case(TWO_IXP)
                  do k = ONE_IXP,IG_NGLL
                     do i = ONE_IXP,IG_NGLL
                        rg_quadp_gll_rhovs(k,i,iquad) = rg_hexa_gll_rho(i,ONE_IXP,k,ihexa)*sqrt(rg_hexa_gll_rhovs2(i,ONE_IXP,k,ihexa)/rg_hexa_gll_rho(i,ONE_IXP,k,ihexa))
                        rg_quadp_gll_rhovp(k,i,iquad) = rg_hexa_gll_rho(i,ONE_IXP,k,ihexa)*sqrt(rg_hexa_gll_rhovp2(i,ONE_IXP,k,ihexa)/rg_hexa_gll_rho(i,ONE_IXP,k,ihexa))
                     enddo
                  enddo

               case(THREE_IXP)
                  do k = ONE_IXP,IG_NGLL
                     do j = ONE_IXP,IG_NGLL
                        rg_quadp_gll_rhovs(k,j,iquad) = rg_hexa_gll_rho(IG_NGLL,j,k,ihexa)*sqrt(rg_hexa_gll_rhovs2(IG_NGLL,j,k,ihexa)/rg_hexa_gll_rho(IG_NGLL,j,k,ihexa))
                        rg_quadp_gll_rhovp(k,j,iquad) = rg_hexa_gll_rho(IG_NGLL,j,k,ihexa)*sqrt(rg_hexa_gll_rhovp2(IG_NGLL,j,k,ihexa)/rg_hexa_gll_rho(IG_NGLL,j,k,ihexa))
                     enddo
                  enddo

               case(FOUR_IXP)
                  do k = ONE_IXP,IG_NGLL
                     do i = ONE_IXP,IG_NGLL
                        rg_quadp_gll_rhovs(i,k,iquad) = rg_hexa_gll_rho(i,IG_NGLL,k,ihexa)*sqrt(rg_hexa_gll_rhovs2(i,IG_NGLL,k,ihexa)/rg_hexa_gll_rho(i,IG_NGLL,k,ihexa))
                        rg_quadp_gll_rhovp(i,k,iquad) = rg_hexa_gll_rho(i,IG_NGLL,k,ihexa)*sqrt(rg_hexa_gll_rhovp2(i,IG_NGLL,k,ihexa)/rg_hexa_gll_rho(i,IG_NGLL,k,ihexa))
                     enddo
                  enddo

               case(FIVE_IXP)
                  do k = ONE_IXP,IG_NGLL
                     do j = ONE_IXP,IG_NGLL
                        rg_quadp_gll_rhovs(j,k,iquad) = rg_hexa_gll_rho(ONE_IXP,j,k,ihexa)*sqrt(rg_hexa_gll_rhovs2(ONE_IXP,j,k,ihexa)/rg_hexa_gll_rho(ONE_IXP,j,k,ihexa))
                        rg_quadp_gll_rhovp(j,k,iquad) = rg_hexa_gll_rho(ONE_IXP,j,k,ihexa)*sqrt(rg_hexa_gll_rhovp2(ONE_IXP,j,k,ihexa)/rg_hexa_gll_rho(ONE_IXP,j,k,ihexa))
                     enddo
                  enddo

               case(SIX_IXP)
                  do j = ONE_IXP,IG_NGLL
                     do i = ONE_IXP,IG_NGLL
                        rg_quadp_gll_rhovs(j,i,iquad) = rg_hexa_gll_rho(i,j,IG_NGLL,ihexa)*sqrt(rg_hexa_gll_rhovs2(i,j,IG_NGLL,ihexa)/rg_hexa_gll_rho(i,j,IG_NGLL,ihexa))
                        rg_quadp_gll_rhovp(j,i,iquad) = rg_hexa_gll_rho(i,j,IG_NGLL,ihexa)*sqrt(rg_hexa_gll_rhovp2(i,j,IG_NGLL,ihexa)/rg_hexa_gll_rho(i,j,IG_NGLL,ihexa))
                     enddo
                  enddo

               case default
                  write(info,'(a)') "error in subroutine init_quadp_medium: invalid face number"
                  call error_stop(info)

            end select
    
         enddo
   
      endif
      
      if (allocated(ig_quadp_neighbor_hexa    )) deallocate(ig_quadp_neighbor_hexa)
      if (allocated(ig_quadp_neighbor_hexaface)) deallocate(ig_quadp_neighbor_hexaface)
      
      return
!***********************************************************************************************************************************************************************************
      end subroutine init_quadp_medium
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine initializes the material number of hexahedron elements based on material number in files *.cpu*.mat
!>@param il_mat_num_of_hexa : array filled with material number of hexahedron elements in cpu myrank.
!***********************************************************************************************************************************************************************************
      subroutine init_medium_from_mat_file(il_mat_num_of_hexa)
!***********************************************************************************************************************************************************************************
   
      use mpi
   
      use mod_global_variables, only :&
                                      ig_nhexa&
                                     ,get_newunit&
                                     ,cg_myrank&
                                     ,cg_prefix
   
      implicit none
   
      integer(kind=IXP), intent(out) :: il_mat_num_of_hexa(:)
      integer(kind=IXP)              :: ihexa
      integer(kind=IXP)              :: myunit
   !
   !->fill the array 'il_mat_num_of_hexa' using the file *.mat specific for myrank
      open(unit=get_newunit(myunit),file=trim(cg_prefix)//".cpu."//trim(cg_myrank)//".mat")

      do ihexa = ONE_IXP,ig_nhexa

         read(unit=myunit,fmt=*) il_mat_num_of_hexa(ihexa)

      enddo

      close(myunit)
   
      return
!***********************************************************************************************************************************************************************************
      end subroutine init_medium_from_mat_file
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine writes S and P-wave velocities of the physical domain of cpu myrank in UnstructuredGrid binary vtk_cell xml file readable by ParaView.
!>@return files *.vtu
!***********************************************************************************************************************************************************************************
      subroutine write_medium_vtk_cell_xml()
!***********************************************************************************************************************************************************************************
   
      use mpi
   
      use mod_global_variables, only :&
                                      ig_hexa_material_number&
                                     ,ig_nhexa&
                                     ,ig_hexa_nnode&
                                     ,ig_hexa_gnode_glonum&
                                     ,ig_mesh_nnode&
                                     ,rg_gnode_x&
                                     ,rg_gnode_y&
                                     ,rg_gnode_z&
                                     ,tg_elastic_material&
                                     ,cg_prefix&
                                     ,ig_ncpu&
                                     ,ig_myrank&
                                     ,cg_myrank

      use mod_vtk_io

      implicit none

      real   (kind=RXP), dimension(ig_nhexa)               :: var

      integer(kind=IXP), dimension(ig_ncpu)                :: vtk_nhexa
      integer(kind=I8 ), dimension(ig_nhexa)               :: cell_type
      integer(kind=IXP), dimension(ig_nhexa)               :: offset
      integer(kind=IXP), dimension(ig_hexa_nnode*ig_nhexa) :: connectivity
      integer(kind=IXP)                                    :: ihexa
      integer(kind=IXP)                                    :: inode
      integer(kind=IXP)                                    :: imat
      integer(kind=IXP)                                    :: icon
      integer(kind=IXP)                                    :: ios
                                                         
      character(len=CIL)                                   :: fname

!
!---->file name to store the medium of each cpu

      fname = trim(cg_prefix)//'.medium.cpu.'//trim(cg_myrank)//".vtu"

!
!---->fill the array cell_type with 12 --> hexahedron 8 nodes

      do ihexa = ONE_IXP,ig_nhexa
         cell_type(ihexa) = 12_I8
      enddo

!
!---->fill the array offset : sum of vertices over all hexahedra

      do ihexa = ONE_IXP,ig_nhexa
         offset(ihexa) = ihexa*ig_hexa_nnode
      enddo

!
!---->fill the array connectivity

      icon = ZERO_IXP

      do ihexa = ONE_IXP,ig_nhexa
         do inode = ONE_IXP,ig_hexa_nnode

            icon               = icon + ONE_IXP
            connectivity(icon) = ig_hexa_gnode_glonum(inode,ihexa) - ONE_IXP

         enddo
      enddo

      ios = VTK_INI_XML(                                   &
                        output_format = 'BINARY'           &
                       ,filename      = fname              &
                       ,mesh_topology = 'UnstructuredGrid' )
                                                           
      ios = VTK_GEO_XML(                   &
                        NN = ig_mesh_nnode &
                       ,NC = ig_nhexa      &
                       ,X  = rg_gnode_x    &
                       ,Y  = rg_gnode_y    &
                       ,Z  = rg_gnode_z    )
                                                           
      ios = VTK_CON_XML(                         &
                        NC        = ig_nhexa     &
                       ,connect   = connectivity &
                       ,offset    = offset       &
                       ,cell_type = cell_type    )
    
      ios = VTK_DAT_XML(                          &
                        var_location     = 'CELL' &
                       ,var_block_action = 'OPEN' )

!
!---->fill the array var with P-wave velocity value

      do ihexa = ONE_IXP,ig_nhexa

         imat         = ig_hexa_material_number(ihexa)
         var(ihexa)   = tg_elastic_material(imat)%pvel

      enddo

      ios = VTK_VAR_XML(                   &
                        NC_NN   = ig_nhexa &
                       ,varname = 'Vp'     &
                       ,var     = var      &
                                           )
!
!---->fill the array var with S-wave velocity value

      do ihexa = ONE_IXP,ig_nhexa
         imat         = ig_hexa_material_number(ihexa)
         var(ihexa)   = tg_elastic_material(imat)%svel
      enddo

      ios = VTK_VAR_XML(                   &
                        NC_NN   = ig_nhexa &
                       ,varname = 'Vs'     &
                       ,var     = var      &
                                           )

      ios = VTK_DAT_XML(                           &
                        var_location     = 'CELL'  &
                       ,var_block_action = 'CLOSE' &
                                                   )

      ios = VTK_GEO_XML()

      ios = VTK_END_XML()

      vtk_nhexa(:) = 1_IXP

      if (ig_myrank == ZERO_IXP) call write_medium_collection(vtk_nhexa)

      return
!***********************************************************************************************************************************************************************************
      end subroutine write_medium_vtk_cell_xml
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine prepares the arrays for writing elastic properties of the medium.
!***********************************************************************************************************************************************************************************
   subroutine write_medium_vtk_gll_xml()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      ig_ngll_total&
                                     ,ig_nhexa&
                                     ,IG_NGLL&
                                     ,cg_prefix&
                                     ,cg_myrank&
                                     ,ig_vtk_nhexa_snapshot&
                                     ,ig_hexa_gll_glonum&
                                     ,rg_hexa_gll_rhovs2&
                                     ,rg_hexa_gll_rhovp2&
                                     ,rg_hexa_gll_rho

      use mod_snapshot_volume, only : write_snapshot_volume_vtk

      implicit none

      real   (kind=RXP), dimension(ig_ngll_total) :: gll_vs
      real   (kind=RXP), dimension(ig_ngll_total) :: gll_vp
      real   (kind=RXP), dimension(ig_ngll_total) :: gll_rho

      integer(kind=IXP)                           :: ihexa
      integer(kind=IXP)                           :: igll
      integer(kind=IXP)                           :: k
      integer(kind=IXP)                           :: l
      integer(kind=IXP)                           :: m
                                              
      character(len=CIL)                          :: fname

      do ihexa = ONE_IXP,ig_nhexa

         do k = ONE_IXP,IG_NGLL
            do l = ONE_IXP,IG_NGLL
               do m = ONE_IXP,IG_NGLL

                  igll = ig_hexa_gll_glonum(m,l,k,ihexa)

                  gll_vs (igll) = sqrt(rg_hexa_gll_rhovs2(m,l,k,ihexa)/rg_hexa_gll_rho(m,l,k,ihexa))
                  gll_vp (igll) = sqrt(rg_hexa_gll_rhovp2(m,l,k,ihexa)/rg_hexa_gll_rho(m,l,k,ihexa))
                  gll_rho(igll) = rg_hexa_gll_rho(m,l,k,ihexa)

               enddo
            enddo
         enddo

      enddo

      if (ig_vtk_nhexa_snapshot > ZERO_IXP) then

         fname = trim(cg_prefix)//".medium.cpu."//trim(cg_myrank)//".vtu"

         call write_snapshot_volume_vtk(fname,gll_vs,"vs",gll_vp,"vp",gll_rho,"rho")

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine write_medium_vtk_gll_xml
!***********************************************************************************************************************************************************************************



!
!
!>@brief
!!This subroutine writes a VTK xml collection file *.pvd which contains all *.vtu files generated by \ref write_medium_vtk_cell_xml or \ref write_medium_vtk_node_xml.
!>@return files *.medium.collection.pvd
!***********************************************************************************************************************************************************************************
      subroutine write_medium_collection(nhexa)
!***********************************************************************************************************************************************************************************

         use mod_global_variables, only :&
                                         get_newunit&
                                        ,cg_prefix&
                                        ,ig_ncpu

         implicit none 

         integer(kind=IXP), dimension(:), intent(in) :: nhexa
         
         integer(kind=IXP)                           :: icpu
         integer(kind=IXP)                           :: myunit
         integer(kind=IXP)                           :: pos
                                                     
         character(len=CIL)                          :: fname
         character(len= 92)                          :: prefix
         character(len=  6)                          :: cpart

         pos    = scan(cg_prefix,'/')
         prefix = cg_prefix(pos+ONE_IXP:92_IXP)
         
         open(unit=get_newunit(myunit),file=trim(cg_prefix)//".medium.collection.pvd")
         
         write(unit=myunit,fmt='(a)') "<?xml version=""1.0""?>"
         write(unit=myunit,fmt='(a)') "<VTKFile type=""Collection"" version=""0.1"" byte_order=""BigEndian"">"
         write(unit=myunit,fmt='(a)') "  <Collection>"
         
         do icpu = ONE_IXP,ig_ncpu

            if (nhexa(icpu) > ZERO_IXP) then
         
               write(cpart,'(I6.6)') icpu-ONE_IXP
               
               fname = trim(prefix)//".medium.cpu."//trim(cpart)//".vtu"
               
               write(unit=myunit,fmt='(3a)') "    <DataSet group="""" part=""0"" file=""",trim(fname),"""/>"

            endif
         
         enddo
         
         write(unit=myunit,fmt='(a)') "  </Collection>"
         write(unit=myunit,fmt='(a)') "</VTKFile>"
         
         close(myunit)

      return
!***********************************************************************************************************************************************************************************
      end subroutine write_medium_collection
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine checks if the material mechanical properties are well defined.
!***********************************************************************************************************************************************************************************
      subroutine info_medium()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      rg_hexa_gll_rho&
                                     ,rg_hexa_gll_rhovs2&
                                     ,rg_hexa_gll_rhovp2&
                                     ,ig_nhexa&
                                     ,ig_mpi_comm_simu&
                                     ,ig_myrank&
                                     ,error_stop&
                                     ,LG_VISCO&
                                     ,IG_LST_UNIT&
                                     ,IG_NGLL

      use mod_init_memory, only : init_array_real

      implicit none

      real   (kind=RXP), dimension(:,:,:,:), allocatable :: vs
      real   (kind=RXP), dimension(:,:,:,:), allocatable :: vp
      real   (kind=RXP), dimension(:,:,:,:), allocatable :: nu

      real   (kind=RXP)                                  :: rho_min
      real   (kind=RXP)                                  :: rho_max
      real   (kind=RXP)                                  :: vs_min
      real   (kind=RXP)                                  :: vs_max
      real   (kind=RXP)                                  :: vp_min
      real   (kind=RXP)                                  :: vp_max
      real   (kind=RXP)                                  :: nu_min
      real   (kind=RXP)                                  :: nu_max
      real   (kind=RXP)                                  :: rho_min_allcpu
      real   (kind=RXP)                                  :: rho_max_allcpu
      real   (kind=RXP)                                  :: vs_min_allcpu
      real   (kind=RXP)                                  :: vs_max_allcpu
      real   (kind=RXP)                                  :: vp_min_allcpu
      real   (kind=RXP)                                  :: vp_max_allcpu
      real   (kind=RXP)                                  :: nu_min_allcpu
      real   (kind=RXP)                                  :: nu_max_allcpu
                                                         
      integer(kind=IXP)                                  :: ihexa
      integer(kind=IXP)                                  :: k
      integer(kind=IXP)                                  :: l
      integer(kind=IXP)                                  :: m
      integer(kind=IXP)                                  :: ios

      character(len=CIL)                                 :: info

      ios = init_array_real(vs,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"mod_init_medium:info_medium:vs")
      ios = init_array_real(vp,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"mod_init_medium:info_medium:vp")
      ios = init_array_real(nu,ig_nhexa,IG_NGLL,IG_NGLL,IG_NGLL,"mod_init_medium:info_medium:nu")

!
!---->compute shear-wave velocity

      do ihexa = 1_IXP,ig_nhexa

         do k = 1_IXP,IG_NGLL

            do l = 1_IXP,IG_NGLL

               do m = 1_IXP,IG_NGLL

                  vs(m,l,k,ihexa) = sqrt(rg_hexa_gll_rhovs2(m,l,k,ihexa) / rg_hexa_gll_rho (m,l,k,ihexa) )

               enddo

            enddo

         enddo

      enddo

!
!---->compute pressure-wave velocity

      do ihexa = 1_IXP,ig_nhexa

         do k = 1_IXP,IG_NGLL

            do l = 1_IXP,IG_NGLL

               do m = 1_IXP,IG_NGLL

                  vp(m,l,k,ihexa) = sqrt(rg_hexa_gll_rhovp2(m,l,k,ihexa) / rg_hexa_gll_rho (m,l,k,ihexa) )

               enddo

            enddo

         enddo

      enddo

!
!---->compute Poisson ratio

      do ihexa = 1_IXP,ig_nhexa

         do k = 1_IXP,IG_NGLL

            do l = 1_IXP,IG_NGLL

               do m = 1_IXP,IG_NGLL

                  nu(m,l,k,ihexa) = ( vp(m,l,k,ihexa)**2_IXP - 2.0_RXP*vs(m,l,k,ihexa)**2_IXP ) / ( 2.0_RXP*(vp(m,l,k,ihexa)**2_IXP - vs(m,l,k,ihexa)**2_IXP) )

               enddo

            enddo

         enddo

      enddo



!---->get min/max for cpu myrank

      rho_min = minval(rg_hexa_gll_rho)
      rho_max = maxval(rg_hexa_gll_rho)

      vs_min  = minval(vs)
      vs_max  = maxval(vs)

      vp_min  = minval(vp)
      vp_max  = maxval(vp)

      nu_min  = minval(nu)
      nu_max  = maxval(nu)

!
!---->get min/max over all cpus

      call mpi_allreduce(rho_min,rho_min_allcpu,ONE_IXP,MPI_REAL,MPI_MIN,ig_mpi_comm_simu,ios)
      call mpi_allreduce(rho_max,rho_max_allcpu,ONE_IXP,MPI_REAL,MPI_MAX,ig_mpi_comm_simu,ios)

      call mpi_allreduce( vs_min, vs_min_allcpu,ONE_IXP,MPI_REAL,MPI_MIN,ig_mpi_comm_simu,ios)
      call mpi_allreduce( vs_max, vs_max_allcpu,ONE_IXP,MPI_REAL,MPI_MAX,ig_mpi_comm_simu,ios)

      call mpi_allreduce( vp_min, vp_min_allcpu,ONE_IXP,MPI_REAL,MPI_MIN,ig_mpi_comm_simu,ios)
      call mpi_allreduce( vp_max, vp_max_allcpu,ONE_IXP,MPI_REAL,MPI_MAX,ig_mpi_comm_simu,ios)

      call mpi_allreduce( nu_min, nu_min_allcpu,ONE_IXP,MPI_REAL,MPI_MIN,ig_mpi_comm_simu,ios)
      call mpi_allreduce( nu_max, nu_max_allcpu,ONE_IXP,MPI_REAL,MPI_MAX,ig_mpi_comm_simu,ios)

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(/,a)'            ) "min/max of mechanical properties"
         write(IG_LST_UNIT,'(  a,2(f15.3,1X))')    " --> density                   = ",rho_min_allcpu,rho_max_allcpu
         
         if (LG_VISCO) then

            write(IG_LST_UNIT,'(  a,2(f15.3,1X))') " --> unrelaxed s-wave velocity = ",vs_min_allcpu, vs_max_allcpu
            write(IG_LST_UNIT,'(  a,2(f15.3,1X))') " --> unrelaxed p-wave velocity = ",vp_min_allcpu, vp_max_allcpu
            write(IG_LST_UNIT,'(  a,2(f15.3,1X))') " --> unrelaxed Poisson ratio   = ",nu_min_allcpu, nu_max_allcpu

         else

            write(IG_LST_UNIT,'(  a,2(f15.3,1X))') " --> s-wave velocity           = ",vs_min_allcpu, vs_max_allcpu
            write(IG_LST_UNIT,'(  a,2(f15.3,1X))') " --> p-wave velocity           = ",vp_min_allcpu, vp_max_allcpu
            write(IG_LST_UNIT,'(  a,2(f15.3,1X))') " --> Poisson ratio             = ",nu_min_allcpu, nu_max_allcpu

         endif


      endif

!
!---->abort simulation if abnormal values of mechanical properties

     !if ( (rho_min_allcpu < 0.0_RXP) .or. (vs_min_allcpu < 0.0_RXP) .or. (vp_min_allcpu < 0.0_RXP) .or. (nu_min_allcpu < 0.0_RXP) .or. (nu_max_allcpu >= 0.5_RXP) ) then
      if ( (rho_min_allcpu < 0.0_RXP) .or. (vs_min_allcpu < 0.0_RXP) .or. (vp_min_allcpu < 0.0_RXP) ) then

         write(info,'(a)') "error while checking medium properties"
         call error_stop(info)         

      endif

      return
!***********************************************************************************************************************************************************************************
      end subroutine info_medium
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine allocates arrays related to the tomographic model, reads the model from input files,
!!displays some infos about the model, and finally broadcasts it to all MPI procs.
!***********************************************************************************************************************************************************************************
      subroutine read_tomo()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      error_stop&
                                     ,get_newunit&
                                     ,IG_LST_UNIT&
                                     ,ig_myrank&
                                     ,LG_VISCO&
                                     ,tg_tomo

      implicit none

      integer(kind=IXP)  :: myunit
      integer(kind=IXP)  :: ios
      integer(kind=IXP)  :: ios_glob

      character(len=CIL) :: info

      !read tomo infos
      if (ig_myrank == ZERO_IXP) then

         open(unit=get_newunit(myunit), file=trim(tg_tomo%input_folder)//"/infos.txt", status='old', action='read', iostat=ios)
            read(unit=myunit,fmt=*) tg_tomo%nx, tg_tomo%ny, tg_tomo%nz
            read(unit=myunit,fmt=*) tg_tomo%dx, tg_tomo%dy, tg_tomo%dz
            read(unit=myunit,fmt=*) tg_tomo%x0, tg_tomo%y0, tg_tomo%z0
            if(LG_VISCO) read(unit=myunit,fmt=*) tg_tomo%qf
         close(myunit)

         !print out tomo infos
         write(IG_LST_UNIT,'(a,i5)')      " --> tomography size nx = ",tg_tomo%nx
         write(IG_LST_UNIT,'(a,i5)')      "                     ny = ",tg_tomo%ny
         write(IG_LST_UNIT,'(a,i5)')      "                     nz = ",tg_tomo%nz
         write(IG_LST_UNIT,'(a,f15.7,a)') " --> tomography step dx = ",tg_tomo%dx," m"
         write(IG_LST_UNIT,'(a,f15.7,a)') "                     dy = ",tg_tomo%dy," m"
         write(IG_LST_UNIT,'(a,f15.7,a)') "                     dz = ",tg_tomo%dz," m"
         write(IG_LST_UNIT,'(a,f15.7,a)') " --> tomography origin x0 = ",tg_tomo%x0," m"
         write(IG_LST_UNIT,'(a,f15.7,a)') "                       y0 = ",tg_tomo%y0," m"
         write(IG_LST_UNIT,'(a,f15.7,a)') "                       z0 = ",tg_tomo%z0," m"

      endif

      !broadcast tomo infos to all mpi procs
      call mpi_bcast(tg_tomo%nx, ONE_IXP, MPI_INT, ZERO_IXP, MPI_COMM_WORLD, ios)
      call mpi_bcast(tg_tomo%ny, ONE_IXP, MPI_INT, ZERO_IXP, MPI_COMM_WORLD, ios)
      call mpi_bcast(tg_tomo%nz, ONE_IXP, MPI_INT, ZERO_IXP, MPI_COMM_WORLD, ios)
      call mpi_bcast(tg_tomo%dx, ONE_IXP, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)
      call mpi_bcast(tg_tomo%dy, ONE_IXP, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)
      call mpi_bcast(tg_tomo%dz, ONE_IXP, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)
      call mpi_bcast(tg_tomo%x0, ONE_IXP, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)
      call mpi_bcast(tg_tomo%y0, ONE_IXP, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)
      call mpi_bcast(tg_tomo%z0, ONE_IXP, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)
      if(LG_VISCO) call mpi_bcast(tg_tomo%qf, ONE_IXP, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)

      !allocate tomo arrays
      allocate(tg_tomo%vp(tg_tomo%ny,tg_tomo%nx,tg_tomo%nz))
      allocate(tg_tomo%vs(tg_tomo%ny,tg_tomo%nx,tg_tomo%nz))
      allocate(tg_tomo%rho(tg_tomo%ny,tg_tomo%nx,tg_tomo%nz))
      allocate(tg_tomo%qp(tg_tomo%ny,tg_tomo%nx,tg_tomo%nz))
      allocate(tg_tomo%qs(tg_tomo%ny,tg_tomo%nx,tg_tomo%nz))

      !read tomo files
      if (ig_myrank == ZERO_IXP) then

         ios_glob = 0

         !vp
         open(unit=get_newunit(myunit), file=trim(tg_tomo%input_folder)//"/vp.bin", status='old', action='read', access='stream', form='unformatted', iostat=ios)
            if (ios /= ZERO_IXP) ios_glob = ios
            read(unit=myunit,iostat=ios) tg_tomo%vp(:,:,:)
            if (ios /= ZERO_IXP) ios_glob = ios
         close(myunit)
         !vs
         open(unit=get_newunit(myunit), file=trim(tg_tomo%input_folder)//"/vs.bin", status='old', action='read', access='stream', form='unformatted', iostat=ios)
            if (ios /= ZERO_IXP) ios_glob = ios
            read(unit=myunit,iostat=ios) tg_tomo%vs(:,:,:)
            if (ios /= ZERO_IXP) ios_glob = ios
         close(myunit)
         !rho
         open(unit=get_newunit(myunit), file=trim(tg_tomo%input_folder)//"/rho.bin", status='old', action='read', access='stream', form='unformatted', iostat=ios)
            if (ios /= ZERO_IXP) ios_glob = ios
            read(unit=myunit,iostat=ios) tg_tomo%rho(:,:,:)
            if (ios /= ZERO_IXP) ios_glob = ios
         close(myunit)

         if (LG_VISCO) then
            !qp
            open(unit=get_newunit(myunit), file=trim(tg_tomo%input_folder)//"/qp.bin", status='old', action='read', access='stream', form='unformatted', iostat=ios)
               if (ios /= ZERO_IXP) ios_glob = ios
               read(unit=myunit,iostat=ios) tg_tomo%qp(:,:,:)
               if (ios /= ZERO_IXP) ios_glob = ios
            close(myunit)
            !qs
            open(unit=get_newunit(myunit), file=trim(tg_tomo%input_folder)//"/qs.bin", status='old', action='read', access='stream', form='unformatted', iostat=ios)
               if (ios /= ZERO_IXP) ios_glob = ios
               read(unit=myunit,iostat=ios) tg_tomo%qs(:,:,:)
               if (ios /= ZERO_IXP) ios_glob = ios
            close(myunit)
         end if

         if (ios_glob /= ZERO_IXP) then
            write(info,'(a)') "error in subroutine module_init_medium:read_tomo while opening tomo files"
            call error_stop(info)
         endif

         !print out model infos
         write(IG_LST_UNIT,'(a,f10.3,a,f10.3,a)') " --> min / max s-wave velocity = ",minval(tg_tomo%vs)," / ",maxval(tg_tomo%vs)," m/s"
         write(IG_LST_UNIT,'(a,f10.3,a,f10.3,a)') " --> min / max p-wave velocity = ",minval(tg_tomo%vp)," / ",maxval(tg_tomo%vp)," m/s"
         write(IG_LST_UNIT,'(a,f10.3,a,f10.3,a)') " --> min / max density         = ",minval(tg_tomo%rho)," / ",maxval(tg_tomo%rho)," kg/m3"
         if (LG_VISCO) then
            write(IG_LST_UNIT,'(a,f10.3,a,f10.3)') " --> min / max s-wave quality factor = ",minval(tg_tomo%qs)," / ",maxval(tg_tomo%qs)
            write(IG_LST_UNIT,'(a,f10.3,a,f10.3)') " --> min / max p-wave quality factor = ",minval(tg_tomo%qp)," / ",maxval(tg_tomo%qp)
         endif

      endif   !end if master proc (ig_myrank == 0)

      !broadcast tomo arrays to all mpi procs
      ios_glob = 0
      call mpi_bcast(tg_tomo%vp, tg_tomo%nx*tg_tomo%ny*tg_tomo%nz, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)
      if (ios /= ZERO_IXP) ios_glob = ios
      call mpi_bcast(tg_tomo%vs, tg_tomo%nx*tg_tomo%ny*tg_tomo%nz, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)
      if (ios /= ZERO_IXP) ios_glob = ios
      call mpi_bcast(tg_tomo%rho,tg_tomo%nx*tg_tomo%ny*tg_tomo%nz, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)
      if (ios /= ZERO_IXP) ios_glob = ios
      call mpi_bcast(tg_tomo%qp, tg_tomo%nx*tg_tomo%ny*tg_tomo%nz, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)
      if (ios /= ZERO_IXP) ios_glob = ios
      call mpi_bcast(tg_tomo%qs, tg_tomo%nx*tg_tomo%ny*tg_tomo%nz, MPI_REAL, ZERO_IXP, MPI_COMM_WORLD, ios)
      if (ios /= ZERO_IXP) ios_glob = ios

      if (ios_glob /= ZERO_IXP) then
         write(info,'(a)') "error in subroutine module_init_medium:read_tomo while broadcasting tomo variables"
         call error_stop(info)
      endif

      return
!***********************************************************************************************************************************************************************************
      end subroutine read_tomo
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine performs a 3D trilinear interpolation of a cartesian tomographic model onto at a given GLL node
!***********************************************************************************************************************************************************************************
      subroutine trilinear_interpolation(v_gll, v_in, igll)
!***********************************************************************************************************************************************************************************

! Algorithm taken from
! https://en.wikipedia.org/wiki/Trilinear_interpolation
!
! First we interpolate along x (imagine we are "pushing" the face of the cube defined by C_{0jk}
! to the opposing face, defined by C_{1jk}), giving:
! \begin{align}
!   c_{00} &= c_{000} (1 - x_\text{d}) + c_{100} x_\text{d} \\
!   c_{01} &= c_{001} (1 - x_\text{d}) + c_{101} x_\text{d} \\
!   c_{10} &= c_{010} (1 - x_\text{d}) + c_{110} x_\text{d} \\
!   c_{11} &= c_{011} (1 - x_\text{d}) + c_{111} x_\text{d}
! \end{align}
!
! Where c_{000} means the function value of  (x_0, y_0, z_0).
! Then we interpolate these values (along y, "pushing" from C_{i0k} to C_{i1k}), giving:
! \begin{align}
!   c_0 &= c_{00}(1 - y_\text{d}) + c_{10}y_\text{d} \\
!   c_1 &= c_{01}(1 - y_\text{d}) + c_{11}y_\text{d}
! \end{align}
!
! Finally we interpolate these values along z (walking through a line):
!   c = c_0(1 - z_\text{d}) + c_1z_\text{d}

!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                       rg_gll_coordinate&
                                      ,IG_LST_UNIT&
                                      ,tg_tomo

      implicit none

      real   (kind=RXP), intent(out) :: v_gll
      integer(kind=IXP), intent(in)  :: igll
      real   (kind=RXP), intent(in), dimension(tg_tomo%ny,tg_tomo%nx,tg_tomo%nz) :: v_in

      integer(kind=IXP)              :: nx
      integer(kind=IXP)              :: ny
      integer(kind=IXP)              :: nz
      integer(kind=IXP)              :: ix
      integer(kind=IXP)              :: iy
      integer(kind=IXP)              :: iz

      real   (kind=RXP)              :: x_gll
      real   (kind=RXP)              :: y_gll
      real   (kind=RXP)              :: z_gll
      real   (kind=RXP)              :: dx
      real   (kind=RXP)              :: dy
      real   (kind=RXP)              :: dz
      real   (kind=RXP)              :: x0
      real   (kind=RXP)              :: y0
      real   (kind=RXP)              :: z0
      real   (kind=RXP)              :: xi
      real   (kind=RXP)              :: yi
      real   (kind=RXP)              :: zi
      real   (kind=RXP)              :: wx
      real   (kind=RXP)              :: wy
      real   (kind=RXP)              :: wz
      real   (kind=RXP)              :: c000
      real   (kind=RXP)              :: c001
      real   (kind=RXP)              :: c010
      real   (kind=RXP)              :: c100
      real   (kind=RXP)              :: c011
      real   (kind=RXP)              :: c101
      real   (kind=RXP)              :: c110
      real   (kind=RXP)              :: c111
      real   (kind=RXP)              :: c00
      real   (kind=RXP)              :: c01
      real   (kind=RXP)              :: c10
      real   (kind=RXP)              :: c11
      real   (kind=RXP)              :: c0
      real   (kind=RXP)              :: c1

      !rename some variables to make the following more generic
      nx = tg_tomo%nx
      ny = tg_tomo%ny
      nz = tg_tomo%nz
      dx = tg_tomo%dx
      dy = tg_tomo%dy
      dz = tg_tomo%dz
      x0 = tg_tomo%x0
      y0 = tg_tomo%y0
      z0 = tg_tomo%z0

      !find coordinates of this GLL point
      x_gll = rg_gll_coordinate(1,igll)
      y_gll = rg_gll_coordinate(2,igll)
      z_gll = rg_gll_coordinate(3,igll)

      !find indices and coordinates of the lower-left corner of the voxel that contains this GLL point in the input cartesian tomo
      ix = floor((x_gll-x0)/dx) + 1
      iy = floor((y_gll-y0)/dy) + 1
      iz = floor((z_gll-z0)/dz) + 1

      !!quick and dirty fix in case of rounding errors
      !if(ix==0) ix = 1
      !if(iy==0) iy = 1
      !if(iz==0) iz = 1

      !!restrict indices to tomographic domain (i.e. perform nearest neighbour interpolation outside the domain)
      !if(ix<=0) ix = 1
      !if(iy<=0) iy = 1
      !if(iz<=0) iz = 1
      !if(ix>nx) ix = nx
      !if(iy>ny) iy = ny
      !if(iz>nz) iz = nz

      !xi = x0 + (ix-1)*dx
      !yi = y0 + (iy-1)*dy
      !zi = z0 + (iz-1)*dz

      !!define weights for weighted averages
      !wx = (x_gll - xi) / dx
      !wy = (y_gll - yi) / dy
      !wz = (z_gll - zi) / dz

      ! /!\ FL: the above was assuming that the GLL mesh and the tomo grid have the same origin (x0,y0,z0), /!\
      ! /!\     which not the case anymore (since [x/y]ll_corners are not set to 0.0).                      /!\
      ! /!\ The following takes this into account:                                                          /!\

      !define weights for weighted averages
      if ( (ix >= 1) .and. (ix <= nx) ) then
         xi = x0 + (ix-1)*dx
         wx = (x_gll - xi) / dx
      endif
      if ( (iy >= 1) .and. (iy <= ny) ) then
         yi = y0 + (iy-1)*dy
         wy = (y_gll - yi) / dy
      endif
      if ( (iz >= 1) .and. (iz <= nz) ) then
         zi = z0 + (iz-1)*dz
         wz = (z_gll - zi) / dz
      endif

      !deal with points outside tomographic domain (nearest neighbour interpolation)
      if (ix < 1) then
         ix = 1
         wx = 0.0
      elseif (ix > nx) then
         ix = nx
         wx = 1.0
      endif
      if (iy < 1) then
         iy = 1
         wy = 0.0
      elseif (iy > ny) then
         iy = ny
         wy = 1.0
      endif
      if (iz < 1) then
         iz = 1
         wz = 0.0
      elseif (iz > nz) then
         iz = nz
         wz = 1.0
      endif

      !interpolate (taking care of edges by constant extrapolation)
      c000 = v_in(iy, ix, iz)
      c001 = v_in(iy, ix, min(iz+1,nz))
      c010 = v_in(min(iy+1,ny), ix, iz)
      c100 = v_in(iy, min(ix+1,nx), iz)
      c011 = v_in(min(iy+1,ny), ix, min(iz+1,nz))
      c101 = v_in(iy, min(ix+1,nx), min(iz+1,nz))
      c110 = v_in(min(iy+1,ny), min(ix+1,nx), iz)
      c111 = v_in(min(iy+1,ny), min(ix+1,nx), min(iz+1,nz))

      c00 = c000*(1-wx) + c100*wx
      c01 = c001*(1-wx) + c101*wx
      c10 = c010*(1-wx) + c110*wx
      c11 = c011*(1-wx) + c111*wx

      c0 = c00*(1-wy) + c10*wy
      c1 = c01*(1-wy) + c11*wy

      v_gll = c0*(1-wz) + c1*wz

      return
!***********************************************************************************************************************************************************************************
      end subroutine trilinear_interpolation
!***********************************************************************************************************************************************************************************

end module mod_init_medium

