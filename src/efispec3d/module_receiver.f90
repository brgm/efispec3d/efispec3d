!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to compute information about receivers.

!>@brief
!!This module contains subroutines to compute information about receivers and to write receivers' time history into binary files.
module mod_receiver

   use mpi

   use mod_precision

   use mod_global_variables, only : CIL

   implicit none

   private

   public  :: init_hexa_receiver
   public  :: init_quad_receiver
   public  :: search_closest_hexa_gll
   public  :: search_closest_quad_gll
   public  :: search_closest_quad_centroid
   public  :: compute_info_hexa_receiver
   public  :: compute_info_quad_receiver
   public  :: write_receiver_output
   public  :: locate_point_in_hexa
   public  :: is_point_inside_hexahedron
   public  :: is_point_inside_hexahedron_new
   private :: is_point_inside_polyhedron
   public  :: is_inside_quad
   public  :: is_inside_polygon
   private :: polygon_solid_angle_3d
   private :: polygon_normal_3d
   private :: r4vec_cross_product_3d
   private :: i4_wrap
   private :: i4_modp
   private :: r4vec_norm
   private :: r4_acos
   private :: r4vec_scalar_triple_product
   private :: write_1d_velocity_structure

   contains

!
!
!>@brief
!!This subroutine reads all receivers in file *.vor; determines to which cpu they belong; computes their local coordinates @f$\xi,\eta,\zeta@f$ in hexahedron elements
!!and open receivers' output files in which displacements, velocities and accelerations are written.
!>@return mod_global_variables::tg_receiver_hexa
!>@return mod_global_variables::ig_nreceiver_hexa
!***********************************************************************************************************************************************************************************
   subroutine init_hexa_receiver()
!***********************************************************************************************************************************************************************************
      
      use mpi
      
      use mod_global_variables, only :&
                                       cg_prefix&
                                      ,tg_receiver_hexa&
                                      ,ig_nreceiver_hexa&
                                      ,IG_LST_UNIT&
                                      ,ig_myrank&
                                      ,ig_ncpu&
                                      ,type_receiver_hexa&
                                      ,ig_receiver_saving_incr&
                                      ,LG_OUTPUT_DEBUG_FILE&
                                      ,cg_myrank&
                                      ,ig_hexa_receiver_unit&
                                      ,get_newunit&
                                      ,ig_mpi_comm_simu&
                                      ,error_stop&
                                      ,ig_ndt
      
      implicit none

      real   (kind=RXP), dimension(ig_ncpu)               :: rldum1      
      real   (kind=RXP), dimension(ig_ncpu)               :: maxdmin_all_cpu
      real   (kind=RXP)                                   :: maxdmin
      real   (kind=RXP)                                   :: rldmin
      real   (kind=RXP), parameter                        :: TOO_FAR = ONE_RXP
      
      integer(kind=IXP), dimension(ONE_IXP)               :: min_loc                                                       
      integer(kind=IXP), dimension(ig_ncpu)               :: ilrcpu
      integer(kind=IXP)                                   :: ios
      integer(kind=IXP)                                   :: icpu
      integer(kind=IXP)                                   :: iloc
      integer(kind=IXP)                                   :: irec
      integer(kind=IXP)                                   :: j
      integer(kind=IXP)                                   :: ilnrec
      integer(kind=IXP)                                   :: myunit
                                                          
      character(len=  1)                                  :: ctmp
      character(len=  6)                                  :: crec

      logical(kind=IXP)                                   :: is_inside
      logical(kind=IXP), dimension(ig_ncpu)               :: is_inside_all_cpu

      type(type_receiver_hexa), dimension(:), allocatable :: tlrinf


!
!---->initialize total number of receivers in cpu myrank

      ig_nreceiver_hexa = ZERO_IXP

!
!---->if file *.vor exists, then we read receiver x,y,z location

      open(unit=get_newunit(myunit),file=trim(cg_prefix)//".vor",status='old',iostat=ios)

      if ( (ios == ZERO_IXP) .and. (ig_receiver_saving_incr <= ig_ndt) ) then

         if (ig_myrank == ZERO_IXP) then
            write(IG_LST_UNIT,'(a)') " "
            write(IG_LST_UNIT,'(a)') "information about receivers in file "//trim(cg_prefix)//".vor (more info available in files "//trim(cg_prefix)//".vor.info.cpu.xxxxxx)"
         endif

         do while (.true.)

            read(unit=myunit,fmt=*,iostat=ios) ctmp
            if (ios /= ZERO_IXP) exit
            ig_nreceiver_hexa = ig_nreceiver_hexa + ONE_IXP

         enddo

         rewind(myunit)

         allocate(tlrinf(ig_nreceiver_hexa))

         ilnrec = ZERO_IXP

         do irec = ONE_IXP,ig_nreceiver_hexa

!
!---------->initialize tlrinf

            tlrinf(irec)%x          = ZERO_RXP
            tlrinf(irec)%y          = ZERO_RXP
            tlrinf(irec)%z          = ZERO_RXP
            tlrinf(irec)%xi         = ZERO_RXP
            tlrinf(irec)%eta        = ZERO_RXP
            tlrinf(irec)%zeta       = ZERO_RXP
            tlrinf(irec)%dmin       = huge(rldmin)
            tlrinf(irec)%lag(:,:,:) = ZERO_RXP
            tlrinf(irec)%gll(:,:,:) = ZERO_IXP
            tlrinf(irec)%cpu        = -ONE_IXP
            tlrinf(irec)%iel        = ZERO_IXP
            tlrinf(irec)%kgll       = ZERO_IXP
            tlrinf(irec)%lgll       = ZERO_IXP
            tlrinf(irec)%mgll       = ZERO_IXP
            tlrinf(irec)%rglo       = ZERO_IXP
            is_inside               = .false.

            read(unit=myunit,fmt=*) tlrinf(irec)%x,tlrinf(irec)%y,tlrinf(irec)%z

!
!---------->find which hexahedron element contains the receiver irec

           !call is_point_inside_hexahedron    (tlrinf(irec)%x,tlrinf(irec)%y,tlrinf(irec)%z,tlrinf(irec)%dmin,tlrinf(irec)%kgll,tlrinf(irec)%lgll,tlrinf(irec)%mgll,tlrinf(irec)%iel,is_inside)
            call is_point_inside_hexahedron_new(tlrinf(irec)%x,tlrinf(irec)%y,tlrinf(irec)%z,tlrinf(irec)%dmin,tlrinf(irec)%kgll,tlrinf(irec)%lgll,tlrinf(irec)%mgll,tlrinf(irec)%iel,is_inside)

!
!---------->cpu0 gather all 'is_inside' variable to check:
!              1) if the receiver is inside a hexahedron element. if not, then switch to the search technique 'search_closest_hexa_gll'
!              2) if the receiver is found by multiple cpus. if yes, then affect the receiver to only one cpu

            call mpi_gather(is_inside,ONE_IXP,MPI_LOGICAL,is_inside_all_cpu,ONE_IXP,MPI_LOGICAL,ZERO_IXP,ig_mpi_comm_simu,ios)

            if (ig_myrank == ZERO_IXP) then
!        
!------------->search the first cpu that contains the receiver

               iloc = -ONE_IXP

               do icpu = ONE_IXP,ig_ncpu
             
                  if (is_inside_all_cpu(icpu)) then
                     iloc = icpu
                     exit
                  endif
             
               enddo
         
!
!------------->receiver has been found by the a cpu

               if (iloc >= ONE_IXP) then
         
                  do icpu = iloc+ONE_IXP,ig_ncpu

                     is_inside_all_cpu(icpu) = .false.

                  enddo

               endif
         
            endif
!
!---------->cpu0 informs all cpus about the result of the search of the receiver 'irec' (iloc = -1: irec not found | iloc>=1: irec found)

            call mpi_bcast(iloc,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)
         
!
!---------->if among all cpus, subroutine 'is_point_inside_hexahedron' has not found an hexa element containing the receiver 'irec' --> switch to subroutine 'search_closest_hexa_gll'

            if (iloc == -ONE_IXP) then

               write(IG_LST_UNIT,'(a,I6,3(F10.3,1X))') "warning: alternative search for receiver ",irec,tlrinf(irec)%x,tlrinf(irec)%y,tlrinf(irec)%z

!------------->find the closest element and gll point to the receiver irec

               call search_closest_hexa_gll(tlrinf(irec)%x   ,tlrinf(irec)%y ,tlrinf(irec)%z   ,tlrinf(irec)%dmin&
                                           ,tlrinf(irec)%kgll,tlrinf(irec)%lgll,tlrinf(irec)%mgll,tlrinf(irec)%iel)
!             
!------------->check to which cpu the receiver belongs (i.e., cpu which has the smallest dmin)

               call mpi_allgather(tlrinf(irec)%dmin,ONE_IXP,MPI_REAL,rldum1,ONE_IXP,MPI_REAL,ig_mpi_comm_simu,ios)

               min_loc = minloc(rldum1(ONE_IXP:ig_ncpu))
              
               if (min_loc(ONE_IXP) == ig_myrank+ONE_IXP) then

                  tlrinf(irec)%cpu  = ig_myrank + ONE_IXP
                  ilnrec            = ilnrec    + ONE_IXP

               else

                  tlrinf(irec)%cpu = min_loc(ONE_IXP)

               endif

!
!---------->the receiver 'irec' has been found inside an hexa element of a cpu
            else
         
!        
!------------->cpu0 scatter array is_inside_all_cpu. The cpu that obtain the value is_inside = .true. will compute the receiver irec

               call mpi_scatter(is_inside_all_cpu,ONE_IXP,MPI_LOGICAL,is_inside,ONE_IXP,MPI_LOGICAL,ZERO_IXP,ig_mpi_comm_simu,ios)
        
               if (is_inside) then
            
                  tlrinf(irec)%cpu  = ig_myrank + ONE_IXP
                  ilnrec            = ilnrec    + ONE_IXP
            
               endif

            endif

         enddo

         close(myunit)

!
!------->transfer array tlrinf which knows all the receivers to array tg_receiver_hexa that needs only to know the receiver of cpu myrank

         if (ilnrec > ZERO_IXP) then

            allocate(tg_receiver_hexa(ilnrec))

            j = ZERO_IXP
            do irec = ONE_IXP,ig_nreceiver_hexa
               if (tlrinf(irec)%cpu == ig_myrank+ONE_IXP) then
                  j = j + ONE_IXP
                  tg_receiver_hexa(j)      = tlrinf(irec)
                  tg_receiver_hexa(j)%rglo = irec
               endif
            enddo

            ig_nreceiver_hexa = ilnrec

         else

            ig_nreceiver_hexa = ZERO_IXP

         endif

         deallocate(tlrinf)

!
!------->cpu 0 gather the number of receiver (ig_nreceiver_hexa) of the other cpus

         call mpi_gather(ig_nreceiver_hexa,ONE_IXP,MPI_INTEGER,ilrcpu,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!------->compute information for receivers that belong to cpu myrank and send 'dmin' to cpu 0

         do irec = ONE_IXP,ig_nreceiver_hexa

            call compute_info_hexa_receiver(tg_receiver_hexa(irec))

         enddo

!
!------->all cpus compute their maximum dmin

         if (ig_nreceiver_hexa > ZERO_IXP) then

            maxdmin = -huge(maxdmin)

            do irec = ONE_IXP,ig_nreceiver_hexa

               maxdmin = max(maxdmin,tg_receiver_hexa(irec)%dmin)

            enddo

         else

            maxdmin = ZERO_RXP

         endif

!
!------->cpu 0 gathers dmin of all cpus and write the maximum dmin in *.lst

         call mpi_gather(maxdmin,ONE_IXP,MPI_REAL,maxdmin_all_cpu,ONE_IXP,MPI_REAL,ZERO_IXP,ig_mpi_comm_simu,ios)

         if (ig_myrank == ZERO_IXP) then

            write(IG_LST_UNIT,'(a,e14.7)') " --> maximum localisation error of all receivers inside volume of computation = ",maxval(maxdmin_all_cpu)

            if (maxval(maxdmin_all_cpu) >= TOO_FAR) then

               write(unit=IG_LST_UNIT,fmt='(a)') " --> WARNING: some receivers may be located outside of the domain of computation"

            endif

            call flush(IG_LST_UNIT)

         endif

!
!------->open receivers file once and for all

         if ( ig_nreceiver_hexa > ZERO_IXP ) then

            allocate(ig_hexa_receiver_unit(ig_nreceiver_hexa))

            do irec = ONE_IXP,ig_nreceiver_hexa

               write(crec,'(i6.6)') tg_receiver_hexa(irec)%rglo

                open(unit       = get_newunit(ig_hexa_receiver_unit(irec))&
                    ,file       = trim(cg_prefix)//".vor."//trim(adjustl(crec))//".gpl"&
                    ,status     = 'replace'&
                    ,action     = 'write'&
                    ,access     = 'stream'&
                    ,form       = 'unformatted')

            enddo

!
!---------->output receiver info for each CPU

            open(unit=get_newunit(myunit),file=trim(cg_prefix)//".vor.info.cpu."//trim(cg_myrank))

            do irec = ONE_IXP,ig_nreceiver_hexa

               write(unit=myunit,fmt='(/,a,i10    )') "Receiver     ",tg_receiver_hexa(irec)%rglo
               if (tg_receiver_hexa(irec)%dmin >= TOO_FAR) then
                  write(unit=myunit,fmt='(a)') "WARNING: this receiver may be located outside of the domain of computation"
               endif
               write(unit=myunit,fmt='( a,i10     )') "    in core  ",tg_receiver_hexa(irec)%cpu - ONE_IXP
               write(unit=myunit,fmt='( a,i10     )') "    in hexa  ",tg_receiver_hexa(irec)%iel
               write(unit=myunit,fmt='(        3a )') "   X found     ","   Y found     ","   Z found     "
               write(unit=myunit,fmt='(3(e14.7,1x))') tg_receiver_hexa(irec)%x,tg_receiver_hexa(irec)%y,tg_receiver_hexa(irec)%z
               write(unit=myunit,fmt='(        4a )') "      XI       ","      ETA      ","     ZETA      ","     DISTANCE     "
               write(unit=myunit,fmt='(4(e14.7,1x))') tg_receiver_hexa(irec)%xi,tg_receiver_hexa(irec)%eta,tg_receiver_hexa(irec)%zeta,tg_receiver_hexa(irec)%dmin

            enddo

            close(myunit)

         endif

      else

         if (ig_myrank == ZERO_IXP) then
            write(IG_LST_UNIT,'(/a)') "no volume receiver computed"
            call flush(IG_LST_UNIT)
         endif

      endif
      
      return
!***********************************************************************************************************************************************************************************
   end subroutine init_hexa_receiver
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine reads all receivers in file *.fsr; determines to which cpu they belong; computes their local coordinates @f$\xi,\eta@f$ in quadrangle elements
!!and open receivers' output files in which displacements, velocities and accelerations are written.
!>@return mod_global_variables::tg_receiver_quad
!>@return mod_global_variables::ig_nreceiver_quad
!***********************************************************************************************************************************************************************************
   subroutine init_quad_receiver()
!***********************************************************************************************************************************************************************************
      
      use mpi
      
      use mod_global_variables, only :&
                                       cg_prefix&
                                      ,error_stop&
                                      ,lg_receiver_1d_velocity_structure&
                                      ,rg_receiver_1D_velocity_structure_dz&
                                      ,tg_receiver_quad&
                                      ,ig_nquad_fsurf&
                                      ,ig_quad_nnode&
                                      ,ig_quadf_gnode_glonum&
                                      ,ig_nreceiver_quad&
                                      ,IG_LST_UNIT&
                                      ,IG_NDOF&
                                      ,ig_myrank&
                                      ,ig_ncpu&
                                      ,type_receiver_quad&
                                      ,ig_receiver_saving_incr&
                                      ,LG_OUTPUT_DEBUG_FILE&
                                      ,LG_FIR_FILTER&
                                      ,cg_myrank&
                                      ,ig_quad_receiver_unit&
                                      ,ig_mpi_comm_simu&
                                      ,get_newunit&
                                      ,ig_ndt

      use mod_init_memory

      implicit none

      real   (kind=RXP)  , dimension(ig_ncpu)             :: maxdmin_all_cpu
      real   (kind=RXP)                                   :: maxdmin
      real   (kind=RXP)                                   :: rldmin
      real   (kind=RXP)                                   :: xrec
      real   (kind=RXP)                                   :: yrec
      real   (kind=RXP)                                   :: zrec
                                                          
      integer(kind=IXP), dimension(ig_ncpu)               :: ilrcpu
      integer(kind=IXP)                                   :: ios
      integer(kind=IXP)                                   :: icpu
      integer(kind=IXP)                                   :: iloc
      integer(kind=IXP)                                   :: irec
      integer(kind=IXP)                                   :: j
      integer(kind=IXP)                                   :: ilnrec
      integer(kind=IXP)                                   :: myunit
                                                          
      character(len=  1)                                  :: ctmp
      character(len=  6)                                  :: crec
      character(len=CIL)                                  :: info

      logical(kind=IXP)                                   :: is_inside
      logical(kind=IXP), dimension(ig_ncpu)               :: is_inside_all_cpu

      type(type_receiver_quad), allocatable, dimension(:) :: tlrinf

!
!---->initialize total number of receivers in cpu myrank

      ig_nreceiver_quad = ZERO_IXP
!
!
!---->if file *receiver exists, then we read receiver x,y,z location

      open(unit=get_newunit(myunit),file=trim(cg_prefix)//".fsr",status='old',iostat=ios)

      if( (ios == ZERO_IXP) .and. (ig_receiver_saving_incr <= ig_ndt) ) then

         if (ig_myrank == ZERO_IXP) then
            write(IG_LST_UNIT,'(a)') " "
            write(IG_LST_UNIT,'(a)') "information about receivers in file "//trim(cg_prefix)//".fsr (more info available in files "//trim(cg_prefix)//".fsr.info.cpu.xxxxxx)"
         endif

         do while (.true.)
            read(unit=myunit,fmt=*,iostat=ios) ctmp
            if (ios /= ZERO_IXP) exit
            ig_nreceiver_quad = ig_nreceiver_quad + ONE_IXP
         enddo

         rewind(myunit)

         allocate(tlrinf(ig_nreceiver_quad))
         ilnrec = ZERO_IXP

         do irec = ONE_IXP,ig_nreceiver_quad
         
!        
!---------->initialize tlrinf

            tlrinf(irec)%x        = ZERO_RXP
            tlrinf(irec)%y        = ZERO_RXP
            tlrinf(irec)%z        = ZERO_RXP
            tlrinf(irec)%xi       = ZERO_RXP
            tlrinf(irec)%eta      = ZERO_RXP
            tlrinf(irec)%dmin     = huge(rldmin)
            tlrinf(irec)%lag(:,:) = ZERO_RXP
            tlrinf(irec)%gll(:,:) = ZERO_IXP
            tlrinf(irec)%cpu      = -ONE_IXP
            tlrinf(irec)%iel      = ZERO_IXP
            tlrinf(irec)%lgll     = ZERO_IXP
            tlrinf(irec)%mgll     = ZERO_IXP
            tlrinf(irec)%rglo     = ZERO_IXP
         
            read(unit=myunit,fmt=*) tlrinf(irec)%x,tlrinf(irec)%y
         
!        
!---------->find which quadrangle element contains the receiver irec

            if (ig_nquad_fsurf > ZERO_IXP) then

               call is_inside_quad(tlrinf(irec)%x,tlrinf(irec)%y,ig_quadf_gnode_glonum,ig_nquad_fsurf,ig_quad_nnode,tlrinf(irec)%dmin,tlrinf(irec)%lgll,tlrinf(irec)%mgll,tlrinf(irec)%iel,is_inside)

            else

               is_inside = .false.

            endif

!
!---------->cpu0 gather all is_inside to check:
!              1) if the receiver is inside a quadrangle element: if no then abort computation
!              2) if the receiver is found by multiple cpus: if yes then affect the receiver to only one cpu

            call mpi_gather(is_inside,ONE_IXP,MPI_LOGICAL,is_inside_all_cpu,ONE_IXP,MPI_LOGICAL,ZERO_IXP,ig_mpi_comm_simu,ios)
         
            if (ig_myrank == ZERO_IXP) then

!        
!------------->search the first cpu that contains the receiver

               iloc = -ONE_IXP

               do icpu = ONE_IXP,ig_ncpu
             
                  if (is_inside_all_cpu(icpu)) then
                     iloc = icpu
                     exit
                  endif
             
               enddo
         
!        
!------------->if the receiver is not inside any quadrangle element among all the cpus --> abort computation

               if (iloc == -ONE_IXP) then
         
                  write(info,'(a)') "error in subroutine init_quad_receiver: receiver not found over all cpus"
                  call error_stop(info)
         
               else
         
                  do icpu = iloc+ONE_IXP,ig_ncpu
                     is_inside_all_cpu(icpu) = .false.
                  enddo
         
               endif
         
            endif
         
!        
!---------->cpu0 scatter array is_inside_all_cpu. The cpu that obtain the value is_inside = .true. will compute the receiver irec

            call mpi_scatter(is_inside_all_cpu,ONE_IXP,MPI_LOGICAL,is_inside,ONE_IXP,MPI_LOGICAL,ZERO_IXP,ig_mpi_comm_simu,ios)

            if (is_inside) then
         
               tlrinf(irec)%cpu  = ig_myrank + ONE_IXP
               ilnrec            = ilnrec    + ONE_IXP
         
            endif
         
         enddo

         close(myunit)

!
!------->transfer array tlrinf which knows all the receivers to array tg_receiver_quad that needs only to know the receiver of cpu myrank

         if (ilnrec > ZERO_IXP) then

            allocate(tg_receiver_quad(ilnrec))

            j = ZERO_IXP

            do irec = ONE_IXP,ig_nreceiver_quad
               if (tlrinf(irec)%cpu == ig_myrank+ONE_IXP) then
                  j = j + ONE_IXP
                  tg_receiver_quad(j)      = tlrinf(irec)
                  tg_receiver_quad(j)%rglo = irec
               endif
            enddo

            ig_nreceiver_quad = ilnrec

         else

            ig_nreceiver_quad = ZERO_IXP

         endif

         deallocate(tlrinf)
!
!------->cpu 0 gather the number of receiver (ig_nreceiver_quad) of the other cpus

         call mpi_gather(ig_nreceiver_quad,ONE_IXP,MPI_INTEGER,ilrcpu,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!------->compute information for receivers that belongs to cpu myrank and send 'dmin' to cpu 0

         do irec = ONE_IXP,ig_nreceiver_quad

            call compute_info_quad_receiver(tg_receiver_quad(irec))

         enddo

!
!------->all cpus compute their maximum dmin

         if (ig_nreceiver_quad > ZERO_IXP) then

            maxdmin = -huge(maxdmin)

            do irec = ONE_IXP,ig_nreceiver_quad
               maxdmin = max(maxdmin,tg_receiver_quad(irec)%dmin)
            enddo

         else

            maxdmin = ZERO_RXP

         endif

!
!------->cpu 0 gathers dmin of all cpus and write the maximum dmin in *.lst

         call mpi_gather(maxdmin,ONE_IXP,MPI_REAL,maxdmin_all_cpu,ONE_IXP,MPI_REAL,ZERO_IXP,ig_mpi_comm_simu,ios)

         if (ig_myrank == ZERO_IXP) then

            write(IG_LST_UNIT,'(a,e14.7)') " --> maximum localisation error of all receivers in free surface = ",maxval(maxdmin_all_cpu)
            call flush(IG_LST_UNIT)

         endif

!
!------->open receivers file once and for all

         if ( ig_nreceiver_quad > ZERO_IXP ) then

            ios = init_array_int(ig_quad_receiver_unit,ig_nreceiver_quad,"ig_quad_receiver_unit")

            do irec = ONE_IXP,ig_nreceiver_quad

               write(crec,'(i6.6)') tg_receiver_quad(irec)%rglo

               open(unit       = get_newunit(ig_quad_receiver_unit(irec))&
                   ,file       = trim(cg_prefix)//".fsr."//trim(adjustl(crec))//".gpl"&
                   ,status     = 'replace'&
                   ,action     = 'write'&
                   ,access     = 'stream'&
                   ,form       = 'unformatted')

            enddo

!
!---------->output receiver info for each CPU

            open(unit=get_newunit(myunit),file=trim(cg_prefix)//".fsr.info.cpu."//trim(cg_myrank))

            do irec = ONE_IXP,ig_nreceiver_quad
               write(unit=myunit,fmt='(/,a,i10    )') "Receiver     ",tg_receiver_quad(irec)%rglo
               write(unit=myunit,fmt='( a,i10     )') "    in core  ",tg_receiver_quad(irec)%cpu-ONE_IXP
               write(unit=myunit,fmt='( a,i10     )') "    in quad  ",tg_receiver_quad(irec)%iel
               write(unit=myunit,fmt='(        2a )') "   X found     ","   Y found     "
               write(unit=myunit,fmt='(2(e14.7,1x))') tg_receiver_quad(irec)%x,tg_receiver_quad(irec)%y
               write(unit=myunit,fmt='(        3a )') "      XI       ","      ETA      ","   DISTANCE    "
               write(unit=myunit,fmt='(3(e14.7,1x))') tg_receiver_quad(irec)%xi,tg_receiver_quad(irec)%eta,tg_receiver_quad(irec)%dmin
            enddo

            close(myunit)

         endif

!
!------->output 1D velocity structure at receiver location

         if (lg_receiver_1d_velocity_structure) then

            irec = 1_IXP

            open(unit=get_newunit(myunit),file=trim(cg_prefix)//".fsr",status='old',iostat=ios)

            do 

               read(unit=myunit,fmt=*,iostat=ios) xrec,yrec

               if (ios /= ZERO_IXP) exit

!------------->cpu holding receiver irec sends z-coordinate to other cpus

               icpu = -1_IXP

               do j = 1_IXP,ig_nreceiver_quad

                  if (tg_receiver_quad(j)%rglo == irec) then

                     zrec = tg_receiver_quad(j)%z

                     icpu = tg_receiver_quad(j)%cpu - ONE_IXP

                     exit

                  endif

               enddo

!------------->all cpus gather icpu to know which cpu will sends zrec

               call mpi_allgather(icpu,1_IXP,MPI_INTEGER,ilrcpu,1_IXP,MPI_INTEGER,ig_mpi_comm_simu,ios)

!------------->cpu maxval(ilrcpu) sends zrec

               call mpi_bcast(zrec,1_IXP,MPI_REAL,maxval(ilrcpu),ig_mpi_comm_simu,ios)

               call write_1d_velocity_structure(irec,xrec,yrec,zrec,rg_receiver_1D_velocity_structure_dz)

               irec = irec + 1_IXP

            enddo

            close(myunit)

         endif

      else

         if (ig_myrank == ZERO_IXP) then
            write(IG_LST_UNIT,'(/a)') "no free surface receiver computed"
            call flush(IG_LST_UNIT)
         endif

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine init_quad_receiver
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine searches among all hexahedron elements in cpu myrank the closest GLL node to a point of coordinates @f$x,y,z@f$
!>@param x    : @f$x@f$-coordinates of a point
!>@param y    : @f$y@f$-coordinates of a point
!>@param z    : @f$z@f$-coordinates of a point
!>@param dmin : minimal Euclidean distance from point to all GLL nodes in cpu myrank
!>@param kgll : closest GLL node index along @f$\zeta@f$-coordinate 
!>@param lgll : closest GLL node index along @f$\eta @f$-coordinate
!>@param mgll : closest GLL node index along @f$\xi  @f$-coordinate
!>@param iel  : number of hexahedron element in cpu myrank whose GLL node has the minimal dmin
!***********************************************************************************************************************************************************************************
   subroutine search_closest_hexa_gll(x,y,z,dmin,kgll,lgll,mgll,iel)
!***********************************************************************************************************************************************************************************
   
      use mpi
   
      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,rg_gll_coordinate&
                                     ,ig_nhexa&
                                     ,ig_hexa_gll_glonum
      
      implicit none
      
      real   (kind=RXP), intent(in)  :: x
      real   (kind=RXP), intent(in)  :: y
      real   (kind=RXP), intent(in)  :: z
      real   (kind=RXP), intent(out) :: dmin
      integer(kind=IXP), intent(out) :: kgll
      integer(kind=IXP), intent(out) :: lgll
      integer(kind=IXP), intent(out) :: mgll
      integer(kind=IXP), intent(out) :: iel
      
      real   (kind=RXP)              :: d
      integer(kind=IXP)              :: ihexa
      integer(kind=IXP)              :: k
      integer(kind=IXP)              :: l
      integer(kind=IXP)              :: m
      integer(kind=IXP)              :: igll

      dmin = huge(dmin)

      do ihexa = ONE_IXP,ig_nhexa

         do k = ONE_IXP,IG_NGLL
            do l = ONE_IXP,IG_NGLL
               do m = ONE_IXP,IG_NGLL

                  igll = ig_hexa_gll_glonum(m,l,k,ihexa)

                  d = sqrt( (rg_gll_coordinate(ONE_IXP  ,igll) - x)**TWO_IXP &
                           +(rg_gll_coordinate(TWO_IXP  ,igll) - y)**TWO_IXP &
                           +(rg_gll_coordinate(THREE_IXP,igll) - z)**TWO_IXP )

                  if (d < dmin) then
                     dmin = d
                     kgll = k
                     lgll = l
                     mgll = m
                     iel  = ihexa
                  endif

               enddo
            enddo
         enddo

      enddo
      
      return
!***********************************************************************************************************************************************************************************
   end subroutine search_closest_hexa_gll
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine searches among all free surface quadrangle elements in cpu myrank the closest GLL node to a point of coordinates @f$x,y@f$
!>@param x          : @f$x@f$-coordinates of a point
!>@param y          : @f$y@f$-coordinates of a point
!>@param global_gll : indirection from local GLL nodes to global GLL nodes for quadrangle elements
!>@param dmin       : minimal Euclidean distance from point to all GLL nodes in cpu myrank
!>@param lgll       : closest GLL node index along @f$\eta @f$-coordinate
!>@param mgll       : closest GLL node index along @f$\xi  @f$-coordinate
!>@param iel        : number of free surface quadrangle element in cpu myrank whose GLL node has the minimal dmin
!***********************************************************************************************************************************************************************************
   subroutine search_closest_quad_gll(x,y,global_gll,dmin,lgll,mgll,iel)
!***********************************************************************************************************************************************************************************
   
      use mpi
   
      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,rg_gll_coordinate
      
      implicit none
      
      real   (kind=RXP), intent(in)                   :: x
      real   (kind=RXP), intent(in)                   :: y
      integer(kind=IXP), intent(in), dimension(:,:,:) :: global_gll

      real   (kind=RXP), intent(out)                  :: dmin
      integer(kind=IXP), intent(out)                  :: lgll
      integer(kind=IXP), intent(out)                  :: mgll
      integer(kind=IXP), intent(out)                  :: iel
                                            
      real   (kind=RXP)                               :: d
                                                     
      integer(kind=IXP)                               :: iquad
      integer(kind=IXP)                               :: l
      integer(kind=IXP)                               :: m
      integer(kind=IXP)                               :: nquad
      integer(kind=IXP)                               :: igll

      dmin  = huge(dmin)
      nquad = size(global_gll,THREE_IXP)

      do iquad = ONE_IXP,nquad

         do l = ONE_IXP,IG_NGLL

            do m = ONE_IXP,IG_NGLL

               igll  = global_gll(m,l,iquad)
               d     = sqrt( (rg_gll_coordinate(ONE_IXP,igll) - x)**TWO_IXP + (rg_gll_coordinate(TWO_IXP,igll) - y)**TWO_IXP )

               if (d < dmin) then
                  dmin = d
                  lgll = l
                  mgll = m
                  iel  = iquad
               endif

            enddo

         enddo

      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine search_closest_quad_gll
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine searches among all free surface quadrangle elements in cpu myrank the 'nc' closest centroid to a point of coordinates @f$x,y@f$
!>@param x             : @f$x@f$-coordinates of a point
!>@param y             : @f$y@f$-coordinates of a point
!>@param quad_gnode    : indirection from local geometric nodes to global geometric nodes for quadrangle elements
!>@param nc            : number of closest quad saved
!>@return closest_quad : indirection from local geometric nodes to global geometric nodes for the 'nc' closest quadrangle elements 
!***********************************************************************************************************************************************************************************
   subroutine search_closest_quad_centroid(x,y,quad_gnode,quad_cxy,nc,closest_quad)
!***********************************************************************************************************************************************************************************
   
      use mpi

      implicit none
      
      real   (kind=RXP), intent( in)                 :: x
      real   (kind=RXP), intent( in)                 :: y
      integer(kind=IXP), intent( in)                 :: nc
      integer(kind=IXP), intent( in), dimension(:,:) :: quad_gnode
      real   (kind=RXP), intent( in), dimension(:,:) :: quad_cxy
      integer(kind=IXP), intent(out), dimension(:,:) :: closest_quad
                                           
      real   (kind=RXP)                              :: d
      real   (kind=RXP)             , dimension(nc)  :: dmin
      integer(kind=IXP)             , dimension(nc)  :: quad_num
      real   (kind=RXP)                              :: cx
      real   (kind=RXP)                              :: cy
                                           
      integer(kind=IXP)                              :: iquad
      integer(kind=IXP)                              :: inode
      integer(kind=IXP)                              :: ic
      integer(kind=IXP)                              :: i
      integer(kind=IXP)                              :: nquad
      integer(kind=IXP)                              :: nnode

      dmin(:)     = huge(dmin)
      quad_num(:) = ZERO_IXP
      nquad       = size(quad_gnode,TWO_IXP)
      nnode       = size(quad_gnode,ONE_IXP)

      do iquad = ONE_IXP,nquad

         cx = quad_cxy(ONE_IXP,iquad) 
         cy = quad_cxy(TWO_IXP,iquad) 

         d  = sqrt( (x-cx)**TWO_IXP + (y-cy)**TWO_IXP )

!
!------->check if current iquad is among the closest quad to point x,y

         ic = nc

         do while ( (d < dmin(ic)) )

            ic = ic - ONE_IXP

            if (ic == ZERO_IXP) exit

         enddo

         ic = ic + ONE_IXP

         if (ic <= nc) then

!
!---------->shift previous closest

            do i = nc,ic+ONE_IXP,-ONE_IXP
          
               dmin(i)     = dmin(i-ONE_IXP)
               quad_num(i) = quad_num(i-ONE_IXP)
          
            enddo

!         
!---------->update closest

            dmin(ic)     = d
            quad_num(ic) = iquad

         endif

      enddo

!
!---->fill in closest_quad with the closest quad

      do ic = ONE_IXP,nc

         iquad = quad_num(ic)

         do inode = ONE_IXP,nnode

            closest_quad(inode,ic) = quad_gnode(inode,iquad)

         enddo

      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine search_closest_quad_centroid
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine computes local coordinates @f$\xi,\eta,\zeta@f$ of a receiver in the hexahedron element it belongs.
!!It also stores global GLL nodes numbers for future interpolations as well as Lagrange polynomial values at the receiver.
!>@param tlxinf : data structure for a receiver located inside a hexahedron element
!***********************************************************************************************************************************************************************************
   subroutine compute_info_hexa_receiver(tlxinf)
!***********************************************************************************************************************************************************************************
   
      use mpi
   
      use mod_global_variables, only :&
                                      rg_gll_abscissa&
                                     ,IG_NGLL&
                                     ,ig_hexa_gll_glonum&
                                     ,type_receiver_hexa

      use mod_jacobian        , only : compute_hexa_jacobian 

      use mod_lagrange        , only : lagrap
      
      use mod_coordinate      , only : compute_hexa_point_coord
      
      implicit none
      
      type(type_receiver_hexa), intent(inout) :: tlxinf

      integer(kind=IXP), parameter            :: ITER_MAX = 100_IXP

      real   (kind=RXP)                       :: eps
      real   (kind=RXP)                       :: dmin
      real   (kind=RXP)                       :: xisol
      real   (kind=RXP)                       :: etsol
      real   (kind=RXP)                       :: zesol
      real   (kind=RXP)                       :: newx
      real   (kind=RXP)                       :: newy
      real   (kind=RXP)                       :: newz
      real   (kind=RXP)                       :: dxidx
      real   (kind=RXP)                       :: dxidy
      real   (kind=RXP)                       :: dxidz
      real   (kind=RXP)                       :: detdx
      real   (kind=RXP)                       :: detdy
      real   (kind=RXP)                       :: detdz
      real   (kind=RXP)                       :: dzedx
      real   (kind=RXP)                       :: dzedy
      real   (kind=RXP)                       :: dzedz
      real   (kind=RXP)                       :: dx
      real   (kind=RXP)                       :: dy
      real   (kind=RXP)                       :: dz
      real   (kind=RXP)                       :: dxi
      real   (kind=RXP)                       :: det
      real   (kind=RXP)                       :: dze
      real   (kind=RXP)                       :: deter
                                                                     
      integer(kind=IXP)                       :: ihexa 
      integer(kind=IXP)                       :: igll
      integer(kind=IXP)                       :: k
      integer(kind=IXP)                       :: l
      integer(kind=IXP)                       :: m
      integer(kind=IXP)                       :: iter

!
!---->initialize element number and coordinates (needed if 'dmin < eps' because the do while loop will not be performed)

      ihexa= tlxinf%iel
      newx = tlxinf%x
      newy = tlxinf%y
      newz = tlxinf%z
      eps  = 10.0_RXP*EPSILON_MACHINE_RXP
      iter = ZERO_IXP

!
!---->set the first guessed solution

      xisol = rg_gll_abscissa(tlxinf%mgll)
      etsol = rg_gll_abscissa(tlxinf%lgll)
      zesol = rg_gll_abscissa(tlxinf%kgll)
      dmin  = tlxinf%dmin

!
!---->solve the nonlinear system to find local coordinates

      do while(dmin > eps)

!
!------->update tlxinf%xi,eta,zeta with the guessed solution xisol, etsol and zesol

         tlxinf%xi   = xisol
         tlxinf%eta  = etsol
         tlxinf%zeta = zesol

         call compute_hexa_jacobian(ihexa,xisol,etsol,zesol,dxidx,dxidy,dxidz,detdx,detdy,detdz,dzedx,dzedy,dzedz,deter)

         call compute_hexa_point_coord(ihexa,xisol,etsol,zesol,newx,newy,newz)

         dmin  = sqrt( (newx-tlxinf%x)**TWO_IXP + (newy-tlxinf%y)**TWO_IXP + (newz-tlxinf%z)**TWO_IXP )

!
!------->prepare next iteration by keeping local solution within interval [-1:+1]

         iter  = iter + ONE_IXP

         dx    = tlxinf%x - newx
         dy    = tlxinf%y - newy
         dz    = tlxinf%z - newz

         dxi   = dxidx*dx + dxidy*dy + dxidz*dz
         det   = detdx*dx + detdy*dy + detdz*dz
         dze   = dzedx*dx + dzedy*dy + dzedz*dz

         xisol = xisol + dxi
         etsol = etsol + det
         zesol = zesol + dze

!
!------->bound solution within the elementary element, otherwise if the receiver is outside every hexa element 'dmin' would be wrong (|xxsol| > 1.0) and the interpolation would be wrong

         if (xisol > +ONE_RXP) xisol = +ONE_RXP
         if (xisol < -ONE_RXP) xisol = -ONE_RXP
         if (etsol > +ONE_RXP) etsol = +ONE_RXP
         if (etsol < -ONE_RXP) etsol = -ONE_RXP
         if (zesol > +ONE_RXP) zesol = +ONE_RXP
         if (zesol < -ONE_RXP) zesol = -ONE_RXP

!
!------->if no convergence after ITER_MAX, start over with larger eps

         if (mod(iter,ITER_MAX) == ZERO_IXP) then

            eps   = eps*10.0_RXP
            xisol = rg_gll_abscissa(tlxinf%mgll)
            etsol = rg_gll_abscissa(tlxinf%lgll)
            zesol = rg_gll_abscissa(tlxinf%kgll)
            dmin  = tlxinf%dmin

         endif

      enddo

!
!---->update tlxinf

      tlxinf%dmin = dmin
      tlxinf%x    = newx
      tlxinf%y    = newy
      tlxinf%z    = newz

!
!---->store global GLL nodes number

      do k = ONE_IXP,IG_NGLL
         do l = ONE_IXP,IG_NGLL
            do m = ONE_IXP,IG_NGLL
               igll              = ig_hexa_gll_glonum(m,l,k,ihexa)
               tlxinf%gll(m,l,k) = igll
            enddo
         enddo
      enddo

!     
!---->compute and store Lagrange polynomial values of a receiver at xisol, etsol and zesol

      do k = ONE_IXP,IG_NGLL
         do l = ONE_IXP,IG_NGLL
            do m = ONE_IXP,IG_NGLL
               tlxinf%lag(m,l,k) = lagrap(m,tlxinf%xi,IG_NGLL)*lagrap(l,tlxinf%eta,IG_NGLL)*lagrap(k,tlxinf%zeta,IG_NGLL)
            enddo
         enddo
      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine compute_info_hexa_receiver
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine computes local coordinates @f$\xi,\eta@f$ of a receiver in the quadrangle element it belongs.
!!It also stores global GLL nodes numbers for future interpolations as well as Lagrange polynomial values at the receiver.
!>@param tlxinf : data structure for a receiver located inside a quadrangle element
!***********************************************************************************************************************************************************************************
   subroutine compute_info_quad_receiver(tlxinf)
!***********************************************************************************************************************************************************************************
   
      use mpi
   
      use mod_global_variables, only : rg_gll_abscissa&
                                      ,IG_NGLL&
                                      ,ig_quadf_gll_glonum&
                                      ,ig_quadf_gnode_glonum&
                                      ,type_receiver_quad

      use mod_jacobian        , only : compute_quad_jacobian

      use mod_lagrange        , only : lagrap
      
      use mod_coordinate      , only :&
                                      compute_quad_point_coord&
                                     ,compute_quad_point_coord_z
      
      implicit none

      type(type_receiver_quad), intent(inout) :: tlxinf
      
      real(kind=RXP)                          :: eps
      real(kind=RXP)                          :: dmin
      real(kind=RXP)                          :: xisol
      real(kind=RXP)                          :: etsol
      real(kind=RXP)                          :: newx
      real(kind=RXP)                          :: newy
      real(kind=RXP)                          :: newz
      real(kind=RXP)                          :: dxidx
      real(kind=RXP)                          :: dxidy
      real(kind=RXP)                          :: detdx
      real(kind=RXP)                          :: detdy
      real(kind=RXP)                          :: deter
      real(kind=RXP)                          :: dx
      real(kind=RXP)                          :: dy
      real(kind=RXP)                          :: dxi
      real(kind=RXP)                          :: det
                                                                        
      integer(kind=IXP)                       :: k
      integer(kind=IXP)                       :: l
      integer(kind=IXP)                       :: iquad
      integer(kind=IXP)                       :: igll
      integer(kind=IXP)                       :: iter
      integer(kind=IXP), parameter            :: ITER_MAX = 100_IXP
    

!
!---->initialize guessed solution (in case dmin already < to epsilon)

      xisol = rg_gll_abscissa(tlxinf%mgll)
      etsol = rg_gll_abscissa(tlxinf%lgll)

!
!---->initialize dmin, epsilon and iteration

      dmin  = tlxinf%dmin
      eps   = 1.0e-3_RXP
      iter  = ZERO_IXP

!
!---->quad containing the receiver

      iquad = tlxinf%iel

!
!---->solve the nonlinear system to find local coordinates

      call compute_quad_jacobian(iquad,xisol,etsol,dxidx,dxidy,detdx,detdy,deter)

      call compute_quad_point_coord(iquad,xisol,etsol,newx,newy,newz)

      do while(dmin > eps)

         dx    = tlxinf%x - newx
         dy    = tlxinf%y - newy

         dxi   = dxidx*dx + dxidy*dy
         det   = detdx*dx + detdy*dy

         xisol = xisol + dxi
         etsol = etsol + det

         call compute_quad_point_coord(iquad,xisol,etsol,newx,newy,newz)

         dmin  = sqrt( (newx-tlxinf%x)**TWO_IXP + (newy-tlxinf%y)**TWO_IXP )

!
!------->increase eps in case a solution is not found after ITER_MAX iterations

         if ( (mod(iter,ITER_MAX) == ZERO_IXP) .and. (dmin > eps) ) then

            eps   = eps*TWO_RXP
            newx  = tlxinf%x
            newy  = tlxinf%y
            dmin  = tlxinf%dmin
            xisol = rg_gll_abscissa(tlxinf%mgll)
            etsol = rg_gll_abscissa(tlxinf%lgll)

         endif
!
!------->prepare next iteration

         call compute_quad_jacobian(iquad,xisol,etsol,dxidx,dxidy,detdx,detdy,deter)

         iter  = iter + ONE_IXP

      enddo

      tlxinf%dmin = dmin
      tlxinf%x    = newx
      tlxinf%y    = newy
      tlxinf%z    = compute_quad_point_coord_z(ig_quadf_gnode_glonum,iquad,xisol,etsol)
      tlxinf%xi   = xisol
      tlxinf%eta  = etsol

!
!---->store global GLL nodes number

      do k = ONE_IXP,IG_NGLL
         do l = ONE_IXP,IG_NGLL
            igll            = ig_quadf_gll_glonum(l,k,iquad)
            tlxinf%gll(l,k) = igll
         enddo
      enddo

!     
!---->compute and store Lagrange polynomial values of a receiver at xisol and etsol

      do k = ONE_IXP,IG_NGLL
         do l = ONE_IXP,IG_NGLL
            tlxinf%lag(l,k) = lagrap(l,tlxinf%xi,IG_NGLL)*lagrap(k,tlxinf%eta,IG_NGLL)
         enddo
      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine compute_info_quad_receiver
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine writes @f$x,y,z@f$-displacements, velocities and accelerations at receivers
!***********************************************************************************************************************************************************************************
   subroutine write_receiver_output()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      ig_nreceiver_hexa&
                                     ,ig_nreceiver_quad&
                                     ,rg_simu_current_time&
                                     ,tg_receiver_hexa&
                                     ,tg_receiver_quad&
                                     ,rg_gll_displacement&
                                     ,rg_gll_velocity&
                                     ,rg_gll_acceleration&
                                     ,IG_NGLL&
                                     ,IG_NDOF&
                                     ,ig_hexa_receiver_unit&
                                     ,ig_quad_receiver_unit

      use mod_gll_value

      use mod_lagrange,    only :&
                                 hexa_lagrange_interp&
                                ,quad_lagrange_interp

      use mod_filter, only : apply_fir_filter

      implicit none 

      real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: gll_dis_hexa
      real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: gll_vel_hexa
      real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL,IG_NGLL) :: gll_acc_hexa

      real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL)         :: gll_dis_quad
      real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL)         :: gll_vel_quad
      real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL)         :: gll_acc_quad

      real(kind=RXP)                                             :: dis_x
      real(kind=RXP)                                             :: dis_y
      real(kind=RXP)                                             :: dis_z
      real(kind=RXP)                                             :: vel_x
      real(kind=RXP)                                             :: vel_y
      real(kind=RXP)                                             :: vel_z
      real(kind=RXP)                                             :: acc_x
      real(kind=RXP)                                             :: acc_y
      real(kind=RXP)                                             :: acc_z
      real(kind=RXP)                                             :: simu_current_time
                                                                 
      integer(kind=IXP)                                          :: irec
!     integer(kind=IXP)                                          :: idt

!
!---->set time to write in file

      simu_current_time = rg_simu_current_time

!
!
!********************************************************
!---->receivers inside hexahedron elements
!********************************************************

      do irec = ONE_IXP,ig_nreceiver_hexa

         call get_hexa_gll_value(rg_gll_displacement,tg_receiver_hexa(irec)%gll,gll_dis_hexa)
         call get_hexa_gll_value(rg_gll_velocity    ,tg_receiver_hexa(irec)%gll,gll_vel_hexa)
         call get_hexa_gll_value(rg_gll_acceleration,tg_receiver_hexa(irec)%gll,gll_acc_hexa)

!
!------->interpolate displacement, velocity and acceleration for receivers inside cpu myrank

         call hexa_lagrange_interp(gll_dis_hexa,tg_receiver_hexa(irec)%lag,dis_x,dis_y,dis_z)
         call hexa_lagrange_interp(gll_vel_hexa,tg_receiver_hexa(irec)%lag,vel_x,vel_y,vel_z)
         call hexa_lagrange_interp(gll_acc_hexa,tg_receiver_hexa(irec)%lag,acc_x,acc_y,acc_z)
!
!------->write binary values (files have been open inside the subroutine init_hexa_receiver)

         write(unit=ig_hexa_receiver_unit(irec)) simu_current_time,dis_x,dis_y,dis_z,vel_x,vel_y,vel_z,acc_x,acc_y,acc_z

      enddo

!
!
!********************************************************
!---->receivers inside quadrangle elements
!********************************************************

      do irec = ONE_IXP,ig_nreceiver_quad

         call get_quad_gll_value(rg_gll_displacement,tg_receiver_quad(irec)%gll,gll_dis_quad)
         call get_quad_gll_value(rg_gll_velocity    ,tg_receiver_quad(irec)%gll,gll_vel_quad)
         call get_quad_gll_value(rg_gll_acceleration,tg_receiver_quad(irec)%gll,gll_acc_quad)

!
!------->interpolate displacement, velocity and acceleration for receivers inside cpu myrank

         call quad_lagrange_interp(gll_dis_quad,tg_receiver_quad(irec)%lag,dis_x,dis_y,dis_z)
         call quad_lagrange_interp(gll_vel_quad,tg_receiver_quad(irec)%lag,vel_x,vel_y,vel_z)
         call quad_lagrange_interp(gll_acc_quad,tg_receiver_quad(irec)%lag,acc_x,acc_y,acc_z)

!
!------->write binary values at each time step if no FIR_FILTER is applied on time history. (files have been open inside the subroutine init_quad_receiver)

         write(unit=ig_quad_receiver_unit(irec)) simu_current_time,dis_x,dis_y,dis_z,vel_x,vel_y,vel_z,acc_x,acc_y,acc_z

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine write_receiver_output
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine finds which cpu and which hexa of this cpu contains the point defined in the data strucutre 'point3d'
!>@param  p    : data type that contains information about point in 3d
!>@return p    : data type that contains information about point in 3d
!>@return ndcs : number of points in cpu 'myrank'
!***********************************************************************************************************************************************************************************
   subroutine locate_point_in_hexa(p,ndcs)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      IG_LST_UNIT&
                                     ,ig_ncpu&
                                     ,ig_myrank&
                                     ,point3d&
                                     ,ig_mpi_comm_simu

      implicit none

      type(point3d)                  , intent(inout)  :: p
      integer(kind=IXP)              , intent(inout)  :: ndcs

      real(kind=RXP)            , dimension(ig_ncpu)  :: rldum1 

      integer(kind=IXP)         , dimension(ONE_IXP)  :: min_loc
      integer(kind=IXP)                               :: ios
      integer(kind=IXP)                               :: icpu
      integer(kind=IXP)                               :: iloc
                                                    
      logical(kind=IXP)                               :: is_inside
      logical(kind=IXP), dimension(ig_ncpu)           :: is_inside_all_cpu

      is_inside = .false.

      iloc = -1_IXP

!
!---->find which hexahedron element contains the source iso

     !call is_point_inside_hexahedron    (tlsrc%x,tlsrc%y,tlsrc%z,tlsrc%dmin,tlsrc%kgll,tlsrc%lgll,tlsrc%mgll,tlsrc%iel,is_inside)
      call is_point_inside_hexahedron_new(p%x,p%y,p%z,p%dmin,p%kgll,p%lgll,p%mgll,p%iel,is_inside)

!
!---->cpu0 gather all is_inside to check:
!        1) if the source is inside a hexahedron element. if no then launch an alternative search with another algorithm.
!        2) if the source is found by multiple cpus. If yes, then affect the source to only one cpu

      call mpi_gather(is_inside,ONE_IXP,MPI_LOGICAL,is_inside_all_cpu,ONE_IXP,MPI_LOGICAL,ZERO_IXP,ig_mpi_comm_simu,ios)
   
      if (ig_myrank == ZERO_IXP) then
!  
!------->search the first cpu that contains the source

         iloc = -ONE_IXP

         do icpu = ONE_IXP,ig_ncpu
       
            if (is_inside_all_cpu(icpu)) then

               iloc = icpu
               exit

            endif
       
         enddo

!
!------->source has been found by a cpu

         if (iloc >= ONE_IXP) then

            do icpu = iloc+ONE_IXP,ig_ncpu

               is_inside_all_cpu(icpu) = .false.

            enddo

         endif

      endif

!
!---->cpu0 informs all cpus about the result of the search of the source 'iso' (iloc = -1: iso not found | iloc>=1: iso found)

      call mpi_bcast(iloc,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)

!  
!---->if among all cpus, subroutine 'is_point_inside_hexahedron' has not found an hexa element containing the source 'iso' --> switch to subroutine 'search_closest_hexa_gll'

      if (iloc == -ONE_IXP) then

        !write(IG_LST_UNIT,'(a,3(F10.3,1X))') "warning: alternative search for source ",p%x,p%y,p%z !to be written only by cpu0. need to be gather by cpu0 before writing
  
!------->find the closest element and gll point to the source 'iso'

         call search_closest_hexa_gll(p%x,p%y,p%z,p%dmin,p%kgll,p%lgll,p%mgll,p%iel)
!       
!------->check which cpu the source belongs to (i.e., cpu which has the smallest dmin)

         call mpi_allgather(p%dmin,ONE_IXP,MPI_REAL,rldum1,ONE_IXP,MPI_REAL,ig_mpi_comm_simu,ios)

         min_loc = minloc(rldum1(ONE_IXP:ig_ncpu))

         if (min_loc(ONE_IXP) == ig_myrank+ONE_IXP) then

            p%cpu = ig_myrank + ONE_IXP
            ndcs  = ndcs      + ONE_IXP

         else

            p%cpu = min_loc(ONE_IXP)

         endif

!
!---->the source 'iso' has been found inside an hexa element of a cpu

      else
      
!  
!------->cpu0 scatter array is_inside_all_cpu. The cpu that obtain the value is_inside = .true. will compute the source 'iso'

         call mpi_scatter(is_inside_all_cpu,ONE_IXP,MPI_LOGICAL,is_inside,ONE_IXP,MPI_LOGICAL,ZERO_IXP,ig_mpi_comm_simu,ios)
      
         if (is_inside) then
    
            p%cpu = ig_myrank + ONE_IXP
            ndcs  = ndcs      + ONE_IXP
    
         endif

      endif

      return
   
!***********************************************************************************************************************************************************************************
   end subroutine locate_point_in_hexa
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine determines if a 3D point is inside a hexahedon. 
!
!>@param x          : @f$x@f$-coordinate of a receiver
!>@param y          : @f$y@f$-coordinate of a receiver
!>@param z          : @f$z@f$-coordinate of a receiver
!>@return dmin      : minimal Euclidean distance from the 3D point to the GLL(ONE_IXP,ONE_IXP,ONE_IXP)
!>@return kgll      : first GLL node along @f$\zeta@f$-coordinate
!>@return lgll      : first GLL node along @f$\eta @f$-coordinate
!>@return mgll      : first GLL node along @f$\xi  @f$-coordinate
!>@return iel       : hexahedron element number which contains the 3D point
!>@return is_inside : is true if the point is inside the hexahedron
!***********************************************************************************************************************************************************************************
   subroutine is_point_inside_hexahedron(x,y,z,dmin,kgll,lgll,mgll,iel,is_inside)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :IG_NDOF&
                                     ,IG_NGLL&
                                     ,ig_nhexa&
                                     ,ig_hexa_nnode&
                                     ,ig_hexa_face_node&
                                     ,ig_hexa_gnode_glonum&
                                     ,rg_gnode_x&
                                     ,rg_gnode_y&
                                     ,rg_gnode_z

      implicit none

      real   (kind=RXP), intent(in)                      :: x
      real   (kind=RXP), intent(in)                      :: y
      real   (kind=RXP), intent(in)                      :: z
      real   (kind=RXP), intent(out)                     :: dmin
      integer(kind=IXP), intent(out)                     :: kgll
      integer(kind=IXP), intent(out)                     :: lgll
      integer(kind=IXP), intent(out)                     :: mgll
      integer(kind=IXP), intent(out)                     :: iel
      logical(kind=IXP), intent(out)                     :: is_inside
                                                
      integer(kind=IXP), parameter                       :: NFACE      = SIX_IXP
      integer(kind=IXP), parameter                       :: NNODE_FACE = FOUR_IXP
      integer(kind=IXP), dimension(NFACE)                :: face_nnode = (/FOUR_IXP,FOUR_IXP,FOUR_IXP,FOUR_IXP,FOUR_IXP,FOUR_IXP/)
      integer(kind=IXP)                                  :: ihexa
      integer(kind=IXP)                                  :: inode
      integer(kind=IXP)                                  :: node_global_num

      real(kind=RXP)  , dimension(IG_NDOF,ig_hexa_nnode) :: v_xyz
      real(kind=RXP)  , dimension(IG_NDOF)               :: p_xyz


      p_xyz(ONE_IXP  ) = x
      p_xyz(TWO_IXP  ) = y
      p_xyz(THREE_IXP) = z

      iel  = ZERO_IXP
      kgll = ZERO_IXP
      lgll = ZERO_IXP
      mgll = ZERO_IXP
      dmin = huge(dmin)

      do ihexa = ONE_IXP,ig_nhexa

         do inode = ONE_IXP,ig_hexa_nnode

            node_global_num = ig_hexa_gnode_glonum(inode,ihexa)

            v_xyz(ONE_IXP,inode) = rg_gnode_x(node_global_num)
            v_xyz(TWO_IXP,inode) = rg_gnode_y(node_global_num)
            v_xyz(THREE_IXP,inode) = rg_gnode_z(node_global_num)

         enddo

         call is_point_inside_polyhedron(ig_hexa_nnode,NFACE,NNODE_FACE,v_xyz,face_nnode,ig_hexa_face_node,p_xyz,is_inside)

         if (is_inside) then

            iel  = ihexa
            kgll = ONE_IXP
            lgll = ONE_IXP
            mgll = ONE_IXP
            dmin = sqrt( (v_xyz(ONE_IXP,ONE_IXP)-x)**TWO_IXP + (v_xyz(TWO_IXP,ONE_IXP)-y)**TWO_IXP + (v_xyz(THREE_IXP,ONE_IXP)-z)**TWO_IXP ) !computed from the first geom node at GLL (m,l,k) = (ONE_IXP,ONE_IXP,ONE_IXP)

            return

         endif

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine is_point_inside_hexahedron
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine determines if a 3D point is inside a hexahedon. 
!
!>@param x          : @f$x@f$-coordinate of a receiver
!>@param y          : @f$y@f$-coordinate of a receiver
!>@param z          : @f$z@f$-coordinate of a receiver
!>@return dmin      : minimal Euclidean distance from the 3D point to the GLL(ONE_IXP,ONE_IXP,ONE_IXP)
!>@return kgll      : first GLL node along @f$\zeta@f$-coordinate
!>@return lgll      : first GLL node along @f$\eta @f$-coordinate
!>@return mgll      : first GLL node along @f$\xi  @f$-coordinate
!>@return iel       : hexahedron element number which contains the 3D point
!>@return is_inside : is true if the point is inside the hexahedron
!***********************************************************************************************************************************************************************************
   subroutine is_point_inside_hexahedron_new(x,y,z,dmin,kgll,lgll,mgll,iel,is_inside)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :IG_NDOF&
                                     ,IG_NGLL&
                                     ,ig_nhexa&
                                     ,ig_hexa_nnode&
                                     ,ig_hexa_gnode_glonum&
                                     ,rg_gnode_x&
                                     ,rg_gnode_y&
                                     ,rg_gnode_z

      use mod_GeoPoint
      use mod_GeoPolygon
      use mod_GeoPolygonProc

      implicit none

      real   (kind=RXP), intent(in)                       :: x
      real   (kind=RXP), intent(in)                       :: y
      real   (kind=RXP), intent(in)                       :: z
      real   (kind=RXP), intent(out)                      :: dmin
      integer(kind=IXP), intent(out)                      :: kgll
      integer(kind=IXP), intent(out)                      :: lgll
      integer(kind=IXP), intent(out)                      :: mgll
      integer(kind=IXP), intent(out)                      :: iel
      logical(kind=IXP), intent(out)                      :: is_inside
                                                
      real   (kind=RXP), dimension(IG_NDOF,ig_hexa_nnode) :: v_xyz

      integer(kind=IXP)                                   :: ihexa
      integer(kind=IXP)                                   :: inode
      integer(kind=IXP)                                   :: node_global_num
      integer(kind=IXP)                                   :: ios

      type(GeoPoint)                                      :: p
      type(GeoPoint)  , dimension(ig_hexa_nnode)          :: verticesArray
      type(GeoPolygon)                                    :: polygon
      type(GeoPolygonProc)                                :: procObj

      p         = GeoPoint(x,y,z)
      iel       = ZERO_IXP
      kgll      = ZERO_IXP
      lgll      = ZERO_IXP
      mgll      = ZERO_IXP
      dmin      = huge(dmin)
      is_inside = .false.

      do ihexa = ONE_IXP,ig_nhexa

         do inode = ONE_IXP,ig_hexa_nnode

            node_global_num = ig_hexa_gnode_glonum(inode,ihexa)

            v_xyz(ONE_IXP  ,inode) = rg_gnode_x(node_global_num)
            v_xyz(TWO_IXP  ,inode) = rg_gnode_y(node_global_num)
            v_xyz(THREE_IXP,inode) = rg_gnode_z(node_global_num)

            verticesArray(inode) = GeoPoint(rg_gnode_x(node_global_num),rg_gnode_y(node_global_num),rg_gnode_z(node_global_num))

         enddo

         polygon = GeoPolygon(verticesArray)

         call procObj%InitGeoPolygonProc(polygon)

         is_inside = procObj%PointInside3DPolygon(p%x,p%y,p%z)

         if (is_inside) then

            iel  = ihexa
            kgll = ONE_IXP
            lgll = ONE_IXP
            mgll = ONE_IXP
            dmin = sqrt( (v_xyz(ONE_IXP,ONE_IXP)-x)**TWO_IXP + (v_xyz(TWO_IXP,ONE_IXP)-y)**TWO_IXP + (v_xyz(THREE_IXP,ONE_IXP)-z)**TWO_IXP ) !computed from the first geom node at GLL (m,l,k) = (ONE_IXP,ONE_IXP,ONE_IXP)

            return

         else

            call procObj%Delete()

         endif

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine is_point_inside_hexahedron_new
!***********************************************************************************************************************************************************************************



!
!
!>@brief
!!This subroutine determines if a point is inside a polyhedron. The reference states that the polyhedron should be simple 
!!(that is, the faces should form a single connected surface), and that the individual faces should be consistently oriented.
!!However, the polyhedron does not, apparently, need to be convex.
!
!>@license GNU LGPL
!
!>@author John Burkardt (modified 30 August 2005)
!
!>@reference Paulo Cezar Pinto Carvalho, Paulo Roma Cavalcanti, Point in Polyhedron Testing Using Spherical Polygons,in Graphics Gems V,edited by Alan Paeth, Academic Press, 1995,ISBN: 0125434553
!
!>@param node_num       : the number of vertices of the polyhedron
!>@param face_num       : the number of faces    of the polyhedron
!>@param face_order_max : the maximum order of any face (=4 vertices per face for a hexahedron element)
!>@param v              : the coordinates of the vertices
!>@param face_order     : the order of each face
!>@param face_point     : the indices of the nodes that make up each face
!>@param p              : the point to be tested
!>@return inside        : is true if the point is inside the polyhedron
!***********************************************************************************************************************************************************************************
   subroutine is_point_inside_polyhedron(node_num,face_num,face_order_max,v,face_order,face_point,p,inside)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), parameter                                       :: dim_num = THREE_IXP
                                                              
      integer(kind=IXP), intent(in )                                     :: node_num
      integer(kind=IXP), intent(in )                                     :: face_num
      integer(kind=IXP), intent(in )                                     :: face_order_max
      real   (kind=RXP), intent(in ), dimension(dim_num,node_num)        :: v
      integer(kind=IXP), intent(in ), dimension(face_num)                :: face_order
      integer(kind=IXP), intent(in ), dimension(face_order_max,face_num) :: face_point
      real   (kind=RXP), intent(in ), dimension(dim_num)                 :: p
      logical(kind=IXP), intent(out)                                     :: inside
    
      real   (kind=RXP)                                                  :: d1
      real   (kind=RXP)                                                  :: area
      integer(kind=IXP)                                                  :: face
      integer(kind=IXP)                                                  :: k
      integer(kind=IXP)                                                  :: node
      integer(kind=IXP)                                                  :: node_num_face
      real   (kind=RXP)                                                  :: solid_angle
      real   (kind=RXP), dimension(dim_num,face_order_max)               :: v_face

    
      area = ZERO_RXP
    
      do face = ONE_IXP,face_num
    
         node_num_face = face_order(face)
         
         do k = ONE_IXP, node_num_face
         
           node = face_point(k,face)
         
           v_face(ONE_IXP:dim_num,k) = v(ONE_IXP:dim_num,node)
         
         enddo
         
         call polygon_solid_angle_3d(node_num_face,v_face,p,solid_angle)
         
         area = area + solid_angle

         d1 = abs(abs(solid_angle) - TWO_RXP*PI_RXP)

!
!------->probably on a face of a hexa element

         if ( d1 <= TWO_RXP*EPSILON_MACHINE_RXP ) then

            inside = .true.
            return

         endif

      enddo

!
!---->inside hexa element

      if ( (area < -3.0_RXP*PI_RXP) .or. (area > 3.0_RXP*PI_RXP)  ) then

         inside = .true.

!
!---->probably on an edge of a hexa element

      elseif ( (abs(abs(area) - 3.14_RXP)) < 0.01_RXP ) then

         inside = .true.

      else

        inside = .false.

      endif
    
      return

!***********************************************************************************************************************************************************************************
   end subroutine is_point_inside_polyhedron
!***********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine finds which quadrangle element contains a receiver
!
!>@param x          : @f$x@f$-coordinate of a receiver
!>@param y          : @f$y@f$-coordinate of a receiver
!>@param quad_gnode : global geometric nodes of quadrangle elements
!>@param nquad      : number of quadrangle element in quad_gnode
!>@param quad_nnode : number of geometric node of a quadrangle element
!>@param dmin       : minimal Euclidean distance from point to all GLL nodes in cpu myrank
!>@param lgll       : closest GLL node index along @f$\eta @f$-coordinate
!>@param mgll       : closest GLL node index along @f$\xi  @f$-coordinate
!>@param quad_num   : number of quadrangle element in cpu myrank whose GLL node has the minimal dmin
!>@param  is_inside : .true. is the receiver is inside or on the polygonal line
!>@return is_inside : .true. is the receiver is inside or on the polygonal line
!***********************************************************************************************************************************************************************************
   subroutine is_inside_quad(x,y,quad_gnode,nquad,quad_nnode,dmin,lgll,mgll,quad_num,is_inside)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      rg_gnode_x&
                                     ,rg_gnode_y
      implicit none
      
      real   (kind=RXP), intent( in)                              :: x
      real   (kind=RXP), intent( in)                              :: y
      integer(kind=IXP), intent( in)                              :: nquad
      integer(kind=IXP), intent( in)                              :: quad_nnode
      integer(kind=IXP), intent( in), dimension(quad_nnode,nquad) :: quad_gnode
      real   (kind=RXP), intent(out)                              :: dmin
      integer(kind=IXP), intent(out)                              :: lgll
      integer(kind=IXP), intent(out)                              :: mgll
      integer(kind=IXP), intent(out)                              :: quad_num
      logical(kind=IXP), intent(out)                              :: is_inside
                                                        
      integer(kind=IXP)                                           :: node_num
      integer(kind=IXP)                                           :: inode
      integer(kind=IXP)                                           :: iquad
      integer(kind=IXP)                                           :: l
      integer(kind=IXP)                                           :: m
      real   (kind=RXP), dimension(quad_nnode)                    :: quad_gnode_x
      real   (kind=RXP), dimension(quad_nnode)                    :: quad_gnode_y


      is_inside = .false.

      dmin = huge(dmin)

      do iquad = ONE_IXP,nquad

         do inode = ONE_IXP,quad_nnode

            node_num = quad_gnode(inode,iquad)

            quad_gnode_x(inode) = rg_gnode_x(node_num)
            quad_gnode_y(inode) = rg_gnode_y(node_num)

         enddo

         call is_inside_polygon(x,y,quad_gnode_x,quad_gnode_y,quad_nnode,l,m)

         if (l >= ZERO_IXP) then

            is_inside = .true.
            quad_num  = iquad
            lgll      = ONE_IXP
            mgll      = ONE_IXP
            dmin      = sqrt( (quad_gnode_x(ONE_IXP)-x)**TWO_IXP + (quad_gnode_y(ONE_IXP)-y)**TWO_IXP )

            return

         endif

      enddo

      return
      
!***********************************************************************************************************************************************************************************
   end subroutine is_inside_quad
!**********************************************************************************************************************************************************************************


!
!
!>@brief This subroutine tests if a point P with coordinates @f$x0,y0@f$ is inside a polygonal line.
!
!>@license GNU LGPL
!
!>@author Fortran 66 version by A.H. Morris. Converted to ELF90 compatibility by Alan Miller, 15 February 1997.
!
!>@param  x0 : @f$x@f$-coordinate of the point P
!>@param  y0 : @f$y@f$-coordinate of the point P
!>@param  x  : @f$x@f$-coordinate of the vertices of the polygonal line
!>@param  y  : @f$y@f$-coordinate of the vertices of the polygonal line
!>@param  n  : number of vertices of the polygonal line
!>@return l  : l = -1 if (x0,y0) is outside the polygonal path, l = 0 if (x0,y0) lies on the polygonal path, l = 1 if (x0,y0) is inside the polygonal path
!>@return m  : m =  0 if (x0,y0) is on or outside the path. if (x0,y0) is inside the path then m is the winding number of the path around the point (x0,y0).
!***********************************************************************************************************************************************************************************
   subroutine is_inside_polygon(x0,y0,x,y,n,l,m)
!***********************************************************************************************************************************************************************************
!     Given a polygonal line connecting the vertices (x(i),y(i)) (i = 1,...,n) taken in this order.  
!     It is assumed that the polygonal path is a loop and that there is a line from (x(n),y(n)) to (x(1),y(1)).  
!     
!     N.B.: the polygon may cross itself any number of times.
!     
!     (x0,y0) is an arbitrary point and l and m are variables.
!     on output, l and m are assigned the following values:
!        l = -1   if (x0,y0) is outside the polygonal path
!        l =  0   if (x0,y0) lies on the polygonal path
!        l =  1   if (x0,y0) is inside the polygonal path
!        m =  0   if (x0,y0) is on or outside the path.  if (x0,y0) is inside the
!                 path then m is the winding number of the path around the point (x0,y0).
!     
      implicit none
      
      integer(kind=IXP), intent(in)  :: n
      real   (kind=RXP), intent(in)  :: x0
      real   (kind=RXP), intent(in)  :: y0
      real   (kind=RXP), intent(in)  :: x(n)
      real   (kind=RXP), intent(in)  :: y(n)
      integer(kind=IXP), intent(out) :: l
      integer(kind=IXP), intent(out) :: m

      real   (kind=RXP), parameter   :: PIx2 = TWO_RXP*PI_RXP

      integer(kind=IXP)              :: i
      integer(kind=IXP)              :: n0
      real   (kind=RXP)              :: angle
      real   (kind=RXP)              :: sum
      real   (kind=RXP)              :: theta
      real   (kind=RXP)              :: theta1
      real   (kind=RXP)              :: thetai
      real   (kind=RXP)              :: tol
      real   (kind=RXP)              :: u
      real   (kind=RXP)              :: v
      
      
      n0 = n

      tol = 4.0_RXP*EPSILON_MACHINE_RXP*PI_RXP

      l   = -ONE_IXP
      m   =  ZERO_IXP
      
      u = x(ONE_IXP) - x0
      v = y(ONE_IXP) - y0
      if ( (abs(u) < EPSILON_MACHINE_RXP) .and. (abs(v) < EPSILON_MACHINE_RXP) ) then
         l = ZERO_IXP
         return
      endif

      if (n0 < TWO_IXP) return
      theta1 = atan2(v, u)
      
      sum   = ZERO_RXP
      theta = theta1

      do i = TWO_IXP,n0

        u = x(i) - x0
        v = y(i) - y0
        if ( (abs(u) < EPSILON_MACHINE_RXP) .and. (abs(v) < EPSILON_MACHINE_RXP) ) then
           l = ZERO_IXP
           return
        endif

        thetai = atan2(v, u)      
        angle  = abs(thetai - theta)

        if (abs(angle - PI_RXP) < tol) then
           l = ZERO_IXP
           return
        endif

        if (angle > PI_RXP) angle =  angle - PIx2
        if (theta > thetai      ) angle = -angle

        sum   = sum + angle
        theta = thetai

      enddo
      
      angle = abs(theta1 - theta)
      if (abs(angle - PI_RXP) < tol) then
         l = ZERO_IXP
         return
      endif
      if (angle > PI_RXP ) angle =  angle - PIx2
      if (theta > theta1       ) angle = -angle
      sum = sum + angle
      !
      !sum = 2*PI_RXP*m where m is the winding number
      m = abs(sum)/PIx2 + 0.2_RXP
      if (m == ZERO_IXP) return
      l = ONE_IXP
      if (sum < ZERO_RXP) m = -m
      return
      
!***********************************************************************************************************************************************************************************
   end subroutine is_inside_polygon
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine determines projected solid angle of a 3D plane polygon
!
!>@details
!!A point P is at the center of a unit sphere.  A planar polygon
!!is to be projected onto the surface of this sphere, by drawing
!!the ray from P to each polygonal vertex, and noting where this ray
!!intersects the sphere.
!!We compute the area on the sphere of the projected polygon.
!!Since we are projecting the polygon onto a unit sphere, the area 
!!of the projected polygon is equal to the solid angle subtended by 
!!the polygon.
!!The value returned by this routine will include a sign.  The
!!angle subtended will be NEGATIVE if the normal vector defined by
!!the polygon points AWAY from the viewing point, and will be
!!POSITIVE if the normal vector points towards the viewing point.
!!If the orientation of the polygon is of no interest to you,
!!then you can probably simply take the absolute value of the
!!solid angle as the information you want.
!
!>@license GNU LGPL
!
!>@author John Burkardt (modified 29 October 2007)
!
!>@reference Paulo Cezar Pinto Carvalho, Paulo Roma Cavalcanti, Point in Polyhedron Testing Using Spherical Polygons,in Graphics Gems V,edited by Alan Paeth, Academic Press, 1995,ISBN: 0125434553
!
!>@param n              : the number of vertices
!>@param v              : the coordinates of the vertices
!>@param p              : the point at the center of the unit sphere
!>@return solid_angle   : the solid angle subtended by the polygon, as projected onto the unit sphere around the point P
!***********************************************************************************************************************************************************************************
   subroutine polygon_solid_angle_3d(n,v,p,solid_angle)
!***********************************************************************************************************************************************************************************

      implicit none
    
      integer(kind=IXP), parameter                         :: dim_num = THREE_IXP

      integer(kind=IXP), intent(in )                       :: n
      real   (kind=RXP), intent(in ), dimension(dim_num,n) :: v
      real   (kind=RXP), intent(out)                       :: solid_angle
    
      real   (kind=RXP)             , dimension(dim_num)   :: a
      real   (kind=RXP)             , dimension(dim_num)   :: b
      real   (kind=RXP)             , dimension(dim_num)   :: normal1
      real   (kind=RXP)             , dimension(dim_num)   :: normal2
      real   (kind=RXP)             , dimension(dim_num)   :: p
      real   (kind=RXP)             , dimension(dim_num)   :: plane
      real   (kind=RXP)             , dimension(dim_num)   :: r1
      real   (kind=RXP)                                    :: angle
      real   (kind=RXP)                                    :: area
      real   (kind=RXP)                                    :: normal1_norm
      real   (kind=RXP)                                    :: normal2_norm
      real   (kind=RXP)                                    :: s
      integer(kind=IXP)                                    :: j
      integer(kind=IXP)                                    :: jp1

 
      if ( n < THREE_IXP ) then

        solid_angle = ZERO_RXP
        return

      endif
    
      call polygon_normal_3d(n,v,plane)
     
      a(ONE_IXP:dim_num) = v(ONE_IXP:dim_num,n) - v(ONE_IXP:dim_num,ONE_IXP)
    
      area = ZERO_RXP
    
      do j = ONE_IXP,n
    
        r1(ONE_IXP:dim_num) = v(ONE_IXP:dim_num,j) - p(ONE_IXP:dim_num)
    
        jp1 = i4_wrap (j+ONE_IXP,ONE_IXP,n)
    
        b(ONE_IXP:dim_num) = v(ONE_IXP:dim_num,jp1) - v(ONE_IXP:dim_num,j)
    
        call r4vec_cross_product_3d(a,r1,normal1)
    
        normal1_norm = r4vec_norm(dim_num,normal1)
    
        call r4vec_cross_product_3d(r1,b,normal2)
    
        normal2_norm = r4vec_norm(dim_num,normal2)
        
        s = dot_product( normal1(ONE_IXP:dim_num), normal2(ONE_IXP:dim_num) ) / ( normal1_norm * normal2_norm )
    
        angle = r4_acos(s)
    
        s = r4vec_scalar_triple_product(b,a,plane)
    
        if ( ZERO_RXP < s ) then

          area = area + PI_RXP - angle

        else

          area = area + PI_RXP + angle

        endif
    
        a(ONE_IXP:dim_num) = -b(ONE_IXP:dim_num)
    
      enddo
    
      area = area - PI_RXP*real(n-TWO_IXP,kind=RXP)
    
      if ( ZERO_RXP < dot_product ( plane(ONE_IXP:dim_num), r1(ONE_IXP:dim_num) ) ) then

        solid_angle = -area

      else

        solid_angle = area

      endif
    
      return

!***********************************************************************************************************************************************************************************
    end subroutine polygon_solid_angle_3d
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine computes the normal vector to a polygon in 3D
!
!>@details
!!If the polygon is planar, then this calculation is correct.
!!Otherwise, the normal vector calculated is the simple average
!!of the normals defined by the planes of successive triples
!!of vertices.
!!If the polygon is "almost" planar, this is still acceptable.
!!But as the polygon is less and less planar, so this averaged normal
!!vector becomes more and more meaningless.
!
!>@license GNU LGPL
!
!>@author John Burkardt (modified 12 August 2005)
!
!>@reference Paulo Cezar Pinto Carvalho, Paulo Roma Cavalcanti, Point in Polyhedron Testing Using Spherical Polygons,in Graphics Gems V,edited by Alan Paeth, Academic Press, 1995,ISBN: 0125434553
!
!>@param n       : the number of vertices
!>@param v       : the coordinates of the vertices
!>@return normal : the averaged normal vector to the polygon
!***********************************************************************************************************************************************************************************
   subroutine polygon_normal_3d(n,v,normal) 
!***********************************************************************************************************************************************************************************

      implicit none
    
      integer(kind=IXP), parameter                         :: dim_num = THREE_IXP

      integer(kind=IXP), intent(in )                       :: n
      real   (kind=RXP), intent(in ), dimension(dim_num,n) :: v
      real   (kind=RXP), intent(out), dimension(dim_num)   :: normal
    
      real   (kind=RXP)                                    :: normal_norm
      real   (kind=RXP)             , dimension(dim_num)   :: p
      real   (kind=RXP)             , dimension(dim_num)   :: v1
      real   (kind=RXP)             , dimension(dim_num)   :: v2

      integer(kind=IXP)                                    :: j

    
      normal(ONE_IXP:dim_num) = ZERO_RXP
    
      v1(ONE_IXP:dim_num) = v(ONE_IXP:dim_num,TWO_IXP) - v(ONE_IXP:dim_num,ONE_IXP)
    
      do j = THREE_IXP,n
    
        v2(ONE_IXP:dim_num) = v(ONE_IXP:dim_num,j) - v(ONE_IXP:dim_num,ONE_IXP)
    
        call r4vec_cross_product_3d(v1,v2,p)
    
        normal(ONE_IXP:dim_num) = normal(ONE_IXP:dim_num) + p(ONE_IXP:dim_num)
    
        v1(ONE_IXP:dim_num) = v2(ONE_IXP:dim_num)
    
      enddo

!
!---->normalize

      normal_norm = r4vec_norm (dim_num,normal)
    
      if ( normal_norm < EPSILON_MACHINE_RXP ) then

        return

      endif
    
      normal(ONE_IXP:dim_num) = normal(ONE_IXP:dim_num) / normal_norm
    
      return

!***********************************************************************************************************************************************************************************
   end subroutine polygon_normal_3d
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This function forces an integer(kind=IXP)to lie between given limits by wrapping.
!
!>@details
!! Example:
!!
!! ILO = 4, IHI = 8
!!
!! I  I4_WRAP
!!
!! -2     8
!! -1     4
!!  0     5
!!  1     6
!!  2     7
!!  3     8
!!  4     4
!!  5     5
!!  6     6
!!  7     7
!!  8     8
!!  9     4
!! 10     5
!! 11     6
!! 12     7
!! 13     8
!! 14     4
!
!>@license GNU LGPL
!
!>@author John Burkardt (modified 19 August 2003)
!
!>@param ival     : an integer(kind=IXP)value
!>@param ilo      : lower  bound for the integer
!>@param ihi      : higher bound for the integer
!>@return i4_wrap : a "wrapped" version of IVAL
!***********************************************************************************************************************************************************************************
   integer(kind=IXP)function i4_wrap(ival,ilo,ihi)
!***********************************************************************************************************************************************************************************
   
      implicit none
      
      integer(kind=IXP), intent(in) :: ival
      integer(kind=IXP), intent(in) :: ihi
      integer(kind=IXP), intent(in) :: ilo

      integer(kind=IXP)             :: jhi
      integer(kind=IXP)             :: jlo
      integer(kind=IXP)             :: wide
      
      jlo = min ( ilo, ihi )
      jhi = max ( ilo, ihi )
      
      wide = jhi - jlo + ONE_IXP
      
      if ( wide == ONE_IXP ) then
        i4_wrap = jlo
      else
        i4_wrap = jlo + i4_modp ( ival - jlo, wide )
      endif
      
      return

!***********************************************************************************************************************************************************************************
   end function i4_wrap
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This function returns the nonnegative remainder of integer(kind=IXP)division.
!
!>@details
!!  If
!!    NREM = I4_MODP ( I, J )
!!    NMULT = ( I - NREM ) / J
!!  then
!!    I = J * NMULT + NREM
!!  where NREM is always nonnegative.
!!
!!  The MOD function computes a result with the same sign as the
!!  quantity being divided.  Thus, suppose you had an angle A,
!!  and you wanted to ensure that it was between 0 and 360.
!!  Then mod(A,360) would do, if A was positive, but if A
!!  was negative, your result would be between -360 and 0.
!!
!!  On the other hand, I4_MODP(A,360) is between 0 and 360, always.
!!
!!Example:
!!
!!      I     J     MOD  I4_MODP    Factorization
!!
!!    107    50       7       7    107 =  2 *  50 + 7
!!    107   -50       7       7    107 = -2 * -50 + 7
!!   -107    50      -7      43   -107 = -3 *  50 + 43
!!   -107   -50      -7      43   -107 =  3 * -50 + 43
!
!>@license GNU LGPL
!
!>@author John Burkardt (modified 02 March 1999)
!
!>@param i        : the number to be divided
!>@param j        : the number that divides i
!>@return i4_modp : the nonnegative remainder when i is divided by j
!***********************************************************************************************************************************************************************************
   function i4_modp(i,j)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only : error_stop
    
      implicit none
      
      integer(kind=IXP), intent(in) :: i
      integer(kind=IXP), intent(in) :: j

      integer(kind=IXP)             :: i4_modp
      character(len=CIL)            :: info
      
      if (j == ZERO_IXP) then

         write(info,'(a)') "error in function i4_modp: j=0 impossible"
         call error_stop(info)

      endif
      
      i4_modp = mod(i,j)
      
      if ( i4_modp < ZERO_IXP ) then

        i4_modp = i4_modp + abs(j)

      endif
      
      return

!***********************************************************************************************************************************************************************************
   end function i4_modp
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine computes the cross product of two vectors in 3D.
!
!>@details
!!The cross product in 3D can be regarded as the determinant of the symbolic matrix : det [  [  i  j  k ] [ x1 y1 z1 ] [ x2 y2 z2 ] ]
!!= ( y1 * z2 - z1 * y2 ) * i + ( z1 * x2 - x1 * z2 ) * j  + ( x1 * y2 - y1 * x2 ) * k
!
!>@license GNU LGPL
!
!>@author John Burkardt (modified 07 August 2005)
!
!>@param v1  : first  vector
!>@param v2  : second vector
!>@return v3 : the cross product vector
!***********************************************************************************************************************************************************************************
   subroutine r4vec_cross_product_3d(v1,v2,v3)
!***********************************************************************************************************************************************************************************
   
      implicit none
      
      real(kind=RXP), intent(in ) :: v1(THREE_IXP)
      real(kind=RXP), intent(in ) :: v2(THREE_IXP)
      real(kind=RXP), intent(out) :: v3(THREE_IXP)
      
      v3(ONE_IXP  ) = v1(TWO_IXP  ) * v2(THREE_IXP) - v1(THREE_IXP) * v2(TWO_IXP  )
      v3(TWO_IXP  ) = v1(THREE_IXP) * v2(ONE_IXP  ) - v1(ONE_IXP  ) * v2(THREE_IXP)
      v3(THREE_IXP) = v1(ONE_IXP  ) * v2(TWO_IXP  ) - v1(TWO_IXP  ) * v2(ONE_IXP  )
      
      return

!***********************************************************************************************************************************************************************************
   end subroutine r4vec_cross_product_3d
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This function returns the L2 norm of a vector
!
!>@param n           : size of vector a
!>@param a           : vector whose L2 norm is computed
!>@return r4vec_norm : the L2 norm of vector a
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function r4vec_norm(n,a)
!***********************************************************************************************************************************************************************************
   
      implicit none
      
      integer(kind=IXP), intent(in)               :: n
      real   (kind=RXP), intent(in), dimension(n) :: a
      
      r4vec_norm = sqrt ( sum ( a(ONE_IXP:n)**TWO_IXP ) )
      
      return

!***********************************************************************************************************************************************************************************
   end function r4vec_norm
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This function returns the arc cosine function, with argument truncation
!
!>@details
!!If you call your system ACOS routine with an input argument that is
!!even slightly outside the range [-1.0, 1.0 ], you may get an unpleasant surprise
!
!>@license GNU LGPL
!
!>@author John Burkardt (modified 19 October 2012)
!
!>@param  c       : the argument
!>@return r4_acos : an angle whose cosine is c
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function r4_acos(c)
!***********************************************************************************************************************************************************************************
   
      implicit none
      
      real(kind=RXP), intent(in) :: c
      real(kind=RXP)             :: c2
      
      c2 = c
      c2 = max ( c2, -ONE_RXP )
      c2 = min ( c2, +ONE_RXP )
      
      r4_acos = acos ( c2 )
      
      return

!***********************************************************************************************************************************************************************************
   end function r4_acos
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This function finds the scalar triple product in 3D
!
!>@details
!![A,B,C] = A dot ( B cross C ) 
!!        = B dot ( C cross A )
!!        = C dot ( A cross B )
!!The volume of a parallelepiped, whose sides are given by
!!vectors A, B, and C, is abs ( A dot ( B cross C ) ).
!!Three vectors are coplanar if and only if their scalr triple product vanishes.
!
!>@license GNU LGPL
!
!>@author John Burkardt (modified 07 August 2005)
!
!>@param  v1      : first vector
!>@param  v2      : first vector
!>@param  v3      : first vector
!>@return r4vec_scalar_triple_product : the scalar triple product
!***********************************************************************************************************************************************************************************
   real(kind=RXP)function r4vec_scalar_triple_product (v1,v2,v3)
!***********************************************************************************************************************************************************************************
   
      implicit none
      
      real(kind=RXP), intent(in), dimension(THREE_IXP) :: v1
      real(kind=RXP), intent(in), dimension(THREE_IXP) :: v2
      real(kind=RXP), intent(in), dimension(THREE_IXP) :: v3

      real(kind=RXP)            , dimension(THREE_IXP) :: v4
      
      call r4vec_cross_product_3d(v2,v3,v4)
      
      r4vec_scalar_triple_product = dot_product ( v1(ONE_IXP:THREE_IXP), v4(ONE_IXP:THREE_IXP) )
      
      return

!***********************************************************************************************************************************************************************************
   end function r4vec_scalar_triple_product
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine writes 1D velocity structure at point 'p'
!***********************************************************************************************************************************************************************************
      subroutine write_1d_velocity_structure(irec,x,y,z,dz)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                       IG_NGLL&
                                      ,cg_prefix&
                                      ,rg_mesh_zmin&
                                      ,point3d&
                                      ,rg_hexa_gll_rho&
                                      ,rg_hexa_gll_rhovs2&
                                      ,rg_hexa_gll_rhovp2&
                                      ,ig_myrank

      use mod_init_memory     , only : init_array_point3d

      use mod_coordinate      , only : compute_point3d_local_coordinate

      use mod_lagrange        , only :&
                                       hexa_lagrange_set&
                                      ,hexa_lagrange_interp

      implicit none

      type(point3d)                                         :: p
                                                            
      integer(kind=IXP), intent(in)                         :: irec
      real   (kind=RXP), intent(in)                         :: x
      real   (kind=RXP), intent(in)                         :: y
      real   (kind=RXP), intent(in)                         :: z
      real   (kind=RXP), intent(in)                         :: dz
                                                            
      real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL) :: gll_values
      real   (kind=RXP), dimension(IG_NGLL,IG_NGLL,IG_NGLL) :: lag
      real   (kind=RXP)                                     :: dz_cur
      real   (kind=RXP)                                     :: lz
      real   (kind=RXP)                                     :: z_cur
      real   (kind=RXP)                                     :: z_free_surface
      real   (kind=RXP)                                     :: z_min
      real   (kind=RXP)                                     :: v

      integer(kind=IXP)                                     :: i
      integer(kind=IXP)                                     :: n
      integer(kind=IXP)                                     :: ntmp = 0_IXP
      integer(kind=IXP)                                     :: ios
      integer(kind=IXP)                                     :: u

      character(len=6_IXP)                                  :: crec

!
!---->adjust z_free_surface and z_min to ensure that point will be inside a hexahedron element

      z_free_surface = z - EPSILON_MACHINE_RXP
      
      z_min = rg_mesh_zmin/2.0_RXP

      lz = z_free_surface - z_min

!
!---->adjust dz to match lz/n

      n = int(lz/dz,kind=IXP)+1_IXP

      dz_cur = lz/real(n-1_IXP,kind=RXP)

!
!---->loop over points of 1d velocity structure

      write(crec,'(i6.6)') irec

      open(newunit=u,file=trim(cg_prefix)//".fsr."//trim(crec)//".vst",form='unformatted',access='stream',action='write')

      do i = 1_IXP,n

         ios = init_array_point3d(p,"mod_init_medium:write_1d_velocity_structure:p")

         z_cur = z_min + real(i-1_IXP,kind=RXP)*dz_cur

         p%x = x 
         p%y = y
         p%z = z_cur

         call locate_point_in_hexa(p,ntmp)

!
!------->cpu holding the point p computes its local coordinates and velocity

         if (p%iel > 0_IXP) then

!---------->compute Vs values at GLL nodes for hexa 'iel'

            gll_values(:,:,:) = sqrt(rg_hexa_gll_rhovs2(:,:,:,p%iel)/rg_hexa_gll_rho(:,:,:,p%iel))

!---------->compute point local coordinate in hexa                         

            call compute_point3d_local_coordinate(p)

!---------->compute 3d lagrange polynomial

            call hexa_lagrange_set(p%xi,p%et,p%ze,lag)

!--------->interpolate value

            call hexa_lagrange_interp(gll_values,lag,v)

            write(u) z_cur,v

         endif

      enddo 

      close(u)

      return
!***********************************************************************************************************************************************************************************
      end subroutine write_1d_velocity_structure
!***********************************************************************************************************************************************************************************

end module mod_receiver
