!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This files contains a module to initialize MPI buffers between cpu myrank and its neighbor cpus.

!>@brief
!!This module contains subroutines to initialize MPI buffers between cpu myrank and its neighbor cpus.
module mod_init_mpibuffer

   use mod_precision

   use mod_global_variables, only : CIL

   implicit none

   private

   public  :: init_mpi_buffers
   private :: create_gll_buffer_recv
   private :: remove_duplicate
   
   contains

!
!
!>@brief
!!This subroutine searches for common GLL nodes between cpu myrank and its neighbor cpus.
!!Send and receive buffers are filled only with unique GLLs (i.e., no duplicated GLL are present in MPI buffers).
!>@return mod_global_variables::tg_cpu_neighbor
!>@return mod_global_variables::ig_mpi_buffer_sizemax
!***********************************************************************************************************************************************************************************
   subroutine init_mpi_buffers()
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                      IG_NGLL&
                                     ,ig_myrank&
                                     ,ig_ncpu_neighbor&
                                     ,ig_nhexa_outer&
                                     ,ig_hexa_gll_glonum&
                                     ,tg_cpu_neighbor&
                                     ,ig_cpu_neighbor_info&
                                     ,ig_nneighbor_all_kind&
                                     ,rg_gll_coordinate&
                                     ,ig_mpi_buffer_offset&
                                     ,rg_mpi_buffer_send&
                                     ,rg_mpi_buffer_recv&
                                     ,IG_NDOF&
                                     ,ig_mpi_request_send&
                                     ,ig_mpi_request_recv&
                                     ,error_stop&
                                     ,cg_prefix&
                                     ,cg_myrank&
                                     ,get_newunit&
                                     ,ig_mpi_comm_simu&
                                     ,LG_ASYNC_MPI_COMM&
                                     ,LG_OUTPUT_DEBUG_FILE&
                                     ,IG_LST_UNIT&
                                     ,ig_mpi_buffer_sizemax

      use mod_init_memory

      implicit none

      integer(kind=IXP), parameter                     :: NFACE =  6_IXP
      integer(kind=IXP), parameter                     :: NEDGE = 12_IXP
      integer(kind=IXP), parameter                     :: NNODE =  8_IXP
               
      real   (kind=RXP)  , dimension(:,:), allocatable :: gll_coord_send
      real   (kind=RXP)  , dimension(:,:), allocatable :: gll_coord_recv
      real   (kind=RXP)                                :: octet_sent
      real   (kind=RXP)                                :: octet_sent_total
               
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)    :: statut
      integer(kind=IXP)                                :: i
      integer(kind=IXP)                                :: j
      integer(kind=IXP)                                :: ios
      integer(kind=IXP)                                :: myunit_debug
      integer(kind=IXP)                                :: icpu_neighbor
      integer(kind=IXP)                                :: icpu
      integer(kind=IXP)                                :: isurf
      integer(kind=IXP)                                :: ngll_duplicate
      integer(kind=IXP)                                :: ngll_send
      integer(kind=IXP)                                :: ngll_recv
      integer(kind=IXP)                                :: num_gll
      integer(kind=IXP)                                :: surf_num
      integer(kind=IXP)                                :: elt_num
      integer(kind=IXP)                                :: ngll_unique
      integer(kind=IXP)                                :: mpi_buffer_sizemax
      integer(kind=IXP)                                :: buffer_sizemax
      integer(kind=IXP), dimension(:), allocatable     :: buffer_gll_duplicate

      character(len=  6)                               :: cl_rank
      character(len=CIL)                               :: info

      if (ig_myrank == ZERO_IXP) then
         write(IG_LST_UNIT,'(" ",/,a)') "creating mpi buffers between cpu myrank and its neighbors..."
         call flush(IG_LST_UNIT)
      endif

      buffer_sizemax = (6_IXP*IG_NGLL*IG_NGLL + 12_IXP*IG_NGLL + 8_IXP) * ig_nhexa_outer !DAVID: worst case, may be reduced

      ios = init_array_int(buffer_gll_duplicate,buffer_sizemax,"buffer_gll_duplicate")
       
      if (LG_ASYNC_MPI_COMM) then

          ios = init_array_int(ig_mpi_request_send,ig_ncpu_neighbor,"ig_mpi_request_send")
          ios = init_array_int(ig_mpi_request_recv,ig_ncpu_neighbor,"ig_mpi_request_recv")
          ios = init_array_int(ig_mpi_buffer_offset,ig_ncpu_neighbor+ONE_IXP,"ig_mpi_buffer_offset")

      endif
      
      mpi_buffer_sizemax = ZERO_IXP

      do icpu_neighbor = ONE_IXP,ig_ncpu_neighbor

          icpu                    = tg_cpu_neighbor(icpu_neighbor)%icpu                  
          buffer_gll_duplicate(:) = ZERO_IXP
          ngll_duplicate          = ZERO_IXP

          do isurf = ONE_IXP,ig_nneighbor_all_kind

              if (ig_cpu_neighbor_info(THREE_IXP,isurf) == icpu) then

                surf_num = ig_cpu_neighbor_info(TWO_IXP,isurf)
                elt_num  = ig_cpu_neighbor_info(ONE_IXP,isurf)

!
!---------------->put gll points from contact face in the buffer

                  if (surf_num <= NFACE) then
                      do i = ONE_IXP,IG_NGLL
                          do j = ONE_IXP,IG_NGLL
                              select case(surf_num)
                                  case(1_IXP)
                                      num_gll = ig_hexa_gll_glonum(j,i,ONE_IXP,elt_num)
                                  case(2_IXP)
                                      num_gll = ig_hexa_gll_glonum(j,ONE_IXP,i,elt_num)
                                  case(3_IXP)
                                      num_gll = ig_hexa_gll_glonum(IG_NGLL,j,i,elt_num)
                                  case(4_IXP)
                                      num_gll = ig_hexa_gll_glonum(j,IG_NGLL,i,elt_num)
                                  case(5_IXP)
                                      num_gll = ig_hexa_gll_glonum(ONE_IXP,j,i,elt_num)
                                  case(6_IXP)
                                      num_gll = ig_hexa_gll_glonum(j,i,IG_NGLL,elt_num)
                              end select

                              ngll_duplicate = ngll_duplicate + ONE_IXP

                              if (ngll_duplicate > buffer_sizemax) then
                                 write(info,'(a)') "error in subroutine init_mpi_buffers: face in contact : size of buffer_gll_duplicate too small"
                                 call error_stop(info)
                              endif

                              buffer_gll_duplicate(ngll_duplicate) = num_gll

                          enddo
                      enddo
!
!---------------->put gll points from contact edge in the buffer 

                  else if(surf_num <= NFACE+NEDGE) then
                      do i = ONE_IXP,IG_NGLL
                          select case(surf_num - NFACE)
                              case(1_IXP)
                                  num_gll = ig_hexa_gll_glonum(i,ONE_IXP,ONE_IXP,elt_num)
                              case(2_IXP)
                                  num_gll = ig_hexa_gll_glonum(IG_NGLL,i,ONE_IXP,elt_num)
                              case(3_IXP)
                                  num_gll = ig_hexa_gll_glonum(i,IG_NGLL,ONE_IXP,elt_num)
                              case(4_IXP)
                                  num_gll = ig_hexa_gll_glonum(ONE_IXP,i,ONE_IXP,elt_num)
                              case(5_IXP)
                                  num_gll = ig_hexa_gll_glonum(i,ONE_IXP,IG_NGLL,elt_num)
                              case(6_IXP)
                                  num_gll = ig_hexa_gll_glonum(IG_NGLL,i,IG_NGLL,elt_num)
                              case(7_IXP)
                                  num_gll = ig_hexa_gll_glonum(i,IG_NGLL,IG_NGLL,elt_num)
                              case(8_IXP)
                                  num_gll = ig_hexa_gll_glonum(ONE_IXP,i,IG_NGLL,elt_num)
                              case(9_IXP)
                                  num_gll = ig_hexa_gll_glonum(ONE_IXP,ONE_IXP,i,elt_num)
                              case(10_IXP)
                                  num_gll = ig_hexa_gll_glonum(IG_NGLL,ONE_IXP,i,elt_num)
                              case(11_IXP)
                                  num_gll = ig_hexa_gll_glonum(IG_NGLL,IG_NGLL,i,elt_num)
                              case(12_IXP)
                                  num_gll = ig_hexa_gll_glonum(ONE_IXP,IG_NGLL,i,elt_num)
                          end select

                          ngll_duplicate = ngll_duplicate + ONE_IXP

                          if (ngll_duplicate > buffer_sizemax) then
                             write(info,'(a)') "error in subroutine init_mpi_buffers: edge in contact : size of buffer_gll_duplicate too small"
                             call error_stop(info)
                          endif

                          buffer_gll_duplicate(ngll_duplicate) = num_gll

                      enddo

!
!---------------->put gll points from contact corner nodes in the buffer 

                  else
                      select case(surf_num - (NFACE+NEDGE))
                          case(1_IXP)
                              num_gll = ig_hexa_gll_glonum(ONE_IXP,ONE_IXP,ONE_IXP,elt_num)
                          case(2_IXP)
                              num_gll = ig_hexa_gll_glonum(IG_NGLL,ONE_IXP,ONE_IXP,elt_num)
                          case(3_IXP)
                              num_gll = ig_hexa_gll_glonum(IG_NGLL,IG_NGLL,ONE_IXP,elt_num)
                          case(4_IXP)
                              num_gll = ig_hexa_gll_glonum(ONE_IXP,IG_NGLL,ONE_IXP,elt_num)
                          case(5_IXP)
                              num_gll = ig_hexa_gll_glonum(ONE_IXP,ONE_IXP,IG_NGLL,elt_num)
                          case(6_IXP)
                              num_gll = ig_hexa_gll_glonum(IG_NGLL,ONE_IXP,IG_NGLL,elt_num)
                          case(7_IXP)
                              num_gll = ig_hexa_gll_glonum(IG_NGLL,IG_NGLL,IG_NGLL,elt_num)
                          case(8_IXP)
                              num_gll = ig_hexa_gll_glonum(ONE_IXP,IG_NGLL,IG_NGLL,elt_num)
                      end select

                      ngll_duplicate = ngll_duplicate + ONE_IXP

                      if (ngll_duplicate > buffer_sizemax) then
                         write(info,'(a)') "error in subroutine init_mpi_buffers: node in contact : size of buffer_gll_duplicate too small"
                         call error_stop(info)
                      endif

                      buffer_gll_duplicate(ngll_duplicate) = num_gll

                  endif
              endif
          enddo

!
!-------->remove duplicates

          call remove_duplicate(buffer_gll_duplicate,ngll_duplicate,tg_cpu_neighbor(icpu_neighbor)%gll_send,ngll_unique)
          
          if (mpi_buffer_sizemax < ngll_unique) mpi_buffer_sizemax = ngll_unique
          tg_cpu_neighbor(icpu_neighbor)%ngll = ngll_unique

!
!-------->ig_mpi_buffer_offset(x) -> give offset in rg_mpi_buffer for xth connected proc (start from 0 !!!)

          if (LG_ASYNC_MPI_COMM) then
             ig_mpi_buffer_offset(icpu_neighbor+ONE_IXP) = ig_mpi_buffer_offset(icpu_neighbor) + tg_cpu_neighbor(icpu_neighbor)%ngll*IG_NDOF
          endif

          if (LG_OUTPUT_DEBUG_FILE)  then
              open(unit=get_newunit(myunit_debug),file=trim(cg_prefix)//".mpi.buffer.cpu."//trim(cg_myrank)//".dbg")
              write(unit=myunit_debug,fmt='(3(a,i6),a)') "from proc ",ig_myrank," to proc ",icpu," : ",ngll_unique," gll points in MPI buffer"
              do i = ONE_IXP,ngll_unique
                  write(unit=myunit_debug,fmt='(i10,3(e14.7,1x))') tg_cpu_neighbor(icpu_neighbor)%gll_send(i)&
                                                                  ,rg_gll_coordinate(ONE_IXP,tg_cpu_neighbor(icpu_neighbor)%gll_send(i))&
                                                                  ,rg_gll_coordinate(TWO_IXP,tg_cpu_neighbor(icpu_neighbor)%gll_send(i))&
                                                                  ,rg_gll_coordinate(THREE_IXP,tg_cpu_neighbor(icpu_neighbor)%gll_send(i))
              enddo
              close(myunit_debug)
          endif

      enddo

!
!---->check if cpus connected together send and receive the same number of gll

      do icpu_neighbor = ONE_IXP,ig_ncpu_neighbor

          icpu      = tg_cpu_neighbor(icpu_neighbor)%icpu
          ngll_send = tg_cpu_neighbor(icpu_neighbor)%ngll 

          call mpi_sendrecv(ngll_send,ONE_IXP,MPI_INTEGER,icpu,89_IXP,ngll_recv,ONE_IXP,MPI_INTEGER,icpu,89_IXP,ig_mpi_comm_simu,statut,ios)

          if (ngll_send /= ngll_recv) then
             write(cl_rank,'(i6.6)') icpu
             write(info,'(a)') "error in subroutine init_mpi_buffers: different number ngll_send and ngll_recv betwen cpu"//trim(cg_myrank)//" and "//trim(cl_rank)
             call error_stop(info)
          endif

      enddo
      
!
!---->set maximum size of MPI buffers between cpu myrank and its neighbors

      ig_mpi_buffer_sizemax = mpi_buffer_sizemax

!
!---->initialize memory of GLL nodes coordinates

      ios = init_array_real(gll_coord_send,ig_ncpu_neighbor,IG_NDOF*mpi_buffer_sizemax,"gll_coord_send")

      ios = init_array_real(gll_coord_recv,ig_ncpu_neighbor,IG_NDOF*mpi_buffer_sizemax,"gll_coord_recv")

!
!---->send/recv 2D buffer's glls coordinates for linked procs

      do icpu_neighbor = ONE_IXP,ig_ncpu_neighbor

          icpu = tg_cpu_neighbor(icpu_neighbor)%icpu    

          !
          !fill coords to send
          do i = ONE_IXP, tg_cpu_neighbor(icpu_neighbor)%ngll
             gll_coord_send((i-ONE_IXP)*IG_NDOF+ONE_IXP  ,icpu_neighbor) = rg_gll_coordinate(ONE_IXP  ,tg_cpu_neighbor(icpu_neighbor)%gll_send(i))
             gll_coord_send((i-ONE_IXP)*IG_NDOF+TWO_IXP  ,icpu_neighbor) = rg_gll_coordinate(TWO_IXP  ,tg_cpu_neighbor(icpu_neighbor)%gll_send(i))
             gll_coord_send((i-ONE_IXP)*IG_NDOF+THREE_IXP,icpu_neighbor) = rg_gll_coordinate(THREE_IXP,tg_cpu_neighbor(icpu_neighbor)%gll_send(i))
          enddo
          
          call mpi_sendrecv(gll_coord_send(ONE_IXP,icpu_neighbor),IG_NDOF*tg_cpu_neighbor(icpu_neighbor)%ngll,MPI_REAL,icpu,90_IXP,gll_coord_recv(ONE_IXP,icpu_neighbor),IG_NDOF*tg_cpu_neighbor(icpu_neighbor)%ngll,MPI_REAL,icpu,90_IXP,ig_mpi_comm_simu,statut,ios)

          if (ios /= ZERO_IXP) then
             write(info,'(a)') "error in subroutine init_mpi_buffers while sending gll coordinates"
             call error_stop(info)
          endif

      enddo

!
!---->create receive buffer

      do icpu_neighbor = ONE_IXP,ig_ncpu_neighbor

         call create_gll_buffer_recv(gll_coord_send(ONE_IXP,icpu_neighbor),gll_coord_recv(ONE_IXP,icpu_neighbor),tg_cpu_neighbor(icpu_neighbor)%ngll,tg_cpu_neighbor(icpu_neighbor)%gll_recv,tg_cpu_neighbor(icpu_neighbor)%gll_send,mpi_buffer_sizemax)

      enddo

!
!---->each cpu writes a file which contains their communication volume

      if (LG_OUTPUT_DEBUG_FILE)  then

         octet_sent_total = ZERO_RXP

         open(unit=get_newunit(myunit_debug),file=trim(cg_prefix)//".mpi.buffer.volume.cpu."//trim(cg_myrank)//".dbg")

         do icpu_neighbor = ONE_IXP,ig_ncpu_neighbor

            octet_sent = real(IG_NDOF*ngll_unique*4_IXP,kind=RXP)/(1024.0_RXP**2_IXP)

            octet_sent_total = octet_sent_total + octet_sent

            write(unit=myunit_debug,fmt='(a,i6,a,F15.7)') "volume (Mo) sent to proc ",tg_cpu_neighbor(icpu_neighbor)%icpu," : ",octet_sent

         enddo

         write(unit=myunit_debug,fmt='(a,F15.7)') "total =  ",octet_sent_total

         close(myunit_debug)

      endif


!
!---->deallocate arrays

      deallocate(gll_coord_send)
      deallocate(gll_coord_recv)
      deallocate(ig_cpu_neighbor_info)

!
!---->allocate array for mpi buffers

      if (LG_ASYNC_MPI_COMM) then

         ios = init_array_real(rg_mpi_buffer_send,ig_mpi_buffer_offset(ig_ncpu_neighbor+ONE_IXP),"rg_mpi_buffer_send")

         ios = init_array_real(rg_mpi_buffer_recv,ig_mpi_buffer_offset(ig_ncpu_neighbor+ONE_IXP),"rg_mpi_buffer_recv")

      endif

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(a)') "done"
         call flush(IG_LST_UNIT)

      endif

!***********************************************************************************************************************************************************************************
   end subroutine init_mpi_buffers
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine creates GLLs buffers received by cpu myrank from other cpus.
!!Identical GLLs between connected cpus are found by computing minimal distance among GLLs. 
!!Two GLLs from different cpus with the smallest minimal distance are supposed to be identical.
!>@param gll_coord_send : @f$x,y,z@f$-coordinates of GLLs in MPI_send buffers
!>@param gll_coord_recv : @f$x,y,z@f$-coordinates of GLLs in MPI_recv buffers
!>@param ngll        : number of unique common GLLs between cpu myrank and its connected cpus
!>@param gll_recv    : list of GLLs received by cpu myrank from its connected cpus
!>@param gll_send    : list of GLLs sent by cpu myrank to its connected cpus
!>@param max_size    : maximum buffer size for cpu myrank
!***********************************************************************************************************************************************************************************
   subroutine create_gll_buffer_recv(gll_coord_send, gll_coord_recv, ngll, gll_recv, gll_send, max_size)
!***********************************************************************************************************************************************************************************

       use mpi
       
       use mod_global_variables, only :&
                                       IG_NDOF&
                                      ,error_stop

       use mod_init_memory

       implicit none

       integer(kind=IXP), intent(in)                                :: ngll
       integer(kind=IXP), intent(in)                                :: max_size
       integer(kind=IXP), intent(in)              , dimension(ngll) :: gll_send
       integer(kind=IXP), intent(out), allocatable, dimension(:)    :: gll_recv

       real   (kind=RXP), intent(in), dimension(IG_NDOF*max_size)   :: gll_coord_send
       real   (kind=RXP), intent(in), dimension(IG_NDOF*max_size)   :: gll_coord_recv
                                                          
       integer(kind=IXP)                                            :: i
       integer(kind=IXP)                                            :: j
       integer(kind=IXP)                                            :: myindex
       integer(kind=IXP)                                            :: ios
       integer(kind=IXP)                                            :: imin
                                                                    
       real   (kind=RXP)                                            :: xs
       real   (kind=RXP)                                            :: ys
       real   (kind=RXP)                                            :: zs
       real   (kind=RXP)                                            :: xr
       real   (kind=RXP)                                            :: yr
       real   (kind=RXP)                                            :: zr
       real   (kind=RXP)                                            :: dist
       real   (kind=RXP)                                            :: mindist
                                                          
       ios = init_array_int(gll_recv,ngll,"gll_recv")

ext:   do i = ONE_IXP,ngll

           mindist = huge(dist)
           imin    = ZERO_IXP
           xr      = gll_coord_recv((i-ONE_IXP)*IG_NDOF+ONE_IXP)
           yr      = gll_coord_recv((i-ONE_IXP)*IG_NDOF+TWO_IXP)
           zr      = gll_coord_recv((i-ONE_IXP)*IG_NDOF+THREE_IXP)

int:       do j = ONE_IXP,ngll

               xs = gll_coord_send((j-ONE_IXP)*IG_NDOF+ONE_IXP)
               ys = gll_coord_send((j-ONE_IXP)*IG_NDOF+TWO_IXP)
               zs = gll_coord_send((j-ONE_IXP)*IG_NDOF+THREE_IXP)

               dist = sqrt((xs-xr)**TWO_IXP + (ys-yr)**TWO_IXP + (zs-zr)**TWO_IXP)

               if ( dist < EPSILON_MACHINE_RXP ) then
                   gll_recv(i) = gll_send(j)
                   cycle ext
                elseif (dist < mindist) then 
                    mindist = dist
                    myindex = j
               endif

           enddo int

           gll_recv(i) = gll_send(myindex)

       enddo ext

       return

!***********************************************************************************************************************************************************************************
   end subroutine create_gll_buffer_recv
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine removes duplicated GLLs between cpu myrank and its connected cpus.
!>@param x1 : array with duplicated GLLs
!>@param n  : size of array x1
!>@param x3 : array without duplicated GLLs
!>@param m  : size of array x3
!***********************************************************************************************************************************************************************************
   subroutine remove_duplicate(x1,n,x3,m)
!***********************************************************************************************************************************************************************************
       
       implicit none
       
       integer(kind=IXP), intent( in)                            :: n  
       integer(kind=IXP), intent( in)             , dimension(n) :: x1 
       integer(kind=IXP), intent(out), allocatable, dimension(:) :: x3 
       integer(kind=IXP), intent(out)                            :: m 
                                                      
       integer(kind=IXP)                          , dimension(n) :: x2 !array without duplicate (size n)
       integer(kind=IXP)                                         :: i
       integer(kind=IXP)                                         :: j

       m     = ONE_IXP

       x2(ONE_IXP) = x1(ONE_IXP)

outer: do i = TWO_IXP,n
           do j = ONE_IXP,m
               if (x2(j) == x1(i)) then
!
!----------------->Found a match so start looking again
                   cycle outer
               endif
           enddo
!
!--------->No match found so add it to array x2
           m      = m + ONE_IXP
           x2(m)  = x1(i)
       enddo outer

       allocate(x3(m))

       x3(ONE_IXP:m) = x2(ONE_IXP:m)

       return

!***********************************************************************************************************************************************************************************
   end subroutine remove_duplicate
!***********************************************************************************************************************************************************************************

end module mod_init_mpibuffer
