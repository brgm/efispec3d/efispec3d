!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to compute global @f$x,y,z@f$-coordinates of a given point in the physical domain from its local @f$\xi,\eta,\zeta@f$-coordinates

!>@brief
!!This module contains subroutines to compute global @f$x,y,z@f$-coordinates of a given point in the physical domain from its local @f$\xi,\eta,\zeta@f$-coordinates
module mod_coordinate
   
   use mpi

   use mod_precision

   use mod_init_memory
   
   implicit none

   private

   public  :: compute_hexa_point_coord
   public  :: compute_quad_point_coord
   public  :: compute_quad_point_coord_z
   public  :: get_gnode_coord
   public  :: compute_edge_min
   public  :: compute_point3d_local_coordinate

   interface get_gnode_coord

      module procedure get_gnode_coord_rank2

   end interface get_gnode_coord

   interface compute_edge_min

      module procedure compute_edge_min_hexa

   end interface compute_edge_min

   contains

!
!
!>@brief subroutine to compute @f$x,y,z@f$-coordinates of a point knowing to which hexahedron element it belongs and its local coordinates @f$\xi,\eta,\zeta@f$.
!>@param ihexa: hexahedron element number in cpu myrank
!>@param xi   : @f$\xi  @f$ local coordinate where to compute global coordinates
!>@param eta  : @f$\eta @f$ local coordinate where to compute global coordinates
!>@param zeta : @f$\zeta@f$ local coordinate where to compute global coordinates
!>@param x    : @f$x@f$-coordinate
!>@param y    : @f$y@f$-coordinate
!>@param z    : @f$z@f$-coordinate
!***********************************************************************************************************************************************************************************
   subroutine compute_hexa_point_coord(ihexa,xi,eta,zeta,x,y,z)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      ig_hexa_nnode&
                                     ,ig_hexa_gnode_xiloc&
                                     ,ig_hexa_gnode_etloc&
                                     ,ig_hexa_gnode_etloc&
                                     ,ig_hexa_gnode_zeloc&
                                     ,ig_line_nnode&
                                     ,rg_gnode_x&
                                     ,rg_gnode_y&
                                     ,rg_gnode_z&
                                     ,ig_hexa_gnode_glonum

      use mod_lagrange       , only : lagrap_geom

      implicit none

      integer(kind=IXP), intent( in) :: ihexa
      real   (kind=RXP), intent( in) :: xi
      real   (kind=RXP), intent( in) :: eta
      real   (kind=RXP), intent( in) :: zeta
      real   (kind=RXP), intent(out) :: x
      real   (kind=RXP), intent(out) :: y
      real   (kind=RXP), intent(out) :: z

      real   (kind=RXP)              :: lag_xi
      real   (kind=RXP)              :: lag_et
      real   (kind=RXP)              :: lag_ze
      real   (kind=RXP)              :: xgnode
      real   (kind=RXP)              :: ygnode
      real   (kind=RXP)              :: zgnode
 
      integer(kind=IXP)              :: m

      x = ZERO_RXP
      y = ZERO_RXP
      z = ZERO_RXP

      do m = ONE_IXP,ig_hexa_nnode

         lag_xi = lagrap_geom(ig_hexa_gnode_xiloc(m),xi  ,ig_line_nnode)
         lag_et = lagrap_geom(ig_hexa_gnode_etloc(m),eta ,ig_line_nnode)
         lag_ze = lagrap_geom(ig_hexa_gnode_zeloc(m),zeta,ig_line_nnode)

         xgnode = rg_gnode_x(ig_hexa_gnode_glonum(m,ihexa))
         ygnode = rg_gnode_y(ig_hexa_gnode_glonum(m,ihexa))
         zgnode = rg_gnode_z(ig_hexa_gnode_glonum(m,ihexa))
                
         x      = x + lag_xi*lag_et*lag_ze*xgnode
         y      = y + lag_xi*lag_et*lag_ze*ygnode
         z      = z + lag_xi*lag_et*lag_ze*zgnode

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine compute_hexa_point_coord
!***********************************************************************************************************************************************************************************

!
!
!>@brief subroutine to compute @f$x,y,z@f$-coordinates of a point knowing to which quadrangle element it belongs and its local coordinates @f$\xi,\eta@f$.
!>@param iquad: quadrangle element number in cpu myrank
!>@param xi   : @f$\xi  @f$ local coordinate where to compute global coordinates
!>@param eta  : @f$\eta @f$ local coordinate where to compute global coordinates
!>@param x    : @f$x@f$-coordinate
!>@param y    : @f$y@f$-coordinate
!>@param z    : @f$z@f$-coordinate
!***********************************************************************************************************************************************************************************
   subroutine compute_quad_point_coord(iquad,xi,eta,x,y,z)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      ig_quad_nnode&
                                     ,ig_quad_gnode_xiloc&
                                     ,ig_quad_gnode_etloc&
                                     ,ig_line_nnode&
                                     ,rg_gnode_x&
                                     ,rg_gnode_y&
                                     ,rg_gnode_z&
                                     ,ig_quadf_gnode_glonum

      use mod_lagrange       , only : lagrap_geom

      implicit none

      integer(kind=IXP), intent( in) :: iquad
      real   (kind=RXP), intent( in) :: xi
      real   (kind=RXP), intent( in) :: eta
      real   (kind=RXP), intent(out) :: x
      real   (kind=RXP), intent(out) :: y
      real   (kind=RXP), intent(out) :: z

      real   (kind=RXP)              :: lag_xi
      real   (kind=RXP)              :: lag_et
      real   (kind=RXP)              :: xgnode
      real   (kind=RXP)              :: ygnode
      real   (kind=RXP)              :: zgnode
 
      integer(kind=IXP)              :: m

      x = ZERO_RXP
      y = ZERO_RXP
      z = ZERO_RXP

      do m = ONE_IXP,ig_quad_nnode

         lag_xi = lagrap_geom(ig_quad_gnode_xiloc(m),xi ,ig_line_nnode)
         lag_et = lagrap_geom(ig_quad_gnode_etloc(m),eta,ig_line_nnode)

         xgnode = rg_gnode_x(ig_quadf_gnode_glonum(m,iquad))
         ygnode = rg_gnode_y(ig_quadf_gnode_glonum(m,iquad))
         zgnode = rg_gnode_z(ig_quadf_gnode_glonum(m,iquad))
                
         x      = x + lag_xi*lag_et*xgnode
         y      = y + lag_xi*lag_et*ygnode
         z      = z + lag_xi*lag_et*zgnode

      enddo

      return

!***********************************************************************************************************************************************************************************
   end subroutine compute_quad_point_coord
!***********************************************************************************************************************************************************************************

!
!
!>@brief function to compute @f$z@f$-coordinates of a point knowing to which quadrangle element it belongs and its local coordinates @f$\xi,\eta@f$.
!>@param iquad: quadrangle element number in cpu myrank
!>@param xi   : @f$\xi @f$ local coordinate where to compute global coordinates
!>@param eta  : @f$\eta@f$ local coordinate where to compute global coordinates
!***********************************************************************************************************************************************************************************
   function compute_quad_point_coord_z(quad_gnode_glonum,iquad,xi,eta) result(z)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      ig_quad_nnode&
                                     ,rg_gnode_z&
                                     ,ig_line_nnode&
                                     ,ig_quad_gnode_xiloc&
                                     ,ig_quad_gnode_etloc
      
      use mod_lagrange       , only : lagrap_geom

      implicit none
      
      integer(kind=IXP), intent(in), dimension(:,:) :: quad_gnode_glonum
      integer(kind=IXP), intent(in)                 :: iquad
      real   (kind=RXP), intent(in)                 :: xi
      real   (kind=RXP), intent(in)                 :: eta
                                           
      real   (kind=RXP)                             :: lag_xi
      real   (kind=RXP)                             :: lag_et
      real   (kind=RXP)                             :: zgnode
      real   (kind=RXP)                             :: z
      integer(kind=IXP)                             :: m

      z = ZERO_RXP

      do m = ONE_IXP,ig_quad_nnode
   
        lag_xi = lagrap_geom(ig_quad_gnode_xiloc(m),xi ,ig_line_nnode)
        lag_et = lagrap_geom(ig_quad_gnode_etloc(m),eta,ig_line_nnode)
   
        zgnode = rg_gnode_z(quad_gnode_glonum(m,iquad))
        
        z      = z + lag_xi*lag_et*zgnode
   
      enddo

      return
!***********************************************************************************************************************************************************************************
   end function compute_quad_point_coord_z
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to get @f$x@f$ or @f$y@f$ or @f$z@f$ coordinates of a list of geometric nodes of the mesh
!>@param        gnode: list of geometric nodes for which coordinates need to be obtained from array coord
!>@param        coord: array of coordinates (@f$x@f$ or @f$y@f$ or @f$z@f$)
!>@return gnode_coord: coordinates of geometric nodes
!***********************************************************************************************************************************************************************************
   subroutine get_gnode_coord_rank2(gnode,coord,gnode_coord)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), intent( in), dimension(:,:) :: gnode
      real   (kind=RXP), intent( in), dimension(:)   :: coord
      real   (kind=RXP), intent(out), dimension(:,:) :: gnode_coord

      integer(kind=IXP)                              :: n1
      integer(kind=IXP)                              :: n2
      integer(kind=IXP)                              :: i
      integer(kind=IXP)                              :: j
      integer(kind=IXP)                              :: k

      n1 = size(gnode,ONE_IXP)
      n2 = size(gnode,TWO_IXP)

      do i = ONE_IXP,n2

         do j = ONE_IXP,n1

            k = gnode(j,i)

            gnode_coord(j,i) = coord(k)

         enddo

      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine get_gnode_coord_rank2
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute the minimum size of the edge of hexahedron elements 
!>@param hexa_gnode : connectivity array of geometric nodes of hexahedron elements
!>@param x          :  @f$x@f$ coordinates of geometric nodes
!>@param y          :  @f$y@f$ coordinates of geometric nodes
!>@param z          :  @f$z@f$ coordinates of geometric nodes
!>@return           : minimum size of the edge of all hexahedron elements in array hexa_gnode
!***********************************************************************************************************************************************************************************
   real(kind=RXP) function compute_edge_min_hexa(hexa_gnode,x,y,z)
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP), dimension(:,:), intent( in) :: hexa_gnode
      real   (kind=RXP), dimension(:)  , intent( in) :: x
      real   (kind=RXP), dimension(:)  , intent( in) :: y
      real   (kind=RXP), dimension(:)  , intent( in) :: z

      integer(kind=IXP), dimension(:), allocatable   :: nodes
      integer(kind=IXP)                              :: nnode
      integer(kind=IXP)                              :: nhexa
      integer(kind=IXP)                              :: inode
      integer(kind=IXP)                              :: ihexa
      integer(kind=IXP)                              :: ios

      real   (kind=RXP), dimension(12_IXP)           :: edge
      real   (kind=RXP)                              :: edge_min

      nnode = size(hexa_gnode,ONE_IXP)
      nhexa = size(hexa_gnode,TWO_IXP)

      ios = init_array_int(nodes,nnode,"module_coordinate:compute_edge_min_hexa:nodes")

      compute_edge_min_hexa = huge(compute_edge_min_hexa)

      do ihexa = ONE_IXP,nhexa

!
!------->get global node number
         do inode = ONE_IXP,nnode

            nodes(inode) = hexa_gnode(inode,ihexa)

         enddo

!
!------->compute size of the edges
         edge(1_IXP ) = sqrt( (x(nodes(1_IXP)) - x(nodes(2_IXP)))**TWO_IXP + (y(nodes(1_IXP)) - y(nodes(2_IXP)))**TWO_IXP + (z(nodes(1_IXP)) - z(nodes(2_IXP)))**TWO_IXP )
         edge(2_IXP ) = sqrt( (x(nodes(2_IXP)) - x(nodes(3_IXP)))**TWO_IXP + (y(nodes(2_IXP)) - y(nodes(3_IXP)))**TWO_IXP + (z(nodes(2_IXP)) - z(nodes(3_IXP)))**TWO_IXP )
         edge(3_IXP ) = sqrt( (x(nodes(3_IXP)) - x(nodes(4_IXP)))**TWO_IXP + (y(nodes(3_IXP)) - y(nodes(4_IXP)))**TWO_IXP + (z(nodes(3_IXP)) - z(nodes(4_IXP)))**TWO_IXP )
         edge(4_IXP ) = sqrt( (x(nodes(1_IXP)) - x(nodes(4_IXP)))**TWO_IXP + (y(nodes(1_IXP)) - y(nodes(4_IXP)))**TWO_IXP + (z(nodes(1_IXP)) - z(nodes(4_IXP)))**TWO_IXP )
         edge(5_IXP ) = sqrt( (x(nodes(5_IXP)) - x(nodes(6_IXP)))**TWO_IXP + (y(nodes(5_IXP)) - y(nodes(6_IXP)))**TWO_IXP + (z(nodes(5_IXP)) - z(nodes(6_IXP)))**TWO_IXP )
         edge(6_IXP ) = sqrt( (x(nodes(6_IXP)) - x(nodes(7_IXP)))**TWO_IXP + (y(nodes(6_IXP)) - y(nodes(7_IXP)))**TWO_IXP + (z(nodes(6_IXP)) - z(nodes(7_IXP)))**TWO_IXP )
         edge(7_IXP ) = sqrt( (x(nodes(7_IXP)) - x(nodes(8_IXP)))**TWO_IXP + (y(nodes(7_IXP)) - y(nodes(8_IXP)))**TWO_IXP + (z(nodes(7_IXP)) - z(nodes(8_IXP)))**TWO_IXP )
         edge(8_IXP ) = sqrt( (x(nodes(5_IXP)) - x(nodes(8_IXP)))**TWO_IXP + (y(nodes(5_IXP)) - y(nodes(8_IXP)))**TWO_IXP + (z(nodes(5_IXP)) - z(nodes(8_IXP)))**TWO_IXP )
         edge(9_IXP ) = sqrt( (x(nodes(1_IXP)) - x(nodes(5_IXP)))**TWO_IXP + (y(nodes(1_IXP)) - y(nodes(5_IXP)))**TWO_IXP + (z(nodes(1_IXP)) - z(nodes(5_IXP)))**TWO_IXP )
         edge(10_IXP) = sqrt( (x(nodes(2_IXP)) - x(nodes(6_IXP)))**TWO_IXP + (y(nodes(2_IXP)) - y(nodes(6_IXP)))**TWO_IXP + (z(nodes(2_IXP)) - z(nodes(6_IXP)))**TWO_IXP )
         edge(11_IXP) = sqrt( (x(nodes(3_IXP)) - x(nodes(7_IXP)))**TWO_IXP + (y(nodes(3_IXP)) - y(nodes(7_IXP)))**TWO_IXP + (z(nodes(3_IXP)) - z(nodes(7_IXP)))**TWO_IXP )
         edge(12_IXP) = sqrt( (x(nodes(4_IXP)) - x(nodes(8_IXP)))**TWO_IXP + (y(nodes(4_IXP)) - y(nodes(8_IXP)))**TWO_IXP + (z(nodes(4_IXP)) - z(nodes(8_IXP)))**TWO_IXP )

!
!------->min edge among the 12 edges of the hexahedron element 'ihexa'
         edge_min = minval(edge)

!
!------->min edge among all hexa
         compute_edge_min_hexa = min(compute_edge_min_hexa,edge_min)

      enddo

      return
!***********************************************************************************************************************************************************************************
   end function compute_edge_min_hexa
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to compute @f$\xi,\eta,\zeta@f$-coordinates of a point knowing to which hexahedron element it belongs and its global coordinates @f$x, y, z@f$.
!>@param  p: type(point3d)
!>@return p
!***********************************************************************************************************************************************************************************
   subroutine compute_point3d_local_coordinate(p)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      point3d&
                                     ,rg_gll_abscissa

      use mod_jacobian        , only : compute_hexa_jacobian
   
      implicit none
   
      type(point3d), intent(inout) :: p
      
      real(kind=RXP)               :: eps
      real(kind=RXP)               :: dmin
      real(kind=RXP)               :: xisol
      real(kind=RXP)               :: etsol
      real(kind=RXP)               :: zesol
      real(kind=RXP)               :: newx
      real(kind=RXP)               :: newy
      real(kind=RXP)               :: newz
      real(kind=RXP)               :: dxidx
      real(kind=RXP)               :: dxidy
      real(kind=RXP)               :: dxidz
      real(kind=RXP)               :: detdx
      real(kind=RXP)               :: detdy
      real(kind=RXP)               :: detdz
      real(kind=RXP)               :: dzedx
      real(kind=RXP)               :: dzedy
      real(kind=RXP)               :: dzedz
      real(kind=RXP)               :: dx
      real(kind=RXP)               :: dy
      real(kind=RXP)               :: dz
      real(kind=RXP)               :: dxi
      real(kind=RXP)               :: det
      real(kind=RXP)               :: dze
      real(kind=RXP)               :: deter

      integer(kind=IXP)            :: ihexa
      integer(kind=IXP)            :: iter
                                                     
      integer(kind=IXP), parameter :: ITER_MAX=100_IXP
                              
!
!---->initialize element number and coordinates (needed if dmin < eps)

      ihexa =  p%iel
      newx  =  p%x
      newy  =  p%y
      newz  =  p%z
      ihexa =  p%iel  
      eps   = 1.0e-3_RXP
      iter  = ZERO_IXP

!
!---->set the first guessed solution 

      xisol = rg_gll_abscissa(p%mgll)
      etsol = rg_gll_abscissa(p%lgll)
      zesol = rg_gll_abscissa(p%kgll)
      dmin  = p%dmin
   
!      
!---->solve the nonlinear system to find local coordinates

      do while(dmin > eps)

          p%xi = xisol
          p%et = etsol
          p%ze = zesol
   
         call compute_hexa_jacobian(ihexa,xisol,etsol,zesol,dxidx,dxidy,dxidz,detdx,detdy,detdz,dzedx,dzedy,dzedz,deter)
   
         call compute_hexa_point_coord(ihexa,xisol,etsol,zesol,newx,newy,newz)

         dmin  = sqrt( (newx-p%x)**TWO_IXP + (newy-p%y)**TWO_IXP + (newz-p%z)**TWO_IXP )

!
!------->prepare next iteration by keeping local solution within interval [-1:+1]

         iter  = iter + ONE_IXP
   
         dx    = p%x - newx
         dy    = p%y - newy
         dz    = p%z - newz

         dxi   = dxidx*dx + dxidy*dy + dxidz*dz
         det   = detdx*dx + detdy*dy + detdz*dz
         dze   = dzedx*dx + dzedy*dy + dzedz*dz

         xisol = xisol + dxi
         etsol = etsol + det
         zesol = zesol + dze

         if (xisol > +ONE_RXP) xisol = +ONE_RXP
         if (xisol < -ONE_RXP) xisol = -ONE_RXP
         if (etsol > +ONE_RXP) etsol = +ONE_RXP
         if (etsol < -ONE_RXP) etsol = -ONE_RXP
         if (zesol > +ONE_RXP) zesol = +ONE_RXP
         if (zesol < -ONE_RXP) zesol = -ONE_RXP   
   
         if (mod(iter,ITER_MAX) == ZERO_IXP) then

            eps   = eps*TWO_RXP
            xisol = rg_gll_abscissa(p%mgll)
            etsol = rg_gll_abscissa(p%lgll)
            zesol = rg_gll_abscissa(p%kgll)
            dmin  = p%dmin

         endif
   
      enddo
   
!
!---->update p

      p%dmin = dmin
      p%x    = newx
      p%y    = newy
      p%z    = newz
    
      return

!***********************************************************************************************************************************************************************************
   end subroutine compute_point3d_local_coordinate
!***********************************************************************************************************************************************************************************

end module mod_coordinate
