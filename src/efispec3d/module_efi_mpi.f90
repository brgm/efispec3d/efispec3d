!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to compute global @f$x,y,z@f$-coordinates of a given point in the physical domain from its local @f$\xi,\eta,\zeta@f$-coordinates

!>@brief
!!This module contains subroutines to compute global @f$x,y,z@f$-coordinates of a given point in the physical domain from its local @f$\xi,\eta,\zeta@f$-coordinates
module mod_efi_mpi
   
   use mpi

   use mod_precision

   use mod_global_variables

   use mod_init_memory
   
   implicit none

   private

   public  :: init_mpi
   private :: init_mpi_simu
   public  :: init_simu_folder
   public  :: efi_mpi_gatherv
   public  :: efi_mpi_comm_split

   interface efi_mpi_gatherv

      module procedure efi_mpi_gatherv_int
      module procedure efi_mpi_gatherv_real

   end interface

   contains

!>@brief
!!This subroutine initializes MPI WORLD COMMUNICATOR
!>@return mod_global_variables::ig_ncpu_world
!>@return mod_global_variables::ig_myrank_world
!>@return mod_global_variables::cg_myrank_world
!>@return mod_global_variables::ig_ncpu
!>@return mod_global_variables::ig_myrank
!>@return mod_global_variables::cg_myrank
!>@return mod_global_variables::ig_nsimu
!>@return mod_global_variables::ig_simu
!>@return mod_global_variables::cg_nsimu
!>@return mod_global_variables::cg_cpu_name
!>@return mod_global_variables::ig_cpu_name_len
!>@return mod_global_variables::ig_mpi_nboctet_real
!>@return mod_global_variables::ig_mpi_nboctet_int
!***********************************************************************************************************************************************************************************
   subroutine init_mpi()
!***********************************************************************************************************************************************************************************

      implicit none

      integer(kind=IXP)  :: ios
      integer(kind=IXP)  :: narg

      character(len=CIL) :: info

!
!
!*************************************************************************
!---->init MPI_COMM_WORLD 
!*************************************************************************

      call mpi_init(ios)

      if (ios /= ZERO_IXP) then

         write(info,'(a)') "error while initializing mpi"

         call error_stop(info)

      endif
      
!
!---->get number of cpus in MPI_COMM_WORLD

      call mpi_comm_size(MPI_COMM_WORLD,ig_ncpu_world,ios)

      write(cg_ncpu_world,'(i6.6)') ig_ncpu_world
      
      call mpi_comm_rank(MPI_COMM_WORLD,ig_myrank_world,ios)

      write(cg_myrank_world,'(i6.6)') ig_myrank_world
      
      call mpi_get_processor_name(cg_cpu_name,ig_cpu_name_len,ios)

!
!---->init type size

      call mpi_type_size(MPI_REAL   ,ig_mpi_nboctet_real,ios)

      call mpi_type_size(MPI_INTEGER,ig_mpi_nboctet_int ,ios)

!
!
!*************************************************************************
!---->init mpi_comm_simu 
!*************************************************************************

!
!---->default: EFISPEC is set to single simulation mode when no argument is provided. number of cpu of a simulation set to ig_ncpu_world

      ig_ncpu = ig_ncpu_world

      write(cg_ncpu,'(i6.6)') ig_ncpu
   
!
!---->check arguments

      narg = ZERO_IXP

      narg = command_argument_count()


!---->EFISPEC is set to post-processing mode, uncertainty quantification single simulation mode, or uncertainty quantification multi simulations mode

      if (narg == 2_IXP) then

         call get_command_argument(1_IXP,cg_efi_mode)

         selectcase(trim(adjustl(cg_efi_mode)))

            case("uqss") !run only one uncertainty quantification simulation given by 'cg_uq_simu'

               call get_command_argument(2_IXP,cg_uq_simu)

               read(cg_uq_simu,*) ig_uq_simu

               write(cg_uq_simu ,'(i6.6)') ig_uq_simu !ensure that cg_uq_simu has zero-padding

            case("uqms")  !run all uncertainty quantification simulations

               call get_command_argument(2_IXP,cg_ncpu)

               read(cg_ncpu,*) ig_ncpu

         endselect

      endif

      call init_mpi_simu(ig_myrank_world,ig_ncpu_world,ig_ncpu,ig_mpi_comm_simu,ig_myrank,cg_myrank,ig_nsimu,ig_simu)

      write(cg_nsimu,'(i6.6)') ig_nsimu
      write(cg_simu ,'(i6.6)') ig_simu

      if (cg_efi_mode == "uqms") then

         ig_uq_simu = ig_simu

         write(cg_uq_simu ,'(i6.6)') ig_uq_simu

      endif

      cg_prefix = get_prefix()

      return

!***********************************************************************************************************************************************************************************
   end subroutine init_mpi
!***********************************************************************************************************************************************************************************

!>@brief
!!This subroutine initializes MPI SIMU COMMUNICATOR
!>@param  rank_world    : rank of cpu in MPI_COMM_WORLD
!>@param  ncpu_world    : number of cpus in MPI_COMM_WORLD
!>@param  ncpu_simu     : number of cpus in ig_mpi_comm_simu (i.e., number of cpus for one simulation, mod_global_variables::ig_ncpu)
!>@return mpi_comm_simu : local mpi communicator of a simulation (mod_global_variables::ig_mpi_comm_simu)
!>@return rank_simu     : rank of cpu in ig_mpi_comm_simu (mod_global_variables::ig_myrank)
!>@return c_rank_simu   : character rank of cpu in ig_mpi_comm_simu (mod_global_variables::cg_myrank)
!>@return nsimu         : number of simulation inside communicator ig_mpi_comm_simu (mod_global_variables::ig_nsimu)
!>@return  simu         : simulation number inside communicator ig_mpi_comm_simu (mod_global_variables::ig_simu)
!***********************************************************************************************************************************************************************************
   subroutine init_mpi_simu(rank_world,ncpu_world,ncpu_simu,mpi_comm_simu,rank_simu,c_rank_simu,nsimu,simu)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                      error_stop

      implicit none

      integer(kind=IXP), intent( in)  :: rank_world
      integer(kind=IXP), intent( in)  :: ncpu_world
      integer(kind=IXP), intent( in)  :: ncpu_simu
      integer(kind=IXP), intent(out)  :: mpi_comm_simu
      integer(kind=IXP), intent(out)  :: rank_simu
      character(len=6) , intent(out)  :: c_rank_simu
      integer(kind=IXP), intent(out)  :: nsimu
      integer(kind=IXP), intent(out)  :: simu 

      integer(kind=IXP)               :: ios
      integer(kind=IXP)               :: isimu
      integer(kind=IXP)               :: rank1_simu
      integer(kind=IXP)               :: rankn_simu
      integer(kind=IXP)               :: key
      integer(kind=IXP)               :: ncpu_simu_check

      character(len=CIL)              :: info

      if (mod(ncpu_world,ncpu_simu) /= 0) then

         write(info,'(a)') "error in init_mpi_simu: number of cpu in ig_mpi_comm_simu must be a multiple of number of cpu in ig_mpi_comm_simu"
         call error_stop(info)

      endif

!
!---->set the simulation number of each rank of the MPI_COMM_WORLD communicator

      nsimu = ncpu_world/ncpu_simu

      do isimu = ONE_IXP,nsimu
      
         rank1_simu = (isimu-ONE_IXP)*ncpu_simu
         rankn_simu = (isimu        )*ncpu_simu - ONE_IXP
      
         if ( (rank_world >= rank1_simu) .and. (rank_world <= rankn_simu) ) then
      
            simu = isimu
      
         endif
      
      enddo
      
!
!---->split MPI_COMM_WORLD into mpi_comm_simu using 'simu' as the color of splitting

      key = -ONE_IXP
      
      call mpi_comm_split(MPI_COMM_WORLD,simu,key,mpi_comm_simu,ios)

!
!---->sanity check

      call mpi_comm_size(mpi_comm_simu,ncpu_simu_check,ios)

      if (ncpu_simu_check /= ncpu_simu) then

         write(info,'(a)') "error in init_mpi_simu: invalid number of cpu inside mpi_comm_simu"
         call error_stop(info)

      endif

!
!---->get rank inside mpi_comm_simu

      call mpi_comm_rank(mpi_comm_simu,rank_simu,ios)

      write(c_rank_simu,'(i6.6)') rank_simu

      return

!***********************************************************************************************************************************************************************************
   end subroutine init_mpi_simu
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine initializes the directories trees in multi/single simulation modes
!@return : cg_simu_dir
!@return : cg_prefix
!@return : ig_uq_simu
!@return : ig_uq_ndimu
!@return : rg_uq_coeff
!***********************************************************************************************************************************************************************************
   subroutine init_simu_folder()
!***********************************************************************************************************************************************************************************

      use mod_uq, only : init_uq_experience

      implicit none

      integer(kind=IXP)  :: system
      integer(kind=IXP)  :: ios
      character(len=CIL) :: cmd

      if (cg_efi_mode == "ss") then

         cg_simu_dir = "./" !sanity instruction because variable 'cg_simu_dir' is already defined in mod_global_variables

      else !uqss or uqms mode

         cg_simu_dir = "SIMU"//trim(cg_uq_simu)//"/"

!------->cpu0 of mpi_comm_simu creates SIMU directory and copies the input files to its SIMU directory
         if (ig_myrank == ZERO_IXP) then

            write(cmd,'(a)') " mkdir -p "//trim(cg_simu_dir)

            ios = system(cmd)

            cmd = "cp "//trim(adjustl(cg_prefix))//".* prefix "//trim(cg_simu_dir)
      
            ios = system(cmd)

         endif

      endif

!
!---->mpi_barrier is needed to ensure that all cpus can read the freshly copied files

      call mpi_barrier(MPI_COMM_WORLD,ios) !ig_mpi_comm_simu also possible

!
!---->update cg_prefix to SIMU directory or to ./ in case of a single simulation

      cg_prefix = trim(adjustl(cg_simu_dir))//trim(adjustl(cg_prefix))

!
!---->init experience plan

      if ( (cg_efi_mode == "uqss") .or. (cg_efi_mode == "uqms") ) then

         call init_uq_experience(rg_uq_coeff)

         ig_uq_ndim = size(rg_uq_coeff)

      else

         allocate(rg_uq_coeff(1_IXP))!not used but to avoid compiler error with option -check all

      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine init_simu_folder
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to init and perform gatherv of integer values
!>@param  nr   : total number of rank
!>@param  my_r : rank of the cpu
!>@param  ga_r : rank which perform the gatherv
!>@param  nv   : number of values by cpu to be gathered
!>@param  v    : pointer to values to be gathered
!>@param  c    : the mpi communicator to be used
!>@return a    : array containing the values gather from all cpus
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_gatherv_int(nr,my_r,ga_r,nv,v,c,a)
!***********************************************************************************************************************************************************************************

      integer(kind=IXP)                           , intent( in) :: nr
      integer(kind=IXP)                           , intent( in) :: my_r
      integer(kind=IXP)                           , intent( in) :: ga_r
      integer(kind=IXP)                           , intent( in) :: nv
      integer(kind=IXP), dimension(:), pointer    , intent( in) :: v
      integer(kind=IXP)                           , intent( in) :: c
      integer(kind=IXP), dimension(:), allocatable, intent(out) :: a

      integer(kind=IXP), dimension(:), allocatable              :: nv_all_cpus
      integer(kind=IXP), dimension(:), allocatable              :: nv_all_cpus_offset
      integer(kind=IXP), dimension(:), allocatable              :: v_cpu

      integer(kind=IXP)                                         :: sum_nv_all_cpus
      integer(kind=IXP)                                         :: icpu
      integer(kind=IXP)                                         :: ii
      integer(kind=IXP)                                         :: ios

!
!---->cpu 'ga_r' gathers the number of value 'nv' from all cpus.
!     To avoid seg_fault at runtime with compilation option -check all, remove the if conditions when there is memory allocation.

!     if (my_r == ga_r) then

         ios = init_array_int(nv_all_cpus       ,nr,"mod_efi_mpi:efi_mpi_gatherv:nv_all_cpus")

         ios = init_array_int(nv_all_cpus_offset,nr,"mod_efi_mpi:efi_mpi_gatherv:nv_all_cpus_offset")

!     endif

      call mpi_gather(nv,ONE_IXP,MPI_INTEGER,nv_all_cpus,ONE_IXP,MPI_INTEGER,ga_r,c,ios)

!
!---->cpu 'ga_r' init offset array for mpi_gatherv

      if (my_r == ga_r) then

         nv_all_cpus_offset(ONE_IXP) = ZERO_IXP

         do icpu = TWO_IXP,nr

            nv_all_cpus_offset(icpu) = nv_all_cpus_offset(icpu-ONE_IXP) + nv_all_cpus(icpu-ONE_IXP)

         enddo

      endif

! 
!---->cpu 'ga_r' gathers the values 'v' of all cpus

      sum_nv_all_cpus = sum(nv_all_cpus)

!     if (my_r == ga_r) then

         ios = init_array_int(a,sum_nv_all_cpus,"mod_efi_mpi:efi_mpi_gatherv:a")

!     endif

      if (associated(v)) then

         ios = init_array_int(v_cpu,nv,"mod_efi_mpi:efi_mpi_gatherv:v_cpu")

         do ii = ONE_IXP,nv
       
            v_cpu(ii) = v(ii)
       
         enddo

      endif

      call mpi_gatherv(v_cpu,nv,MPI_INTEGER,a,nv_all_cpus,nv_all_cpus_offset,MPI_INTEGER,ga_r,c,ios)

      return

!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_gatherv_int
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to init and perform gatherv of real values
!>@param  nr   : total number of rank
!>@param  my_r : rank of the cpu
!>@param  ga_r : rank which perform the gatherv
!>@param  nv   : number of values by cpu to be gathered
!>@param  v    : pointer to values to be gathered
!>@param  c    : the mpi communicator to be used
!>@return a    : array containing the values gather from all cpus
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_gatherv_real(nr,my_r,ga_r,nv,v,c,a)
!***********************************************************************************************************************************************************************************

      integer(kind=IXP)                           , intent( in) :: nr
      integer(kind=IXP)                           , intent( in) :: my_r
      integer(kind=IXP)                           , intent( in) :: ga_r
      integer(kind=IXP)                           , intent( in) :: nv
      real   (kind=RXP), dimension(:), pointer    , intent( in) :: v
      integer(kind=IXP)                           , intent( in) :: c
      real   (kind=RXP), dimension(:), allocatable, intent(out) :: a

      integer(kind=IXP), dimension(:), allocatable              :: nv_all_cpus
      integer(kind=IXP), dimension(:), allocatable              :: nv_all_cpus_offset
      real   (kind=RXP), dimension(:), allocatable              :: v_cpu

      integer(kind=IXP)                                         :: sum_nv_all_cpus
      integer(kind=IXP)                                         :: icpu
      integer(kind=IXP)                                         :: ii
      integer(kind=IXP)                                         :: ios

!
!---->cpu 'ga_r' gathers the number of value 'nv' from all cpus.
!     To avoid seg_fault at runtime with compilation option -check all, remove the if conditions when there is memory allocation.

!     if (my_r == ga_r) then

         ios = init_array_int(nv_all_cpus       ,nr,"mod_efi_mpi:efi_mpi_gatherv:nv_all_cpus")

         ios = init_array_int(nv_all_cpus_offset,nr,"mod_efi_mpi:efi_mpi_gatherv:nv_all_cpus_offset")

!     endif

      call mpi_gather(nv,ONE_IXP,MPI_INTEGER,nv_all_cpus,ONE_IXP,MPI_INTEGER,ga_r,c,ios)

!
!---->cpu 'ga_r' init offset array for mpi_gatherv

      if (my_r == ga_r) then

         nv_all_cpus_offset(ONE_IXP) = ZERO_IXP

         do icpu = TWO_IXP,nr

            nv_all_cpus_offset(icpu) = nv_all_cpus_offset(icpu-ONE_IXP) + nv_all_cpus(icpu-ONE_IXP)

         enddo

      endif

! 
!---->cpu 'ga_r' gathers the values 'v' of all cpus

      sum_nv_all_cpus = sum(nv_all_cpus)

!     if (my_r == ga_r) then

         ios = init_array_real(a,sum_nv_all_cpus,"mod_efi_mpi:efi_mpi_gatherv:a")

!     endif

      if (associated(v)) then

         ios = init_array_real(v_cpu,nv,"mod_efi_mpi:efi_mpi_gatherv:v_cpu")

         do ii = ONE_IXP,nv
       
            v_cpu(ii) = v(ii)
       
         enddo

      endif

      call mpi_gatherv(v_cpu,nv,MPI_REAL,a,nv_all_cpus,nv_all_cpus_offset,MPI_REAL,ga_r,c,ios)

      return

!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_gatherv_real
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to init a new communicator for cpu whose n > 0
!>@param  n        : number of value to write
!>@param  c        : color
!>@param  old_comm : old mpi communicator
!>@return new_comm : new mpi communicator
!***********************************************************************************************************************************************************************************
   subroutine efi_mpi_comm_split(n,c,old_comm,new_comm)
!***********************************************************************************************************************************************************************************

      integer(kind=IXP)                           , intent( in) :: n
      integer(kind=IXP)                           , intent( in) :: c
      integer(kind=IXP)                           , intent( in) :: old_comm
      integer(kind=IXP)                           , intent(out) :: new_comm

      integer(kind=IXP)                                         :: color
      integer(kind=IXP)                                         :: key
      integer(kind=IXP)                                         :: ios

      if ( n > ZERO_IXP ) then

         color = c

      else

         color = MPI_UNDEFINED

      ENDIF

      key = -ONE_IXP

      call mpi_comm_split(old_comm,color,key,new_comm,ios)

      return

!***********************************************************************************************************************************************************************************
   end subroutine efi_mpi_comm_split
!***********************************************************************************************************************************************************************************

end module mod_efi_mpi
