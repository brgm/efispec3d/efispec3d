!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to assemble forces between cpu myrank and its neighbor cpus.

!>@brief
!!This module contains subroutines to assemble forces between cpu myrank and its neighbor cpus by synchrone or asynchrone MPI communications.
module mod_mpi_assemble

   use mpi

   use mod_precision

   implicit none

   public :: assemble_force_sync_comm
   public :: assemble_force_async_comm_init
   public :: assemble_force_async_comm_end

   contains

!
!
!>@brief subroutine to assemble forces between connected cpus by MPI synchrone communications.
!>@return mod_global_variables::rg_gll_acceleration
!***********************************************************************************************************************************************************************************
   subroutine assemble_force_sync_comm()
!***********************************************************************************************************************************************************************************
   
      use mpi
      
      use mod_global_variables, only :&
                                      tg_cpu_neighbor&
                                     ,rg_gll_acceleration&
                                     ,IG_NDOF&
                                     ,ig_mpi_comm_simu&
                                     ,ig_mpi_buffer_sizemax&
                                     ,ig_ncpu_neighbor
      
      implicit none
      
      real(kind=RXP)  , dimension(IG_NDOF,ig_mpi_buffer_sizemax)                  :: dummyr
      real(kind=RXP)  , dimension(IG_NDOF,ig_mpi_buffer_sizemax,ig_ncpu_neighbor) :: dlacce
      
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)                                :: statut
      integer(kind=IXP)                                                           :: igll
      integer(kind=IXP)                                                           :: icon
      integer(kind=IXP)                                                           :: ngll
      integer(kind=IXP)                                                           :: ios

!
!
!*********************************************
!---->fill buffer dlacce with gll to be send
!*********************************************

      do icon = ONE_IXP,ig_ncpu_neighbor
         do igll = ONE_IXP,tg_cpu_neighbor(icon)%ngll
   
            dlacce(ONE_IXP,igll,icon) = rg_gll_acceleration(ONE_IXP,tg_cpu_neighbor(icon)%gll_send(igll))
            dlacce(TWO_IXP,igll,icon) = rg_gll_acceleration(TWO_IXP,tg_cpu_neighbor(icon)%gll_send(igll))
            dlacce(THREE_IXP,igll,icon) = rg_gll_acceleration(THREE_IXP,tg_cpu_neighbor(icon)%gll_send(igll))
   
         enddo
      enddo

!
!
!**************************************************************
!---->synchrone comm: exchange buffers between cpu in contact
!**************************************************************

      do icon = ONE_IXP,ig_ncpu_neighbor
      
         ngll = tg_cpu_neighbor(icon)%ngll
         
         call mpi_sendrecv(dlacce(ONE_IXP,ONE_IXP,icon),IG_NDOF*ngll,MPI_REAL,tg_cpu_neighbor(icon)%icpu,300_IXP,dummyr(ONE_IXP,ONE_IXP),IG_NDOF*ngll,MPI_REAL,tg_cpu_neighbor(icon)%icpu,300_IXP,ig_mpi_comm_simu,statut,ios)
         
         do igll = ONE_IXP,ngll
   
            rg_gll_acceleration(ONE_IXP,tg_cpu_neighbor(icon)%gll_recv(igll)) = rg_gll_acceleration(ONE_IXP,tg_cpu_neighbor(icon)%gll_recv(igll)) + dummyr(ONE_IXP,igll)
            rg_gll_acceleration(TWO_IXP,tg_cpu_neighbor(icon)%gll_recv(igll)) = rg_gll_acceleration(TWO_IXP,tg_cpu_neighbor(icon)%gll_recv(igll)) + dummyr(TWO_IXP,igll)
            rg_gll_acceleration(THREE_IXP,tg_cpu_neighbor(icon)%gll_recv(igll)) = rg_gll_acceleration(THREE_IXP,tg_cpu_neighbor(icon)%gll_recv(igll)) + dummyr(THREE_IXP,igll)
   
         enddo
      
      enddo
   
      return

!***********************************************************************************************************************************************************************************
   end subroutine assemble_force_sync_comm
!***********************************************************************************************************************************************************************************

!
!
!>@brief subroutine to initialize forces assembly between connected cpus by MPI asynchrone communications.
!***********************************************************************************************************************************************************************************
   subroutine assemble_force_async_comm_init()
!***********************************************************************************************************************************************************************************
   
      use mpi
   
      use mod_global_variables, only :&
                                      tg_cpu_neighbor&
                                     ,rg_gll_acceleration&
                                     ,IG_NDOF&
                                     ,ig_ncpu&
                                     ,ig_mpi_comm_simu&
                                     ,ig_mpi_buffer_offset&
                                     ,rg_mpi_buffer_send&
                                     ,rg_mpi_buffer_recv&
                                     ,ig_mpi_request_send&
                                     ,ig_mpi_request_recv&
                                     ,ig_mpi_buffer_sizemax&
                                     ,ig_ncpu_neighbor
   
      implicit none
   
      real(kind=RXP), dimension(IG_NDOF,ig_mpi_buffer_sizemax,ig_ncpu_neighbor) :: dlacce
      integer(kind=IXP)                                                        :: igll
      integer(kind=IXP)                                                        :: icon
      integer(kind=IXP)                                                        :: ngll
      integer(kind=IXP)                                                        :: ios
      integer(kind=IXP)                                                        :: dof_offset
      integer(kind=IXP)                                                        :: proc_offset   

!
!
!********************************************************************************************************************************************
!---->fill buffer dlacce with gll to be send
!********************************************************************************************************************************************

      do icon = ONE_IXP,ig_ncpu_neighbor

         do igll = ONE_IXP,tg_cpu_neighbor(icon)%ngll
   
             dlacce(ONE_IXP,igll,icon) = rg_gll_acceleration(ONE_IXP,tg_cpu_neighbor(icon)%gll_send(igll))
             dlacce(TWO_IXP,igll,icon) = rg_gll_acceleration(TWO_IXP,tg_cpu_neighbor(icon)%gll_send(igll))
             dlacce(THREE_IXP,igll,icon) = rg_gll_acceleration(THREE_IXP,tg_cpu_neighbor(icon)%gll_send(igll))
   
         enddo

      enddo

!
!
!*********************************************************************************************************************************************
!---->wait until all send buffers are released
!*********************************************************************************************************************************************

      if ( (ig_ncpu > ONE_IXP) .and. (ig_mpi_request_send(ONE_IXP) /= ZERO_IXP) ) call mpi_Waitall(ig_ncpu_neighbor, ig_mpi_request_send, MPI_STATUSES_IGNORE, ios)

!
!
!********************************************************************************************************************************************
!---->asynchrone comm: exchange buffers between cpu in contact
!********************************************************************************************************************************************

      do icon = ONE_IXP,ig_ncpu_neighbor
   
         ngll        = tg_cpu_neighbor(icon)%ngll
         proc_offset = ig_mpi_buffer_offset(icon)
   
!
!------->fill buffer for cpu myrank

         do igll = ONE_IXP,ngll
   
            !beginning of x buffer = ZERO_IXP
            dof_offset = ZERO_IXP
            rg_mpi_buffer_send(proc_offset+dof_offset+igll) = dlacce(ONE_IXP,igll,icon)
   
            !beginning of y buffer = ngll
            dof_offset = ngll
            rg_mpi_buffer_send(proc_offset+dof_offset+igll) = dlacce(TWO_IXP,igll,icon)
   
            !beginning of z buffer = 2*ngll
            dof_offset = TWO_IXP*ngll
            rg_mpi_buffer_send(proc_offset+dof_offset+igll) = dlacce(THREE_IXP,igll,icon)
   
         enddo
      
         call mpi_Irecv(rg_mpi_buffer_recv(proc_offset+ONE_IXP),IG_NDOF*ngll,MPI_REAL,tg_cpu_neighbor(icon)%icpu,300_IXP,ig_mpi_comm_simu,ig_mpi_request_recv(icon),ios)
         call mpi_Isend(rg_mpi_buffer_send(proc_offset+ONE_IXP),IG_NDOF*ngll,MPI_REAL,tg_cpu_neighbor(icon)%icpu,300_IXP,ig_mpi_comm_simu,ig_mpi_request_send(icon),ios)
   
      enddo
   
      return
!***********************************************************************************************************************************************************************************
   end subroutine assemble_force_async_comm_init
!***********************************************************************************************************************************************************************************

!
!
!>@brief subroutine to finalize forces assembly between connected cpus by MPI asynchrone communications.
!>@return mod_global_variables::rg_gll_acceleration
!***********************************************************************************************************************************************************************************
   subroutine assemble_force_async_comm_end()
!***********************************************************************************************************************************************************************************
   
      use mpi
   
      use mod_global_variables, only :&
                                      tg_cpu_neighbor&
                                     ,rg_gll_acceleration&
                                     ,IG_NDOF&
                                     ,ig_mpi_buffer_offset&
                                     ,rg_mpi_buffer_recv&
                                     ,ig_mpi_request_recv&
                                     ,ig_ncpu_neighbor
   
      implicit none
   
      integer(kind=IXP)                            :: igll
      integer(kind=IXP)                            :: ngll
      integer(kind=IXP)                            :: ios
      integer(kind=IXP)                            :: dof_offset
      integer(kind=IXP)                            :: proc_offset
      integer(kind=IXP)                            :: nb_msg
      integer(kind=IXP)                            :: r_index
      integer(kind=IXP), dimension(MPI_STATUS_SIZE) :: statut
      
      nb_msg = ZERO_IXP
   
      do while(nb_msg < ig_ncpu_neighbor)
!
!------->wait for any received message

         call mpi_waitany(ig_ncpu_neighbor, ig_mpi_request_recv, r_index, statut, ios)
   
         nb_msg      = nb_msg + ONE_IXP
         ngll        = tg_cpu_neighbor(r_index)%ngll
         proc_offset = ig_mpi_buffer_offset(r_index)

!
!------->sum contributions for this proc
   
         !beginning of x buffer = ZERO_IXP
         dof_offset = ZERO_IXP
         do igll = ONE_IXP,ngll
            rg_gll_acceleration(ONE_IXP,tg_cpu_neighbor(r_index)%gll_recv(igll)) = rg_gll_acceleration(ONE_IXP,tg_cpu_neighbor(r_index)%gll_recv(igll)) + rg_mpi_buffer_recv(proc_offset+dof_offset+igll)
         enddo
   
         !beginning of y buffer = ngll
         dof_offset = ngll
         do igll = ONE_IXP,ngll
            rg_gll_acceleration(TWO_IXP,tg_cpu_neighbor(r_index)%gll_recv(igll)) = rg_gll_acceleration(TWO_IXP,tg_cpu_neighbor(r_index)%gll_recv(igll)) + rg_mpi_buffer_recv(proc_offset+dof_offset+igll)
         enddo
   
         !beginning of z buffer = 2*ngll
         dof_offset = TWO_IXP*ngll
         do igll = ONE_IXP,ngll
            rg_gll_acceleration(THREE_IXP,tg_cpu_neighbor(r_index)%gll_recv(igll)) = rg_gll_acceleration(THREE_IXP,tg_cpu_neighbor(r_index)%gll_recv(igll)) + rg_mpi_buffer_recv(proc_offset+dof_offset+igll)
         enddo
   
      enddo

      return
!***********************************************************************************************************************************************************************************
   end subroutine assemble_force_async_comm_end
!***********************************************************************************************************************************************************************************

end module mod_mpi_assemble
