!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module write snapshots readable by GMT or ParaView.

!>@brief
!!This module contains subroutines to compute and write snapshots of the free surface (either in GMT or VTK xml formats).
module mod_snapshot_surface

   use mpi

   use mod_precision

   use mod_global_variables, only : CIL

   implicit none

   public  :: init_snapshot_surface
   public  :: write_snapshot_surface
   public  :: init_snapshot_surface_gll
   public  :: write_snapshot_surface_gll
   private :: write_snapshot_gmt_nat_fmt
   public  :: write_header_gmt_nat_fmt
   private :: write_snapshot_vtk
   public  :: write_collection_vtk_surf
   private :: collection_vtk_surface
   public  :: write_peak_ground_motion
   public  :: write_peak_ground_motion_gll
   public  :: convert_quad_to_grd
   public  :: write_grid_meta

   interface convert_quad_to_grd

      module procedure convert_quad_to_grd_lag_interp
     !module procedure convert_quad_to_grd_bil_interp

   end interface

   contains

!
!
!>@brief
!!This subroutine generates a structured grid of receivers on the free surface.
!>@return mod_global_variables::tg_receiver_snapshot_quad : information about receivers' used for snapshots in cpu myrank. See mod_global_variables::type_receiver_quad
!>@return mod_global_variables::rg_receiver_snapshot_z : @f$z@f$-coordinate of receivers' used for snapshots
!***********************************************************************************************************************************************************************************
   subroutine init_snapshot_surface()
!***********************************************************************************************************************************************************************************

      use mpi
      
      use mod_global_variables, only :&
                                      tg_receiver_snapshot_quad&
                                     ,ig_nquad_fsurf&
                                     ,ig_quad_nnode&
                                     ,ig_quadf_gnode_glonum&
                                     ,ig_nreceiver_snapshot&
                                     ,ig_receiver_snapshot_glonum&
                                     ,rg_receiver_snapshot_z&
                                     ,ig_receiver_snapshot_locnum&
                                     ,ig_receiver_snapshot_total_number&
                                     ,ig_receiver_snapshot_mpi_shift&
                                     ,IG_LST_UNIT&
                                     ,IG_NGLL&
                                     ,LG_SNAPSHOT_VTK&
                                     ,LG_SNAPSHOT_SURF_GLL&
                                     ,LG_SNAPSHOT_SURF_SPA_DER&
                                     ,ig_myrank&
                                     ,error_stop&
                                     ,ig_ncpu&
                                     ,ig_receiver_snapshot_nx&
                                     ,ig_receiver_snapshot_ny&
                                     ,rg_mesh_xmin&
                                     ,rg_mesh_xmax&
                                     ,rg_mesh_ymin&
                                     ,rg_mesh_ymax&
                                     ,rg_gnode_x&
                                     ,rg_gnode_y&
                                     ,ig_mpi_comm_simu&
                                     ,rg_receiver_snapshot_dxdy&
                                     ,rg_receiver_snapshot_dx&
                                     ,rg_receiver_snapshot_dy
     
      use mod_receiver        , only :&
                                      compute_info_quad_receiver&
                                     ,search_closest_quad_centroid&
                                     ,is_inside_quad

      use mod_coordinate      , only :&
                                      compute_quad_point_coord

      use mod_init_memory

      implicit none
      
      real(kind=RXP)  , allocatable, dimension(:)          :: rldum2
      real(kind=RXP)  , allocatable, dimension(:)          :: rldum3
      real(kind=RXP)                                       :: rec_x
      real(kind=RXP)                                       :: rec_y
      real(kind=RXP)                                       :: rec_dmin
      real(kind=RXP)                                       :: gnode_xmin
      real(kind=RXP)                                       :: gnode_xmax
      real(kind=RXP)                                       :: gnode_ymin
      real(kind=RXP)                                       :: gnode_ymax
      real(kind=RXP)                                       :: gnode_x
      real(kind=RXP)                                       :: gnode_y
      real(kind=RXP)  , dimension(TWO_IXP,ig_nquad_fsurf)  :: quad_centroid_xy
      real(kind=RXP)                                       :: cx
      real(kind=RXP)                                       :: cy
      real(kind=RXP)                                       :: cz
                                                 
                                                 
      integer(kind=IXP), parameter                         :: NCLOSESTP = 15_IXP! can be increased if a receicer is not found, especially if the size if the quandrangle of the free surface are very different
      integer(kind=IXP)                                    :: nclosest
      integer(kind=IXP), allocatable, dimension(:,:)       :: closest_quad
      integer(kind=IXP), allocatable, dimension(:)         :: ildum1
      integer(kind=IXP)                                    :: nx
      integer(kind=IXP)                                    :: ny
      integer(kind=IXP)                                    :: ix
      integer(kind=IXP)                                    :: iy
      integer(kind=IXP)                                    :: iz
                                                           
      integer(kind=IXP)                                    :: rec_iel
      integer(kind=IXP)                                    :: rec_lgll
      integer(kind=IXP)                                    :: rec_mgll
      integer(kind=IXP)                                    :: irec
      integer(kind=IXP)                                    :: jrec
      integer(kind=IXP)                                    :: nrec
      integer(kind=IXP)                                    :: ifsurf
      integer(kind=IXP)                                    :: inode
      integer(kind=IXP)                                    :: gnode_num
                                                           
      integer(kind=IXP)                                    :: icpu
      integer(kind=IXP)                                    :: iloc
      integer(kind=IXP)                                    :: ios
                                                           
      logical(kind=IXP), allocatable, dimension(:)         :: is_rec_in_cpu
      logical(kind=IXP), allocatable, dimension(:)         :: is_rec_in_all_cpu
      logical(kind=IXP)                                    :: is_inside
      logical(kind=IXP), dimension(ig_ncpu)                :: is_inside_all_cpu
      logical(kind=IXP), parameter                         :: IS_WRITE_GRID_META = .true.
                                                           
      character(len=CIL)                                   :: info
   
      if (ig_myrank == ZERO_IXP) then
         write(IG_LST_UNIT,'(" ",/,a)') "checking if receivers for snapshot are inside quadrangle elements..."
         call flush(IG_LST_UNIT)
      endif

!
!
!****************************************************************************************************************************************************
!---->adjust the space increment given in file *.cfg depending on the input value (either ig_receiver_snapshot_nx or rg_receiver_snapshot_dxdy)
!****************************************************************************************************************************************************

!
!---->x-direction

      if (rg_receiver_snapshot_dx < TINY_REAL_RXP) rg_receiver_snapshot_dx = rg_receiver_snapshot_dxdy

      if (ig_receiver_snapshot_nx /= ZERO_IXP) then

         nx = ig_receiver_snapshot_nx

         rg_receiver_snapshot_dx = (rg_mesh_xmax - rg_mesh_xmin)/real(nx-ONE_IXP,kind=RXP)

      else

         nx = int((rg_mesh_xmax - rg_mesh_xmin)/rg_receiver_snapshot_dx,kind=IXP) + ONE_IXP
   
         ig_receiver_snapshot_nx = nx

      endif

!
!---->y-direction

      if (rg_receiver_snapshot_dy < TINY_REAL_RXP) rg_receiver_snapshot_dy = rg_receiver_snapshot_dxdy

      if (ig_receiver_snapshot_ny /= ZERO_IXP) then

         ny = ig_receiver_snapshot_ny

         rg_receiver_snapshot_dy = (rg_mesh_ymax - rg_mesh_ymin)/real(ny-ONE_IXP,kind=RXP)

      else

         ny = int((rg_mesh_ymax - rg_mesh_ymin)/rg_receiver_snapshot_dy,kind=IXP) + ONE_IXP
   
         ig_receiver_snapshot_ny = ny

      endif

!
!---->memory allocation

      allocate(is_rec_in_cpu(nx*ny),stat=ios)

      if (ios /= ZERO_IXP) then
         write(info,'(a)') "error in subroutine init_snapshot_surface while allocating is_rec_in_cpu"
         call error_stop(info)
      else
         do ix = ONE_IXP,nx*ny
            is_rec_in_cpu(ix) = .false.
         enddo
      endif

      allocate(is_rec_in_all_cpu(nx*ny),stat=ios)

      if (ios /= ZERO_IXP) then
         write(info,'(a)') "error in subroutine init_snapshot_surface while allocating is_rec_in_all_cpu"
         call error_stop(info)
      else
         do ix = ONE_IXP,nx*ny
            is_rec_in_all_cpu(ix) = .false.
         enddo
      endif
   
      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(2(a,F10.3))') " -->space increment dx,dy between receivers for snapshot = ",rg_receiver_snapshot_dx,",",rg_receiver_snapshot_dy
         write(IG_LST_UNIT,'(3(a,i0))')    " -->number of receivers for snapshot                     = ",nx,"*",ny," = ",nx * ny
         call flush(IG_LST_UNIT)

      endif

!
!
!****************************************************************************************************************************************************
!---->determine the xmin/xmax/ymin/ymax boundaries of the box surrounding the arbitrary shape formed by the free surface quadrangle in cpu 'myrank'
!     In the first pass (below), if the receiver is not inside this box, then there is no need to check if it is inside a quadrangle
!****************************************************************************************************************************************************

      gnode_xmin = +huge(gnode_xmin)
      gnode_xmax = -huge(gnode_xmax)
      gnode_ymin = +huge(gnode_ymin)
      gnode_ymax = -huge(gnode_ymax)

      do ifsurf = ONE_IXP,ig_nquad_fsurf

         do inode = ONE_IXP,ig_quad_nnode

            gnode_num = ig_quadf_gnode_glonum(inode,ifsurf)
            gnode_x   = rg_gnode_x(gnode_num)
            gnode_y   = rg_gnode_y(gnode_num)

            gnode_xmin = min(gnode_xmin,gnode_x)
            gnode_xmax = max(gnode_xmax,gnode_x)
            gnode_ymin = min(gnode_ymin,gnode_y)
            gnode_ymax = max(gnode_ymax,gnode_y)

         enddo

      enddo

      gnode_xmin = gnode_xmin - 10.0_RXP*EPSILON_MACHINE_RXP
      gnode_xmax = gnode_xmax + 10.0_RXP*EPSILON_MACHINE_RXP
      gnode_ymin = gnode_ymin - 10.0_RXP*EPSILON_MACHINE_RXP
      gnode_ymax = gnode_ymax + 10.0_RXP*EPSILON_MACHINE_RXP

!
!
!*********************************************************************************************************************
!---->compute hexa centroid for subroutine search_closest_quad_centroid
!*********************************************************************************************************************

      do ifsurf = ONE_IXP,ig_nquad_fsurf

         call compute_quad_point_coord(ifsurf,ZERO_RXP,ZERO_RXP,cx,cy,cz)

         quad_centroid_xy(ONE_IXP,ifsurf) = cx
         quad_centroid_xy(TWO_IXP,ifsurf) = cy

      enddo

!
!
!********************************************************************************************************************************
!---->init memory of array 'closest_quad' used to store the 'nclosest' quad of the receiver 'irec' before calling is_inside_quad
!********************************************************************************************************************************

   if (ig_nquad_fsurf > ZERO_IXP) then

      nclosest = min(NCLOSESTP,ig_nquad_fsurf)
 
      ios = init_array_int(closest_quad,nclosest,ig_quad_nnode,"mod_snapshot_surface:init_snapshot_surface:closest_quad")

   endif

!
! 
!*********************************************************************************************************************
!---->first pass on nx*ny receivers to count the number of receivers that belong to cpu 'myrank'
!     this first pass avoid all cpus to allocate a temporary 'tl_receiver_snapshot_quad' of size nx*ny
!*********************************************************************************************************************

      irec = ZERO_IXP
      nrec = ZERO_IXP
   
      do iy = ONE_IXP,ny
      
         do ix = ONE_IXP,nx
      
            irec  = irec + ONE_IXP
            rec_x = rg_mesh_xmin + real(ix-ONE_IXP,kind=RXP)*rg_receiver_snapshot_dx
            rec_y = rg_mesh_ymax - real(iy-ONE_IXP,kind=RXP)*rg_receiver_snapshot_dy
      
!     
!---------->find which quadrangle element contains the receiver irec

            if ( (ig_nquad_fsurf > ZERO_IXP) .and. (rec_x >= gnode_xmin) .and. (rec_x <= gnode_xmax ) .and. (rec_y >= gnode_ymin) .and. (rec_y <= gnode_ymax ) ) then

!
!------------->check only the 'nclosest' closest free surface quandrangle elements (faster than ckecking all free surface quandrangle elements)

               call search_closest_quad_centroid(rec_x,rec_y,ig_quadf_gnode_glonum,quad_centroid_xy,nclosest,closest_quad)

               call is_inside_quad(rec_x,rec_y,closest_quad,nclosest,ig_quad_nnode,rec_dmin,rec_lgll,rec_mgll,rec_iel,is_inside)

!
!------------->check all free surface quandrangle elements (too slow, solution just above using only the 'nclosest' closest free surface quandrangle elements is faster)
!              call is_inside_quad(rec_x,rec_y,ig_quadf_gnode_glonum,ig_nquad_fsurf,ig_quad_nnode,rec_dmin,rec_lgll,rec_mgll,rec_iel,is_inside)

            else

               is_inside = .false.

            endif

!
!---------->cpu0 gather all is_inside to check: 1) if the receiver is inside a quadrangle element: if not abort computation 2) if the receiver is found by multiple cpus: if yes, affect the receiver to only one cpu

            call mpi_gather(is_inside,ONE_IXP,MPI_LOGICAL,is_inside_all_cpu,ONE_IXP,MPI_LOGICAL,ZERO_IXP,ig_mpi_comm_simu,ios)

            if (ig_myrank == ZERO_IXP) then
!
!------------->search the first cpu that contains the receiver
               iloc = -ONE_IXP
               do icpu = ONE_IXP,ig_ncpu
             
                  if (is_inside_all_cpu(icpu)) then
                     iloc = icpu
                     exit
                  endif
             
               enddo

!
!------------->if the receiver is not inside any quadrangle element among all the cpus --> abort computation
               if (iloc == -ONE_IXP) then

                  write(info,'(a)') "error in subroutine init_snapshot_surface: receiver for snapshot not found over all cpus"
                  call error_stop(info)

               else

                  do icpu = iloc+ONE_IXP,ig_ncpu
                     is_inside_all_cpu(icpu) = .false.
                  enddo

               endif

            endif

!
!---------->cpu0 scatter array is_inside_all_cpu. The cpu that obtain the value is_inside = .true. will compute the receiver irec

            call mpi_scatter(is_inside_all_cpu,ONE_IXP,MPI_LOGICAL,is_inside,ONE_IXP,MPI_LOGICAL,ZERO_IXP,ig_mpi_comm_simu,ios)

            if (is_inside) then
      
               nrec                = nrec + ONE_IXP
               is_rec_in_cpu(irec) = .true.
      
            endif
      
            if ( (ig_myrank == ZERO_IXP) .and. ( (irec == ONE_IXP)  .or. (mod(irec,5000_IXP) == ZERO_IXP) .or. (irec == nx*ny) ) ) then
               write(IG_LST_UNIT,'(a,i10)') "checking receiver ",irec
               call flush(IG_LST_UNIT)
            endif
      
         enddo
      
      enddo

! 
!---->allocate array structure tg_receiver_snapshot_quad

      ig_nreceiver_snapshot = nrec

      if (ig_nreceiver_snapshot > ZERO_IXP) then
 
         allocate(tg_receiver_snapshot_quad(ig_nreceiver_snapshot),stat=ios)

         if (ios /= ZERO_IXP) then
         
            write(info,'(a)') "error in subroutine init_snapshot_surface while allocating tg_receiver_snapshot_quad"
            call error_stop(info)
         
         else
         
            do irec = ONE_IXP,ig_nreceiver_snapshot
         
               tg_receiver_snapshot_quad(irec)%x        = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%y        = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%z        = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%xi       = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%eta      = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%dmin     = huge(rec_dmin)
               tg_receiver_snapshot_quad(irec)%lag(:,:) = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%pgd_x    = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%pgd_y    = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%pgd_z    = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%pgv_x    = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%pgv_y    = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%pgv_z    = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%pgv_xyz  = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%pga_x    = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%pga_y    = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%pga_z    = ZERO_RXP
               tg_receiver_snapshot_quad(irec)%gll(:,:) = ZERO_IXP
               tg_receiver_snapshot_quad(irec)%cpu      = ZERO_IXP
               tg_receiver_snapshot_quad(irec)%iel      = ZERO_IXP
               tg_receiver_snapshot_quad(irec)%lgll     = ZERO_IXP
               tg_receiver_snapshot_quad(irec)%mgll     = ZERO_IXP
               tg_receiver_snapshot_quad(irec)%rglo     = ZERO_IXP
         
            enddo
         
         endif

      endif

!
! 
!*********************************************************************************************************************
!---->second pass on receiver that belong to cpu 'myrank' and fill array structure tg_receiver_snapshot_quad
!*********************************************************************************************************************

      irec = ZERO_IXP
      jrec = ZERO_IXP
   
      if ( (ig_nquad_fsurf > ZERO_IXP) .and. (ig_nreceiver_snapshot > ZERO_IXP) ) then

         do iy = ONE_IXP,ny
            do ix = ONE_IXP,nx
         
               irec  = irec + ONE_IXP
         
               if (is_rec_in_cpu(irec)) then
         
                  jrec  = jrec + ONE_IXP
                  rec_x = rg_mesh_xmin + real(ix-ONE_IXP,kind=RXP)*rg_receiver_snapshot_dx
                  rec_y = rg_mesh_ymax - real(iy-ONE_IXP,kind=RXP)*rg_receiver_snapshot_dy
         
!        
!---------------->find which quadrangle element contains the receiver irec (for this pass, no need to search within the 'nclosest' as in the first pass, because no gain in terms of cputime)

                  call is_inside_quad(rec_x,rec_y,ig_quadf_gnode_glonum,ig_nquad_fsurf,ig_quad_nnode,rec_dmin,rec_lgll,rec_mgll,rec_iel,is_inside)

!
!---------------->check only the 'nclosest' closest free surface quandrangle elements (faster than ckecking all free surface quandrangle elements)
!                 call search_closest_quad_centroid(rec_x,rec_y,ig_quadf_gnode_glonum,quad_centroid_xy,nclosest,closest_quad)
!       
!                 call is_inside_quad(rec_x,rec_y,closest_quad,nclosest,ig_quad_nnode,rec_dmin,rec_lgll,rec_mgll,rec_iel,is_inside)

                  if ( (ig_myrank == ZERO_IXP) .and. ( (jrec == ONE_IXP)  .or. (mod(jrec,5000_IXP) == ZERO_IXP) .or. (jrec == nx*ny) ) ) then
                     write(IG_LST_UNIT,'(a,i10)') "checking receiver second pass ",irec
                     call flush(IG_LST_UNIT)
                  endif
         
                  if (is_inside) then
                 
                     tg_receiver_snapshot_quad(jrec)%x    = rec_x
                     tg_receiver_snapshot_quad(jrec)%y    = rec_y
                     tg_receiver_snapshot_quad(jrec)%dmin = rec_dmin
                     tg_receiver_snapshot_quad(jrec)%cpu  = ig_myrank+ONE_IXP
                     tg_receiver_snapshot_quad(jrec)%iel  = rec_iel 
                     tg_receiver_snapshot_quad(jrec)%lgll = rec_lgll
                     tg_receiver_snapshot_quad(jrec)%mgll = rec_mgll
                     tg_receiver_snapshot_quad(jrec)%rglo = irec
         
                  endif
         
               endif
            
            enddo
         enddo

      endif

      deallocate(is_rec_in_cpu)
!   
!---->compute receivers local coordinates and initialize PGx

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(a)') " -->computing local coordinates of receivers"
         call flush(IG_LST_UNIT)

      endif
   
      do irec = ONE_IXP,ig_nreceiver_snapshot
   
         call compute_info_quad_receiver(tg_receiver_snapshot_quad(irec))
   
      enddo

!
!---->cpu0 gathers the number of receiver of all cpus. To avoid seg_fault at runtime with compilation option -check all, remove the if condition.

      if (ig_myrank == ZERO_IXP) then

         ios = init_array_int(ig_receiver_snapshot_total_number,ig_ncpu,"ig_receiver_snapshot_total_number")

         ios = init_array_int(ig_receiver_snapshot_mpi_shift   ,ig_ncpu,"ig_receiver_snapshot_mpi_shift")

      endif

      call mpi_gather(ig_nreceiver_snapshot,ONE_IXP,MPI_INTEGER,ig_receiver_snapshot_total_number,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!---->cpu0 prepares array ig_receiver_snapshot_mpi_shift for mpi_gatherv

      if (ig_myrank == ZERO_IXP) then

         ig_receiver_snapshot_mpi_shift(ONE_IXP) = ZERO_IXP

         do icpu = TWO_IXP,ig_ncpu

            ig_receiver_snapshot_mpi_shift(icpu) = ig_receiver_snapshot_mpi_shift(icpu-ONE_IXP) + ig_receiver_snapshot_total_number(icpu-ONE_IXP)

         enddo

      endif

! 
!---->cpu0 gathers global number of all snapshot receivers of all cpus (because only cpu0 writes the *.grd or *.vts files)

      ios = init_array_int(ig_receiver_snapshot_glonum,nx*ny,"ig_receiver_snapshot_glonum") !could be inside condition if (ig_myrank== ZERO_IXP). TODO FLO

!
!---->cpu0 gathers global number and z-coordinate of all snapshot receivers of all cpus

      ios = init_array_int(ildum1,ig_nreceiver_snapshot,"ildum1")

      do irec = ONE_IXP,ig_nreceiver_snapshot

         ildum1(irec) = tg_receiver_snapshot_quad(irec)%rglo

      enddo

      call mpi_gatherv(ildum1                            &
                      ,ig_nreceiver_snapshot             &
                      ,MPI_INTEGER                       &
                      ,ig_receiver_snapshot_glonum       &
                      ,ig_receiver_snapshot_total_number &
                      ,ig_receiver_snapshot_mpi_shift    &
                      ,MPI_INTEGER                       &
                      ,ZERO_IXP                          &
                      ,ig_mpi_comm_simu                    &
                      ,ios                               )

      deallocate(ildum1)

!
!
!*********************************************************************************************************************
!---->only if snapshots are in VTK format. GMT does not need rg_receiver_snapshot_z nor ig_receiver_snapshot_locnum
!*********************************************************************************************************************

      if (LG_SNAPSHOT_VTK) then

!    
!------->cpu0 gathers z-coordinate of all snapshot receivers of all cpus (because only cpu0 writes the .*vts files)
     
         ios = init_array_real(rldum2,ig_nreceiver_snapshot,"rldum2")
     
         ios = init_array_real(rldum3,nx*ny                ,"rldum3")
     
         do irec = ONE_IXP,ig_nreceiver_snapshot
     
            rldum2(irec) = tg_receiver_snapshot_quad(irec)%z
     
         enddo
     
         call mpi_gatherv(rldum2                            &
                         ,ig_nreceiver_snapshot             &
                         ,MPI_REAL                          &
                         ,rldum3                            &
                         ,ig_receiver_snapshot_total_number &
                         ,ig_receiver_snapshot_mpi_shift    &
                         ,MPI_REAL                          &
                         ,ZERO_IXP                          &
                         ,ig_mpi_comm_simu                    &
                         ,ios                               )
     
         deallocate(rldum2)

         if (ig_myrank == ZERO_IXP) then
         
            allocate(rg_receiver_snapshot_z(nx*ny),stat=ios)
            if (ios /= ZERO_IXP) then
               write(info,'(a)') "error in subroutine init_snapshot_surface while allocating rg_receiver_snapshot_z"
               call error_stop(info)
            endif
         
            allocate(ig_receiver_snapshot_locnum(nx*ny),stat=ios)
            if (ios /= ZERO_IXP) then
               write(info,'(a)') "error in subroutine init_snapshot_surface while allocating ig_receiver_snapshot_locnum"
               call error_stop(info)
            endif
         
            irec = ZERO_IXP
         
            do iy = ONE_IXP,ny
               do ix = ONE_IXP,nx
         
                  irec = irec + ONE_IXP
         
                  do iz = ONE_IXP,nx*ny
         
                     if (ig_receiver_snapshot_glonum(iz) == irec) then
         
                        ig_receiver_snapshot_locnum(irec) = iz
                        rg_receiver_snapshot_z(irec)      = rldum3(iz)
         
                        exit
         
                     endif
         
                  enddo
            
               enddo
            enddo
         
         endif

      endif

!
!
!*********************************************************************************************************************
!---->output receivers metadata used to interpolate receivers' results for gmt grid
!*********************************************************************************************************************

      if (IS_WRITE_GRID_META .and. (LG_SNAPSHOT_SURF_GLL .or. LG_SNAPSHOT_SURF_SPA_DER) ) then

         call write_grid_meta(tg_receiver_snapshot_quad)

      endif
 
      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(a)') "done"
         call flush(IG_LST_UNIT)

      endif
  
      return

!***********************************************************************************************************************************************************************************
   end subroutine init_snapshot_surface
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine computes and writes @f$x,y,z@f$-displacements, velocities and accelerations at receivers used for free surface snapshots.
!>@return Snapshots are written in binary native GMT format and/or in binary VTK xml format.
!***********************************************************************************************************************************************************************************
   subroutine write_snapshot_surface()
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                      LG_SNAPSHOT_GMT&
                                     ,LG_SNAPSHOT_VTK&
                                     ,IG_NDOF&
                                     ,IG_NGLL&
                                     ,ig_idt&
                                     ,cg_prefix&
                                     ,lg_snapshot_displacement&
                                     ,lg_snapshot_velocity&
                                     ,lg_snapshot_acceleration&
                                     ,ig_nreceiver_snapshot&
                                     ,ig_snapshot_saving_incr&
                                     ,tg_receiver_snapshot_quad&
                                     ,rg_gll_displacement&
                                     ,rg_gll_velocity&
                                     ,rg_gll_acceleration

      use mod_gll_value       , only : get_quad_gll_value

      use mod_lagrange        , only : quad_lagrange_interp
     
      implicit none

      real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL) :: gll_dis
      real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL) :: gll_vel
      real(kind=RXP), dimension(IG_NDOF,IG_NGLL,IG_NGLL) :: gll_acc

      real(kind=RXP), dimension(ig_nreceiver_snapshot)   :: ux
      real(kind=RXP), dimension(ig_nreceiver_snapshot)   :: uy
      real(kind=RXP), dimension(ig_nreceiver_snapshot)   :: uz
      real(kind=RXP), dimension(ig_nreceiver_snapshot)   :: vx
      real(kind=RXP), dimension(ig_nreceiver_snapshot)   :: vy
      real(kind=RXP), dimension(ig_nreceiver_snapshot)   :: vz
      real(kind=RXP), dimension(ig_nreceiver_snapshot)   :: ax
      real(kind=RXP), dimension(ig_nreceiver_snapshot)   :: ay
      real(kind=RXP), dimension(ig_nreceiver_snapshot)   :: az

      integer(kind=IXP)                                  :: irec
                                               
      character(len=CIL)                                 :: fname
      character(len=  6)                                 :: csnapshot
      character(len=  4)                                 :: vname
!
!
!*********************************************************************************************************************************
!---->before writing snapshots, displacement, velocity and acceleration are interpolated at the snapshot_receiver x,y,z location
!*********************************************************************************************************************************

      do irec = ONE_IXP,ig_nreceiver_snapshot

!
!------->get GLL nodes displacement, velocity and acceleration xyz-values from quadrangle element which contains the snapshot receiver

         call get_quad_gll_value(rg_gll_displacement,tg_receiver_snapshot_quad(irec)%gll,gll_dis)
         call get_quad_gll_value(rg_gll_velocity    ,tg_receiver_snapshot_quad(irec)%gll,gll_vel)
         call get_quad_gll_value(rg_gll_acceleration,tg_receiver_snapshot_quad(irec)%gll,gll_acc)

!
!------->interpolate displacement at the snapshot receiver

         call quad_lagrange_interp(gll_dis,tg_receiver_snapshot_quad(irec)%lag,ux(irec),uy(irec),uz(irec))

!
!------->compute PGD in x, y and z directions

         if ( abs(ux(irec)) > tg_receiver_snapshot_quad(irec)%pgd_x ) tg_receiver_snapshot_quad(irec)%pgd_x = abs(ux(irec))
         if ( abs(uy(irec)) > tg_receiver_snapshot_quad(irec)%pgd_y ) tg_receiver_snapshot_quad(irec)%pgd_y = abs(uy(irec))
         if ( abs(uz(irec)) > tg_receiver_snapshot_quad(irec)%pgd_z ) tg_receiver_snapshot_quad(irec)%pgd_z = abs(uz(irec))

!
!------->interpolate velocity at the snapshot receiver

         call quad_lagrange_interp(gll_vel,tg_receiver_snapshot_quad(irec)%lag,vx(irec),vy(irec),vz(irec))

!
!------->compute PGV in x, y and z directions

         if ( abs(vx(irec)) > tg_receiver_snapshot_quad(irec)%pgv_x ) tg_receiver_snapshot_quad(irec)%pgv_x = abs(vx(irec))
         if ( abs(vy(irec)) > tg_receiver_snapshot_quad(irec)%pgv_y ) tg_receiver_snapshot_quad(irec)%pgv_y = abs(vy(irec))
         if ( abs(vz(irec)) > tg_receiver_snapshot_quad(irec)%pgv_z ) tg_receiver_snapshot_quad(irec)%pgv_z = abs(vz(irec))
        !if ( sqrt(vx(irec)**2 + vy(irec)**2 + vz(irec)**2)  > tg_receiver_snapshot_quad(irec)%pgv_xyz ) tg_receiver_snapshot_quad(irec)%pgv_xyz = sqrt(vx(irec)**2 + vy(irec)**2 + vz(irec)**2)

!
!------->interpolate acceleration at the snapshot receiver

         call quad_lagrange_interp(gll_acc,tg_receiver_snapshot_quad(irec)%lag,ax(irec),ay(irec),az(irec))

!
!------->compute PGA in x, y and z directions

         if ( abs(ax(irec)) > tg_receiver_snapshot_quad(irec)%pga_x ) tg_receiver_snapshot_quad(irec)%pga_x = abs(ax(irec))
         if ( abs(ay(irec)) > tg_receiver_snapshot_quad(irec)%pga_y ) tg_receiver_snapshot_quad(irec)%pga_y = abs(ay(irec))
         if ( abs(az(irec)) > tg_receiver_snapshot_quad(irec)%pga_z ) tg_receiver_snapshot_quad(irec)%pga_z = abs(az(irec))

      enddo
!
!
!*********************************************************************************************************************************
!---->write snapshot in gmt or VTK format
!*********************************************************************************************************************************

      write(csnapshot,'(i6.6)') ig_idt

!
!
!---->snapshot displacement

      if ( lg_snapshot_displacement .and. (mod(ig_idt-ONE_IXP,ig_snapshot_saving_incr) == ZERO_IXP) ) then

         if (LG_SNAPSHOT_GMT) then

            fname = trim(cg_prefix)//".snapshot.ux."//trim(csnapshot)//".grd"
            call write_snapshot_gmt_nat_fmt(fname,ux)

            fname = trim(cg_prefix)//".snapshot.uy."//trim(csnapshot)//".grd"
            call write_snapshot_gmt_nat_fmt(fname,uy)

            fname = trim(cg_prefix)//".snapshot.uz."//trim(csnapshot)//".grd"
            call write_snapshot_gmt_nat_fmt(fname,uz)

         endif

         if (LG_SNAPSHOT_VTK) then

            fname = trim(cg_prefix)//".snapshot.uxyz."//trim(csnapshot)//".vts"
            vname = "disp"
            call write_snapshot_vtk(fname,vname,ux,uy,uz)

         endif

      endif
!
!
!---->snapshot velocity

      if ( lg_snapshot_velocity .and. (mod(ig_idt-ONE_IXP,ig_snapshot_saving_incr) == ZERO_IXP) ) then

         if (LG_SNAPSHOT_GMT) then

            fname = trim(cg_prefix)//".snapshot.vx."//trim(csnapshot)//".grd"
            call write_snapshot_gmt_nat_fmt(fname,vx)
          
            fname = trim(cg_prefix)//".snapshot.vy."//trim(csnapshot)//".grd"
            call write_snapshot_gmt_nat_fmt(fname,vy)
          
            fname = trim(cg_prefix)//".snapshot.vz."//trim(csnapshot)//".grd"
            call write_snapshot_gmt_nat_fmt(fname,vz)

         endif

         if (LG_SNAPSHOT_VTK) then

            fname = trim(cg_prefix)//".snapshot.vxyz."//trim(csnapshot)//".vts"
            vname = "velo"
            call write_snapshot_vtk(fname,vname,vx,vy,vz)

         endif

      endif
!
!
!---->snapshot acceleration

      if ( lg_snapshot_acceleration .and. (mod(ig_idt-ONE_IXP,ig_snapshot_saving_incr) == ZERO_IXP) )  then

         if (LG_SNAPSHOT_GMT) then

            fname = trim(cg_prefix)//".snapshot.ax."//trim(csnapshot)//".grd"
            call write_snapshot_gmt_nat_fmt(fname,ax)
          
            fname = trim(cg_prefix)//".snapshot.ay."//trim(csnapshot)//".grd"
            call write_snapshot_gmt_nat_fmt(fname,ay)
          
            fname = trim(cg_prefix)//".snapshot.az."//trim(csnapshot)//".grd"
            call write_snapshot_gmt_nat_fmt(fname,az)

         endif

         if (LG_SNAPSHOT_VTK) then

            fname = trim(cg_prefix)//".snapshot.axyz."//trim(csnapshot)//".vts"
            vname = "acce"
            call write_snapshot_vtk(fname,vname,ax,ay,az)

         endif

      endif

      return
     
!***********************************************************************************************************************************************************************************
   end subroutine write_snapshot_surface
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine writes @f$x,y,z@f$-displacements at GLLs located at the free surface.
!>@return a snapshot file written in binary format that contains all time steps.
!***********************************************************************************************************************************************************************************
   subroutine init_snapshot_surface_gll()
!***********************************************************************************************************************************************************************************

      use mod_precision

      use mpi

      use mod_global_variables, only :&
                                       cg_prefix&
                                      ,cg_iir_filter_output_motion&
                                      ,ig_mpi_nboctet_real&
                                      ,ig_nquad_fsurf&
                                      ,ig_nquad_fsurf_saved&
                                      ,ig_nquad_fsurf_all_cpu&
                                      ,ig_quadf_gll_glonum&
                                      ,ig_quadf_gnode_glonum&
                                      ,ig_quad_nnode&
                                      ,rg_gnode_x&
                                      ,rg_gnode_y&
                                      ,rg_gnode_z&
                                      ,IG_NGLL&
                                      ,IG_NDOF&
                                      ,IG_LST_UNIT&
                                      ,ig_ndt&
                                      ,ig_ngll_order_by_quadf&
                                      ,ig_gll_order_by_quadf&
                                      ,ig_unit_snap_quad_gll_dis&
                                      ,ig_unit_snap_quadf_dis_spa_der&
                                      ,ig_pos_cpu_snap_surf_gll&
                                      ,ig_mpi_comm_fsurf&
                                      ,ig_ncpu_fsurf&
                                      ,ig_myrank_fsurf&
                                      ,ig_type_vector_snap_fsurf&
                                      ,ig_mpi_comm_simu&
                                      ,LG_FIR_FILTER&
                                      ,LG_LUSTRE_FILE_SYS&
                                      ,LG_SNAPSHOT_SURF_SPA_DER&
                                      ,IG_MPI_WRITE_TYPE&
                                      ,rg_fir_filter_buffer_dis&
                                      ,rg_fir_filter_buffer_dis_spa_der&
                                      ,ig_fir_filter_order

      use mod_init_memory

      use mod_coordinate, only : get_gnode_coord&
                               , compute_quad_point_coord

      use mod_io_array  , only : efi_mpi_file_write_at_all&
                               , efi_mpi_file_open

      implicit none

      integer(kind=IXP), dimension(:)  , allocatable   :: nquad_all_cpu
      integer(kind=IXP), dimension(:)  , allocatable   :: local_to_global_num
      integer(kind=IXP), dimension(:,:), allocatable   :: quadf_gnode_glonum
      integer(kind=IXP)                                :: nquad_fsurf
      integer(kind=IXP)                                :: ios
      integer(kind=IXP)                                :: color
      integer(kind=IXP)                                :: key
      integer(kind=IXP)                                :: icpu
      integer(kind=IXP)                                :: iquad
      integer(kind=IXP)                                :: jquad
      integer(kind=IXP)                                :: igll
      integer(kind=IXP)                                :: hgll
      integer(kind=IXP)                                :: k
      integer(kind=IXP)                                :: l
                                                              
      real   (kind=RXP), dimension(:,:,:), allocatable :: quad_gnode_xyz 
      real   (kind=RXP)                                :: xmin
      real   (kind=RXP)                                :: xmax
      real   (kind=RXP)                                :: ymin
      real   (kind=RXP)                                :: ymax
      real   (kind=RXP)                                :: x
      real   (kind=RXP)                                :: y
      real   (kind=RXP)                                :: z
                                                              
      logical(kind=IXP), dimension(ig_nquad_fsurf)     :: is_quad_save

      character(len=CIL)                               :: fname

!
!
!*****************************************************************************************************************************
!---->select which free surface quadrangle elements will be used for snapshot
!*****************************************************************************************************************************

      xmin = -huge(xmin)
      xmax = +huge(xmin)
      ymin = -huge(xmin)
      ymax = +huge(xmin)

!
!---->first pass to count the number of elements within the box defined by xmin/xmax/ymin/ymax

      nquad_fsurf = ZERO_IXP

      is_quad_save(:) = .false.

      do iquad = ONE_IXP,ig_nquad_fsurf

         call compute_quad_point_coord(iquad,ZERO_RXP,ZERO_RXP,x,y,z)

         if ( (x >= xmin) .and. (x <= xmax) .and. (y >= ymin) .and. (y <= ymax) ) then

            nquad_fsurf = nquad_fsurf + ONE_IXP

            is_quad_save(iquad) = .true.

         endif

      enddo

      if (nquad_fsurf /= ZERO_IXP) then

         ios = init_array_int(local_to_global_num,nquad_fsurf,"mod_snapshot_surface:init_snapshot_surface_gll:local_to_global_num")

      endif

      ig_nquad_fsurf_saved = nquad_fsurf

!
!---->second pass to initialize the indirection array from local to global numbering

      jquad = ZERO_IXP

      do iquad = ONE_IXP,ig_nquad_fsurf

         if (is_quad_save(iquad)) then

             jquad = jquad + ONE_IXP

             local_to_global_num(jquad) = iquad

         endif

      enddo

!
!
!*****************************************************************************************************************************
!---->init array ig_gll_quadf that contains the list of GLL ordered by free surface quadrangles
!*****************************************************************************************************************************

      ig_ngll_order_by_quadf = nquad_fsurf*IG_NGLL*IG_NGLL

      ios = init_array_int(ig_gll_order_by_quadf,ig_ngll_order_by_quadf,"mod_snapshot_surface:init_snapshot_surface_gll:ig_gll_order_by_quadf")

      hgll = ZERO_IXP

      do iquad = ONE_IXP,nquad_fsurf

         jquad = local_to_global_num(iquad)

         do k = ONE_IXP,IG_NGLL

            do l = ONE_IXP,IG_NGLL

               igll = ig_quadf_gll_glonum(l,k,jquad)

               hgll = hgll + ONE_IXP

               ig_gll_order_by_quadf(hgll) = igll

            enddo

         enddo

      enddo

!
!
!*****************************************************************************************************************************
!---->create local communicator between cpu that computes free surface quadrangle elements and open files
!*****************************************************************************************************************************

      if ( nquad_fsurf > ZERO_IXP ) then

         color = 123456_IXP

      else

         color = MPI_UNDEFINED

      ENDIF

      key = -ONE_IXP

      call mpi_comm_split(ig_mpi_comm_simu,color,key,ig_mpi_comm_fsurf,ios)

      if (ig_mpi_comm_fsurf /= MPI_COMM_NULL) then

!
!------->each cpu gather the number of quad of all cpu to know where to write in file unit 'ig_unit_snap_quad_gll_dis'

         call mpi_comm_size(ig_mpi_comm_fsurf,ig_ncpu_fsurf  ,ios)

         call mpi_comm_rank(ig_mpi_comm_fsurf,ig_myrank_fsurf,ios)

         ios = init_array_int(nquad_all_cpu,ig_ncpu_fsurf,"mod_snapshot_surface:init_snapshot_surface_gll:nquad_all_cpu")
         
         ig_pos_cpu_snap_surf_gll = ZERO_IXP
         
         call mpi_allgather(nquad_fsurf,ONE_IXP,MPI_INTEGER,nquad_all_cpu,ONE_IXP,MPI_INTEGER,ig_mpi_comm_fsurf,ios)

         do icpu = ONE_IXP,ig_myrank_fsurf
      
            ig_pos_cpu_snap_surf_gll = ig_pos_cpu_snap_surf_gll + nquad_all_cpu(icpu)
      
         enddo

         ig_nquad_fsurf_all_cpu = sum(nquad_all_cpu)

!     
!------->open file that will store GLL x,y,z displacement for all time steps

         selectcase(trim(cg_iir_filter_output_motion))

            case("dis")
               fname = trim(cg_prefix)//".snapshot.quadf.gll.uxyz"

            case("vel")
               fname = trim(cg_prefix)//".snapshot.quadf.gll.vxyz"

            case("acc")
               fname = trim(cg_prefix)//".snapshot.quadf.gll.axyz"

            case default
               write(*,*) "***error in init_snapshot_surface_gll: unknown output motion"
               call mpi_abort(ig_mpi_comm_simu,100_IXP,ios)
               stop

         endselect
        
         call efi_mpi_file_open(ig_mpi_comm_fsurf,fname,MPI_MODE_WRONLY + MPI_MODE_CREATE,LG_LUSTRE_FILE_SYS,ig_unit_snap_quad_gll_dis)
        
         if (LG_SNAPSHOT_SURF_SPA_DER) then

            fname = trim(fname)//".sd"

            call efi_mpi_file_open(ig_mpi_comm_fsurf,fname,MPI_MODE_WRONLY + MPI_MODE_CREATE,LG_LUSTRE_FILE_SYS,ig_unit_snap_quadf_dis_spa_der)
        
         endif

      endif

!
!
!*****************************************************************************************************************************
!---->create datatype for mpi_file_view -> file hierarchy = processor-quad-gll-xyz-time
!*****************************************************************************************************************************

      if (IG_MPI_WRITE_TYPE == THREE_IXP) then

         call MPI_TYPE_VECTOR(nquad_fsurf*IG_NGLL*IG_NGLL*IG_NDOF,ONE_IXP,ig_ndt,MPI_REAL,ig_type_vector_snap_fsurf,ios)
         
         call MPI_TYPE_COMMIT(ig_type_vector_snap_fsurf,ios)

      endif


!
!
!*****************************************************************************************************************************
!---->init fir filter buffer array (only displacement so far, but velocity and acceleration could be added)
!*****************************************************************************************************************************

      if (LG_FIR_FILTER) then

         ios = init_array_real(rg_fir_filter_buffer_dis,ig_ngll_order_by_quadf,IG_NDOF,ig_fir_filter_order,"mod_snapshot_surface:init_snapshot_surface_gll:rg_fir_filter_buffer_dis")

         if (LG_SNAPSHOT_SURF_SPA_DER) then

            ios = init_array_real(rg_fir_filter_buffer_dis_spa_der,ig_nquad_fsurf,IG_NDOF*IG_NDOF,ig_fir_filter_order,"mod_snapshot_surface:init_snapshot_surface_gll:rg_fir_filter_buffer_dis_spa_der")

         endif

      endif

!
!
!*****************************************************************************************************************************
!---->store x,y,z coordinates of the geometric nodes of the quadrangle elements of the free surface
!*****************************************************************************************************************************

      if ( ig_mpi_comm_fsurf /= MPI_COMM_NULL ) then

!
!------->use only the elements to be saved (not all the elements of the free surface)

         ios = init_array_int(quadf_gnode_glonum,nquad_fsurf,ig_quad_nnode,"mod_snapshot_surface:init_snapshot_surface_gll:quadf_gnode_glonum")

         ios = init_array_real(quad_gnode_xyz,nquad_fsurf,ig_quad_nnode,IG_NDOF,"mod_snapshot_surface:init_snapshot_surface_gll:quad_gnode_xyz")

         do iquad = ONE_IXP,nquad_fsurf

            jquad = local_to_global_num(iquad)

            quadf_gnode_glonum(:,iquad) = ig_quadf_gnode_glonum(:,jquad)

         enddo

!
!------->get the x,y,z spatial coordinates

         call get_gnode_coord(quadf_gnode_glonum,rg_gnode_x,quad_gnode_xyz(1_IXP,:,:))
         call get_gnode_coord(quadf_gnode_glonum,rg_gnode_y,quad_gnode_xyz(2_IXP,:,:))
         call get_gnode_coord(quadf_gnode_glonum,rg_gnode_z,quad_gnode_xyz(3_IXP,:,:))

!
!------->save the spatial coordinates to file

         fname = trim(cg_prefix)//".snapshot.quadf.gno.xyz"

         call efi_mpi_file_write_at_all(ig_mpi_comm_fsurf,quad_gnode_xyz,fname,ig_myrank_fsurf,ig_pos_cpu_snap_surf_gll+ONE_IXP,ig_mpi_nboctet_real,LG_LUSTRE_FILE_SYS)

      endif

      return
     
!***********************************************************************************************************************************************************************************
   end subroutine init_snapshot_surface_gll
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine initializes i) files' unit number, name and ii) arrays needed by subroutine write_snapshot_surface_gll.
!>@return a snapshot file written in binary format that contains all time steps.
!***********************************************************************************************************************************************************************************
   subroutine write_snapshot_surface_gll()
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_precision

      use mod_global_variables, only :&
                                       ig_idt&
                                      ,ig_snapshot_surf_gll_nsave&
                                      ,cg_iir_filter_output_motion&
                                      ,IG_NDOF&
                                      ,IG_NGLL&
                                      ,ig_nquad_fsurf&
                                      ,ig_nquad_fsurf_all_cpu&
                                      ,ig_ngll_order_by_quadf&
                                      ,ig_gll_order_by_quadf&
                                      ,rg_gll_displacement&
                                      ,rg_gll_velocity&
                                      ,rg_gll_acceleration&
                                      ,rg_quadf_disp_spatial_deriv&
                                      ,ig_unit_snap_quad_gll_dis&
                                      ,ig_unit_snap_quadf_dis_spa_der&
                                      ,ig_mpi_nboctet_real&
                                      ,ig_mpi_comm_fsurf&
                                      ,ig_pos_cpu_snap_surf_gll&
                                      ,ig_type_vector_snap_fsurf&
                                      ,LG_FIR_FILTER&
                                      ,LG_SNAPSHOT_SURF_SPA_DER&
                                      ,IG_MPI_WRITE_TYPE&
                                      ,IG_FIR_DECIMATION_FAC&
                                      ,ig_fir_filter_order&
                                      ,rg_fir_filter&
                                      ,rg_fir_filter_buffer_dis&
                                      ,rg_fir_filter_buffer_dis_spa_der

      use mod_filter, only : apply_fir_filter

      implicit none

      real(kind=RXP)  , dimension(IG_NDOF,ig_ngll_order_by_quadf) :: rl_gll_motion

      integer(kind=IXP), save                                     :: idt_save = ZERO_IXP
      integer(kind=IXP)                                           :: iquad
      integer(kind=IXP)                                           :: hgll
      integer(kind=IXP)                                           :: igll
      integer(kind=IXP)                                           :: ios
                                                         
      integer(kind=I64)                                           :: pos_dis !normaly kind=mpi_offset_kind but not always recognized -> use of iso_fortran_env INT64
      integer(kind=I64)                                           :: pos_spa_der
                                                                  
      integer(kind=I64)                                           :: ngll_int64
      integer(kind=I64)                                           :: ndof_int64
      integer(kind=I64)                                           :: nquad_fsurf_all_cpu_int64
      integer(kind=I64)                                           :: idt_int64
      integer(kind=I64)                                           :: pos_cpu_snap_surf_gll_int64
      integer(kind=I64)                                           :: mpi_nboctet_real_int64
      integer(kind=I64), parameter                                :: one_int64 = 1_I64

      integer(kind=IXP), dimension(mpi_status_size)               :: statut

      logical(kind=IXP)                                           :: is_save

!
!
!***************************************************************************************************************
!---->get x,y,z GLL displacements to be written
!***************************************************************************************************************

      selectcase(trim(cg_iir_filter_output_motion))

         case("dis")

            do hgll = ONE_IXP,ig_ngll_order_by_quadf
 
               igll = ig_gll_order_by_quadf(hgll)
 
               rl_gll_motion(ONE_IXP  ,hgll) = rg_gll_displacement(ONE_IXP  ,igll) !x-component
               rl_gll_motion(TWO_IXP  ,hgll) = rg_gll_displacement(TWO_IXP  ,igll) !y-component
               rl_gll_motion(THREE_IXP,hgll) = rg_gll_displacement(THREE_IXP,igll) !z-component
 
            enddo

         case("vel")

            do hgll = ONE_IXP,ig_ngll_order_by_quadf
 
               igll = ig_gll_order_by_quadf(hgll)
 
               rl_gll_motion(ONE_IXP  ,hgll) = rg_gll_velocity(ONE_IXP  ,igll) !x-component
               rl_gll_motion(TWO_IXP  ,hgll) = rg_gll_velocity(TWO_IXP  ,igll) !y-component
               rl_gll_motion(THREE_IXP,hgll) = rg_gll_velocity(THREE_IXP,igll) !z-component
 
            enddo

         case("acc")

            do hgll = ONE_IXP,ig_ngll_order_by_quadf
 
               igll = ig_gll_order_by_quadf(hgll)
 
               rl_gll_motion(ONE_IXP  ,hgll) = rg_gll_acceleration(ONE_IXP  ,igll) !x-component
               rl_gll_motion(TWO_IXP  ,hgll) = rg_gll_acceleration(TWO_IXP  ,igll) !y-component
               rl_gll_motion(THREE_IXP,hgll) = rg_gll_acceleration(THREE_IXP,igll) !z-component
 
            enddo

         endselect

!
!
!***************************************************************************************************************
!---->FIR filter
!***************************************************************************************************************

      is_save = .false.

      if (LG_FIR_FILTER) then

!
!------->filter displacement field

         do hgll = ONE_IXP,ig_ngll_order_by_quadf
       
            call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis(ONE_IXP,ONE_IXP  ,hgll),rl_gll_motion(ONE_IXP  ,hgll))
            call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis(ONE_IXP,TWO_IXP  ,hgll),rl_gll_motion(TWO_IXP  ,hgll))
            call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis(ONE_IXP,THREE_IXP,hgll),rl_gll_motion(THREE_IXP,hgll))
       
         enddo

         if (LG_SNAPSHOT_SURF_SPA_DER) then

!
!---------->filter spatial derivative of displacement field

            do iquad = ONE_IXP,ig_nquad_fsurf

               call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis_spa_der(ONE_IXP,1_IXP,iquad),rg_quadf_disp_spatial_deriv(1_IXP,iquad))
               call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis_spa_der(ONE_IXP,2_IXP,iquad),rg_quadf_disp_spatial_deriv(2_IXP,iquad))
               call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis_spa_der(ONE_IXP,3_IXP,iquad),rg_quadf_disp_spatial_deriv(3_IXP,iquad))
               call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis_spa_der(ONE_IXP,4_IXP,iquad),rg_quadf_disp_spatial_deriv(4_IXP,iquad))
               call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis_spa_der(ONE_IXP,5_IXP,iquad),rg_quadf_disp_spatial_deriv(5_IXP,iquad))
               call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis_spa_der(ONE_IXP,6_IXP,iquad),rg_quadf_disp_spatial_deriv(6_IXP,iquad))
               call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis_spa_der(ONE_IXP,7_IXP,iquad),rg_quadf_disp_spatial_deriv(7_IXP,iquad))
               call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis_spa_der(ONE_IXP,8_IXP,iquad),rg_quadf_disp_spatial_deriv(8_IXP,iquad))
               call apply_fir_filter(rg_fir_filter,ig_fir_filter_order,rg_fir_filter_buffer_dis_spa_der(ONE_IXP,9_IXP,iquad),rg_quadf_disp_spatial_deriv(9_IXP,iquad))

            enddo

         endif

         if ( (ig_idt == ONE_IXP) .or.  mod((ig_idt-ONE_IXP),IG_FIR_DECIMATION_FAC) == ZERO_IXP ) then

            idt_save = idt_save + ONE_IXP

            is_save  = .true.

         endif

      else

         idt_save = idt_save + ONE_IXP

         is_save  = .true.

      endif

!
!
!***************************************************************************************************************
!each cpu writes into disk. One file per physical quantity: displacement, divergence and curl
!***************************************************************************************************************

!
!---->write with mpi_file_write_at

      if (IG_MPI_WRITE_TYPE == ONE_IXP) then
      
         pos_dis = ((idt_save-ONE_IXP)*ig_nquad_fsurf_all_cpu + ig_pos_cpu_snap_surf_gll)*IG_NGLL*IG_NGLL*IG_NDOF*ig_mpi_nboctet_real !jump to position of current time step 'idt_save' and cpu position 'ig_pos_cpu_snap_surf_gll'
       
         if ( (ig_mpi_comm_fsurf /= MPI_COMM_NULL) .and. (is_save) ) then

            call mpi_file_write_at(ig_unit_snap_quad_gll_dis,pos_dis,rl_gll_motion,IG_NDOF*ig_ngll_order_by_quadf,MPI_REAL,statut,ios)

         endif

         if (LG_SNAPSHOT_SURF_SPA_DER) then

            pos_spa_der = ((idt_save-ONE_IXP)*ig_nquad_fsurf_all_cpu + ig_pos_cpu_snap_surf_gll)*IG_NDOF*IG_NDOF*ig_mpi_nboctet_real

            if ( (ig_mpi_comm_fsurf /= MPI_COMM_NULL) .and. (is_save) ) then

               call mpi_file_write_at(ig_unit_snap_quadf_dis_spa_der,pos_spa_der,rg_quadf_disp_spatial_deriv,IG_NDOF*IG_NDOF*ig_nquad_fsurf,MPI_REAL,statut,ios)

            endif

         endif
 
!     
!---->write with mpi_file_write_at_all

      elseif (IG_MPI_WRITE_TYPE == TWO_IXP) then
      
         if ( (ig_mpi_comm_fsurf /= MPI_COMM_NULL) .and. (is_save) ) then

           !conversion needed on shaheen to avoid variable 'pos_dis' being an INT32
            ngll_int64                  = int(IG_NGLL                 ,kind=I64)
            ndof_int64                  = int(IG_NDOF                 ,kind=I64)
            nquad_fsurf_all_cpu_int64   = int(ig_nquad_fsurf_all_cpu  ,kind=I64)
            pos_cpu_snap_surf_gll_int64 = int(ig_pos_cpu_snap_surf_gll,kind=I64)
            mpi_nboctet_real_int64      = int(ig_mpi_nboctet_real     ,kind=I64)
            idt_int64                   = int(idt_save                ,kind=I64)
           
            pos_dis = ((idt_int64-one_int64)*nquad_fsurf_all_cpu_int64 + pos_cpu_snap_surf_gll_int64)*ngll_int64*ngll_int64*ndof_int64*mpi_nboctet_real_int64 

            call mpi_file_write_at_all(ig_unit_snap_quad_gll_dis,pos_dis,rl_gll_motion,IG_NDOF*ig_ngll_order_by_quadf,MPI_REAL,statut,ios)

         endif

         if (LG_SNAPSHOT_SURF_SPA_DER) then

            if ( (ig_mpi_comm_fsurf /= MPI_COMM_NULL) .and. (is_save) ) then

               pos_spa_der = ((idt_int64-one_int64)*nquad_fsurf_all_cpu_int64 + pos_cpu_snap_surf_gll_int64)*ndof_int64*ndof_int64*mpi_nboctet_real_int64

               call mpi_file_write_at_all(ig_unit_snap_quadf_dis_spa_der,pos_spa_der,rg_quadf_disp_spatial_deriv,IG_NDOF*IG_NDOF*ig_nquad_fsurf,MPI_REAL,statut,ios)

            endif

         endif

!     
!---->write with mpi_file_set_view and mpi_file_write_all: position rl_gll_motion so that time series of gll in file are ordered by time step

      elseif (IG_MPI_WRITE_TYPE == THREE_IXP) then

         if (ig_mpi_comm_fsurf /= MPI_COMM_NULL) then

            pos_dis = (ig_snapshot_surf_gll_nsave*ig_pos_cpu_snap_surf_gll*IG_NGLL*IG_NGLL*IG_NDOF + (idt_save-ONE_IXP))*ig_mpi_nboctet_real

            call mpi_file_set_view(ig_unit_snap_quad_gll_dis,pos_dis,MPI_REAL,ig_type_vector_snap_fsurf,"native",MPI_INFO_NULL,ios)
          
            call mpi_file_write_all(ig_unit_snap_quad_gll_dis,rl_gll_motion,IG_NDOF*ig_ngll_order_by_quadf,MPI_REAL,statut,ios)

         endif
      
      endif

      return
     
!***********************************************************************************************************************************************************************************
   end subroutine write_snapshot_surface_gll
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine writes structured grid snapshot in binary native GMT format
!>@param fname : file name of the snapshot
!>@param val   : values to be written
!***********************************************************************************************************************************************************************************
   subroutine write_snapshot_gmt_nat_fmt(fname,val)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                      ig_nreceiver_snapshot&
                                     ,ig_receiver_snapshot_glonum&
                                     ,ig_receiver_snapshot_total_number&
                                     ,ig_receiver_snapshot_mpi_shift&
                                     ,rg_receiver_snapshot_dx&
                                     ,rg_receiver_snapshot_dy&
                                     ,rg_mesh_xmax&
                                     ,rg_mesh_ymax&
                                     ,rg_mesh_xmin&
                                     ,rg_mesh_ymin&
                                     ,ig_receiver_snapshot_nx&
                                     ,ig_receiver_snapshot_ny&
                                     ,ig_mpi_comm_simu&
                                     ,ig_myrank

      use mod_init_memory, only : init_array_real

      implicit none

      character(len=CIL)                                 , intent(in) :: fname
      real   (kind=RXP), dimension(ig_nreceiver_snapshot), intent(in) :: val
                                          
      integer(kind=IXP), parameter                                    :: HEADER_SIZE = 892_IXP
                                          
      real   (kind=RXP)                                               :: val_max
      real   (kind=RXP)                                               :: val_min
      real   (kind=RXP)                                               :: val_max_all_cpu
      real   (kind=RXP)                                               :: val_min_all_cpu
      real   (kind=RXP), dimension(:) , allocatable                   :: val_all_cpu !allocated by cpu0 only. If compiled with -check all option, then this array must be allocated to all cpus to avoid seg_fault at runtime
      real   (kind=RXP), dimension(:) , allocatable                   :: val_all_cpu_order
                                                                      
      integer(kind=IXP)                                               :: nboctet_real
      integer(kind=IXP)                                               :: irec
      integer(kind=IXP)                                               :: rglo
      integer(kind=IXP)                                               :: desc
      integer(kind=IXP)                                               :: ios
      integer(kind=MPI_OFFSET_KIND)                                   :: pos
      integer(kind=IXP), dimension(MPI_STATUS_SIZE)                   :: statut

!
!---->compute min and max. cpu 0 gather data for writing gmt header

      if (ig_nreceiver_snapshot > ZERO_IXP) then

         val_max = maxval(val)
         val_min = minval(val)

      else

         val_min = ZERO_RXP
         val_max = ZERO_RXP

      endif

      call mpi_reduce(val_max,val_max_all_cpu,ONE_IXP,MPI_REAL,MPI_MAX,ZERO_IXP,ig_mpi_comm_simu,ios)
      call mpi_reduce(val_min,val_min_all_cpu,ONE_IXP,MPI_REAL,MPI_MIN,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!---->open file 'fname' for snapshot

      call mpi_file_open(ig_mpi_comm_simu,fname,MPI_MODE_WRONLY + MPI_MODE_CREATE,MPI_INFO_NULL,desc,ios)

      call mpi_type_size(MPI_REAL,nboctet_real,ios)

!
!------>cpu 0 writes gmt grd file header

      if (ig_myrank == ZERO_IXP) then

        call write_header_gmt_nat_fmt(desc                                            &
                                     ,ig_receiver_snapshot_nx,ig_receiver_snapshot_ny &
                                     ,rg_mesh_xmin,rg_mesh_xmax                       &
                                     ,rg_mesh_ymin,rg_mesh_ymax                       &
                                     ,val_min_all_cpu,val_max_all_cpu                 &
                                     ,rg_receiver_snapshot_dx,rg_receiver_snapshot_dy )

         ios = init_array_real(val_all_cpu,ig_receiver_snapshot_nx*ig_receiver_snapshot_ny,"val_all_cpu")

         ios = init_array_real(val_all_cpu_order,ig_receiver_snapshot_nx*ig_receiver_snapshot_ny,"val_all_cpu_order")

      endif

!
!---->cpu0 gathers array val to write gmt *.grd file

      call mpi_gatherv(val                                &
                      ,ig_nreceiver_snapshot              &
                      ,MPI_REAL                           &
                      ,val_all_cpu                        &
                      ,ig_receiver_snapshot_total_number  &
                      ,ig_receiver_snapshot_mpi_shift     &
                      ,MPI_REAL                           &
                      ,ZERO_IXP                           &
                      ,ig_mpi_comm_simu                     &
                      ,ios                                )
!
!---->cpu0 writes all receivers of all cpus using global numbering of snapshot receiver

      if (ig_myrank == ZERO_IXP) then

!
!------->order values gathered from all cpus so that cpu0 can write them at once

         do irec = ONE_IXP,ig_receiver_snapshot_nx*ig_receiver_snapshot_ny
 
            rglo =  ig_receiver_snapshot_glonum(irec)

            val_all_cpu_order(rglo) = val_all_cpu(irec)

         enddo

!
!------->cpu0 writes into GMT *.grd=bf file

         pos  = HEADER_SIZE !warning: should be written in terms of mpi_type_size

         call mpi_file_write_at(desc,pos,val_all_cpu_order,ig_receiver_snapshot_nx*ig_receiver_snapshot_ny,MPI_REAL,statut,ios)
       
      endif
!
!---->close files

      call mpi_file_close(desc,ios)

      return

!***********************************************************************************************************************************************************************************
   end subroutine write_snapshot_gmt_nat_fmt
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine writes header of binary native GMT files.
!>@param desc  : mpi_unit of the file
!>@param nx    : number of receivers along @f$x@f$-direction
!>@param ny    : number of receivers along @f$y@f$-direction
!>@param x_min : minimum @f$x@f$-coordinate
!>@param x_max : maximum @f$x@f$-coordinate
!>@param y_min : minimum @f$y@f$-coordinate
!>@param y_max : maximum @f$y@f$-coordinate
!>@param z_min : minimum @f$z@f$-coordinate
!>@param z_max : maximum @f$z@f$-coordinate
!>@param x_inc : @f$x@f$-increment between receivers
!>@param y_inc : @f$y@f$-increment between receivers
!***********************************************************************************************************************************************************************************
   subroutine write_header_gmt_nat_fmt(desc,nx,ny,x_min,x_max,y_min,y_max,z_min,z_max,x_inc,y_inc)
!***********************************************************************************************************************************************************************************

      use mpi

      implicit none

      integer(kind=IXP), intent(in)                 :: desc
      integer(kind=IXP), intent(in)                 :: nx
      integer(kind=IXP), intent(in)                 :: ny
      real(kind=RXP)  , intent(in)                 :: x_min
      real(kind=RXP)  , intent(in)                 :: x_max
      real(kind=RXP)  , intent(in)                 :: y_min
      real(kind=RXP)  , intent(in)                 :: y_max
      real(kind=RXP)  , intent(in)                 :: z_min
      real(kind=RXP)  , intent(in)                 :: z_max
      real(kind=RXP)  , intent(in)                 :: x_inc
      real(kind=RXP)  , intent(in)                 :: y_inc

      integer(kind=IXP), parameter                  :: GRD_COMMAND_LEN = 320_IXP 
      integer(kind=IXP), parameter                  :: GRD_REMARK_LEN  = 160_IXP
      integer(kind=IXP), parameter                  :: GRD_TITLE_LEN   =  80_IXP
      integer(kind=IXP), parameter                  :: GRD_UNIT_LEN    =  80_IXP
      integer(kind=IXP), parameter                  :: GRD_VARNAME_LEN =  80_IXP
      integer(kind=IXP), parameter                  :: NODE_OFFSET     =   0_IXP !if 1: pixel node registration
      real(kind=RXP)  , parameter                  :: Z_SCALE_FACTOR  =  ONE_RXP !grd values must be multiplied by this
      real(kind=RXP)  , parameter                  :: Z_ADD_OFFSET    = ZERO_RXP !After scaling, add this

      real(kind=R64), dimension(10_IXP)             :: ddum

      integer(kind=IXP), dimension(3_IXP)               :: idum
      integer(kind=IXP)                            :: nboctet_double_precision
      integer(kind=IXP)                            :: nboctet_char 
      integer(kind=IXP)                            :: nboctet_int
      integer(kind=IXP)                            :: ios
      integer(kind=mpi_offset_kind)                 :: pos
      integer(kind=IXP), dimension(mpi_status_size) :: statut

      character(len=GRD_UNIT_LEN)         :: x_units !units in x-direction
      character(len=GRD_UNIT_LEN)         :: y_units !units in y-direction
      character(len=GRD_UNIT_LEN)         :: z_units !grid value units
      character(len=GRD_TITLE_LEN)        :: title   !name of data set
      character(len=GRD_COMMAND_LEN)      :: command !name of generating command
      character(len=GRD_REMARK_LEN)       :: remark  !comments on this data set


      call mpi_type_size(MPI_DOUBLE_PRECISION,nboctet_double_precision,ios)
      call mpi_type_size(MPI_CHARACTER       ,nboctet_char            ,ios)
      call mpi_type_size(MPI_INTEGER         ,nboctet_int             ,ios)

      x_units = "m"//CHAR(0)
      y_units = "m"//CHAR(0)
      z_units = " "//CHAR(0)
      title   = "EFISPEC3D snapshot"//CHAR(0)
      command = "mpi_file_write_at()"//CHAR(0)
      remark  = "created by EFISPEC3D: http://efispec.free.fr"//CHAR(0)

      idum(1_IXP) = nx
      idum(2_IXP) = ny
      idum(3_IXP) = NODE_OFFSET

      ddum(1_IXP) = real(x_min,kind=R64)

      if (NODE_OFFSET == 1_IXP) then
         ddum(2_IXP) = real(x_max,kind=R64)+real(x_inc,kind=R64) !x_inc must be added to the size of the domain because of pixel node registration (see variable NODE_OFFSET)
         ddum(4_IXP) = real(y_max,kind=R64)+real(y_inc,kind=R64) !y_inc must be added to the size of the domain because of pixel node registration (see variable NODE_OFFSET)
      else
         ddum(2_IXP) = real(x_max,kind=R64)
         ddum(4_IXP) = real(y_max,kind=R64)
      endif

      ddum( 3_IXP) = real(y_min,kind=R64)
      ddum( 5_IXP) = real(z_min,kind=R64)
      ddum( 6_IXP) = real(z_max,kind=R64)
      ddum( 7_IXP) = real(x_inc,kind=R64)
      ddum( 8_IXP) = real(y_inc,kind=R64)
      ddum( 9_IXP) = real(Z_SCALE_FACTOR,kind=R64) 
      ddum(10_IXP) = real(Z_ADD_OFFSET,kind=R64)

      pos = ZERO_I64
      call mpi_file_write_at(desc,pos,idum,THREE_IXP,MPI_INTEGER,statut,ios)

      pos = pos + int(THREE_IXP*nboctet_int,kind=I64)
      call mpi_file_write_at(desc,pos,ddum,10_IXP,MPI_DOUBLE_PRECISION,statut,ios)

      pos = pos + int(10_IXP*nboctet_double_precision,kind=I64)
      call mpi_file_write_at(desc,pos,x_units,GRD_UNIT_LEN,MPI_CHARACTER,statut,ios)

      pos = pos + int(nboctet_char*GRD_UNIT_LEN,kind=I64)
      call mpi_file_write_at(desc,pos,y_units,GRD_UNIT_LEN,MPI_CHARACTER,statut,ios)

      pos = pos + int(nboctet_char*GRD_UNIT_LEN,kind=I64)
      call mpi_file_write_at(desc,pos,z_units,GRD_UNIT_LEN,MPI_CHARACTER,statut,ios)

      pos = pos + int(nboctet_char*GRD_UNIT_LEN,kind=I64)
      call mpi_file_write_at(desc,pos,title,GRD_TITLE_LEN,MPI_CHARACTER,statut,ios)

      pos = pos + int(nboctet_char*GRD_TITLE_LEN,kind=I64)
      call mpi_file_write_at(desc,pos,command,GRD_COMMAND_LEN,MPI_CHARACTER,statut,ios)

      pos = pos + int(nboctet_char*GRD_COMMAND_LEN,kind=I64)
      call mpi_file_write_at(desc,pos,remark,GRD_REMARK_LEN,MPI_CHARACTER,statut,ios)

      return

!***********************************************************************************************************************************************************************************
   end subroutine write_header_gmt_nat_fmt
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine writes structured grid snapshot in binary VTK xml format.
!>@param fname   : file name of the snapshot
!>@param varname : variable name
!>@param valx    : @f$x@f$-direction values
!>@param valy    : @f$y@f$-direction values
!>@param valz    : @f$z@f$-direction values
!***********************************************************************************************************************************************************************************
   subroutine write_snapshot_vtk(fname,varname,valx,valy,valz)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                      ig_nreceiver_snapshot&
                                     ,ig_receiver_snapshot_total_number&
                                     ,ig_receiver_snapshot_mpi_shift&
                                     ,ig_receiver_snapshot_locnum&
                                     ,rg_receiver_snapshot_z&
                                     ,ig_myrank&
                                     ,error_stop&
                                     ,ig_mpi_comm_simu&
                                     ,ig_receiver_snapshot_nx&
                                     ,ig_receiver_snapshot_ny&
                                     ,rg_receiver_snapshot_dx&
                                     ,rg_receiver_snapshot_dy&
                                     ,rg_mesh_xmin&
                                     ,rg_mesh_ymax

      use mod_vtk_io

      implicit none

      character(len=CIL), intent(in) :: fname
      character(len=  4), intent(in) :: varname

      real(kind=RXP)             , intent(in) :: valx(ig_nreceiver_snapshot)
      real(kind=RXP)             , intent(in) :: valy(ig_nreceiver_snapshot)
      real(kind=RXP)             , intent(in) :: valz(ig_nreceiver_snapshot)
                        
      real(kind=RXP)                          :: val_all_cpu(ig_receiver_snapshot_nx*ig_receiver_snapshot_ny)
      real(kind=RXP)                          :: val_all_cpu_order(ig_receiver_snapshot_nx*ig_receiver_snapshot_ny)
      real(kind=RXP)                          :: xrec(ig_receiver_snapshot_nx*ig_receiver_snapshot_ny)
      real(kind=RXP)                          :: yrec(ig_receiver_snapshot_nx*ig_receiver_snapshot_ny)
                        
      integer(kind=IXP)                       :: ix
      integer(kind=IXP)                       :: iy
      integer(kind=IXP)                       :: irec
      integer(kind=IXP)                       :: irec_gatherv
      integer(kind=IXP)                       :: ios

      character(len=CIL)             :: info

!
!---->cpu0 gathers array valx to be store in VTK file

      call mpi_gatherv(valx                               &
                      ,ig_nreceiver_snapshot              &
                      ,MPI_REAL                           &
                      ,val_all_cpu                        &
                      ,ig_receiver_snapshot_total_number  &
                      ,ig_receiver_snapshot_mpi_shift     &
                      ,MPI_REAL                           &
                      ,ZERO_IXP                           &
                      ,ig_mpi_comm_simu                     &
                      ,ios                                )
!
!---->cpu0 writes all receivers of all cpus using global numbering of snapshot receiver

      if (ig_myrank == ZERO_IXP) then

!
!------->compute x, y and z coordinates of receiver snapshot 

         irec = ZERO_IXP

         do iy = ONE_IXP,ig_receiver_snapshot_ny
            do ix = ONE_IXP,ig_receiver_snapshot_nx

               irec                    = irec + ONE_IXP
               irec_gatherv            = ig_receiver_snapshot_locnum(irec)
                                       
               xrec(irec)              = rg_mesh_xmin + real(ix-ONE_IXP,kind=RXP)*rg_receiver_snapshot_dx
               yrec(irec)              = rg_mesh_ymax - real(iy-ONE_IXP,kind=RXP)*rg_receiver_snapshot_dy !y for VTK coordinate system

               val_all_cpu_order(irec) = val_all_cpu(irec_gatherv)

            enddo
         enddo 

!
!------->initialize VTK file

         ios = VTK_INI_XML(                                        &
                           output_format = 'BINARY'                &
                          ,filename      = trim(fname)             &
                          ,mesh_topology = 'StructuredGrid'        &
                          ,nx1           = ONE_IXP                 &
                          ,nx2           = ig_receiver_snapshot_nx &
                          ,ny1           = ONE_IXP                 &
                          ,ny2           = ig_receiver_snapshot_ny &
                          ,nz1           = ONE_IXP                 &
                          ,nz2           = ONE_IXP                 )

         if (ios /= ZERO_IXP) then
            write(info,'(a)') "error in subroutine write_snapshot_vtk: VTK_INI_XML"
            call error_stop(info)
         endif

!
!------->store free surface geometry

         ios = VTK_GEO_XML(                                                                &
                           nx1           = ONE_IXP                                         &
                          ,nx2           = ig_receiver_snapshot_nx                         &
                          ,ny1           = ONE_IXP                                         &
                          ,ny2           = ig_receiver_snapshot_ny                         &
                          ,nz1           = ONE_IXP                                         &
                          ,nz2           = ONE_IXP                                         &
                          ,NN            = ig_receiver_snapshot_nx*ig_receiver_snapshot_ny &
                          ,X             = xrec                                            &
                          ,Y             = yrec                                            &
                          ,Z             = rg_receiver_snapshot_z                          )

         if (ios /= ZERO_IXP) then

            write(info,'(a)') "error in subroutine write_snapshot_vtk: VTK_GEO_XML (init)"
            call error_stop(info)

         endif
         
!
!------->store x-component

         ios = VTK_DAT_XML(                         &
                           var_location     = 'NODE'&
                          ,var_block_action = 'OPEN')

         if (ios /= ZERO_IXP) then
            write(info,'(a)') "error in subroutine write_snapshot_vtk: VTK_DAT_XML (open)"
            call error_stop(info)
         endif
         
         ios = VTK_VAR_XML(                                                          &
                           NC_NN   = ig_receiver_snapshot_nx*ig_receiver_snapshot_ny &
                          ,varname = "X-"//trim(varname)                             &
                          ,var     = val_all_cpu_order                               &
                                                                                     )

         if (ios /= ZERO_IXP) then
            write(info,'(a)') "error in subroutine write_snapshot_vtk: VTK_VAR_XML X-component"
            call error_stop(info)
         endif

      endif

!
!---->cpu0 gathers array valy to be store in VTK file

      call mpi_gatherv(valy                              &
                      ,ig_nreceiver_snapshot             &
                      ,MPI_REAL                          &
                      ,val_all_cpu                       &
                      ,ig_receiver_snapshot_total_number &
                      ,ig_receiver_snapshot_mpi_shift    &
                      ,MPI_REAL                          &
                      ,ZERO_IXP                          &
                      ,ig_mpi_comm_simu                    &
                      ,ios                               )


      if (ig_myrank == ZERO_IXP) then

!
!------->order data from gatherv

         irec = ZERO_IXP

         do iy = ONE_IXP,ig_receiver_snapshot_ny
            do ix = ONE_IXP,ig_receiver_snapshot_nx

               irec                    = irec + ONE_IXP
               irec_gatherv            = ig_receiver_snapshot_locnum(irec)
               val_all_cpu_order(irec) = val_all_cpu(irec_gatherv)

            enddo
         enddo 

!
!------->store y-component

         ios = VTK_VAR_XML(                                                          &
                           NC_NN   = ig_receiver_snapshot_nx*ig_receiver_snapshot_ny &
                          ,varname = "Y-"//trim(varname)                             &
                          ,var     = val_all_cpu_order                               &
                                                                                     )
         if (ios /= ZERO_IXP) then
            write(info,'(a)') "error in subroutine write_snapshot_vtk: VTK_VAR_XML Y-component"
            call error_stop(info)
         endif

      endif

!
!---->cpu0 gathers array valz to be store in VTK file

      call mpi_gatherv(valz                              &
                      ,ig_nreceiver_snapshot             &
                      ,MPI_REAL                          &
                      ,val_all_cpu                       &
                      ,ig_receiver_snapshot_total_number &
                      ,ig_receiver_snapshot_mpi_shift    &
                      ,MPI_REAL                          &
                      ,ZERO_IXP                          &
                      ,ig_mpi_comm_simu                    &
                      ,ios                               )

      if (ig_myrank == ZERO_IXP) then

!
!------->order data from gatherv

         irec = ZERO_IXP

         do iy = ONE_IXP,ig_receiver_snapshot_ny
            do ix = ONE_IXP,ig_receiver_snapshot_nx

               irec                    = irec + ONE_IXP
               irec_gatherv            = ig_receiver_snapshot_locnum(irec)
               val_all_cpu_order(irec) = val_all_cpu(irec_gatherv)

            enddo
         enddo
!
!------->store z-component

         ios = VTK_VAR_XML(                                                          &
                           NC_NN   = ig_receiver_snapshot_nx*ig_receiver_snapshot_ny &
                          ,varname = "Z-"//trim(varname)                             &
                          ,var     = val_all_cpu_order                               &
                                                                                     )

         if (ios /= ZERO_IXP) then
            write(info,'(a)') "error in subroutine write_snapshot_vtk: VTK_VAR_XML Z-component"
            call error_stop(info)
         endif
         
!
!------->finalize VTK file

         ios = VTK_DAT_XML(                          &
                           var_location     = 'NODE' &
                          ,var_block_action = 'CLOSE')

         if (ios /= ZERO_IXP) then
            write(info,'(a)') "error in subroutine write_snapshot_vtk: VTK_DAT_XML (close)"
            call error_stop(info)
         endif
         
         ios = VTK_GEO_XML()

         if (ios /= ZERO_IXP) then
            write(info,'(a)') "error in subroutine write_snapshot_vtk: VTK_GEO_XML (finalize)"
            call error_stop(info)
         endif

         ios = VTK_END_XML()

         if (ios /= ZERO_IXP) then
            write(info,'(a)') "error in subroutine write_snapshot_vtk: VTK_END_XML"
            call error_stop(info)
         endif

      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_snapshot_vtk
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine selects which ParaView collection files should be written (displacement, velocity and/or acceleration collections)
!!depending on value of variables mod_global_variables::lg_snapshot_displacement, mod_global_variables::lg_snapshot_velocity and mod_global_variables::lg_snapshot_acceleration.
!***********************************************************************************************************************************************************************************
   subroutine write_collection_vtk_surf()
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      lg_snapshot_displacement&
                                     ,lg_snapshot_velocity&
                                     ,lg_snapshot_acceleration

      implicit none

      if (lg_snapshot_displacement) then
         call collection_vtk_surface("snapshot.uxyz")
      endif

      if (lg_snapshot_velocity) then
         call collection_vtk_surface("snapshot.vxyz")
      endif

      if (lg_snapshot_acceleration) then
         call collection_vtk_surface("snapshot.axyz")
      endif

      return
!***********************************************************************************************************************************************************************************
   end subroutine write_collection_vtk_surf
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine write Paraview collection file *.pvd of VTK xml snapshot files *.vts.
!>@param varname : variable name
!***********************************************************************************************************************************************************************************
   subroutine collection_vtk_surface(varname)
!***********************************************************************************************************************************************************************************

      use mod_global_variables, only :&
                                      get_newunit&
                                     ,cg_prefix&
                                     ,rg_dt&
                                     ,ig_ndt&
                                     ,ig_snapshot_saving_incr
               
      implicit none

      character(len=*), intent(in) :: varname

      real(kind=RXP)                        :: time

      integer(kind=IXP)                     :: istep
      integer(kind=IXP)                     :: myunit

      character(len=CIL)           :: fname
      character(len=6  )           :: csnapshot

      open(unit=get_newunit(myunit),file=trim(cg_prefix)//".collection."//trim(varname)//".pvd")

      write(unit=myunit,fmt='(a)') "<?xml version=""1.0""?>"
      write(unit=myunit,fmt='(a)') "<VTKFile type=""Collection"" version=""0.1"" byte_order=""BigEndian"">"
      write(unit=myunit,fmt='(a)') "  <Collection>"

      do istep = ONE_IXP,ig_ndt,ig_snapshot_saving_incr

         write(csnapshot,'(I6.6)') istep

         time  = real(istep-ONE_IXP,kind=RXP)*rg_dt

         fname = trim(cg_prefix)//"."//trim(varname)//"."//trim(csnapshot)//".vts"

         write(unit=myunit,fmt='(a,E14.7,3a)') "    <DataSet timestep=""",time,""" group="""" part=""0"" file=""",trim(fname),"""/>"

      enddo

      write(unit=myunit,fmt='(a)') "  </Collection>"
      write(unit=myunit,fmt='(a)') "</VTKFile>"

      close(myunit)

      return
!***********************************************************************************************************************************************************************************
   end subroutine collection_vtk_surface
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine writes peak ground motions files in GMT or VTK formats.
!***********************************************************************************************************************************************************************************
   subroutine write_peak_ground_motion()
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                      tg_receiver_snapshot_quad&
                                     ,ig_myrank&
                                     ,ig_nreceiver_snapshot&
                                     ,cg_prefix&
                                     ,LG_SNAPSHOT_VTK&
                                     ,IG_LST_UNIT

      implicit none

      real(kind=RXP), dimension(ig_nreceiver_snapshot) :: pgd_x
      real(kind=RXP), dimension(ig_nreceiver_snapshot) :: pgd_y
      real(kind=RXP), dimension(ig_nreceiver_snapshot) :: pgd_z
      real(kind=RXP), dimension(ig_nreceiver_snapshot) :: pgv_x
      real(kind=RXP), dimension(ig_nreceiver_snapshot) :: pgv_y
      real(kind=RXP), dimension(ig_nreceiver_snapshot) :: pgv_z
      real(kind=RXP), dimension(ig_nreceiver_snapshot) :: pga_x
      real(kind=RXP), dimension(ig_nreceiver_snapshot) :: pga_y
      real(kind=RXP), dimension(ig_nreceiver_snapshot) :: pga_z

      integer(kind=IXP)                                :: irec

      character(len=CIL)                               :: fname

      if (ig_myrank == ZERO_IXP) then
         write(IG_LST_UNIT,'(" ",/,a)') "writing snapshots of peak ground motion..."
         call flush(IG_LST_UNIT)
      endif

      do irec = ONE_IXP,ig_nreceiver_snapshot
         pgd_x(irec) = tg_receiver_snapshot_quad(irec)%pgd_x
      enddo

      do irec = ONE_IXP,ig_nreceiver_snapshot
         pgd_y(irec) = tg_receiver_snapshot_quad(irec)%pgd_y
      enddo

      do irec = ONE_IXP,ig_nreceiver_snapshot
         pgd_z(irec) = tg_receiver_snapshot_quad(irec)%pgd_z
      enddo

      do irec = ONE_IXP,ig_nreceiver_snapshot
         pgv_x(irec) = tg_receiver_snapshot_quad(irec)%pgv_x
      enddo

      do irec = ONE_IXP,ig_nreceiver_snapshot
         pgv_y(irec) = tg_receiver_snapshot_quad(irec)%pgv_y
      enddo

      do irec = ONE_IXP,ig_nreceiver_snapshot
         pgv_z(irec) = tg_receiver_snapshot_quad(irec)%pgv_z
      enddo

      do irec = ONE_IXP,ig_nreceiver_snapshot
         pga_x(irec) = tg_receiver_snapshot_quad(irec)%pga_x
      enddo

      do irec = ONE_IXP,ig_nreceiver_snapshot
         pga_y(irec) = tg_receiver_snapshot_quad(irec)%pga_y
      enddo

      do irec = ONE_IXP,ig_nreceiver_snapshot
         pga_z(irec) = tg_receiver_snapshot_quad(irec)%pga_z
      enddo

!
!---->write peak ground motion in GMT format

      fname = trim(cg_prefix)//".snapshot.PGDx.grd"
      call write_snapshot_gmt_nat_fmt(fname,pgd_x)
      
      fname = trim(cg_prefix)//".snapshot.PGDy.grd"
      call write_snapshot_gmt_nat_fmt(fname,pgd_y)
      
      fname = trim(cg_prefix)//".snapshot.PGDz.grd"
      call write_snapshot_gmt_nat_fmt(fname,pgd_z)
      
      fname = trim(cg_prefix)//".snapshot.PGVx.grd"
      call write_snapshot_gmt_nat_fmt(fname,pgv_x)
      
      fname = trim(cg_prefix)//".snapshot.PGVy.grd"
      call write_snapshot_gmt_nat_fmt(fname,pgv_y)
      
      fname = trim(cg_prefix)//".snapshot.PGVz.grd"
      call write_snapshot_gmt_nat_fmt(fname,pgv_z)
      
      fname = trim(cg_prefix)//".snapshot.PGAx.grd"
      call write_snapshot_gmt_nat_fmt(fname,pga_x)
      
      fname = trim(cg_prefix)//".snapshot.PGAy.grd"
      call write_snapshot_gmt_nat_fmt(fname,pga_y)
      
      fname = trim(cg_prefix)//".snapshot.PGAz.grd"
      call write_snapshot_gmt_nat_fmt(fname,pga_z)

!
!---->write peak ground motion in VTK format

      if (LG_SNAPSHOT_VTK) then

         fname = trim(cg_prefix)//".snapshot.PGDxyz.vts"
         call write_snapshot_vtk(fname,"displacement",pgd_x,pgd_y,pgd_z)
        
         fname = trim(cg_prefix)//".snapshot.PGVxyz.vts"
         call write_snapshot_vtk(fname,"velocity"    ,pgv_x,pgv_y,pgv_z)
        
         fname = trim(cg_prefix)//".snapshot.PGAxyz.vts"
         call write_snapshot_vtk(fname,"acceleration",pga_x,pga_y,pga_z)

      endif

      if (ig_myrank == ZERO_IXP) then
         write(IG_LST_UNIT,'(a)') "done"
         call flush(IG_LST_UNIT)
      endif

      return

!***********************************************************************************************************************************************************************************
   end subroutine write_peak_ground_motion
!***********************************************************************************************************************************************************************************

!
!
!>@brief
!!This subroutine writes peak ground motions files in GMT or VTK formats.
!***********************************************************************************************************************************************************************************
   subroutine write_peak_ground_motion_gll(quad_gll_global,fname)
!***********************************************************************************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                      ig_myrank&
                                     ,tg_receiver_snapshot_quad&
                                     ,ig_nreceiver_snapshot&
                                     ,ig_ngll_total&
                                     ,IG_NGLL&
                                     ,IG_LST_UNIT

      use mod_gll_value, only : get_quad_gll_value

      use mod_lagrange , only : quad_lagrange_interp

      implicit none

      real   (kind=RXP), dimension(ig_ngll_total), intent(in) :: quad_gll_global
      character(len=*)                           , intent(in) :: fname

      real   (kind=RXP), dimension(ig_nreceiver_snapshot)     :: grid
      real   (kind=RXP), dimension(IG_NGLL,IG_NGLL)           :: quad_gll_local
      integer(kind=IXP)                                       :: irec

      if (ig_myrank == ZERO_IXP) then

         write(IG_LST_UNIT,'(" ",/,a)') "writing snapshots of peak ground motion in file "//trim(fname)

      endif

     !print *,"warning flo"

      do irec = ONE_IXP,ig_nreceiver_snapshot

!
!------->get GLL nodes displacement, velocity and acceleration xyz-values from quadrangle element which contains the snapshot receiver

         call get_quad_gll_value(quad_gll_global,tg_receiver_snapshot_quad(irec)%gll,quad_gll_local)

!
!------->interpolate displacement at the snapshot receiver

         call quad_lagrange_interp(quad_gll_local,tg_receiver_snapshot_quad(irec)%lag,grid(irec))
        !grid(irec) = quad_gll_local(3_IXP,3_IXP)

      enddo

!
!---->write peak ground motion in GMT format

      call write_snapshot_gmt_nat_fmt(fname,grid)
      
      return

!***********************************************************************************************************************************************************************************
   end subroutine write_peak_ground_motion_gll
!***********************************************************************************************************************************************************************************


!
!
!**************************************************************************************************************************************************************************************
      subroutine convert_quad_to_grd_lag_interp(quad_gll,grid_meta,grd)
!**************************************************************************************************************************************************************************************

         use mod_global_variables, only : IG_NGLL

         use mod_lagrange, only : lagrap,quad_lagrange_interp

         implicit none

         real(kind=RXP), intent(in ), dimension(:,:,:)          :: quad_gll
         real(kind=RXP), intent(in ), dimension(:,:)            :: grid_meta
         real(kind=RXP), intent(out), dimension(:), allocatable :: grd

         real(kind=RXP), dimension(IG_NGLL,IG_NGLL)             :: gll
         real(kind=RXP), dimension(IG_NGLL,IG_NGLL)             :: lag
         real(kind=RXP)                                         :: v
         real(kind=RXP)                                         :: xi
         real(kind=RXP)                                         :: et

         integer(kind=IXP)                                      :: i
         integer(kind=IXP)                                      :: n
         integer(kind=IXP)                                      :: kgll
         integer(kind=IXP)                                      :: lgll
         integer(kind=IXP)                                      :: iquad

         n = size(grid_meta,TWO_IXP)

         allocate(grd(n))

         do i = ONE_IXP,n

            iquad = int(grid_meta(4_IXP,i),kind=IXP)

!
!---------->get GLLs value

            do kgll = ONE_IXP,IG_NGLL

               do lgll = ONE_IXP,IG_NGLL

                  gll(lgll,kgll) = quad_gll(lgll,kgll,iquad)

               enddo

            enddo

!
!---------->compute lagrange at xi,eta of the grid point

            xi = grid_meta(5_IXP,i)
            et = grid_meta(6_IXP,i)

            if (xi > +ONE_RXP) xi = +ONE_RXP
            if (et > +ONE_RXP) et = +ONE_RXP
            if (xi < -ONE_RXP) xi = -ONE_RXP
            if (et < -ONE_RXP) et = -ONE_RXP

            do kgll = ONE_IXP,IG_NGLL

               do lgll = ONE_IXP,IG_NGLL

                  lag(lgll,kgll) = lagrap(lgll,xi,IG_NGLL)*lagrap(kgll,et,IG_NGLL)

               enddo

            enddo

!
!---------->interpolate

            call quad_lagrange_interp(gll,lag,v)
 
!
!---------->store to grid array

            grd(i) = v

         enddo   

         return

!**************************************************************************************************************************************************************************************
      end subroutine convert_quad_to_grd_lag_interp
!**************************************************************************************************************************************************************************************


!
!
!**************************************************************************************************************************************************************************************
      subroutine write_grid_meta(tl_receiver_quad)
!**************************************************************************************************************************************************************************************

         use mod_global_variables, only :&
                                         ig_myrank&
                                        ,ig_ncpu&
                                        ,cg_prefix&
                                        ,ig_mpi_comm_simu&
                                        ,type_receiver_quad&
                                        ,ig_nquad_fsurf_all_cpu&
                                        ,ig_nreceiver_snapshot&
                                        ,ig_mpi_nboctet_real

         use mod_io_array        , only : get_file_rank_elt_offset

         use mod_init_memory     , only : init_array_real

         implicit none

         type(type_receiver_quad), intent(in), dimension(ig_nreceiver_snapshot) :: tl_receiver_quad
                                                       
                                                       
         real   (kind=RXP), dimension(SIX_IXP)                                  :: receivers_info
                                                                                
         integer(kind=IXP)                                                      :: nelt_rank
         integer(kind=IXP)                                                      :: nelt_floor
         integer(kind=IXP)                                                      :: ielt_offset
         integer(kind=IXP)                                                      :: irec
         integer(kind=IXP)                                                      :: desc
         integer(kind=IXP)                                                      :: ios
         integer(kind=I64)                                                      :: pos_dis
                                                                                
         integer(kind=IXP), dimension(MPI_STATUS_SIZE)                          :: statut
                                                                                
         character(len=CIL)                                                     :: fname


         call get_file_rank_elt_offset(ig_myrank,ig_ncpu,ig_nquad_fsurf_all_cpu,nelt_rank,nelt_floor,ielt_offset)

         if (ig_nreceiver_snapshot > ZERO_IXP) then
         
            fname = trim(cg_prefix)//".snapshot.gll2grd"
         
            call mpi_file_open(ig_mpi_comm_simu,fname,MPI_MODE_WRONLY + MPI_MODE_CREATE,MPI_INFO_NULL,desc,ios)
         
            do irec = ONE_IXP,ig_nreceiver_snapshot
         
               receivers_info(1_IXP) = tl_receiver_quad(irec)%x
               receivers_info(2_IXP) = tl_receiver_quad(irec)%y
               receivers_info(3_IXP) = tl_receiver_quad(irec)%z
               receivers_info(4_IXP) = real(tl_receiver_quad(irec)%iel + ielt_offset - ONE_IXP,kind=RXP)
               receivers_info(5_IXP) = tl_receiver_quad(irec)%xi
               receivers_info(6_IXP) = tl_receiver_quad(irec)%eta
         
               pos_dis = (int(tl_receiver_quad(irec)%rglo,kind=I64)-ONE_I64)*6_I64*int(ig_mpi_nboctet_real,kind=I64)
         
               call mpi_file_write_at(desc,pos_dis,receivers_info,6_IXP,MPI_REAL,statut,ios)
         
            enddo
         
            call mpi_file_close(desc,ios)
         
         endif

         return

!**************************************************************************************************************************************************************************************
      end subroutine write_grid_meta
!**************************************************************************************************************************************************************************************

end module mod_snapshot_surface
