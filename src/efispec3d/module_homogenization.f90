!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module for homogenizing jump/discontinuity in mechanical properties at the interfaces of layers by using a vertical homogenization method.

!>@author E. Chaljub and F. De Martin 
!!Original sources have been modified under GNU GPL V3 license terms to suit EFISPEC3D purpose.

!>@brief
!!This module contains subroutines for homogenizing discontinuity in mechanical properties at the interfaces of layers by using a vertical homogenization method.

module mod_homogenization

   use mod_precision

   use mod_init_memory

   use mod_global_variables, only :&
                                    error_stop&
                                   ,CIL

   implicit none

   character(len=CIL) :: info

   private

   public  :: medium_homogenization
   private :: init_column_discontinuity
   private :: init_column_material
   private :: init_column_material_discretized
   private :: compute_homogenization
   private :: compute_homogenization_random
   private :: find_top_layer 
   private :: find_bot_layer 
   private :: integrate_within_layer 
   public  :: init_medium_visco
   private :: read_interfaces 

   contains


!
!>@brief subroutine to compute medium homogeneization based on 1D soil column
!>@return mod_global_variables::rg_hexa_gll_rho
!>@return mod_global_variables::rg_hexa_gll_rhovs2
!>@return mod_global_variables::rg_hexa_gll_rhovp2
!>@return mod_global_variables::rg_hexa_gll_wkqs
!>@return mod_global_variables::rg_hexa_gll_wkqp
!*************************************************************************************************************************************
      subroutine medium_homogenization()
!*************************************************************************************************************************************

         use mpi

         use mod_global_variables, only :&
                                         ig_mpi_comm_simu&
                                        ,cg_prefix&
                                        ,ig_myrank&
                                        ,ig_nhexa&
                                        ,ig_nlayer&
                                        ,IG_LST_UNIT&
                                        ,IG_NGLL&
                                        ,IG_NRELAX&
                                        ,LG_VISCO&
                                        ,ig_hexa_gll_glonum&
                                        ,rg_gll_coordinate&
                                        ,tg_layer&
                                        ,type_random_material_KL&
                                        ,rg_hexa_gll_rho&
                                        ,rg_hexa_gll_rhovs2&
                                        ,rg_hexa_gll_rhovp2&
                                        ,rg_hexa_gll_wkqs&
                                        ,rg_hexa_gll_wkqp&
                                        ,ig_hexa_gnode_glonum&
                                        ,rg_gnode_x&
                                        ,rg_gnode_y&
                                        ,rg_gnode_z

         use mod_coordinate      , only :&
                                         compute_edge_min

         use mod_random_field

         real   (kind=RXP), allocatable, dimension(:,:,:)     :: interfaces
         real   (kind=RXP), allocatable, dimension(:,:)       :: interfaces_minmax
         real   (kind=RXP)                                    :: xin
         real   (kind=RXP)                                    :: yin
         real   (kind=RXP)                                    :: zin
         real   (kind=RXP)                                    :: h
         real   (kind=RXP)                                    :: vp
         real   (kind=RXP)                                    :: vs
         real   (kind=RXP)                                    :: rho
         real   (kind=RXP)                                    :: qp
         real   (kind=RXP)                                    :: qs
         real   (kind=RXP)                                    :: qf
         real   (kind=RXP), dimension(IG_NRELAX)              :: wkqp
         real   (kind=RXP), dimension(IG_NRELAX)              :: wkqs
         real   (kind=RXP)                                    :: upwm
         real   (kind=RXP)                                    :: uswm
         real   (kind=RXP)                                    :: z_min
         real   (kind=RXP)                                    :: dz
         real   (kind=RXP)                                    :: lz

         real   (kind=RXP), allocatable, dimension(:)         :: zdisc_orig
         real   (kind=RXP)                                    :: hexa_edge_min
         real   (kind=RXP)                                    :: lcx_norm
         real   (kind=RXP)                                    :: lcy_norm
         real   (kind=RXP)                                    :: lcz_norm
         real   (kind=RXP)                                    :: xllcorner
         real   (kind=RXP)                                    :: yllcorner
         real   (kind=RXP)                                    :: cellsize

         logical(kind=IXP)             , dimension(ig_nlayer) :: is_layer_exist
                                                              
         integer(kind=IXP), allocatable, dimension(:)         :: zdisc_orig_layer
         integer(kind=IXP)                                    :: ncols
         integer(kind=IXP)                                    :: nrows
         integer(kind=IXP)                                    :: iz
         integer(kind=IXP)                                    :: ilay
         integer(kind=IXP)                                    :: ihexa
         integer(kind=IXP)                                    :: igll
         integer(kind=IXP)                                    :: k
         integer(kind=IXP)                                    :: l
         integer(kind=IXP)                                    :: m
         integer(kind=IXP)                                    :: n
         integer(kind=IXP)                                    :: nlayer
         integer(kind=IXP)                                    :: ios
         integer(kind=IXP)                                    :: myunit
         integer(kind=IXP)                                    :: u
         integer(kind=IXP)                                    :: irec
         integer(kind=IXP)                                    :: nz
         integer(kind=IXP)                                    :: lb

         character(len=6_IXP)                                 :: crec

!
!------->read all interfaces (files with extension *.if*)

         call read_interfaces(interfaces,ncols,nrows,xllcorner,yllcorner,cellsize)

!
!------->define the averaging length (in meter) used for homogenization

         hexa_edge_min = compute_edge_min(ig_hexa_gnode_glonum,rg_gnode_x,rg_gnode_y,rg_gnode_z)

         call mpi_allreduce(hexa_edge_min,h,ONE_IXP,MPI_REAL,MPI_MIN,ig_mpi_comm_simu,ios)

         if (ig_myrank == ZERO_IXP) then

            write(IG_LST_UNIT,'(/,a,f15.7,a)') " --> homogenization length      = ",h," m"

            write(IG_LST_UNIT,'(/,a)') "min/max extension of layers"

         endif

!
!------->initialize random medium properties for each layers using a Karuhnen-Loeve expansion -> find roots, eigeinvalues and eigenmodes for exponential kernel

         call minmax_interfaces(interfaces,cellsize,interfaces_minmax)

         do ilay = ONE_IXP,ig_nlayer
         
            tg_layer(ilay)%xmin  = interfaces_minmax(1_IXP,ilay)
            tg_layer(ilay)%xmax  = interfaces_minmax(2_IXP,ilay)
            tg_layer(ilay)%ymin  = interfaces_minmax(3_IXP,ilay)
            tg_layer(ilay)%ymax  = interfaces_minmax(4_IXP,ilay)
            tg_layer(ilay)%zmin  = interfaces_minmax(5_IXP,ilay)
            tg_layer(ilay)%zmax  = interfaces_minmax(6_IXP,ilay)
            tg_layer(ilay)%zminh = interfaces_minmax(5_IXP,ilay) - h !enlarge a bit zmin an zmax for security when generating the box containing the random field
            tg_layer(ilay)%zmaxh = interfaces_minmax(6_IXP,ilay) + h

            tg_layer(ilay)%xl = tg_layer(ilay)%xmax  - tg_layer(ilay)%xmin
            tg_layer(ilay)%yl = tg_layer(ilay)%ymax  - tg_layer(ilay)%ymin
            tg_layer(ilay)%zl = tg_layer(ilay)%zmaxh - tg_layer(ilay)%zminh

            if (tg_layer(ilay)%structure_type == 3_IXP) then
   
               lcx_norm = tg_layer(ilay)%random(ONE_IXP)%lcx/tg_layer(ilay)%xl
               lcy_norm = tg_layer(ilay)%random(ONE_IXP)%lcy/tg_layer(ilay)%yl
               lcz_norm = tg_layer(ilay)%random(ONE_IXP)%lcz/tg_layer(ilay)%zl

               call KL_roots(lcx_norm,tg_layer(ilay)%KLx)
               call KL_roots(lcy_norm,tg_layer(ilay)%KLy)
               call KL_roots(lcz_norm,tg_layer(ilay)%KLz)

               call KL_sort(tg_layer(ilay)%KLx,tg_layer(ilay)%KLy,tg_layer(ilay)%KLz,tg_layer(ilay)%modes_ind)

               call KL_init_distribution(ilay,tg_layer(ilay)%modes_ind,tg_layer(ilay)%random(ONE_IXP)%rd)

            endif
         
            if (ig_myrank == ZERO_IXP) then
          
               write(IG_LST_UNIT,'(a,i0,a,2(E15.7,1X)  )') " --> min/max of layer ",ilay," along x-direction = ",tg_layer(ilay)%xmin,tg_layer(ilay)%xmax
               write(IG_LST_UNIT,'(a,i0,a,2(E15.7,1X)  )') " --> min/max of layer ",ilay," along y-direction = ",tg_layer(ilay)%ymin,tg_layer(ilay)%ymax
               write(IG_LST_UNIT,'(a,i0,a,2(E15.7,1X),/)') " --> min/max of layer ",ilay," along z-direction = ",tg_layer(ilay)%zmin,tg_layer(ilay)%zmax
          
            endif

         enddo

!
!
!------->compute homogeneized properties for each GLL points

         do ihexa = ONE_IXP,ig_nhexa

            do k = ONE_IXP,IG_NGLL
               do l = ONE_IXP,IG_NGLL
                  do m = ONE_IXP,IG_NGLL

                     igll = ig_hexa_gll_glonum(m,l,k,ihexa)

                     xin = rg_gll_coordinate(1_IXP,igll)
                     yin = rg_gll_coordinate(2_IXP,igll)
                     zin = rg_gll_coordinate(3_IXP,igll)

!
!------------------->initialize the discontinuities of the 1D soil column at xin,yin

                     call init_column_discontinuity(xin,yin,h,ncols,nrows,xllcorner,yllcorner,cellsize,interfaces,nlayer,zdisc_orig,zdisc_orig_layer,is_layer_exist)

!
!------------------->initialize the mechanical properties of the 1D soil column at xin,yin,zin and compute values of the effective model at elevation zin

                     !S-wave velocity
                     call init_column_material_discretized("svel",xin,yin,zin,h,nlayer,zdisc_orig,zdisc_orig_layer,vs)
                    !call compute_homogenization_random(vd,vs)

                     !P-wave velocity 
                     call init_column_material_discretized("pvel",xin,yin,zin,h,nlayer,zdisc_orig,zdisc_orig_layer,vp)
                    !call compute_homogenization_random(vd,vp)
                     
                     !density
                     call init_column_material_discretized("dens",xin,yin,zin,h,nlayer,zdisc_orig,zdisc_orig_layer,rho)
                    !call compute_homogenization_random(vd,rho)

!
!-------------------->fill arrays for the solver

                     if (.not.LG_VISCO) then

                        rg_hexa_gll_rho   (m,l,k,ihexa) = rho      
                        rg_hexa_gll_rhovs2(m,l,k,ihexa) = rho*vs*vs
                        rg_hexa_gll_rhovp2(m,l,k,ihexa) = rho*vp*vp

                     else

                        !S-wave quality factor
                        call init_column_material_discretized("qfsv",xin,yin,zin,h,nlayer,zdisc_orig,zdisc_orig_layer,qs)
                       !call compute_homogenization_random(vd,qs)
                        
                        !P-wave quality factor
                        call init_column_material_discretized("qfpv",xin,yin,zin,h,nlayer,zdisc_orig,zdisc_orig_layer,qp)
                       !call compute_homogenization_random(vd,qp)
                        
                        !quality factor central frequency
                        call init_column_material_discretized("qffr",xin,yin,zin,h,nlayer,zdisc_orig,zdisc_orig_layer,qf)
                       !call compute_homogenization_random(vd,qf)

                        call init_medium_visco(vp,vs,rho,qp,qs,qf,wkqp,wkqs,upwm,uswm)
                    
                        rg_hexa_gll_rho   (m,l,k,ihexa) = rho 
                        rg_hexa_gll_rhovs2(m,l,k,ihexa) = uswm
                        rg_hexa_gll_rhovp2(m,l,k,ihexa) = upwm
                    
                        do n = ONE_IXP,IG_NRELAX

                           rg_hexa_gll_wkqs(n,m,l,k,ihexa) = wkqs(n)
                           rg_hexa_gll_wkqp(n,m,l,k,ihexa) = wkqp(n)

                        enddo
                    
                     endif
                  enddo
               enddo
            enddo

         enddo

         call KL_deallocate(tg_layer)

         deallocate(interfaces,interfaces_minmax)

         return

!*************************************************************************************************************************************
      end subroutine medium_homogenization
!*************************************************************************************************************************************


!
!
!>@brief subroutine to initialize the 1D soil column (number of layers, depth of interfaces, etc.) at a coordinate @f$x@f$,@f$y@f$
!>@param  xin              : @f$x@f$-coordinate where to initialize the 1D soil column
!>@param  yin              : @f$y@f$-coordinate where to initialize the 1D soil column
!>@param  h                : homogenization length
!>@param  ncols            : number of columns of the array interfaces
!>@param  nrows            : number of rows of the array interfaces
!>@param  xllcorner        : @f$x@f$-coordinate of the lower left corner of the cells in array interfaces
!>@param  yllcorner        : @f$y@f$-coordinate of the lower left corner of the cells in array interfaces
!>@param  cellsize         : size of a cell in directions @f$x@f$ and @f$y@f$
!>@param  interfaces       : array containing the depth of all the interfaces
!>@return nlayer           : number of layer of the 1D soil column (bounded between one and mod_global_variables::ig_nlayer)
!>@return zdisc_orig       : depth of the interfaces where a discontinuity of mechanical property can occur
!>@return zdisc_orig_layer : layer number in between the @f$z@f$-discontinuities
!>@return is_layer_exist   : array of size mod_global_variables::ig_nlayer that returns .true. if the layer exists
!****************************************************************************************************************************************************
      subroutine init_column_discontinuity(xin,yin,h,ncols,nrows,xllcorner,yllcorner,cellsize,interfaces,nlayer,zdisc_orig,zdisc_orig_layer,is_layer_exist)
!****************************************************************************************************************************************************

         use mod_global_variables, only :&
                                         ig_nlayer&
                                        ,ig_ninterface&
                                        ,rg_mesh_zmin
         implicit none

         integer(kind=IXP), intent( in)                                                    :: ncols
         integer(kind=IXP), intent( in)                                                    :: nrows
         real   (kind=RXP), intent( in)                                                    :: cellsize
         integer(kind=IXP), intent(out)                                                    :: nlayer
         real   (kind=RXP), intent( in)                                                    :: xllcorner
         real   (kind=RXP), intent( in)                                                    :: yllcorner
                                                                                           
         real   (kind=RXP), intent( in)                                                    :: xin
         real   (kind=RXP), intent( in)                                                    :: yin
         real   (kind=RXP), intent( in)                                                    :: h
         real   (kind=RXP), intent( in)             , dimension(nrows,ncols,ig_ninterface) :: interfaces
                                                                                           
         real   (kind=RXP), intent(out), allocatable, dimension(:)                         :: zdisc_orig
         logical(kind=IXP), intent(out)             , dimension(ig_nlayer)                 :: is_layer_exist
         integer(kind=IXP), intent(out), allocatable, dimension(:)                         :: zdisc_orig_layer
                                                                                           
         real   (kind=RXP)                          , dimension(ig_ninterface)             :: interfaces_current
         real   (kind=RXP)                                                                 :: xi
         real   (kind=RXP)                                                                 :: eta
         real   (kind=RXP)                                                                 :: xmin
         real   (kind=RXP)                                                                 :: xmax
         real   (kind=RXP)                                                                 :: ymin
         real   (kind=RXP)                                                                 :: ymax
                                                                                           
         integer(kind=IXP)                                                                 :: iin
         integer(kind=IXP)                                                                 :: jin
         integer(kind=IXP)                                                                 :: i
         integer(kind=IXP)                                                                 :: iinterface
                                                                                           

!
!------->first deallocate dynamic array if needed
         if ( allocated(zdisc_orig) ) deallocate(zdisc_orig)

!       
!------->find position of (xin,yin) in the regular surface grid
         iin  = ONE_IXP + floor((xin-xllcorner)/cellsize)
         iin  = max(iin,ONE_IXP)
         iin  = min(iin,ncols-ONE_IXP)

         jin  = ONE_IXP + floor((yin-yllcorner)/cellsize)
         jin  = max(jin,ONE_IXP)
         jin  = min(jin,nrows-ONE_IXP)

         xmin = xllcorner + real(iin-ONE_IXP,kind=RXP)*cellsize
         xmax = xllcorner + real(iin        ,kind=RXP)*cellsize
                                                      
         ymin = yllcorner + real(jin-ONE_IXP,kind=RXP)*cellsize
         ymax = yllcorner + real(jin        ,kind=RXP)*cellsize

!       
!------->compute approximate local coordinates of xin,yin within the gridcell
         xi  = (xin-xmin)/(xmax-xmin)
         eta = (yin-ymin)/(ymax-ymin)

         if (xi  < -ONE_RXP) xi =-ONE_RXP
         if (xi  >  ONE_RXP) xi = ONE_RXP
         if (eta < -ONE_RXP) eta=-ONE_RXP
         if (eta >  ONE_RXP) eta= ONE_RXP


!
!------->compute elevation of all interfaces at xin,yin
         do i = ONE_IXP,ig_ninterface

            interfaces_current(i) = (ONE_RXP-xi)*(ONE_RXP-eta)*interfaces(jin        ,iin        ,i) + &
                                    (        xi)*(ONE_RXP-eta)*interfaces(jin        ,iin+ONE_IXP,i) + &
                                    (ONE_RXP-xi)*(        eta)*interfaces(jin+ONE_IXP,iin        ,i) + &
                                    (        xi)*(        eta)*interfaces(jin+ONE_IXP,iin+ONE_IXP,i)

         enddo


!
!------->count the number of layers at xin,yin by checking if layers are thick enough wrt the averaging length 'h'
         nlayer = ZERO_IXP

         do i = ONE_IXP,ig_ninterface-ONE_IXP

            if ( abs(interfaces_current(i+ONE_IXP) - interfaces_current(i)) > h/10.0_RXP ) then

               is_layer_exist(i) = .true.

               nlayer = nlayer + ONE_IXP

            else

               is_layer_exist(i) = .false.

            endif

         enddo

!
!------->last layer always exist to define the bedrock
         nlayer = nlayer + ONE_IXP

         is_layer_exist(ig_nlayer) = .true.


!
!
!*******************************************************************************************************************
!------->init the interfaces
!*******************************************************************************************************************

         allocate(zdisc_orig(ZERO_IXP:nlayer))

         allocate(zdisc_orig_layer(nlayer))

!
!------->free surface interface is always defined
         iinterface = ZERO_IXP

         zdisc_orig(iinterface) = interfaces_current(ONE_IXP)

!
!------->set interface elevation if layer exists
         do i = ONE_IXP,ig_nlayer-ONE_IXP

            if (is_layer_exist(i)) then

               iinterface = iinterface + ONE_IXP

               zdisc_orig(iinterface) = interfaces_current(i+ONE_IXP)

               zdisc_orig_layer(iinterface) = i

            endif

         enddo

!
!------->set last interface which is the bottom of the mesh
         iinterface = iinterface + ONE_IXP

         zdisc_orig(iinterface) = rg_mesh_zmin

         zdisc_orig_layer(iinterface) = ig_nlayer
         
         return

!*******************************************************************************************************************
      end subroutine init_column_discontinuity
!*******************************************************************************************************************


!
!
!>@brief subroutine to initialize the materials' mechanical properties of layers
!>@param  elastic_mat    : mechanical properties of layers
!>@param  is_layer_exist : array of size mod_global_variables::ig_nlayer that returns .true. if the layer exists
!>@param  n              : number of existing layer of the 1D soil column (bounded between one and mod_global_variables::ig_nlayer)
!>@return top            : mechanical properties at the top    of existing layers
!>@return bot            : mechanical properties at the bottom of existing layers
!*******************************************************************************************************************
      subroutine init_column_material(elastic_mat,is_layer_exist,n,top,bot)
!*******************************************************************************************************************

         use mod_global_variables, only :&
                                         lg_layer_gradient&
                                        ,ig_nmaterial&
                                        ,ig_nlayer

         
         implicit none

         real   (kind=RXP), intent( in)             , dimension(ig_nmaterial)              :: elastic_mat
         logical(kind=IXP), intent( in)             , dimension(ig_nlayer)                 :: is_layer_exist
         integer(kind=IXP), intent( in)                                                    :: n
         real   (kind=RXP), intent(out), allocatable, dimension(:)                         :: top
         real   (kind=RXP), intent(out), allocatable, dimension(:)                         :: bot
                                                                                           
         integer(kind=IXP)                                                                 :: i
         integer(kind=IXP)                                                                 :: ilayer
         integer(kind=IXP)                                                                 :: imat
         integer(kind=IXP)                                                                 :: ios
                                                                                           

!
!------->first deallocate dynamic arrays if needed
         if (allocated(top)) deallocate(top)
         if (allocated(bot)) deallocate(bot)

         ios = init_array_real(top,n,"module_homogenization:init_column_material:top")
         ios = init_array_real(bot,n,"module_homogenization:init_column_material:bot")
        
!
!
!*******************************************************************************************************************
!------->init material of layers
!*******************************************************************************************************************
         ilayer = ZERO_IXP

         if (lg_layer_gradient) then

            do i = ONE_IXP,ig_nlayer

               imat = (i - ONE_IXP)*TWO_IXP + ONE_IXP
            
               if (is_layer_exist(i)) then
            
                  ilayer = ilayer + ONE_IXP
            
                  top(ilayer)  = elastic_mat(imat        )
                  bot(ilayer)  = elastic_mat(imat+ONE_IXP)
                  
               endif 
            
            enddo

         else

            do i = ONE_IXP,ig_nlayer

               imat = i
            
               if (is_layer_exist(i)) then
            
                  ilayer = ilayer + ONE_IXP
            
                  top(ilayer)  = elastic_mat(imat)
                  bot(ilayer)  = elastic_mat(imat)
                  
               endif 
            
            enddo

         endif

         return

!*******************************************************************************************************************
      end subroutine init_column_material
!*******************************************************************************************************************


!
!
!>@brief subroutine to initialize the materials' random mechanical properties over a 1D soil column centered at zin with size of the homogeneization length
!>@param  x   : @f$x@f$-coordinate of the GLL point where material is random  
!>@param  y   : @f$y@f$-coordinate of the GLL point where material is random  
!>@param  z   : @f$z@f$-coordinate of the GLL point where material is random  
!>@param  h   : homogeneization length
!>@param  zd  : @f$z@f$-coordinate of the discontinuities of the material
!>@param  zdl : layer number in between the elevation in zd
!>@param  vin : input value of the material property to use
!>@param  KLx : type containing KL expansion arrays for @f$x@f$-coordinate
!>@param  KLy : type containing KL expansion arrays for @f$y@f$-coordinate
!>@param  KLz : type containing KL expansion arrays for @f$z@f$-coordinate
!>@return v   : discretized value of the properties to be homogeneized
!*******************************************************************************************************************
      subroutine init_column_material_discretized(p,x,y,zin,h,n,zd,zdl,vout)
!*******************************************************************************************************************

         use mod_global_variables, only : type_random_material_KL&
                                        , tg_layer

         use mod_random_field, only : KL_eval3D

         implicit none

         character(len=4_IXP)                                       , intent(in)    :: p
         real   (kind=RXP)                                          , intent(in)    :: x
         real   (kind=RXP)                                          , intent(in)    :: y
         real   (kind=RXP)                                          , intent(in)    :: zin
         real   (kind=RXP)                                          , intent(in)    :: h
         integer(kind=IXP)                                          , intent(in)    :: n
         real   (kind=RXP)            , dimension(0:n)              , intent(in)    :: zd
         integer(kind=IXP)            , dimension(n)                , intent(in)    :: zdl
         real   (kind=RXP)                                          , intent(out)   :: vout

         real   (kind=RXP)            , dimension(:)   , allocatable                :: v
         real   (kind=RXP), dimension(TWO_IXP*n+ONE_IXP)                            :: zdp
         real   (kind=RXP), allocatable, dimension(:)                               :: vin
         real   (kind=RXP), dimension(TWO_IXP*n)                                    :: length_ratio
         real   (kind=RXP)                                                          :: vz0
         real   (kind=RXP)                                                          :: z
         real   (kind=RXP)                                                          :: x_norm
         real   (kind=RXP)                                                          :: y_norm
         real   (kind=RXP)                                                          :: z_norm
         real   (kind=RXP)                                                          :: zp
         real   (kind=RXP)                                                          :: zplus
         real   (kind=RXP)                                                          :: zmoins
         real   (kind=RXP)                                                          :: dz
         real   (kind=RXP)                                                          :: m
         real   (kind=RXP)                                                          :: e
         real   (kind=RXP)                                                          :: zmax
         real   (kind=RXP)                                                          :: zmin
         real   (kind=RXP)                                                          :: vtop
         real   (kind=RXP)                                                          :: vbot
         real   (kind=RXP)                                                          :: a
         real   (kind=RXP)                                                          :: b
         real   (kind=RXP)                                                          :: sum_ratio
         real   (kind=RXP)                                                          :: sum_v

         integer(kind=IXP), dimension(TWO_IXP*n)                                    :: zdlp
         integer(kind=IXP), parameter                                               :: NP = 21_IXP
         integer(kind=IXP)                                                          :: first_layer
         integer(kind=IXP)                                                          :: current_layer
         integer(kind=IXP)                                                          :: nlayer_for_h
         integer(kind=IXP)                                                          :: zdlp_layer
         integer(kind=IXP)                                                          :: imat
         integer(kind=IXP)                                                          :: nmat
         integer(kind=IXP)                                                          :: i
         integer(kind=IXP)                                                          :: j
         integer(kind=IXP)                                                          :: ilay
         integer(kind=IXP)                                                          :: ilayertop
         integer(kind=IXP)                                                          :: ilayerbot
         integer(kind=IXP)                                                          :: ios

         character(len=CIL)                                                         :: info

         length_ratio(:) = ZERO_RXP
   
         sum_ratio = ZERO_RXP
         sum_v     = ZERO_RXP

         ios = init_array_real(v,NP,"mod_homogenization:init_column_material_discretized:v")

         dz = h/real(NP-ONE_IXP,kind=RXP)

!
!------->correct zin if necessary. If the DEM and the mesh do not have the same resolution, z_mesh can be higher than z_dem and the homogenization fails

         if (zin > zd(ZERO_IXP)) then

            z = zd(ZERO_IXP)

         else

            z = zin

         endif

         zplus  = z + h/TWO_RXP
         zmoins = z - h/TWO_RXP

! 
!------->make domain periodic wrt the free surface

         do j = ONE_IXP,n
   
            i      = n-j+ONE_IXP
            zdp(j) = TWO_RXP*zd(ZERO_IXP)-zd(i)
   
         enddo
   
         do j = n+ONE_IXP,TWO_IXP*n+ONE_IXP
   
            i      = j-n-ONE_IXP
            zdp(j) = zd(i)
   
         enddo

         do i = ONE_IXP,n

            zdlp(i)   = zdl(n-i+ONE_IXP)

            zdlp(n+i) = zdl(i)

         enddo

!
!------->get the number of the first layer

         first_layer = zdl(ONE_IXP)

!
!------->compute the ratio of the length of the layer over the total length of homogeneization 'h'

         call find_top_layer(zplus ,zdp,n,ilayertop)
         call find_bot_layer(zmoins,zdp,n,ilayerbot)

         nlayer_for_h = ZERO_IXP

         do i = ilayertop,ilayerbot

            nlayer_for_h = nlayer_for_h + ONE_IXP

         enddo

         if (nlayer_for_h == ONE_IXP) then

            length_ratio(ilayertop) = ONE_RXP

         else

            length_ratio(ilayertop) = (zplus-zdp(ilayertop+ONE_IXP))/h
            length_ratio(ilayerbot) = (zdp(ilayerbot)-zmoins)/h
          
            do i = ilayertop+ONE_IXP,ilayerbot-ONE_IXP
          
               length_ratio(i) = (zdp(i) - zdp(i+ONE_IXP))/h
          
            enddo

         endif
      
!
!------->set value at the free surface (used for periodic condition wrt the free surface)

         !init for random medium
         if (tg_layer(first_layer)%structure_type == 3_IXP) then

            x_norm = (x             - tg_layer(first_layer)%xmin )/tg_layer(first_layer)%xl
            y_norm = (y             - tg_layer(first_layer)%ymin )/tg_layer(first_layer)%yl
            z_norm = (zd(ZERO_IXP)  - tg_layer(first_layer)%zminh)/tg_layer(first_layer)%zl !z-coordinate of the free surface

            if (x_norm < ZERO_RXP) x_norm = ZERO_RXP
            if (x_norm >  ONE_RXP) x_norm =  ONE_RXP
            if (y_norm < ZERO_RXP) y_norm = ZERO_RXP
            if (y_norm >  ONE_RXP) y_norm =  ONE_RXP
            if (z_norm < ZERO_RXP) z_norm = ZERO_RXP
            if (z_norm >  ONE_RXP) z_norm =  ONE_RXP

         endif

         if (p == "svel") then

            if (tg_layer(first_layer)%structure_type == 3_IXP) then

               m = tg_layer(first_layer)%elastic(ONE_IXP)%svel
               e = tg_layer(first_layer)%random(ONE_IXP)%e

               call KL_eval3D(m,e,x_norm,y_norm,z_norm,tg_layer(first_layer)%KLx,tg_layer(first_layer)%KLy,tg_layer(first_layer)%KLz,tg_layer(first_layer)%random(ONE_IXP)%rd,tg_layer(first_layer)%modes_ind,vz0)

            else

               vz0 = tg_layer(first_layer)%elastic(ONE_IXP)%svel

            endif

         elseif(p == "pvel") then

            if (tg_layer(first_layer)%structure_type == 3_IXP) then

               m = tg_layer(first_layer)%elastic(ONE_IXP)%pvel
               e = tg_layer(first_layer)%random(ONE_IXP)%e

               call KL_eval3D(m,e,x_norm,y_norm,z_norm,tg_layer(first_layer)%KLx,tg_layer(first_layer)%KLy,tg_layer(first_layer)%KLz,tg_layer(first_layer)%random(ONE_IXP)%rd,tg_layer(first_layer)%modes_ind,vz0)

            else

               vz0 = tg_layer(first_layer)%elastic(ONE_IXP)%pvel

            endif


         elseif(p == "dens") then

            vz0 = tg_layer(first_layer)%elastic(ONE_IXP)%dens

         elseif(p == "qfsv") then

            vz0 = tg_layer(first_layer)%anelastic(ONE_IXP)%qs

         elseif(p == "qfpv") then

            vz0 = tg_layer(first_layer)%anelastic(ONE_IXP)%qp

         elseif(p == "qffr") then

            vz0 = tg_layer(first_layer)%anelastic(ONE_IXP)%freq

         endif

!
!
!**********************************************************************************************************************************
!------->loop over the discrete point from (z - h/2) to (z + h/2)
!**********************************************************************************************************************************
         do i = ONE_IXP,NP

!
!---------->elevation where to compute the random property
            zp = z - h/2.0_RXP + real(i-ONE_IXP,kind=RXP)*dz

!
!---------->get the layer which contains zp

            if (zp > zdp(ONE_IXP)) then

               current_layer = zdlp(ONE_IXP)
               zdlp_layer    = ONE_IXP

            elseif (zp < zdp(TWO_IXP*n+ONE_IXP)) then

               current_layer = zdlp(TWO_IXP*n)
               zdlp_layer    = TWO_IXP*n

            else

               do ilay = ONE_IXP,n*TWO_IXP
               
                  if ( (zp <= zdp(ilay)) .and. (zp >= zdp(ilay+ONE_IXP)) ) then
               
                     current_layer = zdlp(ilay)
                     zdlp_layer    = ilay
               
                  endif
               
               enddo

            endif

!
!---------->transfer layer material properties of the current layer to local array. TODO FLO!!! VERY UGLY: THIS HAS TO BE CHANGE WITH POINTER TO DERIVED DATA-TYPE DIRECTLY FROM THE CALL OF THE SUBROUTINE

            nmat = tg_layer(current_layer)%nmat

            ios = init_array_real(vin,nmat,"mod_homogenization:init_column_material_discretized:vin")

            if (p == "svel") then

               do imat = ONE_IXP,nmat 

                  vin(imat) = tg_layer(current_layer)%elastic(imat)%svel

               enddo

            elseif(p == "pvel") then

               do imat = ONE_IXP,nmat 

                  vin(imat) = tg_layer(current_layer)%elastic(imat)%pvel

               enddo

            elseif(p == "dens") then

               do imat = ONE_IXP,nmat 

                  vin(imat) = tg_layer(current_layer)%elastic(imat)%dens

               enddo

            elseif(p == "qfsv") then

               do imat = ONE_IXP,nmat 

                  vin(imat) = tg_layer(current_layer)%anelastic(imat)%qs

               enddo

            elseif(p == "qfpv") then

               do imat = ONE_IXP,nmat 

                  vin(imat) = tg_layer(current_layer)%anelastic(imat)%qp

               enddo

            elseif(p == "qffr") then

               do imat = ONE_IXP,nmat 

                  vin(imat) = tg_layer(current_layer)%anelastic(imat)%freq

               enddo

            endif

!
!---------->select structure type of the layer (constant,linear gradient, random, etc.)

!---------->constant
            if (tg_layer(current_layer)%structure_type == ONE_IXP) then

               if (zdlp_layer > n) then

                  v(i) = vin(ONE_IXP)

               else!periodic

                  v(i) = TWO_RXP*vz0 - vin(ONE_IXP)

               endif

!---------->linear gradient
            elseif (tg_layer(current_layer)%structure_type == TWO_IXP) then

               if (zdlp_layer > n) then

                  vtop = vin(ONE_IXP)
                  vbot = vin(TWO_IXP)

                 !zmax = tg_layer(current_layer)%zmax
                  zmax = zd(ZERO_IXP)
                  zmin = tg_layer(current_layer)%zmin

               else!periodic

                  vtop =  TWO_RXP*vz0 - vin(TWO_IXP)
                  vbot =  TWO_RXP*vz0 - vin(ONE_IXP)

                  zmax = TWO_IXP*zd(ZERO_IXP) - tg_layer(current_layer)%zmin
                 !zmin = TWO_IXP*zd(ZERO_IXP) - tg_layer(current_layer)%zmax
                  zmin =         zd(ZERO_IXP)

               endif

               a    = (vtop-vbot)/(zmax-zmin)
               b    = vtop - a*zmax

               v(i) = a*zp + b
 
!---------->random
            elseif (tg_layer(current_layer)%structure_type == THREE_IXP) then

!------------->normalize coordinate before KL expansion
               x_norm = (x  - tg_layer(current_layer)%xmin)/tg_layer(current_layer)%xl
               y_norm = (y  - tg_layer(current_layer)%ymin)/tg_layer(current_layer)%yl

!
!------------->periodic medium wrt the free surface
               if (zdlp_layer > n) then!inside real medium

                  z_norm = (zp - tg_layer(current_layer)%zminh)/tg_layer(current_layer)%zl

                  !not random (all except svel and pvel)
                  v(i) = vin(ONE_IXP)

               else!inside periodic medium

                  z_norm = ( (TWO_RXP*zd(ZERO_IXP)-zp) - tg_layer(current_layer)%zminh)/tg_layer(current_layer)%zl

                  !not random (all except svel and pvel)
                  v(i) = TWO_RXP*vz0 - vin(ONE_IXP)

               endif
   
               if (x_norm < ZERO_RXP) x_norm = ZERO_RXP
               if (x_norm >  ONE_RXP) x_norm =  ONE_RXP
               if (y_norm < ZERO_RXP) y_norm = ZERO_RXP
               if (y_norm >  ONE_RXP) y_norm =  ONE_RXP
               if (z_norm < ZERO_RXP) z_norm = ZERO_RXP
               if (z_norm >  ONE_RXP) z_norm =  ONE_RXP

               !random only for svel and pvel
               if ( (p == "svel") .or. (p == "pvel") ) then

                  m = vin(ONE_IXP)
                
                  e = tg_layer(current_layer)%random(ONE_IXP)%e
                
                  call KL_eval3D(m,e,x_norm,y_norm,z_norm,tg_layer(current_layer)%KLx,tg_layer(current_layer)%KLy,tg_layer(current_layer)%KLz,tg_layer(current_layer)%random(ONE_IXP)%rd,tg_layer(current_layer)%modes_ind,v(i))

                  if (zdlp_layer <= n) then!inside periodic medium

                     v(i) = TWO_RXP*vz0 - v(i)

                  endif

               endif

!---------->user defined
            else

                write(info,'(a)') "error in subroutine module_homogenization:init_column_material_discretized:no user defined medium"
                call error_stop(info)

            endif

            sum_v     = sum_v     + v(i)*length_ratio(zdlp_layer)
            sum_ratio = sum_ratio +      length_ratio(zdlp_layer)

         enddo

         vout = sum_v/sum_ratio

         return

!*******************************************************************************************************************
      end subroutine init_column_material_discretized
!*******************************************************************************************************************


!
!
!>@brief subroutine to compute the homogenized property at a depth coordinate @f$z@f$
!>@param  zin            : @f$z@f$-coordinate where to compute the homogenized property
!>@param  nlayer         : number of layer of the 1D soil column (bounded between one and mod_global_variables::ig_nlayer)
!>@param  zdisc_orig     : depth of the interfaces where a discontinuity of mechanical property can occur
!>@param  top            : mechanical properties at the top    of existing layers
!>@param  bot            : mechanical properties at the bottom of existing layers
!>@param  h              : homogenization length
!>@param  is_arithmetic  : method used to compute the homogenized property
!>@return vh             : homogenized property
!*******************************************************************************************************************
      subroutine compute_homogenization(zin,nlayer,zdisc_orig,top,bot,h,vh,is_arithmetic)
!*******************************************************************************************************************

         implicit none

         real   (kind=RXP)                            , intent(in ) :: zin
         integer(kind=IXP)                            , intent(in ) :: nlayer
         real   (kind=RXP), dimension(ZERO_IXP:nlayer), intent(in ) :: zdisc_orig
         real   (kind=RXP), dimension(         nlayer), intent(in ) :: top
         real   (kind=RXP), dimension(         nlayer), intent(in ) :: bot
         real   (kind=RXP)                            , intent(in ) :: h

         real   (kind=RXP)                            , intent(out) :: vh
         logical(kind=IXP)                            , intent( in) :: is_arithmetic

         integer(kind=IXP)                                          :: i
         integer(kind=IXP)                                          :: j
         integer(kind=IXP)                                          :: ibotlayer
         integer(kind=IXP)                                          :: itoplayer

         real   (kind=RXP)                                          :: zmoins
         real   (kind=RXP)                                          :: zplus
         real   (kind=RXP)                                          :: q
         real   (kind=RXP)                                          :: qout

         real   (kind=RXP), dimension(TWO_IXP*nlayer+ONE_IXP)       :: zdisc
         real   (kind=RXP), dimension(TWO_IXP*nlayer)               :: alpha
         real   (kind=RXP), dimension(TWO_IXP*nlayer)               :: beta


!
!------->define the size of the averaging length
         zmoins = zin - h/TWO_RXP
         zplus  = zin + h/TWO_RXP

! 
!------->make domain periodic wrt the free surface
         do j = ONE_IXP,nlayer
   
            i        = nlayer-j+ONE_IXP
            zdisc(j) = TWO_RXP*zdisc_orig(ZERO_IXP)-zdisc_orig(i)
   
         enddo
   
         do j = nlayer+ONE_IXP,TWO_IXP*nlayer+ONE_IXP
   
            i        = j-nlayer-ONE_IXP
            zdisc(j) = zdisc_orig(i)
   
         enddo

!
!------->define gradient parameters in each layer
         do j = nlayer+ONE_IXP,TWO_IXP*nlayer

            i = j-nlayer

            alpha(j) = ( top(i)-bot(i) ) / ( zdisc_orig(i-ONE_IXP)-zdisc_orig(i) )
            beta (j) = ( zdisc_orig(i-ONE_IXP)*bot(i)-zdisc_orig(i)*top(i) ) / ( zdisc_orig(i-ONE_IXP)-zdisc_orig(i) )

         enddo

         do j = ONE_IXP,nlayer

            i = TWO_IXP*nlayer-j+ONE_IXP

            alpha(j) =        -alpha(i)
            beta (j) = TWO_RXP*alpha(i)*zdisc_orig(ZERO_IXP) + beta(i)

         enddo
   
         call find_bot_layer(zmoins,zdisc,nlayer,ibotlayer)
         call find_top_layer(zplus ,zdisc,nlayer,itoplayer)

! 
!------->compute homogenized value 'vh' through 1D averaging (use slowness for velocity by passing is_arithmetic = .false.)

         q = ZERO_RXP

         do j = ibotlayer,itoplayer,-ONE_IXP

            call integrate_within_layer(zdisc(j+ONE_IXP),zdisc(j),h,alpha(j),beta(j),qout,is_arithmetic)
            q = q + qout

         enddo

         call integrate_within_layer(zdisc(ibotlayer+ONE_IXP),zmoins,h,alpha(ibotlayer),beta(ibotlayer),qout,is_arithmetic)
         q = q - qout

         call integrate_within_layer(zplus,zdisc(itoplayer),h,alpha(itoplayer),beta(itoplayer),qout,is_arithmetic)
         q = q - qout

         if (is_arithmetic) then

            vh = q

         else

            vh = ONE_RXP/q

         endif

         return

!*******************************************************************************************************************
        end subroutine compute_homogenization
!*******************************************************************************************************************


!
!
!>@brief subroutine to compute the homogenized property at a depth coordinate @f$z@f$
!>@param  x : array containing values to be homogenized
!>@return v : homogenized value
!*******************************************************************************************************************
      subroutine compute_homogenization_random(x,v)
!*******************************************************************************************************************

         use mod_signal_processing

         implicit none

         real   (kind=RXP), dimension(:), intent(in ) :: x
         real   (kind=RXP)              , intent(out) :: v

         real   (kind=RXP), dimension(:), allocatable :: h
         integer(kind=IXP)                            :: nh

         nh = size(x) !must be odd: this is the case because NP = 21 in subroutine init_column_material_discretized

         call hanning(nh,h)

         v = wam(x,h)

         return

!*******************************************************************************************************************
        end subroutine compute_homogenization_random
!*******************************************************************************************************************


!
!
!>@brief subroutine to get the top layer used for homogenization
!>@param  zin    : @f$z@f$-coordinate
!>@param  zdisc  : depth of the interfaces where a discontinuity of mechanical property can occur. This array contains values symetric wrt the free surface
!>@param  nlayer : number of layer of the 1D soil column (bounded between one and mod_global_variables::ig_nlayer)
!>@return ilayer : layer containing depth coordinate @f$z@f$
!*******************************************************************************************************************
      subroutine find_top_layer(z,zdisc,nlayer,ilayer)
!*******************************************************************************************************************

         implicit none
         
         real   (kind=RXP), intent(in )                                    :: z
         integer(kind=IXP), intent(in )                                    :: nlayer
         real   (kind=RXP), intent(in ), dimension(TWO_IXP*nlayer+ONE_IXP) :: zdisc
         integer(kind=IXP), intent(out)                                    :: ilayer
         integer(kind=IXP)                                                 :: i
         character(len=CIL)                                                :: info
         
         ilayer = -999_IXP

         do i = ONE_IXP,TWO_IXP*nlayer

            if ( (zdisc(i+ONE_IXP) < z) .and. (z <= zdisc(i)) ) then

               ilayer=i
               return

            endif

         enddo

         if (ilayer == -999_IXP) then

            write(info,'(a)') "error in subroutine module_homogenization:find_top_layer"
            call error_stop(info)

         endif

         return

!*******************************************************************************************************************
      end subroutine find_top_layer
!*******************************************************************************************************************


!
!
!>@brief subroutine to get the bottom layer used for homogenization
!>@param  zin    : @f$z@f$-coordinate
!>@param  zdisc  : depth of the interfaces where a discontinuity of mechanical property can occur. This array contains values symetric wrt the free surface
!>@param  nlayer : number of layer of the 1D soil column (bounded between one and mod_global_variables::ig_nlayer)
!>@return ilayer : layer containing depth coordinate @f$z@f$
!*******************************************************************************************************************
      subroutine find_bot_layer(z,zdisc,nlayer,ilayer)
!*******************************************************************************************************************

         implicit none

         real   (kind=RXP), intent(in )                                    :: z
         integer(kind=IXP), intent(in )                                    :: nlayer
         real   (kind=RXP), intent(in ), dimension(TWO_IXP*nlayer+ONE_IXP) :: zdisc
         integer(kind=IXP), intent(out)                                    :: ilayer
         integer(kind=IXP)                                                 :: i
         character(len=CIL)                                                :: info
         
         ilayer = -999_IXP

!
!------->when zmoins is smaller than zmin of the mesh, return directly the last layer of the column (= TWO_IXP*nlayer)
         if (z < zdisc(TWO_IXP*nlayer+ONE_IXP)) then

            ilayer = TWO_IXP*nlayer
            return

         endif

         do i = ONE_IXP,TWO_IXP*nlayer

            if ( (zdisc(i+ONE_IXP) <= z) .and. (z < zdisc(i)) ) then

               ilayer = i
               return

            endif

         enddo

         if (ilayer == -999_IXP) then

            write(info,'(a)') "error in subroutine module_homogenization:find_bot_layer"
            call error_stop(info)

         endif

         return

!*******************************************************************************************************************
      end subroutine find_bot_layer
!*******************************************************************************************************************


!
!
!>@brief subroutine to compute integral between coordinates @f$z1@f$ and @f$z2@f$
!>@param  z1         : @f$z@f$-coordinate of the first  bound of the integral
!>@param  z2         : @f$z@f$-coordinate of the second bound of the integral
!>@param  h          : homogenization length
!>@param  alpha      : slope    of the gradient
!>@param  beta       : constant of the gradient
!>@return qout       : result of the integral
!>@param  arithmetic : method used to integrate
!*******************************************************************************************************************
      subroutine integrate_within_layer(z1,z2,h,alpha,beta,qout,arithmetic)
!*******************************************************************************************************************

         implicit none
       
         real(kind=RXP), intent(in   ) :: z1,z2,h,alpha,beta
         real(kind=RXP), intent(inout) :: qout
         logical(kind=IXP)         , intent(in   ) :: arithmetic
         real(kind=RXP)                :: q1,q2
       
         if (arithmetic) then
       
            qout = alpha*(z2*z2-z1*z1)/TWO_RXP + beta*(z2-z1)
       
            qout = qout/h
       
         else
       
            q1 = alpha*z1+beta
            q2 = alpha*z2+beta

            if ( abs(q1-q2) <= EPSILON_MACHINE_RXP ) then
       
               qout = ((z2-z1)/h)/beta
       
            else
       
               qout = log(q2/q1)/(h*alpha)
       
            endif

         endif 

         return

!*******************************************************************************************************************
      end subroutine integrate_within_layer
!*******************************************************************************************************************


!
!
!>@brief subroutine to compute the unrelaxed (s,p)-wave modulii and the weight coefficients of viscoelastic materials
!>@param  vp   : p-wave velocity
!>@param  vs   : s-wave velocity
!>@param  rho  : density
!>@param  qp   : p-wave quality factor @f$ Q_p @f$
!>@param  qs   : s-wave quality factor @f$ Q_s @f$
!>@param  qf   : reference frequency @f$ f_r @f$ for which unrelaxed s-wave and p-wave modulii are defined
!>@return wkqp : weight coefficients @f$ w^{P}_{k} @f$ associated to @f$ Q_p @f$ 
!>@return wkqs : weight coefficients @f$ w^{S}_{k} @f$ associated to @f$ Q_s @f$
!>@return upwm : unrelaxed p-wave modulus @f$ M^{P}_{u} @f$
!>@return uswm : unrelaxed s-wave modulus @f$ M^{S}_{u} @f$
!*******************************************************************************************************************
      subroutine init_medium_visco(vp,vs,rho,qp,qs,qf,wkqp,wkqs,upwm,uswm)
!*******************************************************************************************************************

        use mod_global_variables, only :&
                                        IG_NRELAX&
                                       ,PI_RXP&
                                       ,RG_RELAX_COEFF

        implicit none

        real   (kind=RXP), intent( in)                       :: vp
        real   (kind=RXP), intent( in)                       :: vs
        real   (kind=RXP), intent( in)                       :: rho
        real   (kind=RXP), intent( in)                       :: qp
        real   (kind=RXP), intent( in)                       :: qs
        real   (kind=RXP), intent( in)                       :: qf
        real   (kind=RXP), intent(out), dimension(IG_NRELAX) :: wkqp
        real   (kind=RXP), intent(out), dimension(IG_NRELAX) :: wkqs
        real   (kind=RXP), intent(out)                       :: upwm
        real   (kind=RXP), intent(out)                       :: uswm

        complex(kind=RXP)                                    :: ctmpqs
        complex(kind=RXP)                                    :: ctmpqp
        real   (kind=RXP)                                    :: dtmpqs
        real   (kind=RXP)                                    :: dtmpqp
        integer(kind=IXP)                                    :: k


        dtmpqs = (3.071_RXP+1.433_RXP*qs**(-1.158_RXP)*log(qs/5.0_RXP))/(1.0_RXP+0.415_RXP*qs)
        dtmpqp = (3.071_RXP+1.433_RXP*qp**(-1.158_RXP)*log(qp/5.0_RXP))/(1.0_RXP+0.415_RXP*qp)

        do k = ONE_IXP,IG_NRELAX

           wkqs(k) = dtmpqs*(dtmpqs*real(RG_RELAX_COEFF(k,TWO_IXP),kind=RXP)+real(RG_RELAX_COEFF(k,THREE_IXP),kind=RXP))
           wkqp(k) = dtmpqp*(dtmpqp*real(RG_RELAX_COEFF(k,TWO_IXP),kind=RXP)+real(RG_RELAX_COEFF(k,THREE_IXP),kind=RXP))

        enddo

        ctmpqs = cmplx(0.0_RXP,0.0_RXP)
        ctmpqp = cmplx(0.0_RXP,0.0_RXP)

        do k = ONE_IXP,IG_NRELAX

           ctmpqs = ctmpqs + cmplx(wkqs(k),0.0_RXP)/(1.0_RXP + cmplx(0.0_RXP,1.0_RXP)*2.0_RXP*PI_RXP*qf*real(RG_RELAX_COEFF(k,ONE_IXP),kind=RXP))
           ctmpqp = ctmpqp + cmplx(wkqp(k),0.0_RXP)/(1.0_RXP + cmplx(0.0_RXP,1.0_RXP)*2.0_RXP*PI_RXP*qf*real(RG_RELAX_COEFF(k,ONE_IXP),kind=RXP))

        enddo

        uswm = (rho*vs*vs)/abs(1.0_RXP-ctmpqs)
        upwm = (rho*vp*vp)/abs(1.0_RXP-ctmpqp)

        return

!*******************************************************************************************************************
      end subroutine init_medium_visco
!*******************************************************************************************************************


!
!
!>@brief subroutine to read interfaces contains in files *.if*
!>@return interfaces     : array containing the depth of all the interfaces
!>@return ncols          : number of columns of the array interfaces
!>@return nrows          : number of rows of the array interfaces
!>@return xllcorner      : @f$x@f$-coordinate of the lower left corner of the cells in array interfaces
!>@return yllcorner      : @f$y@f$-coordinate of the lower left corner of the cells in array interfaces
!>@return cellsize       : size of a cell in directions @f$x@f$ and @f$y@f$
!*************************************************************************************************************************************
      subroutine read_interfaces(interfaces,ncols,nrows,xllcorner,yllcorner,cellsize)
!*************************************************************************************************************************************

         use mpi

         use mod_global_variables, only :&
                                         get_newunit&
                                        ,IG_LST_UNIT&
                                        ,error_stop&
                                        ,ig_myrank&
                                        ,cg_prefix&
                                        ,rg_mesh_xmin&
                                        ,rg_mesh_ymin&
                                        ,rg_mesh_zmin&
                                        ,ig_mpi_comm_simu&
                                        ,ig_ninterface

         implicit none

         real   (kind=RXP), intent(out), allocatable, dimension(:,:,:) :: interfaces
         integer(kind=IXP), intent(out)                                :: ncols
         integer(kind=IXP), intent(out)                                :: nrows
         real   (kind=RXP), intent(out)                                :: xllcorner
         real   (kind=RXP), intent(out)                                :: yllcorner
         real   (kind=RXP), intent(out)                                :: cellsize
                                                                       
         integer(kind=IXP)                                             :: myunit
         integer(kind=IXP)                                             :: ios
         integer(kind=IXP)                                             :: j
         integer(kind=IXP)                                             :: icol
         integer(kind=IXP)                                             :: irow
         integer(kind=IXP)                                             :: NODATA_value_read
         real   (kind=RXP)                                             :: NODATA_value
                                                                       
         character(len=CIL)                                            :: info
         character(len= 12)                                            :: junk
         character(len=  6)                                            :: ci

         if (ig_myrank == ZERO_IXP) then

!
!---------->read and store in memory all interfaces
            do j = ONE_IXP,ig_ninterface
            
               write(ci,'(i0)') j-ONE_IXP !warning: free surface read in file *.if0 and stored in interface(*,1) ; bottom of layer 1 read in file *.if1 and stored in interface(*,2) ; etc.
            
               open(unit=get_newunit(myunit),file=trim(cg_prefix)//".if"//trim(adjustl(ci)),status='old',action='read',iostat=ios)

               if (ios /= 0_IXP) then

                  write(info,'(a)') "error in module_homogenization:read_interfaces while opening interface "//trim(cg_prefix)//".if"//trim(adjustl(ci))

                  call error_stop(info)

               endif 
            
               read(unit=myunit,fmt=*) junk,ncols
               read(unit=myunit,fmt=*) junk,nrows
               read(unit=myunit,fmt=*) junk,xllcorner
               read(unit=myunit,fmt=*) junk,yllcorner
               read(unit=myunit,fmt=*) junk,cellsize
               read(unit=myunit,fmt=*) junk,NODATA_value_read

               if(.not.allocated(interfaces)) ios = init_array_real(interfaces,ig_ninterface,ncols,nrows,"module_homogenization:read_interfaces:interfaces")
             
!
!------------->warning: the reading is done so that the lower left corner of the dem is at the indices 1,1 of the array interfaces (because xllcorner/yllcorner = west/south cardinal point)
               do irow = nrows,ONE_IXP,-ONE_IXP
                  read(myunit,*) (interfaces(irow,icol,j),icol=ONE_IXP,ncols)
               enddo
            
               close(myunit)
            
            enddo

            NODATA_value = real(NODATA_value_read,kind=RXP)

!
!---------->set xllcorner and yllcorner to zero only if mesh south-west corner is (0.0,0.0). if not, then xllcorner/yllcorner and mesh south-west corner must be in the same coordinate system

            write(IG_LST_UNIT,'(/,a,E15.7)') "xllcorner read = ",xllcorner
            write(IG_LST_UNIT,'(/,a,E15.7)') "yllcorner read = ",yllcorner


            if ( (abs(rg_mesh_xmin) < EPSILON_MACHINE_RXP) .and. (abs(rg_mesh_ymin) < EPSILON_MACHINE_RXP) ) then

               xllcorner = ZERO_IXP
               yllcorner = ZERO_IXP

            endif

            write(IG_LST_UNIT,'(/,a,E15.7)') "xllcorner set = ",xllcorner
            write(IG_LST_UNIT,'(/,a,E15.7)') "yllcorner set = ",yllcorner

!
!---------->check that the free surface read in file *.if0 is defined everywhere
            do icol = ONE_IXP,ncols

               do irow = ONE_IXP,nrows
            
                  if (interfaces(irow,icol,ONE_IXP) <= NODATA_value) then

                     write(info,'(a)') "error in module_homogenization:read_interfaces: free surface read from file "//trim(cg_prefix)//".if0 is not defined everywhere. Check that this file has not NODATA value"
                     call error_stop(info)

                  endif
            
               enddo

            enddo

!
!---------->before defining 1D soil column, set all NODATA_value (generally -9999) to the free surface elevation to avoid any interaction at depth because the standard NODATA_value = -9999 meters depth can be included in the 3D model.
            do j = TWO_IXP,ig_ninterface

               do icol = ONE_IXP,ncols

                  do irow = ONE_IXP,nrows
               
                     if (interfaces(irow,icol,j) <= NODATA_value) interfaces(irow,icol,j) = interfaces(irow,icol,ONE_IXP)
               
                  enddo

               enddo

            enddo

!
!---------->make sure that no interface is higher than the free surface. if so, set its value to the free surface elevation
            do j = TWO_IXP,ig_ninterface

               do icol = ONE_IXP,ncols

                  do irow = ONE_IXP,nrows
               
                     if (interfaces(irow,icol,j) > interfaces(irow,icol,ONE_IXP)) interfaces(irow,icol,j) = interfaces(irow,icol,ONE_IXP)
               
                  enddo

               enddo

            enddo

!
!---------->make sure that no interface is lower than the bottom of the mesh
            do j = ONE_IXP,ig_ninterface

               do icol = ONE_IXP,ncols

                  do irow = ONE_IXP,nrows
               
                     if (interfaces(irow,icol,j) <= rg_mesh_zmin) then

                        write(info,'(a)') "error in module_homogenization:read_interfaces: an interface is deeper than the bottom of the mesh. Check files *.if*"
                        call error_stop(info)

                     endif
               
                  enddo

               enddo

            enddo


!
!---------->ensure that the depper interfaces 'erode' the ones above by setting interface j-1 equal to interface j when interface(*,j-1) < interface(*,j) for 3 <= j <= ninterface (free surface is not concerned)
!           condition not applyed if the j interface is equal to the free surface (interface 1) because the j interface does not exist in this precise case
            do j = ig_ninterface,THREE_IXP,-ONE_IXP

               do icol = ONE_IXP,ncols

                  do irow = ONE_IXP,nrows
               
                     if ( (interfaces(irow,icol,j-ONE_IXP) < interfaces(irow,icol,j)) .and. .not.(abs(interfaces(irow,icol,j)-interfaces(irow,icol,ONE_IXP)) < EPSILON_MACHINE_RXP) ) interfaces(irow,icol,j-ONE_IXP) = interfaces(irow,icol,j)
               
                  enddo

               enddo

            enddo

         endif

!
!------->cpu0 broadcasts interfaces info and data
         call mpi_bcast(ncols            ,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)
         call mpi_bcast(nrows            ,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)
         call mpi_bcast(xllcorner        ,ONE_IXP,MPI_REAL   ,ZERO_IXP,ig_mpi_comm_simu,ios)
         call mpi_bcast(yllcorner        ,ONE_IXP,MPI_REAL   ,ZERO_IXP,ig_mpi_comm_simu,ios)
         call mpi_bcast(cellsize         ,ONE_IXP,MPI_REAL   ,ZERO_IXP,ig_mpi_comm_simu,ios)
         call mpi_bcast(NODATA_value_read,ONE_IXP,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)
         call mpi_bcast(NODATA_value     ,ONE_IXP,MPI_REAL   ,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!------->allocate interfaces array
         if(.not.allocated(interfaces)) ios = init_array_real(interfaces,ig_ninterface,ncols,nrows,"module_homogenization:read_interfaces:interfaces")

         call mpi_bcast(interfaces(ONE_IXP,ONE_IXP,ONE_IXP),nrows*ncols*ig_ninterface,MPI_REAL,ZERO_IXP,ig_mpi_comm_simu,ios)

         return

!*************************************************************************************************************************************
      end subroutine read_interfaces
!*************************************************************************************************************************************


!
!
!>@brief subroutine to find, for each interfaces, the minmax of a rectangular box which contains the interfaces "i"/"i+1"
!>@param  ifs    : interfaces
!>@param  cs     : cellsize  
!>@return ifs_mm : minmax of the box containing each couple of interfaces "i"/"i+1"
!*************************************************************************************************************************************
      subroutine minmax_interfaces(ifs,cs,ifs_mm)
!*************************************************************************************************************************************

         use mod_global_variables, only : rg_mesh_zmin,rg_mesh_zmax

         implicit none

         real   (kind=RXP), dimension(:,:,:)          , intent(in ) :: ifs
         real   (kind=RXP)                            , intent(in ) :: cs
         real   (kind=RXP), dimension(:,:),allocatable, intent(out) :: ifs_mm
                                                     
         real   (kind=RXP)                                          :: d
                                                                  
         real   (kind=RXP), dimension(:)  ,allocatable              :: z_min
         real   (kind=RXP), dimension(:)  ,allocatable              :: z_max
                                                                  
         integer(kind=IXP), dimension(:)  ,allocatable              :: ir_min
         integer(kind=IXP), dimension(:)  ,allocatable              :: ir_max
         integer(kind=IXP), dimension(:)  ,allocatable              :: ic_min
         integer(kind=IXP), dimension(:)  ,allocatable              :: ic_max
         integer(kind=IXP)                                          :: ii
         integer(kind=IXP)                                          :: ic
         integer(kind=IXP)                                          :: ir
         integer(kind=IXP)                                          :: ni
         integer(kind=IXP)                                          :: nc
         integer(kind=IXP)                                          :: nr
         integer(kind=IXP)                                          :: ios

         ni = size(ifs,3_IXP)
         nc = size(ifs,2_IXP)
         nr = size(ifs,1_IXP)

         d = ZERO_RXP

         ios = init_array_int(ir_min,ni,"mod_homogenization:minmax_interfaces:ir_min")
         ios = init_array_int(ir_max,ni,"mod_homogenization:minmax_interfaces:ir_max")
         ios = init_array_int(ic_min,ni,"mod_homogenization:minmax_interfaces:ir_min")
         ios = init_array_int(ic_max,ni,"mod_homogenization:minmax_interfaces:ir_max")

         ios = init_array_real(z_min,ni,"mod_homogenization:minmax_interfaces:z_min")
         ios = init_array_real(z_max,ni,"mod_homogenization:minmax_interfaces:z_max")

         ios = init_array_real(ifs_mm,ni,6_IXP,"mod_homogenization:minmax_interfaces:ifs_mm")

!
!------->find min col
oicmin:  do ii = ONE_IXP,ni-ONE_IXP

            do ic = ONE_IXP,nc

               do ir = ONE_IXP,nr

                  d = ifs(ir,ic,ii) - ifs(ir,ic,ii+ONE_IXP)

                  if (d > EPSILON_MACHINE_RXP) then

                     ic_min(ii) = max(1_IXP,ic-ONE_IXP)

                     cycle oicmin

                  endif

               enddo

            enddo

         enddo oicmin

         !last layer is always the entire domain of simulation
         ic_min(ni) = 1_IXP

!
!------->find max col
oicmax:  do ii = ONE_IXP,ni-ONE_IXP

            do ic = nc,ONE_IXP,-ONE_IXP

               do ir = ONE_IXP,nr

                  d = ifs(ir,ic,ii) - ifs(ir,ic,ii+ONE_IXP)

                  if (d > EPSILON_MACHINE_RXP) then

                     ic_max(ii) = min(nc,ic+ONE_IXP)

                     cycle oicmax

                  endif

               enddo

            enddo

         enddo oicmax

         !last layer is always the entire domain of simulation
         ic_max(ni) = nc

!
!------->find min row
oirmin:  do ii = ONE_IXP,ni-ONE_IXP

            do ir = ONE_IXP,nr

               do ic = ONE_IXP,nc

                  d = ifs(ir,ic,ii) - ifs(ir,ic,ii+ONE_IXP)

                  if (d > EPSILON_MACHINE_RXP) then

                     ir_min(ii) = max(1_IXP,ir-ONE_IXP)

                     cycle oirmin

                  endif

               enddo

            enddo

         enddo oirmin

         !last layer is always the entire domain of simulation
         ir_min(ni) = 1_IXP

!
!------->find max row
oirmax:  do ii = ONE_IXP,ni-ONE_IXP

            do ir = nr,ONE_IXP,-ONE_IXP

               do ic = ONE_IXP,nc

                  d = ifs(ir,ic,ii) - ifs(ir,ic,ii+ONE_IXP)

                  if (d > EPSILON_MACHINE_RXP) then

                     ir_max(ii) = min(nr,ir+ONE_IXP)

                     cycle oirmax

                  endif

               enddo

            enddo

         enddo oirmax

         !last layer is always the entire domain of simulation
         ir_max(ni) = nr

!
!------->find min z
         do ii = ONE_IXP,ni-ONE_IXP
   
            z_min(ii) = minval(ifs(:,:,ii+ONE_IXP))

         enddo

         z_min(ni) = rg_mesh_zmin

!
!------->find max z
         z_max(:) = -HUGE(d)
         
         do ii = ONE_IXP,ni-ONE_IXP

            do ic = ONE_IXP,nc

               do ir = ONE_IXP,nr

                  d = ifs(ir,ic,ii) - ifs(ir,ic,ii+ONE_IXP)

                  if (d > EPSILON_MACHINE_RXP) then
   
                     z_max(ii) = max(z_max(ii),ifs(ir,ic,ii))

                  endif

               enddo

            enddo

         enddo

         z_max(ni) = rg_mesh_zmax

!
!------->fill output array with minmax value
         do ii = ONE_IXP,ni

            ifs_mm(1_IXP,ii) = real(ic_min(ii)-ONE_IXP)*cs
            ifs_mm(2_IXP,ii) = real(ic_max(ii)-ONE_IXP)*cs
            ifs_mm(3_IXP,ii) = real(ir_min(ii)-ONE_IXP)*cs
            ifs_mm(4_IXP,ii) = real(ir_max(ii)-ONE_IXP)*cs
            ifs_mm(5_IXP,ii) = z_min(ii)
            ifs_mm(6_IXP,ii) = z_max(ii)

         enddo

         return

!*************************************************************************************************************************************
      end subroutine minmax_interfaces
!*************************************************************************************************************************************

end module mod_homogenization
