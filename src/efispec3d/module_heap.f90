!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to perform heap sort.
!!
!>@brief
!!This module was originally coded by Daniel Pena under the following terms:
!!Copyright (c) 2014, Daniel Pena 
!!All rights reserved.
!!Redistribution and use in source and binary forms, with or without
!!modification, are permitted provided that the following conditions are met:
!!1. Redistributions of source code must retain the above copyright notice, this
!!list of conditions and the following disclaimer.
!!2. Redistributions in binary form must reproduce the above copyright notice,
!!this list of conditions and the following disclaimer in the documentation
!!and/or other materials provided with the distribution.
!!THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
!!ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
!!WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
!!DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
!!ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
!!(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
!!LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
!!ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
!!(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
!!SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
!! 
!!The original module coded by D. Pena was adapted by F. De Martin to fit EFISPEC3D purpose.

module mod_heap

   use mod_precision

   use mod_global_variables, only :&
                                    error_stop&
                                   ,CIL

   use mod_init_memory

   implicit none
   
   integer(kind=IXP), parameter :: GRID_DIM = 2_IXP

   private
   
   public :: heapfun
   public :: theap
   
   abstract interface

      function heapfun( node1, node2 ) result(res)

         use mod_precision

         real   (kind=RXP), dimension(:), intent(in) :: node1
         real   (kind=RXP), dimension(:), intent(in) :: node2
         logical(kind=IXP)                           :: res

      end function heapfun

   end interface
   
   type :: theap

      integer(kind=IXP)                   :: nmax      ! max size
      integer(kind=IXP)                   :: n         ! current heap size
      integer(kind=IXP)                   :: m         ! current tree size
      integer(kind=IXP)                   :: nlen      ! node size (>1 if more than one value stored in a node)
      real   (kind=RXP), allocatable      :: data(:,:) ! node data
      integer(kind=IXP), allocatable      :: grid(:,:) ! 2D grid (defined by rows and colums) associated to the node
      integer(kind=IXP), allocatable      :: indx(:)   ! node index

      procedure(heapfun), nopass, pointer :: fun       ! heap function to find root node

      contains
      procedure :: size           => heap_size
      procedure :: init           => heap_init
      procedure :: insert         => heap_insert
      procedure :: peek           => heap_peek
      procedure :: peek_indx2grid => heap_peek_indx2grid
      procedure :: percolate_down => heap_percolate_down
      procedure :: pop            => heap_pop
      procedure :: grow           => heap_grow
      procedure :: reheap         => heap_reheap
      procedure :: belong         => heap_belong
      procedure :: release        => heap_release

   end type theap
   
   contains


!
! 
!>@brief returns the heap current size
!***********************************************************************************************************************************************************************************
      integer(kind=IXP) function heap_size( heap )
!***********************************************************************************************************************************************************************************

         class(theap) :: heap

         heap_size = heap%n

!***********************************************************************************************************************************************************************************
      end function heap_size
!***********************************************************************************************************************************************************************************
   

!
! 
!***********************************************************************************************************************************************************************************
      subroutine heap_init(heap,nmax,nlen,hpfun)
!***********************************************************************************************************************************************************************************

         ! initializes the heap 
         ! nmax  -  max size of the heap
         ! nlen  -  size of each node 
         ! hpfun -  the heap function (provides comparison between two nodes' data)

         class(theap)                  :: heap

         integer(kind=IXP), intent(in) :: nmax, nlen

         procedure(heapfun)            :: hpfun

         integer(kind=IXP)             :: i 
         integer(kind=IXP)             :: ios 

         heap%nmax = nmax
         heap%n    = 0_IXP
         heap%m    = 0_IXP
         heap%nlen = nlen
         heap%fun  => hpfun

         ios = init_array_int(heap%indx,nmax,"mod_heap:heap_init:indx")

         ios = init_array_real(heap%data,nmax,nlen,"mod_heap:heap_init:data")

         ios = init_array_int(heap%grid,nmax,GRID_DIM,"mod_heap:heap_init:grid")

         do i = 1_IXP, nmax

            heap%indx(i) = i

         enddo

         return   

!***********************************************************************************************************************************************************************************
      end subroutine heap_init
!***********************************************************************************************************************************************************************************
      

!
! 
!>@brief insert a node into a heap. the resulting tree is re-heaped.
!>@param heap : the heap 
!>@param node : a real array, nlen long, which contains the node's information to be inserted.
!>@param grid : an integer array, 2 long, which contains the row and colum of the grid associated to the node.
!***********************************************************************************************************************************************************************************
      subroutine heap_insert(heap,node,grid)
!***********************************************************************************************************************************************************************************

         class(theap) :: heap

         real   (kind=RXP), dimension(heap%nlen), intent(in) :: node
         integer(kind=IXP), dimension(GRID_DIM) , intent(in) :: grid
   
         integer(kind=IXP)                                   :: k1
         integer(kind=IXP)                                   :: k2
         integer(kind=IXP)                                   :: il
         integer(kind=IXP)                                   :: ir

         character(len=CIL)                                  :: info
   
         if( heap%n .eq. heap%nmax ) then

            write(info,'(a)') "error in module_heap:heap_insert: max size of the heap reached"

            call error_stop(info)

         endif

!         
!------->add one element and copy node data to new element

         heap%n = heap%n + 1_IXP
         heap%m = heap%m + 1_IXP

         heap%data(:,heap%indx(heap%n)) = node(:)
         heap%grid(:,heap%indx(heap%n)) = grid(:)

!   
!------->re-index the heap from the bottom up

         k2 = heap%n

         do while ( k2 /= 1_IXP )

            k1 = int(k2 / 2_IXP)

            ir = heap%indx(k2) 
            il = heap%indx(k1) 

            if( heap%fun( heap%data(:,il), heap%data(:,ir) ) ) return

            call swapint( heap%indx(k2), heap%indx(k1) )

            k2 = int(k2 / 2_IXP)

         enddo

!***********************************************************************************************************************************************************************************
      end subroutine heap_insert                  
!***********************************************************************************************************************************************************************************

   
!
! 
!>@brief access the k-th node of the heap
!***********************************************************************************************************************************************************************************
      subroutine heap_peek( heap, k, node )                      
!***********************************************************************************************************************************************************************************

         class(theap)                   :: heap
         integer(kind=IXP), intent( in) :: k
         real   (kind=RXP), intent(out) :: node(heap%nlen)

         if (k .lt. 1_IXP .or. k .gt. heap%n .or. heap%n .gt. heap%nmax) return

         node(:) = heap%data(:,heap%indx(k))

!***********************************************************************************************************************************************************************************
      end subroutine heap_peek
!***********************************************************************************************************************************************************************************


!
! 
!>@brief access the k-th grid of the heap
!***********************************************************************************************************************************************************************************
      subroutine heap_peek_indx2grid(heap,k,i,j)
!***********************************************************************************************************************************************************************************

         class(theap)                   :: heap

         integer(kind=IXP), intent( in) :: k
         integer(kind=IXP), intent(out) :: i
         integer(kind=IXP), intent(out) :: j

         if (k .lt. 1_IXP .or. k .gt. heap%n) return

         i = heap%grid(1_IXP,heap%indx(k))
         j = heap%grid(2_IXP,heap%indx(k))

!***********************************************************************************************************************************************************************************
      end subroutine heap_peek_indx2grid
!***********************************************************************************************************************************************************************************


!
! 
!>@brief get the index of the grid i,j
!***********************************************************************************************************************************************************************************
      subroutine heap_percolate_down(heap,node,grid)
!***********************************************************************************************************************************************************************************

         class(theap)                                        :: heap

         real   (kind=RXP), dimension(heap%nlen), intent(in) :: node
         integer(kind=IXP), dimension(GRID_DIM) , intent(in) :: grid

         integer(kind=IXP)                                   :: n
         integer(kind=IXP)                                   :: i
         integer(kind=IXP)                                   :: j
         integer(kind=IXP)                                   :: k
         integer(kind=IXP)                                   :: k2
         integer(kind=IXP)                                   :: k1
         integer(kind=IXP)                                   :: ip
         integer(kind=IXP)                                   :: ic

         logical(kind=IXP)                                   :: is_found

         character(len=CIL)                                  :: info

!
!------>find the heap index of the grid point 

         is_found = .false.

         do i = ONE_IXP,heap%n

            n = ZERO_IXP

            do j = ONE_IXP,GRID_DIM

               if (grid(j) == heap%grid(j,i)) then

                  n = n + 1_IXP

               endif

            enddo

            if (n == GRID_DIM) then

               k = i 

               is_found = .true.

               exit

            endif

         enddo

         if (.not.is_found) then

            write(info,'(a)') "error in module_heap:heap_percolate_down: grid point not found in the heap"
          
            call error_stop(info)

         endif

!
!------->change the value of the heap node

         heap%data(:,k) = node(:)

!
!------->percolate down: re-index the heap from the index k

         k2 = k

         do while( k2 /= 1_IXP )

            k1 = int(k2/2_IXP)

            ic = heap%indx(k2) !child
            ip = heap%indx(k1) !parent

            if ( heap%fun( heap%data(:,ip), heap%data(:,ic) ) ) return

            call swapint( heap%indx(k2), heap%indx(k1) )

            k2 = int(k2/2_IXP)

         enddo


!***********************************************************************************************************************************************************************************
      end subroutine heap_percolate_down
!***********************************************************************************************************************************************************************************


!
!
!>@brief retrieve the root element off the heap. the resulting tree is re-heaped. no data is deleted.
!>@return (optional) node : the deleted node
!***********************************************************************************************************************************************************************************
      subroutine heap_pop(heap,node)
!***********************************************************************************************************************************************************************************

         class(theap)                          :: heap

         real(kind=RXP), intent(out), optional :: node(heap%nlen)
   
         if( heap%n .eq. 0_IXP ) return
   
         if( present(node) ) then

            node(:) = heap%data(:,heap%indx(1_IXP))

         endif
   
         call swapint( heap%indx(ONE_IXP), heap%indx(heap%n) )
         
         heap%n = heap%n - ONE_IXP
   
         call heap_grow(heap,ONE_IXP)
   
!***********************************************************************************************************************************************************************************
      end subroutine heap_pop
!***********************************************************************************************************************************************************************************
   

!
!
!>@brief forms a heap out of a tree. used by heap_reheap and heap_pop.
!! the root node of the tree is stored in the location indx(ktemp).
!! the first child node is in location indx(2*ktemp)...
!! the next child node is in location indx(2*ktemp+1).
!! this subroutines assumes each branch of the tree is itself a heap.
!***********************************************************************************************************************************************************************************
      subroutine heap_grow(heap,ktemp)                
!***********************************************************************************************************************************************************************************

         class(theap)                  :: heap

         integer(kind=IXP), intent(in) :: ktemp

         integer(kind=IXP)             :: i, k, il, ir
   
         k = ktemp

         do while( 2_IXP*k <= heap%n )
   
            i = 2_IXP*k

!    
!---------->if there is more than one child node, find which is the smallest.

            if ( 2_IXP*k /= heap%n ) then 

               il = heap%indx(2_IXP*k+1_IXP)
               ir = heap%indx(2_IXP*k)   

               if ( heap%fun(heap%data(:,il),heap%data(:,ir)) ) then

                  i = i + 1_IXP

               endif

            endif
   
!---------->if a child is larger than its parent, interchange them... this destroys the heap property, so the remaining elements must be re-heaped. 

            il    = heap%indx(k) 
            ir    = heap%indx(i) 

            if ( heap%fun(heap%data(:,il),heap%data(:,ir)) ) return
            
            call swapint( heap%indx(i), heap%indx(k) )
            
            k = i
         
         enddo
   
!***********************************************************************************************************************************************************************************
      end subroutine heap_grow
!***********************************************************************************************************************************************************************************
   

!
! 
!***********************************************************************************************************************************************************************************
      subroutine heap_reheap(heap,hpfun)                   
!***********************************************************************************************************************************************************************************
         ! builds the heap from the element data using the provided heap function.
         ! at exit, the root node satisfies the heap condition:
         !   hpfun( root_node, node ) = .true. for any other node 
         ! 
         class(theap)                 :: heap
         procedure(heapfun), optional :: hpfun
         integer(kind=IXP)            :: k
   
         heap%n = heap%m

         if( present( hpfun ) ) then
            heap%fun => hpfun 
         endif
   
         if(heap%nmax .lt. heap%n) return
   
         do k = heap%n / 2, 1, -1

            call heap_grow(heap,k) 

         enddo

!***********************************************************************************************************************************************************************************
      end subroutine heap_reheap
!***********************************************************************************************************************************************************************************


!
!
!>@brief subroutine to check if the grid point (m,l) belongs to the heap structure 
!***********************************************************************************************************************************************************************************
      logical(kind=IXP) function heap_belong(heap,grid)
!***********************************************************************************************************************************************************************************

         class(theap)                                        :: heap

         integer(kind=IXP), dimension(GRID_DIM) , intent(in) :: grid

         integer(kind=IXP)                                   :: i
         integer(kind=IXP)                                   :: j
         integer(kind=IXP)                                   :: n

         heap_belong = .false.

         do i = ONE_IXP,heap%n

            n = ZERO_IXP

            do j = ONE_IXP,GRID_DIM

               if (grid(j) == heap%grid(j,i)) then

                  n = n + 1_IXP

               endif

            enddo

            if (n == GRID_DIM) then

               heap_belong = .true.

               return

            endif

         enddo

!***********************************************************************************************************************************************************************************
      end function heap_belong
!***********************************************************************************************************************************************************************************


!
! 
!***********************************************************************************************************************************************************************************
      subroutine swapint( i, k )
!***********************************************************************************************************************************************************************************

         integer(kind=IXP) :: i, k, t

         t = i
         i = k
         k = t

!***********************************************************************************************************************************************************************************
      end subroutine swapint
!***********************************************************************************************************************************************************************************


!
! 
!>@brief releases all the allocated memory and resets the heap
!***********************************************************************************************************************************************************************************
      subroutine heap_release(heap)
!***********************************************************************************************************************************************************************************
         class(theap), intent(inout) :: heap

         deallocate(heap%indx)
         deallocate(heap%data)
         deallocate(heap%grid)

         heap%n    = 0_IXP
         heap%m    = 0_IXP
         heap%nmax = 0_IXP
         heap%fun  => null()

!***********************************************************************************************************************************************************************************
      end subroutine heap_release                          
!***********************************************************************************************************************************************************************************
   
end module mod_heap
