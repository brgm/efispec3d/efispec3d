!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!

!>@file
!!This file contains a module to initialize the time step of the simulation.

!>@brief
!!This module contains subroutines to initialize the time step of the simulation.
module mod_init_time_step
   
   use mpi

   use mod_precision

   implicit none

   private

   public  :: init_time_step
   private :: round_time_step
   private :: manexp
   
   contains


!
!
!>@brief
!!This subroutine returns the time step mod_global_variables::rg_dt that makes the simulation stable.
!!It also verifies that there are enough GLL points per smallest wavelength.
!!Hexahedron elements should not be deformed too much.
!>@return mod_global_variables::rg_dt
!>@return mod_global_variables::rg_mem_var_exp
!***********************************************************************************************************************************************************************************
      subroutine init_time_step()
!***********************************************************************************************************************************************************************************

         use mpi
      
         use mod_global_variables, only :&
                                         IG_NGLL&
                                        ,IG_LST_UNIT&
                                        ,TINY_REAL_RXP&
                                        ,IG_NRELAX&
                                        ,RG_RELAX_COEFF&
                                        ,LG_VISCO&
                                        ,rg_mem_var_exp&
                                        ,rg_simu_times&
                                        ,rg_simu_total_time&
                                        ,rg_dt&
                                        ,ig_ndt&
                                        ,ig_nhexa&
                                        ,rg_gll_coordinate&
                                        ,rg_hexa_gll_rho&
                                        ,rg_hexa_gll_rhovp2&
                                        ,rg_hexa_gll_rhovs2&
                                        ,ig_hexa_gll_glonum&
                                        ,lg_boundary_absorption&
                                        ,rg_fmax_simu&
                                        ,ig_mpi_comm_simu&
                                        ,ig_myrank&
                                        ,error_stop

         use mod_init_efi, only :&
                                 init_temporal_domain&
                                ,init_temporal_saving

         use mod_write_listing

         use mod_init_memory, only : init_array_real

         real   (kind=RXP), dimension(ig_nhexa) :: hexa_dt_stable
         real   (kind=RXP), dimension(ig_nhexa) :: hexa_npts_per_lambda

         real   (kind=RXP)                      :: dt_stable
         real   (kind=RXP)                      :: dt_stable_all_cpu

         real   (kind=RXP)                      :: min_npts_per_lambda
         real   (kind=RXP)                      :: max_npts_per_lambda
         real   (kind=RXP)                      :: min_npts_per_lambda_all_cpu
         real   (kind=RXP)                      :: max_npts_per_lambda_all_cpu

         real   (kind=RXP)                      :: igll_dist_k
         real   (kind=RXP)                      :: igll_dist_l
         real   (kind=RXP)                      :: igll_dist_m
         real   (kind=RXP)                      :: igll_dist_k_l
         real   (kind=RXP)                      :: igll_dist_k_m
         real   (kind=RXP)                      :: igll_dist_l_m
         real   (kind=RXP)                      :: igll_dist_k_kl
         real   (kind=RXP)                      :: igll_dist_l_kl
         real   (kind=RXP)                      :: igll_dist_k_km
         real   (kind=RXP)                      :: igll_dist_m_km
         real   (kind=RXP)                      :: igll_dist_l_lm
         real   (kind=RXP)                      :: igll_dist_m_lm
         real   (kind=RXP)                      :: igll_dist_kl_klm
         real   (kind=RXP)                      :: igll_dist_km_klm
         real   (kind=RXP)                      :: igll_dist_lm_klm
         real   (kind=RXP)                      :: igll_dist_max

         real   (kind=RXP)                      :: igll_corner_x
         real   (kind=RXP)                      :: igll_corner_y
         real   (kind=RXP)                      :: igll_corner_z
         real   (kind=RXP)                      :: igll_k_x
         real   (kind=RXP)                      :: igll_k_y
         real   (kind=RXP)                      :: igll_k_z
         real   (kind=RXP)                      :: igll_l_x
         real   (kind=RXP)                      :: igll_l_y
         real   (kind=RXP)                      :: igll_l_z
         real   (kind=RXP)                      :: igll_m_x
         real   (kind=RXP)                      :: igll_m_y
         real   (kind=RXP)                      :: igll_m_z
         real   (kind=RXP)                      :: igll_kl_x
         real   (kind=RXP)                      :: igll_kl_y
         real   (kind=RXP)                      :: igll_kl_z
         real   (kind=RXP)                      :: igll_km_x
         real   (kind=RXP)                      :: igll_km_y
         real   (kind=RXP)                      :: igll_km_z
         real   (kind=RXP)                      :: igll_lm_x
         real   (kind=RXP)                      :: igll_lm_y
         real   (kind=RXP)                      :: igll_lm_z
         real   (kind=RXP)                      :: igll_klm_x
         real   (kind=RXP)                      :: igll_klm_y
         real   (kind=RXP)                      :: igll_klm_z

         real   (kind=RXP)                      :: igll_corner_vp
         real   (kind=RXP)                      :: igll_k_vp
         real   (kind=RXP)                      :: igll_l_vp
         real   (kind=RXP)                      :: igll_m_vp
         real   (kind=RXP)                      :: igll_kl_vp
         real   (kind=RXP)                      :: igll_km_vp
         real   (kind=RXP)                      :: igll_lm_vp
         real   (kind=RXP)                      :: igll_klm_vp

         real   (kind=RXP)                      :: igll_corner_vs
         real   (kind=RXP)                      :: igll_k_vs
         real   (kind=RXP)                      :: igll_l_vs
         real   (kind=RXP)                      :: igll_m_vs
         real   (kind=RXP)                      :: igll_kl_vs
         real   (kind=RXP)                      :: igll_km_vs
         real   (kind=RXP)                      :: igll_lm_vs
         real   (kind=RXP)                      :: igll_klm_vs

         real   (kind=RXP)                      :: vp_max
         real   (kind=RXP)                      :: vp_min
         real   (kind=RXP)                      :: vs_min
         real   (kind=RXP)                      :: v_min

         real   (kind=RXP)                      :: C_max

         real   (kind=RXP)                      :: dt_c_k
         real   (kind=RXP)                      :: dt_c_l
         real   (kind=RXP)                      :: dt_c_m
         real   (kind=RXP)                      :: dt_k_l
         real   (kind=RXP)                      :: dt_k_m
         real   (kind=RXP)                      :: dt_l_m
         real   (kind=RXP)                      :: dt_k_kl
         real   (kind=RXP)                      :: dt_l_kl
         real   (kind=RXP)                      :: dt_k_km
         real   (kind=RXP)                      :: dt_m_km
         real   (kind=RXP)                      :: dt_l_lm
         real   (kind=RXP)                      :: dt_m_lm
         real   (kind=RXP)                      :: dt_kl_klm
         real   (kind=RXP)                      :: dt_km_klm
         real   (kind=RXP)                      :: dt_lm_klm

         integer(kind=IXP)                      :: ihexa
         integer(kind=IXP)                      :: kgll
         integer(kind=IXP)                      :: lgll
         integer(kind=IXP)                      :: mgll
         integer(kind=IXP)                      :: igll_corner
         integer(kind=IXP)                      :: igll_k
         integer(kind=IXP)                      :: igll_l
         integer(kind=IXP)                      :: igll_m
         integer(kind=IXP)                      :: igll_kl
         integer(kind=IXP)                      :: igll_km
         integer(kind=IXP)                      :: igll_lm
         integer(kind=IXP)                      :: igll_klm
         integer(kind=IXP)                      :: kshift
         integer(kind=IXP)                      :: lshift
         integer(kind=IXP)                      :: mshift
         integer(kind=IXP)                      :: ios
         integer(kind=IXP)                      :: imem_var
         integer(kind=IXP)                      :: idt

         character(len=CIL)                     :: info

!
!------->define CFL condition

         if(lg_boundary_absorption) then
      
            C_max = 0.29_RXP
      
         else
      
            C_max = 0.59_RXP
      
         endif

!
!------->loop over hexa

         do ihexa = ONE_IXP,ig_nhexa
      
            hexa_dt_stable(ihexa) = huge(C_max)
            hexa_npts_per_lambda(ihexa) = huge(6.0)

            do kgll = ONE_IXP,IG_NGLL,IG_NGLL-ONE_IXP
      
               kshift = kgll - ONE_IXP + TWO_IXP*mod(kgll,IG_NGLL)
      
               do lgll = ONE_IXP,IG_NGLL,IG_NGLL-ONE_IXP
      
                  lshift = lgll - ONE_IXP + TWO_IXP*mod(lgll,IG_NGLL)
      
                  do mgll = ONE_IXP,IG_NGLL,IG_NGLL-ONE_IXP
      
                     mshift = mgll - ONE_IXP + TWO_IXP*mod(mgll,IG_NGLL)

!
!------------------->get global gll numbering
      
                     igll_corner = ig_hexa_gll_glonum(mgll  ,lgll  ,kgll  ,ihexa)

                     igll_k      = ig_hexa_gll_glonum(mgll  ,lgll  ,kshift,ihexa)
                     igll_l      = ig_hexa_gll_glonum(mgll  ,lshift,kgll  ,ihexa)
                     igll_m      = ig_hexa_gll_glonum(mshift,lgll  ,kgll  ,ihexa)

                     igll_kl     = ig_hexa_gll_glonum(mgll  ,lshift,kshift,ihexa)
                     igll_km     = ig_hexa_gll_glonum(mshift,lgll  ,kshift,ihexa)
                     igll_lm     = ig_hexa_gll_glonum(mshift,lshift,kgll  ,ihexa)

                     igll_klm    = ig_hexa_gll_glonum(mshift,lshift,kshift,ihexa)
      
!     
!------------------->get corner GLL coordinates

                     igll_corner_x = rg_gll_coordinate(1_IXP,igll_corner)
                     igll_corner_y = rg_gll_coordinate(2_IXP,igll_corner)
                     igll_corner_z = rg_gll_coordinate(3_IXP,igll_corner)

                     igll_k_x = rg_gll_coordinate(1_IXP,igll_k)
                     igll_k_y = rg_gll_coordinate(2_IXP,igll_k)
                     igll_k_z = rg_gll_coordinate(3_IXP,igll_k)
      
                     igll_l_x = rg_gll_coordinate(1_IXP,igll_l)
                     igll_l_y = rg_gll_coordinate(2_IXP,igll_l)
                     igll_l_z = rg_gll_coordinate(3_IXP,igll_l)

                     igll_m_x = rg_gll_coordinate(1_IXP,igll_m)
                     igll_m_y = rg_gll_coordinate(2_IXP,igll_m)
                     igll_m_z = rg_gll_coordinate(3_IXP,igll_m)

                     igll_kl_x = rg_gll_coordinate(1_IXP,igll_kl)
                     igll_kl_y = rg_gll_coordinate(2_IXP,igll_kl)
                     igll_kl_z = rg_gll_coordinate(3_IXP,igll_kl)
      
                     igll_km_x = rg_gll_coordinate(1_IXP,igll_km)
                     igll_km_y = rg_gll_coordinate(2_IXP,igll_km)
                     igll_km_z = rg_gll_coordinate(3_IXP,igll_km)
      
                     igll_lm_x = rg_gll_coordinate(1_IXP,igll_lm)
                     igll_lm_y = rg_gll_coordinate(2_IXP,igll_lm)
                     igll_lm_z = rg_gll_coordinate(3_IXP,igll_lm)
      
                     igll_klm_x = rg_gll_coordinate(1_IXP,igll_klm)
                     igll_klm_y = rg_gll_coordinate(2_IXP,igll_klm)
                     igll_klm_z = rg_gll_coordinate(3_IXP,igll_klm)
      
!     
!------------------->compute the distance between the corner GLL node and the closest GLL node towards m-direction

                     igll_dist_m = sqrt((igll_corner_x-igll_m_x)**TWO_IXP + (igll_corner_y-igll_m_y)**TWO_IXP + (igll_corner_z-igll_m_z)**TWO_IXP)
      
!     
!------------------->compute the distance between the corner GLL node and the closest GLL node towards l-direction

                     igll_dist_l = sqrt((igll_corner_x-igll_l_x)**TWO_IXP +(igll_corner_y-igll_l_y)**TWO_IXP + (igll_corner_z-igll_l_z)**TWO_IXP)
      
!     
!------------------->compute the distance between the corner GLL node and the closest GLL node towards k-direction

                     igll_dist_k = sqrt((igll_corner_x-igll_k_x)**TWO_IXP +(igll_corner_y-igll_k_y)**TWO_IXP + (igll_corner_z-igll_k_z)**TWO_IXP)

!     
!------------------->compute the distance between the k, l or m-direction GLL node and the k, l or m-direction GLL node

                     igll_dist_k_l     = sqrt((igll_k_x-igll_l_x)**TWO_IXP +(igll_k_y-igll_l_y)**TWO_IXP + (igll_k_z-igll_l_z)**TWO_IXP)
                     igll_dist_k_m     = sqrt((igll_k_x-igll_m_x)**TWO_IXP +(igll_k_y-igll_m_y)**TWO_IXP + (igll_k_z-igll_m_z)**TWO_IXP)
                     igll_dist_l_m     = sqrt((igll_l_x-igll_m_x)**TWO_IXP +(igll_l_y-igll_m_y)**TWO_IXP + (igll_l_z-igll_m_z)**TWO_IXP)
                                       
                     igll_dist_k_kl    = sqrt((igll_k_x-igll_kl_x)**TWO_IXP +(igll_k_y-igll_kl_y)**TWO_IXP + (igll_k_z-igll_kl_z)**TWO_IXP)
                     igll_dist_l_kl    = sqrt((igll_l_x-igll_kl_x)**TWO_IXP +(igll_l_y-igll_kl_y)**TWO_IXP + (igll_l_z-igll_kl_z)**TWO_IXP)
                                       
                     igll_dist_k_km    = sqrt((igll_k_x-igll_km_x)**TWO_IXP +(igll_k_y-igll_km_y)**TWO_IXP + (igll_k_z-igll_km_z)**TWO_IXP)
                     igll_dist_m_km    = sqrt((igll_m_x-igll_km_x)**TWO_IXP +(igll_m_y-igll_km_y)**TWO_IXP + (igll_m_z-igll_km_z)**TWO_IXP)
                                       
                     igll_dist_l_lm    = sqrt((igll_l_x-igll_lm_x)**TWO_IXP +(igll_l_y-igll_lm_y)**TWO_IXP + (igll_l_z-igll_lm_z)**TWO_IXP)
                     igll_dist_m_lm    = sqrt((igll_m_x-igll_lm_x)**TWO_IXP +(igll_m_y-igll_lm_y)**TWO_IXP + (igll_m_z-igll_lm_z)**TWO_IXP)

                     igll_dist_kl_klm  = sqrt((igll_kl_x-igll_klm_x)**TWO_IXP +(igll_kl_y-igll_klm_y)**TWO_IXP + (igll_kl_z-igll_klm_z)**TWO_IXP)
                     igll_dist_km_klm  = sqrt((igll_km_x-igll_klm_x)**TWO_IXP +(igll_km_y-igll_klm_y)**TWO_IXP + (igll_km_z-igll_klm_z)**TWO_IXP)
                     igll_dist_lm_klm  = sqrt((igll_lm_x-igll_klm_x)**TWO_IXP +(igll_lm_y-igll_klm_y)**TWO_IXP + (igll_lm_z-igll_klm_z)**TWO_IXP)

!     
!------------------->get P-wave velocity around corner kgll,lgll,mgll

                     igll_corner_vp = sqrt(rg_hexa_gll_rhovp2(mgll  ,lgll  ,kgll  ,ihexa)/rg_hexa_gll_rho(mgll  ,lgll  ,kgll  ,ihexa))

                     igll_k_vp      = sqrt(rg_hexa_gll_rhovp2(mgll  ,lgll  ,kshift,ihexa)/rg_hexa_gll_rho(mgll  ,lgll  ,kshift,ihexa))
                     igll_l_vp      = sqrt(rg_hexa_gll_rhovp2(mgll  ,lshift,kgll  ,ihexa)/rg_hexa_gll_rho(mgll  ,lshift,kgll  ,ihexa))
                     igll_m_vp      = sqrt(rg_hexa_gll_rhovp2(mshift,lgll  ,kgll  ,ihexa)/rg_hexa_gll_rho(mshift,lgll  ,kgll  ,ihexa))

                     igll_kl_vp     = sqrt(rg_hexa_gll_rhovp2(mgll  ,lshift,kshift,ihexa)/rg_hexa_gll_rho(mgll  ,lshift,kshift,ihexa))
                     igll_km_vp     = sqrt(rg_hexa_gll_rhovp2(mshift,lgll  ,kshift,ihexa)/rg_hexa_gll_rho(mshift,lgll  ,kshift,ihexa))
                     igll_lm_vp     = sqrt(rg_hexa_gll_rhovp2(mshift,lshift,kgll  ,ihexa)/rg_hexa_gll_rho(mshift,lshift,kgll  ,ihexa))

                     igll_klm_vp    = sqrt(rg_hexa_gll_rhovp2(mshift,lshift,kshift,ihexa)/rg_hexa_gll_rho(mshift,lshift,kshift,ihexa))

!     
!------------------->get S-wave velocity around corner kgll,lgll,mgll

                     igll_corner_vs = sqrt(rg_hexa_gll_rhovs2(mgll  ,lgll  ,kgll  ,ihexa)/rg_hexa_gll_rho(mgll  ,lgll  ,kgll  ,ihexa))

                     igll_k_vs      = sqrt(rg_hexa_gll_rhovs2(mgll  ,lgll  ,kshift,ihexa)/rg_hexa_gll_rho(mgll  ,lgll  ,kshift,ihexa))
                     igll_l_vs      = sqrt(rg_hexa_gll_rhovs2(mgll  ,lshift,kgll  ,ihexa)/rg_hexa_gll_rho(mgll  ,lshift,kgll  ,ihexa))
                     igll_m_vs      = sqrt(rg_hexa_gll_rhovs2(mshift,lgll  ,kgll  ,ihexa)/rg_hexa_gll_rho(mshift,lgll  ,kgll  ,ihexa))

                     igll_kl_vs     = sqrt(rg_hexa_gll_rhovs2(mgll  ,lshift,kshift,ihexa)/rg_hexa_gll_rho(mgll  ,lshift,kshift,ihexa))
                     igll_km_vs     = sqrt(rg_hexa_gll_rhovs2(mshift,lgll  ,kshift,ihexa)/rg_hexa_gll_rho(mshift,lgll  ,kshift,ihexa))
                     igll_lm_vs     = sqrt(rg_hexa_gll_rhovs2(mshift,lshift,kgll  ,ihexa)/rg_hexa_gll_rho(mshift,lshift,kgll  ,ihexa))

                     igll_klm_vs    = sqrt(rg_hexa_gll_rhovs2(mshift,lshift,kshift,ihexa)/rg_hexa_gll_rho(mshift,lshift,kshift,ihexa))

!
!------------------->compute maximal P-wave velocity around corner kgll,lgll,mgll. safety condition to get stable simulation (but potentially using more cputime)

                     vp_max = max(igll_corner_vp,igll_k_vp,igll_l_vp,igll_m_vp,igll_kl_vp,igll_km_vp,igll_lm_vp,igll_klm_vp)

                     dt_c_k    = C_max*igll_dist_k/vp_max
                     dt_c_l    = C_max*igll_dist_l/vp_max
                     dt_c_m    = C_max*igll_dist_m/vp_max
                     dt_k_l    = C_max*igll_dist_k_l/vp_max
                     dt_k_m    = C_max*igll_dist_k_m/vp_max
                     dt_l_m    = C_max*igll_dist_l_m/vp_max
                     dt_k_kl   = C_max*igll_dist_k_kl/vp_max
                     dt_l_kl   = C_max*igll_dist_l_kl/vp_max
                     dt_k_km   = C_max*igll_dist_k_km/vp_max
                     dt_m_km   = C_max*igll_dist_m_km/vp_max
                     dt_l_lm   = C_max*igll_dist_l_lm/vp_max
                     dt_m_lm   = C_max*igll_dist_m_lm/vp_max
                     dt_kl_klm = C_max*igll_dist_kl_klm/vp_max
                     dt_km_klm = C_max*igll_dist_km_klm/vp_max
                     dt_lm_klm = C_max*igll_dist_lm_klm/vp_max

                     hexa_dt_stable(ihexa) = min(hexa_dt_stable(ihexa),dt_c_k,dt_c_l,dt_c_m,dt_k_l,dt_k_m,dt_l_m,dt_k_kl,dt_l_kl,dt_k_km,dt_m_km,dt_l_lm,dt_m_lm,dt_kl_klm,dt_km_klm,dt_lm_klm)

!
!------------------->compute minimal wave velocity around corner kgll,lgll,mgll, and check number of GLL points per smallest wavelength

                     vp_min = min(igll_corner_vp,igll_k_vp,igll_l_vp,igll_m_vp,igll_kl_vp,igll_km_vp,igll_lm_vp,igll_klm_vp)

                     vs_min = min(igll_corner_vs,igll_k_vs,igll_l_vs,igll_m_vs,igll_kl_vs,igll_km_vs,igll_lm_vs,igll_klm_vs)

                     v_min  = min(vp_min,vs_min)

                     igll_dist_max = max(igll_dist_k,igll_dist_l,igll_dist_m)

                     min_npts_per_lambda = v_min/(rg_fmax_simu*igll_dist_max)

                     hexa_npts_per_lambda(ihexa) = min(hexa_npts_per_lambda(ihexa), min_npts_per_lambda)

                  enddo
               enddo
            enddo

         enddo

         dt_stable = minval(hexa_dt_stable)

         min_npts_per_lambda = minval(hexa_npts_per_lambda)

         max_npts_per_lambda = maxval(hexa_npts_per_lambda)

         call round_time_step(dt_stable)

         call mpi_allreduce(dt_stable,dt_stable_all_cpu,ONE_IXP,MPI_REAL,MPI_MIN,ig_mpi_comm_simu,ios)

         call mpi_allreduce(min_npts_per_lambda,min_npts_per_lambda_all_cpu,ONE_IXP,MPI_REAL,MPI_MIN,ig_mpi_comm_simu,ios)

         call mpi_allreduce(max_npts_per_lambda,max_npts_per_lambda_all_cpu,ONE_IXP,MPI_REAL,MPI_MAX,ig_mpi_comm_simu,ios)

         if ( (rg_dt <= TINY_REAL_RXP) .or. (rg_dt > dt_stable_all_cpu) ) then

            rg_dt = dt_stable_all_cpu

            call init_temporal_domain(rg_simu_total_time,rg_dt)

            call init_temporal_saving(ig_ndt)

            call write_cfl_condition_ko(rg_dt)

         else

            call write_cfl_condition_ok(rg_dt)

         endif

         if (ig_myrank == ZERO_IXP) then

            write(IG_LST_UNIT,'(" ",/,a)') "checking nb of grid points per smallest wavelength (should be >6)"
            write(IG_LST_UNIT,'(a,f10.3,a)')   " --> for fmax = ", rg_fmax_simu, " Hz"
            write(IG_LST_UNIT,'(a,f10.3)')     " --> min nb of points per wavelength = ", min_npts_per_lambda_all_cpu
            write(IG_LST_UNIT,'(a,f10.3)')     " --> max nb of points per wavelength = ", max_npts_per_lambda_all_cpu

         endif

         if ( min_npts_per_lambda_all_cpu < 6.0_RXP ) then

            write(info,'(a)') "error: nb of grid points per smallest wavelength < 6"
            call error_stop(info)

         endif

!       
!------->once time step is determined: precompute exponential used for computing viscoelastic internal forces

         if (LG_VISCO) then
        
            do imem_var = ONE_IXP,IG_NRELAX

               rg_mem_var_exp(imem_var) = exp(-rg_dt/RG_RELAX_COEFF(imem_var,ONE_IXP))

            enddo
        
         endif

!
!------->initialize array containing the dicrete times of the simulation

         ios = init_array_real(rg_simu_times,ig_ndt,"mod_init_time_step:init_time_step:rg_simu_times")

         do idt = ONE_IXP,ig_ndt

            rg_simu_times(idt) = real(idt-ONE_IXP,kind=RXP)*rg_dt

         enddo

         return
!***********************************************************************************************************************************************************************************
      end subroutine init_time_step
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine rounds down the mantissa of the scientific notation of a real(kind=RXP)number to .0 or .5
!>@return x : real(kind=RXP)number whose mantissa of the scientific notation is rounded down to .0 or .5
!***********************************************************************************************************************************************************************************
      subroutine round_time_step(x)
!***********************************************************************************************************************************************************************************

         real   (kind=RXP), intent(inout) :: x

         integer(kind=IXP)                :: e
         real   (kind=RXP)                :: m
         real   (kind=RXP), parameter     :: R = 10.0_RXP

!
!------->get the mantissa and exponent of a real number
         call manexp(x,m,e)

!
!------->round r*mantissa to .0 or .5
         if ( (R*m - int(R*m,kind=IXP)) >= 0.5_RXP ) then

            x = ((int(R*m,kind=IXP) + 0.5_RXP)/R)*10.0_RXP**(e)

         else

            x = ((int(R*m,kind=IXP))/R)*10.0_RXP**(e)

         endif

         return

!***********************************************************************************************************************************************************************************
      end subroutine round_time_step
!***********************************************************************************************************************************************************************************


!
!
!>@brief
!!This subroutine returns the mantissa and the exponent of the exponential notation of a real(kind=RXP)number
!>@return m : mantissa of the exponential notation of a real(kind=RXP)number x
!>@return e : exponent of the exponential notation of a real(kind=RXP)number x
!***********************************************************************************************************************************************************************************
      subroutine manexp(x,m,e)
!***********************************************************************************************************************************************************************************

         real   (kind=RXP), intent(in ) :: x
         integer(kind=IXP), intent(out) :: e
         real   (kind=RXP), intent(out) :: m
         
         real   (kind=RXP)              :: xx
         
         if (x < 0.0_RXP) then

            xx = -x
            e = int(log10(xx),kind=IXP)
            if (e > ZERO_IXP) e = e + ONE_IXP
            m = - xx * 10.0_RXP**(-e)

         elseif (x > 0.0_RXP) then

            e = int(log10(x),kind=IXP)
            if (e > ZERO_IXP) e = e + ONE_IXP      
            m = x * 10.0_RXP**(-e)

         else

            e = ZERO_RXP
            m = ZERO_RXP

         endif

         return

!***********************************************************************************************************************************************************************************
      end subroutine manexp
!***********************************************************************************************************************************************************************************

end module mod_init_time_step
