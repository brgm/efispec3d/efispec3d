//!>!===================================================================================================================================!<!
//!>!                                                        EFISPEC3D                                                                  !<!
//!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
//!>!                                                                                                                                   !<!
//!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
//!>!                                                                                                                                   !<!
//!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                                                 http://efispec.free.fr                                                            !<!
//!>!                                                                                                                                   !<!
//!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
//!>!                                                                                                                                   !<!
//!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
//!>!                                                                David    MICHEA                                                    !<!
//!>!                                                                Philippe THIERRY                                                   !<!
//!>!                                                                Sylvain  JUBERTIE                                                  !<!
//!>!                                                                Emmanuel CHALJUB                                                   !<!
//!>!                                                                Francois LAVOUE                                                    !<!
//!>!                                                                Tom      BUDON                                                     !<!
//!>!                                                                Emmanuel MELIN                                                     !<!
//!>!                                                                                                                                   !<!
//!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
//!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
//!>!                                                                                                                                   !<!
//!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
//!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
//!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
//!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
//!>!                           "http://www.cecill.info".                                                                               !<!
//!>!                                                                                                                                   !<!
//!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
//!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
//!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
//!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
//!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
//!>!                                                                                                                                   !<!
//!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
//!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
//!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
//!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
//!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
//!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
//!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
//!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
//!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
//!>!                           securite.                                                                                               !<!
//!>!                                                                                                                                   !<!
//!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
//!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
//!>!                                                                                                                                   !<!
//!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
//!>!                           motion using a finite spectral-element method.                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
//!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
//!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
//!>!                           version.                                                                                                !<!
//!>!                                                                                                                                   !<!
//!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
//!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
//!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
//!>!                                                                                                                                   !<!
//!>!                           You should have received a copy of the GNU General Public License along with                            !<!
//!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  3 ---> Thirdparty libraries                                                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
//!>!                                                                                                                                   !<!
//!>!                             --> METIS 5.1.0                                                                                       !<! 
//!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
//!>!                                                                                                                                   !<!
//!>!                             --> Lib_VTK_IO                                                                                        !<!
//!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
//!>!                                                                                                                                   !<!
//!>!                             --> INTERP_LINEAR                                                                                     !<!
//!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
//!>!                                                                                                                                   !<!
//!>!                             --> FLASProc                                                                                          !<!
//!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
//!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
//!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                             --> EXODUS II                                                                                         !<!
//!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
//!>!                                                                                                                                   !<!
//!>!                             --> NETCDF                                                                                            !<!
//!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                             --> HDF5                                                                                              !<!
//!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
//!>!                                                                                                                                   !<!
//!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
//!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
//!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
//!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
//!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
//!>!                           Computers & Structures, 245, 106459.                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
//!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
//!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
//!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
//!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
//!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
//!>!                                                                                                                                   !<!
//!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
//!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
//!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
//!>!                           Journal International, 201(1), 90-111.                                                                  !<!
//!>!                                                                                                                                   !<!
//!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
//!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
//!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
//!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
//!>!                                                                                                                                   !<!
//!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
//!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
//!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
//!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
//!>!                                                                                                                                   !<!
//!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
//!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
//!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
//!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
//!>!                           170(1), 43-64.                                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
//!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
//!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                  5 ---> Enjoy !                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!===================================================================================================================================!<!

/** \file partCubitMesh.c 
 *   \brief This file contains C subroutines to partition a mesh with (<a href="http://glaros.dtc.umn.edu/gkhome/metis/metis/overview" target="_blank">METIS-5.1.0 library</a>) 
 *          and to initialize GLL nodes global numbering. Mesh file formats surported: ABAQUS *.inp and EXODUS_II *.ex2.
 */

// Standard C include
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <ctype.h>

// my include 
#include "partCubitMesh.h"
#include "mpi.h"

#include "exodusII.h"


/********************************************************** FUNCTIONS DECLARATION **********************************************************/
int count_elt(FILE *unit);

void setHexaWeight(mesh_t *mesh);

void readCubitMeshAbaqus(char *fileinp, mesh_t *mesh, int *number_of_elemnt_block);

void readCubitMeshExodus(char *fileinp, mesh_t *mesh, int *number_of_elemnt_block);

void read_part_cubit_mesh_(char     *fileinp
                         , MPI_Fint *ig_mpi_comm_simu
                         , int      *ig_ncpu
                         , int      *ig_myrank
                         , int      *size_real_t
                         , int      *ig_mesh_nnode
                         , int      *ig_ncpu_neighbor
                         , int      *ig_nhexa
                         , int      *ig_nhexa_outer
                         , int      *ig_nquad_parax
                         , int      *ig_nquad_fsurf
                         , int      *ig_hexa_nnode
                         , int      *ig_quad_nnode
                         , float    *rg_mesh_xmax
                         , float    *rg_mesh_ymax
                         , float    *rg_mesh_zmax
                         , float    *rg_mesh_xmin
                         , float    *rg_mesh_ymin
                         , float    *rg_mesh_zmin);

int fill_mesh_arrays_(MPI_Fint *ig_mpi_comm_simu
                     ,int      *ig_myrank
                     ,int      *ig_ngll_total
                     ,int      *ig_nneighbor_all_kind
                     ,int      *cpu_neighbor
                     ,int      *ig_cpu_neighbor_info
                     ,int      *ig_hexa_gnode_glonum
                     ,int      *ig_quadp_gnode_glonum
                     ,int      *ig_quadf_gnode_glonum
                     ,int      *ig_hexa_gll_glonum
                     ,int      *ig_quadp_gll_glonum
                     ,int      *ig_quadf_gll_glonum
                     ,int      *ig_quadp_neighbor_hexa
                     ,int      *ig_quadp_neighbor_hexaface
                     ,int      *ig_quadf_neighbor_hexa
                     ,int      *ig_quadf_neighbor_hexaface
                     ,int      *ig_hexa_material_number);

void fill_efispec_arrays( info_t   *info
                        , mesh_t   *mesh
                        , MPI_Comm  mpi_comm_simu
                        , int     rank
                        , int    *ig_ngll_total
                        , int    *ig_nneighbor_all_kind
                        , int    *cpu_neighbor
                        , int    *ig_cpu_neighbor_info
                        , int    *ig_hexa_gnode_glonum
                        , int    *ig_quadp_gnode_glonum
                        , int    *ig_quadf_gnode_glonum
                        , int    *ig_hexa_gll_glonum
                        , int    *ig_quadp_gll_glonum
                        , int    *ig_quadf_gll_glonum
                        , int    *ig_quadp_neighbor_hexa
                        , int    *ig_quadp_neighbor_hexaface
                        , int    *ig_quadf_neighbor_hexa
                        , int    *ig_quadf_neighbor_hexaface
                        , int    *ig_hexa_material_number);

info_t *getConnInfo(mesh_t *mesh, MPI_Comm mpi_comm_simu, int rank, int *ig_mesh_nnode);

int getProcConn(int ielt, int iproc, proc_info_t *proc_tab, mesh_t *mesh);

void setEltConn(int ielt, int iproc, elt_info_t *elt_tab, mesh_t *mesh, proc_info_t *proc_tab);

void setHexaConnDetail(int elt_num, int neigh_num, int nb_conn, int *connex_nodes, elt_info_t *elt_tab, mesh_t *mesh);

void setQuadConnDetail(int elt_num, int neigh_num, int nb_conn, int *connex_nodes, elt_info_t *elt_tab, mesh_t *mesh);

int findFaceOrientation(int *connex_nodes, int num_conn_source, int num_conn_target);

int findEdgeOrientation(int *connex_nodes, int num_conn_source, int num_conn_target);

char *ltrim(char*s);
char *rtrim(char*s);
char *trim (char*s);

void printMemUsage(mesh_t *mesh);

char* rotateVect(char *ortn, char *ret_or, int cw_quarter_rot);

void add_conn(topo_t typecon, elt_info_t *elt_tab, int hexa_src, int numcon_src, idx_t num_neigh, int numcon_neigh, int orientation, elt_t type);

int getnbneigh(topo_t typecon, elt_info_t *elt_tab, int numcon_src);

void free_all_1();

void free_all_2();

void get_domain_min_max(mesh_t *mesh, float *rg_mesh_xmax, float *rg_mesh_ymax, float *rg_mesh_zmax, float *rg_mesh_xmin, float *rg_mesh_ymin, float *rg_mesh_zmin);

// Fortran routines
void init_gll_number(int*,int*);

void propagate_gll_nodes_face(int*, int*, int*, int*, int*);

void propagate_gll_nodes_edge(int*, int*, int*, int*, int*);

void propagate_gll_nodes_corner(int*, int*, int*, int*);

void propagate_gll_nodes_quad(int*, int*, int*, int*, int*);

void pass_address_back_(float *, float *, float *);

void allocate_node_coord_arrays_();

void flush()
{ 
   fflush(stdout);
   fflush(stderr);
}

/********************************************************** GLOBAL VARIABLES **********************************************************/
static mesh_t *mesh_ptr;
static info_t *info_ptr;

static int ncpu_neighbor, nhexa_local;
static int nquad_parax;
static int nquad_fsurf;
static int num_nodes_hexa;

float *xcoord;
float *ycoord;
float *zcoord;

/******************************************************** FUNCTIONS DEFINITION ********************************************************/
void pass_address_back_(float *rg_gnode_x, float *rg_gnode_y, float *rg_gnode_z)
{
   xcoord = rg_gnode_x;
   ycoord = rg_gnode_y;
   zcoord = rg_gnode_z;
}

void free_all_1()
{ 
   free(mesh_ptr->xadj);
   free(mesh_ptr->adjncy);
   free(mesh_ptr->xadj_hex);
   free(mesh_ptr->adjncy_hex);
   free(mesh_ptr->ncoords_x);
   free(mesh_ptr->ncoords_y);
   free(mesh_ptr->ncoords_z);
}

void free_all_2() // � modifier (cf 896 et avant)
{ 
   int iproc;
   for (iproc=0; iproc<mesh_ptr->npart; iproc++) {
      free(info_ptr->proc[iproc].local_elts);
      free(info_ptr->proc[iproc].connex);
   }
   free(info_ptr->proc);

   int nbelt = mesh_ptr->nh;
   for(int i=0; i< nbelt; i++) {
      elt_info_t elt = info_ptr->elt[i];

      for (int j=0; j<NEDGE; j++) {
         // conn_info* ptr = &elt->edges[j];
         conn_info* ptr = &elt.edges[j];
         if (ptr->defined) {
            conn_info* next;
            int first = 1;
            while(ptr)
            {   next = ptr->next;
               if (!first) free(ptr);
               ptr = next;
               first = 0;
            }
         }
      }
      for (int j=0; j<NCORNER; j++) {
         // conn_info* ptr = &elt->corners[j];
         conn_info* ptr = &elt.corners[j];
         if (ptr->defined) {
            conn_info* next;
            int first = 1;
            while(ptr)
            {   next = ptr->next;
               if (!first) free(ptr);
               ptr = next;
               first = 0;
            }
         }
      }
   }

   free(info_ptr->elt);
   free(info_ptr);

   free(mesh_ptr->eptr);
   free(mesh_ptr->eind);
   if (mesh_ptr->hex27) {
      free(mesh_ptr->eptr_27);
      free(mesh_ptr->eind_27);
   }
   free(mesh_ptr->part);
   free(mesh_ptr->layer);
   free(mesh_ptr->vwgt);
   free(mesh_ptr->types);

   free(mesh_ptr);
}

/******************************************************** FUNCTIONS *******************************************************************/
void read_part_cubit_mesh_( char     *prefix
                          , MPI_Fint *ig_mpi_comm_simu
                          , int      *ig_ncpu
                          , int      *ig_myrank
                          , int      *size_real_t
                          , int      *ig_mesh_nnode
                          , int      *ig_ncpu_neighbor
                          , int      *ig_nhexa
                          , int      *ig_nhexa_outer
                          , int      *ig_nquad_parax
                          , int      *ig_nquad_fsurf
                          , int      *ig_hexa_nnode
                          , int      *ig_quad_nnode
                          , float    *rg_mesh_xmax
                          , float    *rg_mesh_ymax
                          , float    *rg_mesh_zmax
                          , float    *rg_mesh_xmin
                          , float    *rg_mesh_ymin
                          , float    *rg_mesh_zmin)
{
 
   int rank, iproc;
   MPI_Comm mpi_comm_simu;
   mesh_t *mesh; // -> the mesh structure
   idx_t ncommon;
   idx_t pnumflag;
   idx_t edgecuts;
   float r_buf[6];
   int i_buf[8];
   int number_of_elemnt_block[N_BLOCK_MAX];
   MPI_Status status;
   char fileinp[100];

   mesh = MALLOC(mesh_t, 1);

   // input
   mesh->npart   = *ig_ncpu;
   rank          = *ig_myrank;
   mpi_comm_simu = MPI_Comm_f2c(*ig_mpi_comm_simu);

   if (rank == 0) {

      MSG("")
      // 93 because cg_prefix is a 92 car string + \0
      strncpy(fileinp, prefix, 93);
      strcat(fileinp, ".ex2");
      FILE * check = fopen(fileinp, "r");
      if (check) 
      {  
         fclose(check);
         MSG("Found exodusII binary mesh file")
         readCubitMeshExodus(fileinp, mesh, number_of_elemnt_block);
      }
      else
      {
         strncpy(fileinp, prefix, 93);
         strcat(fileinp, ".inp");
         check = fopen(fileinp, "r");
         if (check) {
            fclose(check);
            MSG("Found abaqus ascii mesh file")
            readCubitMeshAbaqus(fileinp, mesh, number_of_elemnt_block);
         } else {
            printf("\nMesh file not found (%s.ex2 or %s.inp)\naborting ...\n\n",prefix,prefix);
            MPI_Abort(mpi_comm_simu, 1);
         }
      }

#ifdef VERBOSE   
      MSG("")
      MSG("Mesh information :")
      printf("\t%d nodes\n",mesh->nn);
      printf("\t%d nodes per hexahedron\n",mesh->num_nodes_hexa);
      printf("\t%d hexahedron elements\n",mesh->nh);
      printf("\t%d quadrangle elements as boundary absorption\n",mesh->nq_parax);
      printf("\t%d quadrangle elements as free surface snapshot\n",mesh->nq_surf);
      MSG("")
      MSG("Computation started: see *.lst file for more information")
#endif

      get_domain_min_max(  mesh, rg_mesh_xmax, rg_mesh_ymax, rg_mesh_zmax, rg_mesh_xmin, rg_mesh_ymin, rg_mesh_zmin);

      r_buf[0] = *rg_mesh_xmax;
      r_buf[1] = *rg_mesh_ymax;
      r_buf[2] = *rg_mesh_zmax;
      r_buf[3] = *rg_mesh_xmin;
      r_buf[4] = *rg_mesh_ymin;
      r_buf[5] = *rg_mesh_zmin;

      MPI_Bcast( r_buf, 6, MPI_FLOAT, 0, mpi_comm_simu);

   } else {

      MPI_Bcast( r_buf, 6, MPI_FLOAT, 0, mpi_comm_simu);

      *rg_mesh_xmax = r_buf[0];
      *rg_mesh_ymax = r_buf[1];
      *rg_mesh_zmax = r_buf[2];
      *rg_mesh_xmin = r_buf[3];
      *rg_mesh_ymin = r_buf[4];
      *rg_mesh_zmin = r_buf[5];

   }

   if (rank == 0) {

      //MSG("Build complete adjacency graph with 4 nodes connections")
      ncommon  = 4;
      pnumflag = 0;
      METIS_MeshToDual(&mesh->ne, &mesh->nn, mesh->eptr, mesh->eind, &ncommon, &pnumflag, &mesh->xadj, &mesh->adjncy);

      //MSG("Build hexahedron adjacency graph with 1 node connection")
           ncommon = 1;
      METIS_MeshToDual(&mesh->nh, &mesh->nn, mesh->eptr, mesh->eind, &ncommon, &pnumflag, &mesh->xadj_hex, &mesh->adjncy_hex);

      //MSG("Set weight to hexa")
         setHexaWeight(mesh);

      //MSG("Part mesh using Metis")

         if (mesh->npart > 1) {

            METIS_PartGraphRecursive(&mesh->nh, &mesh->ncon, mesh->xadj_hex, mesh->adjncy_hex, mesh->vwgt, NULL, NULL, &mesh->npart, NULL, NULL, NULL, &edgecuts, mesh->part);
         // METIS_PartGraphKway     (&mesh->nh, &mesh->ncon, mesh->xadj_hex, mesh->adjncy_hex, mesh->vwgt, NULL, NULL, &mesh->npart, NULL, NULL, NULL, &edgecuts, mesh->part);
            int numelem = 0;

            for (int hh=0; hh<mesh->nh; hh++) {
               if (mesh->part[hh] == rank) numelem++;
            }

         } else {

            memset(mesh->part, 0, mesh->nh*sizeof(idx_t));

         }

      //MSG("Get complete connectivity information")

      // rg_[xyz]_coord_geom_nodes allocated and filled in getConnInfo()
      // allocation coord_geom_nodes -> peut-etre redecouper ici !!!
      info_t* info = getConnInfo(mesh, mpi_comm_simu, rank, ig_mesh_nnode);

      for (iproc = 0; iproc < mesh->npart; iproc++) {

         if (iproc == 0) {

            //sizes 4 efispec
            *size_real_t      = sizeof(real_t);
            *ig_nhexa         = info->proc[iproc].nb_elt;
            *ig_nhexa_outer   = info->proc[iproc].nb_ext;
            *ig_nquad_parax   = info->proc[iproc].nb_quad_p;
            *ig_nquad_fsurf   = info->proc[iproc].nb_quad_f;
            *ig_hexa_nnode    = mesh->num_nodes_hexa;
            *ig_ncpu_neighbor = info->proc[iproc].nb_conn;
            *ig_quad_nnode    = mesh->num_node_per_dim*mesh->num_node_per_dim;

         } else { 

            i_buf[0] = sizeof(real_t);  
            i_buf[1] = info->proc[iproc].nb_elt;  
            i_buf[2] = info->proc[iproc].nb_ext;  
            i_buf[3] = info->proc[iproc].nb_quad_p; 
            i_buf[4] = info->proc[iproc].nb_quad_f; 
            i_buf[5] = mesh->num_nodes_hexa;  
            i_buf[6] = info->proc[iproc].nb_conn; 
            i_buf[7] = mesh->num_node_per_dim*mesh->num_node_per_dim;

            MPI_Send(i_buf, 8, MPI_INT, iproc, 5, mpi_comm_simu);

         }
      }
      // DAVID : statics ptr for memory freeing
      mesh_ptr = mesh;
      info_ptr = info;

   } else {

      getConnInfo(mesh, mpi_comm_simu, rank, ig_mesh_nnode);

      MPI_Recv(i_buf, 8, MPI_INT, 0, 5, mpi_comm_simu, &status);

      *size_real_t      = i_buf[0];
      *ig_nhexa         = i_buf[1];
      *ig_nhexa_outer   = i_buf[2];
      *ig_nquad_parax   = i_buf[3];
      *ig_nquad_fsurf   = i_buf[4];
      *ig_hexa_nnode    = i_buf[5];
      *ig_ncpu_neighbor = i_buf[6];
      *ig_quad_nnode    = i_buf[7];

      mesh_ptr = mesh;

   }

   // saving these values for later
   ncpu_neighbor  = *ig_ncpu_neighbor;
   nhexa_local    = *ig_nhexa;
   nquad_parax    = *ig_nquad_parax;
   nquad_fsurf    = *ig_nquad_fsurf;
   num_nodes_hexa = *ig_hexa_nnode;


   // free what's not needed anymore to avoid a memory consumption pick as much as possible
   if (rank == 0) free_all_1();

   return;
}

void get_domain_min_max(mesh_t* mesh, float* rg_mesh_xmax, float* rg_mesh_ymax, float* rg_mesh_zmax, float* rg_mesh_xmin, float* rg_mesh_ymin, float* rg_mesh_zmin)
{

   float xmin, ymin, zmin;
   float xmax, ymax, zmax;

   xmin = ymin = zmin = +FLT_MAX;
   xmax = ymax = zmax = -FLT_MAX;

   for (int node=0; node<mesh->nn; node++) {

      if (mesh->ncoords_x[node] < xmin) xmin = mesh->ncoords_x[node];
      if (mesh->ncoords_y[node] < ymin) ymin = mesh->ncoords_y[node];
      if (mesh->ncoords_z[node] < zmin) zmin = mesh->ncoords_z[node];

      if (mesh->ncoords_x[node] > xmax) xmax = mesh->ncoords_x[node];
      if (mesh->ncoords_y[node] > ymax) ymax = mesh->ncoords_y[node];
      if (mesh->ncoords_z[node] > zmax) zmax = mesh->ncoords_z[node];

   }

   *rg_mesh_xmax = xmax;
   *rg_mesh_ymax = ymax;
   *rg_mesh_zmax = zmax;

   *rg_mesh_xmin = xmin;
   *rg_mesh_ymin = ymin;
   *rg_mesh_zmin = zmin;

}

int fill_mesh_arrays_(MPI_Fint *ig_mpi_comm_simu,
                      int      *rank_,
                      int      *ig_ngll_total,
                      int      *ig_nneighbor_all_kind,
                      int      *cpu_neighbor,
                      int      *ig_cpu_neighbor_info,
                      int      *ig_hexa_gnode_glonum, 
                      int      *ig_quadp_gnode_glonum,
                      int      *ig_quadf_gnode_glonum, 
                      int      *ig_hexa_gll_glonum,
                      int      *ig_quadp_gll_glonum,
                      int      *ig_quadf_gll_glonum, 
                      int      *ig_quadp_neighbor_hexa,
                      int      *ig_quadp_neighbor_hexaface,
                      int      *ig_quadf_neighbor_hexa,
                      int      *ig_quadf_neighbor_hexaface,
                      int      *ig_hexa_material_number)
{ 

   mesh_t *mesh = mesh_ptr;
   info_t *info = info_ptr;

   MPI_Comm mpi_comm_simu = MPI_Comm_f2c(*ig_mpi_comm_simu);
   
   fill_efispec_arrays( info
                      , mesh
                      , mpi_comm_simu
                      ,*rank_
                      , ig_ngll_total
                      , ig_nneighbor_all_kind
                      , cpu_neighbor
                      , ig_cpu_neighbor_info
                      , ig_hexa_gnode_glonum
                      , ig_quadp_gnode_glonum
                      , ig_quadf_gnode_glonum
                      , ig_hexa_gll_glonum
                      , ig_quadp_gll_glonum
                      , ig_quadf_gll_glonum
                      , ig_quadp_neighbor_hexa
                      , ig_quadp_neighbor_hexaface
                      , ig_quadf_neighbor_hexa
                      , ig_quadf_neighbor_hexaface
                      , ig_hexa_material_number);

   if (*rank_ == 0) free_all_2();
   else free(mesh_ptr);
   
   //if (*rank_ == 0) MSG("Mesh partition done.")

   return 0;

}

// write domain files for each proc of efispec3D
void fill_efispec_arrays( info_t   *info
                        , mesh_t   *mesh
                        , MPI_Comm  mpi_comm_simu
                        , int       rank
                        , int      *ig_ngll_total
                        , int      *ig_nneighbor_all_kind
                        , int      *cpu_neighbor
                        , int      *ig_cpu_neighbor_info //*
                        , int      *ig_hexa_gnode_glonum //*
                        , int      *ig_quadp_gnode_glonum
                        , int      *ig_quadf_gnode_glonum
                        , int      *ig_hexa_gll_glonum
                        , int      *ig_quadp_gll_glonum
                        , int      *ig_quadf_gll_glonum
                        , int      *ig_quadp_neighbor_hexa //*
                        , int      *ig_quadp_neighbor_hexaface //*
                        , int      *ig_quadf_neighbor_hexa //*
                        , int      *ig_quadf_neighbor_hexaface //*
                        , int      *ig_hexa_material_number) //*
{
 
   int iproc, ineigh, ielt, i, j, k;
   int *i_buf, *ibuf_hexa_material_number, *ibuf_hexa_gnode_glonum;

   proc_info_t *proc_tab;
   elt_info_t  *elt_tab;

   int *nneighbor_all_kind;
   int *cpu_neighbor_info;
   int *ibuf_quadp_neighbor_hexa;
   int *ibuf_quadp_neighbor_hexaface;
   int *ibuf_quadf_neighbor_hexa;
   int *ibuf_quadf_neighbor_hexaface;
   int ibuf2[2];

   MPI_Status status;
   int icpu, iparax, ifsurf;

   idx_t *p_eptr, *p_eind;

   if (mesh->hex27) {

      p_eptr = mesh->eptr_27;
      p_eind = mesh->eind_27;

   } else {

      p_eptr = mesh->eptr;
      p_eind = mesh->eind;

   }

   if (rank == 0) {

      proc_tab = info->proc;
      elt_tab  = info->elt;

      for(iproc = 0; iproc<mesh->npart; iproc++) {
         if (iproc == 0) {
            icpu=0;//nb connected proc
            for(ineigh = 0; ineigh<mesh->npart; ineigh++) {
               if (proc_tab[iproc].connex[ineigh]) cpu_neighbor[icpu++] = ineigh; // list of connected proc
            }
         } else {
            // envoi
            i_buf = MALLOC(int, proc_tab[iproc].nb_conn);
            icpu=0;//nb connected proc
            for(ineigh = 0; ineigh<mesh->npart; ineigh++) {
               if (proc_tab[iproc].connex[ineigh]) i_buf[icpu++] = ineigh; // list of connected proc
            }
            MPI_Send(i_buf, proc_tab[iproc].nb_conn, MPI_INT, iproc, 10, mpi_comm_simu);
            free(i_buf);
         }
      }

   } else {

      MPI_Recv(cpu_neighbor, ncpu_neighbor, MPI_INT, 0, 10, mpi_comm_simu, &status);

   }

   for(iproc = 0; iproc<mesh->npart; iproc++) {

      int nneighbor_all_kind = 0;
      int nb_items = 0;

      if (rank == 0) {

         if (iproc != 0) {

            ibuf_hexa_material_number = MALLOC(int, proc_tab[iproc].nb_elt);
            ibuf_hexa_gnode_glonum = MALLOC(int, proc_tab[iproc].nb_elt*mesh->num_nodes_hexa);

            ibuf_quadp_neighbor_hexa = MALLOC(int, proc_tab[iproc].nb_quad_p);
            ibuf_quadp_neighbor_hexaface = MALLOC(int, proc_tab[iproc].nb_quad_p);

            ibuf_quadf_neighbor_hexa = MALLOC(int, proc_tab[iproc].nb_quad_f);
            ibuf_quadf_neighbor_hexaface = MALLOC(int, proc_tab[iproc].nb_quad_f);

            i_buf = MALLOC(int, 7*26*info->proc[iproc].nb_elt); // 1 type + 5 params
            cpu_neighbor_info = MALLOC(int, 3*26*info->proc[iproc].nb_ext);

         }

         iparax = 0;
         ifsurf = 0;

         for (ielt=0; ielt<proc_tab[iproc].nb_elt; ielt++) {// for each hexa of proc iproc

            int ihexa = ielt+1;
            int ielt_num, ielt_number, ielt_face, ielt_edge, ielt_corner, ielt_coty, ielt_cpu;

            elt_info_t* curhex = proc_tab[iproc].local_elts[ielt];
            int globnum = curhex->globalnum;

            // materiau
            if (iproc != 0) ibuf_hexa_material_number[ielt] = mesh->layer[globnum];
            else      ig_hexa_material_number[ielt] = mesh->layer[globnum];

            // geometry
            for (i=0; i<mesh->num_nodes_hexa; i++) {// geom coords of hexa geom nodes

               // use efispec node order
               // ig_hexa_gnode_glonum(inode,ihexa)
               if (p_eind[p_eptr[globnum]+corner2efispec[i]-1] == 0) {
                  STOP("num node should not be 0 (fortran arrays start at 1)");
               }

               if (iproc != 0) ibuf_hexa_gnode_glonum[ielt * mesh->num_nodes_hexa + i] = p_eind[p_eptr[globnum]+corner2efispec[i]-1]; // numerotation starts from 1
               else      ig_hexa_gnode_glonum[ielt * mesh->num_nodes_hexa + i] = p_eind[p_eptr[globnum]+corner2efispec[i]-1]; // numerotation starts from 1

            }

            if (iproc == 0) init_gll_number(&ihexa,ig_ngll_total);

            // faces
            for (i=0; i<NFACE; i++) {

               int iface = i+1;
               // 6 faces connectivity
               int type = curhex->faces[i].type;

               switch (type) {

                  case HEXA :   // hexa

                     ielt_num = elt_tab[curhex->faces[i].num_neigh].localnum + 1; // num commence � 1
                     ielt_face = face2efispec[curhex->faces[i].num_conn];
                     ielt_coty = curhex->faces[i].orientation;
                     ielt_cpu = mesh->part[curhex->faces[i].num_neigh];

                     if (ielt_cpu == iproc) {

                        if (iproc == 0) propagate_gll_nodes_face(&ihexa, &iface, &ielt_num, &ielt_face, &ielt_coty);
                        else {
                           i_buf[nb_items*7] = FACE*10+HEXA;
                           i_buf[nb_items*7+1] = ihexa;
                           i_buf[nb_items*7+2] = iface;
                           i_buf[nb_items*7+3] = ielt_num;
                           i_buf[nb_items*7+4] = ielt_face;
                           i_buf[nb_items*7+5] = ielt_coty;
                           i_buf[nb_items*7+6] = FACE;
                           nb_items++;
                        }

                     } else {

                        if (iproc == 0) {

                           if (*ig_nneighbor_all_kind >= 26*info->proc[iproc].nb_ext) STOP("ig_nneighbor_all_kind too large\n");
                           ig_cpu_neighbor_info[*ig_nneighbor_all_kind*3 + 0] = ihexa;
                           ig_cpu_neighbor_info[*ig_nneighbor_all_kind*3 + 1] = iface;
                           ig_cpu_neighbor_info[*ig_nneighbor_all_kind*3 + 2] = ielt_cpu;
                           (*ig_nneighbor_all_kind)++;

                        } else {

                           if (nneighbor_all_kind >= 26*info->proc[iproc].nb_ext) STOP("nneighbor_all_kind too large\n");
                           cpu_neighbor_info[nneighbor_all_kind*3 + 0] = ihexa;
                           cpu_neighbor_info[nneighbor_all_kind*3 + 1] = iface;
                           cpu_neighbor_info[nneighbor_all_kind*3 + 2] = ielt_cpu;
                           nneighbor_all_kind++;

                        }
                     }
                     break;

                  case QUAD_P :   // quad p

                     if (iproc == 0) {

                        ig_quadp_neighbor_hexa[iparax] = ihexa;
                        ig_quadp_neighbor_hexaface[iparax] = iface;

                     } else {

                        ibuf_quadp_neighbor_hexa[iparax] = ihexa;
                        ibuf_quadp_neighbor_hexaface[iparax] = iface;

                     }

                     if (iproc == 0) {

                        if (mesh->num_node_per_dim*mesh->num_node_per_dim == 4) {

                           switch(iface) {

                              case 1 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+0];
                                  ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+1];
                                  ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+2];
                                  ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+3];
                                  break;

                              case 2 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+0];
                                  ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+4];
                                  ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+5];
                                  ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+1];
                                  break;

                              case 3 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+1];
                                  ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+5];
                                  ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+6];
                                  ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+2];
                                  break;

                              case 4 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+3];
                                  ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+2];
                                  ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+6];
                                  ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+7];
                                  break;

                              case 5 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+0];
                                  ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+3];
                                  ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+7];
                                  ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+4];
                                  break;

                              case 6 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+4];
                                  ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+7];
                                  ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+6];
                                  ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+5];
                                  break;

                           }

                        } else {

                           STOP("not yet ready for 27 nodes elements\n");

                        }

                     }

                     iparax++;
                     //!!! attention iparax vaut +1 � partir d'ici

                     if (iproc == 0)  propagate_gll_nodes_quad(&ihexa, &iface, &iparax, ig_quadp_gll_glonum, &info->proc[iproc].nb_quad_p);
                     else {
                        i_buf[nb_items*7] = FACE*10+QUAD_P;
                        i_buf[nb_items*7+1] = ihexa;
                        i_buf[nb_items*7+2] = iface;
                        i_buf[nb_items*7+3] = iparax;
                        i_buf[nb_items*7+4] = 0;
                        i_buf[nb_items*7+5] = info->proc[iproc].nb_quad_p;
                        i_buf[nb_items*7+6] = FACE;
                        nb_items++;
                        // � la construction de ig_quadp_gnode_glonum pour les autres proc, il faudra prendre ihexa-1 et iparax-1 !!!
                     }
                     break;

                  case QUAD_F :   // quad f

                     if (iproc == 0) {

                        ig_quadf_neighbor_hexa[ifsurf] = ihexa;
                        ig_quadf_neighbor_hexaface[ifsurf] = iface;

                     } else {

                        ibuf_quadf_neighbor_hexa[ifsurf] = ihexa;
                        ibuf_quadf_neighbor_hexaface[ifsurf] = iface;

                     }

                     if (iproc == 0) {

                        if (mesh->num_node_per_dim*mesh->num_node_per_dim == 4) {   

                           switch(iface) {

                              case 1 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+0];
                                  ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+1];
                                  ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+2];
                                  ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+3];
                                  break;                                                                              

                              case 2 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+0];
                                  ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+4];
                                  ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+5];
                                  ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+1];
                                  break;                                                                              

                              case 3 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+1];
                                  ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+5];
                                  ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+6];
                                  ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+2];
                                  break;                                                                              

                              case 4 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+3];
                                  ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+2];
                                  ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+6];
                                  ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+7];
                                  break;                                                                              

                              case 5 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+0];
                                  ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+3];
                                  ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+7];
                                  ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+4];
                                  break;                                                                              

                              case 6 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+4];
                                  ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+7];
                                  ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+6];
                                  ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*mesh->num_nodes_hexa+5];
                                  break;

                           }

                        } else {

                           STOP("not yet ready for 27 nodes elements\n");

                        }
                     }

                     ifsurf++;
                     //!!! attention ifsurf vaut +1 � partir d'ici

                     if (iproc == 0)  propagate_gll_nodes_quad(&ihexa, &iface, &ifsurf, ig_quadf_gll_glonum, &info->proc->nb_quad_f);
                     else {
                        i_buf[nb_items*7] = FACE*10+QUAD_F;
                        i_buf[nb_items*7+1] = ihexa;
                        i_buf[nb_items*7+2] = iface;
                        i_buf[nb_items*7+3] = ifsurf;
                        i_buf[nb_items*7+4] = 0;
                        i_buf[nb_items*7+5] = info->proc[iproc].nb_quad_f;
                        i_buf[nb_items*7+6] = FACE;
                        nb_items++;
                        // � la construction de ig_quadf_gnode_glonum pour les autres proc, il faudra prendre ihexa-1 et ifsurf-1 !!!
                     }
                     break;
               }
            }

            // edges
            for (i=0; i<NEDGE; i++) {  // 12 edges connectivity

               int iedge       = i+1;
               conn_info* ptr  = &curhex->edges[i];
               int nb_neighbor = getnbneigh(EDGE, curhex, i);

               for (int ineigh = 0; ineigh < nb_neighbor; ineigh++) {

                  switch(ptr->type) {

                     case HEXA :    ielt_number = elt_tab[ptr->num_neigh].localnum + 1;   // start from 1
                               ielt_edge   = edge2efispec[ptr->num_conn];            // num edge of neighbor
                               ielt_coty   = ptr->orientation;                       // orientation
                               ielt_cpu    = mesh->part[ptr->num_neigh];             // proc of neighbour

                               if (ielt_cpu == iproc) {

                                  if (ielt_number > ihexa) {

                                     if (iproc == 0 ) {

                                        propagate_gll_nodes_edge(&ihexa, &iedge, &ielt_number, &ielt_edge, &ielt_coty);

                                     } else {

                                        i_buf[nb_items*7] = EDGE*10+HEXA;
                                        i_buf[nb_items*7+1] = ihexa;
                                        i_buf[nb_items*7+2] = iedge;
                                        i_buf[nb_items*7+3] = ielt_number;
                                        i_buf[nb_items*7+4] = ielt_edge;
                                        i_buf[nb_items*7+5] = ielt_coty;
                                        i_buf[nb_items*7+6] = nb_neighbor;
                                        nb_items++;

                                     }
                                  }

                               } else {

                                  if (iproc == 0) {

                                     ig_cpu_neighbor_info[*ig_nneighbor_all_kind*3 + 0] = ihexa;
                                     ig_cpu_neighbor_info[*ig_nneighbor_all_kind*3 + 1] = NFACE + iedge;
                                     ig_cpu_neighbor_info[*ig_nneighbor_all_kind*3 + 2] = ielt_cpu;
                                     (*ig_nneighbor_all_kind)++;

                                  } else {

                                     cpu_neighbor_info[nneighbor_all_kind*3 + 0] = ihexa;
                                     cpu_neighbor_info[nneighbor_all_kind*3 + 1] = NFACE + iedge;
                                     cpu_neighbor_info[nneighbor_all_kind*3 + 2] = ielt_cpu;
                                     nneighbor_all_kind++;

                                  }
                               }

                     case NONE : break;

                     default   : STOP("edge neighbor should be an hexa");

                  }
                  ptr = ptr->next;
               }
            }

            // corner

            for (j=0; j<NCORNER; j++) { // 8 corners connectivity

               int inode       = j+1;
               i               = cornerefispec2cubit[j]; // written in efispec order
               conn_info* ptr  = &curhex->corners[i];
               int nb_neighbor = getnbneigh(CORNER, curhex, i); // nb of neighbors connected to corner i

               for (k=0; k<nb_neighbor; k++) {

                  switch(ptr->type) {

                     case HEXA :    ielt_number = elt_tab[ptr->num_neigh].localnum + 1;   // start from 1
                               ielt_corner = corner2efispec[ptr->num_conn];          // num edge of neighbor
                               ielt_cpu    = mesh->part[ptr->num_neigh];             // proc of neighbour

                               if (ielt_cpu == iproc) {

                                  if (ielt_number > ihexa) {

                                     if (iproc == 0 ) {

                                        propagate_gll_nodes_corner(&ihexa, &inode, &ielt_number, &ielt_corner);

                                     } else {

                                        i_buf[nb_items*7] = CORNER*10+HEXA;
                                        i_buf[nb_items*7+1] = ihexa;
                                        i_buf[nb_items*7+2] = inode;
                                        i_buf[nb_items*7+3] = ielt_number;
                                        i_buf[nb_items*7+4] = ielt_corner;
                                        i_buf[nb_items*7+5] = 0;
                                        i_buf[nb_items*7+6] = nb_neighbor;
                                        nb_items++;

                                     }
                                  }

                               } else {

                                  if (iproc == 0) {

                                     ig_cpu_neighbor_info[*ig_nneighbor_all_kind*3 + 0] = ihexa;
                                     ig_cpu_neighbor_info[*ig_nneighbor_all_kind*3 + 1] = NFACE + NEDGE + inode;
                                     ig_cpu_neighbor_info[*ig_nneighbor_all_kind*3 + 2] = ielt_cpu;
                                     (*ig_nneighbor_all_kind)++;

                                  } else {

                                     cpu_neighbor_info[nneighbor_all_kind*3 + 0] = ihexa;
                                     cpu_neighbor_info[nneighbor_all_kind*3 + 1] = NFACE + NEDGE + inode;
                                     cpu_neighbor_info[nneighbor_all_kind*3 + 2] = ielt_cpu;
                                     nneighbor_all_kind++;

                                  }
                               }

                     case NONE : break;

                     default   : STOP("corner neighbor should be an hexa");

                  }
                  ptr = ptr->next;
               }
            }

         }

         if (iparax != info->proc[iproc].nb_quad_p || ifsurf != info->proc[iproc].nb_quad_f) STOP("problem with quad number");

         if (iproc != 0) {

            MPI_Send(ibuf_hexa_material_number, proc_tab[iproc].nb_elt, MPI_INT, iproc, 21, mpi_comm_simu);
            MPI_Send(ibuf_hexa_gnode_glonum, proc_tab[iproc].nb_elt*mesh->num_nodes_hexa, MPI_INT, iproc, 22, mpi_comm_simu);
            MPI_Send(ibuf_quadp_neighbor_hexa, proc_tab[iproc].nb_quad_p, MPI_INT, iproc, 23, mpi_comm_simu);
            MPI_Send(ibuf_quadp_neighbor_hexaface, proc_tab[iproc].nb_quad_p, MPI_INT, iproc, 24, mpi_comm_simu);
            MPI_Send(ibuf_quadf_neighbor_hexa, proc_tab[iproc].nb_quad_f, MPI_INT, iproc, 25, mpi_comm_simu);
            MPI_Send(ibuf_quadf_neighbor_hexaface, proc_tab[iproc].nb_quad_f, MPI_INT, iproc, 26, mpi_comm_simu);

            ibuf2[0] = nb_items; 
            ibuf2[1] = nneighbor_all_kind;
            MPI_Send(ibuf2, 2, MPI_INT, iproc, 25, mpi_comm_simu);

            MPI_Send(i_buf, nb_items*7, MPI_INT, iproc, 26, mpi_comm_simu);
            MPI_Send(cpu_neighbor_info, 3*nneighbor_all_kind, MPI_INT, iproc, 27, mpi_comm_simu);

            free(ibuf_hexa_material_number);
            free(ibuf_hexa_gnode_glonum);
            free(ibuf_quadp_neighbor_hexa);
            free(ibuf_quadp_neighbor_hexaface);
            free(ibuf_quadf_neighbor_hexa);
            free(ibuf_quadf_neighbor_hexaface);
            free(i_buf);
            free(cpu_neighbor_info);
         }

      } else {

         if (rank == iproc) {

            i_buf = MALLOC(int, 6*26*nhexa_local);
            MPI_Recv(ig_hexa_material_number, nhexa_local, MPI_INT, 0, 21, mpi_comm_simu, &status);
            MPI_Recv(ig_hexa_gnode_glonum, nhexa_local*num_nodes_hexa, MPI_INT, 0, 22, mpi_comm_simu, &status);
            MPI_Recv(ig_quadp_neighbor_hexa, nquad_parax, MPI_INT, 0, 23, mpi_comm_simu, &status);
            MPI_Recv(ig_quadp_neighbor_hexaface, nquad_parax, MPI_INT, 0, 24, mpi_comm_simu, &status);
            MPI_Recv(ig_quadf_neighbor_hexa, nquad_fsurf, MPI_INT, 0, 25, mpi_comm_simu, &status);
            MPI_Recv(ig_quadf_neighbor_hexaface, nquad_fsurf, MPI_INT, 0, 26, mpi_comm_simu, &status);

            MPI_Recv(ibuf2, 2, MPI_INT, 0, 25, mpi_comm_simu, &status);
            nb_items = ibuf2[0]; 
            *ig_nneighbor_all_kind = nneighbor_all_kind = ibuf2[1];

            MPI_Recv(i_buf, nb_items*7, MPI_INT, 0, 26, mpi_comm_simu, &status);
            MPI_Recv(ig_cpu_neighbor_info, 3*nneighbor_all_kind, MPI_INT, 0, 27, mpi_comm_simu, &status);

            int iibuf = 0;

            for (ielt=0; ielt < nhexa_local; ielt++) {

               int ihexa = ielt+1;
               init_gll_number(&ihexa,ig_ngll_total);

               // besoin de savoir combien d'items -> tests avec ihexa & iface
               for (i=0; i<NFACE; i++) { // a rev�rifier ...

                  int iface = i+1;

                  if (i_buf[iibuf*7+6] == FACE && i_buf[iibuf*7+1] == ihexa && i_buf[iibuf*7+2] == iface) {

                     int type = i_buf[iibuf*7];

                     switch (type) {

                        case FACE*10+HEXA :   propagate_gll_nodes_face(&i_buf[iibuf*7+1], &i_buf[iibuf*7+2], &i_buf[iibuf*7+3], &i_buf[iibuf*7+4], &i_buf[iibuf*7+5]);
                                    iibuf++;
                                    break;

                        case FACE*10+QUAD_P : iparax = i_buf[iibuf*7+3]-1;
                                    assert(i_buf[iibuf*7+1]-1 == ielt);
                                    assert(i_buf[iibuf*7+2] == iface);

                                    switch(iface) {

                                       case 1 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+0];
                                           ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+1];
                                           ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+2];
                                           ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+3];
                                           break;

                                       case 2 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+0];
                                           ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+4];
                                           ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+5];
                                           ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+1];
                                           break;

                                       case 3 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+1];
                                           ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+5];
                                           ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+6];
                                           ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+2];
                                           break;

                                       case 4 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+3];
                                           ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+2];
                                           ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+6];
                                           ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+7];
                                           break;

                                       case 5 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+0];
                                           ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+3];
                                           ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+7];
                                           ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+4];
                                           break;

                                       case 6 : ig_quadp_gnode_glonum[iparax*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+4];
                                           ig_quadp_gnode_glonum[iparax*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+7];
                                           ig_quadp_gnode_glonum[iparax*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+6];
                                           ig_quadp_gnode_glonum[iparax*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+5];
                                           break;

                                    }

                                    propagate_gll_nodes_quad(&i_buf[iibuf*7+1], &i_buf[iibuf*7+2], &i_buf[iibuf*7+3], ig_quadp_gll_glonum, &i_buf[iibuf*7+5]);
                                    iibuf++;
                                    break;

                        case FACE*10+QUAD_F : ifsurf = i_buf[iibuf*7+3]-1;
                                    assert(i_buf[iibuf*7+1]-1 == ielt);
                                    assert(i_buf[iibuf*7+2] == iface);

                                    switch(iface) {

                                       case 1 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+0];
                                           ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+1];
                                           ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+2];
                                           ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+3];
                                           break;                                                                        

                                       case 2 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+0];
                                           ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+4];
                                           ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+5];
                                           ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+1];
                                           break;                                                                        

                                       case 3 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+1];
                                           ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+5];
                                           ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+6];
                                           ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+2];
                                           break;                                                                        

                                       case 4 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+3];
                                           ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+2];
                                           ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+6];
                                           ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+7];
                                           break;                                                                        

                                       case 5 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+0];
                                           ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+3];
                                           ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+7];
                                           ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+4];
                                           break;                                                                        

                                       case 6 : ig_quadf_gnode_glonum[ifsurf*4+0] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+4];
                                           ig_quadf_gnode_glonum[ifsurf*4+1] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+7];
                                           ig_quadf_gnode_glonum[ifsurf*4+2] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+6];
                                           ig_quadf_gnode_glonum[ifsurf*4+3] = ig_hexa_gnode_glonum[ielt*num_nodes_hexa+5];
                                           break;
                                    }

                                    propagate_gll_nodes_quad(&i_buf[iibuf*7+1], &i_buf[iibuf*7+2], &i_buf[iibuf*7+3], ig_quadf_gll_glonum, &i_buf[iibuf*7+5]);
                                    iibuf++;
                                    break;

                     }
                  }
               }

               for (i=0; i<NEDGE; i++) {

                  int iedge = i+1;

                  if (i_buf[iibuf*7] == EDGE*10+HEXA && i_buf[iibuf*7+1] == ihexa && i_buf[iibuf*7+2] == iedge) {

                     int nb_neighbor = i_buf[iibuf*7+6];

                     for (int ineigh = 0; ineigh < nb_neighbor; ineigh++) {

                        if (i_buf[iibuf*7] == EDGE*10+HEXA && i_buf[iibuf*7+1] == ihexa && i_buf[iibuf*7+2] == iedge) {
                           propagate_gll_nodes_edge(&i_buf[iibuf*7+1], &i_buf[iibuf*7+2], &i_buf[iibuf*7+3], &i_buf[iibuf*7+4], &i_buf[iibuf*7+5]);
                           iibuf++;
                        }

                     }
                  }
               }

               for (j=0; j<NCORNER; j++) {

                  int inode = j+1;

                  if (i_buf[iibuf*7] == CORNER*10+HEXA && i_buf[iibuf*7+1] == ihexa && i_buf[iibuf*7+2] == inode) {

                     int nb_neighbor = i_buf[iibuf*7+6];

                     for (int ineigh = 0; ineigh < nb_neighbor; ineigh++) {

                        if (i_buf[iibuf*7] == CORNER*10+HEXA && i_buf[iibuf*7+1] == ihexa && i_buf[iibuf*7+2] == inode) {
                           propagate_gll_nodes_corner(&i_buf[iibuf*7+1], &i_buf[iibuf*7+2], &i_buf[iibuf*7+3], &i_buf[iibuf*7+4]);
                           iibuf++;
                        }

                     }
                  }
               }
            }
            free(i_buf);
            assert(iibuf == nb_items);
         }
      } 
   } // endfor iproc

   return;
}


// get all topological informations on all elements. return a filled info_t struct
// write geom nodes in files and overwrite mesh->eind with local node num
info_t* getConnInfo(mesh_t* mesh, MPI_Comm mpi_comm_simu, int rank, int* ig_mesh_nnode)
{ 
   int inode, ielt, iproc, inodeptr;
   idx_t* nodeMask;
   MPI_Status status;
   int nb_local_nodes;
   proc_info_t* proc_tab; 
   elt_info_t * elt_tab;
   info_t     * info;

   if (rank == 0) {   

      // data structure allocation
      proc_tab = MALLOC(proc_info_t, mesh->npart);       // DAVID2FREE
      elt_tab  = MALLOC(elt_info_t, mesh->nh); // DAVID2FREE
      info     = MALLOC(info_t, 1);            // DAVID2FREE
      info->proc            = proc_tab;
      info->elt             = elt_tab;

      for(iproc = 0; iproc<mesh->npart; iproc++) {

         // printf("proc num %d\n", iproc);
         proc_tab[iproc].connex = MALLOC(char, mesh->npart); // DAVID2FREE
         memset(proc_tab[iproc].connex, 0, mesh->npart*sizeof(char));
         proc_tab[iproc].nb_elt = 0;
         proc_tab[iproc].nb_conn = 0;
         proc_tab[iproc].nb_ext = 0;
         proc_tab[iproc].nb_quad_p = 0;
         proc_tab[iproc].nb_quad_f = 0;

      }

      for(int i=0; i<mesh->nh; i++) {
         int j;
         for (j=0; j<NFACE; j++) {
            elt_tab[i].faces[j].defined = 0;
         }
         for (j=0; j<NEDGE; j++) {
            elt_tab[i].edges[j].defined = 0;
         }
         for (j=0; j<NCORNER; j++) {
            elt_tab[i].corners[j].defined = 0;
         }
      }

      // get all connectivity info for each element
      for(ielt=0; ielt<mesh->nh; ielt++) {
         //printf("                           \relt : %d / %d", ielt, mesh->nh);
         iproc = mesh->part[ielt];
         elt_tab[ielt].globalnum = ielt;

         // check if elem is outer & set proc connectivity info
         elt_tab[ielt].outer = getProcConn(ielt, iproc, proc_tab, mesh);

         if (elt_tab[ielt].outer) proc_tab[iproc].nb_ext++;

         proc_tab[iproc].nb_elt++;
         // element's neighbors connectivity
         setEltConn(ielt, iproc, elt_tab, mesh, proc_tab);
      }

      // count local geom nodes
      nodeMask = MALLOC(idx_t, mesh->nn);
   }

   idx_t *p_eptr, *p_eind;

   if (mesh->hex27) {

      p_eptr = mesh->eptr_27;
      p_eind = mesh->eind_27;

   } else {

      p_eptr = mesh->eptr;
      p_eind = mesh->eind;

   }

   // � l'envers pour terminer avec iproc = 0
   float* buff_coord;
   for (iproc = mesh->npart-1; iproc >= 0; iproc--) {

      if (rank == 0) {

         memset(nodeMask, 0, mesh->nn*sizeof(idx_t));

         nb_local_nodes = 0;

         for(ielt=0; ielt<mesh->nh; ielt++) {

            if (mesh->part[ielt] == iproc) {

               for(inodeptr = p_eptr[ielt]; inodeptr<p_eptr[ielt+1]; inodeptr++) {

                  if (nodeMask[p_eind[inodeptr]] == 0) {
                     ++nb_local_nodes;
                     nodeMask[p_eind[inodeptr]]++;
                  }

               }   
            }
         }

         if (iproc != 0) {

            // send nb_local_nodes to iproc
            buff_coord = MALLOC(float, nb_local_nodes*3);
            MPI_Send(&nb_local_nodes, 1, MPI_INT, iproc, 0, mpi_comm_simu);

         } else {

            *ig_mesh_nnode = nb_local_nodes;
            // allocate Fortran arrays rg_gnode_x, rg_gnode_y, rg_gnode_z
            // the call back function pass_address_back_() is then called to store addresses in xcoord, ycoord & zcoord.
            // allocate_node_coord_arrays_(&nb_local_nodes);
            allocate_node_coord_arrays_();

         }

      } else {

         if (rank == iproc) {

            // recv nb_local_nodes from proc 0
            MPI_Recv(ig_mesh_nnode, 1, MPI_INT, 0, 0, mpi_comm_simu, &status);
            buff_coord = MALLOC(float, *ig_mesh_nnode*3);
            allocate_node_coord_arrays_();

         }
      }

      if (rank == 0) {

         // fill arrays 
         memset(nodeMask, 0, mesh->nn*sizeof(idx_t));
         nb_local_nodes = 0;

         for(ielt=0; ielt<mesh->nh; ielt++) {

            if (mesh->part[ielt] == iproc) {

               for(inodeptr = p_eptr[ielt]; inodeptr<p_eptr[ielt+1]; inodeptr++) {

                  int globalnodenum = p_eind[inodeptr];

                  if (nodeMask[globalnodenum] == 0) {

                     nodeMask[globalnodenum] = ++nb_local_nodes;
                     p_eind[inodeptr] = nb_local_nodes;   // overwrite eind since we don't use it anymore

                     // warning 0 or 1 C/FORTRAN convention 
                     if (iproc == 0) {

                        xcoord[nb_local_nodes-1] = mesh->ncoords_x[globalnodenum];
                        ycoord[nb_local_nodes-1] = mesh->ncoords_y[globalnodenum];
                        zcoord[nb_local_nodes-1] = mesh->ncoords_z[globalnodenum];

                     } else {

                        buff_coord[(nb_local_nodes-1)*3] = mesh->ncoords_x[globalnodenum];
                        buff_coord[(nb_local_nodes-1)*3+1] = mesh->ncoords_y[globalnodenum];
                        buff_coord[(nb_local_nodes-1)*3+2] = mesh->ncoords_z[globalnodenum]; 

                     }

                  } else {

                     int localnumber = nodeMask[globalnodenum];
                     p_eind[inodeptr] = localnumber;      // overwrite eind since we don't use it anymore

                  }
               }
            }
         }

         if (iproc != 0) {

            MPI_Send(buff_coord, nb_local_nodes*3, MPI_FLOAT, iproc, 1, mpi_comm_simu);
            free(buff_coord);

         }

      } else {

         if (rank == iproc) {

            MPI_Recv(buff_coord, *ig_mesh_nnode*3, MPI_FLOAT, 0, 1, mpi_comm_simu, &status);

            for (inode = 0; inode<*ig_mesh_nnode; inode++) {
               xcoord[inode] = buff_coord[inode*3];
               ycoord[inode] = buff_coord[inode*3+1];
               zcoord[inode] = buff_coord[inode*3+2];
            }

            free(buff_coord);

         }
      }
   }

   if (rank == 0) {

      free(nodeMask);

      // alloc & settings
      int *num_outer;
      int *num_inner;
      num_outer = MALLOC(int, mesh->npart);
      num_inner = MALLOC(int, mesh->npart);

      for (iproc=0; iproc<mesh->npart; iproc++) {

         num_outer[iproc] = 0;
         num_inner[iproc] = proc_tab[iproc].nb_ext;
         proc_tab[iproc].local_elts = MALLOC(elt_info_t*, proc_tab[iproc].nb_elt); // DAVID2FREE

      }

      // build per proc data struct
      for(ielt=0; ielt<mesh->nh; ielt++) {

         int elt_proc = mesh->part[ielt];

         if (elt_tab[ielt].outer) {

            proc_tab[elt_proc].local_elts[num_outer[elt_proc]] = &elt_tab[ielt];
            elt_tab[ielt].localnum = num_outer[elt_proc]++; // warning, localnum start from 0

         } else {

            proc_tab[elt_proc].local_elts[num_inner[elt_proc]] = &elt_tab[ielt];
            elt_tab[ielt].localnum = num_inner[elt_proc]++; // warning, localnum start from 0

         }
      }

      free(num_inner);
      free(num_outer);

      return info;

   }

   return NULL;

}

// find topological details on connection between one hexahedral elts and all its neighbors, fill structures
void setEltConn(int ielt, int iproc, elt_info_t* elt_tab, mesh_t* mesh, proc_info_t* proc_tab)
{
 
   int ineig, inode, inode_n, i;
   //int num_node_per_face = mesh->num_node_per_dim*mesh->num_node_per_dim;
   int num_node_per_face = 4; // DAVID : pour les hexas � 27 noeuds, on ne se sert que des 8 premiers noeuds g�om pour etablir la connectivit�
   int* connex_nodes;

   connex_nodes = MALLOC(int, (2*num_node_per_face));

   for (i=0; i<6; i++) {

      elt_tab[ielt].faces[i].type    = NONE;
      elt_tab[ielt].faces[i].next    = NULL;
      elt_tab[ielt].faces[i].defined = 0;

   }

   for (i=0; i<12; i++) {

      elt_tab[ielt].edges[i].type    = NONE;
      elt_tab[ielt].edges[i].next    = NULL;
      elt_tab[ielt].edges[i].defined = 0;

   }

   for (i=0; i<8; i++) {

      elt_tab[ielt].corners[i].type    = NONE;
      elt_tab[ielt].corners[i].next    = NULL;
      elt_tab[ielt].corners[i].defined = 0;

   }

   // loop on hexa neighbors
   for (ineig = mesh->xadj_hex[ielt]; ineig < mesh->xadj_hex[ielt+1]; ineig++) {

      int neigh = mesh->adjncy_hex[ineig];
      int nb_conn = 0;

      // get nodes shared by ielt and its neighbor
      for(inode = mesh->eptr[ielt]; inode < mesh->eptr[ielt+1]; inode++) {

         for(inode_n = mesh->eptr[neigh]; inode_n < mesh->eptr[neigh+1]; inode_n++) {

            if (mesh->eind[inode] == mesh->eind[inode_n]) {
               connex_nodes[nb_conn] = inode - mesh->eptr[ielt]; // contact nodes numbers for current elt
               connex_nodes[nb_conn+num_node_per_face] = inode_n - mesh->eptr[neigh]; // associated node numbers for its neighbor
               nb_conn++;
            }

         }
      }

      // get connection topology
      if (nb_conn) {

         setHexaConnDetail(ielt, neigh, nb_conn, connex_nodes, elt_tab, mesh);

      } else {

         STOP("neighbor without contact");

      }
   }

   // loop on quad neighbors
   for (ineig = mesh->xadj[ielt]; ineig < mesh->xadj[ielt+1]; ineig++) {

      int neigh = mesh->adjncy[ineig];

      if (neigh >= mesh->nh) {   // quad   

         if (mesh->types[neigh] == QUAD_P) {

            proc_tab[iproc].nb_quad_p++;

         } else {

            proc_tab[iproc].nb_quad_f++;         

         }

         int nb_conn = 0;
         // get nodes shared by ielt and its attached quad
         for(inode = mesh->eptr[ielt]; inode < mesh->eptr[ielt+1]; inode++) {

            for(inode_n = mesh->eptr[neigh]; inode_n < mesh->eptr[neigh+1]; inode_n++) {

               if (mesh->eind[inode] == mesh->eind[inode_n]) {
                  connex_nodes[nb_conn] = inode - mesh->eptr[ielt]; // contact node number for current elt
                  connex_nodes[nb_conn+num_node_per_face] = inode_n - mesh->eptr[neigh]; // associated node number for its neighbor
                  nb_conn++;
               }

            }
         }

         if (nb_conn) {

            assert(nb_conn == N_NODES_QUAD);
            setQuadConnDetail(ielt, neigh, nb_conn, connex_nodes, elt_tab, mesh);

         } else {

            STOP("neighbor quad without contact");

         }
      }
   }
   free(connex_nodes);
   return;
}

// find topological details on connection between a hexahedral elts and an attached quad, fill structures
void setQuadConnDetail(int elt_num, int neigh_num, int nb_conn, int* connex_nodes, elt_info_t* elt_tab, mesh_t* mesh)
{ 

   int face, inode, inode2;

   face = -1;

   for(inode = 0; inode<4; inode++) {

      if (connex_nodes[inode] == CORNER1) {

         for(inode2 = 0; inode2<4; inode2++) {

            switch(connex_nodes[inode2]) {

               case CORNER3 : face = FACE1; goto face_continue;
               case CORNER6 : face = FACE2; goto face_continue;
               case CORNER8 : face = FACE5; goto face_continue;

            }
         }

      } else if(connex_nodes[inode] == CORNER7) {

         for(inode2 = 0; inode2<4; inode2++) {

            switch(connex_nodes[inode2]) {

               case CORNER4 : face = FACE4; goto face_continue;
               case CORNER5 : face = FACE6; goto face_continue;
               case CORNER2 : face = FACE3; goto face_continue;

            }
         }
      }
   }

face_continue:
   if (face == -1) {
      STOP("error in face search")
   }
   add_conn(FACE, elt_tab, elt_num, face, neigh_num, 0, 0, mesh->types[neigh_num]);
   return;
}

// find topological details on connection between 2 hexahedral elts, fill structures
void setHexaConnDetail(int elt_num, int neigh_num, int nb_conn, int* connex_nodes, elt_info_t* elt_tab, mesh_t* mesh)
{ 
   int icase, inode, inode2, num_conn_source, num_conn_target;

   switch(nb_conn) {

      case 1 : // 1 node connection (corner)
         num_conn_source = connex_nodes[0];
         num_conn_target = connex_nodes[4];
         add_conn(CORNER, elt_tab, elt_num, num_conn_source, neigh_num, num_conn_target, 0, HEXA);
         break;

      case 2 : // 2 nodes connection (edge)
         for(icase = 0; icase<2; icase++) {

            int edge = -1;
            int offset = icase*4;

            for(inode = 0; inode<2; inode++) {

               switch(connex_nodes[inode+offset]) {

                  case CORNER1 : 
                     for(inode2 = 0; inode2<2; inode2++) {
                        switch(connex_nodes[inode2+offset]) {
                           case CORNER2 : 
                              edge = EDGE1; goto edge_continue;
                           case CORNER4 : 
                              edge = EDGE4; goto edge_continue;
                           case CORNER5 :
                              edge = EDGE9; goto edge_continue;
                        }
                     }

                  case CORNER6 : 
                     for(inode2 = 0; inode2<2; inode2++) {
                        switch(connex_nodes[inode2+offset]) {
                           case CORNER2 : 
                              edge = EDGE10; goto edge_continue;
                           case CORNER7 : 
                              edge = EDGE6; goto edge_continue;
                           case CORNER5 : 
                              edge = EDGE5; goto edge_continue;
                        }
                     }

                  case CORNER3 : 
                     for(inode2 = 0; inode2<2; inode2++) {
                        switch(connex_nodes[inode2+offset]) {
                           case CORNER2 :
                              edge = EDGE2; goto edge_continue;
                           case CORNER4 :
                              edge = EDGE3; goto edge_continue;
                           case CORNER7 : 
                              edge = EDGE11; goto edge_continue;
                        }
                     }

                  case CORNER8 : 
                     for(inode2 = 0; inode2<2; inode2++) {
                        switch(connex_nodes[inode2+offset]) {
                           case CORNER7 :
                              edge = EDGE7; goto edge_continue;
                           case CORNER4 :
                              edge = EDGE12; goto edge_continue;
                           case CORNER5 :
                              edge = EDGE8; goto edge_continue;
                        }
                     }
               }
            }

edge_continue:
            if (edge == -1) {
               STOP("error in edge search")
            }
            if (icase==0) {
               num_conn_source = edge;
            } else {
               num_conn_target = edge;
            }
         }
         add_conn(EDGE, elt_tab, elt_num, num_conn_source, neigh_num, num_conn_target, findEdgeOrientation(connex_nodes, num_conn_source, num_conn_target), HEXA);
         break;

      case 9 :
      case 4 : // 4 nodes connection (face)
         for(icase = 0; icase<2; icase++) {
            int face = -1;
            int offset = icase*4;
            for(inode = 0; inode<4; inode++) {
               if (connex_nodes[inode+offset] == CORNER1) {
                  for(inode2 = 0; inode2<4; inode2++) {
                     switch(connex_nodes[inode2+offset]) {
                        case CORNER3 : face = FACE1; goto face_continue;
                        case CORNER6 : face = FACE2; goto face_continue;
                        case CORNER8 : face = FACE5; goto face_continue;
                     }
                  }
               } else if(connex_nodes[inode+offset] == CORNER7) {
                  for(inode2 = 0; inode2<4; inode2++) {
                     switch(connex_nodes[inode2+offset]) {
                        case CORNER4 : face = FACE4; goto face_continue;
                        case CORNER5 : face = FACE6; goto face_continue;
                        case CORNER2 : face = FACE3; goto face_continue;
                     }
                  }
               }
            }

face_continue:
            if (face == -1) {
               STOP("error in face search")
            }
            if (icase == 0) {
               num_conn_source= face;
            } else {
               num_conn_target= face;
            }
         }
         add_conn(FACE, elt_tab, elt_num, num_conn_source, neigh_num, num_conn_target, findFaceOrientation(connex_nodes, num_conn_source, num_conn_target), HEXA);
         break;
      default : // problem !
         STOP("Error in nb nodes connected")
   }
   return;
}

// return true if element is on the boundary
// as a side effect, build procs adjacency graph
int getProcConn(int ielt, int iproc, proc_info_t* proc_tab, mesh_t* mesh)
{ 
   int ineig;
   int isOuter = 0;

   for (ineig = mesh->xadj_hex[ielt]; ineig < mesh->xadj_hex[ielt+1]; ineig++) {
      int neigh = mesh->adjncy_hex[ineig];
      int neig_proc = mesh->part[neigh];
      if (neig_proc != iproc) {
         if (!proc_tab[iproc].connex[neig_proc]) {
            proc_tab[iproc].connex[neig_proc] = 1;
            proc_tab[iproc].nb_conn++;
         }
         isOuter = 1;
      }
   }
   return isOuter;
}

// set elts weight for partionning
void setHexaWeight(mesh_t* mesh)
{ 
   idx_t ihex, ineigh;

   mesh->ncon = 1;
   mesh->vwgt = MALLOC(idx_t, mesh->nh); // DAVID2FREE
   for(ihex=0; ihex<mesh->nh; ihex++) {
      mesh->vwgt[ihex] = HEXA_WEIGHT;
      for (ineigh = mesh->xadj[ihex]; ineigh<mesh->xadj[ihex+1]; ineigh++) {
         idx_t neigh = mesh->adjncy[ineigh];
         if(mesh->types[neigh] == QUAD_P) mesh->vwgt[ihex] += QUAD_P_WEIGHT;
         if(mesh->types[neigh] == QUAD_F) mesh->vwgt[ihex] += QUAD_F_WEIGHT;
      }
   }
   return;
}

void printMemUsage(mesh_t* mesh) {

   double sum = 0.;
   int i=1;
   char* unit;
   const long un = 1;

   sum += mesh->npart * sizeof(proc_info_t);
   sum += mesh->nh * sizeof(elt_info_t);
   sum += sizeof(info_t);
   sum += mesh->npart * mesh->npart * sizeof(char);
   sum += mesh->nh * sizeof(elt_info_t*);
   sum += (2*mesh->nh + mesh->ne+1 + mesh->nh * mesh->num_nodes_hexa + (mesh->nq_parax + mesh->nq_surf) * mesh->num_nodes_quad) * sizeof(idx_t);
   sum += mesh->nh * sizeof(unsigned char);
   sum += mesh->nn * 3 * sizeof(real_t);
   sum += mesh->ne * sizeof(elt_t);
   sum += mesh->nn * sizeof(idx_t);

   while(sum > un<<(i*10)) i++;
   switch(i) {
      case 1 : unit="Octets"; break;
      case 2 : unit="Kio"; break;
      case 3 : unit="Mio"; break;
      case 4 : unit="Gio"; break;
      case 5 : unit="Tio"; break;
      default: unit="Tio"; i=5;
   }
   printf("This program will use %d %s of memory\n", (int)(sum/(1<<((i-1)*10))),unit);
}


void readCubitMeshAbaqus(char* fileinp, mesh_t* mesh, int* number_of_elemnt_block)
{ 
   char line[LENMAX];
   int iblock = 0;
   FILE *unit_inp;
   char *hexa8=NULL, *hexa27=NULL;
   int readquad = 0;
   int   ielt, inode, itmp;
   int   loc_eind = 0;
   int   ielt_glo = 0;
   char *quadf=NULL, *quadp=NULL;
   int   ihexa=0;

   unit_inp = fopen(fileinp,"r");
   if(unit_inp == NULL)
   {
      STOP("can't open input file (readCubitMeshAbaqus (1)")
   }

   //First pass on file *.inp: count number of nodes, hexa, quad_parax and quad_fsurf
   mesh->nh = mesh->nq_parax = mesh->nq_surf = 0;
   // 4 nodes quads in abaqus
   mesh->num_nodes_quad = 4;

   // no 27 nodes hexa in abaqus format !
   mesh->hex27 = 0;
   while (!feof(unit_inp))
   {
      char * cdum = fgets(line, LENMAX, unit_inp);

      //count number of geometric nodes
      if (strstr(line,"NSET=ALLNODES")) mesh->nn = count_elt(unit_inp);

      //count number of hexa
      if ((hexa8 = strstr(line,HEXA_8_HDR)) || (hexa27 = strstr(line,HEXA_27_HDR))) {
         // set number of node in hexa elts
         mesh->num_nodes_hexa = hexa8?8:27;
         // no 27 nodes hexa in abaqus format !
         assert(mesh->num_nodes_hexa == 8);
         number_of_elemnt_block[iblock] = count_elt(unit_inp);
         mesh->nh += number_of_elemnt_block[iblock++];
         if (readquad) {
            STOP("unexpected : quads are read before hexa in cubit mesh")
         }
      }


      //count number of quad_parax
      if (strstr(line,QUAD_P_HDR)) {
         number_of_elemnt_block[iblock] = count_elt(unit_inp);
         mesh->nq_parax += number_of_elemnt_block[iblock++];
         readquad = 1;
      }

      //count number of quad_fsurf
      if (strstr(line,QUAD_F_HDR)) {
         number_of_elemnt_block[iblock] = count_elt(unit_inp);
         mesh->nq_surf += number_of_elemnt_block[iblock++];
         readquad = 1;
      }

   }
   fclose(unit_inp);

   // allocation
   mesh->ne               = mesh->nh + mesh->nq_parax + mesh->nq_surf;
   mesh->ncon             = 0;
   mesh->eptr             = MALLOC(idx_t, (mesh->ne+1)); // DAVID2FREE
   mesh->eind             = MALLOC(idx_t, (mesh->nh * mesh->num_nodes_hexa + (mesh->nq_parax + mesh->nq_surf) * mesh->num_nodes_quad)); // DAVID2FREE
   mesh->layer            = MALLOC(int, mesh->nh);       // DAVID2FREE
   mesh->ncoords_x        = MALLOC(real_t, mesh->nn);    // DAVID2FREE
   mesh->ncoords_y        = MALLOC(real_t, mesh->nn);    // DAVID2FREE
   mesh->ncoords_z        = MALLOC(real_t, mesh->nn);    // DAVID2FREE
   mesh->types            = MALLOC(elt_t, mesh->ne);     // DAVID2FREE
   mesh->part             = MALLOC(idx_t, mesh->nh);     // DAVID2FREE
   mesh->num_node_per_dim = (int) cbrt(mesh->num_nodes_hexa);

   iblock   = 0;
   unit_inp = fopen(fileinp,"r");
   if(unit_inp == NULL)
   {
      STOP("can't open input file (readCubitMeshAbaqus (2)")
   }

   //Second pass on file *.inp: make eind & eptr
   iblock = ihexa = 0;
   while (!feof(unit_inp))
   {   
      char *cdum = fgets(line, LENMAX, unit_inp);

      //fill array gnodes with coordinates of geometric nodes
      if (strstr(line,"NSET=ALLNODES"))
         for (inode=0;inode<mesh->nn;inode++) {
            int idum = fscanf(unit_inp,"%d," "%"SCREAL "," "%"SCREAL "," "%"SCREAL, &itmp, &mesh->ncoords_x[inode], &mesh->ncoords_y[inode], &mesh->ncoords_z[inode]); //SCREAL is defined in metis.h
         }

      //fill eptr and eind for hexa
      if ((hexa8 = strstr(line,HEXA_8_HDR)) || (hexa27 = strstr(line,HEXA_27_HDR)))
      {
         // get num of layer
         char *hdr = hexa8?HEXA_8_HDR:HEXA_27_HDR;
         int layer = atoi(rtrim(strstr(line,hdr) + strlen(hdr)));

         for (ielt=0;ielt<number_of_elemnt_block[iblock];ielt++)
         {
            int idum = fscanf(unit_inp,"%d",&itmp);
            mesh->eptr[ielt_glo]    = loc_eind;
            mesh->types[ielt_glo++] = HEXA;
            mesh->layer[ihexa++]    = layer;
            for (inode=0;inode<mesh->num_nodes_hexa;inode++)
            {
               int idum = fscanf(unit_inp,",%d",&mesh->eind[loc_eind]);
               mesh->eind[loc_eind++]--;   // because num starts on 1 in cubit meshes
            }
         }

         iblock++;
      }

      //fill eptr and eind for quad_parax and quad_fsurf
      if ((quadp = strstr(line,QUAD_P_HDR)) || (quadf = strstr(line,QUAD_F_HDR)))
      {
         for (ielt=0;ielt<number_of_elemnt_block[iblock];ielt++)
         {
            int idum = fscanf(unit_inp,"%d",&itmp);
            mesh->eptr[ielt_glo]    = loc_eind;
            mesh->types[ielt_glo++] = quadp?QUAD_P:QUAD_F;
            for (inode=0;inode<mesh->num_nodes_quad;inode++)
            {
               int idum =  fscanf(unit_inp,",%d",&mesh->eind[loc_eind]);
               mesh->eind[loc_eind++]--;   // because num starts on 1 in cubit meshes
            }
         }
         iblock++;
      }
   }
   mesh->eptr[ielt_glo] = loc_eind;

   if (mesh->nh == 0) {
      STOP("not a valid mesh file");
   }
   return;
}

void readCubitMeshExodus(char *fileinp, mesh_t *mesh, int *number_of_elemnt_block)
{
   int cpu_word_size = sizeof(float);
   int io_word_size = 0;
   float version;
   
   int exoid;
   char title[MAX_LINE_LENGTH+1];
   int num_dim, num_nodes, num_elem, num_elem_blk, num_node_sets, num_side_sets;
   int res;

   int* idelbs;
   char elem_type[MAX_STR_LENGTH+1];
   char block_name[MAX_STR_LENGTH+1];
   int num_elem_this_blk, num_nodes_per_elem, num_attr;

   int* connect;

   mesh->nh = 0;
   mesh->nq_parax = 0;
   mesh->nq_surf = 0;

   // open file
   exoid = ex_open(fileinp, EX_READ, &cpu_word_size, &io_word_size, &version);

   // get header
   res = ex_get_init(exoid, title, &num_dim, &num_nodes, &num_elem, &num_elem_blk, &num_node_sets, &num_side_sets);

   // get element blocks' ids
   idelbs = (int *) calloc(num_elem_blk, sizeof(int));
   res = ex_get_elem_blk_ids (exoid, idelbs);

        MSG("");

   // get number of element for each type/block
   for (int i=0; i<num_elem_blk; i++) {

      res = ex_get_elem_block (exoid, idelbs[i], elem_type, &num_elem_this_blk, &num_nodes_per_elem, &num_attr);

      if (!strcmp(elem_type, "HEX8") || !strcmp(elem_type, "HEX") || !strcmp(elem_type, "HEX27")) {

         res = ex_get_name(exoid, EX_ELEM_BLOCK, idelbs[i], block_name);

         mesh->nh += num_elem_this_blk;
         mesh->num_nodes_hexa = num_nodes_per_elem;
         mesh->hex27 = (num_nodes_per_elem == 27)?1:0;

         printf("Found block %d, block name %s, element type %s \n",idelbs[i],block_name,elem_type);

      } else if (!strcmp(elem_type, "SHELL4") || !strcmp(elem_type, "QUAD4") || !strcmp(elem_type, "QUAD") || !strcmp(elem_type, "SHELL9")) {

         res = ex_get_name(exoid, EX_ELEM_BLOCK, idelbs[i], block_name);

         mesh->num_nodes_quad = num_nodes_per_elem;
         if (!strcmp(block_name, "prx")) {
            // parax
            mesh->nq_parax += num_elem_this_blk;
                                printf("Found block %d, block name %s, element type %s \n",idelbs[i],block_name,elem_type);

         } else if (!strcmp(block_name, "fsu")){
            // surf
            mesh->nq_surf += num_elem_this_blk;
                                printf("Found block %d, block name %s, element type %s \n",idelbs[i],block_name,elem_type);

         } else {
            printf("ERROR : unknown block name : %s in block %d element type %s\n",block_name,idelbs[i],elem_type);
         }
      } else {
         printf("block %d : ERROR, bad type : %s\n",i,elem_type);
      }

   }

   // allocations
   mesh->nn = num_nodes;
   mesh->ne = num_elem;
   mesh->ncon             = 0;
   mesh->layer            = MALLOC(int, mesh->nh);       // DAVID2FREE
   mesh->ncoords_x        = MALLOC(real_t, mesh->nn);    // DAVID2FREE
   mesh->ncoords_y        = MALLOC(real_t, mesh->nn);    // DAVID2FREE
   mesh->ncoords_z        = MALLOC(real_t, mesh->nn);    // DAVID2FREE
   mesh->types            = MALLOC(elt_t, mesh->ne);     // DAVID2FREE
   mesh->part             = MALLOC(idx_t, mesh->nh);     // DAVID2FREE
   mesh->num_node_per_dim = (int) cbrt(mesh->num_nodes_hexa);

   if (mesh->hex27) {
      mesh->eptr             = MALLOC(idx_t, (mesh->ne+1)); // DAVID2FREE
      mesh->eind             = MALLOC(idx_t, (mesh->nh * 8 + (mesh->nq_parax + mesh->nq_surf) * 4)); // DAVID2FREE
      mesh->eptr_27          = MALLOC(idx_t, (mesh->ne+1)); // DAVID2FREE
      mesh->eind_27          = MALLOC(idx_t, (mesh->nh * 27 + (mesh->nq_parax + mesh->nq_surf) * 9)); // DAVID2FREE
   } else {
      mesh->eptr             = MALLOC(idx_t, (mesh->ne+1)); // DAVID2FREE
      mesh->eind             = MALLOC(idx_t, (mesh->nh * mesh->num_nodes_hexa + (mesh->nq_parax + mesh->nq_surf) * mesh->num_nodes_quad)); // DAVID2FREE
   }

   // check
   assert(mesh->ne == mesh->nh + mesh->nq_parax + mesh->nq_surf);    

   // get geometric nodes coordinates
   ex_get_coord(exoid, mesh->ncoords_x, mesh->ncoords_y, mesh->ncoords_z);

   // fill eptr and eind
   int ielem_glob = 0;
   int ihexa = 0;
   int offset;
   int global_offset = 0;
   int global_offset_27 = 0;

   for (int iblk=0; iblk<num_elem_blk; iblk++) {

      res = ex_get_elem_block (exoid, idelbs[iblk], elem_type, &num_elem_this_blk, &num_nodes_per_elem, &num_attr);
      res = ex_get_name(exoid, EX_ELEM_BLOCK, idelbs[iblk], block_name);

      // read element connectivity
      connect = (int *) calloc(num_nodes_per_elem*num_elem_this_blk, sizeof(int));
      offset  = 0;
      res     = ex_get_elem_conn (exoid, idelbs[iblk], connect);

      if (!strcmp(elem_type, "HEX8") || !strcmp(elem_type, "HEX") || !strcmp(elem_type, "HEX27")) {

         int layer = atoi(&block_name[1]); // layers l01 l02 ...

         for (int ielem=0; ielem<num_elem_this_blk; ielem++) {

            mesh->eptr[ielem_glob] = global_offset;

            if (mesh->hex27) {
               mesh->eptr_27[ielem_glob]  = global_offset_27;
            }

            mesh->types[ielem_glob] = HEXA;
            mesh->layer[ihexa++] = layer;

            for (int inode=0; inode<num_nodes_per_elem; inode++) {

               // DAVID : il faudra peut-�tre rajouter l'indirection ex2efi_hexa/quad pour 27/9 noeuds
               if (mesh->hex27) {
                  mesh->eind_27[global_offset_27 + inode] = connect[offset + inode]-1; 
               } 
               //mesh->eind[global_offset+inode] = connect[offset + ex2efi_hexa[inode]]-1; 
               if (inode<8) mesh->eind[global_offset + inode] = connect[offset + inode]-1; // because num starts on 1 in cubit meshes 
            }

            offset += num_nodes_per_elem;

            if (mesh->hex27) {

               global_offset_27 += 27;
               global_offset    += 8;

            } else {

               global_offset += num_nodes_per_elem;

            }

            ielem_glob++;

         }

      } else if (!strcmp(elem_type, "SHELL4") || !strcmp(elem_type, "QUAD4") || !strcmp(elem_type, "QUAD") || !strcmp(elem_type, "SHELL9")) {

         for (int ielem=0; ielem<num_elem_this_blk; ielem++) {

            mesh->eptr[ielem_glob] = global_offset;

            if (mesh->hex27) {
               mesh->eptr_27[ielem_glob]  = global_offset_27;
            }

            if (!strcmp(block_name, "prx")) {
               mesh->types[ielem_glob] = QUAD_P;
            } else if (!strcmp(block_name, "fsu")){
               mesh->types[ielem_glob] = QUAD_F;
            } else {
               printf("ERROR : unknown quad type : %s\n",block_name);
            }

            for (int inode=0; inode<num_nodes_per_elem; inode++) {
               // DAVID : il faudra peut-�tre rajouter l'indirection ex2efi_hexa/quad pour 27/9 noeuds
               if (mesh->hex27) {
                  mesh->eind_27[global_offset_27 + inode] = connect[offset + inode]-1; 
               } 
               //mesh->eind[global_offset+inode] = connect[offset + ex2efi_quad[inode]]-1; 
               if (inode<4) mesh->eind[global_offset+inode] = connect[offset + inode]-1; // because num starts on 1 in cubit meshes
            }

            offset += num_nodes_per_elem;

            if (mesh->hex27) {
               global_offset_27 += 9;
               global_offset += 4;
            } else {
               global_offset += num_nodes_per_elem;
            }

            ielem_glob++;

         }

      } else {

         printf("block %d : ERROR, bad type : %s\n",iblk,elem_type);

      }

      free(connect);

   }
   // dummy ptr on the end of array
   mesh->eptr[ielem_glob] = global_offset;
   if (mesh->hex27) mesh->eptr_27[ielem_glob] = global_offset_27;

   free(idelbs);
   ex_close(exoid);
   return;
}

char* rotateVect(char* ortn, char* ret_or, int cw_quarter_rot)
{ 
   // ortn[0] -> horizontal vect
   // ortn[1] -> vertical vect
   // 1=i, 2=j, 3=k
   // sign :   + : left->right or bottom->up
   //         - : left<-right or bottom<-up
   // ie ortn[] = {1, -3} ->
   //   +-----> i
   //   |
   //   | k
   //   V

   char Hsign, Vsign;

   char H = ortn[0];
   char V = ortn[1];
   // -H(00) -> +V(11) -> +H(10) -> -V(01) clockwise rotation. H or V : right digit, sign : left digit
   //    0         3         2         1
   char Hrot = H>0?2:0;
   char Vrot = V>0?3:1;
   int nbrot = cw_quarter_rot%4;   

   Hrot = (4-nbrot+Hrot)&3;
   Hsign = (Hrot>>1)?1:-1;
   Vrot = (4-nbrot+Vrot)&3;
   Vsign = (Vrot>>1)?1:-1;


   if (Vrot&1) ret_or[1] = Vsign*abs(V);   // impair -> Vert
   else ret_or[0] = Vsign*abs(V);         // pair -> Hor

   if (Hrot&1) ret_or[1] = Hsign*abs(H);   // impair -> Vert
   else ret_or[0] = Hsign*abs(H);         // pair -> Hor

   return ret_or;
}

int findFaceOrientation(int *connex_nodes, int num_conn_source, int num_conn_target)
{ 
   char or[2];
   char or_rot[2];
   int i, rot;
   for (i=0; i<4; i++) {
      if (connex_nodes[4] == face2faceRot[num_conn_source][num_conn_target][i]) {
         rot = i;
         break;
      }
   }
   // printf("node s : %d, node t : %d, face2faceRot[%d][%d][%d]=%d\n",connex_nodes[0], connex_nodes[4], num_conn_source+1, num_conn_target+1, i, face2faceRot[num_conn_source][num_conn_target][i]);
   memcpy(or, face_orientation[num_conn_target], 2*sizeof(char));

   // horizontal flip (on voit la face depuis l'arriere)
   or[0] = -or[0];
   // -rot : sens inverse parce que vu a l'envers
   rotateVect(or, or_rot, -rot);
   char hsign = or_rot[0]>0?1:0;
   char vsign = or_rot[1]>0?1:0;
   char h=abs(or_rot[0]);
   char v=abs(or_rot[1]);
   char valret = (v&3)|(vsign<<2)|((h&3)<<3)|(hsign<<5);
   // printf("rot : %d, face source %d, face target %d : h:%s%s, v:%s%s, " BYTETOBINARYPATTERN , rot, num_conn_source+1, num_conn_target+1, hsign?"+":"-",(abs(or_rot[0])==1)?"i":(abs(or_rot[0])==2)?"j":"k", vsign?"+":"-", (abs(or_rot[1])==1)?"i":(abs(or_rot[1])==2)?"j":"k", BYTETOBINARY(valret));
   // printf("\n\n");

   return (int) valret;
}

int findEdgeOrientation(int *connex_nodes, int num_edge_source, int num_edge_target)
{   
   int source_order=0;
   if (edgeOrder[num_edge_source][0] == connex_nodes[0]) source_order = 1;

   if (edgeOrder[num_edge_target][0] == connex_nodes[4]) {
      if (source_order) return 1; // same direction
   } else {
      if (!source_order) return 1; // same direction
   }

   return 0;   // opposite directions
}


void add_conn(topo_t typecon, elt_info_t* elt_tab, int hexa_src, int numcon_src, idx_t num_neigh, int numcon_neigh, int orientation, elt_t type)
{ 
   conn_info *ptr, *last;

   switch(typecon) {

      case FACE :   if (elt_tab[hexa_src].faces[numcon_src].defined) { // malloc
                  fprintf(stderr, "ERROR : face %d of elt %d is already connected\n", numcon_src, hexa_src);
                  exit(1);
               } else {
                  ptr = &elt_tab[hexa_src].faces[numcon_src];
               }
               break;

      case EDGE : if (elt_tab[hexa_src].edges[numcon_src].defined) { // malloc
                for (ptr = &elt_tab[hexa_src].edges[numcon_src]; ptr; ptr = ptr->next) last = ptr;
                ptr = last->next = MALLOC(conn_info, 1);
             } else {
                ptr = &elt_tab[hexa_src].edges[numcon_src];
             }
             break;

      case CORNER : if (elt_tab[hexa_src].corners[numcon_src].defined) { // malloc
                  for (ptr = &elt_tab[hexa_src].corners[numcon_src]; ptr; ptr = ptr->next) last = ptr;
                  ptr = last->next = MALLOC(conn_info, 1);
               } else {
                  ptr = &elt_tab[hexa_src].corners[numcon_src];
               }
               break;

   }

   ptr->num_neigh = num_neigh;
   ptr->num_conn = numcon_neigh;
   ptr->orientation = orientation;
   ptr->type = type;
   ptr->next = NULL;
   ptr->defined = 1;

   return;

}

int getnbneigh(topo_t typecon, elt_info_t* elt_tab, int numcon_src)
{ 

   conn_info *ptr;
   int count=0;

   switch(typecon) {

      case FACE :   printf("WARNING : funct getnbneigh() should not be called for faces\n");
               count++;
               break;

      case EDGE : if (elt_tab->edges[numcon_src].defined) {
                for (ptr = &elt_tab->edges[numcon_src]; ptr; ptr = ptr->next) count++;
             }
             break;

      case CORNER : if (elt_tab->corners[numcon_src].defined) {
                  for (ptr = &elt_tab->corners[numcon_src]; ptr; ptr = ptr->next) count++;
               }
               break;

   }

   return count;

}

//This function count the number of lines to compute the number of nodes, etc.
int count_elt(FILE *unit)
{ 
   const char EOL = '\n';
   char c;
   int ne = 0;

   while(1)
   {
      c = getc(unit);
      if (c == EOL)
         ne++;
      else if (c == '*')
         break;   
   }
   return ne; 
}

char *ltrim(char *s)
{ 
   while(isspace(*s)) s++;
   return s;
}

char *rtrim(char *s)
{ 
   char* back = s + strlen(s);
   while(isspace(*--back));
   *(back+1) = '\0';
   return s;
}

char *trim(char *s)
{ 
   return rtrim(ltrim(s)); 
}
