!>!===================================================================================================================================!<!
!>!                                                        EFISPEC3D                                                                  !<!
!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
!>!                                                                                                                                   !<!
!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
!>!                                                                                                                                   !<!
!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
!>!                                                                                                                                   !<!
!>!                                                 http://efispec.free.fr                                                            !<!
!>!                                                                                                                                   !<!
!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
!>!                                                                                                                                   !<!
!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
!>!                                                                David    MICHEA                                                    !<!
!>!                                                                Philippe THIERRY                                                   !<!
!>!                                                                Sylvain  JUBERTIE                                                  !<!
!>!                                                                Emmanuel CHALJUB                                                   !<!
!>!                                                                Francois LAVOUE                                                    !<!
!>!                                                                Tom      BUDON                                                     !<!
!>!                                                                Emmanuel MELIN                                                     !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
!>!                                                                                                                                   !<!
!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
!>!                           "http://www.cecill.info".                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
!>!                                                                                                                                   !<!
!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
!>!                           securite.                                                                                               !<!
!>!                                                                                                                                   !<!
!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
!>!                           motion using a finite spectral-element method.                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
!>!                           version.                                                                                                !<!
!>!                                                                                                                                   !<!
!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
!>!                                                                                                                                   !<!
!>!                           You should have received a copy of the GNU General Public License along with                            !<!
!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
!>!                                                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!                  3 ---> Thirdparty libraries                                                                                      !<!
!>!                                                                                                                                   !<!
!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
!>!                                                                                                                                   !<!
!>!                             --> METIS 5.1.0                                                                                       !<! 
!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> Lib_VTK_IO                                                                                        !<!
!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> INTERP_LINEAR                                                                                     !<!
!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
!>!                                                                                                                                   !<!
!>!                             --> FLASProc                                                                                          !<!
!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
!>!                                                                                                                                   !<!
!>!                             --> EXODUS II                                                                                         !<!
!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
!>!                                                                                                                                   !<!
!>!                             --> NETCDF                                                                                            !<!
!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
!>!                                                                                                                                   !<!
!>!                             --> HDF5                                                                                              !<!
!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
!>!                                                                                                                                   !<!
!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
!>!                                                                                                                                   !<!
!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
!>!                           Computers & Structures, 245, 106459.                                                                    !<!
!>!                                                                                                                                   !<!
!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
!>!                                                                                                                                   !<!
!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
!>!                                                                                                                                   !<!
!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
!>!                           Journal International, 201(1), 90-111.                                                                  !<!
!>!                                                                                                                                   !<!
!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
!>!                                                                                                                                   !<!
!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
!>!                                                                                                                                   !<!
!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
!>!                           170(1), 43-64.                                                                                          !<!
!>!                                                                                                                                   !<!
!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
!>!                                                                                                                                   !<!
!>!                  5 ---> Enjoy !                                                                                                   !<!
!>!                                                                                                                                   !<!
!>!===================================================================================================================================!<!
module mod_random_field

   use mod_precision

   use mod_global_variables, only : CIL

   use mod_init_memory

   implicit none

   public  :: KL_eval3D
   public  :: KL_roots
   public  :: KL_sort
   public  :: KL_init_distribution
   public  :: KL_deallocate
   private :: find_roots
   private :: f1
   private :: rand_normal
   private :: rand_normal_marsaglia
   public  :: init_random_seed
   private :: hpsort_eps_epw  

   contains

!
!
!***************************************************************************************************************
   subroutine KL_eval3D(m,e,x,y,z,KLx,KLy,KLz,r,ind,v)
!***************************************************************************************************************

      use mod_global_variables, only : type_random_material_KL

      implicit none

      real   (kind=RXP)                             , intent(in)  :: m
      real   (kind=RXP)                             , intent(in)  :: e
      real   (kind=RXP)                             , intent(in)  :: x
      real   (kind=RXP)                             , intent(in)  :: y
      real   (kind=RXP)                             , intent(in)  :: z
      type   (type_random_material_KL)              , intent(in)  :: KLx
      type   (type_random_material_KL)              , intent(in)  :: KLy
      type   (type_random_material_KL)              , intent(in)  :: KLz
      real   (kind=RXP), dimension(:)               , intent(in)  :: r
      integer(kind=IXP), dimension(:)               , intent(in)  :: ind
      real   (kind=RXP)                             , intent(out) :: v

      real   (kind=RXP), dimension(:) , allocatable               :: phix
      real   (kind=RXP), dimension(:) , allocatable               :: phiy
      real   (kind=RXP), dimension(:) , allocatable               :: phiz
      real   (kind=RXP), dimension(:) , allocatable               :: uxyz
      real   (kind=RXP)                                           :: s
      real   (kind=RXP)                                           :: iroots

      integer(kind=IXP)                                           :: nmodxyz
      integer(kind=IXP)                                           :: nmodx
      integer(kind=IXP)                                           :: nmody
      integer(kind=IXP)                                           :: nmodz
      integer(kind=IXP)                                           :: imod
      integer(kind=IXP)                                           :: i
      integer(kind=IXP)                                           :: j
      integer(kind=IXP)                                           :: k
      integer(kind=IXP)                                           :: l
      integer(kind=IXP)                                           :: ll
      integer(kind=IXP)                                           :: ios

      nmodx = KLx%n
      nmody = KLy%n
      nmodz = KLz%n

!
!---->compute eigenvector phi for the three space direction

      ios = init_array_real(phix,nmodx,"mod_random_field:KL_eigfunc:phix")
      ios = init_array_real(phiy,nmody,"mod_random_field:KL_eigfunc:phiy")
      ios = init_array_real(phiz,nmodz,"mod_random_field:KL_eigfunc:phiz")
 
!
!---->X-direction

     !loop for imod-1 even
      do imod = ONE_IXP,nmodx,TWO_IXP

         iroots = KLx%r(imod)

         phix(imod) = cos(iroots * (x - 0.5_RXP)) / sqrt(0.5_RXP * (1.0_RXP + sin(iroots) / iroots))

      enddo

     !loop for imod-1 odd
      do imod = TWO_IXP,nmodx,TWO_IXP

         iroots = KLx%r(imod)

         phix(imod) = sin(iroots * (x - 0.5_RXP)) / sqrt(0.5_RXP * (1.0_RXP - sin(iroots) / iroots))
       
      enddo

!
!---->Y-direction

     !loop for imod-1 even
      do imod = ONE_IXP,nmody,TWO_IXP

         iroots = KLy%r(imod)

         phiy(imod) = cos(iroots * (y - 0.5_RXP)) / sqrt(0.5_RXP * (1.0_RXP + sin(iroots) / iroots))

      enddo

     !loop for imod-1 odd
      do imod = TWO_IXP,nmody,TWO_IXP

         iroots = KLy%r(imod)

         phiy(imod) = sin(iroots * (y - 0.5_RXP)) / sqrt(0.5_RXP * (1.0_RXP - sin(iroots) / iroots))
       
      enddo

!
!---->Z-direction

     !loop for imod-1 even
      do imod = ONE_IXP,nmodz,TWO_IXP

         iroots = KLz%r(imod)

         phiz(imod) = cos(iroots * (z - 0.5_RXP)) / sqrt(0.5_RXP * (1.0_RXP + sin(iroots) / iroots))

      enddo

     !loop for imod-1 odd
      do imod = TWO_IXP,nmodz,TWO_IXP

         iroots = KLz%r(imod)

         phiz(imod) = sin(iroots * (z - 0.5_RXP)) / sqrt(0.5_RXP * (1.0_RXP - sin(iroots) / iroots))
       
      enddo

!
!---->compute uk = sqrt(lambda_k) * phi_k only for the modes in array modes_ind

      nmodxyz = size(ind)

      ios = init_array_real(uxyz,nmodxyz,"mod_random_field:KL_eigfunc:uxyz")

      do l = 1_IXP,nmodxyz

         ll = ind(l)

!
!------->indirection array from global (ll) to local numbering (i,j,k)

         i = int((ll-1_IXP)/(nmodz*nmody)) + 1_IXP
         j = int(mod((ll-1_IXP),nmodz*nmody)/nmodz) + 1_IXP
         k = mod(mod((ll-1_IXP),nmodz*nmody),nmodz) + 1_IXP

         uxyz(l) = sqrt(KLx%l(i))*phix(i) * sqrt(KLy%l(j))*phiy(j) * sqrt(KLz%l(k))*phiz(k)

      enddo

!
!---->compute random value at point x,y,z

      s = ZERO_RXP
      
      do l = ONE_IXP,nmodxyz
      
         s = s + uxyz(l) * r(l)
        
      enddo

      v = m*exp(e*s)

      return

!***************************************************************************************************************
   end subroutine KL_eval3D
!***************************************************************************************************************

!
!
!***************************************************************************************************************
   subroutine KL_roots(Lc,KL)
!***************************************************************************************************************

      use mod_global_variables, only : type_random_material_KL

      implicit none

      real   (kind=RXP)                             , intent(in)  :: Lc
      type(type_random_material_KL)                 , intent(out) :: KL
                                                     
      real   (kind=RXP), parameter                                :: DV   =    0.01_RXP
      real   (kind=RXP), parameter                                :: VMIN =    0.01_RXP
      real   (kind=RXP), parameter                                :: VMAX =  500.00_RXP
      real   (kind=RXP), dimension(:)  , allocatable              :: v
      real   (kind=RXP), dimension(:)  , allocatable              :: veck
      real   (kind=RXP), dimension(:)  , allocatable              :: troots
      real   (kind=RXP), dimension(:)  , allocatable              :: tlamk
      real   (kind=RXP)                                           :: sum_tlamk
                                                                  
      integer(kind=IXP)                                           :: i
      integer(kind=IXP)                                           :: nmod
      integer(kind=IXP)                                           :: imod
      integer(kind=IXP)                                           :: nv
      integer(kind=IXP)                                           :: ios
   
      nv = int((VMAX-VMIN)/DV)+ONE_IXP

      ios = init_array_real(v,nv,"mod_random_field:KL_roots:v")

      ios = init_array_real(veck,nv,"mod_random_field:KL_roots:veck")

!
!---->discretized intervalle v where to compute veck

      do i = ONE_IXP,nv

         v(i) = vmin + real(i-ONE_IXP,kind=IXP)*dv

      enddo
      
!
!---->veck = discretized function f1

      do i = ONE_IXP,nv

         veck(i) = f1(v(i),Lc)

      enddo

!
!---->find roots

      call find_roots(v,veck,troots)

      ios = init_array_real(tlamk,size(troots,kind=IXP),"mod_random_field:KL_roots:tlamk")

!
!---->compute eigenvalue lambda_k

      sum_tlamk = 0.0_RXP

      do imod = ONE_IXP,size(troots)

         tlamk(imod) = 2.0_RXP * Lc * (1.0_RXP + troots(imod)**TWO_IXP * Lc**TWO_IXP)**(-ONE_IXP)

         sum_tlamk = sum_tlamk + tlamk(imod)

      enddo

      nmod = size(troots)

      ios = init_array_real(KL%r,nmod,"mod_random_field:KL_roots:KL%r")

      ios = init_array_real(KL%l,nmod,"mod_random_field:KL_roots:KL%l")

      KL%r(1_IXP:nmod) = troots(1_IXP:nmod)

      KL%l(1_IXP:nmod) = tlamk (1_IXP:nmod)

      KL%n = nmod

      return

!***************************************************************************************************************
   end subroutine KL_roots
!***************************************************************************************************************


!
!
!***************************************************************************************************************
   subroutine KL_sort(KLx,KLy,KLz,ind_cut)
!***************************************************************************************************************

      use mod_global_variables, only :& 
                                       type_random_material_KL&
                                      ,ig_myrank&
                                      ,IG_LST_UNIT

      implicit none

      type(type_random_material_KL), intent(in)                 :: KLx
      type(type_random_material_KL), intent(in)                 :: KLy
      type(type_random_material_KL), intent(in)                 :: KLz

      integer(kind=IXP), dimension(:), allocatable, intent(out) :: ind_cut

      real   (kind=RXP), dimension(:), allocatable              :: l
      real   (kind=RXP)                                         :: sum_l
                                                                
      integer(kind=IXP), dimension(:), allocatable              :: ind
      integer(kind=IXP)                                         :: nx
      integer(kind=IXP)                                         :: ny
      integer(kind=IXP)                                         :: nz
      integer(kind=IXP)                                         :: nmod_cut
      integer(kind=IXP)                                         :: i
      integer(kind=IXP)                                         :: j
      integer(kind=IXP)                                         :: k
      integer(kind=IXP)                                         :: ii
      integer(kind=IXP)                                         :: ios

      nx = KLx%n
      ny = KLy%n
      nz = KLz%n

      ios = init_array_real(l  ,nx*ny*nz,"mod_random_field:KL_eigsort:l")
      ios = init_array_int (ind,nx*ny*nz,"mod_random_field:KL_eigsort:ind")

!
!---->compute multiplication of lambda_k

      ii = 0_IXP

      do i = 1_IXP,nx
         do j = 1_IXP,ny
            do k = 1_IXP,nz

               ii = ii + 1_IXP

               l(ii) = KLx%l(i)*KLy%l(j)*KLz%l(k)

            enddo
         enddo
      enddo

!
!---->sort multiplication of lambda_k
      call hpsort_eps_epw(nx*ny*nz,l,ind,EPSILON_MACHINE_RXP)

      sum_l = 0.0_RXP

      nmod_cut = 1_IXP

      do while ( (sum_l < 0.90_RXP) .and. (nmod_cut < nx*ny*nz) )

         sum_l = sum_l + l(nmod_cut)

         nmod_cut = nmod_cut + 1_IXP

      enddo

      if (ig_myrank == ZERO_IXP) then

        write(IG_LST_UNIT,'(/,A    )') "random medium by KL expansion"
        write(IG_LST_UNIT,'(A,E15.7)') " --> sum of eigenvalue = ",sum_l
        write(IG_LST_UNIT,'(A,I10  )') " --> number of modes   = ",nmod_cut
      
      endif

      ios = init_array_int(ind_cut,nmod_cut,"mod_random_field:KL_eigsort:ind_cut")

      ind_cut(1_IXP:nmod_cut) = ind(1_IXP:nmod_cut)

      return

!***************************************************************************************************************
   end subroutine KL_sort
!***************************************************************************************************************


!
!
!***************************************************************************************************************
   subroutine KL_init_distribution(il,ind,r)
!***************************************************************************************************************

      use mpi

      use mod_global_variables, only : ig_mpi_comm_simu&
                                     , ig_myrank&
                                     , cg_prefix&
                                     , get_newunit

      implicit none

      integer(kind=IXP)                           , intent(in ) :: il
      integer(kind=IXP), dimension(:)             , intent(in ) :: ind
      real   (kind=RXP), dimension(:), allocatable, intent(out) :: r

      integer(kind=IXP)                                         :: i
      integer(kind=IXP)                                         :: n
      integer(kind=IXP)                                         :: ios
      integer(kind=IXP)                                         :: myunit

      character(len=3)                                          :: cl

      n = size(ind)

!
!---->init random normal values

      ios = init_array_real(r,n,"mod_random_field:KL_init_distribution:r")

      do i = ONE_IXP,n

         r(i) = rand_normal()

      enddo

!                                                                                                                      
!---->for safety cpu 0 broadcast array r(:) to all cpus (loop above could be done by cpu 0 only)                       
      call mpi_bcast(r,n,MPI_REAL,ZERO_IXP,ig_mpi_comm_simu,ios)

!
!---->cpu 0 save array r
      if (ig_myrank == ZERO_IXP) then

        write(cl,'(I3.3)') il

        open(unit=get_newunit(myunit),file=trim(cg_prefix)//".l"//trim(cl)//".klr")

        write(myunit,'(E15.7)') r(:)

        close(myunit)

      endif

      return

!***************************************************************************************************************
   end subroutine KL_init_distribution
!***************************************************************************************************************


!
!
!***************************************************************************************************************
   subroutine KL_deallocate(tl_layer)
!***************************************************************************************************************

      use mod_global_variables, only :&
                                      type_layer&
                                     ,error_stop

      implicit none

      type(type_layer), dimension(:), intent(inout) :: tl_layer

      integer(kind=IXP)                             :: nl
      integer(kind=IXP)                             :: il
      integer(kind=IXP)                             :: ier
      integer(kind=IXP)                             :: ios

      ier = ZERO_IXP
      ios = ZERO_IXP

      nl = size(tl_layer)

      do il = ONE_IXP,nl

         if (allocated(tl_layer(il)%modes_ind)) deallocate(tl_layer(il)%modes_ind,stat=ios) ; ier = ier + ios
         if (allocated(tl_layer(il)%elastic  )) deallocate(tl_layer(il)%elastic  ,stat=ios) ; ier = ier + ios
         if (allocated(tl_layer(il)%anelastic)) deallocate(tl_layer(il)%anelastic,stat=ios) ; ier = ier + ios
         if (allocated(tl_layer(il)%random   )) deallocate(tl_layer(il)%random   ,stat=ios) ; ier = ier + ios
         if (allocated(tl_layer(il)%KLx%r    )) deallocate(tl_layer(il)%KLx%r    ,stat=ios) ; ier = ier + ios
         if (allocated(tl_layer(il)%KLy%r    )) deallocate(tl_layer(il)%KLy%r    ,stat=ios) ; ier = ier + ios
         if (allocated(tl_layer(il)%KLz%r    )) deallocate(tl_layer(il)%KLz%r    ,stat=ios) ; ier = ier + ios
         if (allocated(tl_layer(il)%KLx%l    )) deallocate(tl_layer(il)%KLx%l    ,stat=ios) ; ier = ier + ios
         if (allocated(tl_layer(il)%KLy%l    )) deallocate(tl_layer(il)%KLy%l    ,stat=ios) ; ier = ier + ios
         if (allocated(tl_layer(il)%KLz%l    )) deallocate(tl_layer(il)%KLz%l    ,stat=ios) ; ier = ier + ios

      enddo

      if (ier /= ZERO_IXP) then

         call error_stop("error in subroutine KL_deallocate")

      endif

      return

!***************************************************************************************************************
   end subroutine KL_deallocate
!***************************************************************************************************************


!
!
!***************************************************************************************************************
   subroutine find_roots(x,fx,r)
!***************************************************************************************************************

      implicit none

      real   (kind=RXP), dimension(:)             , intent(in)  :: x
      real   (kind=RXP), dimension(:)             , intent(in)  :: fx
      real   (kind=RXP), dimension(:), allocatable, intent(out) :: r

      real   (kind=RXP), dimension(:), allocatable              :: sngfx

      integer(kind=IXP)                                         :: i
      integer(kind=IXP)                                         :: n
      integer(kind=IXP)                                         :: nroots
      integer(kind=IXP)                                         :: ios

      n = size(fx)

      ios = init_array_real(sngfx,n,"mod_random_field:find_roots:sngfx")

!
!---->init sngfx with sign of fx

      do i = ONE_IXP,n

         sngfx(i) = sign(ONE_RXP,fx(i))

      enddo

!
!---->first pass to count the number of roots

      nroots = ZERO_IXP

      do i = TWO_IXP,n

         if (sngfx(i) /= sngfx(i-ONE_IXP)) nroots = nroots + ONE_IXP

      enddo

!
!---->second pass to store the roots in array r

      ios = init_array_real(r,nroots,"mod_random_field:find_roots:r")

      nroots = ZERO_IXP

      do i = TWO_IXP,n

         if (sngfx(i) /= sngfx(i-ONE_IXP)) then

            nroots = nroots + ONE_IXP

            r(nroots) = x(i-ONE_IXP) + 0.5_RXP * (x(i) - x(i-ONE_IXP))

         endif

      enddo

      return

!***************************************************************************************************************
   end subroutine find_roots
!***************************************************************************************************************


!
!
!***************************************************************************************************************
   real(kind=RXP) function f1(x,Lc)
!***************************************************************************************************************

      implicit none

      real(kind=RXP), intent(in) :: x
      real(kind=RXP), intent(in) :: Lc

      f1 = (ONE_RXP - Lc * x * tan(0.5_RXP*x)) * (Lc * x + tan(0.5_RXP*x))

      return

!***************************************************************************************************************
   end function f1
!***************************************************************************************************************


!
!
!***************************************************************************************************************
   real(kind=RXP) function rand_normal()
!***************************************************************************************************************

   implicit none

   
   real   (kind=RXP)             :: half = 0.5_RXP
                                 
   real   (kind=RXP)             :: s  =  0.449871_RXP
   real   (kind=RXP)             :: t  = -0.386595_RXP
   real   (kind=RXP)             :: a  =  0.19600_RXP
   real   (kind=RXP)             :: b  =  0.25472_RXP
   real   (kind=RXP)             :: r1 =  0.27597_RXP
   real   (kind=RXP)             :: r2 =  0.27846_RXP
   real   (kind=RXP)             :: u, v, x, y, q

   do
   
     call random_number(u)
     call random_number(v)

     v = 1.7156_RXP * (v - half)
   
     x = u - s
     y = ABS(v) - t
     q = x**2_IXP + y*(a*y - b*x)
   
     if (q < r1) exit
   
     if (q > r2) cycle
   
     if (v**2_IXP < -4.0_RXP*log(u)*u**2_IXP) exit
   
   end do
   
   rand_normal = v/u

!***************************************************************************************************************
   end function rand_normal
!***************************************************************************************************************


!
!
!***************************************************************************************************************
   real(kind=RXP) function rand_normal_marsaglia() result(nr)
!***************************************************************************************************************

   implicit none

   real(kind=RXP) :: ur1
   real(kind=RXP) :: ur2
   real(kind=RXP) :: s

   do

      call random_number(ur1)
      call random_number(ur2)

      ur1 = ur1 * 2.0_RXP - 1.0_RXP
      ur2 = ur2 * 2.0_RXP - 1.0_RXP
  
      s = ur1*ur1 + ur2*ur2
 
      if(s >= 1.0_RXP) cycle
  
      nr = ur1 * sqrt(-2.0_RXP*log(s)/s)

      return

   enddo
   
!***************************************************************************************************************
   end function rand_normal_marsaglia
!***************************************************************************************************************


!
!
!***************************************************************************************************************
   subroutine init_random_seed(ext)
!***************************************************************************************************************

      use mpi

      use mod_global_variables, only :&
                                       ig_mpi_comm_simu&
                                     , ig_myrank&
                                     , cg_prefix&
                                     , error_stop&
                                     , IG_LST_UNIT

      implicit none

      character(len=3)               , intent(in)  :: ext

      integer(kind=IXP), dimension(:), allocatable :: myseed

      integer(kind=IXP)                            :: ios
      integer(kind=IXP)                            :: myunit
      integer(kind=IXP)                            :: nseed
                                                   
      logical(kind=IXP)                            :: is_seed_exist

      character(len=CIL)                           :: info

      if ( .not.((ext == "rsm") .or. (ext == "rss")) ) then !rsm=random seed medium. rss=random seed source

         write(info,'(a)') "error in mod_random_field_kl:init_random_seed: unknown file extension"

         call error_stop(info)

      endif

      is_seed_exist = .false.

      call random_seed(size=nseed)

      ios = init_array_int(myseed,nseed,"mod_random_field:init_random_seed:myseed")

      if (ig_myrank == ZERO_IXP) then

         inquire(file=trim(cg_prefix)//"."//trim(ext),exist=is_seed_exist)
        
         if (is_seed_exist) then
        
            open(newunit=myunit,file=trim(cg_prefix)//"."//trim(ext),action="read")
        
            read(myunit,*) myseed(:)
        
            close(myunit)

         else
         
            call random_seed() !ensure new seeds number

            call random_seed(get=myseed) !get the seeds
        
         endif

      endif

!
!---->cpu 0 broadcasts its seeds to garantee the same random number generation by the other cpus

      call mpi_bcast(myseed,nseed,MPI_INTEGER,ZERO_IXP,ig_mpi_comm_simu,ios)

      call random_seed(put=myseed) !force the seeds for all cpus with the ones get from cpu0

      if (ig_myrank == ZERO_IXP) then

         if (ext == "rsm") then

            write(IG_LST_UNIT,'(/,a)') "random seeds for generating stochastic medium properties"
            write(IG_LST_UNIT,*      ) myseed(:)

         elseif (ext == "rss") then

            write(IG_LST_UNIT,'(/,a)') "random seeds for generating stochastic source properties"
            write(IG_LST_UNIT,*      ) myseed(:)

         endif

      endif

      return

!***************************************************************************************************************
   end subroutine init_random_seed
!***************************************************************************************************************


!                                                                            
! Copyright (C) 2010-2016 Samuel Ponce', Roxana Margine, Carla Verdi, Feliciano Giustino 
! Copyright (C) 2007-2009 Jesse Noffsinger, Brad Malone, Feliciano Giustino  
!                                                                            
! This file is distributed under the terms of the GNU General Public         
! License.
!                                                                            
! Adapted from flib/hpsort_eps
!---------------------------------------------------------------------
!***********************************************************************************************************************************************************************************
   subroutine hpsort_eps_epw (n, ra, ind, eps)
!***********************************************************************************************************************************************************************************
!---------------------------------------------------------------------
! sort an array ra(1:n) into ascending order using heapsort algorithm,
! and considering two elements being equal if their values differ
! for less than "eps".
! n is input, ra is replaced on output by its sorted rearrangement.
! create an index table (ind) by making an exchange in the index array
! whenever an exchange is made on the sorted data array (ra).
! in case of equal values in the data array (ra) the values in the
! index array (ind) are used to order the entries.
! if on input ind(1)  = 0 then indices are initialized in the routine,
! if on input ind(1) != 0 then indices are assumed to have been
!                initialized before entering the routine and these
!                indices are carried around during the sorting process
!
! no work space needed !
! free us from machine-dependent sorting-routines !
!
! adapted from Numerical Recipes pg. 329 (new edition)

      implicit none  
 
      !-input/output variables
      integer(kind=IXP), intent(in)   :: n  
      real   (kind=RXP), intent(in)   :: eps
  
      integer(kind=IXP)               :: ind (n)  
      real   (kind=RXP)               :: ra (n)
                                      
      !-local variables              
      integer(kind=IXP)               :: i, ir, j, l, iind  
      real   (kind=RXP)               :: rra  
 
      ! initialize index array
      IF (ind (1_IXP) .eq.0_IXP) then  
         DO i = 1_IXP, n  
            ind (i) = i  
         ENDDO
      ENDIF
 
      ! nothing to order
      IF (n.lt.2_IXP) return  
      ! initialize indices for hiring and retirement-promotion phase
      l = n / 2_IXP + 1_IXP  
  
      ir = n  
  
      sorting: do 
      
        ! still in hiring phase
        IF ( l .gt. 1_IXP ) then  
           l    = l - 1_IXP  
           rra  = ra (l)  
           iind = ind (l)  
           ! in retirement-promotion phase.
        ELSE  
           ! clear a space at the end of the array
           rra  = ra (ir)  
           !
           iind = ind (ir)  
           ! retire the top of the heap into it
           ra (ir) = ra (1_IXP)  
           !
           ind (ir) = ind (1_IXP)  
           ! decrease the size of the corporation
           ir = ir - 1_IXP
           ! done with the last promotion
           IF ( ir .eq. 1_IXP) then  
              ! the least competent worker at all !
              ra (1_IXP)  = rra  
              !
              ind (1_IXP) = iind  
              exit sorting  
           ENDIF
        ENDIF
        ! wheter in hiring or promotion phase, we
        i = l  
        ! set up to place rra in its proper level
        j = l + l  
        !
        DO while ( j .le. ir )  
           IF ( j .lt. ir ) then  
              ! compare to better underling
              IF ( hslt( ra (j),  ra (j + 1_IXP) ) ) then  
                 j = j + 1_IXP  
              !else if ( .not. hslt( ra (j+1),  ra (j) ) ) then
                 ! this means ra(j) == ra(j+1) within tolerance
               !  if (ind (j) .lt.ind (j + 1) ) j = j + 1
              ENDIF
           ENDIF
           ! demote rra
           IF ( hslt( rra, ra (j) ) ) then  
              ra (i) = ra (j)  
              ind (i) = ind (j)  
              i = j  
              j = j + j  
           !else if ( .not. hslt ( ra(j) , rra ) ) then
              !this means rra == ra(j) within tolerance
              ! demote rra
             ! if (iind.lt.ind (j) ) then
             !    ra (i) = ra (j)
             !    ind (i) = ind (j)
             !    i = j
             !    j = j + j
             ! else
                 ! set j to terminate do-while loop
             !    j = ir + 1
             ! endif
              ! this is the right place for rra
           ELSE
              ! set j to terminate do-while loop
              j = ir + 1_IXP
           ENDIF
        ENDDO
        ra (i) = rra  
        ind (i) = iind
  
      ENDDO sorting
  
      call reverse_real(n,ra) 
      call reverse_int (n,ind) 
  
      return

contains

  !  internal function 
  !  compare two real number and return the result

!***********************************************************************************************************************************************************************************
  logical function hslt( a, b)
!***********************************************************************************************************************************************************************************

    REAL(kind=RXP) :: a, b

    IF( abs(a-b) <  eps ) then
      hslt = .false.
    ELSE
      hslt = ( a < b )
    end if

!***********************************************************************************************************************************************************************************
  end function hslt
!***********************************************************************************************************************************************************************************

   !internal function to reverse array of type real
!***********************************************************************************************************************************************************************************
   subroutine  reverse_real(n,a)
!***********************************************************************************************************************************************************************************
   IMPLICIT  NONE

   integer(kind=IXP), intent(in)                    :: n
   real   (kind=RXP), dimension(n), intent(inout)   :: a      ! input array

   real   (kind=RXP)                                :: temp
   integer(kind=IXP)                                :: head           ! pointer moving forward
   integer(kind=IXP)                                :: tail           ! pointer moving backward

   head = 1_IXP                         ! start with the beginning
   tail = n                             ! start with the end
   do                                   ! for each pair...
      if (head >= tail)  exit           !    if head crosses tail, exit
      temp    = a(head)                 !    otherwise, swap them
      a(head) = a(tail)
      a(tail) = temp
      head    = head + 1_IXP            !    move forward
      tail    = tail - 1_IXP            !    move backward
   enddo

!***********************************************************************************************************************************************************************************
   end subroutine  reverse_real
!***********************************************************************************************************************************************************************************

   !internal function to reverse array of type integer
!***********************************************************************************************************************************************************************************
   subroutine  reverse_int(n,a)
!***********************************************************************************************************************************************************************************
   implicit  none

   integer(kind=IXP), intent(in)                    :: n
   integer(kind=IXP), dimension(n), intent(inout)   :: a      ! input array

   integer(kind=IXP)                                :: temp
   integer(kind=IXP)                                :: head           ! pointer moving forward
   integer(kind=IXP)                                :: tail           ! pointer moving backward

   head = 1_IXP                         ! start with the beginning
   tail = n                             ! start with the end
   do                                   ! for each pair...
      if (head >= tail)  exit           !    if head crosses tail, exit
      temp    = a(head)                 !    otherwise, swap them
      a(head) = a(tail)
      a(tail) = temp
      head    = head + 1_IXP            !    move forward
      tail    = tail - 1_IXP            !    move backward
   enddo

!***********************************************************************************************************************************************************************************
   end subroutine  reverse_int
!***********************************************************************************************************************************************************************************

!***********************************************************************************************************************************************************************************
   end subroutine hpsort_eps_epw
!***********************************************************************************************************************************************************************************

end module mod_random_field 
