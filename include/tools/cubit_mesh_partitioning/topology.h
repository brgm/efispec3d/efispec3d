//!>!===================================================================================================================================!<!
//!>!                                                        EFISPEC3D                                                                  !<!
//!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
//!>!                                                                                                                                   !<!
//!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
//!>!                                                                                                                                   !<!
//!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                                                 http://efispec.free.fr                                                            !<!
//!>!                                                                                                                                   !<!
//!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
//!>!                                                                                                                                   !<!
//!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
//!>!                                                                David    MICHEA                                                    !<!
//!>!                                                                Philippe THIERRY                                                   !<!
//!>!                                                                Sylvain  JUBERTIE                                                  !<!
//!>!                                                                Emmanuel CHALJUB                                                   !<!
//!>!                                                                Francois LAVOUE                                                    !<!
//!>!                                                                Tom      BUDON                                                     !<!
//!>!                                                                Emmanuel MELIN                                                     !<!
//!>!                                                                                                                                   !<!
//!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
//!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
//!>!                                                                                                                                   !<!
//!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
//!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
//!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
//!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
//!>!                           "http://www.cecill.info".                                                                               !<!
//!>!                                                                                                                                   !<!
//!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
//!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
//!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
//!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
//!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
//!>!                                                                                                                                   !<!
//!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
//!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
//!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
//!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
//!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
//!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
//!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
//!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
//!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
//!>!                           securite.                                                                                               !<!
//!>!                                                                                                                                   !<!
//!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
//!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
//!>!                                                                                                                                   !<!
//!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
//!>!                           motion using a finite spectral-element method.                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
//!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
//!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
//!>!                           version.                                                                                                !<!
//!>!                                                                                                                                   !<!
//!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
//!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
//!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
//!>!                                                                                                                                   !<!
//!>!                           You should have received a copy of the GNU General Public License along with                            !<!
//!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  3 ---> Thirdparty libraries                                                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
//!>!                                                                                                                                   !<!
//!>!                             --> METIS 5.1.0                                                                                       !<! 
//!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
//!>!                                                                                                                                   !<!
//!>!                             --> Lib_VTK_IO                                                                                        !<!
//!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
//!>!                                                                                                                                   !<!
//!>!                             --> INTERP_LINEAR                                                                                     !<!
//!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
//!>!                                                                                                                                   !<!
//!>!                             --> FLASProc                                                                                          !<!
//!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
//!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
//!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                             --> EXODUS II                                                                                         !<!
//!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
//!>!                                                                                                                                   !<!
//!>!                             --> NETCDF                                                                                            !<!
//!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                             --> HDF5                                                                                              !<!
//!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
//!>!                                                                                                                                   !<!
//!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
//!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
//!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
//!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
//!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
//!>!                           Computers & Structures, 245, 106459.                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
//!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
//!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
//!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
//!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
//!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
//!>!                                                                                                                                   !<!
//!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
//!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
//!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
//!>!                           Journal International, 201(1), 90-111.                                                                  !<!
//!>!                                                                                                                                   !<!
//!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
//!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
//!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
//!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
//!>!                                                                                                                                   !<!
//!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
//!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
//!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
//!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
//!>!                                                                                                                                   !<!
//!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
//!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
//!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
//!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
//!>!                           170(1), 43-64.                                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
//!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
//!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                  5 ---> Enjoy !                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!===================================================================================================================================!<!
// To use efispec3D numerotation
#define FACE1 0
#define FACE2 1
#define FACE3 2
#define FACE4 3
#define FACE5 4
#define FACE6 5

#define EDGE1 0
#define EDGE2 1
#define EDGE3 2
#define EDGE4 3
#define EDGE5 4
#define EDGE6 5
#define EDGE7 6
#define EDGE8 7
#define EDGE9 8
#define EDGE10 9
#define EDGE11 10
#define EDGE12 11

// efispec3D to cubit
#define CORNER1 3
#define CORNER2 2
#define CORNER3 1
#define CORNER4 0
#define CORNER5 7
#define CORNER6 6
#define CORNER7 5
#define CORNER8 4

int edge2efispec[12] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
int face2efispec[6] = {1, 2, 3, 4, 5, 6};
int corner2efispec[8] = {4, 3, 2, 1, 8, 7, 6, 5};
int cornerefispec2cubit[8] = {3, 2, 1, 0, 7, 6, 5, 4};

// Initial state : {Horiz vect, Vert vect} using nodes 1 or 7 in bottom left corner for each face.
// H or V give direction, + or - give sens : toward up & right is +, - for down and left
// 1 is for i, 2 for j, 3 for k
// ie for face 1, considering it with node1 in the bottom left corner, K is the horizontal vetor directed toward right, j is vertical directed toward up
#define OR_F1 {3, 2}
#define OR_F2 {1, 3}
#define OR_F3 {-1, -2}
#define OR_F4 {-3, -1}
#define OR_F5 {2, 1}
#define OR_F6 {-2, -3}
char face_orientation[6][2] = {OR_F1, OR_F2, OR_F3, OR_F4, OR_F5, OR_F6};

#define F1F1 {1,0,3,2}
#define F1F2 {6,2,3,7}
#define F1F3 {2,6,5,1}
#define F1F4 {0,1,5,4}
#define F1F5 {4,7,3,0}
#define F1F6 {7,4,5,6}

#define F2F1 {1,0,3,2}
#define F2F2 {6,2,3,7}
#define F2F3 {2,6,5,1}
#define F2F4 {0,1,5,4}
#define F2F5 {4,7,3,0}
#define F2F6 {7,4,5,6}

#define F3F1 {3,2,1,0}
#define F3F2 {3,7,6,2}
#define F3F3 {5,1,2,6}
#define F3F4 {5,4,0,1}
#define F3F5 {3,0,4,7}
#define F3F6 {5,6,7,4}

#define F4F1 {0,3,2,1}
#define F4F2 {2,3,7,6}
#define F4F3 {6,5,1,2}
#define F4F4 {1,5,4,0}
#define F4F5 {7,3,0,4}
#define F4F6 {4,5,6,7}

#define F5F1 {3,2,1,0}
#define F5F2 {3,7,6,2}
#define F5F3 {5,1,2,6}
#define F5F4 {5,4,0,1}
#define F5F5 {3,0,4,7}
#define F5F6 {5,6,7,4}

#define F6F1 {1,0,3,2}
#define F6F2 {6,2,3,7}
#define F6F3 {2,6,5,1}
#define F6F4 {0,1,5,4}
#define F6F5 {4,7,3,0}
#define F6F6 {7,4,5,6}

// selon la face, indice du premier noeud trouv� dans l'ordre de parcours cubit -> on regarde le noeud d'en face pour en d�duire l'orientation
// ne sert pas dans le code, juste un pense bete pour verifier le code spaghetti ci-dessus
// char node2test[6] = {0, 2, 1, 0, 0, 4}

// dim 1 : num face source
// dim 2 : num face target
// dim 3 : num orientation
// content : first target node corresponding to first source node
// ie:
//	for (i=0; i<4; i++)
//		if (connex_nodes[4] == face2faceRot[num_conn_source][num_conn_target][i]) return i;
int face2faceRot[6][6][4]=	{	{F1F1, F1F2, F1F3, F1F4, F1F5, F1F6	},	// face 1 source
								{F2F1, F2F2, F2F3, F2F4, F2F5, F2F6	},	// face 2 source
								{F3F1, F3F2, F3F3, F3F4, F3F5, F3F6	},	// face 3 source
								{F4F1, F4F2, F4F3, F4F4, F4F5, F4F6	},	// face 4 source
								{F5F1, F5F2, F5F3, F5F4, F5F5, F5F6	},	// face 5 source
								{F6F1, F6F2, F6F3, F6F4, F6F5, F6F6	}	// face 6 source
							};

// define arbitrary order for points composing an edge to define a sens
// l'arete est dirig�e dans le sens du vecteur unit� (du repere orthonorm�) qui lui est colin�aire :i, j ou k
// attention, les numeros suivants sont les numeros cubits commen�ants en 0
#define EDGE1_ORDER {3,2}
#define EDGE2_ORDER {2,1}
#define EDGE3_ORDER {0,1}
#define EDGE4_ORDER {3,0}
#define EDGE5_ORDER {7,6}
#define EDGE6_ORDER {6,5}
#define EDGE7_ORDER {4,5}
#define EDGE8_ORDER {7,4}
#define EDGE9_ORDER {3,7}
#define EDGE10_ORDER {2,6}
#define EDGE11_ORDER {1,5}
#define EDGE12_ORDER {0,4}

int edgeOrder[12][2] = {	EDGE1_ORDER,
							EDGE2_ORDER,
							EDGE3_ORDER,
							EDGE4_ORDER,
							EDGE5_ORDER,
							EDGE6_ORDER,
							EDGE7_ORDER,
							EDGE8_ORDER,
							EDGE9_ORDER,
							EDGE10_ORDER,
							EDGE11_ORDER,
							EDGE12_ORDER};



	


