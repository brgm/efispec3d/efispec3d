//!>!===================================================================================================================================!<!
//!>!                                                        EFISPEC3D                                                                  !<!
//!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
//!>!                                                                                                                                   !<!
//!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
//!>!                                                                                                                                   !<!
//!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                                                 http://efispec.free.fr                                                            !<!
//!>!                                                                                                                                   !<!
//!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
//!>!                                                                                                                                   !<!
//!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
//!>!                                                                David    MICHEA                                                    !<!
//!>!                                                                Philippe THIERRY                                                   !<!
//!>!                                                                Sylvain  JUBERTIE                                                  !<!
//!>!                                                                Emmanuel CHALJUB                                                   !<!
//!>!                                                                Francois LAVOUE                                                    !<!
//!>!                                                                Tom      BUDON                                                     !<!
//!>!                                                                Emmanuel MELIN                                                     !<!
//!>!                                                                                                                                   !<!
//!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
//!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
//!>!                                                                                                                                   !<!
//!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
//!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
//!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
//!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
//!>!                           "http://www.cecill.info".                                                                               !<!
//!>!                                                                                                                                   !<!
//!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
//!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
//!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
//!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
//!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
//!>!                                                                                                                                   !<!
//!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
//!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
//!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
//!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
//!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
//!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
//!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
//!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
//!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
//!>!                           securite.                                                                                               !<!
//!>!                                                                                                                                   !<!
//!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
//!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
//!>!                                                                                                                                   !<!
//!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
//!>!                           motion using a finite spectral-element method.                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
//!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
//!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
//!>!                           version.                                                                                                !<!
//!>!                                                                                                                                   !<!
//!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
//!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
//!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
//!>!                                                                                                                                   !<!
//!>!                           You should have received a copy of the GNU General Public License along with                            !<!
//!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
//!>!                                                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                  3 ---> Thirdparty libraries                                                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
//!>!                                                                                                                                   !<!
//!>!                             --> METIS 5.1.0                                                                                       !<! 
//!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
//!>!                                                                                                                                   !<!
//!>!                             --> Lib_VTK_IO                                                                                        !<!
//!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
//!>!                                                                                                                                   !<!
//!>!                             --> INTERP_LINEAR                                                                                     !<!
//!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
//!>!                                                                                                                                   !<!
//!>!                             --> FLASProc                                                                                          !<!
//!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
//!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
//!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                             --> EXODUS II                                                                                         !<!
//!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
//!>!                                                                                                                                   !<!
//!>!                             --> NETCDF                                                                                            !<!
//!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
//!>!                                                                                                                                   !<!
//!>!                             --> HDF5                                                                                              !<!
//!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
//!>!                                                                                                                                   !<!
//!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
//!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
//!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
//!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
//!>!                                                                                                                                   !<!
//!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
//!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
//!>!                           Computers & Structures, 245, 106459.                                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
//!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
//!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
//!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
//!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
//!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
//!>!                                                                                                                                   !<!
//!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
//!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
//!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
//!>!                           Journal International, 201(1), 90-111.                                                                  !<!
//!>!                                                                                                                                   !<!
//!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
//!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
//!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
//!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
//!>!                                                                                                                                   !<!
//!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
//!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
//!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
//!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
//!>!                                                                                                                                   !<!
//!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
//!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
//!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
//!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
//!>!                           170(1), 43-64.                                                                                          !<!
//!>!                                                                                                                                   !<!
//!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
//!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
//!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
//!>!                                                                                                                                   !<!
//!>!                  5 ---> Enjoy !                                                                                                   !<!
//!>!                                                                                                                                   !<!
//!>!===================================================================================================================================!<!
#include "metis.h"
#include "float.h"

// should be defined with -D in a target of makefile
#define VERBOSE

// debug print
#define CHECK printf("<<< OK, func %s, line %d >>>\n", __FUNCTION__, __LINE__); fflush(stdout);


#define debugmode 0
#define DBG if (debugmode) {printf("file %s, function %s, line %d\n", __FILE__, __FUNCTION__, __LINE__);}
#define DBGR if (debugmode) {printf("rank : %d, file %s, function %s, line %d\n", rank, __FILE__, __FUNCTION__, __LINE__);}

#if 0
# define INFO(fmt, ...) fprintf(stdout,fmt,##__VA_ARGS__)
#else
# define INFO(fmt, ...) (void) 0
#endif

// macros
#define MALLOC(TYPE, NUM) (TYPE*)malloc((NUM)*sizeof(TYPE))
#define CALLOC(TYPE, NUM) (TYPE*)calloc((NUM)*sizeof(TYPE))

//#define STOP(msg) {printf("\n!!! %s -> stop\n\n", msg); fflush(stdout); exit(1);}
#define STOP(msg) {printf("\n!!! %s -> stop\n\n", msg); fflush(stdout);}
#ifdef VERBOSE
#define MSG(msg) printf("%s\n",msg);
#else
#define MSG(msg) ;
#endif

#define N_BLOCK_MAX 99
#define LENMAX 500
#define MAX_FILENAME_LEN 70

// might be wrong for 27 nodes elts
#define N_NODES_QUAD 4

// patterns for element type recognition in cubit mesh file
#define HEXA_8_HDR  "TYPE=C3D8R, ELSET=l"
#define HEXA_27_HDR "TYPE=C3D27R, ELSET=l"
#define QUAD_P_HDR  "TYPE=S4R, ELSET=p"
#define QUAD_F_HDR  "TYPE=S4R, ELSET=f"

// TODO : define weights if no snapshots
#ifdef NO_SNAPSHOTS
#define HEXA_WEIGHT     1
#define QUAD_P_WEIGHT   0
#define QUAD_F_WEIGHT   0
#else
//#define HEXA_WEIGHT 23
//#define QUAD_P_WEIGHT 1
//#define QUAD_F_WEIGHT 48
#define HEXA_WEIGHT     1
#define QUAD_P_WEIGHT   0
#define QUAD_F_WEIGHT   0
#endif

// To use efispec3D numerotation
#define NFACE    6
#define NEDGE   12
#define NCORNER  8

#define FACE1 0
#define FACE2 1
#define FACE3 2
#define FACE4 3
#define FACE5 4
#define FACE6 5

#define EDGE1   0
#define EDGE2   1
#define EDGE3   2
#define EDGE4   3
#define EDGE5   4
#define EDGE6   5
#define EDGE7   6
#define EDGE8   7
#define EDGE9   8
#define EDGE10  9
#define EDGE11 10
#define EDGE12 11

// efispec3D to cubit
#define CORNER1 3
#define CORNER2 2
#define CORNER3 1
#define CORNER4 0
#define CORNER5 7
#define CORNER6 6
#define CORNER7 5
#define CORNER8 4

//int ex2efi_quad[4]		   = {0, 1, 2, 3};
//int ex2efi_hexa[8]		   = {0, 1, 2, 3, 4, 5, 6, 7};

int edge2efispec[12]       = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
int face2efispec[6]        = {1, 2, 3, 4, 5, 6};

// pour efispec, l'ordre des 27 noeuds est le suivant : 
// ordre des 8 coins
// ensuite : ordre des 12 arêtes
// ensuite : ordre des 6 faces
// 27e noeud : centre du cube
int corner2efispec[27]      = { 4, 3, 2, 1, 8, 7, 6, 5, // corners
                                11, 10, 9, 12, 20, 19, 18, 17, 15, 14, 13, 16, // edge centers
                                27, // cube center
                                21, 26, 25, 23, 24, 22 // face centers
                              };
int cornerefispec2cubit[8] = {3, 2, 1, 0, 7, 6, 5, 4};

// Initial state : {Horiz vect, Vert vect} using nodes 1 or 7 in bottom left corner for each face.
// H or V give direction, + or - give sens : toward up & right is +, - for down and left
// 1 is for i, 2 for j, 3 for k
// ie for face 1, considering it with node1 in the bottom left corner, K is the horizontal vetor directed toward right, j is vertical directed toward up
#define OR_F1 { 3,  2}
#define OR_F2 { 1,  3}
#define OR_F3 {-1, -2}
#define OR_F4 {-3, -1}
#define OR_F5 { 2,  1}
#define OR_F6 {-2, -3}
char face_orientation[6][2] = {OR_F1, OR_F2, OR_F3, OR_F4, OR_F5, OR_F6};

#define F1F1 {1,0,3,2}
#define F1F2 {6,2,3,7}
#define F1F3 {2,6,5,1}
#define F1F4 {0,1,5,4}
#define F1F5 {4,7,3,0}
#define F1F6 {7,4,5,6}

#define F2F1 {1,0,3,2}
#define F2F2 {6,2,3,7}
#define F2F3 {2,6,5,1}
#define F2F4 {0,1,5,4}
#define F2F5 {4,7,3,0}
#define F2F6 {7,4,5,6}

#define F3F1 {3,2,1,0}
#define F3F2 {3,7,6,2}
#define F3F3 {5,1,2,6}
#define F3F4 {5,4,0,1}
#define F3F5 {3,0,4,7}
#define F3F6 {5,6,7,4}

#define F4F1 {0,3,2,1}
#define F4F2 {2,3,7,6}
#define F4F3 {6,5,1,2}
#define F4F4 {1,5,4,0}
#define F4F5 {7,3,0,4}
#define F4F6 {4,5,6,7}

#define F5F1 {3,2,1,0}
#define F5F2 {3,7,6,2}
#define F5F3 {5,1,2,6}
#define F5F4 {5,4,0,1}
#define F5F5 {3,0,4,7}
#define F5F6 {5,6,7,4}

#define F6F1 {1,0,3,2}
#define F6F2 {6,2,3,7}
#define F6F3 {2,6,5,1}
#define F6F4 {0,1,5,4}
#define F6F5 {4,7,3,0}
#define F6F6 {7,4,5,6}

// selon la face, indice du premier noeud trouvé dans l'ordre de parcours cubit -> on regarde le noeud d'en face pour en déduire l'orientation
// ne sert pas dans le code, juste un pense bete pour verifier le code spaghetti ci-dessus
// char node2test[6] = {0, 2, 1, 0, 0, 4}

// dim 1 : num face source
// dim 2 : num face target
// dim 3 : num orientation
// content : first target node corresponding to first source node
// ie:
// for (i=0; i<4; i++)
//    if (connex_nodes[4] == face2faceRot[num_conn_source][num_conn_target][i]) return i;
int face2faceRot[6][6][4]= {  {F1F1, F1F2, F1F3, F1F4, F1F5, F1F6 }, // face 1 source
                              {F2F1, F2F2, F2F3, F2F4, F2F5, F2F6 }, // face 2 source
                              {F3F1, F3F2, F3F3, F3F4, F3F5, F3F6 }, // face 3 source
                              {F4F1, F4F2, F4F3, F4F4, F4F5, F4F6 }, // face 4 source
                              {F5F1, F5F2, F5F3, F5F4, F5F5, F5F6 }, // face 5 source
                              {F6F1, F6F2, F6F3, F6F4, F6F5, F6F6 }  // face 6 source
                           };

// define arbitrary order for points composing an edge to define a sens
// l'arete est dirigée dans le sens du vecteur unité (du repere orthonormé) qui lui est colinéaire :i, j ou k
// attention, les numeros suivants sont les numeros cubits commençants en 0
#define EDGE1_ORDER  {3,2}
#define EDGE2_ORDER  {2,1}
#define EDGE3_ORDER  {0,1}
#define EDGE4_ORDER  {3,0}
#define EDGE5_ORDER  {7,6}
#define EDGE6_ORDER  {6,5}
#define EDGE7_ORDER  {4,5}
#define EDGE8_ORDER  {7,4}
#define EDGE9_ORDER  {3,7}
#define EDGE10_ORDER {2,6}
#define EDGE11_ORDER {1,5}
#define EDGE12_ORDER {0,4}

int edgeOrder[12][2] = {   EDGE1_ORDER,
                           EDGE2_ORDER,
                           EDGE3_ORDER,
                           EDGE4_ORDER,
                           EDGE5_ORDER,
                           EDGE6_ORDER,
                           EDGE7_ORDER,
                           EDGE8_ORDER,
                           EDGE9_ORDER,
                           EDGE10_ORDER,
                           EDGE11_ORDER,
                           EDGE12_ORDER
                       };

typedef enum  {
	NONE   = 0,
	HEXA   = 1,
	QUAD_P = 2,
	QUAD_F = 3,
} elt_t;

typedef enum  {
	FACE   = 0,
	EDGE   = 1,
	CORNER = 2,
} topo_t;

// mesh struct
typedef struct {
  int hex27;
  int num_nodes_hexa;          // number of geometric nodes per hexahedron (8 or 27)
  int num_nodes_quad;          // number of geometric nodes per hexahedron (4 or 9)
  int num_node_per_dim;        // number of geometric nodes per direction (2 or 3)
  int npart;                   // number of procs (parts)
  idx_t ne, nn;                // The # of elements and nodes in the mesh
  idx_t nh, nq_parax, nq_surf; // number of hexa, quad parax, quad fsurf
  idx_t ncon;                  // The number of element balancing constraints (element weights)

  idx_t  *eptr, *eind;         // The CSR-structure storing the nodes in the elements elt -> node
  idx_t  *eptr_27, *eind_27;         // The CSR-structure storing the nodes in the elements elt -> node
  idx_t  *xadj, *adjncy;       // Elements adjacency graph (CSR format)
  idx_t  *xadj_hex, *adjncy_hex;// Hexa only adjacency graph (CSR format)
  real_t *ncoords_x;             // The x coordinates of geometric nodes of the elements
  real_t *ncoords_y;             // The y coordinates of geometric nodes of the elements
  real_t *ncoords_z;             // The z coordinates of geometric nodes of the elements
  idx_t  *part;                // Hexa partition table
  int	*layer;               // Layer of elmnts	 
  idx_t *vwgt;                // The weights of the vertices (hexa elts) of hexa adj graph
  elt_t *types;                // elements types
  float *xcoord;
  float *ycoord;
  float *zcoord;
} mesh_t;

// connection with neighbor information
typedef struct ci_t{
        idx_t num_neigh;// neigbor global number
        int num_conn;   // numface, numedge, numcorner
        int orientation;// connection orientation  0  0  s  v  v  s  v  v
                        // hor  vert	
                        // vv: 01=i, 10=j, 11=k s: 1=+, 0=-
        int defined;    // 1 if this struct is filled
        elt_t type;     // HEXA, QUAD_P, QUAD_F
        struct ci_t* next;
} conn_info;

// element information
typedef struct {
        int globalnum;        // global number
        int localnum;         // local number
        char outer;           // element is outer
	conn_info faces  [ 6];// face neighborhood
	conn_info edges  [12];// edge neighborhood
	conn_info corners[ 8];// corner neighborhood
} elt_info_t;

// proc information
typedef struct {
	elt_info_t** local_elts;	
	int nb_elt;
	int nb_ext;
	int nb_quad_p;
	int nb_quad_f;
	int nb_conn;
	char* connex;
	char filename[MAX_FILENAME_LEN];
} proc_info_t;

// convenience struct which hold all information
typedef struct {
	proc_info_t* proc;
	elt_info_t* elt;
} info_t;

// function defined outside in FORTRAN code : 
void extern init_gll_number(int* ihexa,int* ngll_total);

void extern propagate_gll_nodes_face(int* ihexa, int* iface, int* ielt_num, int* ielt_face, int* ielt_coty);

void extern propagate_gll_nodes_edge(int* ihexa, int* iedge, int* ielt_num, int* ielt_edge, int* ielt_coty);

void extern propagate_gll_nodes_corner(int* ihexa, int* inode, int* ielt_num, int* ielt_corner);

void extern propagate_gll_nodes_quad(int* ihexa, int* iface, int* iquad, int* global_gll_of_quad, int* nb_quad_p);
