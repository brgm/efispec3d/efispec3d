#!/bin/bash

echo
echo "WARNING: This script uses sudo to install with the package manager of the system the following packages : GCC, gfortran, CMake, Curl, Metis, ExodusII, MPICH, HDF5, NetCDF C, NetCDF Fortran"
echo
echo "If you don't have a root access, please read the Advanced Install instruction in INSTALL.md"
echo
read -p "Do you want to proceed? (yes/no) " yn
case $yn in 
	yes ) echo ok, we will proceed;;
	no ) echo exiting...;
		exit;;
	* ) echo invalid response;
		exit 1;;
esac
sudo -k
#sudo ls | grep dzqdesdwzeqfzeqfflsmkoidjsld  # <-avoid to  

# Check distribution

distrib=`cat /etc/os-release | grep "^ID="`
distribId=${distrib:3}

#Changed the name of RockyLinux to match the formats of other names
if [[ "$distribId" == '"rocky"' ]] ; then
  distribId="rocky"
fi

#Print distribution detected
echo
echo "$distribId distribution detected"
echo


#Execute the script only for this distributions :
if [[ "$distribId" == "ubuntu" ]] || [[ "$distribId" == "debian" ]] || [[ "$distribId" == "fedora" ]] || [[ "$distribId" == "rocky" ]] ; then

    #Define package management commands, package names and package minimal versions to install for each version

    if [[ "$distribId" == "ubuntu" ]] || [[ "$distribId" == "debian" ]] ; then

      packagesNames=("CC" "gfortran" "CMake" "Curl" "Metis" "ExodusII" "MPICH" "HDF5" "NetCDF C" "NetCDF Fortran")
      packages=("build-essential" "gfortran" "cmake" "libcurl4-openssl-dev" "libmetis-dev" "libexodusii-dev" "libmpich-dev" "libhdf5-dev" "libnetcdf-dev" "libnetcdff-dev")
      versionsMin=("0" "0" "3.16" "0" "5.1.0" "5.14.0" "3.1.0" "1.10.0" "4.7.3" "4.5.2")
      commandePackage="apt"

    else
      if [[ "$distribId" == "fedora" ]]; then

        packagesNames=("GCC" "gfortran" "CMake" "Curl" "Metis" "ExodusII" "MPICH" "HDF5" "NetCDF C" "NetCDF Fortran")
        packages=("gcc" "gcc-gfortran" "cmake" "libcurl-devel" "metis-devel" "exodusii-devel" "mpich-devel" "hdf5-devel" "netcdf-devel" "netcdf-fortran-devel")
        versionsMin=("0" "0" "3.16" "0" "5.1.0" "5.14.0" "3.1.0" "1.10.0" "4.7.3" "4.5.2")
        commandePackage="dnf"

        #install dpkg to compare package versions
        sudo $commandePackage install dpkg

      else
        if [[ "$distribId" == "rocky" ]]; then

          packagesNames=("GCC" "g++" "gfortran" "CMake" "Curl" "Metis" "ExodusII" "MPICH" "HDF5" "NetCDF C" "NetCDF Fortran")
          packages=("gcc" "gcc-c++" "gcc-gfortran" "cmake" "libcurl-devel" "metis-devel" "exo-devel" "mpich-devel" "hdf5-devel" "netcdf-devel" "netcdf-fortran-devel")
          versionsMin=("0" "0" "0" "3.16" "0" "5.1.0" "5.14.0" "3.1.0" "1.10.0" "4.7.3" "4.5.2")
          commandePackage="dnf"

          #add repository to install packages
          sudo $commandePackage install wget
          wget "https://download-ib01.fedoraproject.org/pub/epel/8/Everything/x86_64/Packages/e/epel-release-8-15.el8.noarch.rpm"
          sudo rpm -Uvh epel-release*rpm
          rm epel-release*rpm

          #install dpkg to compare package versions
          sudo $commandePackage install dpkg

        fi
      fi
    fi

    echo "Libraries installation : "
    echo

    #Print for mandatory libraries to compile EFISPEC3D
    echo "   >>> Mandatory compilers and libraries <<<   "

    for i in ${!packagesNames[@]};
    do

      #Install for each package

      packageName=${packagesNames[$i]}
      package=${packages[$i]}
      versionMin=${versionsMin[$i]}

      #Print for optional libraries
      if [[ "$packageName" == "HDF5" ]] ; then
        echo
        echo "   >>> Optional libraries <<<   "
        echo "   (to compile EFISPEC tools)"
      fi

      echo
      echo " -- Check $packageName -- "

      #Checks if libraries are installed with correct version

      if [[ "$distribId" == "ubuntu" ]] || [[ "$distribId" == "debian" ]] ; then
        lang=`locale | grep LANG= | cut -d= -f2 | cut -d_ -f1`
        if [[ "$lang" == "fr" ]]; then
          version=`apt-cache policy $package | grep "Installé"`
          numversion=${version:13}
        else
          version=`apt-cache policy $package | grep "Installed"`
          numversion=${version:13}
        fi
      else
        if [[ "$distribId" == "fedora" ]] || [[ "$distribId" == "rocky" ]] ; then
          version=`dnf info $package --installed | grep "^Version"`
          numversion=${version:15}
        else
          numversion=""
        fi
      fi

      #Install libraries that are not installed

      echo "$packageName version detected : $numversion"
      if [[ "$numversion" == "(none)" ]] || [[ "$numversion" == "(aucun)" ]]; then
        numversion=""
      fi

      if $(dpkg --compare-versions "$numversion" "ge" "$versionMin") ; then
        echo "$packageName version -- OK"
      else
        if [[ "$distribId" == "rocky" ]] ; then
          sudo $commandePackage --enablerepo=powertools install $package
        fi
          sudo $commandePackage install $package
      fi
    done


    if [[ "$distribId" == "fedora" ]] || [[ "$distribId" == "rocky" ]] ; then

      #add Path to mpich home and NetCDF_Fortran_INCLUDE_DIR
      export MPI_HOME=/usr/lib64/mpich/
      export NetCDF_Fortran_INCLUDE_DIR=/usr/lib64/gfortran/modules/

      #extract exodus include file to /usr/include (not always installed from package manager)
      if [ ! -f /usr/include/exodusII.inc ]; then
         sudo tar -xzvf src/thirdparty/exodus-6.09-patched.tar.gz --strip-components 3 -C /usr/include exodus-6.09-patched/forbind/include/exodusII.inc
      fi

    else

      #add Path to mpich home
      export MPI_HOME=/usr/lib/x86_64-linux-gnu/

    fi

    echo
    echo " ** End of installation of libraries for $distribId ** "
    echo

else
  echo "The auto-install on $distribId is not managed"
fi
