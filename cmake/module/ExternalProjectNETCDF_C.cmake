#!>!===================================================================================================================================!<!
#!>!                                                        EFISPEC3D                                                                  !<!
#!>!                                              (Elements FInis SPECtraux 3D)                                                        !<!
#!>!                                                                                                                                   !<!
#!>!                                     This file is part of the open-source code EFISPEC3D                                           !<!
#!>!                                                                                                                                   !<!
#!>!                                            >>>>>>> use it diligently <<<<<<<                                                      !<!
#!>!                                                                                                                                   !<!
#!>!                                                 http://efispec.free.fr                                                            !<!
#!>!                                                                                                                                   !<!
#!>!                                           https://gitlab.brgm.fr/brgm/efispec3d/                                                  !<!
#!>!                                                                                                                                   !<!
#!>!                                                                                                                                   !<!
#!>!                  1 ---> French License: CeCILL V2                                                                                 !<!
#!>!                                                                                                                                   !<!
#!>!                           Copyright BRGM 2009  contributeurs : Florent  DE MARTIN                                                 !<!
#!>!                                                                David    MICHEA                                                    !<!
#!>!                                                                Philippe THIERRY                                                   !<!
#!>!                                                                Sylvain  JUBERTIE                                                  !<!
#!>!                                                                Emmanuel CHALJUB                                                   !<!
#!>!                                                                Francois LAVOUE                                                    !<!
#!>!                                                                Tom      BUDON                                                     !<!
#!>!                                                                Emmanuel MELIN                                                     !<!
#!>!                                                                                                                                   !<!
#!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
#!>!                                                                                                                                   !<!
#!>!                           Ce logiciel est un programme informatique servant a resoudre l'equation du                              !<!
#!>!                           mouvement en trois dimensions via une methode des elements finis spectraux.                             !<!
#!>!                                                                                                                                   !<!
#!>!                           Ce logiciel est regi par la licence CeCILL soumise au droit francais et                                 !<!
#!>!                           respectant les principes de diffusion des logiciels libres. Vous pouvez                                 !<!
#!>!                           utiliser, modifier et/ou redistribuer ce programme sous les conditions de la                            !<!
#!>!                           licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA sur le site                            !<!
#!>!                           "http://www.cecill.info".                                                                               !<!
#!>!                                                                                                                                   !<!
#!>!                           En contrepartie de l'accessibilite au code source et des droits de copie, de                            !<!
#!>!                           modification et de redistribution accordes par cette licence, il n'est offert                           !<!
#!>!                           aux utilisateurs qu'une garantie limitee. Pour les memes raisons, seule une                             !<!
#!>!                           responsabilite restreinte pese sur l'auteur du programme, le titulaire des                              !<!
#!>!                           droits patrimoniaux et les concedants successifs.                                                       !<!
#!>!                                                                                                                                   !<!
#!>!                           A cet egard l'attention de l'utilisateur est attiree sur les risques associes                           !<!
#!>!                           au chargement, a l'utilisation, a la modification et/ou au developpement et a                           !<!
#!>!                           la reproduction du logiciel par l'utilisateur etant donne sa specificite de                             !<!
#!>!                           logiciel libre, qui peut le rendre complexe a manipuler et qui le reserve donc                          !<!
#!>!                           a des developpeurs et des professionnels avertis possedant des connaissances                            !<!
#!>!                           informatiques approfondies. Les utilisateurs sont donc invites a charger et                             !<!
#!>!                           tester l'adequation du logiciel a leurs besoins dans des conditions permettant                          !<!
#!>!                           d'assurer la securite de leurs systemes et ou de leurs donnees et, plus                                 !<!
#!>!                           generalement, a l'utiliser et l'exploiter dans les memes conditions de                                  !<!
#!>!                           securite.                                                                                               !<!
#!>!                                                                                                                                   !<!
#!>!                           Le fait que vous puissiez acceder a cet en-tete signifie que vous avez pris                             !<!
#!>!                           connaissance de la licence CeCILL et que vous en avez accepte les termes.                               !<!
#!>!                                                                                                                                   !<!
#!>!                                                                                                                                   !<!
#!>!                  2 ---> International license: GNU GPL V3                                                                         !<!
#!>!                                                                                                                                   !<!
#!>!                           EFISPEC3D is a computer program that solves the three-dimensional equations of                          !<!
#!>!                           motion using a finite spectral-element method.                                                          !<!
#!>!                                                                                                                                   !<!
#!>!                           Copyright (C) 2009 Florent DE MARTIN                                                                    !<!
#!>!                                                                                                                                   !<!
#!>!                           Contact: f.demartin at brgm.fr                                                                          !<!
#!>!                                                                                                                                   !<!
#!>!                           This program is free software: you can redistribute it and/or modify it under                           !<!
#!>!                           the terms of the GNU General Public License as published by the Free Software                           !<!
#!>!                           Foundation, either version 3 of the License, or (at your option) any later                              !<!
#!>!                           version.                                                                                                !<!
#!>!                                                                                                                                   !<!
#!>!                           This program is distributed in the hope that it will be useful, but WITHOUT ANY                         !<!
#!>!                           WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A                         !<!
#!>!                           PARTICULAR PURPOSE. See the GNU General Public License for more details.                                !<!
#!>!                                                                                                                                   !<!
#!>!                           You should have received a copy of the GNU General Public License along with                            !<!
#!>!                           this program. If not, see http://www.gnu.org/licenses/.                                                 !<!
#!>!                                                                                                                                   !<!
#!>!                                                                                                                                   !<!
#!>!                  3 ---> Thirdparty libraries                                                                                      !<!
#!>!                                                                                                                                   !<!
#!>!                           EFISPEC3D uses the following thirdparty libraries or source code                                        !<!
#!>!                                                                                                                                   !<!
#!>!                             --> METIS 5.1.0                                                                                       !<! 
#!>!                                 see http://glaros.dtc.umn.edu/gkhome/metis/metis/overview                                         !<!
#!>!                                                                                                                                   !<!
#!>!                             --> Lib_VTK_IO                                                                                        !<!
#!>!                                 see S. Zaghi's website: https://github.com/szaghi/Lib_VTK_IO                                      !<!
#!>!                                                                                                                                   !<!
#!>!                             --> INTERP_LINEAR                                                                                     !<!
#!>!                                 see J. Burkardt website: http://people.sc.fsu.edu/~jburkardt/                                     !<!
#!>!                                                                                                                                   !<!
#!>!                             --> FLASProc                                                                                          !<!
#!>!                                 see John Jiyang Hou's article on Code Project:                                                    !<!
#!>!                                 https://www.codeproject.com/Articles/1077660/                                                     !<!
#!>!                                 Point-Inside-D-Convex-Polygon-in-Fortran                                                          !<!
#!>!                                                                                                                                   !<!
#!>!                             --> EXODUS II                                                                                         !<!
#!>!                                 http://sourceforge.net/projects/exodusii/                                                         !<!
#!>!                                                                                                                                   !<!
#!>!                             --> NETCDF                                                                                            !<!
#!>!                                 http://www.unidata.ucar.edu/software/netcdf/                                                      !<!
#!>!                                                                                                                                   !<!
#!>!                             --> HDF5                                                                                              !<!
#!>!                                 http://www.hdfgroup.org/HDF5/                                                                     !<!
#!>!                                                                                                                                   !<!
#!>!                  4 ---> Related Articles (non-exhaustive list)                                                                    !<!
#!>!                                                                                                                                   !<!
#!>!                           De Martin, F., Chaljub, E., Thierry, P., Sochala, P., Dupros, F., Maufroy, E.,                          !<!
#!>!                           ... & Hollender, F. (2021). Influential parameters on 3-D synthetic ground                              !<!
#!>!                           motions in a sedimentary basin derived from global sensitivity analysis.                                !<!
#!>!                           Geophysical Journal International, 227(3), 1795-1817.                                                   !<!
#!>!                                                                                                                                   !<!
#!>!                           Brun, M., De Martin, F., & Richart, N. (2021). Hybrid asynchronous SEM/FEM                              !<!
#!>!                           co-simulation for seismic nonlinear analysis of concrete gravity dams.                                  !<!
#!>!                           Computers & Structures, 245, 106459.                                                                    !<!
#!>!                                                                                                                                   !<!
#!>!                           Sochala, P., De Martin, F., & Le Maitre, O. (2020). Model reduction for                                 !<!
#!>!                           large-scale earthquake simulation in an uncertain 3D medium. International                              !<!
#!>!                           Journal for Uncertainty Quantification, 10(2).                                                          !<!
#!>!                                                                                                                                   !<!
#!>!                           Trovato, C., Lokmer, I., De Martin, F., & Aochi, H. (2016). Long period (LP)                            !<!
#!>!                           events on Mt Etna volcano (Italy): the influence of velocity structures on                              !<!
#!>!                           moment tensor inversion. Geophysical Supplements to the Monthly Notices of the                          !<!
#!>!                           Royal Astronomical Society, 207(2), 785-810.                                                            !<!
#!>!                                                                                                                                   !<!
#!>!                           Chaljub, E., Maufroy, E., Moczo, P., Kristek, J., Hollender, F., Bard, P. Y.,                           !<!
#!>!                           ... & Chen, X. (2015). 3-D numerical simulations of earthquake ground motion in                         !<!
#!>!                           sedimentary basins: testing accuracy through stringent models. Geophysical                              !<!
#!>!                           Journal International, 201(1), 90-111.                                                                  !<!
#!>!                                                                                                                                   !<!
#!>!                           Maufroy, E., Chaljub, E., Hollender, F., Kristek, J., Moczo, P., Klin, P., ...                          !<!
#!>!                           & Bard, P. Y. (2015). Earthquake ground motion in the Mygdonian basin, Greece:                          !<!
#!>!                           The E2VP verification and validation of 3D numerical simulation up to 4 Hz.                             !<!
#!>!                           Bulletin of the Seismological Society of America, 105(3), 1398-1418.                                    !<! 
#!>!                                                                                                                                   !<!
#!>!                           Matsushima, S., Hirokawa, T., De Martin, F., Kawase, H., & Sanchez-Sesma, F. J.                         !<!
#!>!                           (2014). The Effect of Lateral Heterogeneity on Horizontal-to-Vertical Spectral                          !<!
#!>!                           Ratio of Microtremors Inferred from Observation and Synthetics.                                         !<!
#!>!                           Bulletin of the Seismological Society of America, 104(1), 381-393.                                      !<!
#!>!                                                                                                                                   !<!
#!>!                           Aochi, H., Ducellier, A., Dupros, F., Delatre, M., Ulrich, T., De Martin, F., &                         !<!
#!>!                           Yoshimi, M. (2013). Finite difference simulations of seismic wave propagation                           !<!
#!>!                           for the 2007 mw 6.6 Niigata-ken Chuetsu-Oki earthquake: Validity of models and                          !<!
#!>!                           reliable input ground motion in the near-field. Pure and Applied Geophysics,                            !<!
#!>!                           170(1), 43-64.                                                                                          !<!
#!>!                                                                                                                                   !<!
#!>!                           De Martin, F. (2011). Verification of a spectral-element method code for the                            !<!
#!>!                           Southern California Earthquake Center LOH. 3 viscoelastic case. Bulletin of the                         !<!
#!>!                           Seismological Society of America, 101(6), 2855-2865.                                                    !<!
#!>!                                                                                                                                   !<!
#!>!                  5 ---> Enjoy !                                                                                                   !<!
#!>!                                                                                                                                   !<!
#!>!===================================================================================================================================!<!
if (NOT NETCDF_C_FOUND)

   ExternalProject_Add( ${NETCDF_C_EXTERNAL}
   
      PREFIX         ${EFISPEC3D_LIBRARY_BUILD_DIR}
   
      URL            ${EFISPEC3D_LIBRARY_SOURCE_DIR}/${NETCDF_C_URL}
   
      URL_HASH       ${NETCDF_C_HASH}
   
      CMAKE_ARGS     -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX}
                     -DCMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}
                     -DHDF5_INCLUDE_DIR=${HDF5_INCLUDE_DIRS}
                     -DHDF5_C_LIBRARY=${HDF5_C_LIBRARIES}
                     -DHDF5_HL_LIBRARY=${HDF5_C_HL_LIBRARIES}
                     -DCMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}
                     -DCMAKE_LIBRARY_OUTPUT_DIRECTORY:PATH=${CMAKE_INSTALL_PREFIX}/lib
                     -DBUILD_UTILITIES=OFF
                     -DENABLE_TESTS=OFF
                     -DENABLE_DAP=OFF
                     -DCMAKE_C_FLAGS=${CMAKE_C_FLAGS}
                     -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
                     -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
   
      DEPENDS        ${HDF5_LIB}
   
   )
   
   set(NETCDF_C_INCLUDE_DIRS "${CMAKE_INSTALL_PREFIX}/include")

   set(NETCDF_C_LIBRARIES "${CMAKE_INSTALL_PREFIX}/lib/libnetcdf.so")

endif()

if(NOT TARGET ${NETCDF_C_LIB})

   message(STATUS "Compile and create target ${NETCDF_C_LIB}")

   add_library(${NETCDF_C_LIB} UNKNOWN IMPORTED)

   add_dependencies(${NETCDF_C_LIB} ${NETCDF_C_EXTERNAL})
 
   set_target_properties(${NETCDF_C_LIB} 
      PROPERTIES
      IMPORTED_LOCATION             "${NETCDF_C_LIBRARIES}"
      INTERFACE_LINK_LIBRARIES      "${NETCDF_C_LIBRARIES}"
      INTERFACE_INCLUDE_DIRECTORIES "${NETCDF_C_INCLUDE_DIRS}"
   )

endif()
