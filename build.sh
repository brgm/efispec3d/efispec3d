#!/bin/bash 


#
#------------------------------------------------------------------------------------------------------------------------
set_intel_compiler(){

   printf "\nSetting Intel compiler environment variables -> Installation automatically set to advanced mode\n"

   if command -v icc 2>&1 >/dev/null
   then

      echo "icc compiler found"

      export CC="icc"
      export CXX="icpc"
      export FC="ifort"
      export F90="ifort"
      export F77="ifort"
      export MPICC="mpiicc"
      export MPICXX="mpiicpc"
      export MPIFC="mpiifort"
      export MPIF90="mpiifort"

   else

      echo "icc compiler not found"

   fi

   if command -v icx 2>&1 >/dev/null
   then

      echo "icx compiler found"

      export CC="icx"
      export CXX="icpx"
      export FC="ifx"
      export F90="ifx"
      export F77="ifx"
      export MPICC="mpiicx"
      export MPICXX="mpiicpx"
      export MPIFC="mpiifx"
      export MPIF90="mpiifx"

   else

      echo "icx compiler not found"

   fi

}
#------------------------------------------------------------------------------------------------------------------------


#
#------------------------------------------------------------------------------------------------------------------------
set_gnu_compiler(){

   printf "\nSetting GNU compiler environment variables\n"

   export CC="gcc"
   export CXX="g++"
   export FC="gfortran"
   export F90="gfortran"
   export F77="gfortran"
   export MPICC="mpicc"
   export MPICXX="mpicxx"
   export MPIFC="mpifort"
   export MPIF90="mpifort"

}
#------------------------------------------------------------------------------------------------------------------------

#
#------------------------------------------------------------------------------------------------------------------------
usage(){

cat <<END_OF_HELP

   Usage : $(basename $0)

      General options
      ---------------

      -h : Display help

      -l : Activate advanced install (for GNU or Intel compiler)
           EFISPEC3D is built with pre-compiled libraries shipped in EFISPEC3D repository or attempt to compile libraries from scratch if missing.
           If -l not given, quick install is done using system libraries found in /usr/lib*. For GNU compiler only.

      -c <compiler> : Select compiler. <compiler> = gnu | intel.

      -o <options>  : Specific compiler options. (e.g., x<processor> for intel compiler or march=<processor> for gnu compiler)

      -r <rheology> : Select rheology (elastic or viscoelastic). <rheology> = e | v


      Going further
      -------------

      Read file INSTALL.md 
      
END_OF_HELP

exit 0

}

#
#------------------------------------------------------------------------------------------------------------------------
#main part of bash script

#unset compiler environment variables
unset CC
unset CXX
unset FC
unset F90
unset F77
unset MPICC
unset MPICXX
unset MPIFC
unset MPIF90

#default option value
h=""
c=""
o=""
r=""
l=false
VISCO="OFF"

while getopts 'lc:o:r:' opt; do

   case "$opt" in

   l ) #Specify local libraries
      l=true;;

   c ) #Specify compiler. Possible options: gnu, intel
      c=${OPTARG}
      h="ok"
      [ "$c" != "gnu" ]  && [ "$c" != "intel" ] && usage
      [ "$c" = "gnu" ]   && set_gnu_compiler   && VER=$(gcc   --version  | awk 'NR==1 {print $3}')
      [ "$c" = "intel" ] && set_intel_compiler && VER=$(${F90} --version | awk 'NR==1 {print $3}') && l=true
      ;;
  
   o ) #Specify compiler options
      o="-${OPTARG}";;

   r ) #Specify rheology
      r=${OPTARG}
      h="ok"
      [ "$r" != "e" ]  && [ "$r" != "v" ] && usage
      [ "$r" = "e" ] && VISCO="OFF"
      [ "$r" = "v" ] && VISCO="ON"
      printf "\nViscoelastic rheology ${VISCO}"
      ;;

   h ) #Display help
      usage;;

   esac

done

shift $((OPTIND-1))

if [ -z "$h" ]
then
   usage
   exit
fi

printf "\nCompiler version : ${c} ${VER}"
printf "\nCompiler user options : ${o}"

#
# define directories
BUILD_DIR="$PWD/build"
INSTALL_DIR="$PWD/bin"

# remove previous dirs
rm -rf $BUILD_DIR
rm -rf $INSTALL_DIR/efispec3d

# create and go to build dir
mkdir -p $BUILD_DIR
cd $BUILD_DIR

if [ "$l" = true ]
then

   printf "\nAdvanced Install -> Building EFISPEC3D with pre-compiled libraries shipped in EFISPEC3D repository or attempt to compile them from scratch if missing.\n\n"

   # build
   cmake .. \
    -DCMAKE_BUILD_TYPE=release \
    -DUSE_SYSTEM_LIBRARIES=OFF \
    -DEFISPEC3D_INSTALL_DIR=${INSTALL_DIR} \
    -DVISCO=${VISCO} \
    -DOPT=${o}

else

   printf "\nQuick install -> Building EFISPEC3D with system libraries\n\n"

   source libs.sh

   # build
   cmake .. \
    -DCMAKE_BUILD_TYPE=release \
    -DUSE_SYSTEM_LIBRARIES=ON \
    -DEFISPEC3D_INSTALL_DIR=${INSTALL_DIR} \
    -DVISCO=${VISCO} \
    -DOPT=${o}

fi

ncore=$(grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}')

make -j${ncore}
make install

