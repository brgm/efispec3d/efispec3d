echo "test efispec"


distrib=`cat /etc/os-release | grep "^ID="`
distribId=${distrib:3}

#Changed the name of RockyLinux to match the formats of other names
if [[ "$distribId" == '"rocky"' ]] ; then
  distribId="rocky"
fi

#Print distribution detected
echo "$distribId detected !"
echo

  if [[ "$distribId" == "ubuntu" ]] || [[ "$distribId" == "debian" ]] ; then

      packagesNames=("CC" "gfortran" "CMake" "Curl" "MPICH" )
      packages=("build-essential" "gfortran" "cmake" "libcurl4-openssl-dev"  "libmpich-dev" )
      versionsMin=("0" "0" "3.16" "0"  "3.1.0" )
      commandePackage="apt"

    elif [[ "$distribId" == "fedora" ]]; then

        packagesNames=("GCC" "gfortran" "CMake" "Curl"  "MPICH" )
        packages=("gcc" "gcc-gfortran" "cmake" "libcurl-devel" "mpich-devel" )
        versionsMin=("0" "0" "3.16" "0" "3.1.0")
        commandePackage="dnf"

         sudo $commandePackage install -y dpkg

    elif [[ "$distribId" == "rocky" ]]; then

        packagesNames=("GCC" "g++" "gfortran" "CMake" "Curl"  "MPICH")
        packages=("gcc" "gcc-c++" "gcc-gfortran" "cmake" "libcurl-devel" "mpich-devel" )
        versionsMin=("0" "0" "0" "3.16" "0" "3.1.0")
        commandePackage="dnf"

        #add repository to install packages
        sudo $commandePackage install wget
        wget "https://download-ib01.fedoraproject.org/pub/epel/8/Everything/x86_64/Packages/e/epel-release-8-15.el8.noarch.rpm"
        sudo rpm -Uvh epel-release*rpm
        rm epel-release*rpm

          #install dpkg to compare package versions
        sudo $commandePackage install -y dpkg




    fi

#Update package management command
    echo
    echo "$commandePackage update : "
    sudo $commandePackage update -y > /vagrant/packageUpdate 2> /vagrant/packageUpdate.err 
    

  for i in ${!packagesNames[@]};
    do

      #Install for each package


      packageName=${packagesNames[$i]}
      package=${packages[$i]}
       versionMin=${versionsMin[$i]}


    if [[ "$distribId" == "ubuntu" ]] || [[ "$distribId" == "debian" ]] ; then
        lang=`locale | grep LANG= | cut -d= -f2 | cut -d_ -f1`
        if [[ "$lang" == "fr" ]]; then
          version=`apt-cache policy $package | grep "Installé"`
          numversion=${version:13}
        else
          version=`apt-cache policy $package | grep "Installed"`
          numversion=${version:13}
        fi
      else
        if [[ "$distribId" == "fedora" ]] || [[ "$distribId" == "rocky" ]] ; then
          version=`dnf info $package --installed | grep "^Version"`
          numversion=${version:15}
        else
          numversion=""
        fi
      fi

      #Install libraries that are not installed

      echo "$packageName version detected : $numversion"
      if [[ "$numversion" == "(none)" ]] || [[ "$numversion" == "(aucun)" ]]; then
        numversion=""
      fi

      if $(dpkg --compare-versions "$numversion" "ge" "$versionMin") ; then
        echo "$packageName version -- OK"
      else
        echo " -- Install $packageName -- "
        if [[ "$distribId" == "rocky" ]] ; then
          sudo $commandePackage --enablerepo=powertools install -y $package >> /vagrant/packageInstall 2>> /vagrant/packageInstall.err
        else
          
          sudo $commandePackage install -y $package >> /vagrant/packageInstall 2>> /vagrant/packageInstall.err
         fi 
      fi
    done


  



#sudo apt install -y  "build-essential" "gfortran" "cmake" "libcurl4-openssl-dev" "libmpich-dev" "git" > /vagrant/packageInstall 2> /vagrant/packageInstall.err 

