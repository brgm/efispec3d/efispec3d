echo "compile efispec"


distrib=`cat /etc/os-release | grep "^ID="`
distribId=${distrib:3}

#Changed the name of RockyLinux to match the formats of other names
if [[ "$distribId" == '"rocky"' ]] ; then
  distribId="rocky"
fi

#Print distribution detected
echo "$distribId detected !"
echo


 if [[ "$distribId" == "fedora" ]] || [[ "$distribId" == "rocky" ]] ; then

      #add Path to mpich home
      export MPI_HOME=/usr/lib64/mpich/

    else

      #add Path to mpich home
      export MPI_HOME=/usr/lib/x86_64-linux-gnu/

    fi


#cd $HOME/efispec3d-interne
#git config --global --add safe.directory /vagrant/efispec3d-interne 
#git status

cd $HOME
mkdir -p build
cd build
pwd
cmake /efispec3d-interne > /vagrant/resultCmake 2> /vagrant/resultCmake.err 
make install > /vagrant/resultInstall 2> /vagrant/resultInstall.err
make check-loh3-elastic > /vagrant/resultTest 2> /vagrant/resultTest.err



