#!/bin/bash


usage () { echo "launch multiple test onto vm with vagrant


You may have these directory in ./:

config_file/Vagrantfile_generic		
efispec3d-interne	
launch.bash		
scripts/script_test_efispec.sh
scripts/script_compile_efispec.sh

test some boxes: 

 ./launch.bash -e \$(pwd)/../../../efispec3d-interne ubuntu/focal64 generic/fedora30 generic/rocky8

delete all vm located into vmFileTest: 
 ./launch.bash -d 

delete all vm located into userDir: 

 ./launch.bash -d -R userDir

delete all vm located into vmFileTest and with  time stamp 220603165015: 

 ./launch.bash -d  -s 220603165015

delete all vm located into myVm : 

 ./launch.bash -d  -R myVm 

delete all vm located into myVm and with  time stamp 220603165015: 

 ./launch.bash -d  -R myVm -s 220603165015


"; }

cpt=0
while getopts e:R:hds: options
do
case $options in
e ) efispecDir=$OPTARG ;;
R ) vmFileTest=$OPTARG ;;
s ) stamp=$OPTARG ;;
d ) delete="true" ;;
h  ) usage; exit;;
:  ) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
\? ) echo "Unknown option: -$OPTARG" >&2; exit 1;;
*  ) echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
esac
done
shift $(( $OPTIND -1 ))

curPath=$(pwd)



[ -d ${efispecDir} ] && echo "Directory ${efispecDir} exists." || { echo "Error: Directory ${efispecDir} does not exists.  Stopped .." ; exit 1 ; }


[ -z $vmFileTest ] && vmFileTest="vmFileTest"

if [  ! -z $delete ] 
then
if [  ! -z $stamp ] 
then
for vm in $(find  ${vmFileTest} -name "*${stamp}") 
    do
     cd $vm
     if ! grep -q "not created" <(vagrant status) 
     then
     echo delete Vagrant VM $vm
     vagrant destroy -f
     fi
     cd $curPath
     echo rm -rf $vm
     rm -rf $vm
    done
else
for vm in $(ls  ${vmFileTest} ) 
    do
     cd $vmFileTest/$vm
     if ! grep -q "not created" <(vagrant status) 
     then
    echo delete Vagrant VM $vm
     vagrant destroy -f
     fi
     cd $curPath
     echo rm -rf  ${vmFileTest}/$vm
     rm -rf  ${vmFileTest}/$vm
    done
 echo rm -rf ${vmFileTest}
 rm -rf ${vmFileTest}
fi
exit 0
fi

#vmFileTest="vmFileTest"
stamp=$(date "+%y%m%d%H%M%S")

mkdir -p ${vmFileTest}

for disbrib in $@ 
do
echo build $disbrib
vmName=${disbrib##*/}at${stamp}
echo create ${vmFileTest}/${vmName} folder
mkdir -p ${vmFileTest}/${vmName}
cat config_file/Vagrantfile_generic |  sed s!myBox!${disbrib}!g |  sed s!myName!${vmName}!g  |  sed s!efispecGitFolder!${efispecDir}!g    > ${vmFileTest}/${vmName}/Vagrantfile
for file in $(ls scripts) 
do
    cp scripts/$file ${vmFileTest}/${vmName}
done
cd ${vmFileTest}/${vmName}
 vagrant up
  vagrant ssh -c '. $HOME/script_compile_efispec.sh'
   vagrant halt
cd -
done

