#!/bin/sh


PATH_CUB="/home/${USER}/Codes/CUBIT-13.2/"
PATH_EFI="/home/admin/Codes/EFISPEC3D.GIT/efispec3d/bin/intel/17.0.1/bin/"
P="e2vp2"

./clean.sh

#create domain with flat free surface
${PATH_CUB}/cubit -nogui -nojournal -nographics -batch  ${P}.jou

#apply topography
${PATH_EFI}/efispec3d_cubit_topography ${P}.ex2 zplane -2000 mnt.grd

#search hexa in basin to refine
${PATH_EFI}/efispec3d_cubit_refine ${P}.ex2 basin_border.txt zplane -1000.0

#launch cubit to refine
${PATH_CUB}/cubit -nogui -nojournal -nographics ${P}_topo.jou
