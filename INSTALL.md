# Foreword

This document describes how to build EFISPEC3D from source codes on Linux. By default, EFISPEC3D is installed in $HOME/efispec3d directory.

## Build and runtime dependencies

To build EFISPEC3D, you must have:

- A MPI compiler (e.g., Intel MPI, MPICH, OpenMPI, etc.)
- [Git](https://git-scm.com/)
- [CMake](https://cmake.org/) (>=3.16)
- [Metis](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview) (>=5.1.0)
- [ExodusII](https://github.com/sandialabs/seacas) (>=5.14.0)
- [HDF5](https://www.hdfgroup.org) (>=1.10.0)
- [NetCDF C](https://www.unidata.ucar.edu/software/netcdf) (>=4.7.3)
- [NetCDF Fortran](https://www.unidata.ucar.edu/software/netcdf) (>=4.5.2)

## Cloning EFISPEC3D
```bash
git clone https://gitlab.brgm.fr/brgm/efispec3d/efispec3d.git
```

## Quick Install

The quick install uses the distribution package manager to retrieve and install mandatory compilers and thirdparty libraries. Root priviledge are required. 

The quick install is for GNU compiler only. To do the quick install, run:

```bash
./build.sh -c gnu
```

## Advanced Install

Before proceeding, please make sure you have cmake, curl, openssl, perl, c/fortran compiler and a MPI library.

The advanced install uses pre-compiled thirdparty libraries shipped with EFISPEC3D or attempt to compile them from scratch if missing.

To do the advanced install with INTEL compiler, run:

```bash
 ./build.sh -l -c intel
```

To do the advanced install with GNU compiler, run:

```bash
 ./build.sh -l -c gnu
```


## DIY Install

Do It Yourself Install.

To build EFISPEC3D and its dependencies with GNU compiler, run:

```bash
mkdir build

cd build

CC=gcc CXX=g++ FC=gfortran MPICC=mpicc MPICXX=mpicxx MPIFC=mpifort cmake .. [-DVISCO=OFF|ON] [-DOPT=Specific compiler options]

make

make install
```

To build EFISPEC3D and its dependencies with Intel compiler, run:

```bash
mkdir build

cd build

CC=icc CXX=icpc FC=ifort MPICC=mpiicc MPICXX=mpiicpc MPIFC=mpiifort cmake .. [-DVISCO=OFF|ON] [-DOPT=Specific compiler options]

make

make install
```


## Check install

If -DVISCO=OFF, run:

```bash
make check-loh3-elastic
```

If -DVISCO=ON, run:

```bash
make check-loh3-viscoelastic
```
