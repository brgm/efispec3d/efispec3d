# EFISPEC3D : Elements FInis SPECtraux 3D

[website](http://efispec.free.fr)

[gitlab](https://gitlab.brgm.fr/brgm/efispec3d)

## What is EFISPEC3D

EFISPEC3D is a computer program that solves the three-dimensional equations of
motion using a continuous Galerkin spectral finite element method. EFISPEC3D is
mainly dedicated to ground motion simulations in complex geological structures,
but can also be used for any problems involving the three-dimensional equations
of motion.

## Licences

CeCILL V2 and GNU GPL V3 (see file [LICENSE.txt](LICENSE.txt))

## A reminder

If you think it is appropriate, you may consider including
this article in the reference list of your future publications that
will benefit from the availability of EFISPEC3D:

> De Martin, F. (2011).
> Verification of a spectral-element method code for the Southern California Earthquake Center LOH.3 viscoelastic case. Bulletin of the Seismological Society of America Vol.101, No.6
> https://doi.org/10.1785/0120100305

## Install

See file [INSTALL.md](INSTALL.md)
