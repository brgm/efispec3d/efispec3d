to perform test compilation on multiple linux distribution with vagrant:

    
    git clone https://gitlab.brgm.fr/brgm/efispec3d/efispec3d-interne.git

    cd efispec3d-interne/test

    git checkout origin/dev_EM_compilation
    
    cd vagrant
    
    ./launch.bash -h
    
    ./launch.bash -e $(pwd)/../../../efispec3d-interne ubuntu/focal64 generic/fedora30 generic/rocky8

    ls vmFileTest
